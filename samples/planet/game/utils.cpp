#include "utils.h"

Int randInt(Int from, Int to)
{
	Int v = to - from;
	Int rnd = mthGetRandom(v+1);
	return from + rnd;
}

Float randFloat(Float from, Float to)
{
	return from + mthGetRandomFx(to - from);
}
