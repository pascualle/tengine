#pragma once

struct TouchPadData;

class Input
{
public:
	Input();
	virtual ~Input();

	void init();
	void update();

	const TouchPadData& touchPadData() const { return *tpdata; }

private:
	TouchPadData* tpdata;
};