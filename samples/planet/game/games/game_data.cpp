#include "game_data.h"

GameData::GameData(Game* gm)
	: game(gm)
	, levelsCount(LEVELS_COUNT)
	, lose(false)
{
	initLevels();
	reset(true);
}

GameData::~GameData()
{

}

void GameData::reset(bool newGame)
{
	if (newGame)
	{
		currentLevel = 1;
	}

	levelStatus = LS_NONE;
	points = 0;
	lose = false;
}

const GameData::LevelData& GameData::getCurrentData() const
{
	SDK_ASSERT(0 < currentLevel && currentLevel <= LEVELS_COUNT);

	return levelData[currentLevel];
}

UnitClass GameData::getNextDataUnit() const
{
	if (currentLevel < LEVELS_COUNT)
	{
		return levelData[currentLevel + 1].newUnit;
	}

	return UnitClass::U_UNIT;
}

Int GameData::getLevelPoints() const
{ 
	return getCurrentData().levelPoints;
}


bool GameData::levelFinishCondition() const
{
	return getPoints() == getLevelPoints() || lose;
}

bool GameData::gameFinishCondition() const
{
	return levelFinishCondition() && getLevelsCount() == getCurrentLevel();
}

void GameData::initLevels()
{

	levelData[0] = { 
		0,
		false,
		UnitClass::U_UNIT,
		UnitClass::U_UNIT,
		{ UnitClass::U_UNIT, } };


	levelData[1] = { 
		3,
		true,
		UnitClass::U_UNIT,
		UnitClass::U_UNIT,
		{ UnitClass::U_UNIT, } };

}

void initLevelData()
{

}

Int GameData::getStartPointId(StartPoint startPoint) const
{
	switch (startPoint)
	{
	default:
		SDK_ASSERT(false);
		break;
	}
	return -1;
}