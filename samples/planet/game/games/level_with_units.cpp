#include "touchpad.h"
#include "level_with_units.h"
#include "units/units_pool.h"
#include "units/unit.h"
#include "games/game.h"

LevelWithUnits::LevelWithUnits(GameLayer lr, Game* gm)
	: Level(lr, gm)
	, unitsPool(new UnitsPool())
	, unitIter(0)
	, hitPoint({ 0, 0, false })
{

}

LevelWithUnits::~LevelWithUnits()
{
	delete unitsPool;
}

void LevelWithUnits::resetUnits()
{
	unitsPool->reset();
}

void LevelWithUnits::updateUnits(Int ms)
{
	if (hitPoint.hit)
	{
		onUpdateHitPoint();

		Unit* unit = nextUnit(true);
		while (unit)
		{
			if ((unit->getHealth() > 0) && unit->isCollidible() && unit->collide(hitPoint.x, hitPoint.y))
			{
				if (unitClick(unit, hitPoint.x, hitPoint.y))
				{
					break;
				}
			}
			unit = nextUnit();
		}

	}

	unitsPool->updateUnits(ms);

	hitPoint.hit = false;
}

void LevelWithUnits::updateInput(const TouchPadData& tpdata)
{
	if (!inputActive())
	{
		return;
	}

	hitPoint = { 0, 0, false };

	if (tpdata.point[0].mTouch)
	{
		Float scale = getRenderPlaneScale(getLayer());
		Float offsetX = getViewOffsetX(getLayer());
		Float offsetY = getViewOffsetY(getLayer());

		Float x = (Float)tpdata.point[0].mX / scale;
		Float y = (Float)tpdata.point[0].mY / scale;

		x += offsetX;
		y += offsetY;

		hitPoint = { (Int)x, (Int)y, true };
	}
}

Unit* LevelWithUnits::createUnit(UnitClass unitClass, Vector pos)
{
	Unit* unit = unitsPool->getUnitFromPool(unitClass);
	unit->setLevel(this);
	unit->setActive(true);
	unit->setPosition(pos);
	unit->setState(STATE_IDLE);

	return unit;
}

const Unit* LevelWithUnits::getUnit(Int i) const
{
	return unitsPool->getUnit(i);
}

Unit* LevelWithUnits::getUnit(Int i)
{
	return unitsPool->getUnit(i);
}

Int LevelWithUnits::getUnitsCount() const
{
	return unitsPool->getUnitsCount();
}

const Unit* LevelWithUnits::nextUnit(bool fromBegin) const
{
	if (fromBegin)
	{
		unitIter = 0;
	}
	Int firstIter = fromBegin ? unitIter : unitIter + 1;

	for (Int i = firstIter; i < getUnitsCount(); ++i)
	{
		if (getUnit(i)->getActive())
		{
			unitIter = i;
			return getUnit(unitIter);
		}
	}
	return NULL;
}

Unit* LevelWithUnits::nextUnit(bool fromBegin)
{
	return const_cast<Unit*>(const_cast<const LevelWithUnits*>(this)->nextUnit(fromBegin));
}
