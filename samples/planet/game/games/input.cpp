#include "input.h"
#include "tengine.h"
#include "touchpad.h"
#include "gamepad.h"

Input::Input()
	:tpdata(new TouchPadData())
{
}

Input::~Input()
{
	delete tpdata;
}

void Input::init()
{
	MI_CpuClear8(tpdata, sizeof(TouchPadData));
	InitGamePad();
	InitTouchPad();
}

void Input::update()
{
	ReadTouchPadData(tpdata);
}
