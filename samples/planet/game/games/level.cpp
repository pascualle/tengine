#include "games/level.h"
#include "games/game.h"
#include "units/units_pool.h"
#include "level_id.h"
#include "units/units.h"
#include "units/unit.h"

Level::Level(GameLayer lr, Game* gm)
	: game(gm)
	, layer(lr)
{
}

Level::~Level()
{
}

GameData* Level::gameData()
{
	return const_cast<GameData*>(const_cast<const Level*>(this)->gameData());
}

const GameData* Level::gameData() const
{
	return game->gameData();
}

void Level::onStateEnterBase()
{
	onStateEnter();
}

void Level::update(Int ms)
{
	updateState(ms);
}
