#pragma once

#include "level.h"

class UnitsPool;



class LevelWithUnits : public Level
{
public:
	LevelWithUnits(GameLayer lr, Game* gm);
	virtual ~LevelWithUnits() override;
	Unit* createUnit(UnitClass unitClass, Vector pos);
	const Unit* nextUnit(bool fromBegin = false) const;
	Unit* nextUnit(bool fromBegin = false);

	const Unit* getUnit(Int i) const;
	Unit* getUnit(Int i);
	Int getUnitsCount() const;

protected:
	struct HitPoint
	{
		Int x;
		Int y;
		bool hit;
	};

	void updateUnits(Int ms);
	virtual void onUpdateHitPoint() {}
	HitPoint& getHitPoint() { return hitPoint; }
	void resetUnits();

	virtual bool inputActive() const { return false; }

	virtual bool unitClick(Unit* unit, Int x, Int y) { (void)unit; (void)x; (void)y; return false; }

private:

	virtual void updateInput(const TouchPadData& tpdata) override;

	UnitsPool* unitsPool;
	mutable Int unitIter;
	HitPoint hitPoint;
};