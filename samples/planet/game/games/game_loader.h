#pragma once

#include "tengine.h"
#include "level_id.h"
#include "games/game.h"
#include "common/state_machine.h"

class GameLoader : public StateMachine
{
 public:
	 friend class Game;
	enum GameLoaderState
	{
		GLS_NONE = 0,
		GLS_DISPLAYINIT,
		GLS_LOADING,
		GLS_GAME,
		GLS_PREPARE_FOR_LOAD,
		GLS_EXIT,
		
		GLS_COUNT
	};
	struct RenderScreenData
	{
		s32 viewWidth;
		s32 viewHeight;
		s32 defaultHeight;
		f32 aspectRatioPC;
		f32 renderScale;
	};

	typedef void (*OnGameStateCallback)();

	GameLoader();
	virtual ~GameLoader() override;

	void setGameStateCallback(GameLoaderState gameState, OnGameStateCallback callback);

	Game* getGame() { return game; }

	void exit();

	void init();
	void release();

	void beginLoad();
	//void layerLoad(GameLayer layer);
	//void layerRelease(GameLayer layer);

	void onBeginUpdate(GameLayer layer);
	void onDrawBackgroundTiles(GameLayer layer);
	void onDrawObject(GameLayer layer, const u32& iId, BOOL* const opDraw);

	void setLayerVisible(GameLayer layer, BOOL visible);
	//switch between GAME+HUD and LAODING layers visibility  
	void setLayerVisibleGame(BOOL visible);

	RenderScreenData& renderScreenData() { return screenData; }
	void setRenderScreenData(s32 w, s32 h);
	void setRenderPlaneScale();

 private:

	virtual void onActiveChangedBase() override;
	virtual void onStateEnterBase() override;
	virtual void onStateUpdateBase(Int ms) override;

	Game* game;
	s32 currentMS;
	RenderScreenData screenData;

	OnGameStateCallback gameStateCallbacks[GLS_COUNT];
};