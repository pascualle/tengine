#include "games/game_loader.h"	
#include "games/game.h"
#include "constants.h"
#include "utils.h"


GameLoader::GameLoader()
	: currentMS(0)
	, game(NULL)
{
	setActive(true);
	setState(GLS_NONE);

	for (Int i = 0; i < GLS_COUNT; ++i)
	{
		gameStateCallbacks[i] = NULL;
	}

	screenData.defaultHeight = 700;
	screenData.aspectRatioPC = 2160.0f/1080.0f;
	screenData.viewWidth = (s32)((f32)screenData.defaultHeight * screenData.aspectRatioPC);
	screenData.viewHeight = screenData.defaultHeight;
}

void GameLoader::setRenderScreenData(s32 w, s32 h)
{
	screenData.viewWidth = w;
	screenData.viewHeight = h;
	screenData.renderScale = (f32)h / screenData.defaultHeight;
}

void GameLoader::setRenderPlaneScale()
{
	::setRenderPlaneScale(screenData.renderScale, LAYER_GAME);
	::setRenderPlaneScale(screenData.renderScale, LAYER_HUD);
	::setRenderPlaneScale(screenData.renderScale, LAYER_LOADING);
}

GameLoader::~GameLoader()
{
}

void GameLoader::setGameStateCallback(GameLoaderState gameState, OnGameStateCallback callback)
{
	gameStateCallbacks[gameState] = callback;
}

void GameLoader::exit()
{
	setState(GLS_EXIT);
}

void GameLoader::init()
{
	setActive(true);
	if (!game)
	{
		game = new Game(this);
		game->init();
	}
}

void GameLoader::release()
{
	if (game)
	{
		delete game;
		game = NULL;
	}
	setActive(false);
}

void GameLoader::onActiveChangedBase()
{
}

void GameLoader::onStateEnterBase()
{
	//if (getState() == GLS_GAME)
	//{
	//	game->setActive(true);
	//}
}

void GameLoader::onStateUpdateBase(Int ms)
{
	currentMS = ms;

	OnGameStateCallback callback = gameStateCallbacks[getState()];
	if (callback)
	{
		callback();
	}

}

void GameLoader::beginLoad()
{
	setLayerVisibleGame(FALSE);

	//layerRelease(LAYER_HUD);
	//layerRelease(LAYER_GAME);
}

//void GameLoader::layerRelease(GameLayer layer)
//{
//	game->levelRelease(layer);
//}

//void GameLoader::layerLoad(GameLayer layer)
//{
//	game->levelLoad(layer);
//}

void GameLoader::onBeginUpdate(GameLayer layer)
{
	if (getState() == GLS_GAME)
	{
		if (!game->getActive())
		{
			game->setActive(true);
		}
		game->updateState(currentMS);

		game->onBeginUpdate(layer);
	}
}

void GameLoader::onDrawBackgroundTiles(GameLayer layer)
{
	game->onDrawBackgroundTiles(layer);
}

void GameLoader::onDrawObject(GameLayer layer, const u32& id, BOOL* const opDraw)
{
	if (getState() == GLS_GAME)
	{
		game->onDrawObject(layer, id, opDraw);
	}
}

void GameLoader::setLayerVisible(GameLayer layer, BOOL visible)
{
	setVisible(layer, visible);
}

void GameLoader::setLayerVisibleGame(BOOL visible)
{
	setLayerVisible(LAYER_GAME, visible);
	setLayerVisible(LAYER_HUD, visible);
	
	setLayerVisible(LAYER_LOADING, !visible);
}
