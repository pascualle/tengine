#pragma once

#include "utils.h"
#include "units/units_data.h"

#define LEVELS_COUNT 1

class Game;

class GameData
{
public:
	friend class Game;
	enum LevelStatus
	{
		LS_NONE,
		LS_LOSE,
		LS_WIN,
		LS_WIN_GAME,
	};
	enum StartPoint
	{
		START_POINT,
		START_POINT_COUNT
	};
	struct LevelData
	{
		Int levelPoints;
		bool rain;
		UnitClass newUnit;
		UnitClass cap;
		UnitClass startUnits[START_POINT_COUNT];
	};

	GameData(Game* gm);
	~GameData();

	void reset(bool newGame);

	void setLevelStatus(LevelStatus status) { levelStatus = status; }
	LevelStatus getLevelStatus() const { return levelStatus; }

	void addPoints(Int p = 1) { points += p; }
	Int getPoints() const { return points; }
	
	Int getLevelPoints() const;
	const LevelData& getCurrentData() const;
	UnitClass getNextDataUnit() const;

	bool levelFinishCondition() const;
	bool gameFinishCondition() const;
	Int getCurrentLevel() const { return currentLevel; }

	void zombieInTheHouse() { lose = true; }
	bool isLose() const { return lose; }

	Int getStartPointId(StartPoint startPoint) const;

private:
	void initLevels();
	void initLevelData();
	Int getLevelsCount() const { return levelsCount; }

	void setCurrentLevel(Int currLevel) { currentLevel = currLevel; }
	void nextLevel() { setCurrentLevel(getCurrentLevel() + 1); }

	Game* game;

	LevelStatus levelStatus;
	Int currentLevel;
	Int points;
	Int levelsCount;
	LevelData levelData[LEVELS_COUNT+1];

	bool lose;
};