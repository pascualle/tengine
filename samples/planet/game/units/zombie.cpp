#include "zombie.h"
#include "games/level_game.h"
#include "games/game.h"
#include "games/game_data.h"

#define FALL_ANG 45
#define ATTACK_TIME 800
#define Z_HEALTH 20
#define Z_HEALTH_HIT 10


Zombie::Zombie(Int id)
	: Unit(id, UnitClass::U_ZOMBIE)
{
	setMaxHealth(Z_HEALTH);
}

void Zombie::reset()
{
	const Float maxSpeed = 0.08f;
	const Float minSpeed = 0.02f;
	Float sp = randFloat(minSpeed, maxSpeed);
	setSpeed(sp);
	setHealth(getMaxHealth());
}

bool Zombie::isMovable() const
{
	return isAlive() && !isAttack() && getHealth() > 0;
}

void Zombie::onActiveChanged()
{
	if (getActive())
	{
		reset();
	}
}

void Zombie::onHit()
{
}

void Zombie::onMove(Int ms)
{
	(void)ms;

	Vector posCurr = getPosition();
	setPosition(posCurr);
}

void Zombie::onStateEnter()
{
}

void Zombie::onStateUpdate(Int ms)
{
}
