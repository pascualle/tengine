#pragma once

#include "utils.h"
#include "units_data.h"
#include "common/state_machine.h"
#include "level_id.h"

class LevelGame;
class Level;

class Unit : public StateMachine
{
public:
	enum Prop
	{
		P_NONE = 0,
		P_ACTIVE = 1,
		P_POS = 2,
		P_DIR = 4,
		P_SPEED = 8,
		P_MOVE = P_POS | P_DIR | P_SPEED,
		P_STATE = 16,
		P_HEALTH = 32,
	};
	Unit(Int unitid, UnitClass uClass);
	virtual ~Unit();

	void setLevel(Level* lev);

	UnitClass getClass() const { return unitClass; }

	Int getId() const { return id; }
	void setPosition(const Vector& pos) { position = pos; setNeedUpdate(P_POS); }
	Vector getPosition() const { return position; }
	void setDirection(const Vector& pos) { direction = pos; setNeedUpdate(P_DIR); }
	Vector getDirection() const { return direction; }
	void setSpeed(Float sp) { speed = sp; setNeedUpdate(P_SPEED); }
	void setHealth(Int h);
	Int getHealth() const { return health; }
	Int getMaxHealth() const { return maxHealth; }

	bool collide(Float x, Float y) const;
	bool collide(Vector pos)  const;
	bool collideVec(Vector p0, Vector p1) const;
	//virtual bool onClick(Int x, Int y) { (void)x; (void)y; return false; }

	bool isAlive() const;
	bool isIdle() const;
	bool isDead() const;
	bool isHit() const;
	bool isAttack() const;

	virtual bool isMovable() const { return true; }
	virtual bool isCollidible() const { return false; }

	virtual bool hit(Int damage);

	virtual void explode();

	GameLayer getLayer() const;

	virtual bool hasPossibilityToAttack(bool melee = true) { (void)melee; return false; }

	virtual Unit* addCap(UnitClass capClass) { (void)capClass; return NULL; }

protected:
	virtual void onActiveChanged() {}
	virtual void onStateUpdate(Int ms) { (void)ms; }
	virtual void onStateEnter() {}
	virtual void onStateFinish() {}

	virtual void onHit() {}
	virtual void onMove(Int ms) { (void)ms; }
	LevelGame* getLevel() { return level; }
	const LevelGame* getLevel() const { return level; }

	void setMaxHealth(Int h) { maxHealth = h; }

private:
	virtual void onActiveChangedBase() override;
	virtual void onStateUpdateBase(Int ms) override;
	virtual void onStateEnterBase() override;
	virtual void onStateFinishBase() override;

	bool needUpdate(Prop prop) const;
	void setNeedUpdate(Prop prop);
	void resetNeedUpdate(Prop prop);
	void updateHealth();
	void updateState();
	bool needUpdateMove();
	void updateMove(Int ms);
	void reset();

	UnitClass unitClass;
	LevelGame* level;
	Vector position;
	Vector direction;
	Float speed;
	Int id;
	Int updateProp;
	Int health;
	Int maxHealth;
};
