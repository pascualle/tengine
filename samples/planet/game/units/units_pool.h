#pragma once

#include "units/units_data.h"
#include "utils.h"
#include "level_id.h"

class Unit;

class UnitsPool
{
public:
	UnitsPool();
	virtual ~UnitsPool();

	void addUnits(UnitClass unitClass, Int firstId, Int count);

	void updateUnits(Int ms);

	Unit* getUnitFromPool(UnitClass unitClass);
	const Unit* const getUnit(Int i) const { return units[i]; }
	Unit* getUnit(Int i) { return units[i]; }
	Int getUnitsCount() const { return unitsCount; }

	void reset();

private:
	struct UnitIndex
	{
		Int startIndex;
		Int count;
	};

	void addUnit(Unit* unit);
	Unit* newUnit(UnitClass unitClass, Int id);

	Unit* units[UNITS_COUNT];
	Int unitsCount;
	UnitIndex unitsInd[(Int)UnitClass::U_COUNT];

};