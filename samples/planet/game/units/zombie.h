#pragma once

#include "unit.h"

#define Z_BLOODS_COUNT 4

class Zombie : public Unit
{
public:
	Zombie(Int id);

private:
	virtual void onHit() override;
	virtual void onMove(Int ms) override;
	virtual void onStateEnter() override;
	virtual void onStateUpdate(Int ms) override;
	virtual void onActiveChanged() override;
	void reset();

	virtual bool isCollidible() const override { return true; }
	virtual bool isMovable() const override;
};

