#pragma once

#include "constants.h"

#define U_UNIT_ID 0
#define U_UNIT_COUNT 0

//#define U_ZOMBIE_ID M0_POOL_OBJECT_22_0_POOL_FIRST
//#define U_ZOMBIE_COUNT M0_POOL_OBJECT_22_0_POOL_COUNT

#define UNITS_COUNT (1)

enum class UnitClass
{
	U_UNIT,

	U_COUNT
};