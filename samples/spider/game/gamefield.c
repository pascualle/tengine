//-------------------------------------------------------------------------------------------
/*

	This example is based on code
	https://github.com/subprotocol/verlet-js
	demo:
	https://github.com/subprotocol/verlet-js/blob/master/examples/spiderweb.html
	http://subprotocol.com/verlet-js/examples/spiderweb.html

*/
//-------------------------------------------------------------------------------------------

#include "tengine.h"
#include "texts.h"
#include "touchpad.h"
#include "gamepad.h"
#include "gamefield.h"
#include "level_id.h"
#include "constants.h"
#include "verlet.h"
#include "gui.h"

//-------------------------------------------------------------------------------------------

enum GameStates
{
	gstNONE = 0,
	gstDISPLAYINIT,
	gstLOADING,
	gstGAME
};

enum LoadMarkers
{
	LM_INIT = 0,
	LM_LOAD
};

enum GameLayers
{
	GAME_LAYER = 0,
	HUD_LAYER = 1,
	LAYER_COUNT
};

enum RenderModes
{
	RM_LINES = 0x001,
	RM_DOTS = 0x010
};

static struct fxVec2 camera_pos;
static s32 gameState = gstNONE;
static s32 renderMode = gstNONE;
static s32 last_fps = -1;
static s32 resizeW = 0;
static s32 resizeH = 0;
static TouchPadData tp_data;
static struct GUIContainer gsGUIContainer = {0};
static struct Verlet gsVerlet = {0};

//-------------------------------------------------------------------------------------------

static void onEngineEvent(const struct EventCallbackData *pData);

static void onDrawObject(u32 layer, s32 iId, BOOL* const oDraw);
static void onBeginUpdate(u32 layer);
static void onVrltRenderCallback(const struct VTCallbackData *iData);
static void onVrltSelectedObjectCallback(const struct VTSelectedObjectCallbackData *iData);

static void onGUIEvent(const struct GUIEvent* event);

static void releaseTengineData(void);

static void load(void);

static void doResize(void);

static void onLevelMapLoad(void);
static void onLevelHudLoad(void);

static s32 spiderweb(struct fxVec2 origin, fx32 radius, s32 segments, s32 depth);

//-------------------------------------------------------------------------------------------

void tfgInitMemory()
{
	InitMemoryAllocator();
}
//-------------------------------------------------------------------------------------------

void tfgInit()
{
    InitGamePad();
	InitTouchPad();
	gameState = gstNONE;
}
//-------------------------------------------------------------------------------------------

void tfgRelease()
{
	releaseTengineData();
}
//-------------------------------------------------------------------------------------------

void releaseTengineData()
{
	// to avoid memory fragmentation, releaseMapData in opposite side
	releaseResources();
	GUIFactory_Reset();
	GUIContainer_FreeAll(&gsGUIContainer);	
	vrltRelease(&gsVerlet);
	releaseMapData(HUD_LAYER);
	releaseMapData(GAME_LAYER);
	releaseTextData();
	releaseCommonData();
	releaseRenderPlane(BGSELECT_SUB3);
	releaseEngine();
}
//-------------------------------------------------------------------------------------------

void tfgSetWindowModeSize(s32 *const w, s32 *const h)
{
	*w = 800;
	*h = 600;
}
//-------------------------------------------------------------------------------------------

void tfgResize(s32 w, s32 h)
{
	gameState = gstDISPLAYINIT;
	resizeW = w;
	resizeH = h;
}
//-------------------------------------------------------------------------------------------

void doResize()
{
	struct RenderPlaneInitParams mp1;
	mp1.mBGType = BGSELECT_SUB3;
	mp1.mSizes.mViewWidth = resizeW; 
	mp1.mSizes.mViewHeight = resizeH;
	mp1.mX = 0;
	mp1.mY = 0;
#ifdef USE_OPENGL_RENDER
	mp1.mMaxRenderObjectsOnPlane = 512;
#endif
#ifdef USE_CUSTOM_RENDER
	mp1.mColorType = BMP_TYPE_DC16;
	{
		const s32 tile_size = 32; // Tile size is 32x32 (see Mapeditor)
		const s32 h8 = ((resizeH + (tile_size - 1)) / tile_size) * tile_size;
		const s32 w8 = ((resizeW + (tile_size - 1)) / tile_size) * tile_size;
		mp1.mSizes.mFrameBufferWidth8 = w8;
		mp1.mSizes.mFrameBufferHeight8 = h8;
	}
#endif
	
	if(!isRenderPlaneInit(GAME_LAYER)) // any active plane, whatever
	{
		struct InitEngineParameters initparams;
		releaseTengineData();
		
		initparams.layersCount = LAYER_COUNT;
		initparams.particlesPoolSize = 0;

		initEngine(&initparams);
		initRenderPlane(&mp1);
		
		assignLayerWithRenderPlane(GAME_LAYER, BGSELECT_SUB3, TRUE);
		assignLayerWithRenderPlane(HUD_LAYER, BGSELECT_SUB3, FALSE);

        //register callbacks
		setOnEvent(onEngineEvent);
		setOnBeginUpdate(onBeginUpdate);
		setOnDrawGameObject(onDrawObject);

		//begin load task
		//parameter will return with iData.initiatorId in onEngineEvent() function ( EVENT_TYPE_ON_END_ASYNH_LOAD_DATA)
		beginLoadListAsynh(LM_INIT);
		{
			//load common data
			addToLoadListCommonData();
			//load text data for certain language
			// to avoid memory fragmentation load it once after common data
			addToLoadListLanguageData(LANGUAGE_ENG);
		}
		//end load task
		endLoadListAsynh(FALSE);
	
		camera_pos.x = 0;
		camera_pos.y = 0;
		renderMode = RM_LINES | RM_DOTS;

		//wait for  EVENT_TYPE_ON_END_ASYNH_LOAD_DATA in onEngineEvent() function
		gameState = gstNONE;
	}
	else
	{
		resizeRenderPlane(GAME_LAYER, &mp1.mSizes);
		gameState = gstGAME;
	}
}
//-------------------------------------------------------------------------------------------

void load()
{
#ifdef NITRO_SDK
    GX_SetVisiblePlane(GX_PLANEMASK_NONE);
#endif
	
	// to avoid memory fragmentation, releaseMapData in opposite side

	releaseResources();
	GUIFactory_Reset();
	GUIContainer_FreeAll(&gsGUIContainer);	
	vrltRelease(&gsVerlet);
	releaseMapData(HUD_LAYER);
	releaseMapData(GAME_LAYER);

	OS_Printf("mem on newGame = %u\n", GetFreeMemorySize());

	beginLoadListAsynh(LM_LOAD);
	{
		//layer LAYER_GAME, load current level
		addToLoadListMap(GAME_LAYER, 0);
		//layer LAYER_HUD, load or reload hud
		addToLoadListMap(HUD_LAYER, 1);
	}
	endLoadListAsynh(TRUE);
	
	//wait for  EVENT_TYPE_ON_END_ASYNH_LOAD_DATA and EVENT_TYPE_ON_END_ASYNH_LOAD_RESOURSES in onEngineEvent() function
	gameState = gstNONE;

	last_fps = -1;
}
//-------------------------------------------------------------------------------------------

// file index 1, layer GAME_LAYER
void onLevelMapLoad()
{
	// only on onLevelMapLoad (called from onEngineEvent function)
	// you can work with M0_* objects, because current active layer is GAME_LAYER 
	//pos = prGetPosition(M0_SPIDER_BODY_4); // constants.h (gererated by MapEditor3)
	camera_pos.x = 0;//pos.x;
	camera_pos.y = 0;//pos.y;

	{
		struct VerletInitParams vp;
		struct VerletObjectInitParams carr[2];
		
		//spiderweb
		carr[0].mConstraintsMax = 284;
		carr[0].mParticlesMax = 140;
		carr[0].mFriction = FX32(0.99f);
		carr[0].mGravity = fxVec2Create(FX32(0), FX32(0.2f));
		carr[0].mGroundFriction = FX32(0.8f);

		carr[1].mConstraintsMax = 7 * 3;
		carr[1].mParticlesMax = 7 + 1;
		carr[1].mFriction = FX32(1.0f);
		carr[1].mGravity = fxVec2Create(FX32(0), FX32(0.2f));
		carr[1].mGroundFriction = FX32(0.8f);

		vp.mObjectsCount = 2;
		vp.mConstraintsPoolMax = 1428;
		vp.mParticlesPoolMax = 1448;
		vp.mHeight = (u32)resizeH;
		vp.mWidth = (u32)resizeW;
		vp.mppObjectInitArr = carr;

		vrltInit(&gsVerlet, &vp);

		{
			s32 size = (resizeW > resizeH ? resizeH : resizeW) / 2;
			/*s32 spiderweb_id =*/ spiderweb(fxVec2Create(FX32(resizeW / 2), FX32(resizeH / 2)), FX32(size), 20, 7);
		}
	}

	if((renderMode & RM_DOTS) != RM_DOTS)
	{
		s32 i = M0_POOL_OBJECT_NODE_0_POOL_FIRST;
		for(; i < M0_POOL_OBJECT_NODE_0_POOL_COUNT; i++)
		{
			prSetCustomAlpha(i, 0);
		}
	}
}
//-------------------------------------------------------------------------------------------

s32 spiderweb(struct fxVec2 origin, fx32 radius, s32 segments, s32 depth)
{
	s32 i, obj_id, map_obj_pool_id;
	fx32 distance;
	const struct fxVec2 *pos1;
	const struct fxVec2 *pos2;
	const fx32 stiffness = FX32(0.6f);
	const fx32 tensor = FX32(0.3f);
	const fx32 stride = (2 * FX_PI) / segments;
	const s32 n = segments * depth;
	const fx32 radiusStride = radius / n;

	obj_id = vrltBeginInitObject(&gsVerlet);
	SDK_ASSERT(obj_id >= 0);

	map_obj_pool_id = M0_POOL_OBJECT_NODE_0_POOL_FIRST;

	for (i = 0; i < n ; i++)
	{
		struct fxVec2 pos;
		fx32 theta, shrinkingRadius, offy;
		theta = i * stride + fxMul(fxCos(i * FX32(0.4f)), FX32(0.05f)) + fxMul(fxCos(i * FX32(0.05f)), FX32(0.2f));
		shrinkingRadius = radius - radiusStride * i + fxCos(i * FX32(0.1f)) * 20;
		offy = fxMul(fxCos(fxMul(theta, FX32(2.1f))), fxMul((radius / depth), FX32(0.2f)));
		pos.x = origin.x + fxMul(fxCos(theta), shrinkingRadius);
		pos.y = origin.y + fxMul(fxSin(theta), shrinkingRadius) + offy;
		vrltInitObject_AddParticle(&gsVerlet, obj_id, pos, &map_obj_pool_id);
		prSetProperty(map_obj_pool_id, PRP_ENABLE, 1);
		map_obj_pool_id++;
		SDK_ASSERT(map_obj_pool_id < M0_POOL_OBJECT_NODE_0_POOL_COUNT);
	}

	for (i = 0; i < segments; i += 4)
	{
		vrltInitObject_AddPinConstraint(&gsVerlet, obj_id, i, NULL);
	}

	for (i = 0; i< n - 1; i++)
	{
		s32 off;
		// neighbor
		pos1 = vrltGetObjectParticlePosition(&gsVerlet, obj_id, i);
		pos2 = vrltGetObjectParticlePosition(&gsVerlet, obj_id, i + 1);
		distance = fxMul(fxVec2Length(fxVec2Sub(*pos1, *pos2)), tensor);
		vrltInitObject_AddDistanceConstraint(&gsVerlet, obj_id, i, i + 1, stiffness, &distance);
		// span rings
		off = i + segments;
		if (off < n - 1)
		{
			pos1 = vrltGetObjectParticlePosition(&gsVerlet, obj_id, i);
			pos2 = vrltGetObjectParticlePosition(&gsVerlet, obj_id, off);
			distance = fxMul(fxVec2Length(fxVec2Sub(*pos1, *pos2)), tensor);
			vrltInitObject_AddDistanceConstraint(&gsVerlet, obj_id, i, off, stiffness, &distance);
		}
		else
		{
			pos1 = vrltGetObjectParticlePosition(&gsVerlet, obj_id, i);
			pos2 = vrltGetObjectParticlePosition(&gsVerlet, obj_id, n - 1);
			distance = fxMul(fxVec2Length(fxVec2Sub(*pos1, *pos2)), tensor);
			vrltInitObject_AddDistanceConstraint(&gsVerlet, obj_id, i, n - 1, stiffness, &distance);
		}
	}

	pos1 = vrltGetObjectParticlePosition(&gsVerlet, obj_id, 0);
	pos2 = vrltGetObjectParticlePosition(&gsVerlet, obj_id, segments - 1);
	distance = fxMul(fxVec2Length(fxVec2Sub(*pos1, *pos2)), tensor);
	vrltInitObject_AddDistanceConstraint(&gsVerlet, obj_id, 0, segments - 1, stiffness, &distance);
	
	vrltEndInitObject(&gsVerlet, obj_id);
	return obj_id;
}
//-------------------------------------------------------------------------------------------

void onLevelHudLoad()
{
	struct GUIButtonInitParameters bp;
	bp.baseInitParameters.enabledPrpId = PRP_ENABLE;
	// id's from constants.h, "Game object types" section
	bp.baseInitParameters.mapObjTypeID[BUTTON_MAPOBJ_ID_IDLE] = BUTTON_IDLE;
	bp.baseInitParameters.mapObjTypeID[BUTTON_MAPOBJ_ID_PRESSED] = BUTTON_PRESSED;
	bp.baseInitParameters.mapObjTypeID[BUTTON_MAPOBJ_ID_FOCUSED] = -1;
	bp.baseInitParameters.mapObjTypeID[BUTTON_MAPOBJ_ID_TEXT] = -1;
	bp.tabOrderPrpId = -1;

	GUIContainer_Init(&gsGUIContainer, HUD_LAYER, 2); // 2 - maximum gui objects
	GUIContainer_SetGUIEventHandler(&gsGUIContainer, onGUIEvent); // OnGuiEvent() registration

	GUIFactory_Init(&gsGUIContainer);
	GUIFactory_AddGUIButtonType(&bp); // register button type

	// parsing array with id's and create gui objects
	// based on registred types
	GUIFactory_ParseMapObjects();
}
//------------------------------------------------------------------------------------

void tfgLostRenderDevice()
{
}
//-------------------------------------------------------------------------------------------

void tfgRestoreRenderDevice()
{
}
//------------------------------------------------------------------------------------

void tfgLowMemory()
{
}
//------------------------------------------------------------------------------------

void tfgTick(s32 ms)
{
	UpdateGamePad();
    if(IsButtonDown(TYPE_START))
    {
		gameState = gstLOADING;
    }

	switch(gameState)
	{
		case gstNONE:
		break;

		case gstDISPLAYINIT:
			doResize();
		break;

		case gstLOADING:
			load();
		break;

		case gstGAME:

			ReadTouchPadData(&tp_data);
			if(tp_data.point[0].mTrg)
			{
				struct fxVec2 pos;
				pos.x = FX32(tp_data.point[0].mX);
				pos.y = FX32(tp_data.point[0].mY);
				vrltSelectNearestObject(&gsVerlet, &pos, FX32(5));
			}
			else if(tp_data.point[0].mRls)
			{
				vrltClearSelectObject(&gsVerlet);
			}

			vrltUpdate(&gsVerlet, ms, onVrltSelectedObjectCallback);

			GUIContainer_ProcessInput(&gsGUIContainer, &tp_data);	
		break;

		default:
		break;
	}
}
//-------------------------------------------------------------------------------------------

void onGUIEvent(const struct GUIEvent* event)
{
	if(GUIObject2D_GetType(event->mpSender) == GUI_TYPE_BUTTON && event->mState == ST_CLICK)
	{
		const struct GUIButton* b = (struct GUIButton*)event->mpSender;
		switch(GUIButton_GetMapObjId(b))
		{
			case M1_BUTTON_RESET:
				gameState = gstLOADING;
			break;
			case M1_BUTTON_MODE:
				if((renderMode & RM_LINES) == RM_LINES && (renderMode & RM_DOTS) == RM_DOTS)
				{
					renderMode = RM_LINES;
				}
				else if((renderMode & RM_LINES) == RM_LINES)
				{
					renderMode = RM_DOTS;
				}
				else
				{
					renderMode = RM_LINES | RM_DOTS;
				}
			break;
			default:
			break;
		}
	}
}
//-------------------------------------------------------------------------------------------

void onBeginUpdate(u32 layer)
{
	switch(layer)
	{
		case GAME_LAYER:
			setCamera(camera_pos);
		default:
		break;
	}
}
//----------------------------------------------------------------------------------

void onVrltRenderCallback(const struct VTCallbackData *iData)
{
	SDK_NULL_ASSERT(iData);
	switch(iData->mType)
	{
		case VTRT_PARTICLE:
			if((renderMode & RM_DOTS) == RM_DOTS)
			{
				prSetPosition(*iData->mpData, *iData->mpPoint[0]); 
			}
		break;
		case VTRT_CONSTRAINT:
			if(iData->mPointCount == 2 && (renderMode & RM_LINES) == RM_LINES)
			{
				fx32 x1, y1, x2, y2;
				setColor(GX_RGBA(0,0,31,1));
				x1 = iData->mpPoint[0]->x; 
				y1 = iData->mpPoint[0]->y;
				x2 = iData->mpPoint[1]->x; 
				y2 = iData->mpPoint[1]->y;
				drawLine(x1, y1, x2, y2);
			}
		default:
		break;
	}
}
//----------------------------------------------------------------------------------

void onVrltSelectedObjectCallback(const struct VTSelectedObjectCallbackData *iData)
{
	SDK_NULL_ASSERT(iData);
	iData->ioPos->x = FX32(tp_data.point[0].mX);
	iData->ioPos->y = FX32(tp_data.point[0].mY);
}
//----------------------------------------------------------------------------------

void onDrawObject(u32 layer, s32 iId, BOOL* const oDraw)
{
	(void)oDraw;
	switch(layer)
	{
		case GAME_LAYER:
			if(iId >= M0_POOL_OBJECT_NODE_0_POOL_FIRST &&
				iId < M0_POOL_OBJECT_NODE_0_POOL_COUNT)
			{
				if((renderMode & RM_DOTS) != RM_DOTS)
				{
					if(prGetProperty(iId, PRP_SPEED) == 0)
					{
						prSetProperty(iId, PRP_SPEED, FX32(15 + (mthGetRandom(35))));
					}
					u8 alpha = prGetCustomAlpha(iId);
					if(alpha > 0)
					{
						const fx32 dt = getCurrentDeltaTimeScale(); 
						struct fxVec2 pos = prGetPosition(iId);
						pos.y += FX_Mul(prGetProperty(iId, PRP_SPEED), dt);
						prSetPosition(iId, pos);
						alpha--; 
						prSetCustomAlpha(iId, alpha);
					}
				}
				else
				{
					if(prGetProperty(iId, PRP_SPEED) != 0)
					{
						prSetProperty(iId, PRP_SPEED, 0);
						prResetAlphaToDefault(iId);
					}
				}
			}

			if(iId == M0_SPIDER_BODY_4)
			{
				vrltRender(&gsVerlet, 0, onVrltRenderCallback);
			}
		break;
		
		case HUD_LAYER:
		{
			switch(iId)
			{
				case M1_TXTBOX_FPS:
				if(last_fps != getFPS())
				{
					wchar aaa[16];
					wchar bbb[16];
					bbb[0] = L'f';
					bbb[1] = L'p';
					bbb[2] = L's';
					bbb[3] = L'=';
					bbb[4] = L'%';
					bbb[5] = L'd';
					bbb[6] = L'\0';
					last_fps = getFPS();
					STD_WSnprintf(aaa, 16, bbb, last_fps);
					txbSetDynamicTextLine(M1_TXTBOX_FPS, 0, aaa);
				}		
				default:
				break;
			}
		}
		default:
		break;
	}
}
//----------------------------------------------------------------------------------

void onEngineEvent(const struct EventCallbackData *pData)
{
	switch(pData->eventType)
	{
		case  EVENT_TYPE_ON_END_ASYNH_LOAD_DATA:
			switch(pData->eventId)
			{
				case LOAD_TYPE_ENDLOADDATATASK:
					if(pData->initiatorId == LM_INIT)
					{
						gameState = gstLOADING;
					}
				break;
				case LOAD_TYPE_MAPDATA:
					if(pData->initiatorId == LM_LOAD)
					{
						switch(pData->layer)
						{
							case HUD_LAYER:
								onLevelHudLoad();
							break;
							case GAME_LAYER:
								onLevelMapLoad();
							default:
							break;
						}
					}
				default:
				break;
			}
		break;
		
		case EVENT_TYPE_ON_END_ASYNH_LOAD_RESOURSES:
			setVisible(HUD_LAYER, TRUE);
			setVisible(GAME_LAYER, TRUE);
			gameState = gstGAME;
		break;

		default:
		break;
	}
}
//----------------------------------------------------------------------------------
