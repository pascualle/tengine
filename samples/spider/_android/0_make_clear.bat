@echo off
set ANDROID_NDK=%ANDROID_NDK%
if "%ANDROID_NDK%"=="" (
echo ----
echo Please set ANDROID_NDK variable
echo ----
goto END
)
set CURRENT_DIR=%~dp0
set NDK_ROOT=%ANDROID_NDK%
cd %CURRENT_DIR%
@echo on

call %NDK_ROOT%\ndk-build clean
:END