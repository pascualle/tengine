Сборка приложения для android:
------------------------------

make_app_debug.bat
скрипт запускает полную сборку, результат *-debug.apk и *-debug-unaligned.apk помещается в bin папке  

make_app_debug.bat install
скрипт запускает полную сборку и пытается установить приложение на подключенное устройство


Минимальная версия platform sdk:
--------------------------------

Так как NDK больше не поддерживает платформы ниже 14 версии, все утилиты настроены на минимальную 14 версию
Убедитесь, что 14 платформа установлена (android_sdk/SDK Manager.exe) 

Логирование:
------------

функция OS_Print() пишет в лог все с префиксом TengineAppInfo
функция OS_Warning() пишет в лог все с префиксом TengineAppWarning

пример logcat командной строки:

adb logcat -s tengine TengineAppInfo TengineAppWarning ActivityManager PackageManager dalvikvm DEBUG





----------------
Структура папок:
----------------

Папка assets/data
-----------------
в этой папке хранятся сгенерированные ресурсы (смотрите файл 2_make_res.bat )

Папка jni
---------
содержит два файла:

Android.mk
файл с синтаксисом make-файла, здесь указывается как компилить с/c++ файлы проекта, какие сторонние библиотеки линковать и тд.
замечание:
переменная LOCAL_MODULE должна называться tengine, это имя библиотеки ожидает java часть приложения
LOCAL_MODULE := tengine

Application.mk
содержит информацию о target-платформах и минимальной версии sdk
пример:
APP_ABI := armeabi armeabi-v7a mips x86 
означает указание компилятору, что нужно собрать библиотеку (tengine) под четыре платформы: armeabi, armeabi-v7a, mips, x86
в apk будет помещено все четыре версии, в зависимости от платформы, ОС сама загрузит ту библиотеку, что ей подходит

Папка res
---------
содержит папки drawable-*, в которых можно положить иконки приложения под разные разрешения экрана
а также содержит в values\strings.xml, файл, в который прописывается название приложения, что будет показано пользователю:
<resources>
    <string name="app_name">SuperGame</string>
</resources> 

Папка src
---------
Должна содержать иерархию папок и java файл:
com\app\super_game\super_game.java

файл super_game.java должен содержать минимальный код:

package com.app.super_game;
import tengine.main.TengineApplication;

public class super_game extends TengineApplication
{		
}

Внимание: если основной класс приложения называется super_game, имя папки, имя java файла, имя package и имя класса должны иметь имя super_game

Файл AndroidManifest.xml
------------------------
основной manifest файл приложения, обязательные поля:
поле package="com.app.super_game" должно содержать package с правильным именем класса
поле android:name=".super_game" должно содержать правильное имя основного класса с точкой в начале строки
