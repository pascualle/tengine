@echo off
set ANDROID_NDK=%ANDROID_NDK%
if "%ANDROID_NDK%"=="" (
echo ----
echo Please set ANDROID_NDK variable
echo ----
goto END
)
set CURRENT_DIR=%~dp0
set NDK_ROOT=%ANDROID_NDK%
cd %CURRENT_DIR%

set FORCE_DEBUG_FLAG=
rem if "%DEBUG_VER%"=="1" (
rem set FORCE_DEBUG_FLAG=NDK_DEBUG=1
rem )
@echo on

call %NDK_ROOT%\ndk-build -C %CURRENT_DIR%\jni %FORCE_DEBUG_FLAG%
:END