@echo off

echo ------------------------------
echo Generate map (MapEditor) files
echo Please verify correct pathes in file ..\mapeditor\editor.ini
echo By default editor.ini contain paths from C disk
echo ------------------------------

set CURRENT_DIR=%~dp0
if EXIST %CURRENT_DIR%\data\o del %CURRENT_DIR%\data\o 
..\..\..\tools\editor\MapEditor3.exe -wchar_size2 -p%CURRENT_DIR%\..\mapeditor\Snuggles1.mpf -i%CURRENT_DIR%\..\mapeditor\ -o%CURRENT_DIR%\data\ -l%CURRENT_DIR%\log.txt 

echo MapEditor report %CURRENT_DIR%\log.txt
type %CURRENT_DIR%\log.txt

if NOT EXIST %CURRENT_DIR%\data\o (
echo .
echo ------------------------------
echo Error: generated files not found
echo Please check pathes in file ..\mapeditor\editor.ini
echo and generate map-files with MapEditor:
echo - open ..\mapeditor\*.mpf file with ..\..\..\tools\editor\MapEditor3.exe
echo - go to main menu [File/Save map] and browse to folder %CURRENT_DIR%\data
echo ------------------------------
pause
goto END
)

echo .
echo ------------------------------
echo convert graphics res 
echo ------------------------------

@echo on

rem for 32bpp color add to bmpcvtr.exe -8888 key

rem main data
for %%i in (..\rawres\*.png) do ..\..\..\tools\bmpcvtr.exe %%i -1555 -bgr -2n -o%CURRENT_DIR%/data

rem for file with objects create alpha collide map
..\..\..\tools\bmpcvtr.exe ..\rawres\FurSnuggleLevel.png -1555 -2n -acm4 -bgr -o%CURRENT_DIR%/data

rem index color files
..\..\..\tools\bmpcvtr.exe ..\rawres\button_pr.png -1555 -2n -bgr -o%CURRENT_DIR%/data
..\..\..\tools\bmpcvtr.exe ..\rawres\system_00.png -1555 -2n -bgr -o%CURRENT_DIR%/data

rem scene1 fur_snuggle_look_out animation 
for %%i in (..\rawres\fur_snuggle_look_out\*.png) do ..\..\..\tools\bmpcvtr.exe %%i -1555 -bgr -2n -o%CURRENT_DIR%/data/fur_snuggle_look_out

rem scene1 cactooInPot animation 
for %%i in (..\rawres\cactooInPot\*.png) do ..\..\..\tools\bmpcvtr.exe %%i -1555 -bgr -2n -o%CURRENT_DIR%/data/cactooInPot

@echo off
copy ..\rawres\arial.fnt %CURRENT_DIR%\data\ 

copy ..\rawres\color.vsh %CURRENT_DIR%\data\
copy ..\rawres\lit.vsh %CURRENT_DIR%\data\
copy ..\rawres\unlit.vsh %CURRENT_DIR%\data\
copy ..\rawres\color.fsh %CURRENT_DIR%\data\
copy ..\rawres\lit.fsh %CURRENT_DIR%\data\
copy ..\rawres\unlit.fsh %CURRENT_DIR%\data\

if NOT EXIST %CURRENT_DIR%\data\color.vsh (
echo .
echo ------------------------------
echo Please copy *.fsh and *.vsh files from tengine\src\_default_shaders to ..\rawres
echo ------------------------------
pause
goto END
)

:END
