#ifndef _GAMELEVEL_HUD_H_
#define _GAMELEVEL_HUD_H_

#include "gamelevel.h"

class GameFieldLevelHud : public GameFieldLevel
{
 public:

	GameFieldLevelHud();
	virtual ~GameFieldLevelHud(){};
	 
	virtual void init(u32 layerIdx);
	virtual void release();

	virtual void afterDataLoad(s32 level);
	virtual void gameStep(s32 ms);
	virtual void onDrawObject(s32 iId, BOOL* const oDraw);
 
 private:

	struct GUIContainer mGUIContainer;
};

#endif //_GAMELEVEL_HUD_H_
