
#include "tengine.h"
#include "touchpad.h"
#include "gamepad.h"
#include "gamefield.h"
#include "gamelevel.h"

#include "constants.h"
#include "level_id.h"

#include "gamelevelLoading.h"
#include "gamelevelHud.h"
#include "gamelevelScene1.h"
#include "gamelevelScene2.h"

#include "gui.h"

#ifdef NITRO_SDK
#include "sound.sadl"
#endif

//-------------------------------------------------------------------------------------------

enum GState
{
	GAMESTATE_NONE = 0,
	GAMESTATE_SYSTEMINIT,
	GAMESTATE_PREPARE_FOR_LOAD_LEVEL,
	GAMESTATE_LOAD_LEVEL,
	GAMESTATE_GAME
};

enum LoadTag
{
	LOADING_TAG_COMMON = 0,
	LOADING_TAG_LEVELS
};

enum
{
	LAYER_GAME = 0,
	LAYER_HUD,
	LAYER_LOADING,	
	LAYER_COUNT
};

static const s32 DISPLAY_WIDTH = 1024; 
static const s32 DISPLAY_HEIGHT = 768;

static enum GState gsiGameState = GAMESTATE_NONE;
static GameFieldLevel *gsLevel[LAYER_COUNT];
static s32 gsCurrentLevel = LEVEL_IDX_SCENE1;

struct TouchPadData gTPData;

//-------------------------------------------------------------------------------------------

static void gameStep(s32 ms);
static void doResize(void);
static void load(void);
static void onDrawObject(u32 layer, s32 iId, BOOL* const oDraw);
static void onEngineEvent(const struct EventCallbackData *pData);
static void onSkipStreamFrame(u32 layer, s32 id, BOOL * const skipFrame);

//-------------------------------------------------------------------------------------------

void tfgInitMemory()
{
	InitMemoryAllocator();
}
//-------------------------------------------------------------------------------------------

void tfgInit()
{
	gsLevel[LAYER_HUD] = NULL;
	gsLevel[LAYER_GAME] = NULL;
	gsLevel[LAYER_LOADING] = NULL;
	InitGamePad();
	InitTouchPad();
	gsiGameState = GAMESTATE_SYSTEMINIT;
}
//-------------------------------------------------------------------------------------------

void tfgRelease()
{
	// to avoid memory fragmentation, release all data in opposite side 
	releaseResources();
	if(gsLevel[LAYER_GAME] != NULL)
	{
		gsLevel[LAYER_HUD]->release();
		gsLevel[LAYER_GAME]->release();
		delete gsLevel[LAYER_HUD];
		delete gsLevel[LAYER_GAME];
	}
	if(gsLevel[LAYER_LOADING] != NULL)
	{
		gsLevel[LAYER_LOADING]->release();
	}
	gsLevel[LAYER_HUD] = NULL;
	gsLevel[LAYER_GAME] = NULL;
	releaseTextData();
	releaseCommonData();
	if(gsLevel[LAYER_LOADING] != NULL)
	{
		delete gsLevel[LAYER_LOADING];
	}
	gsLevel[LAYER_LOADING] = NULL;
	releaseRenderPlane(BGSELECT_SUB2);
	releaseRenderPlane(BGSELECT_SUB3);
	releaseEngine();
}
//-------------------------------------------------------------------------------------------

void tfgLostRenderDevice()
{
}
//-------------------------------------------------------------------------------------------

void tfgRestoreRenderDevice()
{
}
//------------------------------------------------------------------------------------

void tfgPause()
{
}
//------------------------------------------------------------------------------------

void tfgResume()
{
}
//------------------------------------------------------------------------------------

void tfgLowMemory()
{
}
//------------------------------------------------------------------------------------

void tfgSetWindowModeSize(s32 *const w, s32 *const h)
{
	*w = 1024;
	*h = 768;
}
//------------------------------------------------------------------------------------

void tfgResize(s32 w, s32 h)
{
	(void)w;
	(void)h;
	gsiGameState = GAMESTATE_SYSTEMINIT;
}
//-------------------------------------------------------------------------------------------

void doResize()
{
	if(!isRenderPlaneInit(LAYER_GAME))
	{
		// struct mp1: game and hud screens
		// struct mp2: loading screen
		struct RenderPlaneInitParams mp1;
		struct RenderPlaneInitParams mp2;
		mp1.mBGType = BGSELECT_SUB3;
		mp1.mSizes.mViewWidth = DISPLAY_WIDTH; 
		mp1.mSizes.mViewHeight = DISPLAY_HEIGHT;
#ifdef USE_OPENGL_RENDER
		// maximum render objects on screen
		mp1.mMaxRenderObjectsOnPlane = 1024;
#endif
#ifdef USE_CUSTOM_RENDER
		mp1.mColorType = BMP_TYPE_DC16;
#ifdef FRAME_ALLOCATOR
		// when the screen orientation changes, it is necessary to re-create a buffer (resize)
		// if defined FRAME_ALLOCATOR, we cannot recreate new buffer for esaygraphics module because of assertion)
		// so the buffer must be the same for the larger size of the screen
		mp1.mSizes.mFrameBufferWidth8 = DISPLAY_WIDTH;
		mp1.mSizes.mFrameBufferHeight8 = DISPLAY_WIDTH;
#else
		mp1.mSizes.mFrameBufferWidth8 = DISPLAY_WIDTH;
		mp1.mSizes.mFrameBufferHeight8 = DISPLAY_WIDTH;
#endif
#endif
		mp1.mX = 0;
		mp1.mY = 0;

		mp2 = mp1;
		mp2.mBGType = BGSELECT_SUB2;
#ifdef USE_OPENGL_RENDER
		// loading screen has only one object (see mapeditor)
		mp2.mMaxRenderObjectsOnPlane = 11; // sample of precise object calcualtion: "loading..." text (10 obj) + object under text
#endif
		tfgRelease();
		
		{
			struct InitEngineParameters initparams;
			initparams.layersCount = LAYER_COUNT;
			initparams.particlesPoolSize = 0; // no particles in this demo
			initEngine(&initparams);
		}
		initRenderPlane(&mp1);
		initRenderPlane(&mp2);
		// assign logical layers LAYER_HUD and LAYER_GAME to render plane BGSELECT_SUB3
		//  LAYER_HUD cannot use tilemap, even if he has it, the engine will ignore it (primaryLayer = FALSE)
		assignLayerWithRenderPlane(LAYER_HUD, BGSELECT_SUB3, FALSE);
		//  LAYER_GAME is primary
		assignLayerWithRenderPlane(LAYER_GAME, BGSELECT_SUB3, TRUE);
		// assign logical layer LAYER_LOADING to separate render plane BGSELECT_SUB2,
		// because LAYER_LOADING uses a tilemap. Only the primary layer can use tilemap.
		assignLayerWithRenderPlane(LAYER_LOADING, BGSELECT_SUB2, TRUE);

		// register callbacks
		setOnEvent(onEngineEvent);
		setOnDrawGameObject(onDrawObject);
		setOnSkipStreamFrame(onSkipStreamFrame);

		// by default all layers are visible, hide them
		setVisible(LAYER_HUD, FALSE);
		setVisible(LAYER_GAME, FALSE);
		setVisible(LAYER_LOADING, FALSE);

		gsLevel[LAYER_HUD] = NULL;
		gsLevel[LAYER_GAME] = NULL;
		gsLevel[LAYER_LOADING] = new GameFieldLevelLoading();
		gsLevel[LAYER_LOADING]->init(LAYER_LOADING);

		//begin load task
		//parameter will return with iData.initiatorId in onEngineEvent() function (EVENT_TYPE_ON_ENDASYNHLOADDATA)
		beginLoadListAsynh(LOADING_TAG_COMMON);
		{
			//load common data
			addToLoadListCommonData();
			//load text data for certain language
			// to avoid memory fragmentation load it once after common data
			addToLoadListLanguageData(LANGUAGE_DEFAULT);

			addToLoadListMap(LAYER_LOADING, (u16)gsLevel[LAYER_LOADING]->getLevelFileIdx());
		}
		//end load task
		endLoadListAsynh(FALSE);

		//wait for EVENT_TYPE_ON_ENDASYNHLOADDATA in onEngineEvent() function
		gsiGameState = GAMESTATE_NONE;
	}
	else
	{
		gsiGameState = GAMESTATE_GAME;
	}
}
//-------------------------------------------------------------------------------------------

void load()
{
	// to avoid memory fragmentation, releaseMapData in opposite side

	releaseResources();

	// show loading screen, to avoid flickering do it after releaseResources() 
	gsLevel[LAYER_LOADING]->setVisible(TRUE);
	if(gsLevel[LAYER_GAME] != NULL)
	{
		// hide another screens
		gsLevel[LAYER_GAME]->setVisible(FALSE);
		gsLevel[LAYER_HUD]->setVisible(FALSE);
	}	

	if(gsLevel[LAYER_GAME] != NULL)
	{
		gsLevel[LAYER_HUD]->release();
		gsLevel[LAYER_GAME]->release();
		delete gsLevel[LAYER_HUD];
		delete gsLevel[LAYER_GAME];
	}

	switch(gsCurrentLevel)
	{
		case LEVEL_IDX_SCENE1:
			gsLevel[LAYER_GAME] = new GameFieldLevelScene1();
		break;
		case LEVEL_IDX_SCENE2:
			gsLevel[LAYER_GAME] = new GameFieldLevelScene2();
	}
	gsLevel[LAYER_HUD] = new GameFieldLevelHud();
	gsLevel[LAYER_GAME]->init(LAYER_GAME);
	gsLevel[LAYER_HUD]->init(LAYER_HUD);

	OS_Printf("mem on begin load = %u\n", GetFreeMemorySize());

	beginLoadListAsynh(LOADING_TAG_LEVELS);
	{
		//layer LAYER_GAME, load current level
		addToLoadListMap(LAYER_GAME, (u16)gsLevel[LAYER_GAME]->getLevelFileIdx());
		//layer LAYER_HUD, load or reload hud
		addToLoadListMap(LAYER_HUD, (u16)gsLevel[LAYER_HUD]->getLevelFileIdx());
	}
	endLoadListAsynh(TRUE);
	
	//wait for EVENT_TYPE_ON_ENDASYNHLOADDATA and EVENT_TYPE_ON_ENDASYNHLOADGRAPHICS in onEngineEvent() function
    gsiGameState = GAMESTATE_NONE; 
}
//------------------------------------------------------------------------------------

void tfgTick(s32 ms)
{
	UpdateGamePad();
	ReadTouchPadData(&gTPData);

	switch(gsiGameState)
	{
		case GAMESTATE_NONE:
		return;
	
		case GAMESTATE_SYSTEMINIT:
			doResize();
		break;

		case GAMESTATE_PREPARE_FOR_LOAD_LEVEL:
			// prevent flickering, draw few frames without internal logic update
			drawWithoutInternalLogicUpdateMode();
			gsiGameState = GAMESTATE_LOAD_LEVEL;
		break;

		case GAMESTATE_LOAD_LEVEL:
			load();
		break;

		case GAMESTATE_GAME:
			gameStep(ms);
	}
}
//-------------------------------------------------------------------------------------------

void onGUIEvent(const GUIEvent* event)
{
	if(GUIObject2D_GetType(event->mpSender) == GUI_TYPE_BUTTON)
	{
		if(event->mState == ST_CLICK)
		{
			//const gui::GUIButton* b = static_cast<const gui::GUIButton*>(event->mpSender);
			switch(gsCurrentLevel)
			{
				case LEVEL_IDX_SCENE1:
					gsCurrentLevel = LEVEL_IDX_SCENE2;
				break;
				case LEVEL_IDX_SCENE2:
					gsCurrentLevel = LEVEL_IDX_SCENE1;
			}
			gsiGameState = GAMESTATE_PREPARE_FOR_LOAD_LEVEL; 
			return;
		}
	}
	gsLevel[LAYER_HUD]->OnGUIEvent(event);
}
//----------------------------------------------------------------------------------

void onDrawObject(u32 layer, s32 iId, BOOL* const oDraw)
{
	switch(layer)
	{
		case LAYER_GAME:
			gsLevel[LAYER_GAME]->onDrawObject(iId, oDraw);
			break;
		case LAYER_HUD:
			gsLevel[LAYER_HUD]->onDrawObject(iId, oDraw);
			break;
		case LAYER_LOADING:
		default:
			break;
	}
}
//----------------------------------------------------------------------------------

void gameStep(s32 ms)
{
	gsLevel[LAYER_GAME]->gameStep(ms);
	gsLevel[LAYER_HUD]->gameStep(ms);
}
//----------------------------------------------------------------------------------

//CALLBACK FUNCTIONS

//----------------------------------------------------------------------------------

void onEngineEvent(const struct EventCallbackData *pData)
{
	switch(pData->eventType)
	{
		case EVENT_TYPE_ON_ANIMATION:
			if(gsLevel[LAYER_GAME] != NULL && pData->layer == LAYER_GAME)
			{
				gsLevel[LAYER_GAME]->onEngineEvent(pData);	
			}
		break;

		case EVENT_TYPE_ON_END_ASYNH_LOAD_DATA:
			switch(pData->eventId)
			{
				case LOAD_TYPE_ENDLOADDATATASK:
					//load data task completed (but the resources are not loaded yet)
					if(pData->initiatorId == LOADING_TAG_COMMON)
					{
						gsiGameState = GAMESTATE_PREPARE_FOR_LOAD_LEVEL;
					}
				break;
				case LOAD_TYPE_MAPDATA:
					if(pData->initiatorId == LOADING_TAG_LEVELS)
					{
						switch(pData->layer)
						{
							case LAYER_HUD:
								gsLevel[LAYER_HUD]->afterDataLoad(gsCurrentLevel);
							break;
							case LAYER_GAME:
								gsLevel[LAYER_GAME]->afterDataLoad(gsCurrentLevel);
						}
					}
				break;
				case LOAD_TYPE_COMMONDATA:
					OS_Printf("common data loaded \n");
				break;
				case LOAD_TYPE_TEXTDATA:
					OS_Printf("text data loaded \n");
				break;
				default:
				break;
			}
		break;
		
		case EVENT_TYPE_ON_END_ASYNH_LOAD_RESOURSES:
			if(gsLevel[LAYER_HUD] != NULL)
			{
				gsLevel[LAYER_LOADING]->setVisible(FALSE);
				gsLevel[LAYER_HUD]->setVisible(TRUE);
				gsLevel[LAYER_GAME]->setVisible(TRUE);
				gsiGameState = GAMESTATE_GAME;
			}
		break;
		default:
		break;
	}
}
//----------------------------------------------------------------------------------

void onSkipStreamFrame(u32 layer, s32 id, BOOL* const skipFrame)
{
#ifndef SDK_DEBUG
	(void)layer;
	(void)id;
#endif
	// skipFrame = TRUE by default
	// set skipFrame to FALSE triggers force wait for stream frame loading
	// or remain skipFrame with TRUE and draw something like sandglasses
 	OS_Warning("warning: layer = %d, object = %d, waiting while stream frame loading...\n", layer, id);
	*skipFrame = FALSE;
}
//----------------------------------------------------------------------------------
