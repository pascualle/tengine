
#include "gamelevelScene2.h" 
#include "constants.h"
#include "level_id.h"

void GameFieldLevelScene2::init(u32 layerIdx)
{
	GameFieldLevel::init(layerIdx);
	mLevelFileIdx = LEVEL_IDX_SCENE2;
}
//------------------------------------------------------------------------------------

void GameFieldLevelScene2::release()
{
	GameFieldLevel::release();
}
//------------------------------------------------------------------------------------
