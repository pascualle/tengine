﻿Сборка:
ресурсы используются из корневой папки ./assets, собираются утилитой make_res.bat

Запуск с командной строки (или command arguments если запускать под debug в ide)
win32app.exe [-w] [-h] [-p]
-w ширина окна (по умолчанию 512)
-h ширина окна (по умолчанию 512)
-p путь к ресурсам (если -p не задан, ресурсы ожидаются в assets\data в папке с win32app.exe)
пример: win32app.exe -w512 -h512 -p../../assets/data/

Cпециальные клавиши:
F11 - lost/restore render context
F9  - portrait/landscape views (resize)
F7  - low memory signal
Esc - exit