@echo off

set EMSCRIPTEN_HOME=%EMSCRIPTEN_HOME%
if "%EMSCRIPTEN_HOME%"=="" (
echo ------------------------------
echo Please set EMSCRIPTEN_HOME variable
echo ------------------------------
goto END
)

set CURRENT_SESSION_EMSDK_ENV=%CURRENT_SESSION_EMSDK_ENV%
if "%CURRENT_SESSION_EMSDK_ENV%"=="INIT" (
goto SKIP_EMSDK_ENV
)
set CURRENT_SESSION_EMSDK_ENV=INIT
set PATH=%PATH%;%EMSCRIPTEN_HOME%
call %EMSCRIPTEN_HOME%\emsdk activate
set EMSCRIPTEN=%EMSDK%
if "%EMSCRIPTEN%"=="" (
echo ------------------------------
echo Something wrong with emsdk activate
echo ------------------------------
pause
goto END
)
:SKIP_EMSDK_ENV

emcc --clear-cache

:END