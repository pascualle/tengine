@echo off

rem -----
rem Ant APK build
rem based on http://developer.android.com/tools/building/building-cmdline.html
rem make sure you have android-10 api
rem -----

set JAVA_HOME=%JAVA_HOME%
if "%JAVA_HOME%"=="" (
echo ----
echo Please set JAVA_HOME variable
echo ----
goto END
)
echo JAVA_HOME=%JAVA_HOME%

set ANDROID_HOME=%ANDROID_HOME%
if "%ANDROID_HOME%"=="" (
echo ----
echo Please set ANDROID_HOME variable
echo ----
goto END
)

set PATH=%PATH%;%JAVA_HOME%\bin;%ANDROID_HOME%\tools;%ANDROID_HOME%\platform-tools;%ANDROID_HOME%\bin;..\..\..\tools\ant\bin

for /D %%p in ("bin\*.*") do rmdir "%%p" /s /q
del bin\*.* /q
rem call ant.bat clean

set BUILD_FLAG=debug
if "%DEBUG_VER%" NEQ "" (
 if "%DEBUG_VER%" NEQ "1" (
  set BUILD_FLAG=release
  )
)
echo BUILD_FLAG=%BUILD_FLAG%
@echo on

call android update project --path . --subprojects --target android-14
if "install"=="%1" (
call ant %BUILD_FLAG% install
) else (
call ant %BUILD_FLAG%
)

:END