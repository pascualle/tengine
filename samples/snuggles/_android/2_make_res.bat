@echo off

echo ------------------------------
echo Generate map (MapEditor) files
echo Please verify correct pathes in file ..\mapeditor\editor.ini
echo By default editor.ini contain paths from C disk
echo ------------------------------

set CURRENT_DIR=%~dp0
if EXIST %CURRENT_DIR%\assets\data\o del %CURRENT_DIR%\assets\data\o

rem wchar size for android 4
..\..\..\tools\editor\MapEditor3.exe -wchar_size4 -p%CURRENT_DIR%\..\mapeditor\Snuggles1.mpf -i%CURRENT_DIR%\..\mapeditor\ -o%CURRENT_DIR%\assets\data\ -l%CURRENT_DIR%\log.txt 

echo MapEditor report %CURRENT_DIR%\log.txt
type %CURRENT_DIR%\log.txt

if NOT EXIST %CURRENT_DIR%\assets\data\o (
echo .
echo ------------------------------
echo Error: generated files not found
echo Please check pathes in file ..\mapeditor\editor.ini
echo and generate map-files with MapEditor:
echo - open ..\mapeditor\*.mpf file with ..\..\..\tools\editor\MapEditor3.exe
echo - go to main menu [File/Save map] and browse to folder assets\data
echo ------------------------------
pause
goto END
)

echo .
echo ------------------------------
echo convert graphics res 
echo ------------------------------

@echo on
for %%i in (..\rawres\*.png) do ..\..\..\tools\bmpcvtr.exe %%i -8888 -2n -o%CURRENT_DIR%/assets/data

rem for file with objects create alpha collide map
..\..\..\tools\bmpcvtr.exe ..\rawres\FurSnuggleLevel.png -8888 -2n -acm4 -o%CURRENT_DIR%/assets/data

rem index color files
..\..\..\tools\bmpcvtr.exe ..\rawres\button_pr.png -5551 -2n -o%CURRENT_DIR%/assets/data
..\..\..\tools\bmpcvtr.exe ..\rawres\system_00.png -5551 -2n -o%CURRENT_DIR%/assets/data

rem scene1 fur_snuggle_look_out animation 
for %%i in (..\rawres\fur_snuggle_look_out\*.png) do ..\..\..\tools\bmpcvtr.exe %%i -8888 -2n -o%CURRENT_DIR%/assets/data/fur_snuggle_look_out

rem scene1 cactooInPot animation 
for %%i in (..\rawres\cactooInPot\*.png) do ..\..\..\tools\bmpcvtr.exe %%i -8888 -2n -o%CURRENT_DIR%/assets/data/cactooInPot

@echo off
copy ..\rawres\arial.fnt %CURRENT_DIR%\assets\data\ 

copy ..\rawres\color.vsh %CURRENT_DIR%\assets\data\
copy ..\rawres\lit.vsh %CURRENT_DIR%\assets\data\
copy ..\rawres\unlit.vsh %CURRENT_DIR%\assets\data\
copy ..\rawres\color.fsh %CURRENT_DIR%\assets\data\
copy ..\rawres\lit.fsh %CURRENT_DIR%\assets\data\
copy ..\rawres\unlit.fsh %CURRENT_DIR%\assets\data\

if NOT EXIST %CURRENT_DIR%\assets\data\color.vsh (
echo .
echo ------------------------------
echo Please copy *.fsh and *.vsh files from tengine\src\_default_shaders to ..\rawres
echo ------------------------------
pause
goto END
)

:END