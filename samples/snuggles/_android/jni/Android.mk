
LOCAL_PATH := $(call my-dir)

# -----------------------------
# ------ tengine --------------
# -----------------------------

include $(CLEAR_VARS)

LOCAL_MODULE := tengine

LOCAL_DEBUG_VER := $(DEBUG_VER)
# values: opengl1, opengl2 
LOCAL_APP_RENDER_TYPE := opengl2
# values: yes, no
LOCAL_APP_USE_SOUND := yes

LOCAL_C_INCLUDES := \
	$(LOCAL_PATH)/../../game \
	$(LOCAL_PATH)/../../../../src/tengine \
	$(LOCAL_PATH)/../../../../src/tengine/lib \
	$(LOCAL_PATH)/../../../../src/gui \
	$(LOCAL_PATH)/../../../../src/gamefield \
	$(LOCAL_PATH)/../../../../src/_android

LOCAL_CFLAGS := -DANDROID_NDK
ifeq ($(LOCAL_DEBUG_VER),1)
	LOCAL_CFLAGS += -DFRAME_ALLOCATOR -DSDK_DEBUG
else
	LOCAL_CFLAGS += -Wall -Wextra
endif
ifeq ($(LOCAL_APP_USE_SOUND),yes)	
	LOCAL_CFLAGS += -DUSE_SLES_SOUND
else
	LOCAL_CFLAGS += -DUSE_NO_SOUND
endif
ifeq ($(LOCAL_APP_RENDER_TYPE),opengl1)
	LOCAL_CFLAGS += -DUSE_OPENGL_1_RENDER
else
	ifeq ($(LOCAL_APP_RENDER_TYPE),opengl2)
		LOCAL_CFLAGS += -DUSE_OPENGL_2_RENDER
	endif
endif

LOCAL_SRC_FILES := \
	../../game/gamefield.cpp \
	../../game/gamelevel.cpp \
	../../game/gamelevelHud.cpp \
	../../game/gamelevelLoading.cpp \
	../../game/gamelevelScene1.cpp \
	../../game/gamelevelScene2.cpp \
	../../../../src/tengine/lib/a_filesystem.c \
	../../../../src/tengine/lib/fxmath.c \
	../../../../src/tengine/lib/gamepad.c \
	../../../../src/tengine/lib/gx_helpers.c \
	../../../../src/tengine/lib/hash.c \
	../../../../src/tengine/lib/jobs.c \
	../../../../src/tengine/lib/loadhelpers.c \
	../../../../src/tengine/lib/loadtdata.c \
	../../../../src/tengine/lib/memory.c \
	../../../../src/tengine/lib/network.c \
	../../../../src/tengine/lib/platform.c \
	../../../../src/tengine/lib/render2dgl1.c \
	../../../../src/tengine/lib/render2dgl2.c \
	../../../../src/tengine/lib/serialization.c \
	../../../../src/tengine/lib/sound.c \
	../../../../src/tengine/lib/tengine.c \
	../../../../src/tengine/lib/tengine_pr.c \
	../../../../src/tengine/lib/touchpad.c \
	../../../../src/tengine/lib/texts.c \
	../../../../src/tengine/lib/static_allocator.c \
	../../../../src/tengine/containers/allocator_array.c \
	../../../../src/gui/guibasebutton.c \
	../../../../src/gui/guibutton.c \
	../../../../src/gui/guifactory.c \
	../../../../src/gui/guicontainer.c \
	../../../../src/gui/guiobject2D.c \
	../../../../src/gui/guispeedbutton.c \
	../../../../src/gui/guislider.c \
	../../../../src/_android/app-android.c

LOCAL_LDLIBS := -landroid
ifeq ($(LOCAL_DEBUG_VER),1)
	LOCAL_LDLIBS += -ldl -llog
endif
ifeq ($(LOCAL_APP_USE_SOUND),yes)	
	LOCAL_LDLIBS += -lOpenSLES
endif
ifeq ($(LOCAL_APP_RENDER_TYPE),opengl1)
	LOCAL_LDLIBS += -lGLESv1_CM
else
	ifeq ($(LOCAL_APP_RENDER_TYPE),opengl2)
		LOCAL_LDLIBS += -lGLESv2
	endif
endif

LOCAL_STATIC_LIBRARIES := android_native_app_glue

include $(BUILD_SHARED_LIBRARY)

$(call import-module,android/native_app_glue)
