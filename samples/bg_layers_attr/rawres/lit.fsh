uniform sampler2D u_TextureUnit;
varying mediump vec2 v_TextureCoordinates;
varying lowp vec4 v_Color;
void main()
{
    gl_FragColor = texture2D(u_TextureUnit, v_TextureCoordinates) * v_Color;
}