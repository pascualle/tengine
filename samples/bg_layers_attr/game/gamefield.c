// This demo shows how to use BG attributes: blend color and transparency of the tile in the tilemap
// Steps:
// 1) Add the global preprocessor key USE_TILEMAP_ATTRIBUTES to the project
// 2) Create a memory allocator (sgTilemapStaticAllocator) and make a copy of the current tilemap attributes (createCopyTileMapColorAttributes() functiom)
// 3) Set the local copy (tilemapAttr) of the attribute map as default (setTileMapColorAttributes)
// After this, any change to the local copy (tilemapAttr) affects the render immediately.
// 
// Also, here you can find a demonstration of:
// - renderplane init
// - 2 different levels created with mapeditor uses to render on one renderplane (BGSELECT_SUB3)
//     layer 1 - GAME_LAYER based on level map l0 (mapeditor)
//     layer 2 - HUD_LAYER based on level map l1 (mapeditor)
// - FRAME_ALLOCATOR principle functionality memory using
// - wchar
// - text objects: dynamic M1_TXTBOX_FPS
// - getCurrentDeltaTimeScale(), framerate scale value

#include "tengine.h"
#include "texts.h"
#include "gamepad.h"
#include "gamefield.h"
#include "static_allocator.h"
#include "containers/allocator_list.h"
#include "constants.h"

//-------------------------------------------------------------------------------------------

enum GameStates
{
	gstNONE = 0,
	gstFORCEDISPLAYINIT,
	gstDISPLAYINIT,
	gstPREPARE_LOADING,
	gstLOADING,
	gstGAME
};

enum LoadingStates
{
	lstGAME = 0
};

static const s32 TIME_TO_PARTICLE_EFFECT = 2500;

static s32 gameState = gstNONE;
static s32 loadingState = lstGAME;
static struct fxVec2 camera_pos;
static s32 prepare_loading_draw_frame_count = 0;
static s32 last_fps = 0;
static s32 resizeW = 0;
static s32 resizeH = 0;
static fx32 timer = 0;
static u32* tilemapAttr = NULL;

#define EXTRA_MEMORY_FOR_ALLOCATOR_HEADER 32
#define TILEMAP_COPY_ALLOCATOR_SIZE (M0_TILES_COUNT * sizeof(u32) + EXTRA_MEMORY_FOR_ALLOCATOR_HEADER)
static u8 sgTilemapStaticHeap[TILEMAP_COPY_ALLOCATOR_SIZE];
static struct StaticAllocator sgTilemapStaticAllocator;

static u8* sgpContainersStaticHeap = NULL;
static struct StaticAllocator sgContainersStaticAllocator;

static const int CONTAINER_ITEMS_MAX = 10;
static struct AllocatorList tr_list;
static struct AllocatorList clr_list;


enum LoadMarkers
{
	LM_INIT = 0,
	LM_LOAD
};

enum GameLayers
{
	GAME_LAYER = 0,
	HUD_LAYER = 1,
	LAYER_COUNT
};
 
//-------------------------------------------------------------------------------------------

static void onEngineEvent(const struct EventCallbackData *pData);
static void onDrawObject(u32 layer, s32 iId, BOOL* const oDraw);

static void releaseTengineData(void);
static void releaseGameResources(void);

static void load(void);

static void doResize(void);

static void onLevelMapLoad(void);
static void onLevelHudLoad(void);

static void processInput(void);

static void changeBGAttributes(s32 ms);

//-------------------------------------------------------------------------------------------

void tfgInitMemory()
{
	InitMemoryAllocator();

	StaticAllocator_Init(&sgTilemapStaticAllocator, sgTilemapStaticHeap, TILEMAP_COPY_ALLOCATOR_SIZE);

	// Calculate the memory size for the allocator's heap for the tr_list and clr_list lists
	{
		const u32 heapsize_tr_list = AllocatorList_CalculateHeapSize(sizeof(u16), CONTAINER_ITEMS_MAX);
		const u32 heapsize_clr_list = AllocatorList_CalculateHeapSize(sizeof(GXRgba), CONTAINER_ITEMS_MAX);

		sgpContainersStaticHeap = (u8*)MALLOC(heapsize_tr_list + heapsize_clr_list, "tfgInitMemory: sgpContainersStaticHeap");
		StaticAllocator_Init(&sgContainersStaticAllocator, sgpContainersStaticHeap, heapsize_tr_list + heapsize_clr_list);
	}
}
//-------------------------------------------------------------------------------------------

void tfgInit()
{
	InitGamePad();
	gameState = gstNONE;
}
//-------------------------------------------------------------------------------------------

void tfgRelease()
{	
	releaseTengineData();

	// To avoid memory fragmentation, call FREE() last, in the opposite side
	FREE(sgpContainersStaticHeap);
	sgpContainersStaticHeap = NULL;
}
//-------------------------------------------------------------------------------------------

void releaseTengineData()
{
	// to avoid memory fragmentation, releaseMapData in opposite side
	releaseGameResources();
	releaseTextData();
	releaseCommonData();
	releaseRenderPlane(BGSELECT_SUB3);
	releaseEngine();
}
//-------------------------------------------------------------------------------------------

void releaseGameResources()
{
	releaseResources();

	releaseMapData(HUD_LAYER);
	releaseMapData(GAME_LAYER);

	// Allocator trick: Use StaticAllocator_Reset to reset all data assigned to the allocator
	//   There is no need to clear the tr_list and clr_list lists (slow)
	//   However, before using the lists, you need to initialize them to the allocator again (fast)
	StaticAllocator_Reset(&sgTilemapStaticAllocator);
	StaticAllocator_Reset(&sgContainersStaticAllocator);
	tilemapAttr = NULL;
}
//-------------------------------------------------------------------------------------------

void tfgSetWindowModeSize(s32 *const w, s32 *const h)
{
	*w = 800;
	*h = 600;
}
//-------------------------------------------------------------------------------------------

void tfgResize(s32 w, s32 h)
{
	gameState = gstDISPLAYINIT;
	resizeW = w;
	resizeH = h;
}
//-------------------------------------------------------------------------------------------

static void doResize()
{
	const s32 MAX_RENDER_OBJ_ON_SCENE = 128;

	struct RenderPlaneInitParams mp1;

	initRenderPlaneParametersWithDefaultValues(&mp1);
	mp1.mBGType = BGSELECT_SUB3;
	mp1.mSizes.mViewWidth = resizeW; 
	mp1.mSizes.mViewHeight = resizeH;

	// max objs estimation on scene: 128 obj for text "fps"
	mp1.mMaxRenderObjectsOnPlane = MAX_RENDER_OBJ_ON_SCENE;

	mp1.mX = 0;
	mp1.mY = 0;

	if(!isRenderPlaneInit(GAME_LAYER)) // any active plane, whatever
	{
		struct InitEngineParameters params;

		releaseTengineData();

		initEngineParametersWithDefaultValues(&params);
		params.layersCount = LAYER_COUNT;
		params.particlesPoolSize = 0;

		initEngine(&params);
		initRenderPlane(&mp1);
		
		assignLayerWithRenderPlane(GAME_LAYER, BGSELECT_SUB3, TRUE);
		assignLayerWithRenderPlane(HUD_LAYER, BGSELECT_SUB3, FALSE);

        //register callbacks
		setOnEvent(onEngineEvent);
		setOnDrawGameObject(onDrawObject);

		//begin load task
		//parameter will return with iData.initiatorId in onEngineEvent() function ( EVENT_TYPE_ON_END_ASYNH_LOAD_DATA)
		beginLoadListAsynh(LM_INIT);
		{
			//load common data
			addToLoadListCommonData();
			//load text data for certain language
			// to avoid memory fragmentation load it once after common data
			//addToLoadListLanguageData(LANGUAGE_ENG);
		}
		//end load task
		endLoadListAsynh(FALSE);
	
		camera_pos.x =  0;
		camera_pos.y =  0;

		//wait for EVENT_TYPE_ON_END_ASYNH_LOAD_DATA in onEngineEvent() function
		gameState = gstNONE;
	}
	else
	{
		resizeRenderPlane(GAME_LAYER, &mp1.mSizes);
		gameState = gstGAME;
	}
}
//-------------------------------------------------------------------------------------------

void load()
{
	// to avoid memory fragmentation, releaseMapData in opposite side

	//setVisible(HUD_LAYER, FALSE);
	//setVisible(GAME_LAYER, FALSE);

	releaseGameResources();
	OS_Printf("mem on newGame = %u\n", GetFreeMemorySize());

	beginLoadListAsynh(LM_LOAD);
	{
		if(loadingState == lstGAME)
		{
			//layer LAYER_GAME, load current level
			addToLoadListMap(GAME_LAYER, 0); // i.e. l0 map file in assets/data folder
			//layer LAYER_HUD, load or reload hud
			addToLoadListMap(HUD_LAYER, 1); // i.e. l1 map file in assets/data folder
		}
		else
		{
			SDK_ASSERT(0);
		}
	}
	endLoadListAsynh(TRUE);
	
	//wait for  EVENT_TYPE_ON_END_ASYNH_LOAD_DATA and EVENT_TYPE_ON_END_ASYNH_LOAD_RESOURSES in onEngineEvent() function
	gameState = gstNONE;
}
//-------------------------------------------------------------------------------------------

// file index 1, layer GAME_LAYER
void onLevelMapLoad()
{
	//struct fxVec2 pos;

	resetParticles();

	// only on onLevelMapLoad (called from onEngineEvent function)
	// you can work with M0_* objects, because current active layer is GAME_LAYER 
	//pos = prGetPosition(M0_CAMERA_1); // constants.h (gererated by MapEditor3)
	camera_pos.x = 0;//pos.x;
	camera_pos.y = 0;//pos.y;

#ifdef USE_TILEMAP_ATTRIBUTES
	if (tilemapAttr == NULL)
	{
		// Create tilemapAttr and assign it as the main bg attribute data to the engine
		tilemapAttr = createCopyTileMapColorAttributes(GAME_LAYER, &sgTilemapStaticAllocator);
		setTileMapColorAttributes(GAME_LAYER, tilemapAttr, &sgTilemapStaticAllocator);

		// Initialize tr_list and clr_list with the sgContainersStaticAllocator allocator
		AllocatorList_Init(&tr_list, sizeof(u16), &sgContainersStaticAllocator);
		AllocatorList_Init(&clr_list, sizeof(GXRgba), &sgContainersStaticAllocator);
	}
#endif

	setCamera(camera_pos);
}
//-------------------------------------------------------------------------------------------

void onLevelHudLoad()
{
	last_fps = 0;
    gameState = gstGAME;
}
//------------------------------------------------------------------------------------

void tfgLostRenderDevice()
{
}
//-------------------------------------------------------------------------------------------

void tfgRestoreRenderDevice()
{
}
//------------------------------------------------------------------------------------

void tfgLowMemory()
{
}
//-------------------------------------------------------------------------------------------

void tfgTick(s32 ms)
{
	switch(gameState)
	{
		case gstNONE:
		break;

		case gstFORCEDISPLAYINIT:
			releaseTengineData();
		case gstDISPLAYINIT:
			doResize();
		break;

		case gstPREPARE_LOADING:
			// To avoid flickering then loading: 
			//   call drawWithoutInternalLogicUpdateMode() function
			//   and skip one or two ticks
			drawWithoutInternalLogicUpdateMode();
			if(++prepare_loading_draw_frame_count > 2)
			{
				gameState = gstLOADING;
			}
		break;

		case gstLOADING:
			prepare_loading_draw_frame_count = 0;
			load();
		break;

		case gstGAME:
			processInput();
			changeBGAttributes(ms);
			break;
	}
}
//-------------------------------------------------------------------------------------------

void processInput()
{
	UpdateGamePad();

	if(IsButtonDown(TYPE_START))
	{
		gameState = gstPREPARE_LOADING;
	}
}
//----------------------------------------------------------------------------------

void onDrawObject(u32 layer, s32 iId, BOOL* const oDraw)
{
	(void)oDraw;
	switch(layer)
	{		
		case HUD_LAYER:
		{
			switch(iId)
			{
				case M1_TXTBOX_FPS:
					if(last_fps != getFPS())
					{
						wchar aaa[16];
						wchar bbb[16];
						bbb[0] = L'f';
						bbb[1] = L'p';
						bbb[2] = L's';
						bbb[3] = L'=';
						bbb[4] = L'%';
						bbb[5] = L'd';
						bbb[6] = L'\0';
						last_fps = getFPS();
						STD_WSnprintf(aaa, 16, bbb, last_fps);
						txbSetDynamicTextLine(M1_TXTBOX_FPS, 0, aaa);
					}
				
				default:
					break;
			}
		}

		default:
			break;
	}
}
//-------------------------------------------------------------------------------------------

void changeBGAttributes(s32 ms)
{
	(void)ms;
#ifdef USE_TILEMAP_ATTRIBUTES
	if (tilemapAttr != NULL)
	{
		const fx32 speed = FX32(0.2);
		const fx32 currentDeltaTimeScale = getCurrentDeltaTimeScale();
		
		u32 idx = 0;

		if (AllocatorList_Size(&tr_list) == 0)
		{
			for (u16 i = 0; i < CONTAINER_ITEMS_MAX; i++)
			{
				AllocatorList_PushBack(&tr_list, &idx);
				idx += ALPHA_OPAQ / CONTAINER_ITEMS_MAX;

				if (i == 0)
				{
					const GXRgba r = COLOR888TO1555(32, 160, 32);
					AllocatorList_PushBack(&clr_list, &r);
				}
				else if (i == 2)
				{
					const GXRgba g = COLOR888TO1555(0, 255, 0);
					AllocatorList_PushBack(&clr_list, &g);
				}
				else if (i == 4)
				{
					const GXRgba b = COLOR888TO1555(0, 0, 255);
					AllocatorList_PushBack(&clr_list, &b);
				}
				else
				{
					const GXRgba f = (GXRgba)DIFD_DEF_FILL_COLOR;
					AllocatorList_PushBack(&clr_list, &f);
				}
			}
			timer = FX32_ONE;
		}

		timer += FX_Mul(speed, currentDeltaTimeScale);

		if(timer > FX32_ONE)
		{
			const u32 start_tr_tile_idx = 1300;
			const u32 start_color_tile_idx = 1428;
			const struct ListItem* it = AllocatorList_Begin_Const(&tr_list);
			
			idx = 0;
			while (it != AllocatorList_End_Const(&tr_list))
			{
				const u16 tr = *(u16*)AllocatorList_Val(&tr_list, it);
				tilemapAttr[start_tr_tile_idx + idx] = ((tr << 16) | DIFD_DEF_FILL_COLOR);
				it = AllocatorList_Next_Const(&tr_list, it);
				idx++;
			}
			AllocatorList_Move(&tr_list, AllocatorList_Begin(&tr_list), AllocatorList_Tail(&tr_list));

			idx = 0;
			it = AllocatorList_Begin_Const(&clr_list);
			while (it != AllocatorList_End_Const(&clr_list))
			{
				const GXRgba color = *(GXRgba*)AllocatorList_Val(&clr_list, it);
				tilemapAttr[start_color_tile_idx + idx] = ((ALPHA_OPAQ << 16) | color);
				it = AllocatorList_Next_Const(&clr_list, it);
				idx++;
			}
			AllocatorList_Move(&clr_list, AllocatorList_Tail(&clr_list), AllocatorList_Begin(&clr_list));

			timer = 0;
		}
	}
#endif
}
//----------------------------------------------------------------------------------

//CALLBACK FUNCTIONS

//----------------------------------------------------------------------------------

static void onEngineEvent(const struct EventCallbackData *pData)
{	
	switch(pData->eventType)
	{
		case EVENT_TYPE_ON_SOUND:
		break;

		case EVENT_TYPE_ON_ANIMATION:
		case EVENT_TYPE_ON_DIRECTOR:
        break;
    
		case  EVENT_TYPE_ON_END_ASYNH_LOAD_DATA:
			switch(pData->eventId)
			{
				case LOAD_TYPE_ENDLOADDATATASK:
					if(pData->initiatorId == LM_INIT)
					{
						gameState = gstPREPARE_LOADING;
					}
				break;
				case LOAD_TYPE_MAPDATA:
					if(pData->initiatorId == LM_LOAD)
					{
						switch(pData->layer)
						{
							case HUD_LAYER:
								onLevelHudLoad();
							break;
							case GAME_LAYER:
								onLevelMapLoad();
						}
					}
				default:
					break;
			}
		break;
		
		case EVENT_TYPE_ON_END_ASYNH_LOAD_RESOURSES:
			//setVisible(HUD_LAYER, TRUE);
			//setVisible(GAME_LAYER, TRUE);
			gameState = gstGAME;

		default:
			break;
	}
}
//----------------------------------------------------------------------------------
