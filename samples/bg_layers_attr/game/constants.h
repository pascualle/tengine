#ifndef _GE_CONSTANTS_H_
#define _GE_CONSTANTS_H_

enum GEConstants
{
	// Map tiles count
	M0_TILES_COUNT = 3072,
	M1_TILES_COUNT = 768,

	// Languages:
	LANGUAGE_DEFAULT = 0,

	// Game object types:
	TRIGGER = 0,
	DIRECTOR = 1,
	TEXTBOX = 2,
	CAMERA = 5,

	// Properties of game object:
	PRP_ENABLE = 0,

	// Map0 objects list
	M0_CAMERA_2 = 0,

	// Map1 fonts list
	M1_FONT_SYSTEM = 0,

	// Map1 objects list
	M1_TXTBOX_FPS = 0,

	// Animations: (for drawSingleFrame() function only)
	ANIM_IDLE = 0,

	// States:
	STATE_IDLE = 0,

	// Movement :
	DIR_IDLE = 0,
	DIR_UP = 2,
	DIR_LEFT = 4,
	DIR_RIGHT = 6,
	DIR_DOWN = 8,
	DIR_COUNT = 5

};

#endif
