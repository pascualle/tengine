@echo off

echo ------------------------------
echo Generate map (MapEditor) files
echo Please verify correct pathes in file ..\mapeditor\editor.ini
echo By default editor.ini contain paths from C disk
echo ------------------------------

set CURRENT_DIR=%~dp0
if EXIST %CURRENT_DIR%\assets\data\o del %CURRENT_DIR%\assets\data\o

rem for windows wchar_size = 2
..\..\..\tools\editor\MapEditor3.exe -wchar_size2 -p%CURRENT_DIR%\..\mapeditor\bg_layers.meproj -i%CURRENT_DIR%\..\mapeditor\ -o%CURRENT_DIR%\assets\data\ -l%CURRENT_DIR%\log.txt 

echo MapEditor report %CURRENT_DIR%\log.txt
type %CURRENT_DIR%\log.txt

if NOT EXIST %CURRENT_DIR%\assets\data\o (
echo .
echo ------------------------------
echo Error: generated files not found
echo Please check pathes in file ..\mapeditor\editor.ini
echo and generate map-files with MapEditor:
echo - open ..\mapeditor\*.mpf file with ..\..\..\tools\editor\MapEditor3.exe
echo - go to main menu [File/Save map] and browse to folder assets\data
echo ------------------------------
pause
goto END
)

echo .
echo ------------------------------
echo convert graphics res 
echo ------------------------------

@echo on
for %%i in (..\\rawres\*.png) do ..\..\..\tools\bmpcvtr.exe %%i -5551 -2n -fnearest -oassets/data
@echo off

copy ..\rawres\system.fnt assets\data\

copy ..\rawres\color.vsh assets\data\
copy ..\rawres\lit.vsh assets\data\
copy ..\rawres\unlit.vsh assets\data\
copy ..\rawres\color.fsh assets\data\
copy ..\rawres\lit.fsh assets\data\
copy ..\rawres\unlit.fsh assets\data\

if NOT EXIST %CURRENT_DIR%\assets\data\color.vsh (
echo .
echo ------------------------------
echo Please copy *.fsh and *.vsh files from tengine\src\_default_shaders to ..\rawres
echo ------------------------------
pause
goto END
)

:END