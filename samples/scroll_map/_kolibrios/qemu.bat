set QEMU_PATH=c:\qemu
set KOLIBRIOS_IMG_PATH=c:\kolibri\dist\kolibri.img
set HDD_IMG_PATH=c:\Work\kolibri\image\c100.img
set HDD_PATH=.
rem SET QEMU_AUDIO_DRV=dsound
rem %QEMU_PATH%\qemu-system-i386.exe -audio-help
rem %QEMU_PATH%\qemu-system-i386.exe -soundhw help
%QEMU_PATH%\qemu-system-i386.exe -L %QEMU_PATH% -m 128 -drive file=%KOLIBRIOS_IMG_PATH%,if=floppy,media=disk,format=raw -boot a ^
-drive file=%HDD_IMG_PATH%,if=ide,media=disk,format=raw -localtime -vga vmware -net nic,model=rtl8139 -net user -soundhw hda -usb -usbdevice tablet ^
-usb -usbdevice disk:format=raw:fat:%HDD_PATH%