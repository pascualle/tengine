���������� �� ��������� KolibriOS newlib ��� �� Windows
-------------------------------------------------------


��������� toolchain
-------------------

� ����� � ���������� toolchain ��������� �� ������
ftp.KolibriOS.org/users/Serge/new/Toolchain
Windows-������ ������, msys-kos32-x.x.x.7z
� ��� ��������� toolchain ����� mingw/msys, ��� ����� �� ����� www.mingw.org �� ������ downloads ����� ������� � ��������� ���� mingw-get-setup.exe
� ����������� ���� ����� ������� ������ �install� ����� ������� ���� ���������, �������� ������� �������� "..support for graphics user interface" � ������ �Continue�.
��������, ������� � ������ ������������, ��� ��������� mingw ����������� � c:\MinGW
� ����������� ���� ����� �������� mingw32-base � msys-base � ������ ���������. 
� ����� ��������� ������� � ����� c:\MinGW\msys\1.0 � ������� � ��� ������ ����� home\autobuild\tools
� ����������� msys-kos32-x.x.x.7z � c:\MinGW\msys\1.0\home\autobuild\tools
(���� ��� ������� ���������, ���� kos32-gcc.exe ������ ���������� �� ���� c:\MinGW\msys\1.0\home\autobuild\tools\win32\bin\kos32-gcc.exe)
� ��������� ����������������� toolchain ����� �������� c:\MinGW\msys\1.0\msys.bat
� ��������� ������ ������� 
    export PATH=$PATH:/home/autobuild/tools/win32/bin
    kos32-gcc �v
�� ������ ������ ��������� ���������� � ������������ � ������ kos32-gcc


��������� libc
--------------

���������� ��������� libc ��������� � svn://kolibrios.org/contrib �����������, ���� contrib\sdk\sources\newlib  
����� ����� ��������������� ��� ����������� KolibriOS.org � ������� SVN � ������� ��������������� �����.
��������, ������� � ������ ������������, ��� ��������� newlib ����������� � d:\kolibri\contrib\sdk\sources\newlib

(�����������) ���������� ������� ���������� libc (*.a, *.dll) ��������� � ����������� svn://kolibrios.org/data/common/lib

�����:
���������� � ��������� ������ ������������� �� �������� ����� (root) � ����� �������:
(root)\data\common\lib
(root)\contrib\sdk\sources\newlib

����� ������� ���������� �������������� (�����������):
� ����� ��������� ������� � ����� d:\kolibri\contrib\sdk � ������� � ��� ����� bin
� ��������� c:\MinGW\msys\1.0\msys.bat
� � ��������� ������ �������
    cd d:/kolibri/contrib/sdk/sources/newlib/libc
    export PATH=$PATH:/home/autobuild/tools/win32/bin
    make shared
    make install
� ��������� dll ���������� ����� ���������� � d:\kolibri\contrib\sdk\bin


������ ����������
-----------------

� ������� ���������� ��������� KOLIBRIOS_HOME, ��������� ���� � unix ������� � �������� ����� ��� data � contrib
 ������: /d/kolibri
� ��� ������ ���������� ����� ��������� c:\MinGW\msys\1.0\msys.bat
� ��������� ������ �������
    cd d:/Work/tengine/samples/scroll_map/_kolibrios
    make

�����:
��� ������ ������� ����� ������������ ���������� kos32-gcc
� � ������, ������� ������� ��������� ����� ��������:   -lgcc -lc.dll ������ ��������� � ������
� �++ ������, ������� ������� ��������� ����� ��������: -lstdc++ -lsupc++ -lgcc -lc.dll
� � ������ CFLAGS ����������� ���� ��������� -U_Win32 -U_WIN32 -U__MINGW32__
� ����� LDFLAGS ��� ������� ��������: -static --subsystem native --stack <�� �������. default 0x200000> -Tapp-dynamic.lds --image-base 0
  ����� LDFLAGS ��� ����������:       -static                    --stack <�� �������. default 0x200000> -Tapp-dynamic.lds --image-base 0
  ��� ���������� �����������:          ������ -Tapp-dynamic.lds �������� �� -Tapp-static.lds
� ����� LDFLAGS ��� ������ dll:       -shared -s -T dll.lds --entry _DllStartup --image-base=0 --out-implib lib$(LIBRARY).dll.a

  ������: d:/Work/tengine/samples/scroll_map/_kolibrios/makefile


������� ������ ����� Windows � KolibriOS
----------------------------------------

� �������������� ����� Flash/HDD� ����� KolibriOS ��������� �� ������ http://kolibrios.org/ru/download
��������, ��������������, ��� ����� KolibriOS ���������� � ��������� �� ���� d:\kolibri\dist

������ �� �������� ������

����� ������� ������ ���������� ����� ����� Windows � KolibriOS � ��� flash-usb ����������.
� ���������� KolibriOS �� flash-usb ���������� ����� � ������� dist\HD_Load\USB_Boot\inst.exe
(������ dist\HD_Load\USB_Boot\inst.exe �� ����������, ����� ����������� setmbr.exe � ��� �� �����)

��������

Qemu � ������� �������� (� �������������) ����������, ��������������� ��������� �����������. ����� ��������� Qemu ��������� �� ������ qemu.weilnetz.de
� ����� ��������� � ������� ����� (��������, d:\Work\tengine\samples\scroll_map\_kolibrios) ������� ���� qemu.bat
    set QEMU_PATH="c:/Program Files/qemu/"
    set PATH=%PATH%;%QEMU_PATH%
    qemu-system-i386.exe -L . -m 128 -fda d:/kolibri/dist/kolibri.img -boot a -hda image/c100.img -localtime -vga vmware -net nic,model=rtl8139 -net user -soundhw ac97 -usb -usbdevice tablet
��� ��� kolibri.img ����� ������ �� ����������, ����� ������� ��� ���� (image/c100.img) ����� ����� � ���������� ��� ��� hdd. ���� ����� ������ ���� ��� �����������������, ������� ��� ����� �� ���������, � ����� � ���������� �� ������ flash-usb ���������� � ������� ������� Win32DiskImager.exe
������������� ����� ����� � ������� WinImage.

���� ��� �������� KolibriOS ���������� /hd0/1 ����� ������, ����� ������������� ������� � � ������������ ���� �������� ������� ����� "[b] �������� �����, ������� ����� BIOS", � �������� ��� ����� (1)

��������� ����� ������������� �����

� ����� ������������ ���� � KolibriOS � ��������� ����������� ������ ����� � ����� ���� \kolibri.lbl, ���������� �� ������������ d:\kolibri\dist
� ����� ��������� � ������� dll ������ \kolibrios\lib\*.dll, ��� ���� ���� �� ����� �� ������, ������� ��������. ���� ������ ���, �� ����� ����������� �� ������������ d:\kolibri\dist\kolibrios\lib �� ������������ ����