��� �������� js/html ������ ���������� � ������� emscripten v2.0+

- ���������� Emscripten-SDK https://emscripten.org/docs/getting_started/downloads.html
- ��������������� �������, ��������� 0_make_res.bat � ������� �����
- (windows) ���������� ���������� ���������� ����� EMSCRIPTEN_HOME (�������� EMSCRIPTEN_HOME=d:\Emscripten)
- (windows) ��������� 1_make_app.bat � ������� �����
- (linux) ��������� make � ������� �����

��������� �� ie11/chrome/safari (� safari ����� �������� develop/enable webgl)

How do I run a local webserver for testing / why does my program stall in �Downloading�� or �Preparing��:
---------------------------------------------------------------------------------------------------------
That error can happen when loading the page using a file:// URL, which works in some browsers but not in others.
Instead, it�s best to use a local webserver.
For example, Python has one built in, 
python -m http.server 
(in Python 3)
or 
python -m SimpleHTTPServer
(in Python 2)
After doing that, you can visit 
http://localhost:8000/
