#include "classes/MainLayer.hpp"
#ifdef B2D_DEBUG_DRAW
#include "classes/B2DDebugDraw.hpp"
#endif
#include "constants.h"
#include "gamepad.h"
#include "touchpad.h"

//-------------------------------------------------------------------------------------------

static const fx32 SWITCH_CAMERA_STEP = FX32(10.0f);

static const b2Vec2 gsGavity(0.0f, 10.0f);

//-------------------------------------------------------------------------------------------

#define MAX_CHARACTERS_BODIES 5
static s32 gsCharacterObjName[MAX_CHARACTERS_BODIES] =
{
	M0_BUTTON_IDLE_2,
	M0_BUTTON_IDLE_3,
	M0_BUTTON_IDLE_4,
	M0_BUTTON_IDLE_5,
	M0_HERO_7
};

#define MAX_OBSTACLES_BODIES 1
static s32 gsObstacleObjName[MAX_OBSTACLES_BODIES] =
{
	M0_BUTTON_IDLE_6
};

//-------------------------------------------------------------------------------------------

#ifdef B2D_DEBUG_DRAW
class QueryCallback : public b2QueryCallback
{
public:
	QueryCallback(const b2Vec2& point)
	{
		mPoint = point;
		mpFixture = nullptr;
	}

	bool ReportFixture(b2Fixture* fixture) override
	{
		b2Body* body = fixture->GetBody();
		if (body->GetType() == b2_dynamicBody)
		{
			const bool inside = fixture->TestPoint(mPoint);
			if (inside)
			{
				mpFixture = fixture;
				// We are done, terminate the query.
				return false;
			}
		}

		// Continue the query.
		return true;
	}

	b2Vec2 mPoint;
	b2Fixture* mpFixture;
};
#endif
//-------------------------------------------------------------------------------------------

MainLayer::MainLayer(Game* game)
 : GameLayer(game)
 , mSwitchCamera(FALSE)
 , mpWorld(nullptr)
 , mpHero(nullptr)
 , mpCameraObj(nullptr)
 , mKeyState(B2BCharacterManager::Command::CC_IDLE)
{
#ifdef B2D_DEBUG_DRAW
	mpDebugDraw = new B2DDebugDraw();
	mpGroundBody = nullptr;
	mpMmouseJoint = nullptr;
#endif
}
//-------------------------------------------------------------------------------------------

MainLayer::~MainLayer()
{
#ifdef B2D_DEBUG_DRAW
	delete mpDebugDraw;
#endif
	SDK_ASSERT(mpWorld == nullptr);
}
//-------------------------------------------------------------------------------------------

void MainLayer::OnLayerRelease()
{
	if (mpWorld != nullptr)
	{
		DebugMouseUp();

		mObstacleManager.Release();
		mPlatformManager.Release();
		mCharManager.Release();
		mPhysics.Release();

#ifdef B2D_DEBUG_DRAW
		mpWorld->DestroyBody(mpGroundBody);
		mpGroundBody = nullptr;
#endif
		delete mpWorld;
	}
	mpWorld = nullptr;
}
//-------------------------------------------------------------------------------------------

void MainLayer::OnLayerLoad()
{
	mSwitchCamera = FALSE;

	mpHero = mpCameraObj = &gsCharacterObjName[4];

	mpWorld = new b2World(gsGavity);

	mPhysics.Init(mpWorld);

#ifdef B2D_DEBUG_DRAW
	b2BodyDef bodyDef;
	mpGroundBody = mpWorld->CreateBody(&bodyDef);
#endif

	mCharManager.Init(gsCharacterObjName, MAX_CHARACTERS_BODIES, mpWorld);

	mCharManager.SetHero(*mpHero);

#ifdef B2D_DEBUG_DRAW
	{
		mpWorld->SetDebugDraw(mpDebugDraw);
		uint32 flags = 0;
		flags += b2Draw::e_shapeBit;
		//flags += b2Draw::e_jointBit;
		//flags += b2Draw::e_aabbBit;
		//flags += b2Draw::e_centerOfMassBit;
		mpDebugDraw->SetFlags(flags);
	}
#endif

	mPlatformManager.Init(mpWorld);

	mObstacleManager.Init(gsObstacleObjName, MAX_OBSTACLES_BODIES, mpWorld);
}
//-------------------------------------------------------------------------------------------

void MainLayer::OnBeginUpdate(const s32& ms)
{
	const float32 timeStep = static_cast<float32>(ms) / 1000.0f;
	const int32 velocityIterations = 6;
	const int32 positionIterations = 2;
	mpWorld->Step(timeStep, velocityIterations, positionIterations);

	mCharManager.Update(ms);

	fxVec2 opos = prGetPosition(*mpCameraObj);
	if(mSwitchCamera == TRUE)
	{
		mSwitchCamera = nextPointAtLine(mSwitchCameraPos, opos, SWITCH_CAMERA_STEP, &mSwitchCameraPos);
		if(mSwitchCamera == TRUE)
		{
			opos = mSwitchCameraPos;
		}
	}

	setCamera(opos);

	UpdateGamePad();

#ifdef B2D_DEBUG_DRAW
	const TouchPadData* tpData = GetGame()->GetTpData();
	if (tpData->point[0].mTouch)
	{
		fxVec2 mfxpos;
		const fx32& scale = getRenderPlaneScale(GAME_LAYER);
		const fx32 px = FX32(tpData->point[0].mX);
		const fx32 py = FX32(tpData->point[0].mY);
		mfxpos.x = FX_Div(px - FX32(getViewLeft(GAME_LAYER)), scale) + getViewOffsetX(GAME_LAYER);
		mfxpos.y = FX_Div(py - FX32(getViewTop(GAME_LAYER)), scale) + getViewOffsetY(GAME_LAYER);
		const b2Vec2 b2mpos = b2PixelsToMeters(mfxpos);

		if (tpData->point[0].mTrg)
		{
			DebugMouseDown(b2mpos);
		}
		else
		{
			DebugMouseMove(b2mpos);
		}

		if (mKeyState != B2BCharacterManager::Command::CC_IDLE)
		{
			mKeyState = B2BCharacterManager::Command::CC_IDLE;
			mCharManager.SetCommand(mKeyState, *mpHero);
		}
	}
	else
	{
		if (tpData->point[0].mRls)
		{
			DebugMouseUp();
		}
#endif
		s32 oldKeyState = mKeyState;

		if (IsButtonPress(TYPE_LEFT))
		{
			mKeyState |= B2BCharacterManager::Command::CC_LEFT;
			oldKeyState = -1;
		}
		else
		{
			mKeyState &= ~B2BCharacterManager::Command::CC_LEFT;
		}

		if (IsButtonPress(TYPE_RIGHT))
		{
			mKeyState |= B2BCharacterManager::Command::CC_RIGHT;
			oldKeyState = -1;
		}
		else
		{
			mKeyState &= ~B2BCharacterManager::Command::CC_RIGHT;
		}

		if (IsButtonDown(TYPE_UP))
		{
			mKeyState |= B2BCharacterManager::Command::CC_JUMP;
			oldKeyState = -1;
		}
		else
		{
			mKeyState &= ~B2BCharacterManager::Command::CC_JUMP;
		}

		if (mKeyState != oldKeyState)
		{
			mCharManager.SetCommand(mKeyState, *mpHero);
		}
#ifdef B2D_DEBUG_DRAW
	}
#endif
}
//-------------------------------------------------------------------------------------------

#ifdef B2D_DEBUG_DRAW
void MainLayer::DebugMouseDown(const b2Vec2& p)
{
	if (mpMmouseJoint != nullptr)
	{
		return;
	}

	// Make a small box.
	b2AABB aabb;
	b2Vec2 d;
	d.Set(0.001f, 0.001f);
	aabb.lowerBound = p + d;
	aabb.upperBound = p - d;

	// Query the world for overlapping shapes.
	QueryCallback callback(p);
	mpWorld->QueryAABB(&callback, aabb);

	if (callback.mpFixture)
	{
		b2Body* body = callback.mpFixture->GetBody();
		b2MouseJointDef md;
		md.bodyA = mpGroundBody;
		md.bodyB = body;
		md.target = p;
		md.maxForce = 1000.0f * body->GetMass();
		mpMmouseJoint = (b2MouseJoint*)mpWorld->CreateJoint(&md);
		body->SetAwake(true);
	}
}
//-------------------------------------------------------------------------------------------

void MainLayer::DebugMouseUp()
{
	if (mpMmouseJoint)
	{
		mpWorld->DestroyJoint(mpMmouseJoint);
		mpMmouseJoint = nullptr;
	}
}
//-------------------------------------------------------------------------------------------

void MainLayer::DebugMouseMove(const b2Vec2& p)
{
	if (mpMmouseJoint)
	{
		mpMmouseJoint->SetTarget(p);
	}
}
//-------------------------------------------------------------------------------------------
#endif

void MainLayer::OnDrawBackgroundTiles(const s32& ms)
{
	(void)ms;
#ifdef B2D_DEBUG_DRAW
	mpWorld->DrawDebugData();
#endif
}
//-------------------------------------------------------------------------------------------

void MainLayer::OnDrawObject(const s32& ms, const s32& id, BOOL* const opDraw)
{
	(void)ms;
	(void)opDraw;
	
	if (mCharManager.Has(id))
	{
		mCharManager.OnDraw(id);
	}
	else if(mObstacleManager.Has(id))
	{
		mObstacleManager.OnDraw(id);
	}
}
//-------------------------------------------------------------------------------------------
