#include "classes/B2DHelper.hpp"

//-------------------------------------------------------------------------------------------

static b2Vec2 gsVec2;
static fxVec2 gsfxVec2;

//-------------------------------------------------------------------------------------------

__inline void Px_To_M(const fxVec2& p, b2Vec2& res)
{
	res.x = FX_FX32_TO_F32(0.02 * p.x);
	res.y = FX_FX32_TO_F32(0.02 * p.y);
}
//-------------------------------------------------------------------------------------------

__inline void M_To_Px(const b2Vec2& p, fxVec2& res)
{
	res.x = FX32(50.0 * p.x);
	res.y = FX32(50.0 * p.y);
}
//-------------------------------------------------------------------------------------------

const b2Vec2& b2GetGameObjectPosition(const s32& id)
{
	const fxVec2& pos = prGetPosition(id);
	Px_To_M(pos, gsVec2);
	return gsVec2;
}
//-------------------------------------------------------------------------------------------

void b2SetGameObjectPosition(const s32& id, const b2Vec2& b2pos)
{
	fxVec2 pos;
	M_To_Px(b2pos, pos);
	prSetPosition(id, pos);
}
//-------------------------------------------------------------------------------------------

void b2SetGameObjectRotation(const s32& id, const float& angle_rad)
{
	const fx32 angle_deg = FX_Div((FX32(angle_rad) * 180), FX_PI);
	prSetCustomRotation(id, angle_deg);
}
//-------------------------------------------------------------------------------------------

void b2SetCameraPosition(const b2Vec2& b2pos)
{
	fxVec2 pos;
	M_To_Px(b2pos, pos);
	setCamera(pos);
}
//-------------------------------------------------------------------------------------------

float32 b2PixelsToMeters(const float32& val)
{
	return 0.02f * val;
}
//-------------------------------------------------------------------------------------------

float32 b2MetersToPixels(const float32& val)
{
	return 50.0f * val;
}
//-------------------------------------------------------------------------------------------

const b2Vec2& b2PixelsToMeters(const fxVec2& fxpos)
{
	Px_To_M(fxpos, gsVec2);
	return gsVec2;
}
//-------------------------------------------------------------------------------------------

const fxVec2& b2MetersToPixels(const b2Vec2& b2pos)
{
	M_To_Px(b2pos, gsfxVec2);
	return gsfxVec2;
}
//-------------------------------------------------------------------------------------------
