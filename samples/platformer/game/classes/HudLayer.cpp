#include "classes/HudLayer.hpp"
#include "constants.h"
#include "texts.h"

HudLayer::HudLayer(Game* game)
 : GameLayer(game)
 , mCurrentFPS(0)
{
	bbb[0] = L'f';
	bbb[1] = L'p';
	bbb[2] = L's';
	bbb[3] = L'=';
	bbb[4] = L'%';
	bbb[5] = L'd';
	bbb[6] = L'\0';
}
//-------------------------------------------------------------------------------------------

void HudLayer::OnLayerRelease()
{
}
//-------------------------------------------------------------------------------------------

void HudLayer::OnLayerLoad()
{
}
//-------------------------------------------------------------------------------------------

void HudLayer::OnBeginUpdate(const s32& ms)
{
	(void)ms;
}
//-------------------------------------------------------------------------------------------

void HudLayer::OnDrawBackgroundTiles(const s32& ms)
{
	(void)ms;
}
//-------------------------------------------------------------------------------------------

void HudLayer::OnDrawObject(const s32& ms, const s32& id, BOOL* const opDraw)
{
	(void)ms;
	(void)opDraw;
	switch(id)
	{
		case M1_TXTBOX_FPS:
		if(mCurrentFPS != getFPS())
		{
			mCurrentFPS = getFPS();
			STD_WSnprintf(aaa, 7, bbb, mCurrentFPS);
			txbSetDynamicTextLine(M1_TXTBOX_FPS, 0, aaa);
		}		
		default:
		break;
	}	
}
//-------------------------------------------------------------------------------------------
