#include "classes/B2DCharacter.hpp"
#include "classes/B2DHelper.hpp"
#include "level_id.h"

//-------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------

void B2BCharacterManager::Init(const s32* arrId, const s32& maxCharacters, b2World* world)
{
	s32 i;

	mpWorld = world;

	for (i = 0; i < maxCharacters; i++)
	{
		mMap[arrId[i]] = new B2BCharacter();
		Create(arrId[i]);
	}

	mMs = 0;
}

void B2BCharacterManager::SetHero(const s32& id)
{
	for (auto& it : mMap)
	{
		B2BCharacter* c = it.second;
		if (c->mId != id)
		{
			c->SetType(B2BObject::Type::OT_NPC);
		}
		else
		{
			c->SetType(B2BObject::Type::OT_Hero);

			const u32 zone = 0;
			u16 renderlist_index;
			u16 index;
			if (rlGetGameObjectIndex(id, GAME_LAYER, zone, &renderlist_index, &index))
			{
				const u16 rCount = rlGetGameObjectsCount(GAME_LAYER, zone, renderlist_index);
				SDK_ASSERT(rCount > index);
				BOOL res = rlMoveGameObject(GAME_LAYER, zone, renderlist_index, index, 0);
				SDK_ASSERT(res == TRUE);
			}
		}
	}
}

void B2BCharacterManager::OnDraw(const s32& id) const 
{
	B2BCharacter* c = mMap.at(id);
	
	const b2Vec2& position = c->mpBodyB->GetPosition();
	const float32& angle_rad = c->mpBodyB->GetAngle();
	b2SetGameObjectPosition(id, position + c->mTexureOffset);
	b2SetGameObjectRotation(id, angle_rad);

	/*c->mJumpCooldown -= mMs;
	if (c->mJumpCooldown < 0)
	{
		c->mJumpCooldown = 0;
	}*/
}

void B2BCharacterManager::Create(const s32& id)
{
	B2BCharacter* c = mMap.at(id);

	c->mId = id;

	b2BodyDef bodyDef;
	bodyDef.type = b2_dynamicBody;
	bodyDef.fixedRotation = true;

	fx32 rect[RECT_SIZE];
	fxVec2 fxsize;
	prGetCollideRect(id, rect);
	fxsize.x = rect[RECT_RIGHT_TOP_X] - rect[RECT_LEFT_TOP_X];
	fxsize.y = rect[RECT_LEFT_BOTTOM_Y] - rect[RECT_LEFT_TOP_Y];
	fxsize.y -= FX_Div(fxsize.x, FX32(2));
	if (fxsize.y <= 0)
	{
		fxsize.y = FX32(4);
	}
	if (fxsize.x <= 0)
	{
		fxsize.x = FX32(4);
	}
	const b2Vec2 b2size = b2PixelsToMeters(fxsize);

	b2Vec2 pos = b2GetGameObjectPosition(id);
	bodyDef.position.Set(pos.x, pos.y - b2size.y); // center

	c->mTexureOffset = b2Vec2(pos.x - bodyDef.position.x, pos.y - bodyDef.position.y);

	// body (main)
	b2PolygonShape polygonShape;
	polygonShape.SetAsBox(b2size.x * 0.5f, b2size.y * 0.5f);
	c->mpBodyB = mpWorld->CreateBody(&bodyDef);
	c->mpPhysicsFixtureB = c->mpBodyB->CreateFixture(&polygonShape, 1.0f);
	c->mpPhysicsFixtureB->SetUserData(c); // mark fixture

	// body circle (feet)
	b2CircleShape circleShape;
	circleShape.m_radius = b2size.x * 0.5f;
	bodyDef.position.Set(pos.x, pos.y - b2size.y * 0.5f);  // center
	c->mpBodyC = mpWorld->CreateBody(&bodyDef);

	b2FixtureDef circleFixtureDef;
	circleFixtureDef.shape = &circleShape;
	circleFixtureDef.density = 1.0f;
	c->mpPhysicsFixtureC = c->mpBodyC->CreateFixture(&circleFixtureDef);
	c->mpPhysicsFixtureC->SetUserData(c); // mark fixture

	// sensor
	circleFixtureDef.isSensor = true;
	c->mpSensorFixture = c->mpBodyC->CreateFixture(&circleFixtureDef);
	c->mpSensorFixture->SetUserData(c); // mark fixture

	b2RevoluteJointDef jointDef;
	jointDef.bodyA = c->mpBodyB;
	jointDef.bodyB = c->mpBodyC;
	jointDef.localAnchorA.Set(0.0f, b2size.y * 0.5f);
	jointDef.localAnchorB.Set(0.0f, 0.0f);
	c->mpJoint = mpWorld->CreateJoint(&jointDef);
}

const float32 B2BCharacterManager::CalculateVerticalVelocityForHeight(const float32& desiredHeight) const
{
	if (desiredHeight <= 0.0f)
	{
		return 0.0f; //wanna go down? just let it drop
	}

	//gravity is given per second but we want time step values here
	float32 t = 1 / 60.0f;
	float32 stepGravity_y = t * t * -mpWorld->GetGravity().y; // m/s/s

	//quadratic equation setup (ax^2 + bx + c = 0)
	float32 a = 0.5f / stepGravity_y;
	float32 b = 0.5f;
	float32 c = desiredHeight;

	//check both possible solutions
	float32 quadraticSolution1 = (-b - b2Sqrt(b*b - 4*a*c)) / (2*a);
	float32 quadraticSolution2 = (-b + b2Sqrt(b*b - 4*a*c)) / (2*a);

	//use the one which is positive
	float32 v = quadraticSolution1;
	if (v < 0.0f)
	{
		v = quadraticSolution2;
	}

	//convert answer back to seconds
	return v * 60.0f;
}

void B2BCharacterManager::Update(const s32& ms)
{
	mMs = ms;
}

void B2BCharacterManager::SetCommand(s32 state, const s32& id)
{
	B2BCharacter& c = *mMap.at(id);

	const b2Vec2& vel = c.mpBodyB->GetLinearVelocity();

	bool camMoveLeftOrRight = c.IsStayOnSomething() || c.mAllContacts == 0;

	const s32 move = state & (CC_LEFT | CC_RIGHT);
	if (move != 0 && camMoveLeftOrRight)
	{
		float32 desiredVel = 0.0f;
		switch (move)
		{
			case CC_LEFT:  desiredVel = b2Max(vel.x - 0.5f, -5.0f); break;
			case CC_RIGHT: desiredVel = b2Min(vel.x + 0.5f, 5.0f); break;
		}
		c.mpBodyB->SetLinearVelocity(b2Vec2(desiredVel, vel.y));
		c.mpBodyC->SetLinearVelocity(b2Vec2(desiredVel, vel.y));
	}
	else
	{
		c.mpBodyB->SetLinearVelocity(b2Vec2(0.0f, vel.y));
		c.mpBodyC->SetLinearVelocity(b2Vec2(0.0f, vel.y));
	}

	//update player jump
	if (c.IsStayOnSomething() && (state & CC_JUMP))
	{
		const float32& jumpVel = CalculateVerticalVelocityForHeight(3.0f); // for 60fps
		c.mpBodyB->SetLinearVelocity(b2Vec2(vel.x, -jumpVel));
		c.mpBodyC->SetLinearVelocity(b2Vec2(vel.x, -jumpVel));
	}
}
