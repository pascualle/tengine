#pragma once

#include "classes/GameLayer.hpp"
#include "classes/B2DWorld.hpp"
#include "classes/B2DCharacter.hpp"
#include "classes/B2DPlatform.hpp"
#include "classes/B2DObstacle.hpp"
#include <Box2D/Box2D.h>

#define B2D_DEBUG_DRAW

#ifdef B2D_DEBUG_DRAW
class B2DDebugDraw;
#endif

class MainLayer : public GameLayer
{
 public:
	MainLayer(Game* game);
	virtual ~MainLayer();

	virtual void OnLayerRelease();
	virtual void OnLayerLoad();
	virtual void OnBeginUpdate(const s32& ms);
	virtual void OnDrawBackgroundTiles(const s32& ms);
	virtual void OnDrawObject(const s32& ms, const s32& id, BOOL* const opDraw);

private:

#ifdef B2D_DEBUG_DRAW
	B2DDebugDraw* mpDebugDraw;

	b2Body* mpGroundBody;
	b2MouseJoint* mpMmouseJoint;
	b2Vec2 mMouseWorld;

	void DebugMouseUp();
	void DebugMouseDown(const b2Vec2& p);
	void DebugMouseMove(const b2Vec2& p);
#endif

	s32* mpCameraObj;
	s32* mpHero;

	s32 mKeyState;

	b2World *mpWorld;

	B2BWorld mPhysics;
	B2BPlatformManager mPlatformManager;
	B2BCharacterManager mCharManager;
	B2BObstacleManager mObstacleManager;

	fxVec2 mSwitchCameraPos;
	BOOL mSwitchCamera;
};