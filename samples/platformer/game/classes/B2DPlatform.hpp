#pragma once

#include "classes/B2DObject.hpp"

class B2BPlatform : public B2BObject
{
	friend class B2BPlatformManager;
	friend class B2BWorld;

public:
	B2BPlatform()
		: mpBody(nullptr)
	{
		mType = OT_Platform;
	}

	virtual void Release(b2World* world) override
	{
		SDK_ASSERT(world != nullptr);
		if (mpBody != nullptr)
		{
			world->DestroyBody(mpBody);
			mpBody = nullptr;
		}
	}

private:

	b2Fixture* mpFixture;
	b2Body* mpBody;
	b2Vec2 mTexureOffset;
};


class B2BPlatformManager : public B2BObjectManager<B2BPlatform>
{
public:

	enum Command
	{
		CC_IDLE = 0x0,
		CC_LEFT = 0x1,
		CC_RIGHT = 0x2
	};

	void Init(b2World* world);

	virtual void OnDraw(const s32& id) const override;
};