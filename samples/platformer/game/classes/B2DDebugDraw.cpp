#include "classes/Game.hpp"
#include "classes/B2DDebugDraw.hpp"
#include "fxmath.h"

//-------------------------------------------------------------------------------------------

void B2DDebugDraw::DrawPolygon(const b2Vec2* vertices, int32 vertexCount, const b2Color& color)
{
	if(vertexCount > 1)
	{
		fxVec2 pos1, pos2;
		setColor(GX_RGBA(COLOR_TO_5BCOLOR(color.r), COLOR_TO_5BCOLOR(color.g), COLOR_TO_5BCOLOR(color.b), 1));
		pos1 = b2MetersToPixels(vertices[0]);
		for(int32 i = 1; i < vertexCount; i++)
		{
			pos2 = b2MetersToPixels(vertices[i]);
			drawLine(pos1.x, pos1.y, pos2.x, pos2.y);
			pos1 = pos2;
		}
		pos2 = b2MetersToPixels(vertices[0]);
		drawLine(pos1.x, pos1.y, pos2.x, pos2.y);
	}
}
//-------------------------------------------------------------------------------------------

void B2DDebugDraw::DrawSolidPolygon(const b2Vec2* vertices, int32 vertexCount, const b2Color& color)
{
	DrawPolygon(vertices, vertexCount, color);
}
//-------------------------------------------------------------------------------------------

void B2DDebugDraw::DrawCircle(const b2Vec2& center, float32 radius, const b2Color& color)
{
	fx32 xaf, yaf, xa, ya, step, xa_last, ya_last, a, sin, cos, r;
	const s32 vert = 16;
	const fxVec2 pos0 = b2MetersToPixels(center);
	setColor(GX_RGBA(COLOR_TO_5BCOLOR(color.r), COLOR_TO_5BCOLOR(color.g), COLOR_TO_5BCOLOR(color.b), 1));
	step  = FX_Div(FX32(2.0f * 3.14159265358979323846f), FX32(vert));	
	r = FX32(b2MetersToPixels(radius));
	xa_last = pos0.x + r;
	ya_last = pos0.y;
	a = 0;
	for(s32 i = 0; i < vert; ++i)
	{
		a += step;
		fxCordic(a, &sin, &cos, FX_CORDIC_MIDDLE_PRECISION);
		xaf = FX_Mul(r, cos);
		yaf = FX_Mul(r, sin);
		xa = pos0.x + xaf;
		ya = pos0.y + yaf;
		drawLine(xa_last, ya_last, xa, ya);
		xa_last = xa;
		ya_last = ya;
	}
}
//-------------------------------------------------------------------------------------------
	
void B2DDebugDraw::DrawSolidCircle(const b2Vec2& center, float32 radius, const b2Vec2& axis, const b2Color& color)
{
	(void)axis;
	DrawCircle(center, radius, color);
}
//-------------------------------------------------------------------------------------------
	
void B2DDebugDraw::DrawSegment(const b2Vec2& p1, const b2Vec2& p2, const b2Color& color)
{
	setColor(GX_RGBA(COLOR_TO_5BCOLOR(color.r), COLOR_TO_5BCOLOR(color.g), COLOR_TO_5BCOLOR(color.b), 1));
	const fxVec2 pos1 = b2MetersToPixels(p1);
	const fxVec2 pos2 = b2MetersToPixels(p2);
	drawLine(pos1.x, pos1.y, pos2.x, pos2.y);
}
//-------------------------------------------------------------------------------------------

void B2DDebugDraw::DrawTransform(const b2Transform& xf)
{
	(void)xf;
}
//-------------------------------------------------------------------------------------------

void B2DDebugDraw::DrawPoint(const b2Vec2& p, float32 size, const b2Color& color)
{
	setColor(GX_RGBA(COLOR_TO_5BCOLOR(color.r), COLOR_TO_5BCOLOR(color.g), COLOR_TO_5BCOLOR(color.b), 1));
	const fxVec2 pos = b2MetersToPixels(p);
	const fx32 size_fx = FX32(size);
	const fx32 size_half_fx = size_fx / 2;
	fillRect(pos.x - size_half_fx, pos.y - size_half_fx, size_fx, size_fx);
}
//-------------------------------------------------------------------------------------------
