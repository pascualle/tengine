#include "classes/B2DWorld.hpp"
#include "classes/B2DObjectTypes.hpp"
#include "classes/B2DHelper.hpp"

B2BWorld::B2BWorld()
: mpWorld(nullptr)
{
}

void B2BWorld::Init(b2World* world)
{
	mpWorld = world;
	world->SetContactListener(this);
}

void B2BWorld::Release()
{
	if (mpWorld != nullptr)
	{
		mpWorld->SetContactListener(nullptr);
		mpWorld = nullptr;
	}
}

void B2BWorld::BeginContact(b2Contact* contact)
{
	b2Fixture* fixtureA = contact->GetFixtureA();
	b2Fixture* fixtureB = contact->GetFixtureB();
	B2BObject* objA = static_cast<B2BObject*>(fixtureA->GetUserData());
	B2BObject* objB = static_cast<B2BObject*>(fixtureB->GetUserData());
	const bool fixtureAIsPlatform = objA != nullptr && objA->GetType() == B2BObject::Type::OT_Platform;
	const bool fixtureBIsPlatform = objB != nullptr && objB->GetType() == B2BObject::Type::OT_Platform;

	if (fixtureAIsPlatform == true && fixtureBIsPlatform == true)
	{
		contact->SetEnabled(false);
		return;
	}

	const bool fixtureAIsCharacter = objA != nullptr && objA->CanStayOnPlatform();
	const bool fixtureBIsCharacter = objB != nullptr && objB->CanStayOnPlatform();

	if (fixtureAIsCharacter == true && fixtureBIsCharacter == true && objA->PassEachOther() && objB->PassEachOther())
	{
		contact->SetEnabled(false);
		return;
	}

	if (fixtureA->IsSensor() && fixtureAIsCharacter)
	{
		static_cast<B2BCharacter*>(objA)->mFootContacts++;
	}
	if (fixtureB->IsSensor() && fixtureBIsCharacter)
	{
		static_cast<B2BCharacter*>(objB)->mFootContacts++;
	}

	if (fixtureA->IsSensor() || fixtureB->IsSensor())
	{
		contact->SetEnabled(false);
		return;
	}

	b2Fixture* platformFixture = nullptr;
	b2Fixture* otherFixture = nullptr;

	if (fixtureAIsPlatform)
	{
		platformFixture = fixtureA;
		otherFixture = fixtureB;
	}
	else if (fixtureBIsPlatform)
	{
		platformFixture = fixtureB;
		otherFixture = fixtureA;
	}

	if (platformFixture != nullptr)
	{
		B2BObject* obj = static_cast<B2BObject*>(otherFixture->GetUserData());
		if (obj != nullptr && obj->CanStayOnPlatform())
		{
			B2BMovable* movable = static_cast<B2BMovable*>(obj);
			// check contact for "feet" only
			if (movable->mpPhysicsFixtureC == otherFixture)
			{
				movable->mPlatformContacts++;

				const s32 numPoints = contact->GetManifold()->pointCount;
				b2WorldManifold worldManifold;
				contact->GetWorldManifold(&worldManifold);

				const b2Body* platformBody = platformFixture->GetBody();
				const b2Body* otherBody = otherFixture->GetBody();

				/*const b2Vec2& objPos = b2GetGameObjectPosition(movable->mId);*/

				// check if contact points are moving into platform
				bool solid = false;
				for (s32 i = 0; i < numPoints; i++)
				{
					const b2Vec2& pointVelPlatform = platformBody->GetLinearVelocityFromWorldPoint(worldManifold.points[i]);
					const b2Vec2& pointVelOther = otherBody->GetLinearVelocityFromWorldPoint(worldManifold.points[i]);
					const b2Vec2& relativeVel = platformBody->GetLocalVector(pointVelPlatform - pointVelOther);
					if (relativeVel.y < -1.0f)
					{
						// point is moving into platform, leave contact solid
						solid = true;
					}
					else if (relativeVel.y < 1.0f)
					{
						if (worldManifold.normal.y < -0.6f)
						{
							solid = true;
						}
					}
				}

				if (solid != true)
				{
					// no points are moving into platform, contact should not be solid
					contact->SetEnabled(false);
					movable->mContacts.push_back(contact);
				}
			}
			else
			{
				contact->SetEnabled(false);
			}
		}
		else
		{
			contact->SetEnabled(false);
		}
	}
	else
	{
		if (fixtureAIsCharacter)
		{
			static_cast<B2BCharacter*>(objA)->mAllContacts++;
		}
		if (fixtureBIsCharacter)
		{
			static_cast<B2BCharacter*>(objB)->mAllContacts++;
		}
	}
}

void B2BWorld::PreSolve(b2Contact* contact, const b2Manifold* oldManifold)
{
	(void)oldManifold;

	const b2Fixture* fixtureA = contact->GetFixtureA();
	const b2Fixture* fixtureB = contact->GetFixtureB();
	B2BObject* objA = static_cast<B2BObject*>(fixtureA->GetUserData());
	B2BObject* objB = static_cast<B2BObject*>(fixtureB->GetUserData());
	const bool fixtureAIsPlatform = objA != nullptr && objA->GetType() == B2BObject::Type::OT_Platform;
	const bool fixtureBIsPlatform = objB != nullptr && objB->GetType() == B2BObject::Type::OT_Platform;

	if (fixtureAIsPlatform == true && fixtureBIsPlatform == true)
	{
		contact->SetEnabled(false);
		return;
	}

	if (fixtureA->IsSensor() || fixtureB->IsSensor())
	{
		contact->SetEnabled(false);
		return;
	}

	const bool fixtureAIsCharacter = objA != nullptr && objA->CanStayOnPlatform();
	const bool fixtureBIsCharacter = objB != nullptr && objB->CanStayOnPlatform();
	if (fixtureAIsCharacter == true && fixtureBIsCharacter == true && objA->PassEachOther() && objB->PassEachOther())
	{
		contact->SetEnabled(false);
		return;
	}

	const b2Fixture* platformFixture = nullptr;
	const b2Fixture* otherFixture = nullptr;

	if (fixtureAIsPlatform)
	{
		platformFixture = fixtureA;
		otherFixture = fixtureB;
	}
	else if (fixtureBIsPlatform)
	{
		platformFixture = fixtureB;
		otherFixture = fixtureA;
	}

	if (platformFixture != nullptr)
	{
		const B2BObject* obj = static_cast<B2BObject*>(otherFixture->GetUserData());
		if (obj != nullptr && obj->CanStayOnPlatform())
		{
			const B2BMovable* movable = static_cast<const B2BMovable*>(obj);
			// check contact for "feet" only
			if (movable->mpPhysicsFixtureC == otherFixture)
			{
				for (auto& it : movable->mContacts)
				{
					if (it == contact)
					{
						contact->SetEnabled(false);
					}
				}
			}
			else
			{
				contact->SetEnabled(false);
			}
		}
		else
		{
			contact->SetEnabled(false);
		}
	}
}

void B2BWorld::EndContact(b2Contact* contact)
{
	b2Fixture* fixtureA = contact->GetFixtureA();
	b2Fixture* fixtureB = contact->GetFixtureB();
	B2BObject* objA = static_cast<B2BObject*>(fixtureA->GetUserData());
	B2BObject* objB = static_cast<B2BObject*>(fixtureB->GetUserData());
	const bool fixtureAIsCharacter = objA != nullptr && objA->CanStayOnPlatform();
	const bool fixtureBIsCharacter = objB != nullptr && objB->CanStayOnPlatform();

	if (fixtureAIsCharacter == true && fixtureBIsCharacter == true && objA->PassEachOther() && objB->PassEachOther())
	{
		contact->SetEnabled(true);
		return;
	}

	const bool fixtureAIsPlatform = objA != nullptr && objA->GetType() == B2BObject::Type::OT_Platform;
	const bool fixtureBIsPlatform = objB != nullptr && objB->GetType() == B2BObject::Type::OT_Platform;

	if (fixtureAIsPlatform == true && fixtureBIsPlatform == true)
	{
		contact->SetEnabled(true);
		return;
	}

	if (fixtureA->IsSensor() && fixtureAIsCharacter)
	{
		static_cast<B2BCharacter*>(objA)->mFootContacts--;
	}
	if (fixtureB->IsSensor() && fixtureBIsCharacter)
	{
		static_cast<B2BCharacter*>(objB)->mFootContacts--;
	}

	if (fixtureA->IsSensor() || fixtureB->IsSensor())
	{
		contact->SetEnabled(true);
		return;
	}

	b2Fixture* platformFixture = nullptr;
	b2Fixture* otherFixture = nullptr;

	if (fixtureAIsPlatform)
	{
		platformFixture = fixtureA;
		otherFixture = fixtureB;
	}
	else if (fixtureBIsPlatform)
	{
		platformFixture = fixtureB;
		otherFixture = fixtureA;
	}

	if (platformFixture != nullptr)
	{
		B2BObject* obj = static_cast<B2BObject*>(otherFixture->GetUserData());
		if (obj != nullptr && obj->CanStayOnPlatform())
		{
			B2BMovable* movable = static_cast<B2BMovable*>(obj);
			// check contact for "feet" only
			if (movable->mpPhysicsFixtureC == otherFixture)
			{
				movable->mPlatformContacts--;
				auto it = std::find(movable->mContacts.begin(), movable->mContacts.end(), contact);
				if (it != movable->mContacts.end())
				{
					movable->mContacts.erase(it);
				}
			}
		}
	}
	else
	{
		if (fixtureAIsCharacter)
		{
			static_cast<B2BCharacter*>(objA)->mAllContacts--;
		}
		if (fixtureBIsCharacter)
		{
			static_cast<B2BCharacter*>(objB)->mAllContacts--;
		}
	}

	contact->SetEnabled(true);
}
