#ifndef _GE_CONSTANTS_H_
#define _GE_CONSTANTS_H_

enum GEConstants
{

// Languages:
 LANGUAGE_ENG = 0,
 LANGUAGE_RUS = 1,

// Text strings:
 TXT_STRING1 = 0,
 TXT_STRING2 = 1,

// Game object types:
 TRIGGER = 0,
 DIRECTOR = 1,
 TEXTBOX = 2,
 BUTTON_IDLE = 5,
 BUTTON_FOCUS = 6,
 BUTTON_PRESSED = 7,
 HERO = 8,

// Properties of game object:
 PRP_ENABLE = 0,
 PRP_SPEED = 1,

// Map0 objects list
 M0_BUTTON_IDLE_2 = 0,
 M0_BUTTON_IDLE_3 = 1,
 M0_BUTTON_IDLE_4 = 2,
 M0_BUTTON_IDLE_5 = 3,
 M0_BUTTON_IDLE_6 = 4,
 M0_HERO_7 = 5,

// Map0 collide chain shapes list
 M0_COLLIDESHAPE_1 = 0,
 M0_COLLIDESHAPE_3 = 1,
 M0_COLLIDESHAPE_4 = 2,
 M0_COLLIDESHAPE_5 = 3,
 M0_COLLIDESHAPE_6 = 4,
 M0_COLLIDESHAPE_7 = 5,
 M0_COLLIDESHAPE_8 = 6,
 M0_COLLIDESHAPE_9 = 7,

// Map1 fonts list
 M1_FONT_ARIAL = 0,

// Map1 objects list
 M1_TXTBOX_FPS = 0,

// Animations: (for drawSingleFrame() function only)
 ANIM_IDLE = 0,
 ANIM_RIGHT = 1,

// States:
 STATE_IDLE = 0,
 STATE_RIGHT = 1,

// Movement :
 DIR_IDLE = 0,
 DIR_UP = 2,
 DIR_LEFT = 4,
 DIR_RIGHT = 6,
 DIR_DOWN = 8,
 DIR_COUNT = 5 

};

#endif
