#include "tengine.h"
#include "touchpad.h"
#include "gamepad.h"
#include "gamefield.h"
#include "level_id.h"
#include "sound.h"
#include "constants.h"

#include "classes/Game.hpp"

//-------------------------------------------------------------------------------------------

 enum LoadMarkers
 {
	LM_INIT = 0,
	LM_LOAD
 };

enum GameStates
{
	gstNONE = 0,
	gstDISPLAYINIT,
	gstLOADING,
	gstGAME
};

static s32 gameState = gstNONE;
static struct TouchPadData tp_data;

static Game gsGame;

static s32 resizeW = 0;
static s32 resizeH = 0;
 
//-------------------------------------------------------------------------------------------

static void _onEngineEvent(const struct EventCallbackData *pData);
static void _onBeginUpdate(u32 layer);
static void _onDrawBackgroundTiles(u32 layer);
static void _onDrawObject(u32 layer, s32 iId, BOOL* const oDraw);
static void _releaseTengineData(void);
static void _load(void);
static void _doResize(void);

//-------------------------------------------------------------------------------------------

void tfgInitMemory()
{
	InitMemoryAllocator();
}
//-------------------------------------------------------------------------------------------

void tfgInit()
{
	MI_CpuClear8(&tp_data, sizeof(struct TouchPadData));
    InitGamePad();
	InitTouchPad();
	gameState = gstNONE;
}
//-------------------------------------------------------------------------------------------

void tfgRelease()
{
	_releaseTengineData();
	sndReleaseSoundSystem();
}
//-------------------------------------------------------------------------------------------

void _releaseTengineData()
{
	gsGame.OnLayerRelease(HUD_LAYER);
	gsGame.OnLayerRelease(GAME_LAYER);
	gsGame.Release();
	releaseResources();
	releaseMapData(HUD_LAYER);
	releaseMapData(GAME_LAYER);
	releaseTextData();
	releaseCommonData();
	releaseRenderPlane(BGSELECT_SUB3);
	releaseEngine();
}
//-------------------------------------------------------------------------------------------

void tfgSetWindowModeSize(s32 *const w, s32 *const h)
{
	*w = 800;
	*h = 680;
}
//-------------------------------------------------------------------------------------------

void tfgResize(s32 w, s32 h)
{
	gameState = gstDISPLAYINIT;
	resizeW = w;
	resizeH = h;
}
//-------------------------------------------------------------------------------------------

void _doResize()
{
	struct RenderPlaneInitParams mp1;
	mp1.mBGType = BGSELECT_SUB3;
	mp1.mSizes.mViewWidth = resizeW; 
	mp1.mSizes.mViewHeight = resizeH;
	mp1.mX = 0;
	mp1.mY = 0;
#ifdef USE_OPENGL_RENDER
	mp1.mMaxRenderObjectsOnPlane = 512;
#endif
#ifdef USE_CUSTOM_RENDER
	mp1.mColorType = BMP_TYPE_DC16;
	{
		const s32 h8 = ((resizeH + 31) / 32) * 32; // must be multiple of 32 (see mapeditor tile size)
		s32 w8 = ((resizeW + 31) / 32) * 32;
#ifdef FRAME_ALLOCATOR
		// when the screen orientation changes, it is necessary to re-create a buffer (resize)
		// if defined FRAME_ALLOCATOR, we cannot recreate new buffer for easygraphics module because of assertion)
		// so the buffer must be the same for the larger size of the screen
		w8 = (w8 > h8) ? w8 : h8;
		mp1.mSizes.mFrameBufferWidth8 = (u16)w8;
		mp1.mSizes.mFrameBufferHeight8 = (u16)w8;
#else //FRAME_ALLOCATOR
		mp1.mSizes.mFrameBufferWidth8 = w8;
		mp1.mSizes.mFrameBufferHeight8 = h8;
#endif //FRAME_ALLOCATOR
	}
#endif //USE_CUSTOM_RENDER
	if(!isRenderPlaneInit(GAME_LAYER)) // any active plane, whatever
	{
		struct InitEngineParameters initparams;
		_releaseTengineData();
		
		initparams.layersCount = LAYER_COUNT;
		initparams.particlesPoolSize = 0;

		initEngine(&initparams);
		initRenderPlane(&mp1);
		
		assignLayerWithRenderPlane(GAME_LAYER, BGSELECT_SUB3, TRUE);
		assignLayerWithRenderPlane(HUD_LAYER, BGSELECT_SUB3, FALSE);

        //register callbacks
		setOnEvent(_onEngineEvent);
		setOnBeginUpdate(_onBeginUpdate);
		setOnDrawGameObject(_onDrawObject);
		setOnDrawBackgroundTiles(_onDrawBackgroundTiles);

		gsGame.Init();
		gsGame.SetTpData(&tp_data);

		//begin load task
		//parameter will return with iData.initiatorId in onEngineEvent() function ( EVENT_TYPE_ON_END_ASYNH_LOAD_DATA)
		beginLoadListAsynh(LM_INIT);
		{
			//load common data
			addToLoadListCommonData();
			//load text data for certain language
			// to avoid memory fragmentation load it once after common data
			addToLoadListLanguageData(LANGUAGE_ENG);
		}
		//end load task
		endLoadListAsynh(FALSE);

		//wait for  EVENT_TYPE_ON_END_ASYNH_LOAD_DATA in onEngineEvent() function
		gameState = gstNONE;
	}
	else
	{
		resizeRenderPlane(GAME_LAYER, &mp1.mSizes);
		gameState = gstGAME;
	}
}
//-------------------------------------------------------------------------------------------

void _load()
{
	gsGame.OnLayerRelease(HUD_LAYER);
	gsGame.OnLayerRelease(GAME_LAYER);

	// to avoid memory fragmentation, releaseMapData in opposite side
	releaseResources();
	releaseMapData(HUD_LAYER);
	releaseMapData(GAME_LAYER);
	OS_Printf("mem on newGame = %u\n", GetFreeMemorySize());
	beginLoadListAsynh(LM_LOAD);
	{
		//layer LAYER_GAME, load current level
		addToLoadListMap(GAME_LAYER, 0);
		//layer LAYER_HUD, load or reload hud
		addToLoadListMap(HUD_LAYER, 1);
	}
	endLoadListAsynh(TRUE);
	//wait for  EVENT_TYPE_ON_END_ASYNH_LOAD_DATA and EVENT_TYPE_ON_END_ASYNH_LOAD_RESOURSES in onEngineEvent() function
	gameState = gstNONE;
}
//-------------------------------------------------------------------------------------------

void tfgLostRenderDevice()
{
}
//-------------------------------------------------------------------------------------------

void tfgRestoreRenderDevice()
{
}
//------------------------------------------------------------------------------------

void tfgLowMemory()
{
}
//------------------------------------------------------------------------------------

void tfgTick(s32 ms)
{
	ReadTouchPadData(&tp_data);

	switch(gameState)
	{
		case gstNONE:
		break;

		case gstDISPLAYINIT:
			_doResize();
		break;

		case gstLOADING:
			_load();
		break;

		case gstGAME:
			gsGame.Update(ms);
	}
}
//-------------------------------------------------------------------------------------------

void _onBeginUpdate(u32 layer)
{
	gsGame.OnBeginUpdate(static_cast<GameLayers>(layer));
}
//-------------------------------------------------------------------------------------------

void _onDrawBackgroundTiles(u32 layer)
{
	gsGame.OnDrawBackgroundTiles(static_cast<GameLayers>(layer));
}
//----------------------------------------------------------------------------------

void _onDrawObject(u32 layer, s32 iId, BOOL* const oDraw)
{
	gsGame.OnDrawObject(static_cast<GameLayers>(layer), iId, oDraw);
}
//----------------------------------------------------------------------------------

void _onEngineEvent(const struct EventCallbackData *pData)
{
	switch(pData->eventType)
	{
		case  EVENT_TYPE_ON_END_ASYNH_LOAD_DATA:
			switch(pData->eventId)
			{
				case LOAD_TYPE_ENDLOADDATATASK:
					if(pData->initiatorId == LM_INIT)
					{
						gameState = gstLOADING;
					}
				break;
				case LOAD_TYPE_MAPDATA:
					if(pData->initiatorId == LM_LOAD)
					{
						gsGame.OnLayerLoad(static_cast<GameLayers>(pData->layer));
					}

				default:
				break;
			}
		break;

		case EVENT_TYPE_ON_END_ASYNH_LOAD_RESOURSES:
			setVisible(HUD_LAYER, TRUE);
			setVisible(GAME_LAYER, TRUE);
			gameState = gstGAME;

		default:
		break;
	}
}
//----------------------------------------------------------------------------------
