﻿
Resources are used from the root folder ./assets and are built using the make_res.bat utility.

Running from the command line (or command arguments if running under debug in IDE):

win32app.exe [-w] [-h] [-p]
-w window width (default 512)
-h window height (default 512)
-p path to resources (if -p is not specified, resources are expected in assets\data in the folder with win32app.exe)
example: win32app.exe -w512 -h512 -p../../assets/data/

Control keys: Ctrl, Shift, Enter, Arrow keys

Special keys:

F11 - lost/restore render context
F9 - portrait/landscape views (resize)
F7 - low memory signal
Esc - exit
