#include "classes/MainLayer.hpp"
#ifdef B2D_DEBUG_DRAW
#include "classes/B2DDebugDraw.hpp"
#endif
#include "constants.h"
#include "gamepad.h"

//-------------------------------------------------------------------------------------------

static const fx32 SWITCH_CAMERA_STEP = FX32(10.0f);

//-------------------------------------------------------------------------------------------

static s32 gsDynObjName[MAX_DYNAMICS_BODIES] =
{
	M0_BUTTON_IDLE_2,
	M0_BUTTON_IDLE_3,
	M0_BUTTON_IDLE_4,
	M0_BUTTON_IDLE_5,
	M0_BUTTON_IDLE_6
};

//-------------------------------------------------------------------------------------------

MainLayer::MainLayer(Game* game)
 : GameLayer(game)
 , mSwitchCamera(FALSE)
 , mpWorld(NULL)
{
	s32 i;
	mpCameraObj = &gsDynObjName[0];
#ifdef B2D_DEBUG_DRAW
	mpDebugDraw = new B2DDebugDraw();
#endif
	for(i = 0; i < MAX_DYNAMICS_BODIES; i++)
	{
		mpBody[i] = NULL;
	}
	for(i = 0; i < MAX_STATIC_SHAPES; i++)
	{
		mpStaticShape[i] = NULL;
	}
}
//-------------------------------------------------------------------------------------------

MainLayer::~MainLayer()
{
#ifdef B2D_DEBUG_DRAW
	delete mpDebugDraw;
#endif
	SDK_ASSERT(mpWorld == NULL);
}
//-------------------------------------------------------------------------------------------

void MainLayer::DeleteB2DObjects()
{
	if(mpWorld != NULL)
	{
		s32 i = MAX_DYNAMICS_BODIES;
		while(i > 0)
		{
			i--;
			if(mpBody[i] != NULL)
			{
				mpWorld->DestroyBody(mpBody[i]);
				mpBody[i] = NULL;
			}
		}
		i = MAX_STATIC_SHAPES;
		while(i > 0)
		{
			i--;
			if(mpStaticShape[i] != NULL)
			{
				mpWorld->DestroyBody(mpStaticShape[i]);
				mpStaticShape[i] = NULL;
			}
		}
		mpWorld->DestroyBody(mpStaticBody1);
		delete mpWorld;
	}
	mpWorld = NULL;
}
//-------------------------------------------------------------------------------------------

void MainLayer::OnLayerRelease()
{
	DeleteB2DObjects();
}
//-------------------------------------------------------------------------------------------

void MainLayer::OnLayerLoad()
{
	s32 i;
	// constants.h (gererated by MapEditor3)
	for(i = 0; i < MAX_DYNAMICS_BODIES; i++)
	{
		mObjPos0[i] = b2GetGameObjectPosition(gsDynObjName[i]);
	}
	mSwitchCamera = FALSE;

	b2Vec2 gravity(0.0f, 10.0f);
	mpWorld = new b2World(gravity);

#ifdef B2D_DEBUG_DRAW
	{
		mpWorld->SetDebugDraw(mpDebugDraw);
		uint32 flags = 0;
		flags += b2Draw::e_shapeBit;
		//flags += b2Draw::e_jointBit;
		//flags += b2Draw::e_aabbBit;
		//flags += b2Draw::e_centerOfMassBit;
		mpDebugDraw->SetFlags(flags);
	}
#endif

	// Define the static bodies.
	b2BodyDef groundBodyDef;
	b2PolygonShape groundBox;
	groundBodyDef.position.Set(mObjPos0[0].x - b2PixelsToMeters(55.0f), mObjPos0[0].y + b2PixelsToMeters(300.0f));
	mpStaticBody1 = mpWorld->CreateBody(&groundBodyDef);
	groundBox.SetAsBox(b2PixelsToMeters(50.0f), b2PixelsToMeters(5.0f));
	mpStaticBody1->CreateFixture(&groundBox, 0.0f);

	b2BodyDef staticShapeBodyDef;
	staticShapeBodyDef.type = b2_staticBody;
	const s32 chainShapesCount = cldShapesCount(GAME_LAYER);
	for (i = 0; i < chainShapesCount; i++)
	{
		mpStaticShape[i] = mpWorld->CreateBody(&staticShapeBodyDef);
		const struct fxVec2* arr = cldShapeGetNodesArray(GAME_LAYER, i);
		const s32 chainShapesNodesCount = cldShapeNodesCount(GAME_LAYER, i);
		b2Vec2 *b2arr = (b2Vec2*)MALLOC(sizeof(b2Vec2) * chainShapesNodesCount, "OnLayerLoad:b2Vec2 arr");
		for (s32 j = 0; j < chainShapesNodesCount; j++)
		{
			b2arr[j] = b2PixelsToMeters(arr[j]);
		}
		b2ChainShape chainShape;
		chainShape.CreateChain(b2arr, chainShapesNodesCount);
		FREE(b2arr);
		mpStaticShape[i]->CreateFixture(&chainShape, 0.0f);
		chainShape.Clear();
	}

	// Define the dynamic body. We set its position and call the body factory.
	b2BodyDef bodyDef;
	bodyDef.type = b2_dynamicBody;

	b2CircleShape circleShape;
	circleShape.m_p.Set(0.0f, 0.0f);
	circleShape.m_radius = b2PixelsToMeters(32.0f);

	b2FixtureDef fixtureDef;
	fixtureDef.shape = &circleShape;
	// Set the box density to be non-zero, so it will be dynamic.
	fixtureDef.density = 1.0f;
	// Override the default friction.
	fixtureDef.friction = 0.3f;

	for(i = 0; i < MAX_DYNAMICS_BODIES; i++)
	{
		bodyDef.position.Set(mObjPos0[i].x, mObjPos0[i].y);
		mpBody[i] = mpWorld->CreateBody(&bodyDef);
		// Add the shape to the body.
		mpBody[i]->CreateFixture(&fixtureDef);
	}
}
//-------------------------------------------------------------------------------------------

void MainLayer::OnBeginUpdate(const s32& ms)
{
	(void)ms;
	const float32 timeStep = static_cast<float32>(ms) / 1000.0f;
	const int32 velocityIterations = 6;
	const int32 positionIterations = 2;
	mpWorld->Step(timeStep, velocityIterations, positionIterations);

	fxVec2 opos = prGetPosition(*mpCameraObj);
	if(mSwitchCamera == TRUE)
	{
		mSwitchCamera = nextPointAtLine(mSwitchCameraPos, opos, SWITCH_CAMERA_STEP, &mSwitchCameraPos);
		if(mSwitchCamera == TRUE)
		{
			opos = mSwitchCameraPos;
		}
	}

	setCamera(opos);

	UpdateGamePad();
	if(IsButtonDown(TYPE_START))
	{
		mSwitchCameraPos = prGetPosition(*mpCameraObj);
		mSwitchCamera = TRUE;
		for(s32 i = 0; i < MAX_DYNAMICS_BODIES; i++)
		{
			mpBody[i]->SetTransform(mObjPos0[i], 0);
			mpBody[i]->SetLinearVelocity(b2Vec2(0, 0));
			mpBody[i]->SetAngularVelocity(0);
			mpBody[i]->SetAwake(true);
		}
	}
	else
	if(IsButtonDown(TYPE_A))
	{
		mSwitchCameraPos = prGetPosition(*mpCameraObj);
		mSwitchCamera = TRUE;
		if(mpCameraObj == &gsDynObjName[MAX_DYNAMICS_BODIES - 1])
		{
			mpCameraObj = &gsDynObjName[0];
		}
		mpCameraObj++;
	}
}
//-------------------------------------------------------------------------------------------

void MainLayer::OnDrawBackgroundTiles(const s32& ms)
{
	(void)ms;
#ifdef B2D_DEBUG_DRAW
	mpWorld->DrawDebugData();
#endif
}
//-------------------------------------------------------------------------------------------

void MainLayer::OnDrawObject(const s32& ms, const s32& id, BOOL* const opDraw)
{
	(void)ms;
	(void)opDraw;
	if(prGetGroupIdx(id) == BUTTON_IDLE)
	{
		const b2Vec2& position = mpBody[id]->GetPosition();
		const float32& angle_rad = mpBody[id]->GetAngle();
		b2SetGameObjectPosition(id, position);
		b2SetGameObjectRotation(id, angle_rad);
	}
}
//-------------------------------------------------------------------------------------------
