#include "classes/Game.hpp"	
#include "classes/HudLayer.hpp"	
#include "classes/MainLayer.hpp"	
#include "constants.h"

//-------------------------------------------------------------------------------------------

Game::Game()
 : mCurrentMS(0)
{
	for(s32 i = 0; i < LAYER_COUNT; i++)
	{
		mLayer[i] = NULL;
	}
}
//-------------------------------------------------------------------------------------------

void Game::Init()
{
	if(mLayer[HUD_LAYER] == NULL)
	{
		mLayer[HUD_LAYER] = new HudLayer(this);
		mLayer[GAME_LAYER] = new MainLayer(this);
	}
}
//-------------------------------------------------------------------------------------------

void Game::Release()
{
	if(mLayer[HUD_LAYER] != NULL)
	{
		delete mLayer[HUD_LAYER];
		delete mLayer[GAME_LAYER];
	}
	for(s32 i = 0; i < LAYER_COUNT; i++)
	{
		mLayer[i] = NULL;
	}
}
//-------------------------------------------------------------------------------------------

void Game::Update(const s32& ms)
{
	mCurrentMS = ms;
}
//-------------------------------------------------------------------------------------------

void Game::OnLayerRelease(const GameLayers& layer)
{
	if(mLayer[layer] != NULL)
	{
		mLayer[layer]->OnLayerRelease();
	}
}
//-------------------------------------------------------------------------------------------

void Game::OnLayerLoad(const GameLayers& layer)
{
	SDK_ASSERT(mLayer[layer]);
	mLayer[layer]->OnLayerLoad();
}
//-------------------------------------------------------------------------------------------

void Game::OnBeginUpdate(const GameLayers& layer)
{
	(void)layer;
	SDK_ASSERT(mLayer[layer]);
	mLayer[layer]->OnBeginUpdate(mCurrentMS);
}
//-------------------------------------------------------------------------------------------

void Game::OnDrawBackgroundTiles(const GameLayers& layer)
{
	(void)layer;
	SDK_ASSERT(mLayer[layer]);
	mLayer[layer]->OnDrawBackgroundTiles(mCurrentMS);
}
//-------------------------------------------------------------------------------------------

void Game::OnDrawObject(const GameLayers& layer, const u32& id, BOOL* const opDraw)
{
	SDK_ASSERT(mLayer[layer]);
	mLayer[layer]->OnDrawObject(mCurrentMS, id, opDraw);
}
//-------------------------------------------------------------------------------------------
