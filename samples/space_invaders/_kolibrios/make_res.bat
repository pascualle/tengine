@echo off

echo ------------------------------
echo Generate map (MapEditor) files
echo Please verify correct pathes in file ..\mapeditor\editor.ini
echo By default editor.ini contain paths from C disk
echo ------------------------------

set CURRENT_DIR=%~dp0
if EXIST %CURRENT_DIR%\data\o del %CURRENT_DIR%\data\o 
..\..\..\tools\editor\MapEditor3.exe -wchar_size2 -p%CURRENT_DIR%\..\mapeditor\map.mpf -i%CURRENT_DIR%\..\mapeditor\ -o%CURRENT_DIR%\data\ -l%CURRENT_DIR%\log.txt 

echo MapEditor report %CURRENT_DIR%\log.txt
type %CURRENT_DIR%\log.txt

if NOT EXIST %CURRENT_DIR%\data\o (
echo .
echo ------------------------------
echo Error: generated files not found
echo Please check pathes in file mapeditor\editor.ini
echo and generate map-files with MapEditor:
echo - open ..\mapeditor\*.mpf file with ..\..\tools\editor\MapEditor3.exe
echo - go to main menu [File/Save map] and browse to folder %CURRENT_DIR%/data
echo ------------------------------
pause
goto END
)

echo .
echo ------------------------------
echo convert graphics res 
echo ------------------------------

@echo on
for %%i in (..\\rawres\*.png) do ..\..\..\tools\bmpcvtr.exe %%i -bgr -o%CURRENT_DIR%/data
@echo off

copy ..\rawres\arial_28.fnt %CURRENT_DIR%\data\
copy ..\rawres\system.fnt %CURRENT_DIR%\data\
copy ..\rawres\*.wav %CURRENT_DIR%\data\

)

:END
