#include "tengine.h"
#include "texts.h"
#include "touchpad.h"
#include "gamepad.h"
#include "gamefield.h"
#include "constants.h"
#include "fxmath.h"

#define SHOW_NUKLEAR_OVERVIEW_DEMO

#ifdef SHOW_NUKLEAR_OVERVIEW_DEMO
/* only for overview.c */
#include <math.h>
#include <string.h>
#include <limits.h>
#include <time.h>
#endif

#define NK_INCLUDE_FIXED_TYPES
#define NK_INCLUDE_STANDARD_VARARGS
#define NK_INCLUDE_DEFAULT_ALLOCATOR
#define NK_INCLUDE_VERTEX_BUFFER_OUTPUT
#define NK_INCLUDE_DRAW_VERTEX_CUSTOM_FUNCTION
#define NK_INCLUDE_DRAW_TEXT_CUSTOM_FUNCTION
#define NK_INCLUDE_DISABLE_KEYBOARD_EDIT 
#define NK_INCLUDE_UNICODE_SUPPORT
#if defined ANDROID_NDK || defined IOS_APP
#define NK_INCLUDE_TOUCHPAD_MODE
#endif
#define NK_IMPLEMENTATION
#define NK_TENGINE_IMPLEMENTATION

// please download nuklear.h file from https://pascualle@bitbucket.org/pascualle/nuklear.git
#include "nuklear/nuklear.h"

#include "nuklear_helper.h"

//-------------------------------------------------------------------------------------------

#ifdef SHOW_NUKLEAR_OVERVIEW_DEMO
/* only for overview.c */
#define UNUSED(a) (void)a
#define MIN(a,b) ((a) < (b) ? (a) : (b))
#define MAX(a,b) ((a) < (b) ? (b) : (a))
#define LEN(a) (sizeof(a)/sizeof(a)[0])
#endif

enum GameState
{
	gstNONE = 0,
	gstFORCEDISPLAYINIT,
	gstDISPLAYINIT,
	gstPREPARE_LOADING,
	gstLOADING,
	gstGAME
};

enum LoadMarkers
{
	LM_INIT = 0,
	LM_LOAD
};

enum GameLayers
{
	GAME_LAYER = 0,
	HUD_LAYER = 1,
	LAYER_COUNT
};


static enum GameState gameState = gstNONE;
static s32 prepare_loading_draw_frame_count = 0;
static s32 resizeW = 0;
static s32 resizeH = 0;

static float speed_slider = 5.0f;
static float rotation_slider = 0.0f;
static float scale_slider = 1.0f;
static BOOL overview_first_time;

static TouchPadData tp_data;
struct nk_context *main_ctx = NULL;
struct nk_user_font main_font;

//-------------------------------------------------------------------------------------------

static void onEngineEvent(const struct EventCallbackData *pData);
static void onEndUpdate(u32 layer);
static void onDrawObject(u32 layer, s32 iId, BOOL* const oDraw);
static void releaseTengineData(void);
static void releaseGameResources(void);
static void load(void);
static void doResize(void);
static void onLevelGameLoad(void);
static void onHudResourcesLoad(void);

#ifdef SHOW_NUKLEAR_OVERVIEW_DEMO
#include "overview.c"
#endif

//-------------------------------------------------------------------------------------------

void tfgInitMemory()
{
	MI_CpuClear8(&tp_data, sizeof(struct TouchPadData));
	InitMemoryAllocator();
}
//-------------------------------------------------------------------------------------------

void tfgInit()
{
    InitGamePad();
	InitTouchPad();
	gameState = gstNONE;
}
//-------------------------------------------------------------------------------------------

void tfgRelease()
{
	releaseTengineData();
}
//-------------------------------------------------------------------------------------------

void tfgSetWindowModeSize(s32 *const w, s32 *const h)
{
	*w = 800;
	*h = 600;
}
//-------------------------------------------------------------------------------------------

void tfgResize(s32 w, s32 h)
{
	gameState = gstDISPLAYINIT;
	resizeW = w;
	resizeH = h;
}
//-------------------------------------------------------------------------------------------

static void doResize()
{
#ifdef USE_OPENGL_RENDER
	const s32 MAX_HUD_RENDER_OBJ_ON_SCENE = 4000; // nuklear uses primitives to draw hud elements
#endif
	const s32 MAX_RENDER_PARTICLES_ON_SCENE = 0;

	struct RenderPlaneInitParams rp_main;

	initRenderPlaneParametersWithDefaultValues(&rp_main);
	rp_main.mBGType = BGSELECT_SUB3;
	rp_main.mSizes.mViewWidth = resizeW;
	rp_main.mSizes.mViewHeight = resizeH;

#ifdef USE_OPENGL_RENDER
	// max objs estimation on scene: 4000 obj and 0 particles
	rp_main.mMaxRenderObjectsOnPlane = MAX_HUD_RENDER_OBJ_ON_SCENE + MAX_RENDER_PARTICLES_ON_SCENE;
#endif

	if(!isRenderPlaneInit(GAME_LAYER)) // any active plane, whatever
	{
		struct InitEngineParameters params;

		releaseTengineData();

		initEngineParametersWithDefaultValues(&params);
		params.layersCount = LAYER_COUNT;
		params.particlesPoolSize = MAX_RENDER_PARTICLES_ON_SCENE;

		initEngine(&params);
		initRenderPlane(&rp_main);

		assignLayerWithRenderPlane(GAME_LAYER, BGSELECT_SUB3, TRUE);
		assignLayerWithRenderPlane(HUD_LAYER, BGSELECT_SUB3, FALSE);
#if defined ANDROID_NDK || defined IOS_APP
		setRenderPlaneScale(FX32(2), HUD_LAYER);
#endif

		//register callbacks
		setOnEvent(onEngineEvent);
		setOnDrawGameObject(onDrawObject);
		setOnFinishUpdate(onEndUpdate);

		//begin load task
		//parameter will return with iData.initiatorId in onEngineEvent() function ( EVENT_TYPE_ON_END_ASYNH_LOAD_DATA)
		beginLoadListAsynh(LM_INIT);
		{
			//load common data
			addToLoadListCommonData();
			//load text data for certain language
			// to avoid memory fragmentation load it once after common data
			//addToLoadListLanguageData(LANGUAGE_ENG);
		}
		//end load task
		endLoadListAsynh(FALSE);

		//wait for EVENT_TYPE_ON_END_ASYNH_LOAD_DATA in onEngineEvent() function
		gameState = gstNONE;
	}
	else
	{
		resizeRenderPlane(GAME_LAYER, &rp_main.mSizes);
		gameState = gstGAME;
	}

	prepare_loading_draw_frame_count = 0;

	overview_first_time = TRUE;
}
//-------------------------------------------------------------------------------------------

void releaseTengineData()
{
	// to avoid memory fragmentation, releaseMapData in opposite side
	releaseGameResources();
	releaseTextData();
	releaseCommonData();
	releaseRenderPlane(BGSELECT_SUB3);
	releaseEngine();
}
//-------------------------------------------------------------------------------------------

void releaseGameResources()
{
	nk_tengine_shutdown();
	releaseResources();
	releaseMapData(HUD_LAYER);
	releaseMapData(GAME_LAYER);
	ResetTouchPad();
}
//-------------------------------------------------------------------------------------------

void load()
{
	releaseGameResources();

	beginLoadListAsynh(LM_LOAD);
	{
		//layer LAYER_HUD, load current level
		addToLoadListMap(GAME_LAYER, 0); // i.e. l0 map file in assets/data folder
		//layer LAYER_GAME, load current level
		addToLoadListMap(HUD_LAYER, 1); // i.e. l1 map file in assets/data folder
	}
	endLoadListAsynh(TRUE);
	
	//wait for  EVENT_TYPE_ON_END_ASYNH_LOAD_DATA and EVENT_TYPE_ON_END_ASYNH_LOAD_RESOURSES in onEngineEvent() function
	gameState = gstNONE;
}
//-------------------------------------------------------------------------------------------

// file index 1, layer GAME_LAYER
void onLevelGameLoad()
{
	struct fxVec2 scale;
	scale.x = FX32(scale_slider);
	scale.y = scale.x;
	prSetProperty(M0_OBJ_3, PRP_SPEED, FX32(speed_slider));
	prSetCustomRotation(M0_OBJ_3, FX32(rotation_slider));
	prSetCustomScale(M0_OBJ_3, scale);
}
//-------------------------------------------------------------------------------------------

void onHudResourcesLoad()
{
	if(nk_is_tengine_init() == FALSE)
	{
		struct nk_tengine_font font;
		font.font_id = M1_FONT_ARIAL;
		font.font_scale = FX32_ONE;
		font.pixel_accurate = TRUE;
		font.bg_color = GX_RGBA(0, 0, 0, 0);
		main_ctx = nk_tengine_init(&font);
	}
}
//-------------------------------------------------------------------------------------------

void tfgLostRenderDevice()
{
}
//-------------------------------------------------------------------------------------------

void tfgRestoreRenderDevice()
{
}
//------------------------------------------------------------------------------------

void tfgLowMemory()
{	
}
//-------------------------------------------------------------------------------------------

void tfgTick(s32 ms)
{
	(void)ms;
	switch(gameState)
	{
		case gstNONE:
		break;

		case gstFORCEDISPLAYINIT:
			releaseTengineData();

		case gstDISPLAYINIT:
			doResize();
		break;

		case gstPREPARE_LOADING:
			// To avoid flickering then loading: 
			//   call drawWithoutInternalLogicUpdateMode() function
			//   and skip one or two ticks
			drawWithoutInternalLogicUpdateMode();
			if(++prepare_loading_draw_frame_count > 2)
			{
				gameState = gstLOADING;
			}
		break;

		case gstLOADING:
			prepare_loading_draw_frame_count = 0;
			load();
		break;

		case gstGAME:
			break;
	}
}
//-------------------------------------------------------------------------------------------

void onDrawObject(u32 layer, s32 iId, BOOL* const oDraw)
{
	(void)oDraw;
	switch(layer)
	{
	case GAME_LAYER:
		switch(iId)
		{
		case M0_OBJ_3:
		{
			struct fxVec2 scale;
			scale.x = FX32(scale_slider);
			scale.y = scale.x;
			prSetProperty(iId, PRP_SPEED, FX32(speed_slider));
			prSetCustomRotation(iId, FX32(rotation_slider));
			prSetCustomScale(iId, scale);
		}
		default:
			break;
		}
	default:
		break;
	}
}
//----------------------------------------------------------------------------------

void onEndUpdate(u32 layer)
{
	switch(layer)
	{
	case HUD_LAYER:
		if(gameState == gstGAME && getVisible(layer) == TRUE)
			//if(nk_window_is_closed(ctx, "Demo") == FALSE)
		{
			const float panel_width = 230.0f;
			const float panel_x = ((float)resizeW) / (float)fx2int(getRenderPlaneScale(layer)) - panel_width - 10.0f;
			if(nk_begin(main_ctx, NK_T("Options"), nk_rect(panel_x, 50.0f, panel_width, 155.0f),
						NK_WINDOW_BORDER | NK_WINDOW_MOVABLE |
						NK_WINDOW_MINIMIZABLE | NK_WINDOW_TITLE))
			{
				static const float ratio[] = {40, 160};
				nk_layout_row(main_ctx, NK_STATIC, 30, 2, ratio);
				nk_label(main_ctx, NK_T("speed:"), NK_TEXT_LEFT);
				nk_slider_float(main_ctx, 0.0f, &speed_slider, 15.0, 0.5f);
				nk_label(main_ctx, NK_T("rotation:"), NK_TEXT_LEFT);
				nk_slider_float(main_ctx, 0.0f, &rotation_slider, 360.f, 0.5f);
				nk_label(main_ctx, NK_T("scale:"), NK_TEXT_LEFT);
				nk_slider_float(main_ctx, 0.2f, &scale_slider, 5.0, 0.2f);
			}
			nk_end(main_ctx);
#ifdef SHOW_NUKLEAR_OVERVIEW_DEMO
			overview(main_ctx);
			if(overview_first_time == TRUE)
			{
				nk_window_collapse(main_ctx, NK_T("Overview"), NK_MINIMIZED);
				overview_first_time = FALSE;
			}
#endif
			nk_tengine_render(layer, NK_ANTI_ALIASING_ON);

			UpdateGamePad();
			ReadTouchPadData(&tp_data);
			nk_input_begin(main_ctx);
			{
				s32 x, y;
				x = fx2int(FX_Div(FX32(tp_data.point[0].mX), getRenderPlaneScale(layer)));
				y = fx2int(FX_Div(FX32(tp_data.point[0].mY), getRenderPlaneScale(layer)));
				nk_input_motion(main_ctx, x, y);
				if(tp_data.point[0].mTrg)
				{
					nk_input_button(main_ctx, NK_BUTTON_LEFT, x, y, 1);
				}
				if(tp_data.point[0].mRls)
				{
					nk_input_button(main_ctx, NK_BUTTON_LEFT, x, y, 0);
				}
			}
			nk_input_end(main_ctx);
		}
		break;

	case GAME_LAYER:
		setCamera(prGetPosition(M0_OBJ_3));
	}
}
//----------------------------------------------------------------------------------

//CALLBACK FUNCTIONS

//----------------------------------------------------------------------------------

static void onEngineEvent(const struct EventCallbackData *pData)
{	
	switch(pData->eventType)
	{
		case EVENT_TYPE_ON_ANIMATION:
		case EVENT_TYPE_ON_DIRECTOR:
        break;
    
		case  EVENT_TYPE_ON_END_ASYNH_LOAD_DATA:
			switch(pData->eventId)
			{
				case LOAD_TYPE_ENDLOADDATATASK:
					if(pData->initiatorId == LM_INIT)
					{
						gameState = gstPREPARE_LOADING;
					}
				break;
				case LOAD_TYPE_MAPDATA:
					if(pData->initiatorId == LM_LOAD)
					{
						switch(pData->layer)
						{
							case GAME_LAYER:
								onLevelGameLoad();
							break;
							default:
								break;
						}
					}
				default:
					break;
			}
		break;
		
		case EVENT_TYPE_ON_END_ASYNH_LOAD_RESOURSES:
			onHudResourcesLoad();
			gameState = gstGAME;

		default:
			break;
	}
}
//----------------------------------------------------------------------------------
