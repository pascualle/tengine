@echo off

rem --------------------------------------------------------

set TARGET=nuklear_gui.html

set RES_PATH=./assets

set CFLAGS=-I./../game ^
		-I./../../../src/tengine ^
		-I./../../../src/tengine/lib ^
		-I./../../../src/tengine/containers ^
		-I./../../../src/gamefield ^
		-I./../ ^
		-I./../../../src/_emscripten ^
		-Wall -Wextra

set SRC=./../../../src/_emscripten/app_web.c ^
		./../game/gamefield.c ^
		./../../../src/tengine/lib/stdio_filesystem.c ^
		./../../../src/tengine/lib/fxmath.c ^
		./../../../src/tengine/lib/gamepad.c ^
		./../../../src/tengine/lib/gx_helpers.c ^
		./../../../src/tengine/lib/hash.c ^
		./../../../src/tengine/lib/jobs.c ^
		./../../../src/tengine/lib/texts.c ^
		./../../../src/tengine/lib/loadhelpers.c ^
		./../../../src/tengine/lib/loadtdata.c ^
		./../../../src/tengine/lib/memory.c ^
		./../../../src/tengine/lib/network.c ^
		./../../../src/tengine/lib/platform.c ^
		./../../../src/tengine/lib/render2dgl2.c ^
		./../../../src/tengine/lib/serialization.c ^
		./../../../src/tengine/lib/sound.c ^
		./../../../src/tengine/lib/tengine.c ^
		./../../../src/tengine/lib/tengine_pr.c ^
		./../../../src/tengine/lib/touchpad.c ^
		./../../../src/tengine/lib/static_allocator.c ^
		./../../../src/tengine/containers/allocator_array.c ^
		./../../../src/tengine/containers/allocator_list.c ^
		./../../../src/tengine/containers/allocator_htable.c ^
		./../../../src/tengine/lib/external.c
		
set DEFS=-DUSE_EXTERNAL_API -DJOBS_IN_SINGLE_THREAD -DEMSCRIPTEN_APP -Wno-missing-field-initializers -std=c99
rem -DSDK_DEBUG

set EMSCRIPTEN_HOME=%EMSCRIPTEN_HOME%
if "%EMSCRIPTEN_HOME%"=="" (
echo ------------------------------
echo Please set EMSCRIPTEN_HOME variable
echo ------------------------------
goto END
)

set CURRENT_SESSION_EMSDK_ENV=%CURRENT_SESSION_EMSDK_ENV%
if "%CURRENT_SESSION_EMSDK_ENV%"=="INIT" (
goto SKIP_EMSDK_ENV
)
set CURRENT_SESSION_EMSDK_ENV=INIT
set PATH=%PATH%;%EMSCRIPTEN_HOME%
call %EMSCRIPTEN_HOME%\emsdk activate
set EMSCRIPTEN=%EMSDK%
if "%EMSCRIPTEN%"=="" (
echo ------------------------------
echo Something wrong with emsdk activate
echo ------------------------------
pause
goto END
)
:SKIP_EMSDK_ENV

set EMSCRIPTEN_RES=%RES_PATH%@/
if NOT EXIST %RES_PATH%/data/o (
echo .
echo ------------------------------
echo Error: generated or resources files not found
echo Please generate resources and map files with 0_make_res.bat
echo ------------------------------
pause
goto END
)

@echo Please wait...

rem for local testing:
call emcc -O3 -s USE_GLFW=3 %CFLAGS% %SRC% %DEFS% --embed-file %EMSCRIPTEN_RES% -o %TARGET% 

rem How to test: it’s best to use a local webserver. For example, Python has one built in, python -m http.server in Python 3 or python -m SimpleHTTPServer in Python 2.
rem  After doing that, you can visit http://localhost:8000/. You can also use emrun FILENAME.html (which will run a python webserver for you).

:END