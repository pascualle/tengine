#ifndef _GE_CONSTANTS_H_
#define _GE_CONSTANTS_H_

enum GEConstants
{

// Languages:
 LANGUAGE_ENG = 0,
 LANGUAGE_RUS = 1,

// Text strings:
 TXT_STRING1 = 0,
 TXT_STRING2 = 1,

// Game object types:
 TRIGGER = 0,
 DIRECTOR = 1,
 TEXTBOX = 2,

// Properties of game object:
 PRP_ENABLE = 0,
 PRP_SPEED = 1,

// Map0 objects list
 M0_TXTBOX = 0,

// Animations: (for drawSingleFrame() function only)
 ANIM_IDLE = 0,

// States:
 STATE_IDLE = 0,

// Movement :
 DIR_IDLE = 0,
 DIR_UP = 2,
 DIR_LEFT = 4,
 DIR_RIGHT = 6,
 DIR_DOWN = 8,
 DIR_COUNT = 5 

};

#endif
