#include "scalefactor.h"
#include "options.h"
#include "filereader.h"

struct WFSamplesPerPixelScaleFactor;
struct WFPixelsPerSecondScaleFactor;

typedef s32(*getSamplesPerPixelFnPtr)(struct WFScaleFactor* self, s32 sample_rate);

struct WFSamplesPerPixelScaleFactor
{
	struct WFScaleFactor* base;
	s32 samples_per_pixel;
};

struct WFPixelsPerSecondScaleFactor
{
	struct WFScaleFactor* base;
	s32 pixels_per_second;
};

static struct WFScaleFactor
{
	union
	{
		struct WFSamplesPerPixelScaleFactor spp;
		struct WFPixelsPerSecondScaleFactor pps;
	}
	scale_factor;
	getSamplesPerPixelFnPtr getSamplesPerPixelFn;
}gsScaleFactor = {0};

s32 _samplesPerPixelScaleFactor_getSamplesPerPixel(struct WFScaleFactor* self, s32 sample_rate);
s32 _pixelsPerSecondScaleFactor_getSamplesPerPixel(struct WFScaleFactor* self, s32 sample_rate);

//-------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------

s32 _samplesPerPixelScaleFactor_getSamplesPerPixel(struct WFScaleFactor* self, s32 sample_rate)
{
	(void)sample_rate;
	SDK_NULL_ASSERT(self->scale_factor.spp.base);
	return self->scale_factor.spp.samples_per_pixel;
}
//-------------------------------------------------------------------------------------------

s32 _pixelsPerSecondScaleFactor_getSamplesPerPixel(struct WFScaleFactor* self, s32 sample_rate)
{
	SDK_NULL_ASSERT(self->scale_factor.pps.base);
	SDK_ASSERT(self->scale_factor.pps.pixels_per_second > 0);
	return sample_rate / self->scale_factor.pps.pixels_per_second;
}
//-------------------------------------------------------------------------------------------

void wfReleaseScaleFactor(struct WFScaleFactor* self)
{
	SDK_NULL_ASSERT(self);
	self->scale_factor.pps.base = NULL;
	self->scale_factor.spp.base = NULL;
}
//-------------------------------------------------------------------------------------------

struct WFScaleFactor* wfInitScaleFactor(struct WFOptions* options, struct WFFileReader* file_reader)
{
	SDK_ASSERT(gsScaleFactor.scale_factor.pps.base == NULL && gsScaleFactor.scale_factor.spp.base == NULL); // call wfReleaseScaleFactor() before 
	if (options->pixels_per_second >= 0)
	{
		gsScaleFactor.scale_factor.pps.base = &gsScaleFactor;
		gsScaleFactor.scale_factor.pps.pixels_per_second = options->pixels_per_second;
		gsScaleFactor.getSamplesPerPixelFn = _pixelsPerSecondScaleFactor_getSamplesPerPixel;
	}
	else if (options->samples_per_pixel >= 0)
	{
		gsScaleFactor.scale_factor.spp.base = &gsScaleFactor;
		gsScaleFactor.scale_factor.spp.samples_per_pixel = options->samples_per_pixel;
		gsScaleFactor.getSamplesPerPixelFn = _samplesPerPixelScaleFactor_getSamplesPerPixel;
	}
	else // autosize
	{
		s32 sample_rate;
		u32 wav_data_size;
		u16 channels;
		u16 bits_per_sample;
		s32 result;
		wav_data_size = wfFileReaderGetWavDataDataSize(file_reader);
		sample_rate = wfFileReaderGetWavSamplesPerSec(file_reader);
		bits_per_sample = wfFileReaderGetWavBitsPerSample(file_reader);
		channels = wfFileReaderGetWavChannels(file_reader);

		SDK_ASSERT(wfFileReaderGetFileSize(file_reader) != 0);

		gsScaleFactor.scale_factor.pps.base = &gsScaleFactor;
		gsScaleFactor.getSamplesPerPixelFn = _pixelsPerSecondScaleFactor_getSamplesPerPixel;

		options->pixels_per_second = options->image_width / ((wav_data_size / (bits_per_sample / 8) / channels) / sample_rate);
		gsScaleFactor.scale_factor.pps.pixels_per_second = options->pixels_per_second;
		result = wfScaleFactorCalculateMaxBufferItems(&gsScaleFactor, file_reader);
		while(options->image_width < result)
		{
			options->pixels_per_second--;
			gsScaleFactor.scale_factor.pps.pixels_per_second = options->pixels_per_second;
			result = wfScaleFactorCalculateMaxBufferItems(&gsScaleFactor, file_reader);
			if(options->pixels_per_second == 1)
			{
				break;
			}
		}
	}
	return &gsScaleFactor;
}
//-------------------------------------------------------------------------------------------

s32 wfScaleFactor_GetSamplesPerPixel(struct WFScaleFactor* scale_factor, s32 sample_rate)
{
	SDK_NULL_ASSERT(scale_factor);
	return scale_factor->getSamplesPerPixelFn(scale_factor, sample_rate);
}
//-------------------------------------------------------------------------------------------

s32 wfScaleFactorCalculateMaxBufferItems(struct WFScaleFactor* scale_factor, struct WFFileReader* file_reader)
{
	s32 sample_rate;
	u32 wav_data_size;
	u16 channels;
	u16 bits_per_sample;
	wav_data_size = wfFileReaderGetWavDataDataSize(file_reader);
	sample_rate = wfFileReaderGetWavSamplesPerSec(file_reader);
	bits_per_sample = wfFileReaderGetWavBitsPerSample(file_reader);
	channels = wfFileReaderGetWavChannels(file_reader);
	
	SDK_ASSERT(wfFileReaderGetFileSize(file_reader) != 0);

	return wav_data_size / (bits_per_sample / 8) / channels / wfScaleFactor_GetSamplesPerPixel(scale_factor, sample_rate) + 1;
}
//-------------------------------------------------------------------------------------------
