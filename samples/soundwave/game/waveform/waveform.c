#include "waveform.h"
#include "options.h"
#include "scalefactor.h"
#include "buffer.h"
#include "filereader.h"
#include "generator.h"
#include "tengine.h"

struct WFOptions gsOptions = {0};
struct WFBuffer *gspBuffer = NULL;

#define WF_MIN_SCREEN_WIDTH 64

static BOOL _wfInitWaveform(const char* filename, const u8* buffer, const struct WFOptions* options, struct TxtConsole *con);

//-------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------

BOOL wfInitWaveformFromFile(const char* filename, const struct WFOptions* options, struct TxtConsole *con)
{
	return _wfInitWaveform(filename, NULL, options, con);
}
//-------------------------------------------------------------------------------------------

BOOL wfInitWaveformFromBuffer(const u8* buffer, const struct WFOptions* options, struct TxtConsole *con)
{
	return _wfInitWaveform(NULL, buffer, options, con);
}
//-------------------------------------------------------------------------------------------

BOOL _wfInitWaveform(const char* filename, const u8* buffer, const struct WFOptions* options, struct TxtConsole *con)
{
	struct WFScaleFactor *scale_factor;
	struct WFFileReader *audio_file_reader;
	struct WFGenerator *processor;

	SDK_ASSERT(filename != NULL || buffer != NULL);
	SDK_NULL_ASSERT(options);

	SDK_ASSERT(gspBuffer == NULL); // call wfReleaseWaveform() before

	gsOptions = *options;

	SDK_ASSERT(options->image_width >= WF_MIN_SCREEN_WIDTH);

	if(filename != NULL)
	{
		audio_file_reader = wfInitFileReader(filename, con);
		if(wfFileReaderGetFileSize(audio_file_reader) == 0)
		{
			wfReleaseFileReader(audio_file_reader);
			return FALSE;
		}
	}
	else
	{
		audio_file_reader = wfInitFileReaderFromBuffer(buffer, con);
		if(wfFileReaderGetFileSize(audio_file_reader) == 0)
		{
			wfReleaseFileReader(audio_file_reader);
			return FALSE;
		}
	}

	scale_factor = wfInitScaleFactor(&gsOptions, audio_file_reader);

	wfFileReaderDumpFileInfo(audio_file_reader, con);

	gspBuffer = wfInitBuffer(wfScaleFactorCalculateMaxBufferItems(scale_factor, audio_file_reader));

	if(wfFileReaderLoad(audio_file_reader) == FALSE)
	{
		wfReleaseFileReader(audio_file_reader);
		wfReleaseScaleFactor(scale_factor);
		wfReleaseWaveform();
		return FALSE;
	}
	processor = wfInitGenerator(gspBuffer, scale_factor);
	if(wfFileReaderRun(audio_file_reader, processor, con) == FALSE)
	{
		wfReleaseFileReader(audio_file_reader);
		wfReleaseScaleFactor(scale_factor);
		wfReleaseWaveform();
		return FALSE;
	}
	wfGeneratorDumpResult(processor, con);

	wfReleaseFileReader(audio_file_reader);
	wfReleaseScaleFactor(scale_factor);

	return TRUE;
}
//-------------------------------------------------------------------------------------------

void wfReleaseWaveform(void)
{
	if(gspBuffer != NULL)
	{
		wfReleaseBuffer(gspBuffer);
		gspBuffer = NULL;
	}
}
//-------------------------------------------------------------------------------------------

void wfRenderWaveform(void)
{
	s32 x, i, max_x, wave_bottom_y, max_wave_height, start_index, buffer_size;
	if(gspBuffer == NULL)
	{
		return;
	}
	// Avoid drawing over the right border
	max_x = gsOptions.image_width;
	// Avoid drawing over the top and bottom borders
	wave_bottom_y = gsOptions.image_height - 1;
	max_wave_height = gsOptions.image_height;
	start_index = 0;
	buffer_size = wfBufferGetSize(gspBuffer);
	// Avoid drawing over the left border
	x = 0;
	i = start_index;
	setColor(gsOptions.waveform_color);
	for(; x < max_x && i < buffer_size; ++i, ++x)
	{
		// convert range [-32768, 32727] to [0, 65535]
		const s32 low = wfBufferGetMinSample(gspBuffer, i) + 32768;
		const s32 high = wfBufferGetMaxSample(gspBuffer, i) + 32768;
		// scale to fit the bitmap
		const s32 low_y = wave_bottom_y - low  * max_wave_height / 65536;
		const s32 high_y = wave_bottom_y - high * max_wave_height / 65536;
		drawLine(FX32(x), FX32(low_y), FX32(x), FX32(high_y));
	}
}
//-------------------------------------------------------------------------------------------
