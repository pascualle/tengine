#ifndef WF_BUFFER_H
#define WF_BUFFER_H

#include "system.h"

#ifdef __cplusplus
extern "C" {
#endif

struct WFBuffer;

struct WFBuffer* wfInitBuffer(u32 maxSize);
void wfReleaseBuffer(struct WFBuffer* buffer);
void wfBufferClear(struct WFBuffer* buffer);
void wfBufferSetSamplesPerPixel(struct WFBuffer* buffer, s32 samples_per_pixel);
s32 wfBufferGetSamplesPerPixel(const struct WFBuffer* buffer);
void wfBufferSetSampleRate(struct WFBuffer* buffer, s32 sample_rate);
s32 wfBufferGetSize(const struct WFBuffer* buffer);
void wfBufferAppendSamples(struct WFBuffer* buffer, s16 min, s16 max);
s16 wfBufferGetMinSample(const struct WFBuffer* buffer, u32 idx);
s16 wfBufferGetMaxSample(const struct WFBuffer* buffer, u32 idx);


#ifdef __cplusplus
} /* extern "C" */
#endif

#endif