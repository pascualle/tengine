#ifndef WF_GENERATOR_H
#define WF_GENERATOR_H

#include "system.h"

#ifdef __cplusplus
extern "C" {
#endif

struct WFGenerator;
struct WFBuffer;
struct WFScaleFactor;
struct TxtConsole;

struct WFGenerator* wfInitGenerator(struct WFBuffer* input_buffer, struct WFScaleFactor* scale_factor);
BOOL wfGeneratorSetup(struct WFGenerator* generator, s32 sample_rate, s32 channels, struct TxtConsole *con);
void wfGeneratorReset(struct WFGenerator* generator);
s32 wfGeneratorGetSamplesPerPixel(struct WFGenerator* generator);
BOOL wfGeneratorProcess(struct WFGenerator* generator, const s16* input_buffer, s32 input_frame_count);
void wfGeneratorProcessDone(struct WFGenerator* generator);
void wfGeneratorDumpResult(struct WFGenerator* generator, struct TxtConsole *con);

#ifdef __cplusplus
} /* extern "C" */
#endif

#endif