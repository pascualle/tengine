#include "buffer.h"
#include "memory.h"
#include "static_allocator.h"
#include "containers/allocator_array.h"

static struct WFBuffer
{
	u8* heap;
	s32 sample_rate;
	s32 samples_per_pixel;
	struct StaticAllocator allocator;
	struct AllocatorArray data;
}gsBuffer = {0};

//-------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------

struct WFBuffer* wfInitBuffer(u32 maxSize)
{
	const u32 heapSize = AllocatorArray_CalculateHeapSize(sizeof(s16), maxSize * 2); // x2 for min and max
	SDK_ASSERT(gsBuffer.heap == NULL); // Call wfReleaseBuffer() before
	gsBuffer.heap = (u8*)MALLOC(heapSize, "wfInitBuffer:gsBuffer.heap");
	StaticAllocator_Init(&gsBuffer.allocator, gsBuffer.heap, heapSize);
	AllocatorArray_Init(&gsBuffer.data, sizeof(s16), &gsBuffer.allocator);
	return &gsBuffer;
}
//-------------------------------------------------------------------------------------------

void wfReleaseBuffer(struct WFBuffer* buffer)
{
	SDK_NULL_ASSERT(buffer);
	if(buffer->heap != NULL)
	{
		FREE(buffer->heap);
		buffer->heap = NULL;
	}
}
//-------------------------------------------------------------------------------------------

void wfBufferClear(struct WFBuffer* buffer)
{
	SDK_NULL_ASSERT(buffer);
	if(buffer->heap != NULL)
	{
		AllocatorArray_ClearFast(&buffer->data);
	}
}
//-------------------------------------------------------------------------------------------

void wfBufferSetSamplesPerPixel(struct WFBuffer* buffer, s32 samples_per_pixel)
{
	SDK_NULL_ASSERT(buffer);
	buffer->samples_per_pixel = samples_per_pixel;
}
//-------------------------------------------------------------------------------------------

s32 wfBufferGetSamplesPerPixel(const struct WFBuffer* buffer)
{
	SDK_NULL_ASSERT(buffer);
	return 	buffer->samples_per_pixel;
}
//-------------------------------------------------------------------------------------------

void wfBufferSetSampleRate(struct WFBuffer* buffer, s32 sample_rate)
{
	SDK_NULL_ASSERT(buffer);
	buffer->sample_rate = sample_rate;
}
//-------------------------------------------------------------------------------------------

s32 wfBufferGetSize(const struct WFBuffer* buffer)
{
	SDK_NULL_ASSERT(buffer);
	return (s32)AllocatorArray_Size(&buffer->data) / 2; // x2 for min and max
}
//-------------------------------------------------------------------------------------------

void wfBufferAppendSamples(struct WFBuffer* buffer, s16 min, s16 max)
{
	SDK_NULL_ASSERT(buffer);
	AllocatorArray_PushBack(&buffer->data, &min);
	AllocatorArray_PushBack(&buffer->data, &max);
}
//-------------------------------------------------------------------------------------------

s16 wfBufferGetMinSample(const struct WFBuffer* buffer, u32 idx)
{
	return *((s16*)AllocatorArray_At(&buffer->data, 2 * idx));
}
//-------------------------------------------------------------------------------------------

s16 wfBufferGetMaxSample(const struct WFBuffer* buffer, u32 idx)
{
	return *((s16*)AllocatorArray_At(&buffer->data, 2 * idx + 1));
}
//-------------------------------------------------------------------------------------------
