#ifndef WF_OPTIONS_H
#define WF_OPTIONS_H

#include "system.h"

#ifdef __cplusplus
extern "C" {
#endif

struct WFOptions
{
	s32 samples_per_pixel;
	s32 pixels_per_second;
	s32 image_width;
	s32 image_height;
	GXRgba waveform_color;
};

void wfInitOpionsByDefault(struct WFOptions* options);

#ifdef __cplusplus
} /* extern "C" */
#endif

#endif