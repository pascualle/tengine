#ifndef WF_FILEREADER_H
#define WF_FILEREADER_H

#include "system.h"

#ifdef __cplusplus
extern "C" {
#endif

struct WFGenerator;
struct WFFileReader;
struct TxtConsole;

struct WFFileReader* wfInitFileReader(const char* input_filename, struct TxtConsole *err_con);
struct WFFileReader* wfInitFileReaderFromBuffer(const u8* buffer, struct TxtConsole *err_con);
void wfReleaseFileReader(struct WFFileReader* file_reader);
u32 wfFileReaderGetFileSize(struct WFFileReader* file_reader);
u32 wfFileReaderGetWavDataDataSize(struct WFFileReader* file_reader);
u16 wfFileReaderGetWavBitsPerSample(struct WFFileReader* file_reader);
u16 wfFileReaderGetWavChannels(struct WFFileReader* file_reader);
u32 wfFileReaderGetWavSamplesPerSec(struct WFFileReader* file_reader);
BOOL wfFileReaderLoad(struct WFFileReader* file_reader);
BOOL wfFileReaderRun(struct WFFileReader* file_reader, struct WFGenerator* processor, struct TxtConsole *con);
void wfFileReaderDumpFileInfo(struct WFFileReader* file_reader, struct TxtConsole *con);

#ifdef __cplusplus
} /* extern "C" */
#endif

#endif