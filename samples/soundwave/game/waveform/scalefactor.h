#ifndef WF_SCALEFACTOR_H
#define WF_SCALEFACTOR_H

#include "system.h"

#ifdef __cplusplus
extern "C" {
#endif

struct WFOptions;
struct WFScaleFactor;
struct WFFileReader;

struct WFScaleFactor* wfInitScaleFactor(struct WFOptions* options, struct WFFileReader* file_reader);
void wfReleaseScaleFactor(struct WFScaleFactor* scale_factor);
s32 wfScaleFactor_GetSamplesPerPixel(struct WFScaleFactor* scale_factor, s32 sample_rate);
s32 wfScaleFactorCalculateMaxBufferItems(struct WFScaleFactor* scale_factor, struct WFFileReader* file_reader);

#ifdef __cplusplus
} /* extern "C" */
#endif

#endif