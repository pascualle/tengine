#include "tengine.h"
#include "texts.h"
#include "touchpad.h"
#include "gamepad.h"
#include "gamefield.h"
#include "level_id.h"
#include "constants.h"
#include "console.h"
#include "waveform/waveform.h"
#include "waveform/options.h"
#include "mic/oal_miccapture.h"

//-------------------------------------------------------------------------------------------

enum GameStates
{
	gstNONE = 0,
	gstDISPLAYINIT,
	gstLOADING,
	gstGAME
};

enum LoadMarkers
{
	LM_INIT = 0,
	LM_LOAD
};

enum GameLayers
{
	MAIN_LAYER = 0,
	LAYER_COUNT
};

static struct TxtConsole console;
static struct TouchPadData gsTPData;
static s32 gameState = gstNONE;
static s32 renderMode = gstNONE;
static s32 last_fps = 0;
static s32 resizeW = 0;
static s32 resizeH = 0;
static s32 timer = 0;
static s32 timer_refresh = 0;
static const u8* micCaptureWavData = NULL;

#define WBUF_SIZE 64
#define TIME_TO_REFRESH_RECORDING_CONSOLE 200

#define MIC_WAV_BUFFER_SECONDS 5

#define FILE_LIST_COUNT 2
const static char fileList[FILE_LIST_COUNT][64] =
{
	"11k8bitpcm.wav",
	"piano2.wav"
};
static s32 fileIndex = 0;

//-------------------------------------------------------------------------------------------

static void onEngineEvent(const struct EventCallbackData *pData);

static void onBeginUpdate(u32 layer);
static void releaseTengineData(void);
static void load(void);
static void doResize(void);
static void onMainMapLoad(void);

static void setConsoleToBottom(void);
static void createWavForm(void);
static void refreshRecTimer(void);

//-------------------------------------------------------------------------------------------

void tfgInitMemory(void)
{
	InitMemoryAllocator();
}
//-------------------------------------------------------------------------------------------

void tfgInit(void)
{
	InitGamePad();
	InitTouchPad();
	mthInitRandom();
	gameState = gstNONE;
}
//-------------------------------------------------------------------------------------------

void tfgRelease(void)
{
	releaseTengineData();
}
//-------------------------------------------------------------------------------------------

void releaseTengineData(void)
{
	micCaptureWavData = NULL;

	// to avoid memory fragmentation, releaseMapData in opposite side
	wfReleaseWaveform();
	oalMicCaptureRelease();
	releaseResources();
	releaseMapData(MAIN_LAYER);
	releaseTextData();
	releaseCommonData();
	releaseRenderPlane(BGSELECT_SUB3);
	releaseEngine();
}
//-------------------------------------------------------------------------------------------

void tfgSetWindowModeSize(s32 *const w, s32 *const h)
{
	*w = 800;
	*h = 600;
}
//-------------------------------------------------------------------------------------------

void tfgResize(s32 w, s32 h)
{
	gameState = gstDISPLAYINIT;
	resizeW = w;
	resizeH = h;
}
//-------------------------------------------------------------------------------------------

void doResize(void)
{
	struct RenderPlaneInitParams mp1;
	mp1.mBGType = BGSELECT_SUB3;
	mp1.mSizes.mViewWidth = resizeW;
	mp1.mSizes.mViewHeight = resizeH;
	mp1.mX = 0;
	mp1.mY = 0;
#ifdef USE_OPENGL_RENDER
	mp1.mMaxRenderObjectsOnPlane = 2048;
#endif
#ifdef USE_CUSTOM_RENDER
	mp1.mColorType = BMP_TYPE_DC16;
	{
		const s32 tile_size = 32; // Tile size is 32x32 (see Mapeditor)
		const s32 h8 = ((resizeH + (tile_size - 1)) / tile_size) * tile_size;
		const s32 w8 = ((resizeW + (tile_size - 1)) / tile_size) * tile_size;
		mp1.mSizes.mFrameBufferWidth8 = w8;
		mp1.mSizes.mFrameBufferHeight8 = h8;
	}
#endif

	if(!isRenderPlaneInit(MAIN_LAYER)) // any active plane, whatever
	{
		struct InitEngineParameters initparams;
		releaseTengineData();

		initparams.layersCount = LAYER_COUNT;
		initparams.particlesPoolSize = 0;

		initEngine(&initparams);
		initRenderPlane(&mp1);

		assignLayerWithRenderPlane(MAIN_LAYER, BGSELECT_SUB3, TRUE);

		//register callbacks
		setOnEvent(onEngineEvent);
		setOnBeginUpdate(onBeginUpdate);

		//begin load task
		//parameter will return with iData.initiatorId in onEngineEvent() function ( EVENT_TYPE_ON_END_ASYNH_LOAD_DATA)
		beginLoadListAsynh(LM_INIT);
		{
			//load common data
			addToLoadListCommonData();
			//load text data for certain language
			// to avoid memory fragmentation load it once after common data
			addToLoadListLanguageData(LANGUAGE_ENG);
		}
		//end load task
		endLoadListAsynh(FALSE);

		//wait for  EVENT_TYPE_ON_END_ASYNH_LOAD_DATA in onEngineEvent() function
		gameState = gstNONE;
	}
	else
	{
		resizeRenderPlane(MAIN_LAYER, &mp1.mSizes);
		setConsoleToBottom();
		gameState = gstGAME;
	}
}
//-------------------------------------------------------------------------------------------

void load(void)
{
#ifdef NITRO_SDK
	GX_SetVisiblePlane(GX_PLANEMASK_NONE);
#endif

	micCaptureWavData = NULL;

	// to avoid memory fragmentation, releaseMapData in opposite side

	wfReleaseWaveform();
	oalMicCaptureRelease();
	releaseResources();
	releaseMapData(MAIN_LAYER);

	OS_Printf("mem on newGame = %u\n", GetFreeMemorySize());

	beginLoadListAsynh(LM_LOAD);
	{
		addToLoadListMap(MAIN_LAYER, 0);
	}
	endLoadListAsynh(TRUE);

	//wait for  EVENT_TYPE_ON_END_ASYNH_LOAD_DATA and EVENT_TYPE_ON_END_ASYNH_LOAD_RESOURSES in onEngineEvent() function
	gameState = gstNONE;
}
//-------------------------------------------------------------------------------------------

void onMainMapLoad(void)
{
	txbConsoleInit(&console, MAIN_LAYER, M0_TXTBOX);
	setConsoleToBottom();
}
//-------------------------------------------------------------------------------------------

void tfgLostRenderDevice(void)
{
}
//-------------------------------------------------------------------------------------------

void tfgRestoreRenderDevice(void)
{
}
//------------------------------------------------------------------------------------

void tfgLowMemory(void)
{
}
//------------------------------------------------------------------------------------

void tfgTick(s32 ms)
{
	UpdateGamePad();
	ReadTouchPadData(&gsTPData);

	oalMicCaptureUpdate();

	if(IsButtonDown(TYPE_START))
	{
		gameState = gstLOADING;
	}

	switch(gameState)
	{
	case gstNONE:
		break;

	case gstDISPLAYINIT:
		doResize();
		break;

	case gstLOADING:
		load();
		break;

	case gstGAME:
		if(gsTPData.point[0].mRls == TRUE)
		{
			if(micCaptureWavData != NULL)	// microphone demo
			{
				oalMicCaptureStop();
				refreshRecTimer();
			}
			else							// wav files demo
			{
				fileIndex++;
				if(fileIndex == FILE_LIST_COUNT)
				{
					fileIndex = 0;
				}
			}
			createWavForm();
		}
		else if(gsTPData.point[0].mTrg == TRUE )
		{
			txbConsoleClear(&console);
			oalMicCaptureStart();
			timer_refresh = timer = 0;
		}
		
		if(gsTPData.point[0].mTouch == TRUE && micCaptureWavData != NULL)
		{
			timer += ms;
			if(timer > MIC_WAV_BUFFER_SECONDS * 1000)
			{
				timer = MIC_WAV_BUFFER_SECONDS * 1000;
			}
			timer_refresh += ms;
			if(timer_refresh >= TIME_TO_REFRESH_RECORDING_CONSOLE)
			{
				refreshRecTimer();
				timer_refresh = 0;
			}
		}
		break;

	default:
		break;
	}
}
//-------------------------------------------------------------------------------------------

void refreshRecTimer(void)
{
	txbConsoleClear(&console);
	wchar wbuf[WBUF_SIZE];
	wchar wfbuf[WBUF_SIZE];
	txbConvertAnsiToUnicode("Recording: %d ms", wfbuf, WBUF_SIZE);
	STD_WSnprintf(wbuf, WBUF_SIZE, wfbuf, timer);
	txbConsoleAdd(&console, wbuf);
}
//-------------------------------------------------------------------------------------------

void createWavForm(void)
{
	struct WFOptions opt;
	wchar wbuf[WBUF_SIZE];

	wfInitOpionsByDefault(&opt);
	txbConsoleClear(&console);

	wfReleaseWaveform();
	if(micCaptureWavData != NULL) // microphone demo
	{
		if(oalMicCaptureDataExist(micCaptureWavData) == TRUE)
		{
			opt.pixels_per_second = opt.image_width / MIC_WAV_BUFFER_SECONDS;
			wfInitWaveformFromBuffer(micCaptureWavData, &opt, &console);
		}
		txbConvertAnsiToUnicode("Tap and hold anywhere on the screen to start recording", wbuf, WBUF_SIZE);
		txbConsoleAdd(&console, wbuf);
	}
	else // wav files demo
	{
		txbConvertAnsiToUnicode("Microphone not detected, read wav data from file", wbuf, WBUF_SIZE);
		txbConsoleAdd(&console, wbuf);

		wfInitWaveformFromFile(fileList[fileIndex], &opt, &console);

		txbConvertAnsiToUnicode("Touch anywhere on the screen to change wav file", wbuf, WBUF_SIZE);
		txbConsoleAdd(&console, wbuf);
	}
}
//-------------------------------------------------------------------------------------------

void setConsoleToBottom(void)
{
	fxVec2 pos;
	const s32 vh = getViewHeight(MAIN_LAYER);
	const s32 th = txbGetHeightLayer(M0_TXTBOX, MAIN_LAYER);
	pos.x = FX32(1 + txbGetWidthLayer(M0_TXTBOX, MAIN_LAYER) / 2);
	pos.y = FX32(vh - th / 2 - 1);
	prSetPosition(M0_TXTBOX, pos);
}
//-------------------------------------------------------------------------------------------

void onBeginUpdate(u32 layer)
{
	switch(layer)
	{
	case MAIN_LAYER:
		wfRenderWaveform();
	default:
		break;
	}
}
//----------------------------------------------------------------------------------

void onEngineEvent(const struct EventCallbackData *pData)
{
	switch(pData->eventType)
	{
	case  EVENT_TYPE_ON_END_ASYNH_LOAD_DATA:
		switch(pData->eventId)
		{
		case LOAD_TYPE_ENDLOADDATATASK:
			if(pData->initiatorId == LM_INIT)
			{
				gameState = gstLOADING;
			}
			break;
		case LOAD_TYPE_MAPDATA:
			if(pData->initiatorId == LM_LOAD)
			{
				switch(pData->layer)
				{
				case MAIN_LAYER:
					onMainMapLoad();
				default:
					break;
				}
			}
		default:
			break;
		}
		break;

	case EVENT_TYPE_ON_END_ASYNH_LOAD_RESOURSES:
		setVisible(MAIN_LAYER, TRUE);
		gameState = gstGAME;
		// if micCaptureWavData == NULL - microphone init failed
		micCaptureWavData = oalMicCaptureInit(MIC_WAV_BUFFER_SECONDS);
		createWavForm();
		break;

	default:
		break;
	}
}
//----------------------------------------------------------------------------------
