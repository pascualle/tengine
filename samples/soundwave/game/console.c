#include "tengine.h"
#include "console.h"

//-------------------------------------------------------------------------------------------

static void _getLinesAndCharsCount(const struct TxtConsole *tc, s32* lines_ct, s32* chars_ct);

//----------------------------------------------------------------------------------
//----------------------------------------------------------------------------------
//----------------------------------------------------------------------------------

void txbConsoleInit(struct TxtConsole *const tc, s32 layer, s32 textbox_id)
{
	SDK_NULL_ASSERT(tc);
	tc->textBoxId = textbox_id;
	tc->layer = layer;
	SDK_ASSERT(tc->textBoxId >= 0);
	SDK_ASSERT(TEXTBOX_TYPE_DYNAMIC == txbGetTypeLayer(tc->textBoxId, tc->layer));
}
//-------------------------------------------------------------------------------------------

void _getLinesAndCharsCount(const struct TxtConsole *tc, s32* lines_ct, s32* chars_ct)
{
	SDK_ASSERT(tc->textBoxId >= 0);
	*lines_ct = txbGetDynamicTextLinesCountLayer(tc->textBoxId, tc->layer);
	if (*lines_ct > 0)
	{
		*chars_ct = txbGetDynamicTextMaximumLineCharsCountLayer(tc->textBoxId, tc->layer);
	}
}
//-------------------------------------------------------------------------------------------

void txbConsoleAdd(const struct TxtConsole *tc, const wchar *text)
{
	s32 lines_ct, chars_ct, i;
	_getLinesAndCharsCount(tc, &lines_ct, &chars_ct);
	if (lines_ct > 0)
	{
		for (i = 1; i < lines_ct; ++i)
		{
			const wchar* ctext = txbGetDynamicTextLayer(tc->textBoxId, i, tc->layer);
			txbSetDynamicTextLineLayer(tc->textBoxId, i - 1, ctext, tc->layer);
		}
		txbSetDynamicTextLineLayer(tc->textBoxId, lines_ct - 1, text, tc->layer);
	}
}
//-------------------------------------------------------------------------------------------

void txbConsoleClear(const struct TxtConsole *tc)
{
	s32 lines_ct, chars_ct, i;
	wchar empty_str[1];
	empty_str[0] = L'\0';
	_getLinesAndCharsCount(tc, &lines_ct, &chars_ct);
	for (i = 0; i < lines_ct; ++i)
	{
		txbSetDynamicTextLineLayer(tc->textBoxId, i, empty_str, tc->layer);
	}
}
//-------------------------------------------------------------------------------------------

void txbConvertAnsiToUnicode(const char* ansiStr, wchar* unicodeStr, u32 unicodeStrSize)
{
	u32 i, slen;
	SDK_ASSERT(ansiStr);
	SDK_ASSERT(unicodeStr);
	SDK_ASSERT(unicodeStrSize > 0);
	slen = (u32)STD_StrLen(ansiStr);
	if (slen > unicodeStrSize)
	{
		slen = unicodeStrSize;
	}
	for (i = 0; i < slen, i < unicodeStrSize; i++)
	{
		unicodeStr[i] = (wchar)ansiStr[i];
	}
	unicodeStr[slen] = L'\0';
}
//-------------------------------------------------------------------------------------------
