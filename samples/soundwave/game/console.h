#ifndef TXT_CONSOLE_H
#define TXT_CONSOLE_H

#include "system.h"
#include "texts.h"

#ifdef __cplusplus
extern "C" {
#endif

struct TxtConsole
{
	u32 textBoxId;
	s32 layer;
};

void txbConsoleInit(struct TxtConsole *const tc, s32 layer, s32 textbox_id);

void txbConsoleAdd(const struct TxtConsole *tc, const wchar *text);
void txbConsoleClear(const struct TxtConsole *tc);

void txbConvertAnsiToUnicode(const char* ansiStr, wchar* unicodeStr, u32 unicodeStrSize);

#ifdef __cplusplus
} /* extern "C" */
#endif

#endif