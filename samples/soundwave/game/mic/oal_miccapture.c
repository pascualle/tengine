#include "oal_miccapture.h"
#include <AL/al.h>
#include <AL/alc.h>
#include "memory.h"

#define CAPTURE_FREQ 11025
#define CAPTURE_SAMPLES (CAPTURE_FREQ / 2) // How much to capture at a time (affects latency)
#define CAPTURE_AL_FORMAT AL_FORMAT_MONO8
#define CAPTURE_SAMPLE_CHANNELS 1
#define CAPTURE_SAMPLE_BPS 8
#define CAPTURE_SAMPLE_SIZE ((CAPTURE_SAMPLE_BPS / 8) * CAPTURE_SAMPLE_CHANNELS)
#define CAPTURE_TIME_LIMIT_SEC 60

struct MCWavFileFmtStruct
{
	u8	RIFF[4];        // RIFF Header      Magic header
	u32	ChunkSize;      // RIFF Chunk Size  
	u8	WAVE[4];        // WAVE Header      
	u8	fmt[4];         // FMT header       
	u32	Subchunk1Size;  // Size of the fmt chunk                                
	u16	AudioFormat;    // Audio format 1=PCM,6=mulaw,7=alaw, 257=IBM Mu-Law, 258=IBM A-Law, 259=ADPCM 
	u16	NumOfChan;      // Number of channels 1=Mono 2=Sterio                   
	u32	SamplesPerSec;  // Sampling Frequency in Hz                             
	u32	bytesPerSec;    // bytes per second 
	u16	blockAlign;     // 2=16-bit mono, 4=16-bit stereo 
	u16	bitsPerSample;  // Number of bits per sample      
	u8	Subchunk2ID[4]; // "data"  string   
	u32	Subchunk2Size;  // Sampled data length    
};

static ALCdevice *gspALCMicDevice = NULL;
static BOOL gsCaptureStart = FALSE;
static u8 *gspWavBuffer = NULL;
static u32 gsWavBufferSize;
static u32 gsWavBufferPos;
static u8 *gspWavFile = NULL;

static u32 _oalMicCapture_CalculateDataSize(u32 ms);
static u8* _oalMicCapture_CreateEmptyWavFile(u32 seconds);
static void _oalMicCapture_UpdateWavFileDataSize(u32 size, u8* wavfile);

//-------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------

u32 _oalMicCapture_CalculateDataSize(u32 ms)
{
	return (CAPTURE_FREQ * (CAPTURE_SAMPLE_BPS / 8) * CAPTURE_SAMPLE_CHANNELS * ms) / 1000;
}
//-------------------------------------------------------------------------------------------

u8* _oalMicCapture_CreateEmptyWavFile(u32 seconds)
{
	u8 *p;
	struct MCWavFileFmtStruct *wh;
	const u32 wavDataSize = _oalMicCapture_CalculateDataSize(seconds * 1000);

	SDK_ASSERT(CAPTURE_TIME_LIMIT_SEC >= seconds);
	SDK_ASSERT(gspWavFile == NULL);
	p = (u8*)MALLOC(wavDataSize + sizeof(struct MCWavFileFmtStruct), "_oalMicCapture_CreateEmptyWavFile::gspWavFile");

	wh = (struct MCWavFileFmtStruct*)p;
	wh->RIFF[0] = 'R';
	wh->RIFF[1] = 'I';
	wh->RIFF[2] = 'F';
	wh->RIFF[3] = 'F';
	wh->WAVE[0] = 'W';
	wh->WAVE[1] = 'A';
	wh->WAVE[2] = 'V';
	wh->WAVE[3] = 'E';
	wh->fmt[0] = 'f';
	wh->fmt[1] = 'm';
	wh->fmt[2] = 't';
	wh->fmt[3] = ' ';
	wh->Subchunk1Size = 16;
	wh->AudioFormat = 1;
	wh->NumOfChan = CAPTURE_SAMPLE_CHANNELS;
	wh->SamplesPerSec = CAPTURE_FREQ;
	wh->bytesPerSec = wh->SamplesPerSec * wh->NumOfChan * (CAPTURE_SAMPLE_BPS / 8);
	wh->blockAlign = wh->NumOfChan * (CAPTURE_SAMPLE_BPS / 8);
	wh->bitsPerSample = CAPTURE_SAMPLE_BPS;
	wh->Subchunk2ID[0] = 'd';
	wh->Subchunk2ID[1] = 'a';
	wh->Subchunk2ID[2] = 't';
	wh->Subchunk2ID[3] = 'a';
	_oalMicCapture_UpdateWavFileDataSize(0, p);

	return p;
}
//--------------------------------------------------------------------------------------

void _oalMicCapture_UpdateWavFileDataSize(u32 size, u8* wavfile)
{
	struct MCWavFileFmtStruct *wh;
	SDK_NULL_ASSERT(wavfile);
	wh = (struct MCWavFileFmtStruct*)wavfile;
	wh->Subchunk2Size = size;
	wh->ChunkSize = 36 + wh->Subchunk2Size;
}
//--------------------------------------------------------------------------------------

BOOL oalMicCaptureDataExist(const u8* data)
{
	SDK_NULL_ASSERT(data);
	if(data[0] == 'R' && data[1] == 'I' &&  data[2] == 'F' && data[3] == 'F')
	{
		const struct MCWavFileFmtStruct *wh = (struct MCWavFileFmtStruct*)data;
		return wh->Subchunk2Size > 0;
	}
	return FALSE;
}
//--------------------------------------------------------------------------------------

float* oalMicCreateFromDataFloatBuf(const u8 *data, u32 *onumSamples)
{
	SDK_NULL_ASSERT(data);
	SDK_NULL_ASSERT(onumSamples);
	if(oalMicCaptureDataExist(data) == TRUE)
	{
		s32 i, fdata_ct, sample_size;
		float *fdata;
		const struct MCWavFileFmtStruct *wh = (struct MCWavFileFmtStruct*)data;
		sample_size = ((wh->bitsPerSample / 8) * wh->NumOfChan);
		*onumSamples = wh->Subchunk2Size / sample_size;
		fdata_ct = wh->Subchunk2Size / (wh->bitsPerSample / 8);
		fdata = (float*)MALLOC(fdata_ct * sizeof(float), "oalMicCreateFromDataFloatBuf:fdata");
		if(wh->bitsPerSample == 8)
		{
			for(i = 0; i < fdata_ct; i++)
			{
				fdata[i] = ((float)(((s32)*(data + sizeof(struct MCWavFileFmtStruct) + i * sizeof(u8))) - 127)) / 32768.0f;
			}
		}
		else if(wh->bitsPerSample == 16)
		{
			for(i = 0; i < fdata_ct; i++)
			{
				fdata[i] = ((float)((s16)*(data + sizeof(struct MCWavFileFmtStruct) + i * sizeof(s16)))) / 32768.0f;
			}
		}
		return fdata;
	}
	*onumSamples = 0;
	return NULL;
}
//--------------------------------------------------------------------------------------

const u8* oalMicCaptureInit(u32 buffer_in_seconds)
{
	SDK_ASSERT(gspALCMicDevice == NULL);
	// Request the default capture device with a half-second buffer
	gspALCMicDevice = alcCaptureOpenDevice(NULL, CAPTURE_FREQ, CAPTURE_AL_FORMAT, CAPTURE_SAMPLES);
	if(gspALCMicDevice == NULL)
	{
		oalMicCaptureRelease();
		return FALSE;
	}
	gspWavFile = _oalMicCapture_CreateEmptyWavFile(buffer_in_seconds);
	gsWavBufferSize = _oalMicCapture_CalculateDataSize(buffer_in_seconds * 1000);
	gspWavBuffer = gspWavFile + sizeof(struct MCWavFileFmtStruct);
	gsWavBufferPos = 0;
	return gspWavFile;
}
//--------------------------------------------------------------------------------------

void oalMicCaptureRelease()
{
	oalMicCaptureStop();
	if(gspWavFile != NULL)
	{
		FREE(gspWavFile);
		gspWavFile = NULL;
	}
	gspWavBuffer = NULL;
	if(gspALCMicDevice != NULL)
	{
		alcCaptureCloseDevice(gspALCMicDevice);
		gspALCMicDevice = NULL;
	}
}
//--------------------------------------------------------------------------------------

BOOL oalMicCaptureStart(void)
{
	if(gspALCMicDevice != NULL && gsCaptureStart == FALSE)
	{
		ALenum errorCode = AL_NO_ERROR;
		gsWavBufferPos = 0;
		_oalMicCapture_UpdateWavFileDataSize(gsWavBufferPos, gspWavFile);
		alcCaptureStart(gspALCMicDevice);
		errorCode = alcGetError(gspALCMicDevice);
		if(errorCode != AL_NO_ERROR)
		{
			return FALSE;
		}

		gsCaptureStart = TRUE;
	}
	return TRUE;
}
//--------------------------------------------------------------------------------------

void oalMicCaptureStop(void)
{
	if(gsCaptureStart == TRUE)
	{
		gsCaptureStart = FALSE;
		alcCaptureStop(gspALCMicDevice);
		_oalMicCapture_UpdateWavFileDataSize(gsWavBufferPos, gspWavFile);
		gsWavBufferPos = 0;
	}
}
//--------------------------------------------------------------------------------------

void oalMicCaptureUpdate(void)
{
	if(gsCaptureStart == TRUE && gsWavBufferPos != gsWavBufferSize)
	{
		ALCint samples;
		s32 capture_data_size;
		ALenum errorCode = AL_NO_ERROR;
		alcGetIntegerv(gspALCMicDevice, ALC_CAPTURE_SAMPLES, 1, &samples);
		errorCode = alcGetError(gspALCMicDevice);
		if(errorCode != AL_NO_ERROR)
		{
			oalMicCaptureStop();
			return;
		}
		capture_data_size = CAPTURE_SAMPLE_SIZE * samples;
		if(gsWavBufferPos + capture_data_size > gsWavBufferSize)
		{
			capture_data_size = gsWavBufferSize - gsWavBufferPos;
			samples = capture_data_size / CAPTURE_SAMPLE_SIZE;
		}
		alcCaptureSamples(gspALCMicDevice, gspWavBuffer + gsWavBufferPos, samples);
		gsWavBufferPos += capture_data_size;
	}
}
//--------------------------------------------------------------------------------------
