﻿Сборка:
ресурсы используются из ..\assets\data\, собираются make_res.bat

Запуск с командной строки (или command arguments если запускать под debug в ide)
win32app.exe [-p]
-p путь к ресурсам (если -p не задан, ресурсы ожидаются в assets\data в папке с win32app.exe)
пример: win32app.exe -p../../assets/data/

Cпециальные клавиши:
F11 - lost/restore render context
F9  - portrait/landscape views (resize)
F7  - low memory signal
Esc - exit