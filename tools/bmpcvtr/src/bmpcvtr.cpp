//------------------------------------------------------------------------------

#include <windows.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <direct.h>
#include <assert.h>
#include "cximage\ximage.h"

//------------------------------------------------------------------------------

#define	TRUE		1
#define	FALSE		0

#define NUM_COLUMN	16

#define	V5bit(x)((x) >> 3)
#define	RGB1555(r,g,b,a)((V5bit(r) << 0) | (V5bit(g) << 5) | (V5bit(b) << 10) | (((a) & 1) << 15))
#define	RGB5551(r,g,b,a)((V5bit(b) << 1) | (V5bit(g) << 6) | (V5bit(r) << 11) | (((a) & 1) << 0))
#define	RGB8888(r,g,b,a)(((b) << 0) | ((g) << 8) | ((r) << 16) | ((a) << 24))
#define MATH_ROUNDUP(x, base)(((x) + ((base)-1)) & ~((base)-1))
#define	SIZE_READBUFFER	80

typedef unsigned char u8;
typedef unsigned short u16;
typedef unsigned int u32;
typedef __int32 s32;
typedef u16 GXRgba;
typedef u16 GXRgb;
typedef u32 GXRgba32;

enum ArgKey
{
	KEY_1555 = 0,
	KEY_5551,
	KEY_8888,
	KEY_256,
	KEY_pl,
	KEY_o,
	KEY_2n,
	KEY_acm,
	KEY_bgr,
	KEY_flinear,
	KEY_fnearest,
	KEY_COUNT
};

const static _TCHAR pkeys[KEY_COUNT][32] =
{
	{L"-1555"},
	{L"-5551"},
	{L"-8888"},
	{L"-256"},
	{L"-pl"},
	{L"-o"},
	{L"-2n"},
	{L"-acm"},
	{L"-bgr"},
	{L"-flinear"},
	{L"-fnearest"}
};

typedef struct BMPImage
{
	u16 mType;
	u16 mOpaqType;
	u16 mWidth;
	u16 mHeight;
	u16 mWidth2n;
	u16 mHeight2n;
	s32 mRef;
	u32 mFilterType;
	u32 mA5DataSize;
	u32 mACMDataSize;
	u32 mDataSize;
	u16 *mpA5Data;
	u8 *mpACMData;
	union
    {
		GXRgba32 *mpDataDC32;
		GXRgba *mpDataDC16;
		u8 *mpData256;
	};
}BMPImage;

typedef enum BMPType
{
	BMP_TYPE_256 = 0,
	BMP_TYPE_DC16,
	BMP_TYPE_DC32,
	BMP_TYPE_NUM
}
BMPType;

static bool transparency_check(BMPImage &header, CxImage *img);
static s32 write_acm_data(CxImage *img_src, s32 acm_ratio, BMPImage &header, FILE *ofp);

//------------------------------------------------------------------------------

static void usage(void)
{
	fwprintf(stderr,
L"Usage: [path]name_file [-acm4] [-5551] [-bgr] [-2n] [-fnearest] [-oPath]\
\n -1555     - output as 1555 DC color (by default)\
\n -5551     - output as 5551 DC color\
\n -8888     - output as 8888 DC color\
\n -256      - output as 256 index color image (without palette)\
\n -pl       - output as 256 index color palette\
\n -bgr      - output color (1555, 5551, 8888 or palette) components in BGR format\
\n -2n       - width and height are multiples of 2^n\
\n -acm      - add additional 1 bit alpha collide map to file with 1(2,4,8) downsampling ratio\
\n -flinear  - \"linear\" parameter for texture mag/min function (by default)\
\n -fnearest - \"nearest\" parameter for texture mag/min function\
\n -o        - output directory\
\n 24bpp image uses color 0xff00ff as transparent\
\n 32bpp image uses alpha channel for transparency level\n");
	exit(1);
}
//------------------------------------------------------------------------------

int _tmain(int argc, _TCHAR* argv[])
{
	s32	result;
	CxImage *img;
	_TCHAR *outPath;
	_TCHAR s_dir[MAX_PATH];
	BMPImage header;
	ArgKey key = KEY_1555;
	ArgKey key_o = KEY_COUNT;
	ArgKey key_pl = KEY_COUNT;
	ArgKey key_2n = KEY_COUNT;
	ArgKey key_acm = KEY_COUNT;
	ArgKey key_bgr = KEY_COUNT;
	ArgKey key_filter = KEY_flinear;
	s32 acm_ratio = 1;

	memset(&header, 0, sizeof(BMPImage));

	if(argc < 2)
	{
		usage();
		return 1;
	}

	for(result = 2; result < argc; result++)
	{
		for(int i = 0; i < KEY_COUNT; i++)
		{
			_TCHAR *ptr = wcsstr(argv[result], pkeys[i]);
			if(ptr)
			{
				if(argv[result] == ptr)
				{
					switch(i)
					{
						case KEY_1555:
						case KEY_5551:
						case KEY_8888:
						case KEY_256:
							key = (ArgKey)i;
					}
					if(i == KEY_pl)
					{
						key_pl = KEY_pl;
					}
					if(i == KEY_o)
					{
						key_o = KEY_o;
						outPath = &argv[result][2];
					}
					if(i == KEY_2n)
					{
						key_2n = KEY_2n;
					}
					if(i == KEY_acm)
					{
						_TCHAR t_num[2];
						t_num[0] = argv[result][4];
                        t_num[1] = L'\0';
						key_acm = KEY_acm;
						acm_ratio = _wtoi(t_num);
						switch(acm_ratio)
						{
							case 1:
							case 2:
							case 4:
							case 8:
							break;
							default:
								key_acm = KEY_COUNT;
                        }
					}
					if(i == KEY_bgr)
					{
						key_bgr = KEY_bgr;
					}
					if(i == KEY_flinear)
					{
						key_filter = KEY_flinear;
					}
					if(i == KEY_fnearest)
					{
						key_filter = KEY_fnearest;
					}
				}
			}
		}
	}

	img = new CxImage();
	result = img->Load(argv[1], CXIMAGE_FORMAT_UNKNOWN);
	if(result)
	{
		result = img->IsValid();
	}
	if(result)
	{
		u16 depth;
		const u8 *buffer;
		s32 step;
		s32 width_3;
		s32 size;

		if(key_filter == KEY_flinear)
		{
			header.mFilterType = 'l';
		}
		else
		{
			header.mFilterType = 'n';
        }
		header.mHeight  = (u16)img->GetHeight();
		header.mWidth = (u16)img->GetWidth();
		depth = img->GetBpp();

		if(key == KEY_256 || key_pl == KEY_pl)
		{
			if(depth != 8)
			{
				//fwprintf(stderr, L"converting to 256 index color image: %s\n", argv[1]);
				if(depth < 8)
				{
					img->IncreaseBpp(8);
				}
				else
				{
					img->DecreaseBpp(8, false, NULL, 256);
				}
			}
		}
		else
		{
			if(key == KEY_1555 || key == KEY_5551 || key == KEY_8888)
			{
				if(depth < 24)
				{
					img->IncreaseBpp(24);
				}
			}
		}

		depth = img->GetBpp();
		step = depth / 8;
		width_3 = step * header.mWidth;
		if(width_3 % 4)
		{
			width_3 += 4 - width_3 % 4;
		}
		size = width_3 * header.mHeight;

		buffer = img->GetBits();
		if(NULL != buffer)
		{
			FILE	*ofp;
			_TCHAR	fname[MAX_PATH];
			s32		i, n;
			if(key_o == KEY_o)
			{
				wcscpy(s_dir, outPath);
				n = wcslen(s_dir) - 1;
				if((n >= 0) && ((s_dir[n] != L'\\') && (s_dir[n] != L'/')))
				{
					s_dir[n + 1] = L'/', s_dir[n + 2] = L'\0';
				}
			}
			else
			{
				wcscpy(s_dir, argv[1]);
				n = wcslen(s_dir) - 1;
				while((n >= 0) && (s_dir[n] != L'\\') && (s_dir[n] != L'/'))
				{
					--n;
                }
				s_dir[n + 1] = L'\0';
			}

			_wmkdir(s_dir);

			i = n = wcslen(argv[1]) - 1;
			while(n >= 0 && argv[1][n] != L'\\' && argv[1][n] != L'/')
			{
				--n;
			}
			while(i >= 0 && argv[1][i] != L'.')
			{
				--i;
            }
			n++;
			if(n < 0)n = 0;
			if(i < 0)i = 0;
			memcpy(fname, argv[1] + n, (i - n) * sizeof(_TCHAR));
			fname[i - n] = L'\0';
			if(key_pl != KEY_pl)
			{
				wcscat(fname, L".res");
			}
			else
			{
				wcscat(fname, L".plt");
			}
			wcscat(s_dir, fname);

			header.mOpaqType = 1;

			ofp = _wfopen(s_dir, L"wb");
			if(NULL != ofp)
			{
				s32 x, h, ax, ah, color;
				if(key_pl != KEY_pl)
				{
					u8 ttr[3];
					s32 tr_ct, ttr_ct;
					u8 *line8 = NULL;
					u16 *line16 = NULL;
					u32 *line32 = NULL;
					u16 *tr_data = NULL;
					s32 tr_data_sz = header.mWidth * header.mHeight;
					tr_ct = ttr_ct = 0;
					ttr[0] = ttr[1] = ttr[2] = 0;

					fwrite(&header, sizeof(BMPImage), 1, ofp);

					if(key_acm == KEY_acm)
					{
						if(transparency_check(header, img))
						{
							write_acm_data(img, acm_ratio, header, ofp);
						}
					}

					if(key_2n == KEY_2n)
					{
						s32 val = 2;
						while (header.mWidth > val)
						{
							val <<= 1;
						}
						header.mWidth2n = val;
						val = 2;
						while (header.mHeight > val)
						{
							val <<= 1;
						}
						header.mHeight2n = val;
					}
					else
					{
						header.mHeight2n = header.mHeight;
						header.mWidth2n = header.mWidth;
					}

					x = 0;
					h = size - step * header.mWidth;
					ax = 0;
					ah = header.mHeight - 1;
					if(header.mWidth * step != width_3)
					{
						switch(depth)
						{
							case 24:
								h -= width_3 - header.mWidth * 3;
							break;
							case 8:
								h -= width_3 - header.mWidth;
						}
					}

					switch(depth)
					{
						case 32:
							assert(0);
						case 24:
							if(key != KEY_8888)
							{
								header.mType = BMP_TYPE_DC16;
								if(img->AlphaIsValid())
								{
									if(tr_data_sz % 3)
									{
										tr_data_sz = tr_data_sz / 3 + 1;
									}
									else
									{
										tr_data_sz = tr_data_sz / 3;
									}
									tr_data = new u16[tr_data_sz];
								}
								line16 = new u16[header.mWidth2n];
								memset(line16, 0, header.mWidth2n * sizeof(u16));
							}
							else
							{
								header.mType = BMP_TYPE_DC32;
								line32 = new u32[header.mWidth2n];
								memset(line32, 0, header.mWidth2n * sizeof(u32));
                            }
						break;
						case 8:
							header.mType = BMP_TYPE_256;
							line8 = new u8[header.mWidth2n];
							memset(line8, 0, header.mWidth2n * sizeof(u8));
					}

					for(i = 0; i < size;)
					{
						switch(header.mType)
						{
							case BMP_TYPE_DC16:
							{
								u16 color = 0;
								s32 transparent = 1;
								switch(depth)
								{
									case 24:
										if(img->AlphaIsValid())
										{
											ttr[ttr_ct] = img->AlphaGet(ax, ah);
											if(V5bit(ttr[ttr_ct]) == 0)
											{
												transparent = 0;
												color = 0;
												header.mOpaqType = 0;
											}
											ttr_ct++;
											if(ttr_ct == 3)
											{
												ttr_ct = 0;
												tr_data[tr_ct] = RGB5551(ttr[0], ttr[1], ttr[2], 1);
												assert(tr_ct != tr_data_sz);
												tr_ct++;
												ttr[0] = ttr[1] = ttr[2] = 0;
											}
										}
										else
										if(buffer[x + h + 2] == 255 && buffer[x + h + 1] == 0 && buffer[x + h] == 255)
										{
											transparent = 0;
											color = 0;
											header.mOpaqType = 0;
										}
									break;
									case 32:
										assert(0);
								}
								if(transparent == 1)
								{
									if(key_bgr != KEY_bgr)
									{
										switch(key)
										{
											case KEY_1555:
												color = RGB1555(buffer[x + h + 2], buffer[x + h + 1], buffer[x + h], 1);
											break;
											case KEY_5551:
												color = RGB5551(buffer[x + h + 2], buffer[x + h + 1], buffer[x + h], 1);
										}
									}
									else
									{
										switch(key)
										{
											case KEY_1555:
												color = RGB1555(buffer[x + h], buffer[x + h + 1], buffer[x + h + 2], 1);
											break;
											case KEY_5551:
												color = RGB5551(buffer[x + h], buffer[x + h + 1], buffer[x + h + 2], 1);
										}
                                    }
								}
								line16[ax] = color;
							}
							break;
							case BMP_TYPE_DC32:
							{
								assert(depth == 24);
								const bool isAlphaIsValid = img->AlphaIsValid();
								if(!isAlphaIsValid &&
									buffer[x + h + 2] == 255 && buffer[x + h + 1] == 0 && buffer[x + h] == 255)
								{
									line32[ax] = 0;
								}
								else
								{
                                    u8 a32 = 255;
									if(isAlphaIsValid)
									{
										header.mOpaqType = 0;
										a32 = img->AlphaGet(ax, ah);
									}
									if(key_bgr != KEY_bgr)
									{
										line32[ax] = RGB8888(buffer[x + h], buffer[x + h + 1], buffer[x + h + 2], a32);
									}
									else
									{
										line32[ax] = RGB8888(buffer[x + h + 2], buffer[x + h + 1], buffer[x + h], a32);
									}
								}
							}
							break;
							case BMP_TYPE_256:
							{
								line8[ax] = buffer[x + h];
							}
						}
						i += step;
						x += step;
						ax++;
						if(x == step * header.mWidth)
						{
							if(line8)
							{
								fwrite(line8, sizeof(u8), header.mWidth2n, ofp);
								header.mDataSize += header.mWidth2n * sizeof(u8);
							}
							else if(line16)
							{
								fwrite(line16, sizeof(u16), header.mWidth2n, ofp);
								header.mDataSize += header.mWidth2n * sizeof(u16);
							}
							else if(line32)
							{
								fwrite(line32, sizeof(u32), header.mWidth2n, ofp);
								header.mDataSize += header.mWidth2n * sizeof(u32);
							}
							ax = x = 0;
							h -= step * header.mWidth;
							ah--;
							if(header.mWidth * step != width_3)
							{
								switch(depth)
								{
									case 32:
										assert(0);
									break;
									case 24:
										i += width_3 - header.mWidth * 3;
										h -= width_3 - header.mWidth * 3;
									break;
									case 8:
										i += width_3 - header.mWidth;
										h -= width_3 - header.mWidth;
								}
							}
						}
					}
					if(ttr_ct != 0)
					{
						tr_data[tr_ct] = RGB5551(ttr[0], ttr[1], ttr[2], 1);
						assert(tr_ct != tr_data_sz);
					}

					if(header.mHeight < header.mHeight2n)
					{
						for(i = header.mHeight; i < header.mHeight2n; i++)
						{
							if(line8)
							{
								if(i % 2 == 0)
								{
									memset(line8, 0xff, header.mWidth2n * sizeof(u8));
								}
								else
								{
									memset(line8, 0, header.mWidth2n * sizeof(u8));
								}
								fwrite(line8, sizeof(u8), header.mWidth2n, ofp);
								header.mDataSize += header.mWidth2n * sizeof(u8);
							}
							else if(line16)
							{
								if(i % 2 == 0)
								{
									memset(line16, 0xff, header.mWidth2n * sizeof(u16));
								}
								else
								{
									memset(line16, 0, header.mWidth2n * sizeof(u16));
								}
								fwrite(line16, sizeof(u16), header.mWidth2n, ofp);
								header.mDataSize += header.mWidth2n * sizeof(u16);
							}
							else if(line32)
							{
								//if(i % 2 == 0)
								//{
								//	memset(line32, 0xff, header.mWidth2n * sizeof(u32));
								//}
								//else
								{
                                    unsigned char fillchar = (unsigned char)(rand() % 255);
									memset(line32, fillchar, header.mWidth2n * sizeof(u32));
								}
								fwrite(line32, sizeof(u32), header.mWidth2n, ofp);
								header.mDataSize += header.mWidth2n * sizeof(u32);
							}
						}
					}

					if(line8)
					{
						delete[] line8;
					}
					if(line16)
					{
						delete[] line16;
					}
					if(line32)
					{
						delete[] line32;
					}

					if(tr_data != NULL)
					{
						fwrite(tr_data, sizeof(u16), tr_data_sz, ofp);
						header.mA5DataSize = tr_data_sz * sizeof(u16);
					}

					if(tr_data != NULL)
					{
						delete[] tr_data;
					}

					fseek(ofp, 0, SEEK_SET);
					fwrite(&header, sizeof(BMPImage), 1, ofp);
					result = 0;
					//fprintf(stderr, "%s: Width=%d, Height=%d, Depth=%d \n", argv[1], width, height, depth);
					if(header.mA5DataSize != 0)
					{
						fwprintf(stderr, L"note: resource has additional transparent data\n");
					}
				}
				else //palette
				{
                    u16 color;
					const RGBQUAD *pal = img->GetPalette();
					memset(s_dir, 0, 4);
					u16 width = 1;
					fwrite(&width, sizeof(u16), 1, ofp); //ver
					width = img->GetPaletteSize() / sizeof(RGBQUAD);
					fwrite(&width, sizeof(u16), 1, ofp); //size
					fwrite(s_dir, 4, 1, ofp); //for pointer (void*)
					for(i = 0; i < width; i++)
					{
						if(i == 0)
						{
							color = 0;
						}
						else
						{
							if(key_bgr != KEY_bgr)
							{
								color = RGB1555(pal[i].rgbRed, pal[i].rgbGreen, pal[i].rgbBlue, 1);
							}
							else
							{
								color = RGB1555(pal[i].rgbBlue, pal[i].rgbGreen, pal[i].rgbRed, 1);
							}
						}
						fwrite(&color, sizeof(u16), 1, ofp);
					}
					result = 0;
				}

				fclose(ofp);
			}
			else
			{
				fwprintf(stderr, L"Cannot create file \"%s\".\n", s_dir);
			}
		}
		else
		{
			fwprintf(stderr, L"Cannot read data from file \"%s\".\n", argv[1]);
		}
	}

	delete img;

	if (result)
	{
		fwprintf(stderr, L"Cannot convert file \"%s\".\n", argv[1]);
	}

	return result;
}
//------------------------------------------------------------------------------

bool transparency_check(BMPImage &header, CxImage *img)
{
	if(img->AlphaIsValid())
	{
		return true;
	}
	if(header.mType == BMP_TYPE_DC16)
	{
		const u8 *buffer;
		s32 i, x, size, h, step, depth, width_3;
		depth = img->GetBpp();
		step = depth / 8;
		width_3 = step * header.mWidth;
		if(width_3 % 4)
		{
			width_3 += 4 - width_3 % 4;
		}
		size = width_3 * header.mHeight;
		buffer = img->GetBits();
		for(i = 0; i < size;)
		{
			if(buffer[x + h + 2] == 255 && buffer[x + h + 1] == 0 && buffer[x + h] == 255)
			{
				return true;
			}
			i += step;
			x += step;
			if(x == step * header.mWidth)
			{
				x = 0;
				h -= step * header.mWidth;
				if(header.mWidth * step != width_3)
				{
					i += width_3 - header.mWidth * 3;
					h -= width_3 - header.mWidth * 3;
				}
			}
		}
	}
	return false;
}
//------------------------------------------------------------------------------

s32 write_acm_data(CxImage *img_src, s32 acm_ratio, BMPImage &header, FILE *ofp)
{
	u16 depth;
	const u8 *buffer;
	s32 step, width_3, size, i;
	s32 x, h, ax, ah;
	CxImage *img;
	u8 *line_acm;
	s32 w_acm, img_w, img_h;
	img = new CxImage(*img_src, true, false, true);
	if(acm_ratio != 1)
	{
		w_acm = 1;
		if(img->AlphaIsValid())
		{
			w_acm = 0;
		}
		img->Resample(img_src->GetWidth() / acm_ratio,
						img_src->GetHeight() / acm_ratio, w_acm, NULL);
	}
	img_h = (u16)img->GetHeight();
	img_w = (u16)img->GetWidth();
	depth = img->GetBpp();
	step = depth / 8;
	width_3 = step * img_w;
	if(width_3 % 4)
	{
		width_3 += 4 - width_3 % 4;
	}
	size = width_3 * img_h;
	buffer = img->GetBits();
	w_acm = (img_w + 7) / 8;
	line_acm = new u8[w_acm];
	x = 0;
	h = size - step * img_w;
	ax = 0;
	ah = img_h - 1;
	if(img_w * step != width_3)
	{
		h -= width_3 - img_w * 3;
	}
	memset(line_acm, 0, w_acm);
	fwrite(&w_acm, sizeof(u16), 1, ofp);
	fwrite(&acm_ratio, sizeof(u16), 1, ofp);
	for(i = 0; i < size;)
	{
		s32 transparent = 1;
		if(img->AlphaIsValid())
		{
			transparent = V5bit(img->AlphaGet(ax, ah));
			if(transparent < 3)
			{
				transparent = 0;
			}
			else
			{
				transparent = 1;
			}
		}
		else
		if(buffer[x + h + 2] == 255 && buffer[x + h + 1] == 0 && buffer[x + h] == 255)
		{
			transparent = 0;
		}
		i += step;
		x += step;
		line_acm[ax / 8] |= transparent << (ax % 8);
		ax++;
		if(x == step * img_w)
		{
			fwrite(line_acm, sizeof(u8), w_acm, ofp);
			header.mACMDataSize += w_acm * sizeof(u8);
			memset(line_acm, 0, w_acm);
			ax = x = 0;
			h -= step * img_w;
			ah--;
			if(img_w * step != width_3)
			{
				i += width_3 - img_w * 3;
				h -= width_3 - img_w * 3;
			}
		}
	}
	header.mACMDataSize += sizeof(u16) + sizeof(u16);
	delete[] line_acm;
	delete img;
	fwprintf(stderr, L"note: resource has additional 1 bit alpha collide map\n");
	return w_acm * img_h;
}
//------------------------------------------------------------------------------

