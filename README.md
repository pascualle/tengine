**Tiny multiplatform 2d engine for android/ios/win32/nix/kolibrios/web(emscripten)**
--------------------------------------------------------------------------------

A small cross-platform 2D framework for creating game applications for android/ios/win32/nix/kolibrios/web (emscripten).

Includes a level editor, animation, logical properties and states (MapEditor), as well as a resource converter (bmpcvtr).

c, lib, tengine, engine, game, homebrew, indie, gamedev, gui, nuklear, verlet, box2d, openAL, openSL ES, openGL, win32, NDK, IOS, freeBSD, NIX, emscripten, kolibriOS, multiplatform, level editor, converter, samples, API



**Getting Started**
--------------------------------------------------------------------------------
**1. Setting up the PATH environment variable (for Windows users):**

**win32 branch**

No environment variable setup is required.

**android branch**

To work with the android branch, you need to download the **android SDK** (https://developer.android.com/studio) and **NDK** (https://developer.android.com/ndk), **JRE** and **JDK** (https://www.oracle.com/java/technologies/downloads/)

- Create an environment variable ANDROID_HOME and set the path to the root folder of android_sdk

  (example d:\Android\android_sdk\)

- Create an environment variable ANDROID_NDK and set the path to the root folder of android_ndk

  (example d:\Android\android_ndk\)

- Create an environment variable JAVA_HOME and set the path to the root folder of jdk

  (example c:\Program Files (x86)\Java\jdk1.7\)

- Add the path of android_ndk to the PATH environment variable

Optionally:
cygwin (http://www.cygwin.com). Add the path to cygwin/bin to the PATH environment variable.

**web (emscripten) branch**

- Install Emscripten-SDK (https://emscripten.org/docs/getting_started/downloads.html)

- Create an environment variable EMSCRIPTEN_HOME and set the path to the root folder of Emscripten
  
 (example d:\Emscripten\)

- Create an environment variable EMSCRIPTEN and set the path to emcc
  
 (example d:\Emscripten\upstream\emscripten)

**kolibrios branch**

- nstall toolchain, libc, [Qemu] according to the instructions in tengine\samples\scroll_map_kolibrios\readme.txt

- Create an environment variable KOLIBRIOS_HOME and set the path in unix format to the root folder of SDK

  (example /d/kolibri)





**2. Setting up the development environment:**

- Add the path **tengine\src\tengine\** to includes 

This folder contains files necessary for the tengine library to work.

- Add the path **tengine\src\gui\** to includes

This folder contains files for the gui extension for tengine, using this extension is optional.

- Add the path **tengine\src\gamefield\** to includes

This folder contains the gamefield.h file necessary for the application to work based on tengine, it contains API functions for the application entry point. Implementing these functions is a necessary condition for the application to work.

- Add the path **tengine\src\_win32** to includes (or, depending on the platform, respectively **tengine\src\_android**, **tengine\src\_ios**, **tengine\src\_emscripten**)

This folder contains platform-dependent code.

- By default, tengine is configured to use the static **pthread** library. or the win32 environment, the pthread-win32 library is used, which is located at tengine\lib\win32\pthreads\ (*.h files and pthread.lib). Newer versions of the library can be downloaded from https://www.sourceware.org/pthreads-win32/. To disable multithreading, you can set the JOBS_IN_SINGLE_THREAD preprocessor key.

- пBy default, tengine is configured to use the **openAl** library. For the win32 environment, openAl is used, which is located at tengine\lib\win32\openAL\ (include\AL\*.h, Win32\OpenAL32.lib, Win32\OpenAL32.dll). Newer versions of the library can be downloaded from https://www.openal.org/downloads/.

**Note**: In win32, without the installed OpenAL32.dll , many examples may not run and give an error. Do not forget to copy OpenAL32.dll o the windows\system32 folder (or to the folder with the example exe).

- Using the android_sdk\SDK Manager.exe utility, download Tools, Android 2.3.3 (api10), and Extras.





**3. Setting up the development environment, preprocessor keys:**

**JOBS_IN_SINGLE_THREAD**

**JOBS_IN_SEPARATE_THREAD**

By default, tengine includes JOBS_IN_SEPARATE_THREAD, which determines the execution of loading streaming (constantly loaded) animation and background music. In the future, it is planned to develop functional tasks executed in a separate thread.

**SDK_DEBUG**

By default, tengine does not use SDK_DEBUG, which determines the inclusion of debug functionality, SDK_ASSERT and SDK_NULL_ASSERT functions, as well as the operation of OS_Printf and OS_Warning functions (output of debug messages).

**FRAME_ALLOCATOR**

By default, tengine does not use FRAME_ALLOCATOR, it works only when SDK_DEBUG is enabled. This is an optional but highly recommended parameter, allowing to prevent memory leaks and fragmentation. It includes functionality that controls the FRAME_ALLOCATOR principle, all allocated dynamic memory must be freed in strict reverse order. If the principle is not followed, an assert will be issued when using the FREE operator. An example of the code can be found in the scroll_map example (tengine\samples\scroll_map\game\gamefield.c).

**USE_STATIC_MEMORY**

By default, tengine does not use USE_STATIC_MEMORY, which determines the operation of the MALLOC and FREE functions, switching them to use the specified static pool. When using this key, the InitMemoryAllocator() function must specify the pointer to the static pool and its size in bytes as mandatory parameters.

**NITRO_SDK**

**ANDROID_NDK**

**IOS_APP**

**WINDOWS_APP**

**NIX_APP**

**EMSCRIPTEN_APP**

By default, tengine does not use any of these parameters, but setting one of these parameters is **mandatory**.

**DRAW_DEBUG_COLLIDERECTS**

**DRAW_DEBUG_VIEWRECTS**

By default, tengine does not use any of these parameters, setting them enables debug display of “collision” zones and “visibility” zones of objects.

**USE_OPENAL_SOUND**

**USE_SLES_SOUND**

**USE_NO_SOUND**

By default, tengine uses the USE_OPENAL_SOUND key for all platforms except ANDROID_NDK. For ANDROID_NDK, the USE_SLES_SOUND key is automatically set. USE_NO_SOUND disables the use of sound libraries.

**USE_FX32_AS_FLOAT**

**USE_FX32_AS_FIXED**

By default, tengine uses the USE_FX32_AS_FLOAT key, which determines the type and dimension of the fx32 data type. When USE_FX32_AS_FLOAT is enabled, the fx32 type is double, the standard type for storing floating-point values. The USE_FX32_AS_FIXED key defines the fx32 type as fixed, in this case, the representation of real numbers occurs in the int format, all operations on fx32 are integer and do not use the FPU (coprocessor).





**4. Data types and their features**

Since tengine is a cross-platform solution:

All integer types are redefined:

**u8**, **u16**, **s16**, **u32**, **s64**, **u64**, **s32**
prefixes u - unsigned, s - signed


булевый тип:

**BOOL**, takes the value FALSE or TRUE

For operations on floating-point numbers, a “fixed” type is used (the float type is **not recommended** as there may be code portability issues)

**fx32**

Functions for working with fx32:
FX32(x) - converts float to fx32, 
for example FX32(0.2f); 

FX_Mul(x, y) - multiply fx32
FX_Div(x, y) - divide fx32

Two-dimensional vector:
**fxVec2** 

Functions for working with fxVec2:

fxVec2 fxVec2Create(fx32 x, fx32 y)

fxVec2 fxVec2Add(fxVec2 const v1, fxVec2 const v2)

fxVec2 fxVec2Sub(fxVec2 const v1, fxVec2 const v2)

fxVec2 fxVec2Mul(fxVec2 const v1, fxVec2 const v2)

fxVec2 fxVec2MulFx(fxVec2 const v1, fx32 const val)

fxVec2 fxVec2DivFx(fxVec2 const v1, fx32 const val)

fx32 fxVec2Dot(fxVec2 const v1, fxVec2 const v2)

fx32 fxVec2Cross(fxVec2 const v1, fxVec2 const v2)

String type:

**char**

Functions for working with char:
STD_StrLen()

STD_StrCpy()

STD_StrCmp()

STD_StrCmp()

STD_StrCat()

For working with texts, tengine uses unicode, so the wchar type is used in the project
(the wchar_t type is **not recommended** as there may be code portability issues, for example under android)

**wchar**

Functions for working with wchar:
STD_WStrLen()

STD_WSprintf()

STD_WStrCmp()

STD_WStrCpy()

STD_WStrNCmp()

STD_WStrStr()


For working with color, the following types are used (tengine uses 16-bit color format 5551):

**GXRgba**
**GXRgb**

Function for working with GXRgba and GXRgb 
GX_RGBA(r, g, b, a) where r, g, b are in the range 0…31, and a is in the range 0…1

For working with pointers, tengine uses the following functions:

MI_CpuFill8()

MI_CpuFill16()

MI_CpuClear8()

MI_CpuClear32()

MI_CpuClearFast()

MI_CpuCopy8()

MI_CpuCopy16()

MI_CpuCopy32()

MI_CpuCopyFast()

For debug exceptions, the following are used:

SDK_ASSERT()

SDK_NULL_ASSERT()

For debug messages, tengine uses the following functions:

OS_Printf()

OS_Warning()

All types and basic operations with them are described in the file **tengine\src\tengine\platform.h**




**5. Create the gamefield.c (or gamefield.cpp) file**

The gamefield.c file is necessary for the application to work, and it needs to implement the functions from gamefield.h.


#include "tengine.h"
#include "gamefield.h"

/* called at the moment of application creation for memory initialization,
 depending on the set preprocessor key, this function needs to
 initialize the system memory with default settings or the specified static pool */

void tfgInitMemory()
{
}

/* called at the moment of application creation, after initializing the file system and rendering */

void tfgInit()
{
}

/* called when the application is terminated, before deinitializing the file system and rendering */

void tfgRelease()
{
}

/* called at the moment of application window size initialization
 platform-dependent function, operating systems: win32, web (emscripten), kolibrios */

void tfgSetWindowModeSize(s32 *const w, s32 *const h)
{
}

/* called at the moment of physical screen size change and also at the moment of creation, after init() */

void tfgResize(s32 w, s32 h)
{
}

/* called at the moment of losing the physical render device (e.g., phone screen turning off)
 note: after calling this function, the system automatically releases graphic resources
       the code in this function should not be bulky or take a long time to execute */

void tfgLostRenderDevice()
{
}

/* called at the moment of acquiring the physical render device
 note: before calling this function, the system automatically loads the released graphic resources */

void tfgRestoreRenderDevice()
{
}

/* called when the device's operating system requires freeing memory for higher priority tasks
 (platform-dependent function) */

void tfgLowMemory()
{
}

/* called continuously, serves as the main loop of the application,
 ms - the value in microseconds between function calls */

void tfgTick(s32 ms)
{
}



**6. The API documentation can be found in tengine\src\tengine\manual**

Also, read the tutorial on creating a small game in tengine\samples\space_invaders\how_to_make_a_game_tutorial.txt