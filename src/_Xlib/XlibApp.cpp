/*
tengine - cross-platform game engine
------------------------------------

Copyright (c) 2013 Papa Pascualle
Contact Email: pascualle@gmail.com

Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
*/

#include "platform.h"

#include "wchar.h"

#include <time.h>
#include <unistd.h>
#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <X11/X.h>
#include <X11/Xlib.h>
#include <GL/gl.h>
#include <GL/glx.h>
#include <GL/glu.h>

#include "low/tengine_low.h"
#include "low/a_touchpad.h"
#include "low/a_gamepad.h"
#include "render2dgl.h"
#include "gamefield.hpp"
#include "low/jobs_low.h"
#ifdef JOBS_IN_SEPARATE_THREAD
#include "pthread.h"
#endif

#ifndef _USE_OLD_IOSTREAMS
using namespace std;
#endif

GameField GameField::mInstance;
static BOOL gAppAlive = TRUE;
static BOOL gWindowActive = FALSE;
static BOOL gsTengineInit = FALSE;
static BOOL gsRenderDevice = FALSE;
static BOOL lbuttonState = FALSE;
static char gsStrPath[255];

Display                 *dpy;
Window                  root;
GLint                   att[] = { GLX_RGBA, GLX_DEPTH_SIZE, 24, GLX_DOUBLEBUFFER, None };
XVisualInfo             *vi;
Colormap                cmap;
XSetWindowAttributes    swa;
Window                  win;
GLXContext              glc;
XWindowAttributes       gwa;
XEvent                  xev;

#ifndef RES_PATH
  #define RES_PATH "./../assets/data/"
#endif


static int sWindowWidth = 512;
static int sWindowHeight = 512;

#ifdef JOBS_IN_SEPARATE_THREAD
static pthread_t sgThread;
static void* ThreadTasks(void *t);
#endif


//---------------------------------------------------------------------------

static BOOL checkGLErrors()
{
  GLenum error = glGetError();
  if (error != GL_NO_ERROR)
    {
      //_TCHAR errorString[32];
      //_stprintf(errorString, _T("0x%04x"), error);
      //MessageBox(NULL, errorString, _T("GL Error"), MB_OK);
    }
  return error;
}

void _init_tengine()
{
  if(!gsTengineInit)
    {
      s32 w, h;
      struct InitFileSystemData fileSystemData;

      glRender_Init();

      h = sWindowHeight;
      w = sWindowWidth;

      fileSystemData.mpPath = RES_PATH;
      InitFileSystem(&fileSystemData);
      gsRenderDevice = TRUE;
      GameField::getInstance().init();
      GameField::getInstance().resize(w, h);
      OS_Printf("init tengine display with size: %d %d\n", w, h);
      glRender_Resize(w, h);

#ifdef JOBS_IN_SEPARATE_THREAD
      {
	s32 rc;
	pthread_attr_t threadAttr;
	pthread_attr_init(&threadAttr);
	pthread_attr_setdetachstate(&threadAttr, PTHREAD_CREATE_JOINABLE);
	rc = pthread_create(&sgThread, &threadAttr, ThreadTasks, NULL);
	if(rc)
	  {
	    OS_Warning("error: return code from pthread_create() is %d\n", rc);
	    SDK_ASSERT(0);
	  }
	pthread_attr_destroy(&threadAttr);
      }
#endif
      gsTengineInit = TRUE;
    }
}

//---------------------------------------------------------------------------

void _release_tengine()
{
  if(gsTengineInit)
    {
      /*
#ifdef JOBS_IN_SEPARATE_THREAD
      {
	s32 rc = pthread_join(sgThread, NULL);
	if(rc)
	  {
	    OS_Warning("error: return code from pthread_join() is %d\n", rc);
	    SDK_ASSERT(0);
	  }
      }
#endif*/

      GameField::getInstance().release();

      ReleaseFileSystem();
//    setVisible(HUD_LAYER, TRUE);
      glRender_Release();
    }
  gsTengineInit = FALSE;
}

void ExitHandler(int s)
{
  printf("Recieving kill signal\n");
  _release_tengine();
  if(dpy!=NULL)
  {
    if(glc!=NULL)
    {
      glXDestroyContext(dpy, glc);
    }
    XCloseDisplay(dpy);
  }
  exit(0);
}

int EventProcess()
{

  while(XCheckWindowEvent(dpy, win, KeyPressMask | KeyRelease | StructureNotifyMask |//ResizeRedirectMask |
    ButtonPressMask | ButtonReleaseMask | PointerMotionMask, &xev))
  {
    XEvent * ev = &xev;
    if(ev->type == KeyPress)
      {
	  onKeyDown(((XKeyEvent*)ev)->keycode);
      }
    else if(ev->type == KeyRelease)
      {
	onKeyUp(((XKeyEvent*)ev)->keycode);
      }
    else if(ev->type == ConfigureNotify)
      {
	if (sWindowWidth != ((XConfigureEvent*)ev)->width
	    || sWindowHeight != ((XConfigureEvent*)ev)->height) 
	  {
	    sWindowWidth = ((XConfigureEvent*)ev)->width;
	    sWindowHeight = ((XConfigureEvent*)ev)->height;
	    //printf("Resize tu %d, %d\n",sWindowWidth, sWindowHeight);
	    GameField::getInstance().resize(sWindowWidth, sWindowHeight);
	    glRender_Resize(sWindowWidth, sWindowHeight);
	  }
      }
    else if (ev->type == ButtonPress)
      {
	lbuttonState = 1;
	u16 xPos =  ((XButtonEvent*)ev)->x;
	u16 yPos =  ((XButtonEvent*)ev)->y;
	onTouchPadDown(0, xPos, yPos);
      }
    else if (ev->type == ButtonRelease)
      {
	
	lbuttonState = 0;
	u16 xPos =  ((XButtonEvent*)ev)->x;
	u16 yPos =  ((XButtonEvent*)ev)->y;
	onTouchPadUp(0, xPos, yPos);
      } 
    else if(ev->type == MotionNotify)
      {
  	    if( (lbuttonState&0xFF) > 0)
  	    { 
		  u16 xPos =  ((XMotionEvent*)ev)->x;
		  u16 yPos =  ((XMotionEvent*)ev)->y;
		  onTouchPadMove(0, xPos, yPos);
		  //printf("Button 1 move to %d , %d\n", xPos, yPos);
  	    }
      };
  };
  return 1;
}

int main(int argc, char *argv[]) 
{

  signal(SIGKILL, ExitHandler);
  signal(SIGINT, ExitHandler);
  signal(SIGSTOP, ExitHandler);
  signal(SIGQUIT, ExitHandler);
  signal(SIGSEGV, ExitHandler);
  signal(SIGTERM, ExitHandler);

  gAppAlive = true;
  gsRenderDevice = true;
  
  dpy = XOpenDisplay(NULL);
  
  if(dpy == NULL) 
    {
      printf("\n\tcannot connect to X server\n\n");
      exit(0);
    }
  
  root = DefaultRootWindow(dpy);

  vi = glXChooseVisual(dpy, 0, att);

  if(vi == NULL) 
    {
      printf("\n\tno appropriate visual found\n\n");
      exit(0);
    } 
  
  cmap = XCreateColormap(dpy, root, vi->visual, AllocNone);
  
  swa.colormap = cmap;
  swa.event_mask = KeyPressMask | KeyRelease | StructureNotifyMask |//ResizeRedirectMask |
    ButtonPressMask | ButtonReleaseMask | PointerMotionMask;
  
  win = XCreateWindow(dpy, root, 0, 0, sWindowWidth, sWindowHeight,
		      0, vi->depth, InputOutput, vi->visual,
		      CWColormap | CWEventMask, &swa);
  
  XMapWindow(dpy, win);
  XStoreName(dpy, win, "VERY SIMPLE APPLICATION");
 
  glc = glXCreateContext(dpy, vi, NULL, GL_TRUE);
  glXMakeCurrent(dpy, win, glc);
 
  glEnable(GL_DEPTH_TEST); 
  clock_t t;
  clock_t tp;
  tp = clock();

 
  _init_tengine();
 
  while(gAppAlive)
    {
      t = tp;
      tp = clock();
      clock_t ct = ((tp - t) * 10);// * CLOCKS_PER_SEC); -- HACK AND FAKE, NEED FIX!!!!!
	
      //XCheckIfEvent(dpy,&xev, EventProcess, NULL);
      //printf("Start Event check\n");
      EventProcess();
      //printf("End Event check\n");
      
      GameField::getInstance().gamefieldTick(ct);
      transferToVRAM();
      glRender_DrawFrame();
            
      glXSwapBuffers(dpy, win);
      usleep(25000);	
    }

  _release_tengine();
}

#ifdef JOBS_IN_SEPARATE_THREAD
void* ThreadTasks(void *t)
{
  while(gAppAlive)
    {
      if(gsRenderDevice)
	{
	  if(jobUpdateAnimStreamTasksAndTrySetMutex()) // <-- jobCriticalSectionBegin()
	    {
	      jobUpdateAnimStreamTasksAndLoadTextures();
	      jobCriticalSectionEnd();
	    }
	}
      sched_yield();
    }
  pthread_exit(NULL);
  return NULL;
}
#endif


