
#include "memory.h"
#include "verlet.h"
#include "verlet_low.h"
#include "fxmath.h"

//---------------------------------------------------------------------------

#define VRLT_MIN_FLOAT FX32(0.0001)
#define VRLT_MIN_STEP_VALUE 15

static void _vrltInitObject(struct Verlet *v, s32 objectIndex, const struct VerletObjectInitParams* params);
static void _vrltReleaseObject(struct Verlet *v, s32 objectIndex);

static void _vrltParticlePush(struct VTObject *c, struct VTParticle* p);

static void _vrltBounds(struct Verlet *v, struct VTParticle *p);

//---------------------------------------------------------------------------

void vrltInit(struct Verlet *v, struct VerletInitParams *params)
{
	SDK_NULL_ASSERT(v);
	SDK_NULL_ASSERT(params);
	MI_CpuClear8(v, sizeof(struct Verlet));
	v->mWidth = params->mWidth;
	v->mHeight = params->mHeight;
	v->mObjectsPoolMax = (s32)params->mObjectsCount;
	if(v->mObjectsPoolMax > 0)
	{
		s32 i;
		v->mConstraintsPoolMax = (s32)params->mConstraintsPoolMax;
		if(v->mConstraintsPoolMax > 0)
		{
			v->mppConstraintsPool = (struct VTConstraint**)MALLOC(params->mConstraintsPoolMax * sizeof(struct VTConstraint*), "vrltInit:mppConstraintsPool");
			for(i = 0; i < v->mConstraintsPoolMax; i++)
			{
				v->mppConstraintsPool[i] = (struct VTConstraint*)MALLOC(sizeof(struct VTConstraint), "vrltInit:mppConstraintsPool[i]");
			}
		}
		v->mParticlesPoolMax = (s32)params->mParticlesPoolMax;
		if(v->mParticlesPoolMax > 0)
		{
			v->mppParticlesPool = (struct VTParticle**)MALLOC(params->mParticlesPoolMax * sizeof(struct VTParticle*), "vrltInit:mppParticlesPool");
			for(i = 0; i < v->mParticlesPoolMax; i++)
			{
				v->mppParticlesPool[i] = (struct VTParticle*)MALLOC(sizeof(struct VTParticle), "vrltInit:mppParticlesPool[i]");
			}
		}
		v->mppObjectsPool = (struct VTObject**)MALLOC(params->mObjectsCount * sizeof(struct VTObject*), "vrltInit:mppObjects");
		for(i = 0; i < v->mObjectsPoolMax; i++)
		{
			v->mppObjectsPool[i] = (struct VTObject*)MALLOC(sizeof(struct VTObject), "vrltInit:mppObjects[i]");
			_vrltInitObject(v, i, &params->mppObjectInitArr[i]);
		}
	}
}
//---------------------------------------------------------------------------

void vrltRelease(struct Verlet *v)
{
	SDK_NULL_ASSERT(v);
	if(v->mObjectsPoolMax > 0)
	{
		while(v->mObjectsPoolMax > 0)
		{
			v->mObjectsPoolMax--;
			if(v->mppObjectsPool[v->mObjectsPoolMax] != NULL)
			{
				_vrltReleaseObject(v, v->mObjectsPoolMax);
				FREE(v->mppObjectsPool[v->mObjectsPoolMax]);
			}
		}
		FREE(v->mppObjectsPool);
		if(v->mParticlesPoolMax > 0)
		{
			while(v->mParticlesPoolMax > 0)
			{
				v->mParticlesPoolMax--;
				FREE(v->mppParticlesPool[v->mParticlesPoolMax]);
			}
			FREE(v->mppParticlesPool);
		}
		if(v->mConstraintsPoolMax > 0)
		{
			while(v->mConstraintsPoolMax > 0)
			{
				v->mConstraintsPoolMax--;
				FREE(v->mppConstraintsPool[v->mConstraintsPoolMax]);
			}
			FREE(v->mppConstraintsPool);
		}
	}
	MI_CpuClear8(v, sizeof(struct Verlet));
}
//---------------------------------------------------------------------------

void vrltClear(struct Verlet *v)
{
	s32 c;
	SDK_NULL_ASSERT(v);
	for (c = 0 ; c < v->mObjectsPoolCount; c++)
	{
		v->mppObjectsPool[c]->mParticlesCount = 0;
		v->mppObjectsPool[c]->mConstraintsCount = 0;
	}
	v->mConstraintsPoolCount = 0;
	v->mParticlesPoolCount = 0;
	v->mObjectsPoolCount = 0;
}
//---------------------------------------------------------------------------

s32 vrltGetObjectsCount(struct Verlet *v)
{
	SDK_NULL_ASSERT(v);
	return v->mObjectsPoolCount;
}
//---------------------------------------------------------------------------

s32 vrltGetParticlesCount(struct Verlet *v)
{
	SDK_NULL_ASSERT(v);
	return v->mParticlesPoolCount = 0;
}
//---------------------------------------------------------------------------

s32 vrltGetConstraintsCount(struct Verlet *v)
{
	SDK_NULL_ASSERT(v);
	return v->mConstraintsPoolCount = 0;
}
//---------------------------------------------------------------------------

s32 vrltGetObjectParticlesCount(struct Verlet *v, s32 objIndex)
{
	SDK_NULL_ASSERT(v);
	if(objIndex < v->mObjectsPoolCount && objIndex >= 0)
	{
		return v->mppObjectsPool[objIndex]->mParticlesCount;
	}
	return 0;
}
//---------------------------------------------------------------------------

s32 vrltGetObjectConstraintsCount(struct Verlet *v, s32 objIndex)
{
	SDK_NULL_ASSERT(v);
	if(objIndex < v->mObjectsPoolCount && objIndex >= 0)
	{
		return v->mppObjectsPool[objIndex]->mConstraintsCount;
	}
	return 0;
}
//---------------------------------------------------------------------------

void _vrltInitObject(struct Verlet *v, s32 objectIndex, const struct VerletObjectInitParams* params)
{
	SDK_NULL_ASSERT(v);
	SDK_ASSERT(v->mObjectsPoolMax > (s32)objectIndex); // init Verlet before
	SDK_ASSERT(v->mppObjectsPool[objectIndex] != NULL);
	MI_CpuClear8(v->mppObjectsPool[objectIndex], sizeof(struct VTObject));
	v->mppObjectsPool[objectIndex]->mParticlesMax = (s32)params->mParticlesMax;
	v->mppObjectsPool[objectIndex]->mConstraintsMax = (s32)params->mConstraintsMax;
	v->mppObjectsPool[objectIndex]->mGravity = params->mGravity;
	v->mppObjectsPool[objectIndex]->mFriction = params->mFriction;
	v->mppObjectsPool[objectIndex]->mGroundFriction = params->mGroundFriction;
	if(v->mppObjectsPool[objectIndex]->mConstraintsMax > 0)
	{
		v->mppObjectsPool[objectIndex]->mppConstraints = (struct VTConstraint**)MALLOC(v->mppObjectsPool[objectIndex]->mConstraintsMax * sizeof(struct VTConstraint*), "vrltInitObject:mppConstraints");
	}
	if(v->mppObjectsPool[objectIndex]->mParticlesMax > 0)
	{
		v->mppObjectsPool[objectIndex]->mppParticles = (struct VTParticle**)MALLOC(v->mppObjectsPool[objectIndex]->mParticlesMax * sizeof(struct VTParticle*), "vrltInitObject:mppParticles");
	}
}
//---------------------------------------------------------------------------

void _vrltReleaseObject(struct Verlet *v, s32 objectIndex)
{
	SDK_NULL_ASSERT(v->mppObjectsPool[objectIndex]);
	if(v->mppObjectsPool[objectIndex]->mParticlesMax > 0)
	{
		FREE(v->mppObjectsPool[objectIndex]->mppParticles);
	}
	if(v->mppObjectsPool[objectIndex]->mConstraintsMax > 0)
	{
		FREE(v->mppObjectsPool[objectIndex]->mppConstraints);
	}
	MI_CpuClear8(v->mppObjectsPool[objectIndex], sizeof(struct VTObject));
}
//---------------------------------------------------------------------------

void _vrltInitParticle(struct VTParticle *v, const struct fxVec2 *pos, const s32 *data)
{
	SDK_NULL_ASSERT(v);
	SDK_NULL_ASSERT(pos);
	v->pos = *pos;
	v->lastPos = *pos;
	v->data = 0;
	if(data != NULL)
	{
		v->data = *data; 
	}
}
//---------------------------------------------------------------------------

struct VTObject *_vrltObjectPop(struct Verlet *v)
{
	SDK_ASSERT(v->mObjectsPoolMax > 0);
	if(v->mObjectsPoolCount < v->mObjectsPoolMax)
	{
		return v->mppObjectsPool[v->mObjectsPoolCount++];
	}
	SDK_ASSERT(v->mObjectsPoolCount < v->mObjectsPoolMax);
	return NULL;
}
//---------------------------------------------------------------------------

struct VTParticle* _vrltParticlePushToObj(struct Verlet *v, struct VTObject *c)
{
	SDK_ASSERT(v->mParticlesPoolMax > 0);
	if(v->mParticlesPoolCount < v->mParticlesPoolMax)
	{
		struct VTParticle* p;
		p = v->mppParticlesPool[v->mParticlesPoolCount];
		v->mParticlesPoolCount++;
		_vrltParticlePush(c, p);
		return p;
	}
	SDK_ASSERT(v->mParticlesPoolCount < v->mParticlesPoolMax);
	return NULL;
}
//---------------------------------------------------------------------------

void _vrltParticlePush(struct VTObject *c, struct VTParticle* p)
{
	SDK_NULL_ASSERT(c);
	SDK_ASSERT(c->mParticlesMax > 0);
	if(c->mParticlesCount < c->mParticlesMax)
	{
		c->mppParticles[c->mParticlesCount] = p;
		c->mParticlesCount++;
		return;
	}
	SDK_ASSERT(c->mParticlesCount < c->mParticlesMax);
}
//---------------------------------------------------------------------------

void _vrltBounds(struct Verlet *v, struct VTParticle *p)
{
	s32 x, y;
	SDK_NULL_ASSERT(v);
	SDK_NULL_ASSERT(p);
	x = fx2int(p->pos.x);
	y = fx2int(p->pos.y);
	/*if(y < 0)
	{
		p->pos.y = 0;
	}*/
	if(y > v->mHeight - 1)
	{
		p->pos.y = FX32(v->mHeight - 1);
	}
	if(x < 0)
	{
		p->pos.x = 0;
	}
	if(x > v->mWidth - 1)
	{
		p->pos.x = FX32(v->mWidth - 1);
	}
}
//---------------------------------------------------------------------------

void vrltUpdate(struct Verlet *v, s32 ms, OnVrltSelectedObjectCallback pCallbackFn)
{
	s32 i, c;
	struct VTSelectedObjectCallbackData cbd;
	struct VTParticle *selParticle = NULL;
	
	SDK_NULL_ASSERT(v);

	for(c = 0 ; c < v->mObjectsPoolCount; c++)
	{
		SDK_NULL_ASSERT(v->mppObjectsPool);
		SDK_NULL_ASSERT(v->mppObjectsPool[c]);
		if(v->mppObjectsPool[c]->mState == VOS_READY)
		{
			struct VTParticle **particles = v->mppObjectsPool[c]->mppParticles;
			for (i = 0; i < v->mppObjectsPool[c]->mParticlesCount; i++)
			{
				struct fxVec2 p, velocity;
				
				SDK_NULL_ASSERT(particles);
				SDK_NULL_ASSERT(particles[i]);

				// calculate velocity
				p = fxVec2Sub(particles[i]->pos, particles[i]->lastPos);
				velocity = fxVec2MulFx(p, v->mppObjectsPool[c]->mFriction);

				// ground friction
				if(v->mHeight > 0)
				{
					if (fx2int(particles[i]->pos.y) >= v->mHeight - 1 &&
						(fxMul(velocity.x, velocity.x) + fxMul(velocity.y, velocity.y)) > VRLT_MIN_FLOAT)
					{
						const fx32 m = fxVec2Length(velocity);
						velocity.x = fxDiv(velocity.x, m);
						velocity.y = fxDiv(velocity.y, m);
						velocity = fxVec2MulFx(velocity, fxMul(m, v->mppObjectsPool[c]->mGroundFriction));
					}
				}

				// save last good state
				particles[i]->lastPos = particles[i]->pos;

				// gravity
				particles[i]->pos = fxVec2Add(particles[i]->pos, v->mppObjectsPool[c]->mGravity);

				// inertia	
				particles[i]->pos = fxVec2Add(particles[i]->pos, velocity);

				if(particles[i] == v->mpSelectedObject)
				{
					cbd.obj_id = c;
					cbd.idx = i;
					selParticle = particles[i];
				}
			}
		}
	}	

	if(pCallbackFn != NULL && selParticle != NULL)
	{
		cbd.type = VTRT_PARTICLE;
		cbd.ioPos = &selParticle->pos;
		pCallbackFn(&cbd);
	}

	// relax
	{
		fx32 stepCoef;
		struct VTConstraint *selConstraint = NULL;
		if(ms < VRLT_MIN_STEP_VALUE)
		{
			ms = VRLT_MIN_STEP_VALUE;
		}
		stepCoef = fxDiv(FX32_ONE, FX32(ms));
		for (c = 0 ; c < v->mObjectsPoolCount; c++)
		{
			if(v->mppObjectsPool[c]->mState == VOS_READY)
			{
				struct VTConstraint **constraints = v->mppObjectsPool[c]->mppConstraints;
				for(i = 0; i < ms; i++)
				{
					s32 j;
					for(j = 0; j < v->mppObjectsPool[c]->mConstraintsCount; j++)
					{
						_vrltConstraintRelax(constraints[j], stepCoef);
						if(constraints[j] == v->mpSelectedObject)
						{
							cbd.obj_id = c;
							cbd.idx = j;
							selConstraint = constraints[j];
						}
					}
				}
			}
		}

		if(pCallbackFn != NULL && selConstraint != NULL)
		{
			cbd.type = VTRT_CONSTRAINT;
			cbd.ioPos = &selConstraint->mPos;
			pCallbackFn(&cbd);
		}	
	}

	// bounds checking
	if(v->mHeight > 0 && v->mWidth > 0)
	{
		for (c = 0 ; c < v->mObjectsPoolCount; c++)
		{
			if(v->mppObjectsPool[c]->mState == VOS_READY)
			{
				for (i = 0; i < v->mppObjectsPool[c]->mParticlesCount; i++)
				{
					_vrltBounds(v, v->mppObjectsPool[c]->mppParticles[i]);
				}
			}
		}
	}
}
//---------------------------------------------------------------------------

void vrltRender(struct Verlet *v, s32 objIndex, OnVrltRenderCallback pCallbackFn)
{
	SDK_NULL_ASSERT(v);
	SDK_NULL_ASSERT(pCallbackFn);
	if(objIndex < v->mObjectsPoolCount && v->mppObjectsPool[objIndex]->mState == VOS_READY)
	{
		s32 i;
		struct VTCallbackData cbd;
		struct VTParticle **p = v->mppObjectsPool[objIndex]->mppParticles;
		struct VTConstraint **c = v->mppObjectsPool[objIndex]->mppConstraints;
		cbd.mType = VTRT_CONSTRAINT;
		cbd.mpData = NULL;
		for (i = 0; i < v->mppObjectsPool[objIndex]->mConstraintsCount; i++)
		{
			cbd.mPointCount = 1;
			cbd.mpPoint[0] = &c[i]->mpParticle1->pos;
			if(c[i]->mpParticle2 != NULL)
			{
				cbd.mpPoint[1] = &c[i]->mpParticle2->pos;
				cbd.mPointCount = 2;
				if(c[i]->mpParticle3 != NULL)
				{
					cbd.mpPoint[1] = &c[i]->mpParticle3->pos;
					cbd.mPointCount = 3;
				}
			}
			pCallbackFn((const struct VTCallbackData*)&cbd);
		}
		cbd.mType = VTRT_PARTICLE;
		cbd.mPointCount = 1;
		for (i = 0; i < v->mppObjectsPool[objIndex]->mParticlesCount; i++)
		{
			cbd.mpPoint[0] = &p[i]->pos;
			cbd.mpData = &p[i]->data;
			pCallbackFn((const struct VTCallbackData*)&cbd);
		}
	}
}
//---------------------------------------------------------------------------

BOOL vrltSelectNearestObject(struct Verlet *v, const struct fxVec2 *pos, fx32 selectionRadius)
{
	s32 c, i;
	fx32 d2Nearest;
	const void* obj = NULL;
	const struct VTObject* constraintsNearest = NULL;
	d2Nearest = 0;

	SDK_NULL_ASSERT(v);
	SDK_NULL_ASSERT(pos);

	for (c = 0; c < v->mObjectsPoolCount; c++)
	{
		SDK_NULL_ASSERT(v->mppObjectsPool);
		SDK_NULL_ASSERT(v->mppObjectsPool[c]);
		if(v->mppObjectsPool[c]->mState == VOS_READY)
		{
			struct VTParticle** particles = v->mppObjectsPool[c]->mppParticles;
			for(i = 0; i < v->mppObjectsPool[c]->mParticlesCount; i++)
			{
				fx32 d2;
				SDK_NULL_ASSERT(particles);
				SDK_NULL_ASSERT(particles[i]);
				d2 = fxVec2Length(fxVec2Sub(*pos, particles[i]->pos));
				if(d2 <= FX_Mul(selectionRadius, selectionRadius) && (obj == NULL || d2 < d2Nearest))
				{
					obj = particles[i];
					constraintsNearest = v->mppObjectsPool[c];
					d2Nearest = d2;
					break;
				}
			}
		}
	}

	if(constraintsNearest != NULL)
	{
		for(i = 0; i < constraintsNearest->mConstraintsCount; i++)
		{
			const struct VTConstraint* constraint = constraintsNearest->mppConstraints[i];
			SDK_NULL_ASSERT(constraint);
			if(constraint->mType == VTCT_PIN && constraint->mpParticle1 == obj)
			{
				obj = constraint;
				break;
			}
		}
	}
	
	v->mpSelectedObject = obj;
	return obj != NULL;
}
//---------------------------------------------------------------------------

BOOL vrltSelectObject(struct Verlet *v, s32 obj_id, s32 particle_idx)
{
	if(obj_id < v->mObjectsPoolCount && v->mppObjectsPool[obj_id]->mState == VOS_READY)
	{
		const struct VTObject *constraintsNearest = v->mppObjectsPool[obj_id];
		SDK_NULL_ASSERT(constraintsNearest);
		if(particle_idx < constraintsNearest->mParticlesCount)
		{
			s32 i;
			const void* obj = constraintsNearest->mppParticles[particle_idx];
			for(i = 0; i < constraintsNearest->mConstraintsCount; i++)
			{
				const struct VTConstraint* constraint = constraintsNearest->mppConstraints[i];
				SDK_NULL_ASSERT(constraint);
				if(constraint->mType == VTCT_PIN && constraint->mpParticle1 == obj)
				{
					obj = constraint;
					break;
				}
			}
			v->mpSelectedObject = obj;
			return obj != NULL;
		}
	}
	return FALSE;
}
//---------------------------------------------------------------------------

void vrltClearSelectObject(struct Verlet *v)
{
	SDK_NULL_ASSERT(v);
	v->mpSelectedObject = NULL;
}
//---------------------------------------------------------------------------
