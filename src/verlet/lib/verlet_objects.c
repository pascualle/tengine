
#include "verlet.h"
#include "verlet_low.h"

//---------------------------------------------------------------------------

s32 vrltCreateTire(struct Verlet *v, struct fxVec2 origin, fx32 radius, s32 segments, fx32 spokeStiffness, fx32 treadStiffness)
{
	s32 i;
	fx32 stride;
	struct VTObject *obj;
	struct VTParticle* particle;

	SDK_NULL_ASSERT(v);
	obj = _vrltObjectPop(v);
	SDK_NULL_ASSERT(obj);

	stride = (2 * FX_PI) / segments;
	for(i = 0; i < segments; i++)
	{
		fx32 fsin, fcos;
		struct fxVec2 pos;
		fx32 theta = i * stride;
		fxCordic(theta, &fsin, &fcos, FX_CORDIC_MIDDLE_PRECISION);
		particle = _vrltParticlePushToObj(v, obj);
		SDK_NULL_ASSERT(particle);
		pos = fxVec2Create(origin.x + fxMul(fcos, radius), origin.y + fxMul(fsin, radius));
		_vrltInitParticle(particle, &pos, NULL);
	}

	particle = _vrltParticlePushToObj(v, obj);
	_vrltInitParticle(particle, &origin, NULL);

	for(i = 0; i < segments; i++)
	{
		_vrltDistanceConstraintPushToObj(v, obj, i, (i + 1) % segments, treadStiffness, NULL);
		_vrltDistanceConstraintPushToObj(v, obj, i, obj->mParticlesCount - 1, spokeStiffness, NULL); //center == last
		_vrltDistanceConstraintPushToObj(v, obj, i, (i + 5) % segments, treadStiffness, NULL);
	}

	obj->mState = VOS_READY;
	return v->mObjectsPoolCount - 1;
}
//---------------------------------------------------------------------------

s32 vrltCreateCloth(struct Verlet *v, struct fxVec2 origin, s32 width, s32 height, s32 segments, s32 pinMod, fx32 stiffness)
{
	s32 x, y;
	fx32 xstr, ystr, w, h;
	struct VTObject *obj;
	struct VTParticle* particle;

	SDK_NULL_ASSERT(v);
	obj = _vrltObjectPop(v);
	SDK_NULL_ASSERT(obj);

	w = FX32(width);
	h = FX32(height);
	xstr = w / segments;
	ystr = h / segments;
	for(y = 0; y < segments; y++)
	{
		for(x = 0 ; x < segments; x++)
		{
			struct fxVec2 pos;
			pos.x = origin.x + x * xstr - w / 2 + xstr / 2;
			pos.y = origin.y + y * ystr - h / 2 + ystr / 2;
			particle = _vrltParticlePushToObj(v, obj);
			SDK_NULL_ASSERT(particle);
			_vrltInitParticle(particle, &pos, NULL);
			if (x > 0)
			{
				_vrltDistanceConstraintPushToObj(v, obj, y * segments + x, y * segments + x - 1, stiffness, NULL);
			}
			if (y > 0)
			{
				_vrltDistanceConstraintPushToObj(v, obj, y * segments + x, (y - 1) * segments + x, stiffness, NULL);
			}
		}
	}

	for(x = 0; x < segments; x++)
	{
		if(x % pinMod == 0)
		{
			_vrltPinConstraintPushToObj(v, obj, x, NULL);
		}
	}

	obj->mState = VOS_READY;
	return v->mObjectsPoolCount - 1;
}
//---------------------------------------------------------------------------

s32 vrltCreatePoint(struct Verlet *v, struct fxVec2 pos)
{
	struct VTObject *obj;
	struct VTParticle* particle;

	SDK_NULL_ASSERT(v);
	obj = _vrltObjectPop(v);
	SDK_NULL_ASSERT(obj);

	particle = _vrltParticlePushToObj(v, obj);
	SDK_NULL_ASSERT(particle);
	_vrltInitParticle(particle, &pos, NULL);
	
	obj->mState = VOS_READY;
	return v->mObjectsPoolCount - 1;
}
//---------------------------------------------------------------------------

s32 vrltCreateLine(struct Verlet *v, struct fxVec2 *vertices, s32 verticesCount, fx32 stiffness)
{
	s32 i;
	struct VTObject *obj;
	struct VTParticle* particle;

	SDK_NULL_ASSERT(v);
	obj = _vrltObjectPop(v);
	SDK_NULL_ASSERT(obj);

	for (i = 0; i < verticesCount; i++)
	{
		particle = _vrltParticlePushToObj(v, obj);
		SDK_NULL_ASSERT(particle);
		_vrltInitParticle(particle, &vertices[i], NULL);
		if (i > 0)
		{
			_vrltDistanceConstraintPushToObj(v, obj, i, i - 1, stiffness, NULL);
		}
	}
	
	obj->mState = VOS_READY;
	return v->mObjectsPoolCount - 1;
}
//---------------------------------------------------------------------------

s32 vrltBeginInitObject(struct Verlet *v)
{
	struct VTObject *obj;
	SDK_NULL_ASSERT(v);
	obj = _vrltObjectPop(v);
	SDK_NULL_ASSERT(obj);
	obj->mState = VOS_INIT;
	return v->mObjectsPoolCount - 1;
}
//---------------------------------------------------------------------------

void vrltInitObject_AddParticle(struct Verlet *v, s32 obj_id, struct fxVec2 pos, const s32 *custom_data)
{
	if(obj_id < v->mObjectsPoolCount && v->mppObjectsPool[obj_id]->mState == VOS_INIT)
	{
		struct VTParticle* particle = _vrltParticlePushToObj(v, v->mppObjectsPool[obj_id]);
		SDK_NULL_ASSERT(particle);
		_vrltInitParticle(particle, &pos, custom_data);
	}
}
//---------------------------------------------------------------------------

void vrltInitObject_AddPinConstraint(struct Verlet *v, s32 obj_id, s32 particle_idx, struct fxVec2 *pos)
{
	if(obj_id < v->mObjectsPoolCount && v->mppObjectsPool[obj_id]->mState == VOS_INIT)
	{
		_vrltPinConstraintPushToObj(v, v->mppObjectsPool[obj_id], particle_idx, pos);
	}
}
//---------------------------------------------------------------------------

void vrltInitObject_AddDistanceConstraint(struct Verlet *v, s32 obj_id, s32 particle_idx1, s32 particle_idx2, fx32 stiffness, fx32 *distance)
{
	if(obj_id < v->mObjectsPoolCount && v->mppObjectsPool[obj_id]->mState == VOS_INIT)
	{
		_vrltDistanceConstraintPushToObj(v, v->mppObjectsPool[obj_id], particle_idx1, particle_idx2, stiffness, distance);
	}
}
//---------------------------------------------------------------------------

void vrltInitObject_AddAngleConstraint(struct Verlet *v, s32 obj_id, s32 particle_idx1, s32 particle_idx2, s32 particle_idx3, fx32 stiffness)
{
	if(obj_id < v->mObjectsPoolCount && v->mppObjectsPool[obj_id]->mState == VOS_INIT)
	{
		_vrltAngleConstraintPushToObj(v, v->mppObjectsPool[obj_id], particle_idx1, particle_idx2, particle_idx3, stiffness);
	}
}
//---------------------------------------------------------------------------

void vrltEndInitObject(struct Verlet *v, s32 obj_id)
{
	if(obj_id < v->mObjectsPoolCount && v->mppObjectsPool[obj_id]->mState == VOS_INIT)
	{
		v->mppObjectsPool[obj_id]->mState = VOS_READY;
	}
}
//---------------------------------------------------------------------------

const struct fxVec2* vrltGetObjectParticlePosition(struct Verlet *v, s32 obj_id, s32 particle_idx)
{
	if(obj_id < v->mObjectsPoolCount && particle_idx < v->mppObjectsPool[obj_id]->mParticlesCount)
	{
		return &v->mppObjectsPool[obj_id]->mppParticles[particle_idx]->pos;
	}
	return NULL;
}
//---------------------------------------------------------------------------
