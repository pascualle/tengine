/*
tengine - cross-platform game engine
------------------------------------

Copyright (c) 2013 Papa Pascualle
Contact Email: pascualle@gmail.com

Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
*/

#ifndef _T_SOUND_H_
#define _T_SOUND_H_

#include "platform.h"

#ifdef __cplusplus
extern "C" {
#endif

//----------------------------------------------------------------------------------------------------------------

struct SoundHandle;

enum SoundType
{
    SOUND_TYPE_NONE = -1,
    SOUND_TYPE_SFX,
    SOUND_TYPE_BGM1,
    SOUND_TYPE_BGM2
};

struct SoundData
{	
	enum SoundType Type;
	s32  Id;
	fx32 Volume;
	BOOL Looped;
};

//----------------------------------------------------------------------------------------------------------------

#ifdef NITRO_SDK 
void sndInitSoundSystem(u32 iHeapSize, const char *ipSoundArchivePath);
#else
void sndInitSoundSystem(void);
#endif
void sndReleaseSoundSystem(void);

struct SoundHandle* sndPlay(const struct SoundData *ipSoundData);

BOOL sndIsBGMPlaying(void);

void sndStop(struct SoundHandle* ipHandle);
void sndStopAll(void);	

void sndPause(struct SoundHandle* ipHandle, BOOL iPause);	
void sndPauseAll(BOOL iPause);

void sndSetVolume(struct SoundHandle* ipHandle, fx32 iVolume);
fx32 sndGetVolume(const struct SoundHandle* ipHandle);

enum SoundType sndGetSoundType(const struct SoundHandle* ipHandle);
s32 sndGetSoundId(const struct SoundHandle* ipHandle);

//----------------------------------------------------------------------------------------------------------------

#ifdef __cplusplus
} /* extern "C" */
#endif

#endif