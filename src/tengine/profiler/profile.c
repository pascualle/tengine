/*
tengine - cross-platform game engine
------------------------------------

Copyright (c) 2013 Papa Pascualle
Contact Email: pascualle@gmail.com

Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
*/

#include "profile.h"
#include "lib/platform_low.h"
#include "static_allocator.h"
#include "containers/allocator_array.h"
#include "containers/allocator_list.h"

//---------------------------------------------------------------------------

static struct AllocatorArray _profile_labels;
static struct AllocatorList _profile_stack;
static u16 _profile_clevel = 0;
static u32 _profile_stime = 0;
static struct StaticAllocator *_profile_allocator = NULL;

//---------------------------------------------------------------------------

struct _profile_record
{
	char* label;
	u32 startTime;
	u32 endTime;
	u16 level;
};

//---------------------------------------------------------------------------

void _profile_inner_init(struct StaticAllocator* allocator)
{
	_profile_allocator = allocator;
}
//---------------------------------------------------------------------------

void _profile_inner_start()
{
	SDK_NULL_ASSERT(_profile_allocator);
	A_ARRAY_INIT(struct _profile_record, _profile_labels, *_profile_allocator);
	A_LIST_INIT(u32, _profile_stack, *_profile_allocator);
	_profile_stime = Platform_GetTime();
}
//---------------------------------------------------------------------------

void _profile_inner_begin(const char* label)
{
	struct _profile_record crec;
	u32 lastIndex;
	_profile_clevel++;
	crec.label = (char*)label;
	crec.startTime = Platform_GetTime();
	crec.level = _profile_clevel;
	A_ARRAY_PUSH_BACK(struct _profile_record, _profile_labels, crec);
	lastIndex = A_ARRAY_SIZE(_profile_labels) - 1;
	A_LIST_PUSH_BACK(u32, _profile_stack, lastIndex);
}
//---------------------------------------------------------------------------

void _profile_inner_end()
{
	struct ListItem* item = A_LIST_TAIL(u32, _profile_stack);
	u32 rec_index = A_LIST_VAL(u32, _profile_stack, item);
	struct _profile_record* raw_array = (struct _profile_record*)_profile_labels.ptr;
	raw_array[rec_index].endTime = Platform_GetTime();
	_profile_clevel--;
	A_LIST_ERASE(u32, _profile_stack, item);
}
//---------------------------------------------------------------------------

void _profile_inner_finish()
{
	u32 i;
	struct _profile_record* raw_array = (struct _profile_record*)_profile_labels.ptr;

	OS_Printf("===================== RECORD START ====================\n");
	for(i = 0; i < A_ARRAY_SIZE(_profile_labels); i++)
	{
		s32 l;
		struct _profile_record crec = raw_array[i];
		OS_Printf("PROFILER: ");
		for(l = 0; l < crec.level; l++)
		{
			OS_Printf("==");
		}
		OS_Printf("|>\"%s\" start at %u and work %u milisecconds. \n",
					crec.label,
					(_profile_stime - crec.startTime),
					(crec.endTime - crec.startTime));
	}

	OS_Printf("===================== RECORD END ======================\n");

	A_ARRAY_CLEAR(struct _profile_record, _profile_labels);
	A_LIST_CLEAR(u32, _profile_stack);
}
//---------------------------------------------------------------------------



