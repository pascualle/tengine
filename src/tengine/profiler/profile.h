/*
tengine - cross-platform game engine
------------------------------------

Copyright (c) 2013 Papa Pascualle
Contact Email: pascualle@gmail.com

Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
*/

#ifndef PROFILE_H
#define PROFILE_H

#include "platform.h"

#ifdef __cplusplus
extern "C" {
#endif

/*
---------------------------------------------------------------------------
usage:
------
 1. add PROFILER_ENABLE to global preprocessor keys

 2. setup PROFILE_INIT, PROFILE_START and PROFILE_FINISH to main unit (see tengine\src\_win32\win32app.cpp as example)

	void main()
	{
		PROFILE_INIT(allocator)
		...
		code
		...
		while(mainloop)
		{
			PROFILE_START;
			...
			game main loop
			...
			PROFILE_FINISH;
		}
		...
		exit(0);
	}


 3. wrap the desired function with PROFILE_ADD (between PROFILE_START and PROFILE_FINISH looping)

	void some_foo(int a, int b);

	void foo()
	{
		// calling some_foo function without profiling
		some_foo(a, b);
		
		// calling some_foo with profiling
		PROFILE_ADD(some_foo, "message for some_foo profiling", a, b);
	}
---------------------------------------------------------------------------
*/


struct StaticAllocator;

#define FUNCTION_NAME(name) #name

void _profile_inner_init(struct StaticAllocator* allocator);
void _profile_inner_start(void);
void _profile_inner_begin(const char* name);
void _profile_inner_end(void);
void _profile_inner_finish(void);

#ifdef PROFILER_ENABLE
#define PROFILE_INIT(ALLOCATOR) _profile_inner_init(&(ALLOCATOR))
#ifdef ANDROID_NDK
#define PROFILE_ADD(f, m, ... ) _profile_inner_begin(FUNCTION_NAME(f));\
	f(__VA_ARGS__);\
	_profile_inner_end()
#else
#define PROFILE_ADD(f, m, ... ) _profile_inner_begin(FUNCTION_NAME(f)##"("##FUNCTION_NAME(__VA_ARGS__)##")/*"##m##"*/");\
	f(__VA_ARGS__);\
	_profile_inner_end()
#endif
#define PROFILE_START _profile_inner_start()
#define PROFILE_FINISH _profile_inner_finish()
#else
#define PROFILE_INIT(ALLOCATOR)
#define PROFILE_ADD(f, m, ... ) f(__VA_ARGS__);
#define PROFILE_START
#define PROFILE_FINISH
#endif

#ifdef __cplusplus
} /* extern "C" */
#endif

#endif //PROFILE_H
