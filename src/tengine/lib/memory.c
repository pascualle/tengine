/*
tengine - cross-platform game engine
------------------------------------

Copyright (c) 2013 Papa Pascualle
Contact Email: pascualle@gmail.com

Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
*/

#include "system.h"
#include "memory.h"
#include "texts.h"
#ifdef USE_STATIC_MEMORY
#include "static_allocator.h"
#endif
#ifdef _MSC_VER
#include "intrin.h"
#endif
#if defined NIX_APP || defined IOS_APP || defined EMSCRIPTEN_APP || defined KOLIBRIOS_APP || defined ANDROID_NDK
#include "string.h"
#endif

#ifdef NITRO_SDK
static const OSHeapHandle HEAP_ID = 0;
static const OSArenaId ARENA_ID = OS_ARENA_MAIN;
#endif

#ifdef SDK_DEBUG
#ifdef FRAME_ALLOCATOR 
static char gPtNames[PTCOLLECTOR_SIZE][PTCOLLECTOR_MARK_SIZE];
static void* gPtCollector[PTCOLLECTOR_SIZE] = {0};
static s32 gPtCollectorTail = -1;
static u32 gsDbgMMS = 0;
static u32 gSzCollector[PTCOLLECTOR_SIZE];
#endif
#endif

#ifdef USE_STATIC_MEMORY
static struct StaticAllocator *gspMainSystemAllocator = NULL;
#endif

//-------------------------------------------------------------------------------------------

#ifdef USE_STATIC_MEMORY

void InitMemoryAllocator(struct StaticAllocator *allocator)
{
	gspMainSystemAllocator = allocator;
}

#else

void InitMemoryAllocator()
{
#ifdef NITRO_SDK
	void*    arenaLo;
    void*    arenaHi;

    OS_Init();

    arenaLo = OS_GetArenaLo( ARENA_ID );
    arenaHi = OS_GetArenaHi( ARENA_ID );
    // Create a heap
    arenaLo = OS_InitAlloc( ARENA_ID, arenaLo, arenaHi, 1 );
    OS_SetArenaLo( ARENA_ID, arenaLo );
    // Ensure boundaries are 32B aligned
    arenaLo = (void*)MATH_ROUNDUP( (u32)arenaLo, 32 );
    arenaHi = (void*)MATH_ROUNDUP( (u32)arenaHi, 32 );
    // The boundaries given to OSCreateHeap should be 32B aligned
    OS_SetCurrentHeap( ARENA_ID, OS_CreateHeap( ARENA_ID, arenaLo, arenaHi ) );
    // From here on out, OS_Alloc and OS_Free behave like malloc and free respectively
    OS_SetArenaLo( ARENA_ID, arenaLo = arenaHi );
#endif
}
#endif
//-------------------------------------------------------------------------------------------

#ifdef STATIC_HEAP_MEM_DEBUG_DEEP
void DebugMemoryCheck()
{
	sh_debug_check(&gsStaticMemoryHeap);
}
#endif

void* MALLOC(u32 iSize, const char iMarkName[PTCOLLECTOR_MARK_SIZE])
{
    if(iSize == 0)
    {
        return NULL;
    }
#ifdef USE_STATIC_MEMORY
	SDK_NULL_ASSERT(gspMainSystemAllocator);
#endif
#ifdef SDK_DEBUG
    {
		void* pPt =
#ifdef NITRO_SDK
#ifdef USE_STATIC_MEMORY
		sh_malloc(&gsStaticMemoryHeap, iSize);
#else
		OS_AllocFromHeap(ARENA_ID, HEAP_ID, iSize);
#endif		
#else
#ifdef USE_STATIC_MEMORY
		StaticAllocator_Malloc(gspMainSystemAllocator, iSize);
#else
		malloc(iSize);
#endif
#endif
		SDK_NULL_ASSERT(pPt); //something with memory
#ifdef FRAME_ALLOCATOR
        gPtCollectorTail++;
        SDK_ASSERT(gPtCollectorTail < PTCOLLECTOR_SIZE);
		gPtCollector[gPtCollectorTail] = pPt;
		gSzCollector[gPtCollectorTail] = iSize;
		gsDbgMMS += iSize;
		iSize = STD_StrLen(iMarkName);
		if(iSize >= PTCOLLECTOR_MARK_SIZE)
		{
			iSize = PTCOLLECTOR_MARK_SIZE - 1;
		}
		MI_CpuCopy8(iMarkName, gPtNames[gPtCollectorTail], iSize);
		gPtNames[gPtCollectorTail][iSize] = 0;
#else
		(void)iMarkName;
#endif
		return pPt;
    }
#else
		(void)iMarkName;
		return
#ifdef NITRO_SDK
		OS_AllocFromHeap(ARENA_ID, HEAP_ID, iSize);
#else
#ifdef USE_STATIC_MEMORY
		sh_malloc(&gsStaticMemoryHeap, iSize);
#else
		malloc(iSize);
#endif
#endif
#endif
}
//-------------------------------------------------------------------------------------------

void FREE(void* pPointer)
{
#ifdef USE_STATIC_MEMORY
	SDK_NULL_ASSERT(gspMainSystemAllocator);
#endif
#if defined SDK_DEBUG && defined FRAME_ALLOCATOR
	s32 i;
    SDK_NULL_ASSERT(pPointer); //null-pointer for delete
    for(i = gPtCollectorTail; i >= 0 ; i--)
    {
		if(gPtCollector[i] == pPointer)
        {
			if(pPointer != gPtCollector[gPtCollectorTail])
			{
				//  gPtNames[gPtCollectorTail]; // expected
				//  gPtCollector[i]; // current
				OS_Warning("FRAME_ALLOCATOR FREE() assertion, exp '%s' curr '%s'", gPtNames[gPtCollectorTail], gPtNames[i]);
				SDK_ASSERT(0); // pointer must be last in the row
#ifdef _MSC_VER
				__debugbreak();
#endif
			}
#ifdef NITRO_SDK
			OS_FreeToHeap(ARENA_ID, HEAP_ID, pPointer);
#else
#ifdef USE_STATIC_MEMORY
			sh_free(&gsStaticMemoryHeap, pPointer);
#else
			free(pPointer);
#endif
#endif
			gPtCollector[gPtCollectorTail] = 0;
			gsDbgMMS -= gSzCollector[gPtCollectorTail];
			gPtCollectorTail--;
            return;
        }
    }
	SDK_ASSERT(0); //trying to delete pointer without MALLOC
#ifdef _MSC_VER
	__debugbreak();
#endif
#else
#ifdef NITRO_SDK
#ifdef USE_STATIC_MEMORY
	sh_free(&gsStaticMemoryHeap, pPointer);
#else
	OS_FreeToHeap(ARENA_ID, HEAP_ID, pPointer);
#endif	
#else
#ifdef USE_STATIC_MEMORY
	SDK_NULL_ASSERT(gspMainSystemAllocator);
	StaticAllocator_Free(gspMainSystemAllocator, pPointer);
#else
	free(pPointer);
#endif
#endif
#endif
}
//-------------------------------------------------------------------------------------------

u32 GetFreeMemorySize()
{
#ifdef NITRO_SDK
	return OS_GetTotalFreeSize(ARENA_ID, HEAP_ID);
#else
 #ifdef WINDOWS_APP
    MEMORYSTATUSEX status; 
    status.dwLength = sizeof(status); 
    GlobalMemoryStatusEx(&status); 
    return (u32)status.ullAvailVirtual;
 #else
	return 0;
 #endif
#endif
}
//-------------------------------------------------------------------------------------------

#if defined SDK_DEBUG && defined FRAME_ALLOCATOR
u32 GetTotalMALLOCSizeDbg()
{
	return gsDbgMMS;
}
#endif
//-------------------------------------------------------------------------------------------

#ifndef NITRO_SDK

void MI_CpuFill8(void *dest, u8 val, s32 sz)
{
	memset(dest, val, sz);
}
//---------------------------------------------------------------------------

void MI_CpuFill16(void *dest, s16 val, s32 sz)
{
	short* mem = (short*)dest;
	sz /= sizeof(s16);
	while(sz)
	{
		*mem = val;
		mem++;
		sz--;
	}
}
//---------------------------------------------------------------------------

void MI_CpuClear8(void *dest, s32 sz)
{
	memset(dest, 0, sz);
}
//---------------------------------------------------------------------------

void MI_CpuClear32(void *dest, s32 sz)
{
	memset(dest, 0, sz);
}
//---------------------------------------------------------------------------

void MI_CpuClearFast(void *dest, s32 sz)
{
	memset(dest, 0, sz);
}
//---------------------------------------------------------------------------

void MI_CpuCopy8(const void *src, void *dest, u32 sz)
{
	memcpy(dest, src, sz);
}
//---------------------------------------------------------------------------

void MI_CpuCopy16(const void *src, void *dest, u32 sz)
{
	memcpy(dest, src, sz);
}
//---------------------------------------------------------------------------

void MI_CpuCopy32(const void *src, void *dest, u32 sz)
{
	memcpy(dest, src, sz);
}
//---------------------------------------------------------------------------

void MI_CpuCopyFast(const void *src, void *dest, u32 sz)
{
	memcpy(dest, src, sz);
}
//---------------------------------------------------------------------------
#endif

#ifdef __cplusplus
#ifndef IOS_APP
  //I don't know how to isolate overloaded operators new/delete from objective-c
#if defined NIX_APP || defined EMSCRIPTEN_APP || defined NITRO_SDK || defined KOLIBRIOS_APP
#define THROW_BAD_ALLOC throw(std::bad_alloc)
#define THROW_EMPTY throw()
#else
  //Android doesn't fully support exceptions, so its <new> header
  //has operators that don't specify throw() at all. Also include MSVC
  //to suppress build warning spam
#define THROW_BAD_ALLOC
#define THROW_EMPTY
void* operator new(std::size_t isize, const std::nothrow_t&) THROW_EMPTY
{
	return MALLOC(isize, "new(nothrow_t)");
}
void operator delete(void* ptr, const std::nothrow_t&) THROW_EMPTY
{
	FREE(ptr);
}
void* operator new[](std::size_t isize, const std::nothrow_t&) THROW_EMPTY
{
	return MALLOC(isize, "new[](nothrow_t)");
}
void operator delete[](void* ptr, const std::nothrow_t&) THROW_EMPTY
{
	FREE(ptr);
}
#endif

void* operator new(std::size_t isize) THROW_BAD_ALLOC
{
	return MALLOC(isize, "new");
}
void operator delete(void* ptr) THROW_EMPTY
{
	FREE(ptr);
}
void* operator new[](std::size_t isize) THROW_BAD_ALLOC
{
	return MALLOC(isize, "new[]");
}
void operator delete[](void* ptr) THROW_EMPTY
{
	FREE(ptr);
}
#undef THROW_BAD_ALLOC
#undef THROW_EMPTY
#endif
#endif
//---------------------------------------------------------------------------