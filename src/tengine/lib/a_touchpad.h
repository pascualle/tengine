#ifndef A_TOUCH_PAD_H
#define A_TOUCH_PAD_H

#include "platform.h"

#if defined ANDROID_NDK || defined  WINDOWS_APP || defined  NIX_APP || defined IOS_APP || defined EMSCRIPTEN_APP || defined KOLIBRIOS_APP

#ifdef __cplusplus
extern "C" {
#endif

void onTouchPadDown(u8 id, s32 x, s32 y);
void onTouchPadUp(u8 id, s32 x, s32 y);
void onTouchPadMove(u8 id, s32 x, s32 y);

#ifdef __cplusplus
} /* extern "C" */
#endif

#endif
#endif
