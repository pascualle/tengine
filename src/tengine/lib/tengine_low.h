#ifndef TENGINE_LOW_H
#define TENGINE_LOW_H
//---------------------------------------------------------------------------

#include "tengine.h"

#ifdef __cplusplus
extern "C" {
#endif

struct TERFont;
struct TERSound;
struct JOBAnimStreamTask;
struct AllocatorList;
struct StaticAllocator;
struct AlphabetTextureOffsetMap;

#define ONE_SECOND_MS_FX FX32(1000)
#define DELAY_LIMIT_MS_FX ONE_SECOND_MS_FX
#define FIXED_TIMESTEP_MS_FX FX_Div(FX32(1000), FX32(60))
#define DEFAULT_DELAY_MS_FX FX_Div(FX32(1000), FX32(30))

// do not renumerate any index!
enum
{
	NULL_IMG_IDX = 65535,
	MAX_RES_IDX = 65535,

	DISPLAY_VIEW_W_MAX = 65535,
	DISPLAY_VIEW_H_MAX = 65535,

	LOGIC_ZONES_MAX = 64,

	UEOF_EMPTY = 0,
	UEOF_READY = 1,
	UEOF_SKIP_FRAME1 = 2,
	UEOF_SKIP_FRAME2 = 3,
	UEOF_SKIP_FRAME3 = 4,
	UEOF_COUNT,

	IPARENT_IDX = 0,
	IZONE,
	ISTATE,
	ITEMPSTATE,
	IANIFRIDX,
	IANIENDFLAG,
	ITIMER,
	PR_IDATA_COUNT,

	/*FX*/
	/*FY*/
	FLEFT = 2,
	FTOP,
	FCURRWIDTH,
	FCURRHEIGHT,
	PR_FDATA_COUNT,

	/*FX*/
	/*FY*/
	NODE_FDATA_COUNT = 2,

	ILINKTOOBJ = 0,
	INODE = 1,
	IPREVNODE = 2,
	ICLIPRECTOBJ = 3,
	ICUSTOMALPHA = 4,
	ICUSTOMBLENDCOLOR = 5,
	ICUSTOMFLAGS = 6,
	ICLIPRECTTEMPXY = 7,
	ICLIPRECTTEMPWH = 8,
	IJOINNODEBEGIN = 9,
	PR_C_FIXEDIDATA_COUNT = 9,

	FCBLEFT0 = 0,
	FCBTOP0,
	FCBRIGHT0,
	FCBBOTTOM0,
	FVBLEFT0,
	FVBTOP0,
	FVBRIGHT0,
	FVBBOTTOM0,
	FX0_NODE,
	FY0_NODE,
	FDISTLENGHT,
#ifdef CUSTOMAFFINETRANSFORM_SUPPORT
	FCUSTOMROTATIONVALUE,
	FCUSTOMSCALEXVALUE,
	FCUSTOMSCALEYVALUE,
	FROTSIN,
	FROTCOS,
	FCBLTX,
	FCBLTY,
	FCBRTX,
	FCBRTY,
	FCBRBX,
	FCBRBY,
	FCBLBX,
	FCBLBY,
	FVBLTX,
	FVBLTY,
	FVBRTX,
	FVBRTY,
	FVBRBX,
	FVBRBY,
	FVBLBX,
	FVBLBY,
#endif
	PR_C_FDATA_COUNT,

	ICUSTOMROTATIONFLAG = 0x1,
	ICUSTOMALPHAFLAG = 0x2,
	ICUSTOMSCALEFLAG = 0x4,
	IRECALCULATELOGICRECTSFLAG = 0x8,

	RES_IMG_IDX = 0,
	RES_X = 1,
	RES_Y = 2,
	RES_W = 3,
	RES_H = 4,
	resobjprpCount = 5,
	resfrprpCount = 3,
	TILE_PRP = 0,
	TILE_LAYERS = 1,
	tileprpCount = 2,

	ANIVROFFXLT = 0,
	ANIVROFFYLT = 1,
	ANIVROFFXRB = 2,
	ANIVROFFYRB = 3,
	ANILOOP = 4,
	ANIFRCOUNT = 5,
	ppranidataCOUNT = 6,

	ANIFRCROFFXLT = 0,
	ANIFRCROFFYLT = 1,
	ANIFRCROFFXRB = 2,
	ANIFRCROFFYRB = 3,
	ANIFRANGLE = 4,
	ANIFROFFL = 5,
	ANIFROFFT = 6,
	ANIFRW = 7,
	ANIFRH = 8,
	ANIFRTIME = 9,
	ANIEVENTID = 10,
	ANIFRFRCOUNT = 11,
	ANIFRJNBEGIN = 12,
	ppranifrfixeddataCOUNT = 12,

	ANIFRFRID = 0,
	ANIFRFROFFX = 1,
	ANIFRFROFFY = 2,
	ANIFRFRALPHA = 3,
	ANIFRFRDATA = 4,
	ANIFRFRWIDTH = 5,
	ANIFRFRHEIGHT = 6,
	ppranifrfrdataCOUNT = 7,

	TXTSTRCOUNT = 0,
	TXTCHRCOUNT = 1,
	TXTWIDTH = 2,
	TXTHEIGHT = 3,
	TXTMARGIN = 4,
	TXTCOLOR = 5,
	TXTFONTID = 6,
	pprtxtdataCOUNT = 7
};

enum
{
	TRIG_MAP_TYPE = 0,
	TRIG_PROPERTIES_TYPE = 1
};

enum
{
	ITRIG_TYPE = 0,
	ITRIG_TARGET,
	ITRIG_MDIR_PTILE,
	ITRIG_MIDX_POPER,
	ITRIG_MCOUNT_POBJ,
	trprpidataCOUNT,
	
	/*FX*/
	/*FY*/
	trprpfdataCOUNT = 2
};

enum
{
	TRIG_UP_DIR = 2,
	TRIG_DOWN_DIR = 8,
	TRIG_LEFT_DIR = 4,
	TRIG_RIGHT_DIR = 6
};

enum
{
	TRIG_CHANGE_PRP_TRANSMIT = 0,
	TRIG_CHANGE_PRP_DECREASE = 1,
	TRIG_CHANGE_PRP_INCREASE = 2
};

enum
{
	TRIG_CHANGE_PRP_INITIATOR = -1,
	TRIG_CHANGE_PRP_SECOND = -2,
	TRIG_CHANGE_PRP_OPPOSITESTATE = 127,
	TRIG_CHANGE_PRP_BLANKACTION = 127,

	LR_COLLIDEW = 0,
	LR_COLLIDEH = 1,
	LR_VIEWW = 2,
	LR_VIEWH = 3,
	LR_COUNT = 4,
	NODE_RECALCULATE_DIST_FLAG = 4,
	NODE_ID_TRIGGER,
	NODE_ID_EVENT,
	NODE_IDATA_COUNT,

	SPTTYPE_NONE = 0,
	SPTTYPE_COLLIDE = 1,
	SPTTYPE_DIRECTOR = 2,
	SPTTYPE_PROPERTY = 3,

	IMULTI = 0,
	ISWITCHTO = 1,
	IGOTOZONE = 2,
	IENDLEVEL = 3,
	IMESSAGE = 4,
	IINITIATORPRP = 5,
	IINITIATORPVAL = 6,
	IINITIATORPOPER = 7,
	IINITIATORSTATE = 8,
	IINITIATORSOPER = 9,
	ISECONDPRP = 10,
	ISECONDPVAL = 11,
	ISECONDPOPER = 12,
	ISECONDSTATE = 13,
	ISECONDSOPER = 14,
	ISPTTYPE = 15,
	sptprpCOUNT = 16
};

enum
{
	SMT_LESSEQ = 0, // <=
	SMT_EQUAL = 1, // ==
	SMT_MOREEQ = 2, // >=
	SMT_NOT = 3 // !=
};

enum
{
	SAVE = 1,
	OPT = 2,

	OBJ_GAME = 0,
	OBJ_BACK = 1,
	OBJ_FORE = 2,
	OBJ_COUNT = 3,

	CHECK_OFFSET = 1,

	stIDANIMATION = 0,
	stDIRECTION = 1,
	stCOUNT = 2,

	PARENT_TEXT_OBJ = 2
};

enum
{
	RES_TYPE_NONE = 0,
	RES_TYPE_IMAGE = 1,
	RES_TYPE_FONT = 2,
	RES_TYPE_SOUND = 3
};

enum
{
	PCS_OFFX = 2,
	PCS_OFFY,
	PCS_W,
	PCS_H,
	PCS_SPEED,
	PCS_TGTX,
	PCS_TGTY,
	PCS_LIFE,
	PCS_RESID,
	PCS_TGTX0,
	PCS_TGTY0,
	PCS_EXPLODE,
	PCS_TIMER0,
	PCS_DISTLENGHT,
	PCS_ALPHA,
	PCS_ADDITIONAL,
#ifdef CUSTOMAFFINETRANSFORM_SUPPORT
	PCS_ROTATIONORIGX,
	PCS_ROTATIONORIGY,
	PCS_SIN,
	PCS_COS,
	PCS_DW,
	PCS_DH,
#endif
	PCS_COUNT,
	PCS_DEF_POOL_SIZE = 1024
};
//---------------------------------------------------------------------------

struct AnimStreamData
{
	const char *mpFilename;
	struct BMPImage *mpImage;
	BOOL mBufferStatus;
	BOOL mBindStatus;
	u16 mFrpcdata[resobjprpCount];
};
//---------------------------------------------------------------------------

struct FileStreamData
{
	const char *mpFilename;
	struct BMPImage *mpImage;
	u8 *mpFile;
	BOOL mStatus;
	u16 mIdx;
	u8 mValue;
	u8 mMode;
};
//---------------------------------------------------------------------------

struct TEngineInstance
{
	u8 *data_file;

	u16 *tilemap;
	u16 *t_tilemap;
#ifdef USE_TILEMAP_ATTRIBUTES
	u32 *tileattrmap;
#endif
	const s16 **tile_data;
	const u16 ***fr_img;
	const u16 *map_img;
	const u16 *map_fnt;
	const u16 *map_snd_sfx;
	const u16 *map_snd_bgm;

	const u16* cld_sh_nodes_ct;
	const struct fxVec2 **cld_sh_node;
	u8 cld_shape_ct;

	s32 ds_plane_fr_array_beg[BGSELECT_NUM];
	s32 ds_plane_fr_array_end[BGSELECT_NUM];

	u32 pr_arr_off[OBJ_COUNT];
	s32 **trig_idata;

	u16
	**pr_spt,
	**nodes,
	**spt_cldobjlist,
	**spt_bdata,
	**spt_triglist,
	**trig_spt;
	u16 **pr_textdata;

	u8 **trig_st;

	fx32
	**pr_prp,
	**trig_prp,
	**pr_fdata,
	**pr_cfdata,
	**nodes_fdata,
	**trig_fdata;

	char **spt_cldgrouplist;
	wchar ***pr_textdatatext;

	u32
	*spt_cldgrouplist_ct,
	*spt_cldobjlist_ct,
	*spt_triglist_ct,
	*trig_st_ct,
	*trig_prp_ct,
	*pr_textdataalign;

	s32
	** pr_idata,
	** pr_cidata;

	struct AllocatorList** rndr_znlist;
	struct StaticAllocator** rndr_znlist_allocator;
	u8** rndr_znlist_heap;

	s16
	**df_znlist,
	**db_znlist,
	*db_count,
	*df_count,
	*pr_count;

	enum BGSelect bgType;
	s32 visible_mask;
	s32 t_visible_mask;
	s32 ueof_state;

    u16 map_img_ct;
    u16 map_fnt_ct;
    u16 map_snd_sfx_ct;
    u16 map_snd_bgm_ct;
	s32 pr_idata_ct;
	s32 all_pr_count;
	s32 zones_ct;
	s32 nodes_ct;
	s32 trig_common_count;
	s32 script_common_count;
	s32 pnt_count;
    u16 ld_img_res_ct;
    u16 ld_fnt_res_ct;
    u16 ld_snd_sfx_res_ct;
    u16 ld_snd_bgm_res_ct;
	u16 maxbgres;

	s32 map_h;
	s32 map_w;
	s32 d_w;
	s32 d_h;
	fx32 l_w;
	fx32 l_h;
	fx32 logicWidthHalf_fx;
	fx32 logicHeightHalf_fx;
	s32 sz_map;
	s32 sz_mapH;
	s32 sz_mapW;
	fx32 worldx_fx;
	fx32 worldy_fx;
	fx32 sdx_fx;
	fx32 sdy_fx;
	s32 current_zone;
	s32 t_current_zone;
#ifdef USE_CUSTOM_RENDER
	fx32 shiftTilesMax_fx;
	fx32 cx_fx[BGSELECT_NUM];
	fx32 cy_fx[BGSELECT_NUM];
	fx32 t_c_dx_fx[BGSELECT_NUM];
	fx32 t_c_dy_fx[BGSELECT_NUM];
#endif
	fx32 tcx_fx[BGSELECT_NUM];
	fx32 tcy_fx[BGSELECT_NUM];
	s32 tc_dx8[BGSELECT_NUM];
	s32 tc_dy8[BGSELECT_NUM]; 
	BOOL primary;

	fx32 **pcs_fdata;
	s32 particlesCount;
	struct fxVec2 c_fx;

	// callbacks
#ifdef TENGINE_LEGACY_CODE
	OnScriptCallback _onScriptCB;
#endif
	OnChangeHeroCallback _onChangeHeroCB;
};
//---------------------------------------------------------------------------

enum ALState
{
	ALS_STATE_READY = 0,
	ALS_STATE_LIST,
	ALS_STATE_LOAD_DATA
};
// ----------------------------------------------------------------------------------

typedef void (*UpdateEngineObjFn)(struct TEngineInstance* ei, u32 layer, s32 ms);

// ----------------------------------------------------------------------------------

struct TEngineCommonData
{
	s32 states_ct;
	s32 ppr_count;
	s32 lang_id;
	fx32 deltatime_scale;
	fx32 delay_fx;
	u16 p_w;
	u16 p_h;
	u16 tl_layers_ct;
	u16 res_img_ct;
	u16 res_fnt_ct;
	u16 res_snd_ct;
	u16 anim_ct;
	u16 jn_ct;
	BOOL rl_available;
	BOOL pause_engine;

	u16 spd_prp_id;
	u16 enable_prp_id;

	struct InitEngineParameters initParams;
	struct TEngineInstance **instances;
	u32 a_layer;
	struct TEngineInstance *ainst;

	const char **res_img_fnames;
	const char **res_fnt_fnames;
	const char **res_snd_fnames;
	const u8 **states;
	const u8 *states_map;
	s16 *ppr_decortype;
	u8 *common_data_file;
	s16 ***ppr_anim_data;
	const fx32 ****ppr_fr_data;
	const s16 *****ppr_frfr_data;
	const u16 **fr_data;
	const u8 *common_data_tr_pt;
	const u8 *common_data_sp_pt;
	u8 *text_data_file;
	const wchar ***textdatatext;
	u16 *textdatatext_sct;

	struct JOBAnimStreamTask ***strm_obj_data;
	const char **res_strm_names;
	struct TEngineInstance *strm_instance;
	const struct TEngineInstance *snd_instance;
	s32 strm_type_ct;
	s32 strm_obj_ct;

	u32 *ld_p1;
	u16 *ld_p2;
	s16 *ld_task;
	s16 ld_ct;
	s16 ld_ct_t;
	enum ALState ld_st;
	BOOL ag_st;
	s32 ld_param;
	s32 agrl_s;
	s32 agrl_i;
	u16 glstrmidx;
	s32 agrl_strmprid;
	s32 agrl_strmaid;
	s32 agrl_strmbfi;

	s32 last_fps_tick_ct;
	s32 fps_tick_ct;
	fx32 fps_timer_fx;

	s32 wchar_sz;

	s32 trig_visible_mask;

	struct TERFont **res_fnt;
	struct BMPImage **res_img;
	struct TERSound **res_snd;

	BOOL lost_render_device_is_jobs_active;

	OnBeginUpdate _onBeginUpdate;
	OnDrawBackgroundTiles _onDrawBackgroundTiles;
	OnDrawObject _onDrawDecorationObject;
	OnDrawObject _onDrawGameObject;
	OnFinishUpdate _onFinishUpdate;
	OnEventCallback _onEventCB;
	UpdateEngineObjFn _updateEngineObjFn[UEOF_COUNT];
};
//---------------------------------------------------------------------------

void low_SetDummyDrawFunctions(void);
void low_RestoreDrawFunctions(void);

//---------------------------------------------------------------------------

const struct AllocatorList* low_getZoneGameObjList(u32 layer, s32 zone);

void low_addXYToPosition(s32 pr_id, fx32 xv, fx32 yv, u32 layer);

void low_getCollideRect(s32 pr_id, fx32 rect[RECT_SIZE], u32 layer);

void low_getViewRect(s32 pr_id, fx32 rect[RECT_SIZE], u32 layer);

BOOL low_getCurrentAnimationFrameInfo(s32 pr_id, struct GameObjectAnimationFrameInfo *const goafi, u32 layer);

void low_getAnimationFrameInfo(s32 gr_id, s32 anim_id, s32 frame_id, struct GameObjectAnimationFrameInfo *const goafi);

void low_setClipRect(s32 pr_id, s32 clip_pr_id, u32 layer);

//---------------------------------------------------------------------------

void _loadDataInit(void);
BOOL _nextPointAtLine(fx32 x0, fx32 y0, fx32 tx, fx32 ty, fx32 *l, fx32 speed, fx32 pos[2]);
const s16 *_getFramePieceData(s32 ppr_id, s32 anim_id, s32 frame_idx, s32 pc_idx);
void _prGetFramePiecesInfo(struct TEngineInstance *ei, s32 id, s32 *pc_count, s32 *anim_id);
void _updateAnimation(s32 pr_id, s32 ms);
void _updateLoadingProcess(void);
void _setActiveLayer(u32 layer);
void _setState(struct TEngineInstance *ei, s32 pr_id, s32 statedata, BOOL updateNodes);
void _processMovement(s32 pr);
const struct AlphabetTextureOffsetMap* _findTextureOffset(const struct TERFont* ipFont, wchar iChar, u16 arr[SRC_DATA_SIZE]);
struct TEngineInstance* _getInstance(u32 idx);
BOOL _isStreamFrame(s16 data);
fx32 _calcLenght(fx32 x0, fx32 y0, fx32 tx, fx32 ty);
void _calculateCXY8(struct TEngineInstance * i, fx32 *cx, fx32 *cy, s32 *c_dx, s32 *c_dy);
void _calculateLogicSizes(struct TEngineInstance *i, struct TEngineCommonData *cd);
//---------------------------------------------------------------------------

#ifdef __cplusplus
} /* extern "C" */
#endif

#endif /*TENGINE_LOW_H*/

