#ifndef _SOUND_LOW_H_
#define _SOUND_LOW_H_

#include "platform.h"

#ifdef __cplusplus
extern "C" {
#endif

#ifdef NITRO_SDK
 #include <nnsys/snd.h>
#endif
#ifdef USE_OPENAL_SOUND
 #if defined IOS_APP
  #include "OpenAL/al.h"
 #else
 #include "AL/al.h"
#endif
#endif
#ifdef USE_SLES_SOUND
 #include "SLES/OpenSLES.h"
 #include "SLES/OpenSLES_Android.h"
#endif
#include "sound.h"

//----------------------------------------------------------------------------------------------------------------

struct SoundHandle
{
    enum SoundType		Type;
#if defined NITRO_SDK 
	BOOL                Active;
	NNSSndHandle*		pSndHandle;
    NNSSndStrmHandle*	pBgmHandle;
#endif
#if defined USE_OPENAL_SOUND
	ALint				SndHandle;
    ALint				BgmHandle;
#endif
#if defined USE_SLES_SOUND
	SLint32				SndHandle;
    SLint32				BgmHandle;
#endif
#if defined USE_INFINITY_SOUND
	s32					SndHandle;
    s32					BgmHandle;
	s32					Channels;
	s16*				pBegin;
	s16*				pEnd;
#endif
#if defined USE_OPENAL_SOUND || defined USE_SLES_SOUND || defined USE_INFINITY_SOUND
	struct WavStreamFileStruct* pStreamData;
	s16					FHandle;
#endif
    u32					PlayingPos;
    u32					StrmLength;
    fx32				Volume;
	s32                 Id;
    BOOL                Looped;
};

struct TERSound
{
	s32 mResRef;
	s32 mThisRef;
#if defined USE_SLES_SOUND || defined USE_INFINITY_SOUND
	const u8* mpFileData;
#endif
	BOOL mInit;
};

BOOL sndIsSoundSystemInit(void);

void sndUpdateSoundStream(void);

void sndCreateDataBuffers(u32 sfxFileCount, u32 bgmFileCount);
void sndDeleteDataBuffers(void);

void sndLostDevice(void);
void sndRestoreDevice(void);

u8* sndLoadWavData(const char* pFname, s32 resId, BOOL isSfx);

void sndInitResource(const u8* data, struct TERSound *res);
BOOL sndIsInitedResource(struct TERSound *res);
void sndReleaseResource(struct TERSound *res);

//----------------------------------------------------------------------------------------------------------------

#ifdef __cplusplus
} /* extern "C" */
#endif

#endif