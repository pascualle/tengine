/*
tengine - cross-platform game engine
------------------------------------

Copyright (c) 2013 Papa Pascualle
Contact Email: pascualle@gmail.com

Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
*/

#include "tengine.h"
#include "filesystem.h"
#include "loadhelpers.h"
#include "loadtdata.h"
#include "jobs_low.h"
#include "lib/sound_low.h"
#include "lib/render.h"
#include "lib/texts_low.h"
#include "texts.h"
#include "jobs.h"
#include "static_allocator.h"
#include "containers/allocator_list.h"
#include "containers/allocator_htable.h"

//----------------------------------------------------------------------------------------------------------------

static const s32 MAX_INT = 2147483647;
static const fx32 PARTICLES_EXPLODE_FRAME_TIME_MS_FX = FX32(1.0);

static OnSkipStreamFrameCallback _onStreamFrameWaitingFn = NULL;
struct TEngineCommonData cd = {0};
const fx32 fakeTextFrData[ppranifrfixeddataCOUNT] = {0};

static void _initInstance(struct TEngineInstance *i);
static void _draw(const u16* frpc_src_data, struct DrawImageFunctionData* fd);
static inline void _forceDrawBackgroundTiles(struct TEngineInstance * i, const s32 c_dx8, const s32 c_dy8, s32 active_gr_bg);
static void _drawText(struct TEngineInstance *ei, s32 pr_id);
static BOOL _txbSetDynamicText(struct TEngineInstance *a, s32 pr_id, const wchar *text);
static BOOL _txbSetDynamicTextLine(struct TEngineInstance *a, s32 pr_id, s32 line_idx, const wchar *text);
static void _drawBackgroundTiles(BOOL iRedrawBackgroundEvenIfNoScrolling);
static void _drawParticles(s32 ms);
static void _updateParticlesWithoutDraw(s32 ms);
static void _drawGameObj(s32 pr_id);
static void _drawObjectsDummy(struct TEngineInstance* ei, u32 layer, s32 ms);
static void _drawObjectsWithoutUpdate(struct TEngineInstance* ei, u32 layer, s32 ms);
static void _drawObjectsWithUpdate(struct TEngineInstance* ei, u32 layer, s32 ms);

//-------------------------------------------------------------------------------------------

void initEngineParametersWithDefaultValues(struct InitEngineParameters *const params)
{
	SDK_NULL_ASSERT(params);
	params->layersCount = 1;
	params->particlesPoolSize = PCS_DEF_POOL_SIZE;
}
//-------------------------------------------------------------------------------------------

void initEngine(const struct InitEngineParameters *params)
{
	u32 i;
	u32 j;

	SDK_ASSERT(cd.instances == NULL);

	_loadDataInit();

	MI_CpuFill8(&cd, 0, sizeof(struct TEngineCommonData));

	cd.delay_fx = DEFAULT_DELAY_MS_FX;
	cd.deltatime_scale = FX_Div(FIXED_TIMESTEP_MS_FX, cd.delay_fx);
	cd.a_layer = NONE_MAP_IDX;
	cd.spd_prp_id = NONE_MAP_IDX;
	cd.enable_prp_id = NONE_MAP_IDX;

	cd.initParams = *params;
	OS_Printf("initEngine with layers = %d\n", cd.initParams.layersCount);

	SDK_ASSERT(cd.initParams.layersCount > 0); // instanceCount == 0!

	mthInitRandom();
	
	cd.ld_task = (s16*)MALLOC((cd.initParams.layersCount + 2) * sizeof(s16), "initEngine:ld_task");
	cd.ld_p1 = (u32*)MALLOC((cd.initParams.layersCount + 2) * sizeof(u32), "initEngine:ld_p1");
	cd.ld_p2 = (u16*)MALLOC((cd.initParams.layersCount + 2) * sizeof(u16), "initEngine:ld_p2");

	_onStreamFrameWaitingFn = NULL;

	cd.instances = (struct TEngineInstance **)MALLOC(cd.initParams.layersCount * sizeof(struct TEngineInstance**), "initEngine:cd.instances");
	for (j = 0; j < cd.initParams.layersCount; j++)
	{
		cd.instances[j] = (struct TEngineInstance*)MALLOC(sizeof(struct TEngineInstance), "initEngine:cd.instances[j]");
		_initInstance(cd.instances[j]);
		setVisible(j, TRUE);
		if(cd.initParams.particlesPoolSize > 0)
		{
			if (cd.instances[j]->pcs_fdata == NULL)
			{
				cd.instances[j]->pcs_fdata = (fx32**)MALLOC(cd.initParams.particlesPoolSize * sizeof(fx32**), "initEngine:cd.instances[j]->pcs_fdata");
				for (i = 0; i < cd.initParams.particlesPoolSize; i++)
				{
					cd.instances[j]->pcs_fdata[i] = (fx32*)MALLOC(PCS_COUNT * sizeof(fx32), "cd.instances[j]->pcs_fdata[i]");
				}
			}
		}
	}

	cd._updateEngineObjFn[UEOF_EMPTY] = _drawObjectsDummy;
	cd._updateEngineObjFn[UEOF_READY] = _drawObjectsWithUpdate;
	cd._updateEngineObjFn[UEOF_SKIP_FRAME1] = _drawObjectsWithoutUpdate;
	cd._updateEngineObjFn[UEOF_SKIP_FRAME2] = _drawObjectsWithoutUpdate;
	cd._updateEngineObjFn[UEOF_SKIP_FRAME3] = _drawObjectsWithoutUpdate;

	low_SetDummyDrawFunctions();

	cd.ainst = NULL;
	cd.a_layer = NONE_MAP_IDX;
	jobInit();
}
//-------------------------------------------------------------------------------------------

void low_SetDummyDrawFunctions()
{
	if(cd.trig_visible_mask == 0)
	{
		u32 layer = 0;
		cd.trig_visible_mask = 1;
		for(; layer < cd.initParams.layersCount; layer++)
		{
			struct TEngineInstance* ei = cd.instances[layer];
			ei->t_visible_mask = ei->visible_mask;
			ei->visible_mask = 0;
		}
	}
}
//-------------------------------------------------------------------------------------------

void low_RestoreDrawFunctions()
{
	if(cd.trig_visible_mask != 0)
	{
		u32 layer = 0; 
		cd.trig_visible_mask = 0;
		for(; layer < cd.initParams.layersCount; layer++)
		{
			struct TEngineInstance* ei = cd.instances[layer];
			ei->visible_mask = ei->t_visible_mask;
		}
	}
}
//-------------------------------------------------------------------------------------------

void releaseEngine()
{
	u32 i;
	cd.lost_render_device_is_jobs_active = FALSE;
	jobRelease();
	if (cd.instances != NULL)
	{
		_freeRes(&cd);
		i = cd.initParams.layersCount; 
		while(i > 0)
		{
			i--;
			releaseMapData(i);
			_releaseTrigData(cd.instances[i]);
			_releaseScriptData(cd.instances[i]);
		}
	}
	if (cd.instances != NULL)
	{
		i = cd.initParams.layersCount;
		while(i > 0)
		{
			i--;
			if (cd.instances[i]->pcs_fdata != NULL)
			{
				s32 j = (s32)cd.initParams.particlesPoolSize; 
				while(j > 0)
				{
					j--;
					FREE(cd.instances[i]->pcs_fdata[j]);
					cd.instances[i]->pcs_fdata[j] = NULL;
				}
				FREE(cd.instances[i]->pcs_fdata);
				cd.instances[i]->pcs_fdata = NULL;
			}
			FREE(cd.instances[i]);
		}
		FREE(cd.instances);
		cd.instances = NULL;
		cd.initParams.layersCount = 0;
	}
	if(cd.ld_task != NULL)
	{
		FREE(cd.ld_p2);
		FREE(cd.ld_p1);
		FREE(cd.ld_task);
	}
	cd.ainst = NULL;
	MI_CpuFill8(&cd, 0, sizeof(struct TEngineCommonData));
	//OS_Printf("releaseEngine\n");
}
//-------------------------------------------------------------------------------------------

struct TEngineInstance *_getInstance(u32 idx)
{
	SDK_ASSERT(idx <= cd.initParams.layersCount);
	SDK_ASSERT(cd.instances);
	SDK_ASSERT(cd.instances[idx]);
	return cd.instances[idx];
}
//-------------------------------------------------------------------------------------------

void initRenderPlaneParametersWithDefaultValues(struct RenderPlaneInitParams *const params)
{
	SDK_NULL_ASSERT(params);
	params->mSizes.mViewHeight = 512;
	params->mSizes.mViewWidth = 512;
#if defined USE_CUSTOM_RENDER || defined NITRO_SDK
	params->mSizes.mFrameBufferWidth8 = 512;	 // buffer must be multiple of 8
	params->mSizes.mFrameBufferHeight8 = 512;
#endif
	params->mBGType = BGSELECT_SUB2;
#ifdef USE_OPENGL_RENDER
	params->mMaxRenderObjectsOnPlane = 4096;
#endif
#if defined USE_CUSTOM_RENDER || defined NITRO_SDK
	params->mColorType = BMP_TYPE_DC16;
#ifdef NITRO_SDK
	params->mBGPriority = 0;
	params->mScreenBase = GX_BG_BMPSCRBASE_0x00000;
#endif
#endif
	params->mX = 0;
	params->mY = 0;
}
//-------------------------------------------------------------------------------------------

void initRenderPlane(const struct RenderPlaneInitParams* p)
{
	SDK_ASSERT(cd.instances);
	SDK_ASSERT(p);
	SDK_ASSERT(p->mSizes.mViewWidth < DISPLAY_VIEW_W_MAX && p->mSizes.mViewHeight < DISPLAY_VIEW_H_MAX);
	RENDERFN(PlaneInit)(p);
}
//-------------------------------------------------------------------------------------------

void setRenderPlaneScale(fx32 val, u32 layer)
{
	struct TEngineInstance *ei;
	SDK_ASSERT(cd.instances);
	SDK_ASSERT(RENDERFN(IsGraphicsInit)()); // please init graphics system before
	ei = _getInstance(layer);
	RENDERFN(SetRenderPlaneScale)(val, ei->bgType);
	if(ei->ueof_state != UEOF_EMPTY)
	{
//#if defined SDK_DEBUG
//		s32 tdh, tdw;
//		tdh = ei->d_h;
//		tdw = ei->d_w;
//#endif
		_calculateLogicSizes(ei, &cd);
//#if defined SDK_DEBUG
//		// please call resizeRenderPlane() with new view sizes
//		SDK_ASSERT(ei->d_h > tdh);
//		SDK_ASSERT(ei->d_w > tdw);
//#endif
	}
}
//-------------------------------------------------------------------------------------------

fx32 getRenderPlaneScale(u32 layer)
{
	SDK_ASSERT(cd.instances);
	SDK_ASSERT(RENDERFN(IsGraphicsInit)()); // please init graphics system before
	return RENDERFN(GetRenderPlaneScale)(_getInstance(layer)->bgType);
}
//-------------------------------------------------------------------------------------------

BOOL isRenderPlaneInit(u32 layer)
{
	if(cd.instances)
	{
		return RENDERFN(IsRenderPlaneInit)(_getInstance(layer)->bgType);
	}
	return FALSE;
}
//-------------------------------------------------------------------------------------------

void resizeRenderPlane(u32 layer, const struct RenderPlaneSizeParams* p)
{
	u32 i;
	struct TEngineInstance *ei;
	SDK_ASSERT(isRenderPlaneInit(layer)); // please init graphics system before
	ei = _getInstance(layer);
	RENDERFN(PlaneResize)(ei->bgType, p);	
#if defined USE_CUSTOM_RENDER && defined USE_TILEBUFFER
	ei->t_c_dx_fx[ei->bgType] = ei->t_c_dy_fx[ei->bgType] = -FX32_ONE;
#endif
	//reload all res because of FRAME_ALLOCATOR
	i = cd.initParams.layersCount; 
	while(i > 0)
	{
		i--;
		ei = cd.instances[i];
		if(ei->ueof_state != UEOF_EMPTY)
		{
			const BOOL ja = jobIsActive();
			_freeRes(&cd);
			cd.ag_st = TRUE;
			jobSetActive(ja);
			return;
		}
	}
}
//-------------------------------------------------------------------------------------------

void releaseRenderPlane(enum BGSelect iType)
{
	SDK_ASSERT(RENDERFN(IsGraphicsInit)()); // please init graphics system before
	RENDERFN(PlaneRelease)(iType);
}
//-------------------------------------------------------------------------------------------

BOOL assignLayerWithRenderPlane(u32 layer, enum BGSelect iBGType, BOOL primaryLayer)
{
	struct TEngineInstance *ei;
	SDK_ASSERT(RENDERFN(IsGraphicsInit)()); // please init graphics system before
	ei = _getInstance(layer);
	SDK_ASSERT(ei->ueof_state == UEOF_EMPTY); // please call this function before resources loading
	if(ei->ueof_state != UEOF_EMPTY)
	{
		return FALSE;
	}
	ei->bgType = iBGType;
	ei->primary = FALSE;
	if(primaryLayer)
	{
		u32 j;
		for (j = 0; j < cd.initParams.layersCount; j++)
		{
			struct TEngineInstance *tei = _getInstance(j);
			if(tei->primary && tei->bgType == iBGType)
			{
				tei->primary = FALSE;	
			}
		}
		ei->primary = TRUE;
	}
	return TRUE;
}
//-------------------------------------------------------------------------------------------

static void _initInstance(struct TEngineInstance *i)
{
	s32 j;
	MI_CpuFill8(i, 0, sizeof(struct TEngineInstance));
	i->t_current_zone = i->current_zone = -1;
	i->bgType = BGSELECT_NUM;
	for (j = 0; j < BGSELECT_NUM; j++)
	{
#if defined USE_CUSTOM_RENDER && defined USE_TILEBUFFER
		i->t_c_dx_fx[j] = -FX32_ONE;
		i->t_c_dy_fx[j] = -FX32_ONE;
#endif
		i->ds_plane_fr_array_beg[j] = -1;
		i->ds_plane_fr_array_end[j] = -1;
	}
}
//-------------------------------------------------------------------------------------------

void _setActiveLayer(u32 layer)
{
	cd.ainst = _getInstance(layer);
	cd.a_layer = layer;
	RENDERFN(SetActiveBGForGraphics)(cd.ainst->bgType);
}
//-------------------------------------------------------------------------------------------

u32 getActiveLayer(void)
{
	return cd.a_layer;	
}
//-------------------------------------------------------------------------------------------

void setVisible(u32 layer, BOOL val)
{
	struct TEngineInstance *ei = _getInstance(layer);
	if(cd.trig_visible_mask != 0)
	{
		ei->t_visible_mask = val ? 0xf : 0; 
	}
	else
	{
		ei->visible_mask = val ? 0xf : 0;
	}
}
//-------------------------------------------------------------------------------------------

BOOL getVisible(u32 layer)
{
	struct TEngineInstance *ei = _getInstance(layer);
	if(cd.trig_visible_mask != 0)
	{
		return ei->t_visible_mask != 0;
	}
	else
	{
		return ei->visible_mask != 0;
	}
}
//-------------------------------------------------------------------------------------------

void setBackgroundTileLayersForDS(u32 layer, enum BGSelect iBGType, s32 iStartIdx, s32 iEndIdx)
{
	SDK_ASSERT(iBGType < BGSELECT_NUM);
	_getInstance(layer)->ds_plane_fr_array_beg[iBGType] = iStartIdx;
	_getInstance(layer)->ds_plane_fr_array_end[iBGType] = iEndIdx;
}
//-------------------------------------------------------------------------------------------

fx32 getCurrentDeltaTimeScale(void)
{
	return cd.deltatime_scale;
}
//-------------------------------------------------------------------------------------------

s32 getHeightBGTile()
{
	return cd.p_h;
}
//-------------------------------------------------------------------------------------------

s32 getWidthBGTile()
{
	return  cd.p_w;
}
//-------------------------------------------------------------------------------------------

s32 getViewWidth(u32 layer)
{
	SDK_NULL_ASSERT(cd.ainst);
	return RENDERFN(GetViewWidth)(_getInstance(layer)->bgType);
}
//-------------------------------------------------------------------------------------------

s32 getViewHeight(u32 layer)
{
	SDK_NULL_ASSERT(cd.ainst);
	return RENDERFN(GetViewHeight)(_getInstance(layer)->bgType);
}
//-------------------------------------------------------------------------------------------

s32 getViewLeft(u32 layer)
{
	SDK_NULL_ASSERT(cd.ainst);
	return RENDERFN(GetViewLeft)(_getInstance(layer)->bgType);
}
//-------------------------------------------------------------------------------------------

s32 getViewTop(u32 layer)
{
	SDK_NULL_ASSERT(cd.ainst);
	return RENDERFN(GetViewTop)(_getInstance(layer)->bgType);
}
//-------------------------------------------------------------------------------------------

s32 getMapWidth()
{
	return cd.ainst->sz_mapW;
}
//-------------------------------------------------------------------------------------------

s32 getMapHeight()
{
	return cd.ainst->sz_mapH;
}
//-------------------------------------------------------------------------------------------

void setOnBeginUpdate(OnBeginUpdate pCallbackFn)
{
	cd._onBeginUpdate = pCallbackFn;
}
//-------------------------------------------------------------------------------------------

void setOnDrawBackgroundTiles(OnDrawBackgroundTiles pCallbackFn)
{
	cd._onDrawBackgroundTiles = pCallbackFn;
}
//-------------------------------------------------------------------------------------------

void setOnDrawDecorationObject(OnDrawObject pCallbackFn)
{
	cd._onDrawDecorationObject = pCallbackFn;
}
//-------------------------------------------------------------------------------------------

void setOnDrawGameObject(OnDrawObject pCallbackFn)
{
	cd._onDrawGameObject = pCallbackFn;
}
//-------------------------------------------------------------------------------------------

void setOnFinishUpdate(OnFinishUpdate pCallbackFn)
{
	cd._onFinishUpdate = pCallbackFn;
}
//-------------------------------------------------------------------------------------------

void setOnEvent(OnEventCallback pCallbackFn)
{
	cd._onEventCB = pCallbackFn;
}
//-------------------------------------------------------------------------------------------

#ifdef TENGINE_LEGACY_CODE
void setOnScript(u32 layer, OnScriptCallback pCallbackFn)
{
	_getInstance(layer)->_onScriptCB = pCallbackFn;
}
//-------------------------------------------------------------------------------------------
#endif

void setOnChangeHero(u32 layer, OnChangeHeroCallback pCallbackFn)
{
	_getInstance(layer)->_onChangeHeroCB = pCallbackFn;
}
//-------------------------------------------------------------------------------------------

void setOnSkipStreamFrame(OnSkipStreamFrameCallback pCallbackFn)
{
	_onStreamFrameWaitingFn = pCallbackFn;
}
//-------------------------------------------------------------------------------------------

void _setCamera(struct TEngineInstance* ainst, struct fxVec2 pos)
{
	SDK_ASSERT(ainst);
	SDK_ASSERT(ainst->bgType != BGSELECT_NUM);
	SDK_ASSERT(fx2int(fxAbs(pos.x)) < DISPLAY_VIEW_W_MAX);
	SDK_ASSERT(fx2int(fxAbs(pos.y)) < DISPLAY_VIEW_H_MAX);

	ainst->c_fx = pos;
#ifdef USE_CUSTOM_RENDER
	ainst->cx_fx[ainst->bgType] = pos.x;
	ainst->cy_fx[ainst->bgType] = pos.y;
	_calculateCXY8(ainst, &ainst->cx_fx[ainst->bgType], &ainst->cy_fx[ainst->bgType],
							&ainst->tc_dx8[ainst->bgType], &ainst->tc_dy8[ainst->bgType]);
	ainst->tcx_fx[cainst->bgType] = FX32(fx2int(pos.x));
	ainst->tcy_fx[ainst->bgType] = FX32(fx2int(pos.y));
#else
	ainst->tcx_fx[cd.ainst->bgType] = pos.x;
	ainst->tcy_fx[cd.ainst->bgType] = pos.y;
#endif
	_calculateCXY8(ainst, &ainst->tcx_fx[ainst->bgType], &ainst->tcy_fx[ainst->bgType],
							&ainst->tc_dx8[ainst->bgType], &ainst->tc_dy8[ainst->bgType]);
}
//-------------------------------------------------------------------------------------------

void setCamera(struct fxVec2 pos)
{
	_setCamera(cd.ainst, pos);
}
//-------------------------------------------------------------------------------------------

void setCameraLayer(struct fxVec2 pos, u32 layer)
{
	struct TEngineInstance* ei = _getInstance(layer);
	_setCamera(ei, pos);
}
//-------------------------------------------------------------------------------------------

struct fxVec2 _getCameraMinPosition(const struct TEngineInstance* ainst)
{
	struct fxVec2 pos;
	SDK_ASSERT(ainst);
	pos.x = ainst->logicWidthHalf_fx;
	pos.y = ainst->logicHeightHalf_fx;
	if(FX32(ainst->sz_mapW) <= pos.x)
	{
		pos.x = 0;
	}
	if(FX32(ainst->sz_mapH) <= pos.y)
	{
		pos.y = 0;
	}
	return pos;
}
//-------------------------------------------------------------------------------------------

struct fxVec2 getCameraMinPosition()
{
	return _getCameraMinPosition(cd.ainst);
}
//-------------------------------------------------------------------------------------------

struct fxVec2 getCameraMinPositionLayer(u32 layer)
{
	struct TEngineInstance* ei = _getInstance(layer);
	return _getCameraMinPosition(ei);
}
//-------------------------------------------------------------------------------------------

struct fxVec2 _getCameraMaxPosition(const struct TEngineInstance* ainst)
{
	struct fxVec2 pos;
	SDK_ASSERT(ainst);
	pos.x = FX32(ainst->sz_mapW) - ainst->logicWidthHalf_fx;
	pos.y = FX32(ainst->sz_mapH) - ainst->logicHeightHalf_fx;
	if(pos.x < 0)
	{
		pos.x = 0;
	}
	if(pos.y < 0)
	{
		pos.y = 0;
	}
	return pos;
}
//-------------------------------------------------------------------------------------------

struct fxVec2 getCameraMaxPosition()
{
	return _getCameraMaxPosition(cd.ainst);
}
//-------------------------------------------------------------------------------------------

struct fxVec2 getCameraMaxPositionLayer(u32 layer)
{
	struct TEngineInstance* ei = _getInstance(layer);
	return _getCameraMaxPosition(ei);
}
//-------------------------------------------------------------------------------------------

struct fxVec2 _getCamera(const struct TEngineInstance *ainst)
{
	struct fxVec2 pos;
	SDK_ASSERT(ainst);
#ifdef USE_CUSTOM_RENDER
	pos.x = ainst->cx_fx[cd.ainst->bgType];
	pos.y = ainst->cy_fx[cd.ainst->bgType];
#else
	pos.x = ainst->tcx_fx[cd.ainst->bgType];
	pos.y = ainst->tcy_fx[cd.ainst->bgType];
#endif
	return pos;
}
//-------------------------------------------------------------------------------------------

struct fxVec2 getCamera(void)
{
	return _getCamera(cd.ainst);
}
//-------------------------------------------------------------------------------------------

struct fxVec2 getCameraLayer(u32 layer)
{
	struct TEngineInstance* ei = _getInstance(layer);
	return _getCamera(ei);
}
//-------------------------------------------------------------------------------------------

void _calculateCXY8(struct TEngineInstance *ei, fx32 *cx, fx32 *cy, s32 *c_dx8, s32 *c_dy8)
{
	fx32 hfx_fx = ei->logicWidthHalf_fx;
	fx32 hfy_fx = ei->logicHeightHalf_fx;
	if (*cx + hfx_fx >= FX32(ei->sz_mapW))
	{
		*cx = FX32(ei->sz_mapW) - hfx_fx;
	}
	else if (*cx - hfx_fx < 0)
	{
		*cx = hfx_fx = (ei->logicWidthHalf_fx / cd.p_w) * cd.p_w;
	}
	if (*cy + hfy_fx >= FX32(ei->sz_mapH))
	{
		*cy = FX32(ei->sz_mapH) - hfy_fx;
	}
	else if (*cy - hfy_fx < 0)
	{
		*cy = hfy_fx = (ei->logicHeightHalf_fx / cd.p_h) * cd.p_h;
	}
	ei->worldx_fx = *cx - hfx_fx;
	if (ei->worldx_fx < 0)
	{
		ei->worldx_fx = FX32(0);
	}
	ei->worldy_fx = *cy - hfy_fx;
	if (ei->worldy_fx < 0)
	{
		ei->worldy_fx = FX32(0);
	}
	*c_dx8 = fx2int(ei->worldx_fx) / cd.p_w;
	*c_dy8 = fx2int(ei->worldy_fx) / cd.p_h;
#if defined USE_FX32_AS_FLOAT
	ei->sdx_fx = fmod(ei->worldx_fx, FX32(cd.p_w));
	ei->sdy_fx = fmod(ei->worldy_fx, FX32(cd.p_h));
#else
	ei->sdx_fx = ei->worldx_fx % FX32(cd.p_w);
	ei->sdy_fx = ei->worldy_fx % FX32(cd.p_h);
#endif
}
//-------------------------------------------------------------------------------------------

inline void _forceDrawBackgroundTiles(struct TEngineInstance *ei, const s32 c_dx_8, const s32 c_dy_8, s32 abg)
{
	s32 a;
	u16 *m_c, *m_e, *ml_e;
	const u16 *o;
	struct DrawImageFunctionData fd;
	SDK_NULL_ASSERT(ei->tilemap);
	m_c = ei->tilemap + c_dy_8 * ei->map_w + c_dx_8;
	m_e = m_c + ei->d_h * ei->map_w;
	MI_CpuClear8(&fd, sizeof(struct DrawImageFunctionData));
	fd.mY = -ei->sdy_fx;
	fd.mSrcSizeData[SRC_DATA_W] = cd.p_w;
	fd.mSrcSizeData[SRC_DATA_H] = cd.p_h;
	fd.mFillColor = (ALPHA_OPAQ << 16) | DIFD_DEF_FILL_COLOR;
#ifdef CUSTOMAFFINETRANSFORM_SUPPORT
	fd.mDestWidth = FX32(cd.p_w);
	fd.mDestHeight = FX32(cd.p_h);
#endif
	while (m_c <= m_e)
	{
		fd.mX = ei->l_w - ei->sdx_fx;
		ml_e = m_c + ei->d_w;
		while (m_c <= ml_e)
		{
			if (*ml_e < ei->pnt_count)
			{
				for (a = ei->ds_plane_fr_array_beg[abg];
						a < ei->ds_plane_fr_array_end[abg]; ++a)
				{
					SDK_NULL_ASSERT(ei->fr_img[*ml_e][a]);
					o = ei->fr_img[*ml_e][a];
					if(o[RES_IMG_IDX] < NULL_IMG_IDX)
					{
						fd.mType = (enum DrawImageType)a;
						fd.mpSrcData = cd.res_img[o[RES_IMG_IDX]];
						fd.mSrcSizeData[SRC_DATA_X] = o[RES_X];
						fd.mSrcSizeData[SRC_DATA_Y] = o[RES_Y];
#ifdef USE_TILEMAP_ATTRIBUTES
						if (ei->tileattrmap != NULL)
						{
							fd.mFillColor = ei->tileattrmap[ml_e - ei->tilemap];
						}
#endif
						RENDERFN(DrawImage)(&fd);
					}
				}
			}
			--ml_e;
			fd.mX -= FX32(cd.p_w);
		}
		m_c += ei->map_w;
		fd.mY += FX32(cd.p_h);
	}
}
//-------------------------------------------------------------------------------------------

#if defined USE_CUSTOM_RENDER && defined USE_TILEBUFFER
#define RenderTileMacro()	{															\
								for (j = ei->ds_plane_fr_array_beg[ei->bgType];			\
										j < ei->ds_plane_fr_array_end[ei->bgType]; ++j)	\
								{														\
									SDK_NULL_ASSERT(ei->fr_img[*ml_e][j]);				\
									o = ei->fr_img[*ml_e][j];							\
									if(o[RES_IMG_IDX] < NULL_IMG_IDX)					\
									{													\
										fd.mType = (enum DrawImageType)j;				\
										fd.mpSrcData = cd.res_img[o[RES_IMG_IDX]];		\
										fd.mSrcSizeData[SRC_DATA_X] = o[RES_X];			\
										fd.mSrcSizeData[SRC_DATA_Y] = o[RES_Y];			\
										RENDERFN(DrawImage)(&fd);						\
									}													\
								}														\
							}

#define Define_me_mc_mX_ForY_Macro(h_off)																					\
							{																								\
								if (c_dx_fx < 0)																			\
								{																							\
									j = (c_dx_fx >> FX32_SHIFT) / cd.p_w;													\
									m_e = ei->tilemap + (ei->tc_dy8[ei->bgType] + (h_off)) * ei->map_w + ei->tc_dx8[ei->bgType];	\
									m_c = m_e + ei->d_w + j;																	\
									fd.mX = ei->l_w - ei->sdx_fx + j * cd.p_w;												\
								}																							\
								else																						\
								{																							\
									j = (ei->tc_dy8[ei->bgType] + (h_off)) * ei->map_w + ei->tc_dx8[ei->bgType];				\
									m_c = ei->tilemap + j + ei->d_w;															\
									m_e = ei->tilemap + j + (c_dx_fx >> FX32_SHIFT) / cd.p_w + 1;							\
									fd.mX = ei->l_w - ei->sdx_fx;															\
								}																							\
							}
#endif
//-------------------------------------------------------------------------------------------

void _drawBackgroundTiles(BOOL forceRedraw)
{
#if defined USE_CUSTOM_RENDER && defined USE_TILEBUFFER
	s32 j;
	fx32 c_dx_fx, c_dy_fx;
	u16 *m_c, *m_e, *ml_e;
	const u16 *o;
#endif
	struct TEngineInstance *ei = cd.ainst;

	SDK_ASSERT(ei);
	SDK_ASSERT(ei->bgType != BGSELECT_NUM);
	SDK_ASSERT(ei->visible_mask != 0);
	
#if defined USE_CUSTOM_RENDER && defined USE_TILEBUFFER
	c_dx_fx = ei->tcx_fx[ei->bgType];
	c_dy_fx = ei->tcy_fx[ei->bgType];
#endif

#if defined USE_OPENGL_RENDER || (defined USE_CUSTOM_RENDER && !defined USE_TILEBUFFER)
	(void)forceRedraw;
	_forceDrawBackgroundTiles(ei, ei->tc_dx8[ei->bgType], ei->tc_dy8[ei->bgType], ei->bgType);
#endif

#if defined USE_CUSTOM_RENDER && defined USE_TILEBUFFER
	if (c_dx_fx != ei->t_c_dx_fx[ei->bgType] || c_dy_fx != ei->t_c_dy_fx[ei->bgType])
	{
		if (ei->t_c_dx_fx[ei->bgType] < 0 && ei->t_c_dy_fx[ei->bgType] < 0)
		{
			ei->t_c_dx_fx[ei->bgType] = c_dx_fx;
			ei->t_c_dy_fx[ei->bgType] = c_dy_fx;
			_forceDrawBackgroundTiles(ei, ei->tc_dx8[ei->bgType], ei->tc_dy8[ei->bgType], ei->bgType);
		}
		else
		{
			struct DrawImageFunctionData fd;
			MI_CpuClear8(&fd, sizeof(struct DrawImageFunctionData));	
			fd.mX = c_dx_fx;
			fd.mY = c_dy_fx;
			c_dx_fx = ei->t_c_dx_fx[ei->bgType] - c_dx_fx;
			c_dy_fx = ei->t_c_dy_fx[ei->bgType] - c_dy_fx;
			ei->t_c_dx_fx[ei->bgType] = fd.mX;
			ei->t_c_dy_fx[ei->bgType] = fd.mY;

			if (MATH_ABS(c_dx_fx) > ei->shiftTilesMax_fx || MATH_ABS(c_dy_fx) > ei->shiftTilesMax_fx)
			{
				refreshAllScreen();
				_forceDrawBackgroundTiles(ei, ei->tc_dx8[ei->bgType], ei->tc_dy8[ei->bgType], ei->bgType);
			}
			else
			{
				fd.mSrcSizeData[SRC_DATA_W] = cd.p_w;
				fd.mSrcSizeData[SRC_DATA_H] = cd.p_h;
				fd.mFillColor = (ALPHA_OPAQ << 16) | DIFD_DEF_FILL_COLOR;
#ifdef CUSTOMAFFINETRANSFORM_SUPPORT
				fd.mScaleX = fd.mScaleY = FX32_ONE;
				fd.mSrcSizeData[DST_DATA_W] = cd.p_w;
				fd.mSrcSizeData[DST_DATA_H] = cd.p_h;
#endif
				RENDERFN(RestoreGraphicsFromMemory)(-c_dx_fx, -c_dy_fx);

				if (c_dx_fx < 0) // left <
				{
					m_c = ei->tilemap + ei->tc_dy8[ei->bgType] * ei->map_w + ei->tc_dx8[ei->bgType] + ei->d_w;
					fd.mX = ei->l_w - ei->sdx_fx;
					m_e = m_c + (((c_dx_fx - ei->l_w) >> FX32_SHIFT) + RENDERFN(GetViewWidth)(ei->bgType)) / cd.p_w - 1; // div scale?
					while (m_c >= m_e)
					{
						fd.mY = ei->l_h - ei->sdy_fx;
						ml_e = m_c + ei->d_h * ei->map_w;
						while (m_c <= ml_e)
						{
							if (*ml_e < ei->pnt_count)
							{
								RenderTileMacro();
							}
							ml_e -= ei->map_w;
							fd.mY -= cd.p_h << FX32_SHIFT;
						}
						--m_c;
						fd.mX -= cd.p_w << FX32_SHIFT;
					}
				}
				else if (c_dx_fx > 0) // right >
				{
					fd.mX = -ei->sdx_fx;
					m_c = ei->tilemap + ei->tc_dy8[ei->bgType] * ei->map_w + ei->tc_dx8[ei->bgType];
					m_e = m_c + (c_dx_fx >> FX32_SHIFT) / cd.p_w + 1;
					while (m_c <= m_e)
					{
						fd.mY = ei->l_h - ei->sdy_fx;
						ml_e = m_c + ei->d_h * ei->map_w;
						while (m_c <= ml_e)
						{
							if (*ml_e < ei->pnt_count)
							{
								RenderTileMacro();
							}
							ml_e -= ei->map_w;
							fd.mY -= cd.p_h << FX32_SHIFT;
						}
						++m_c;
						fd.mX += cd.p_w << FX32_SHIFT;
					}
				}

				if (c_dy_fx > 0) // down
				{
					c_dy_fx = (c_dy_fx >> FX32_SHIFT) / cd.p_h + 1;
					Define_me_mc_mX_ForY_Macro(c_dy_fx);
					while (m_c >= m_e)
					{
						fd.mY = -ei->sdy_fx;
						ml_e = m_c - c_dy_fx * ei->map_w;
						while (m_c >= ml_e)
						{
							if (*ml_e < ei->pnt_count)
							{
								RenderTileMacro();
							}
							ml_e += ei->map_w;
							fd.mY += cd.p_h << FX32_SHIFT;
						}
						--m_c;
						fd.mX -= cd.p_w << FX32_SHIFT;
					}
				}
				else if (c_dy_fx < 0) // up
				{
					Define_me_mc_mX_ForY_Macro(ei->d_h);
					while (m_c >= m_e)
					{
						j = (((c_dy_fx - ei->l_h) >> FX32_SHIFT) + RENDERFN(GetViewHeight)(ei->bgType)) / cd.p_h - 1; // div scale?
						fd.mY = ei->l_h + j * (cd.p_h << FX32_SHIFT) - ei->sdy_fx;
						ml_e = m_c + j * ei->map_w;
						while (m_c >= ml_e)
						{
							if (*ml_e < ei->pnt_count)
							{
								RenderTileMacro();
							}
							ml_e += ei->map_w;
							fd.mY += cd.p_h << FX32_SHIFT;
						}
						--m_c;
						fd.mX -= cd.p_w << FX32_SHIFT;
					}
				}
			}
		}
		RENDERFN(SaveGraphicsToMemory)();
	}
	else
	{
		if (forceRedraw)
		{
			RENDERFN(RestoreGraphicsFromMemory)(0, 0);
		}
	}
#endif
}
//-------------------------------------------------------------------------------------------

fx32 getViewOffsetX(u32 layer)
{
	struct TEngineInstance *l = _getInstance(layer);
	return l->worldx_fx;
}
//-------------------------------------------------------------------------------------------

fx32 getViewOffsetY(u32 layer)
{
	struct TEngineInstance *l = _getInstance(layer);
	return l->worldy_fx;
}
//-------------------------------------------------------------------------------------------

void refreshAllScreen()
{
#if defined USE_CUSTOM_RENDER && defined USE_TILEBUFFER
	enum BGSelect active_gr_bg = RENDERFN(GetActiveBGForGraphics)();
	if (active_gr_bg != BGSELECT_NUM)
	{
		cd.ainst->t_c_dx_fx[active_gr_bg] = cd.ainst->t_c_dy_fx[active_gr_bg] = -1;
	}
#endif
}
//-------------------------------------------------------------------------------------------

/*
void changeMapIndex(s32 *tileInfo, u16 idxChange, BOOL redrawTile)
{
	s32 pos;
	// SDK_NULL_ASSERT(tileInfo);
	SDK_NULL_ASSERT(cd.ainst->map);
	pos = tileInfo[POSITION];
	if (pos < cd.ainst->sz_map && pos >= 0 &&
		((idxChange < cd.ainst->pnt_count && idxChange >= 0)
			|| idxChange == NONE_MAP_IDX))
	{
		cd.ainst->map[pos] = idxChange;
		refreshAllScreen();

		// if(redrawTile)
		//		{
		//		ch_buff_idx[ch_buff] = idxChange;
		//		ch_buff_pos[ch_buff] = pos;
		//		ch_buff++;
		//		if(ch_buff > CHANGE_MAP_BUFFER_MAX)
		//		refreshAllScreen();
		//		}
	}
}
*/
//-------------------------------------------------------------------------------------------

u16* createCopyTileMap(u32 layer, struct StaticAllocator* const allocator)
{
	u16 *newMap;
	struct TEngineInstance *ei = _getInstance(layer);
	if(ei->tilemap == NULL)
	{
		return NULL;
	}
	newMap = StaticAllocator_Malloc(allocator, ei->map_w * ei->map_h * sizeof(u16));
	MI_CpuCopy16(ei->tilemap, newMap, ei->map_w * ei->map_h * sizeof(u16));
	return newMap;
}
//-------------------------------------------------------------------------------------------

void setTileMap(u32 layer, u16* ipTileMap, struct StaticAllocator* const allocator)
{
	struct TEngineInstance *ei = _getInstance(layer);
	if(ei->tilemap == NULL)
	{
		return;
	}
	SDK_NULL_ASSERT(ipTileMap);
	if (StaticAllocator_Check(allocator, ipTileMap) == ei->map_w * ei->map_h * sizeof(u16))
	{
		ei->tilemap = ipTileMap;
	}
	else
	{
		// invalid pointer to tilemap data
		SDK_ASSERT(0);
	}
}
//-------------------------------------------------------------------------------------------

void setDefaultTileMap(u32 layer)
{
	struct TEngineInstance *ei = _getInstance(layer);
	ei->tilemap = ei->t_tilemap;
}
//-------------------------------------------------------------------------------------------
#ifdef USE_TILEMAP_ATTRIBUTES
u32* createCopyTileMapColorAttributes(u32 layer, struct StaticAllocator* const allocator)
{
	u32* newMap;
	struct TEngineInstance* ei = _getInstance(layer);
	newMap = StaticAllocator_Malloc(allocator, ei->map_w * ei->map_h * sizeof(u32));
	if (ei->tileattrmap == NULL)
	{
		u32* mem;
		u32 sz = ei->map_w * ei->map_h;
		mem = newMap;
		while (sz)
		{
			*mem = ((ALPHA_OPAQ << 16) | DIFD_DEF_FILL_COLOR);
			mem++;
			sz--;
		}
	}
	else
	{
		MI_CpuCopy32(ei->tileattrmap, newMap, ei->map_w * ei->map_h * sizeof(u32));
	}
	return newMap;
}
//-------------------------------------------------------------------------------------------

void setTileMapColorAttributes(u32 layer, u32* ipTileMapCA, struct StaticAllocator* const allocator)
{
	struct TEngineInstance* ei = _getInstance(layer);
	SDK_NULL_ASSERT(ipTileMapCA);
	if (StaticAllocator_Check(allocator, ipTileMapCA) == ei->map_w * ei->map_h * sizeof(u32))
	{
		ei->tileattrmap = ipTileMapCA;
	}
	else
	{
		// invalid pointer to tilemap data
		SDK_ASSERT(0);
	}
}
//-------------------------------------------------------------------------------------------

void setDefaultTileMapColorAttributes(u32 layer)
{
	struct TEngineInstance* ei = _getInstance(layer);
	ei->tileattrmap = NULL;
}
#endif
//-------------------------------------------------------------------------------------------
/*
s16 _getTransform(s16 data)
{
	return data & 0x7FFF; 	
}
//-------------------------------------------------------------------------------------------
*/
BOOL _isStreamFrame(s16 data)
{
	return data & 0x8000;
}
//-------------------------------------------------------------------------------------------

s32 getCurrentLanguageId(void)
{
	return cd.lang_id;
}
//-------------------------------------------------------------------------------------------

s32 getStaticTextLinesCount(s32 text_id)
{
	return cd.textdatatext_sct[text_id];
}
//-------------------------------------------------------------------------------------------

const wchar* getStaticText(s32 text_id, s32 line)
{
	return cd.textdatatext[text_id][line] + 1;
}
//-------------------------------------------------------------------------------------------

TextBoxType txbGetType(s32 pr_id)
{
	if(cd.ainst->all_pr_count > pr_id)
	{
		SDK_NULL_ASSERT(cd.ainst->pr_textdata[pr_id]); // wrong pr_id
		return (cd.ainst->pr_textdata[pr_id][TXTCHRCOUNT] == MAX_RES_IDX) ? TEXTBOX_TYPE_STATIC : TEXTBOX_TYPE_DYNAMIC;
	}
	return TEXTBOX_TYPE_ERROR;
}
//-------------------------------------------------------------------------------------------

TextBoxType txbGetTypeLayer(s32 pr_id, u32 layer)
{
	struct TEngineInstance *l = _getInstance(layer);
	if(l->all_pr_count > pr_id)
	{
		SDK_NULL_ASSERT(l->pr_textdata);
		return (l->pr_textdata[pr_id][TXTCHRCOUNT] == MAX_RES_IDX) ? TEXTBOX_TYPE_STATIC : TEXTBOX_TYPE_DYNAMIC;
	}
	return TEXTBOX_TYPE_ERROR;
}
//-------------------------------------------------------------------------------------------

s32 txbGetDynamicTextLinesCount(s32 pr_id)
{
	if(txbGetType(pr_id) == TEXTBOX_TYPE_DYNAMIC)
	{
		return cd.ainst->pr_textdata[pr_id][TXTSTRCOUNT];
	}
	return 0;
}
//-------------------------------------------------------------------------------------------

s32 txbGetDynamicTextLinesCountLayer(s32 pr_id, u32 layer)
{
	if(txbGetTypeLayer(pr_id, layer) == TEXTBOX_TYPE_DYNAMIC)
	{
		struct TEngineInstance *l = _getInstance(layer);
		return l->pr_textdata[pr_id][TXTSTRCOUNT];
	}
	return 0;
}
//-------------------------------------------------------------------------------------------

s32 txbGetDynamicTextMaximumLineCharsCount(s32 pr_id)
{
	if(txbGetType(pr_id) == TEXTBOX_TYPE_DYNAMIC)
	{
		return cd.ainst->pr_textdata[pr_id][TXTCHRCOUNT];
	}
	return 0;
}
//-------------------------------------------------------------------------------------------

s32 txbGetDynamicTextMaximumLineCharsCountLayer(s32 pr_id, u32 layer)
{
	if(txbGetTypeLayer(pr_id, layer) == TEXTBOX_TYPE_DYNAMIC)
	{
		struct TEngineInstance *l = _getInstance(layer);
		return l->pr_textdata[pr_id][TXTCHRCOUNT];
	}
	return 0;
}
//-------------------------------------------------------------------------------------------

BOOL _txbSetDynamicTextLine(struct TEngineInstance *a, s32 pr_id, s32 line_idx, const wchar *text)
{
	if(line_idx < a->pr_textdata[pr_id][TXTSTRCOUNT])
	{
		s32 cct, str_ct;
		SDK_NULL_ASSERT(text);
		str_ct = STD_WStrLen(text);
		cct = (str_ct > a->pr_textdata[pr_id][TXTCHRCOUNT]) ? a->pr_textdata[pr_id][TXTCHRCOUNT] : str_ct;
		MI_CpuCopy8(text, a->pr_textdatatext[pr_id][line_idx] + 1, cct * sizeof(wchar));
		a->pr_textdatatext[pr_id][line_idx][cct + 1] = W_T('\0');
		a->pr_textdatatext[pr_id][line_idx][0] = (wchar)cct;
		return TRUE;
	}
	return FALSE;
}
//-------------------------------------------------------------------------------------------

BOOL txbSetDynamicTextLine(s32 pr_id, s32 line_idx, const wchar *text)
{
	if(txbGetType(pr_id) == TEXTBOX_TYPE_DYNAMIC)
	{
		return _txbSetDynamicTextLine(cd.ainst, pr_id, line_idx, text);
	}
	return FALSE;
}
//-------------------------------------------------------------------------------------------

BOOL txbSetDynamicTextLineLayer(s32 pr_id, s32 line_idx, const wchar *text, u32 layer)
{
	if(txbGetTypeLayer(pr_id, layer) == TEXTBOX_TYPE_DYNAMIC)
	{
		struct TEngineInstance *l = _getInstance(layer);
		return _txbSetDynamicTextLine(l, pr_id, line_idx, text);
	}
	return FALSE;
}
//-------------------------------------------------------------------------------------------

BOOL txbSetDynamicText(s32 pr_id, const wchar *text)
{
	if(txbGetType(pr_id) == TEXTBOX_TYPE_DYNAMIC)
	{
		return _txbSetDynamicText(cd.ainst, pr_id, text);
	}
	return FALSE;
}
//-------------------------------------------------------------------------------------------

BOOL txbSetDynamicTextLayer(s32 pr_id, const wchar *text, u32 layer)
{
	if(txbGetTypeLayer(pr_id, layer) == TEXTBOX_TYPE_DYNAMIC)
	{
		struct TEngineInstance *l = _getInstance(layer);
		return _txbSetDynamicText(l, pr_id, text);
	}
	return FALSE;
}
//-------------------------------------------------------------------------------------------

const wchar* txbGetDynamicText(s32 pr_id, s32 line)
{
	if(txbGetType(pr_id) == TEXTBOX_TYPE_DYNAMIC)
	{
		return cd.ainst->pr_textdatatext[pr_id][line] + 1;
	}
	return NULL;
}
//-------------------------------------------------------------------------------------------

const wchar* txbGetDynamicTextLayer(s32 pr_id, s32 line, u32 layer)
{
	if(txbGetTypeLayer(pr_id, layer) == TEXTBOX_TYPE_DYNAMIC)
	{
		struct TEngineInstance *l = _getInstance(layer);
		return l->pr_textdatatext[pr_id][line] + 1;
	}
	return NULL;
}
//-------------------------------------------------------------------------------------------

BOOL _txbSetDynamicText(struct TEngineInstance *a, s32 pr_id, const wchar *text)
{
	s32 i, str_ct, c, beg, cct;
	SDK_NULL_ASSERT(text);
	beg = c = i = str_ct = 0;
	i = -1;
	do
	{
		i++;
		if(text[i] == W_T('\n') || text[i] == W_T('\0'))
		{
			cct = (str_ct > a->pr_textdata[pr_id][TXTCHRCOUNT]) ? a->pr_textdata[pr_id][TXTCHRCOUNT] : str_ct;
			if(a->pr_textdatatext[pr_id] == NULL)
			{
				SDK_ASSERT(0); //please increase the number of lines for this textbox in MapEditor
				return FALSE;
			}
			MI_CpuCopy8(text + beg, a->pr_textdatatext[pr_id][c] + 1, cct * sizeof(wchar));
			a->pr_textdatatext[pr_id][c][cct + 1] = W_T('\0');
			a->pr_textdatatext[pr_id][c][0] = (wchar)cct;
			c++;
			beg += str_ct + 1;
			str_ct = 0;
			if(c == a->pr_textdata[pr_id][TXTSTRCOUNT])
			{
				break;
			}
		}
		else
		{
			if(text[i] == W_T('\r'))
			{
				beg += 1;
			}
			else
			{
				str_ct++;
			}
		}
	}
	while(text[i] != 0);
	return TRUE;
}
//-------------------------------------------------------------------------------------------

s32 txbGetWidth(s32 pr_id)
{
	if(cd.ainst->all_pr_count > pr_id)
	{
		return cd.ainst->pr_textdata[pr_id][TXTWIDTH];
	}
	return 0;
}
//-------------------------------------------------------------------------------------------

s32 txbGetWidthLayer(s32 pr_id, u32 layer)
{
	struct TEngineInstance *l = _getInstance(layer);
	if(l->all_pr_count > pr_id)
	{
		return l->pr_textdata[pr_id][TXTWIDTH];
	}
	return 0;
}
//-------------------------------------------------------------------------------------------

s32 txbGetHeight(s32 pr_id)
{
	if(cd.ainst->all_pr_count > pr_id)
	{
		return cd.ainst->pr_textdata[pr_id][TXTHEIGHT];
	}
	return 0;
}
//-------------------------------------------------------------------------------------------

s32 txbGetHeightLayer(s32 pr_id, u32 layer)
{
	struct TEngineInstance *l = _getInstance(layer);
	if(l->all_pr_count > pr_id)
	{
		return l->pr_textdata[pr_id][TXTHEIGHT];
	}
	return 0;
}
//-------------------------------------------------------------------------------------------

void txbSetTextPerPixelAccuracy(s32 pr_id, BOOL val, u32 layer)
{
	struct TEngineInstance *l = _getInstance(layer);
	if(l->all_pr_count > pr_id)
	{
		l->pr_idata[pr_id][ISTATE] = (s32)val;
	}
}
//-------------------------------------------------------------------------------------------

const struct AlphabetTextureOffsetMap* _findTextureOffset(const struct TERFont* ipFont, wchar iChar, u16 arr[SRC_DATA_SIZE])
{
	const struct HTableItem* keyval;
	const struct AlphabetTextureOffsetMap* data;
	SDK_NULL_ASSERT(ipFont);
	SDK_NULL_ASSERT(ipFont->mAlphabet.mpHTable); // Current font is not assigned by data
	keyval = AllocatorHTable_Find(ipFont->mAlphabet.mpHTable, (u32)iChar);
	if(keyval != NULL)
	{
		data = (const struct AlphabetTextureOffsetMap*)AllocatorHTable_Val(ipFont->mAlphabet.mpHTable, keyval);
		arr[SRC_DATA_X] = data->mX;
		arr[SRC_DATA_Y] = data->mY;
		arr[SRC_DATA_W] = data->mW;
		arr[SRC_DATA_H] = data->mH;
		return data;
	}
	else
	/*
	else
	{
		arr[SRC_DATA_Y] = 0;
		arr[SRC_DATA_X] = i * ipFont->mAlphabet.mFixedWidth;
		if (arr[SRC_DATA_X] >= ipFont->mpAbcImage->mWidth)
		{
			arr[SRC_DATA_Y]	= (arr[SRC_DATA_X] / ipFont->mpAbcImage->mWidth) * ipFont->mAlphabet.mFixedHeight;
			arr[SRC_DATA_X] = arr[SRC_DATA_X] % ipFont->mpAbcImage->mWidth;
		}
		SDK_ASSERT(arr[SRC_DATA_Y] <= ipFont->mpAbcImage->mHeight); // Offset for  char is out of font-image Y-bound
		SDK_ASSERT(arr[SRC_DATA_X] <= ipFont->mpAbcImage->mWidth); // Offset for  char is out of font-image X-bound
		arr[SRC_DATA_W] = ipFont->mpAbcImage->mWidth;
		arr[SRC_DATA_H] = ipFont->mpAbcImage->mHeight;
	}
	*/
	{
		arr[SRC_DATA_X] = 0;
		arr[SRC_DATA_Y] = 0;
		arr[SRC_DATA_W] = ipFont->mAlphabet.mpAlphabetTextureOffsetMap[0].mW;
		arr[SRC_DATA_H] = ipFont->mAlphabet.mpAlphabetTextureOffsetMap[0].mH;
		return NULL;
	}
	/*else
	{
		arr[SRC_DATA_Y] = 0;
		arr[SRC_DATA_X] = 0;
		arr[SRC_DATA_W] = ipFont->mAlphabet.mFixedWidth;
		arr[SRC_DATA_H] = ipFont->mAlphabet.mFixedHeight;
		SDK_ASSERT(arr[SRC_DATA_Y] <= ipFont->mpAbcImage->mHeight); // Offset for  char is out of font-image Y-bound
		SDK_ASSERT(arr[SRC_DATA_X] <= ipFont->mpAbcImage->mWidth); // Offset for  char is out of font-image X-bound
		return 0;
	}*/
}
//----------------------------------------------------------------------------------------------------------------

void _drawText(struct TEngineInstance *ei, s32 pr_id)
{
	s32 sct;
	const wchar **ptxt;
	struct TERFont *pfont;
	SDK_NULL_ASSERT(cd.res_fnt); // Something wrong with MapEditor generated data. Possible wrong path to res data in editor.ini
	pfont = cd.res_fnt[ei->pr_textdata[pr_id][TXTFONTID]];
	SDK_NULL_ASSERT(pfont); // Text is not assigned with font
	SDK_NULL_ASSERT(pfont->mAlphabet.mpHTable); // Current font is not assigned by data
	SDK_NULL_ASSERT(pfont->mpAbcImage);
	if(ei->pr_textdata[pr_id][TXTCHRCOUNT] == MAX_RES_IDX)
	{
		sct = (s32)ei->pr_textdatatext[pr_id];
		ptxt = cd.textdatatext[sct];
		sct = cd.textdatatext_sct[sct];
	}
	else
	{
		ptxt = (const wchar **)ei->pr_textdatatext[pr_id];
		sct = ei->pr_textdata[pr_id][TXTSTRCOUNT];
	}
	if(ptxt != NULL && ei->pr_textdata[pr_id][TXTWIDTH] > 0 && ei->pr_textdata[pr_id][TXTHEIGHT] > 0)
	{
		struct DrawImageFunctionData fd;
		const struct AlphabetTextureOffsetMap* atd;
		s32 i, j, len, xb, xe, xs; 
		fx32 ty, tx0, ty0;
#ifdef CUSTOMAFFINETRANSFORM_SUPPORT
		fx32 scaleX;
		fx32 scaleY;
#endif
		fd.mType = DIT_Obj;
		fd.text = TRUE;
		fd.mpClipRect = NULL;
		fd.mpSrcData = pfont->mpAbcImage;
		SDK_NULL_ASSERT(ei->pr_fdata);
#ifdef CUSTOMAFFINETRANSFORM_SUPPORT
		SDK_NULL_ASSERT(ei->pr_cfdata);
		fd.mSin = ei->pr_cfdata[pr_id][FROTSIN];
		fd.mCos = ei->pr_cfdata[pr_id][FROTCOS];
		scaleX = cd.ainst->pr_cfdata[pr_id][FCUSTOMSCALEXVALUE];
		scaleY = cd.ainst->pr_cfdata[pr_id][FCUSTOMSCALEYVALUE];
		tx0 = fd.mX = ei->pr_fdata[pr_id][FX] - ei->worldx_fx - FX_Mul(FX32(ei->pr_textdata[pr_id][TXTWIDTH]) / 2, scaleX);
		ty0 = ty = ei->pr_fdata[pr_id][FY] - ei->worldy_fx - FX_Mul(FX32(ei->pr_textdata[pr_id][TXTHEIGHT]) / 2, scaleY);
#else
		tx0 = fd.mX = ei->pr_fdata[pr_id][FX] - ei->worldx_fx - FX32(ei->pr_textdata[pr_id][TXTWIDTH]) / 2;
		ty0 = ty = ei->pr_fdata[pr_id][FY] - ei->worldy_fx - FX32(ei->pr_textdata[pr_id][TXTHEIGHT]) / 2;
#endif
		if((ei->pr_textdata[pr_id][TXTCOLOR] & GX_RGBA_A_MASK) != 0)
		{
			RENDERFN(SetColor)(ei->pr_textdata[pr_id][TXTCOLOR]);
			RENDERFN(ColorRect)(fd.mX, ty,
#ifdef CUSTOMAFFINETRANSFORM_SUPPORT
								FX_Mul(FX32(ei->pr_textdata[pr_id][TXTWIDTH]), scaleX),
								FX_Mul(FX32(ei->pr_textdata[pr_id][TXTHEIGHT]), scaleY));
#else
								FX32(ei->pr_textdata[pr_id][TXTWIDTH]),
								FX32(ei->pr_textdata[pr_id][TXTHEIGHT]));
#endif
		}
		if ((ei->pr_textdataalign[pr_id] & TEXT_ALIGNMENT_VCENTER) == TEXT_ALIGNMENT_VCENTER)
		{
#ifdef CUSTOMAFFINETRANSFORM_SUPPORT
			ty += FX_Mul(FX32((ei->pr_textdata[pr_id][TXTHEIGHT] - pfont->mAlphabet.mFixedHeight * sct) / 2), scaleY);
#else
			ty += FX32((ei->pr_textdata[pr_id][TXTHEIGHT] - pfont->mAlphabet.mFixedHeight * sct) / 2);
#endif
		}
		else if ((ei->pr_textdataalign[pr_id] & TEXT_ALIGNMENT_BOTTOM) == TEXT_ALIGNMENT_BOTTOM)
		{
#ifdef CUSTOMAFFINETRANSFORM_SUPPORT
			ty += FX_Mul(FX32(ei->pr_textdata[pr_id][TXTHEIGHT] - pfont->mAlphabet.mFixedHeight * sct), scaleY);
#else
			ty += FX32(ei->pr_textdata[pr_id][TXTHEIGHT] - pfont->mAlphabet.mFixedHeight * sct);
#endif
		}
		SDK_NULL_ASSERT(ei->pr_cidata);
		fd.mFillColor = (ei->pr_cidata[pr_id][ICUSTOMALPHA] << 16) | ((u16)cd.ainst->pr_cidata[pr_id][ICUSTOMBLENDCOLOR]);
		for(j = 0; j < sct; j++)
		{
			fd.mSrcSizeData[SRC_DATA_H] = 0;
			len = (s32)ptxt[j][0];
			if ((ei->pr_textdataalign[pr_id] & TEXT_ALIGNMENT_HCENTER) == TEXT_ALIGNMENT_HCENTER)
			{
                fd.mX = 0;
				for (i = 0; i < len; i ++)
				{
					const wchar *ch = &ptxt[j][i + 1];
					_findTextureOffset(pfont, *ch, fd.mSrcSizeData);
					if ((ei->pr_textdataalign[pr_id] & TEXT_ALIGNMENT_HCENTER) == TEXT_ALIGNMENT_HCENTER)
					{
						if (*ch == W_T(' '))
						{
#ifdef CUSTOMAFFINETRANSFORM_SUPPORT
							fd.mX += FX_Mul(FX32(pfont->mSpaceCharSize), scaleX);
#else
							fd.mX += FX32(pfont->mSpaceCharSize);
#endif
						}
						else
						{
#ifdef CUSTOMAFFINETRANSFORM_SUPPORT
							fd.mX += FX_Mul(FX32(fd.mSrcSizeData[SRC_DATA_W] + ei->pr_textdata[pr_id][TXTMARGIN]), scaleX);
#else
							fd.mX += FX32(fd.mSrcSizeData[SRC_DATA_W] + ei->pr_textdata[pr_id][TXTMARGIN]);
#endif
						}
					}
				}
#ifdef CUSTOMAFFINETRANSFORM_SUPPORT
				fd.mX = tx0 + (FX_Mul(FX32(ei->pr_textdata[pr_id][TXTWIDTH]), scaleX) - fd.mX) / 2;
#else
				fd.mX = tx0 + (FX32(ei->pr_textdata[pr_id][TXTWIDTH]) - fd.mX) / 2;
#endif
				xb = 0;
				xe = len;
				xs = 1;
			}
			else if ((ei->pr_textdataalign[pr_id] & TEXT_ALIGNMENT_LEFT) == TEXT_ALIGNMENT_LEFT)
			{
                fd.mX = tx0;
				xb = 0;
				xe = len;
				xs = 1;
			}
			else if ((ei->pr_textdataalign[pr_id] & TEXT_ALIGNMENT_RIGHT) == TEXT_ALIGNMENT_RIGHT)
			{
#ifdef CUSTOMAFFINETRANSFORM_SUPPORT
				fd.mX = tx0 + FX_Mul(FX32(ei->pr_textdata[pr_id][TXTWIDTH]), scaleX);
#else
				fd.mX = tx0 + FX32(ei->pr_textdata[pr_id][TXTWIDTH]);
#endif
				xb = len - 1;
				xe = -1;
				xs = -1;
			}
			else
			{
				xb = xe = xs = -1;
			}
			for (i = xb; i != xe; i += xs)
			{
				const wchar* ch = &ptxt[j][i + 1];
				if ((atd = _findTextureOffset(pfont, *ch, fd.mSrcSizeData)) != NULL)
				{
					if (xs < 0)
					{
						if (*ch == W_T(' '))
						{
#ifdef CUSTOMAFFINETRANSFORM_SUPPORT
							fd.mX -= FX_Mul(FX32(pfont->mSpaceCharSize), scaleX);
#else
							fd.mX -= FX32(pfont->mSpaceCharSize);
#endif
						}
						else
						{
#ifdef CUSTOMAFFINETRANSFORM_SUPPORT
							fd.mX -= FX_Mul(FX32(fd.mSrcSizeData[SRC_DATA_W] + ei->pr_textdata[pr_id][TXTMARGIN]), scaleX);
#else
							fd.mX -= FX32(fd.mSrcSizeData[SRC_DATA_W] + ei->pr_textdata[pr_id][TXTMARGIN]);
#endif
						}
					
					}
#ifdef CUSTOMAFFINETRANSFORM_SUPPORT
					if (((fd.mX - tx0) + FX_Mul(FX32(fd.mSrcSizeData[SRC_DATA_W]), scaleX) < 0 || (fd.mX - tx0) > FX_Mul(FX32(ei->pr_textdata[pr_id][TXTWIDTH]), scaleX))
#else
					if ((fx2int(fd.mX - tx0) + fd.mSrcSizeData[SRC_DATA_W] < 0 || fx2int(fd.mX - tx0) > ei->pr_textdata[pr_id][TXTWIDTH])
#endif
							&& (ei->pr_textdataalign[pr_id] & TEXT_ALIGNMENT_HCENTER) != TEXT_ALIGNMENT_HCENTER)
				    {
						break;
				    }
#ifdef CUSTOMAFFINETRANSFORM_SUPPORT
					if ((fd.mX - tx0) + FX_Mul(FX32(fd.mSrcSizeData[SRC_DATA_W]), scaleX) > FX_Mul(FX32(ei->pr_textdata[pr_id][TXTWIDTH]), scaleX))
#else
					if(fx2int(fd.mX - tx0) + fd.mSrcSizeData[SRC_DATA_W] > ei->pr_textdata[pr_id][TXTWIDTH])
#endif
					{
						const s32 dest_w = ei->pr_textdata[pr_id][TXTWIDTH] + fx2int(tx0 - fd.mX);
						if (dest_w > 0)
						{
							fd.mSrcSizeData[SRC_DATA_W] = (u16)dest_w;
						}
						else
						{
							break;
						}
					}
#ifdef CUSTOMAFFINETRANSFORM_SUPPORT
					if ((ty - ty0) + FX_Mul(FX32(atd->mY_off + fd.mSrcSizeData[SRC_DATA_H]), scaleY) > FX_Mul(FX32(ei->pr_textdata[pr_id][TXTHEIGHT]), scaleY))
#else
					if(fx2int(ty - ty0) + atd->mY_off + fd.mSrcSizeData[SRC_DATA_H] > ei->pr_textdata[pr_id][TXTHEIGHT])
#endif
					{
						const s32 dest_h = ei->pr_textdata[pr_id][TXTHEIGHT] - atd->mY_off + fx2int(ty0 - ty);
						if (dest_h > 0)
						{
							fd.mSrcSizeData[SRC_DATA_H] = (u16)dest_h;
						}
						else
						{
							continue;
						}
					}
#ifdef CUSTOMAFFINETRANSFORM_SUPPORT
					fd.mY = ty + FX_Mul(FX32(atd->mY_off), scaleY);
					fd.mAffineOriginX = fd.mX - tx0 - FX_Mul(FX32(ei->pr_textdata[pr_id][TXTWIDTH]) / 2, scaleX);
					fd.mAffineOriginY = fd.mY - ty0 - FX_Mul(FX32(ei->pr_textdata[pr_id][TXTHEIGHT]) / 2, scaleY);
					fd.mDestWidth = FX_Mul(FX32(fd.mSrcSizeData[SRC_DATA_W]), scaleX);
					fd.mDestHeight = FX_Mul(FX32(fd.mSrcSizeData[SRC_DATA_H]), scaleY);
#else
					fd.mY = ty + FX32(atd->mY_off);
#endif				
					if(ei->pr_idata[pr_id][ISTATE] == TRUE)
					{
						fd.mX = fxFloor(fd.mX);
						fd.mY = fxFloor(fd.mY);
					}
					RENDERFN(DrawImage)(&fd);
				}
				else if (xs < 0)
				{
					if (*ch == W_T(' '))
					{
#ifdef CUSTOMAFFINETRANSFORM_SUPPORT
						fd.mX -= FX_Mul(FX32(pfont->mSpaceCharSize), scaleX);
#else
						fd.mX -= FX32(pfont->mSpaceCharSize);
#endif
					}
					else
					{
#ifdef CUSTOMAFFINETRANSFORM_SUPPORT
						fd.mX -= FX_Mul(FX32(fd.mSrcSizeData[SRC_DATA_W] + ei->pr_textdata[pr_id][TXTMARGIN]), scaleX);
#else
						fd.mX -= FX32(fd.mSrcSizeData[SRC_DATA_W] + ei->pr_textdata[pr_id][TXTMARGIN]);
#endif
					}
				}
				if (xs > 0)
				{
					if (*ch == W_T(' '))
					{
#ifdef CUSTOMAFFINETRANSFORM_SUPPORT
						fd.mX += FX_Mul(FX32(pfont->mSpaceCharSize), scaleX);
#else
						fd.mX += FX32(pfont->mSpaceCharSize);
#endif
					}
					else
					{
#ifdef CUSTOMAFFINETRANSFORM_SUPPORT
						fd.mX += FX_Mul(FX32(fd.mSrcSizeData[SRC_DATA_W] + ei->pr_textdata[pr_id][TXTMARGIN]), scaleX);
#else
						fd.mX += FX32(fd.mSrcSizeData[SRC_DATA_W] + ei->pr_textdata[pr_id][TXTMARGIN]);
#endif
					}
				}
			}
#ifdef CUSTOMAFFINETRANSFORM_SUPPORT
			ty += FX_Mul(FX32(pfont->mAlphabet.mFixedHeight + ei->pr_textdata[pr_id][TXTMARGIN]), scaleY);
#else
			ty += FX32(pfont->mAlphabet.mFixedHeight + ei->pr_textdata[pr_id][TXTMARGIN]);
#endif
#ifdef CUSTOMAFFINETRANSFORM_SUPPORT
			if (((ty - ty0) + FX_Mul(FX32(fd.mSrcSizeData[SRC_DATA_H]), scaleY) < 0 || (ty - ty0) > FX_Mul(FX32(ei->pr_textdata[pr_id][TXTHEIGHT]), scaleY))
#else
			if ((fx2int(ty - ty0) + fd.mSrcSizeData[SRC_DATA_H] < 0 || fx2int(ty - ty0) > ei->pr_textdata[pr_id][TXTHEIGHT])
#endif
				&& (ei->pr_textdataalign[pr_id] & TEXT_ALIGNMENT_VCENTER) != TEXT_ALIGNMENT_VCENTER)
			{
				break;
			}
		}
	}
}
//-------------------------------------------------------------------------------------------

void _draw(const u16 *frpc_src_data, struct DrawImageFunctionData *fd)
{
	SDK_NULL_ASSERT(frpc_src_data);
	SDK_ASSERT(cd.ainst->visible_mask != 0);
	MI_CpuCopy16(&frpc_src_data[RES_X], fd->mSrcSizeData, sizeof(u16) * 4);
	RENDERFN(DrawImage)(fd);
}
//-------------------------------------------------------------------------------------------

void setColor(GXRgba color)
{
	if (cd.ainst->visible_mask != 0)
	{
		RENDERFN(SetColor)(color);
	}
}
//-------------------------------------------------------------------------------------------

void fillRect(fx32 x, fx32 y, fx32 w, fx32 h)
{
	if (cd.ainst->visible_mask != 0)
	{
		RENDERFN(ColorRect)(x - cd.ainst->worldx_fx, y - cd.ainst->worldy_fx, w, h);
	}
}
//-------------------------------------------------------------------------------------------

void drawLine(fx32 x0, fx32 y0, fx32 x1, fx32 y1)
{
	if (cd.ainst->visible_mask != 0)
	{
		RENDERFN(DrawLine)(x0 - cd.ainst->worldx_fx,
					y0 - cd.ainst->worldy_fx,
					x1 - cd.ainst->worldx_fx,
					y1 - cd.ainst->worldy_fx);
	}
}
//-------------------------------------------------------------------------------------------

void drawSingleFrame(s32 group_id, s32 anim_id, s32 anim_frame, fx32 left, fx32 top, u8* alpha, 
						fx32* rotationAngle, fx32* scaleX, fx32* scaleY, fx32 affineOriginX, fx32 affineOriginY)
{
	if (cd.ainst->visible_mask != 0)
	{
		s32 i, pc_count;
		fx32 fl, ft;
		const s16 *frdata;
		struct DrawImageFunctionData fd;
		u16 grfr_id;
#ifdef CUSTOMAFFINETRANSFORM_SUPPORT
		fx32 scalex;
		fx32 scaley;
#endif
		SDK_NULL_ASSERT(cd.ainst->pr_idata);
		SDK_NULL_ASSERT(cd.ppr_fr_data);
		pc_count = fx2int(cd.ppr_fr_data[group_id][anim_id][anim_frame][ANIFRFRCOUNT]);
		fl = cd.ppr_fr_data[group_id][anim_id][anim_frame][ANIFROFFL];
		ft = cd.ppr_fr_data[group_id][anim_id][anim_frame][ANIFROFFT];
		fd.mType = DIT_Obj;
		fd.text = FALSE;
		fd.mpClipRect = NULL;
		fd.mFillColor = (((alpha != NULL) ? *alpha : (u8)ALPHA_OPAQ) << 16) | DIFD_DEF_FILL_COLOR;
#ifdef CUSTOMAFFINETRANSFORM_SUPPORT
		fd.mAffineOriginX = affineOriginX;
		fd.mAffineOriginY = affineOriginY;
		scalex = (scaleX != NULL) ? *scaleX : FX32_ONE;
		scaley = (scaleY != NULL) ? *scaleY : FX32_ONE;
#else
		(void)rotationAngle;
		(void)affineOriginX;
		(void)affineOriginY;
		(void)scaleX;
		(void)scaleY;
#endif
		for(i = 0; i < pc_count; i++)
		{
			frdata = _getFramePieceData(group_id, anim_id, anim_frame, i);
			SDK_NULL_ASSERT(cd.fr_data);
			fd.mFillColor = ((alpha != NULL) ? ((((frdata[ANIFRFRALPHA] * (*alpha * 0xff) / 0x1f)) / 0xff) << 16) : frdata[ANIFRFRALPHA] << 16) | DIFD_DEF_FILL_COLOR;
#ifdef CUSTOMAFFINETRANSFORM_SUPPORT
			if(rotationAngle == NULL)
			{
				//_getTransform(frdata[ANIFRFRDATA])
				fd.mSin = 0;
				fd.mCos = FX32_ONE;
			}
			else
			{
				fxCordic(FX_Mul((*rotationAngle / 180), FX_PI), &fd.mSin, &fd.mCos, FX_CORDIC_MIDDLE_PRECISION);
			}
			fd.mX = left - fl - cd.ainst->worldx_fx + FX_Mul(FX32(frdata[ANIFRFROFFX]), scalex);
			fd.mY = top - ft - cd.ainst->worldy_fx + FX_Mul(FX32(frdata[ANIFRFROFFY]), scaley);
#else
			fd.mX = left - fl - cd.ainst->worldx_fx + FX32(frdata[ANIFRFROFFX]);
			fd.mY = top - ft - cd.ainst->worldy_fx + FX32(frdata[ANIFRFROFFY]);
#endif
			if(_isStreamFrame(frdata[ANIFRFRDATA]))
			{
				SDK_ASSERT(0); // do not use this function for streamed frames
				return;	
			}
			else
			{
				grfr_id = (u16)frdata[ANIFRFRID];
			}
			if (cd.fr_data[grfr_id][RES_IMG_IDX] < NULL_IMG_IDX)
			{
				fd.mpSrcData = cd.res_img[cd.fr_data[grfr_id][RES_IMG_IDX]];
#ifdef CUSTOMAFFINETRANSFORM_SUPPORT
				fd.mDestWidth = FX_Mul(FX32(frdata[ANIFRFRWIDTH]), scalex);
				fd.mDestHeight = FX_Mul(FX32(frdata[ANIFRFRHEIGHT]), scaley);
#endif
				_draw(cd.fr_data[grfr_id], &fd);
			}
		}
	}
}
//-------------------------------------------------------------------------------------------

void _drawGameObj(s32 pr_id)
{
	SDK_ASSERT(cd.ainst->visible_mask != 0);
	SDK_NULL_ASSERT(cd.ainst->pr_idata);
	if(prIsTextBox(pr_id))
	{
		_drawText(cd.ainst, pr_id);
	}
	else
	{
		s32 pc_count, anim_id, i;
		const s16 *frdata;
		struct DrawImageFunctionData fd;
#ifdef CUSTOMAFFINETRANSFORM_SUPPORT
		const fx32 scaleX = cd.ainst->all_pr_count > pr_id ? cd.ainst->pr_cfdata[pr_id][FCUSTOMSCALEXVALUE] : FX32_ONE;
		const fx32 scaleY = cd.ainst->all_pr_count > pr_id ? cd.ainst->pr_cfdata[pr_id][FCUSTOMSCALEYVALUE] : FX32_ONE;
#endif
		fd.mType = DIT_Obj;
		_prGetFramePiecesInfo(cd.ainst, pr_id, &pc_count, &anim_id);
		fd.mpClipRect = NULL;
		fd.text = FALSE;
		if(cd.ainst->all_pr_count > pr_id)
		{
			SDK_NULL_ASSERT(cd.ainst->pr_cidata);
			if(cd.ainst->pr_cidata[pr_id][ICLIPRECTOBJ] != NONE_MAP_IDX)
			{
				SDK_NULL_ASSERT(cd.ainst->pr_fdata);
				cd.ainst->pr_cidata[cd.ainst->pr_cidata[pr_id][ICLIPRECTOBJ]][ICLIPRECTTEMPXY] = 
					((u16)(fx2int(cd.ainst->pr_fdata[cd.ainst->pr_cidata[pr_id][ICLIPRECTOBJ]][FLEFT] - cd.ainst->worldx_fx)) << 16) |
					(u16)(fx2int(cd.ainst->pr_fdata[cd.ainst->pr_cidata[pr_id][ICLIPRECTOBJ]][FTOP] - cd.ainst->worldy_fx));
				cd.ainst->pr_cidata[cd.ainst->pr_cidata[pr_id][ICLIPRECTOBJ]][ICLIPRECTTEMPWH] =
					((u16)(fx2int(cd.ainst->pr_fdata[cd.ainst->pr_cidata[pr_id][ICLIPRECTOBJ]][FCURRWIDTH]) << 16) |
					(u16)(fx2int(cd.ainst->pr_fdata[cd.ainst->pr_cidata[pr_id][ICLIPRECTOBJ]][FCURRHEIGHT])));
				fd.mpClipRect = &cd.ainst->pr_cidata[cd.ainst->pr_cidata[pr_id][ICLIPRECTOBJ]][ICLIPRECTTEMPXY];
			}
		}
		for(i = 0; i < pc_count; i++)
		{
			frdata = _getFramePieceData(cd.ainst->pr_idata[pr_id][IPARENT_IDX], anim_id, cd.ainst->pr_idata[pr_id][IANIFRIDX], i);
			SDK_NULL_ASSERT(cd.fr_data);
#ifdef CUSTOMAFFINETRANSFORM_SUPPORT
			fd.mAffineOriginX = FX_Mul(FX32(frdata[ANIFRFROFFX]), scaleX);
			fd.mAffineOriginY = FX_Mul(FX32(frdata[ANIFRFROFFY]), scaleY);
#endif
			if(cd.ainst->all_pr_count > pr_id)
			{
				SDK_NULL_ASSERT(cd.ainst->pr_cidata);
#ifdef CUSTOMAFFINETRANSFORM_SUPPORT
				SDK_NULL_ASSERT(cd.ainst->pr_cfdata);
				if((cd.ainst->pr_cidata[pr_id][ICUSTOMFLAGS] & ICUSTOMROTATIONFLAG) == ICUSTOMROTATIONFLAG)
				{
					fd.mSin = cd.ainst->pr_cfdata[pr_id][FROTSIN];
					fd.mCos = cd.ainst->pr_cfdata[pr_id][FROTCOS];
				}
				else
				{
					fd.mSin = 0;
					fd.mCos = FX32_ONE;
				}
				if((cd.ainst->pr_cidata[pr_id][ICUSTOMFLAGS] & ICUSTOMSCALEFLAG) == ICUSTOMSCALEFLAG)
				{
					fd.mDestWidth = FX_Mul(FX32(frdata[ANIFRFRWIDTH]), scaleX);
					fd.mDestHeight = FX_Mul(FX32(frdata[ANIFRFRHEIGHT]), scaleY);
					fd.mX = cd.ainst->pr_fdata[pr_id][FX] - cd.ainst->worldx_fx + FX_Mul(FX32(frdata[ANIFRFROFFX]), scaleX);
					fd.mY = cd.ainst->pr_fdata[pr_id][FY] - cd.ainst->worldy_fx + FX_Mul(FX32(frdata[ANIFRFROFFY]), scaleY);
				}
				else
				{
					fd.mDestWidth = FX32(frdata[ANIFRFRWIDTH]);
					fd.mDestHeight = FX32(frdata[ANIFRFRHEIGHT]);
					fd.mX = cd.ainst->pr_fdata[pr_id][FX] - cd.ainst->worldx_fx + FX32(frdata[ANIFRFROFFX]);
					fd.mY = cd.ainst->pr_fdata[pr_id][FY] - cd.ainst->worldy_fx + FX32(frdata[ANIFRFROFFY]);
				}
#else
				fd.mX = cd.ainst->pr_fdata[pr_id][FX] - cd.ainst->worldx_fx + FX32(frdata[ANIFRFROFFX]);
				fd.mY = cd.ainst->pr_fdata[pr_id][FY] - cd.ainst->worldy_fx + FX32(frdata[ANIFRFROFFY]);
#endif
				if((cd.ainst->pr_cidata[pr_id][ICUSTOMFLAGS] & ICUSTOMALPHAFLAG) == ICUSTOMALPHAFLAG)
				{
					fd.mFillColor = ((((frdata[ANIFRFRALPHA] * (cd.ainst->pr_cidata[pr_id][ICUSTOMALPHA] * 0xff) / 0x1f)) / 0xff) << 16) | ((u16)cd.ainst->pr_cidata[pr_id][ICUSTOMBLENDCOLOR]);
				}
				else
				{
					fd.mFillColor = (frdata[ANIFRFRALPHA] << 16) | ((u16)cd.ainst->pr_cidata[pr_id][ICUSTOMBLENDCOLOR]);
				}
			}
			else
			{
				fd.mX = cd.ainst->pr_fdata[pr_id][FX] - cd.ainst->worldx_fx + FX32(frdata[ANIFRFROFFX]);
				fd.mY = cd.ainst->pr_fdata[pr_id][FY] - cd.ainst->worldy_fx + FX32(frdata[ANIFRFROFFY]);
				fd.mFillColor = (frdata[ANIFRFRALPHA] << 16) | DIFD_DEF_FILL_COLOR;
#ifdef CUSTOMAFFINETRANSFORM_SUPPORT
				fd.mSin = 0;
				fd.mCos = FX32_ONE;
				fd.mDestWidth = FX32(frdata[ANIFRFRWIDTH]);
				fd.mDestHeight = FX32(frdata[ANIFRFRHEIGHT]);
#endif
			}
			if(_isStreamFrame(frdata[ANIFRFRDATA]))
			{
				s32 val;
				const struct BMPImage *rimage;
				SDK_NULL_ASSERT(cd.strm_obj_data);
				SDK_NULL_ASSERT(cd.strm_obj_data[pr_id][anim_id]);
				rimage = cd.strm_obj_data[pr_id][anim_id]->mData[0].mpImage; 
				do
				{
					val = FALSE;
#if defined JOBS_IN_SEPARATE_THREAD && defined USE_OPENGL_RENDER
					if(!cd.strm_obj_data[pr_id][anim_id]->mData[0].mBindStatus)
#else
					if(!cd.strm_obj_data[pr_id][anim_id]->mData[0].mBufferStatus)
#endif
					{
						BOOL skipFrame = TRUE; 
						if(_onStreamFrameWaitingFn != NULL)
						{
							_onStreamFrameWaitingFn(cd.a_layer, pr_id, &skipFrame);
						}
						if(skipFrame == FALSE)
						{
#if defined JOBS_IN_SEPARATE_THREAD
#if defined USE_OPENGL_RENDER
							val = !IsFileSystemError();
							while(val && !jobBindTexturesToVRAM())
							{
								val = !IsFileSystemError();
								Sleep(1);
							}
#endif
#else
							jobUpdateAnimStreamTasksAndLoadTextures();
#endif
						}
						else
						{
							rimage = cd.strm_obj_data[pr_id][anim_id]->mpLastDrawnImage;	
						}
					}
				}while(val);
//#ifdef JOBS_IN_SEPARATE_THREAD
//				jobCriticalSectionBegin();
//#endif
				if(rimage != NULL)
				{
					fd.mpSrcData = rimage;
					cd.strm_obj_data[pr_id][anim_id]->mpLastDrawnImage = rimage;
					_draw(cd.strm_obj_data[pr_id][anim_id]->mData[0].mFrpcdata, &fd);
				}
//#ifdef JOBS_IN_SEPARATE_THREAD
//				jobCriticalSectionEnd();
//#endif
			}
			else
			{
				SDK_ASSERT(frdata[ANIFRFRID] >= 0);
				if (cd.fr_data[frdata[ANIFRFRID]][RES_IMG_IDX] < NULL_IMG_IDX)
				{
					fd.mpSrcData = cd.res_img[cd.fr_data[frdata[ANIFRFRID]][RES_IMG_IDX]];
					_draw(cd.fr_data[frdata[ANIFRFRID]], &fd);
				}
			}
		}
	}
#ifdef DRAW_DEBUG_COLLIDERECTS
		if(cd.ainst->all_pr_count > pr_id)
		{
			fx32 rect[RECT_SIZE];
			prGetCollideRect(pr_id, rect);
			if(cd.ainst->pr_cfdata[pr_id][FCBLTX] != cd.ainst->pr_cfdata[pr_id][FCBRTX] ||
				cd.ainst->pr_cfdata[pr_id][FCBLTY] != cd.ainst->pr_cfdata[pr_id][FCBLBY])
			{
				fx32 xrt, yrt, xlb, ylb, xlt, ylt, xrb, yrb;
#ifdef CUSTOMAFFINETRANSFORM_SUPPORT
				xlt = cd.ainst->pr_cfdata[pr_id][FCBLTX];
				ylt = cd.ainst->pr_cfdata[pr_id][FCBLTY];
				xrt = cd.ainst->pr_cfdata[pr_id][FCBRTX];
				yrt = cd.ainst->pr_cfdata[pr_id][FCBRTY];
				xrb = cd.ainst->pr_cfdata[pr_id][FCBRBX];
				yrb = cd.ainst->pr_cfdata[pr_id][FCBRBY];
				xlb = cd.ainst->pr_cfdata[pr_id][FCBLBX];
				ylb = cd.ainst->pr_cfdata[pr_id][FCBLBY];
#else
				xrt = cd.ainst->prg_idata[pr_id][ICBRIGHT0];
				yrt = cd.ainst->prg_idata[pr_id][ICBTOP0];
				xlb = cd.ainst->prg_idata[pr_id][ICBLEFT0];
				ylb = cd.ainst->prg_idata[pr_id][ICBBOTTOM0];
				xlt = xlb;
				ylt = yrt;
				xrb = xrt;
				yrb = ylb;
#endif
				setColor(GX_RGBA(31, 0, 0, 1));
				drawLine(xlt, ylt, xrt, yrt);
				drawLine(xrt, yrt, xrb, yrb);
				drawLine(xrb, yrb, xlb, ylb);
				drawLine(xlb, ylb, xlt, ylt);
			}
		}
#endif

#ifdef DRAW_DEBUG_VIEWRECTS
		if(cd.ainst->all_pr_count > pr_id)
		{
			fx32 rect[RECT_SIZE];
			prGetViewRect(pr_id, rect);
			if(cd.ainst->pr_cfdata[pr_id][FVBLTX] != cd.ainst->pr_cfdata[pr_id][FVBRTX] ||
				cd.ainst->pr_cfdata[pr_id][FVBLTY] != cd.ainst->pr_cfdata[pr_id][FVBLBY])
			{
				fx32 xrt, yrt, xlb, ylb, xlt, ylt, xrb, yrb; 
#ifdef CUSTOMAFFINETRANSFORM_SUPPORT				
				xlt = cd.ainst->pr_cfdata[pr_id][FVBLTX];
				ylt = cd.ainst->pr_cfdata[pr_id][FVBLTY];
				xrt = cd.ainst->pr_cfdata[pr_id][FVBRTX];
				yrt = cd.ainst->pr_cfdata[pr_id][FVBRTY];
				xrb = cd.ainst->pr_cfdata[pr_id][FVBRBX];
				yrb = cd.ainst->pr_cfdata[pr_id][FVBRBY];
				xlb = cd.ainst->pr_cfdata[pr_id][FVBLBX];
				ylb = cd.ainst->pr_cfdata[pr_id][FVBLBY];
#else
				xrt = cd.ainst->prg_idata[pr_id][IVBRIGHT0];
				yrt = cd.ainst->prg_idata[pr_id][IVBTOP0];
				xlb = cd.ainst->prg_idata[pr_id][IVBLEFT0];
				ylb = cd.ainst->prg_idata[pr_id][IVBBOTTOM0];
				xlt = xlb;
				ylt = yrt;
				xrb = xrt;
				yrb = ylb;
#endif
				setColor(GX_RGBA(0, 0, 31, 1));
				drawLine(xlt, ylt, xrt, yrt);
				drawLine(xrt, yrt, xrb, yrb);
				drawLine(xrb, yrb, xlb, ylb);
				drawLine(xlb, ylb, xlt, ylt);
			}
		}
#endif
}

//-------------------------------------------------------------------------------------------

BOOL prIsTextBox(s32 pr_id)
{
	return cd.ainst->pr_idata[pr_id][IPARENT_IDX] == PARENT_TEXT_OBJ;
}
//-------------------------------------------------------------------------------------------

BOOL prIsTextBoxLayer(s32 pr_id, u32 layer)
{
	struct TEngineInstance *l = _getInstance(layer);
	return l->pr_idata[pr_id][IPARENT_IDX] == PARENT_TEXT_OBJ;
}
//-------------------------------------------------------------------------------------------

BOOL callForEachGameObject(s32 iId, CallForEachGameObjectFn pCallbackFn)
{
	SDK_NULL_ASSERT(pCallbackFn);
	if(cd.ainst->ueof_state != UEOF_EMPTY)
	{
		BOOL t_rl_available;
		const struct AllocatorList* rlist;
		const struct ListItem* it;
		SDK_NULL_ASSERT(cd.ainst->current_zone >= 0);
		SDK_NULL_ASSERT(cd.ainst->rndr_znlist);
		SDK_NULL_ASSERT(cd.ainst->pr_count);
		t_rl_available = cd.rl_available;
		cd.rl_available = FALSE;
		rlist = cd.ainst->rndr_znlist[cd.ainst->current_zone];
		it = AllocatorList_Begin_Const(rlist);
		while (it != AllocatorList_End_Const(rlist))
		{
			const u16 oid = *(u16*)AllocatorList_Val(rlist, it);
			pCallbackFn(cd.a_layer, iId, oid);
			it = AllocatorList_Next_Const(rlist, it);
		}
		cd.rl_available = t_rl_available;
		return TRUE;
	}
	return FALSE;
}
//-------------------------------------------------------------------------------------------

void _drawObjectsWithUpdate(struct TEngineInstance* ei, u32 layer, s32 ms)
{
	BOOL draw;
	s16 *gfo, *gbo;
	s32 gfo_ct, gbo_ct, i;
	const struct AllocatorList* rlist;
	const struct ListItem* it;
	
	SDK_NULL_ASSERT(ei->current_zone >= 0);
	SDK_NULL_ASSERT(cd.ainst->rndr_znlist);
	SDK_NULL_ASSERT(ei->pr_count);

	gbo = ei->db_znlist[ei->current_zone];
	gfo	= ei->df_znlist[ei->current_zone];
	gfo_ct = ei->df_count[ei->current_zone];
	gbo_ct = ei->db_count[ei->current_zone];

	_setActiveLayer(layer);

	for(i = 0; i < gbo_ct; i++)
	{
		_updateAnimation(gbo[i], ms);
	}
	rlist = cd.ainst->rndr_znlist[cd.ainst->current_zone];
	it = AllocatorList_Begin_Const(rlist);
	while (it != AllocatorList_End_Const(rlist))
	{
		const u16 oid = *(u16*)AllocatorList_Val(rlist, it);
		_updateAnimation(oid, ms);
		it = AllocatorList_Next_Const(rlist, it);
	}
	for(i = 0; i < gfo_ct; i++)
	{
		_updateAnimation(gfo[i], ms);
	}
	if(cd.strm_instance == ei)
	{
#if defined JOBS_IN_SEPARATE_THREAD
	#if defined USE_OPENGL_RENDER
		jobBindTexturesToVRAM();
	#endif
#else
		jobUpdateAnimStreamTasksAndLoadTextures();
#endif
	}
	
	if(cd._onBeginUpdate)
	{
		cd.rl_available = TRUE;
		cd._onBeginUpdate(layer);
	}
	cd.rl_available = FALSE;
	draw = TRUE;
	if(ei->primary && ei->visible_mask != 0)
	{
		_drawBackgroundTiles(TRUE);
	}
	if(cd._onDrawBackgroundTiles)
	{
		cd.rl_available = TRUE;
		cd._onDrawBackgroundTiles(layer);
		cd.rl_available = FALSE;
	}
	for(i = 0; i < gbo_ct; i++)
	{
		draw = TRUE;
		if(cd._onDrawDecorationObject)
		{
			cd._onDrawDecorationObject(layer, gbo[i], &draw);
		}
		if(draw && ei->visible_mask != 0)
		{
			_drawGameObj(gbo[i]);
		}
		if(ei->pr_idata[gbo[i]][ITEMPSTATE] != -1)
		{
			_setState(ei, gbo[i], ei->pr_idata[gbo[i]][ITEMPSTATE], FALSE);
		}
	}
	if(ei->rndr_znlist[ei->current_zone])
	{
		if(cd.enable_prp_id != NONE_MAP_IDX)
		{
			rlist = ei->rndr_znlist[ei->current_zone];
			it = AllocatorList_Begin_Const(rlist);
			while (it != AllocatorList_End_Const(rlist))
			{
				const u16 oid = *(u16*)AllocatorList_Val(rlist, it);
				if(prGetProperty(oid, cd.enable_prp_id) != 0)
				{
					draw = TRUE;
					_processMovement(oid);
					if(cd._onDrawGameObject)
					{
						cd._onDrawGameObject(layer, oid, &draw);
					}
					if(draw && ei->visible_mask != 0)
					{
						_drawGameObj(oid);
					}
				}
				it = AllocatorList_Next_Const(rlist, it);
			}
		}
		else
		{
			rlist = ei->rndr_znlist[ei->current_zone];
			it = AllocatorList_Begin_Const(rlist);
			while (it != AllocatorList_End_Const(rlist))
			{
				u16 oid = *(u16*)AllocatorList_Val(rlist, it);
				draw = TRUE;
				_processMovement(oid);
				if(cd._onDrawGameObject)
				{
					cd._onDrawGameObject(layer, oid, &draw);
				}
				if(draw && ei->visible_mask != 0)
				{
					_drawGameObj(oid);
				}
				it = AllocatorList_Next_Const(rlist, it);
			}
		}
	}
	for(i = 0; i < gfo_ct; i++)
	{
		draw = TRUE;
		if(cd._onDrawDecorationObject)
		{
			cd._onDrawDecorationObject(layer, gfo[i], &draw);
		}
		if(draw && ei->visible_mask != 0)
		{
			_drawGameObj(gfo[i]);
		}
		if(ei->pr_idata[gfo[i]][ITEMPSTATE] != -1)
		{
			_setState(ei, gfo[i], ei->pr_idata[gfo[i]][ITEMPSTATE], FALSE);
		}
	}
	rlist = cd.ainst->rndr_znlist[cd.ainst->current_zone];
	it = AllocatorList_Begin_Const(rlist);
	while (it != AllocatorList_End_Const(rlist))
	{
		const u16 oid = *(u16*)AllocatorList_Val(rlist, it);
		if (ei->pr_idata[oid][ITEMPSTATE] != -1)
		{
			_setState(ei, oid, ei->pr_idata[oid][ITEMPSTATE], TRUE);
		}
		it = AllocatorList_Next_Const(rlist, it);
	}
	if(ei->visible_mask != 0)
	{
		_drawParticles(ms);
	}
	else
	{
		_updateParticlesWithoutDraw(ms);
	}
	cd.rl_available = TRUE;
	if(cd._onFinishUpdate)
	{
		cd._onFinishUpdate(layer);
	}
	if(ei->t_current_zone != ei->current_zone)
	{
		ei->current_zone = ei->t_current_zone;
	}
}
//-------------------------------------------------------------------------------------------

void _drawObjectsDummy(struct TEngineInstance* ei, u32 layer, s32 ms)
{
	(void)ei;
	(void)layer;
	(void)ms;
}
//-------------------------------------------------------------------------------------------

void _drawObjectsWithoutUpdate(struct TEngineInstance* ei, u32 layer, s32 ms)
{
	(void)ms;
	cd.deltatime_scale = 0;
	_drawObjectsWithUpdate(ei, layer, 0);
	cd.deltatime_scale = FX_Div(FIXED_TIMESTEP_MS_FX, cd.delay_fx);
}
//-------------------------------------------------------------------------------------------

void drawWithoutInternalLogicUpdateMode()
{
	RENDERFN(FreezeCurrentFrameBuffer)(TRUE);
}
//-------------------------------------------------------------------------------------------

// Particles

//-------------------------------------------------------------------------------------------
void createParticles(s32 pr_idx,
					  u32 colCount,
					  u32 rowCount,
					  fx32 minSpeed,
					  fx32 varSpeed,
					  u32 minLifeTime,
					  u32 varLifeTime,
					  u32 exploudTime,
					  fx32 *targetX,
					  fx32 *targetY)
{
	s32 i, ox, oy, ct, ws, hs, a, pc_count, anim_id, wd, hd;
	const s16 *frdata;
	fx32 *pcs;
	fx32 fpcl, fpct;
#ifdef CUSTOMAFFINETRANSFORM_SUPPORT
	fx32 scaleX, scaleY;
#endif
	u16 grfr_id;
	u32 x, y;
	SDK_NULL_ASSERT(cd.ainst->pr_idata);
	SDK_NULL_ASSERT(cd.fr_data);
	if(cd.initParams.particlesPoolSize == 0)
	{
		return;
	}
	_prGetFramePiecesInfo(cd.ainst, pr_idx, &pc_count, &anim_id);
	for(i = 0; i < pc_count; i++)
	{
		frdata = _getFramePieceData(cd.ainst->pr_idata[pr_idx][IPARENT_IDX], anim_id, cd.ainst->pr_idata[pr_idx][IANIFRIDX], i);
		SDK_NULL_ASSERT(cd.fr_data);
		if(_isStreamFrame(frdata[ANIFRFRDATA]))
		{
			SDK_ASSERT(0); // do not use this function for streamed frames
			return;	
		}
		grfr_id = (u16)frdata[ANIFRFRID];
		SDK_ASSERT(grfr_id < NULL_IMG_IDX);
#ifdef CUSTOMAFFINETRANSFORM_SUPPORT
		wd = (s32)frdata[ANIFRFRWIDTH] / (s32)colCount;
		hd = (s32)frdata[ANIFRFRHEIGHT] / (s32)rowCount;
		ws = (s32)cd.fr_data[grfr_id][RES_W] / (s32)colCount;
		hs = (s32)cd.fr_data[grfr_id][RES_H] / (s32)rowCount;
		if ((cd.ainst->pr_cidata[pr_idx][ICUSTOMFLAGS] & ICUSTOMSCALEFLAG) == ICUSTOMSCALEFLAG)
		{
			scaleX = cd.ainst->pr_cfdata[pr_idx][FCUSTOMSCALEXVALUE];
			scaleY = cd.ainst->pr_cfdata[pr_idx][FCUSTOMSCALEYVALUE];
		}
		else
		{
			scaleX = scaleY = FX32_ONE;
		}
		fpcl = cd.ainst->pr_fdata[pr_idx][FX] + FX_Mul(FX32(frdata[ANIFRFROFFX]), scaleX);
		fpct = cd.ainst->pr_fdata[pr_idx][FY] + FX_Mul(FX32(frdata[ANIFRFROFFY]), scaleY);
#else
		ws = wd = (s32)cd.fr_data[grfr_id][RES_W] / (s32)colCount;
		hs = hd = (s32)cd.fr_data[grfr_id][RES_H] / (s32)rowCount;
		fpcl = cd.ainst->pr_fdata[pr_idx][FX] + FX32(frdata[ANIFRFROFFX]);
		fpct = cd.ainst->pr_fdata[pr_idx][FY] + FX32(frdata[ANIFRFROFFY]);
#endif
		ox = cd.fr_data[grfr_id][RES_X];
		oy = cd.fr_data[grfr_id][RES_Y];
		ct = cd.ainst->particlesCount;
		for (x = 0; x < colCount; x++)
			for (y = 0; y < rowCount; y++)
			{
				pcs = cd.ainst->pcs_fdata[ct];
#ifdef CUSTOMAFFINETRANSFORM_SUPPORT
				pcs[PCS_W] = (fx32)ws;
				pcs[PCS_H] = (fx32)hs;
				pcs[FX] = fpcl + FX_Mul((FX32(x * wd)), scaleX);
				pcs[FY] = fpct + FX_Mul((FX32(y * hd)), scaleY);
#else
				pcs[PCS_W] = (fx32)ws;
				pcs[PCS_H] = (fx32)hs;
				pcs[FX] = fpcl + FX32(x * wd);
				pcs[FY] = fpct + FX32(y * hd);
#endif
				pcs[PCS_RESID] = (fx32)cd.fr_data[grfr_id][RES_IMG_IDX];
				pcs[PCS_OFFX] = FX32(ox + x * ws);
				pcs[PCS_OFFY] = FX32(oy + y * hs);
				pcs[PCS_SPEED] = minSpeed + mthGetRandomFx(varSpeed);
				pcs[PCS_ADDITIONAL] = (fx32)(pcs[PCS_LIFE] = minLifeTime + mthGetRandom(varLifeTime));
				pcs[PCS_EXPLODE] = (fx32)(exploudTime / 2 + mthGetRandom(exploudTime / 2));
				pcs[PCS_TGTX0] = (fx32)MAX_INT;
				pcs[PCS_TGTY0] = (fx32)MAX_INT;
				if(targetX != NULL)
				{
					pcs[PCS_TGTX0] = *targetX;
				}
				if(targetY != NULL)
				{
					pcs[PCS_TGTY0] = *targetY;
                }
				pcs[PCS_TIMER0] = 0;
#ifdef CUSTOMAFFINETRANSFORM_SUPPORT
				pcs[PCS_ROTATIONORIGX] = FX32(frdata[ANIFRFROFFX]);
				pcs[PCS_ROTATIONORIGY] = FX32(frdata[ANIFRFROFFY]);
				SDK_NULL_ASSERT(cd.ainst->pr_cfdata);
				if ((cd.ainst->pr_cidata[pr_idx][ICUSTOMFLAGS] & ICUSTOMROTATIONFLAG) == ICUSTOMROTATIONFLAG)
				{
					pcs[PCS_SIN] = cd.ainst->pr_cfdata[pr_idx][FROTSIN];
					pcs[PCS_COS] = cd.ainst->pr_cfdata[pr_idx][FROTCOS];
				}
				else
				{
					pcs[PCS_SIN] = 0;
					pcs[PCS_COS] = FX32_ONE;
				}
				pcs[PCS_DW] = FX_Mul(FX32(wd), scaleX);
				pcs[PCS_DH] = FX_Mul(FX32(hd), scaleY);
#endif
				{
					fx32 fsin, fcos, r;
					a = (s32)mthGetRandom(360);
					r = FX32(DISPLAY_VIEW_W_MAX / 10); // trick: cd.ainst->sz_mapH > cd.ainst->sz_mapW ? cd.ainst->sz_mapH : cd.ainst->sz_mapW;
					fxCordic((a * FX_PI) / 180, &fsin, &fcos, FX_CORDIC_LOW_PRECISION);
					pcs[PCS_TGTX] = pcs[FX] + FX_Mul(r, fcos);
					pcs[PCS_TGTY] = pcs[FY] + FX_Mul(r, fsin);
				}
				pcs[PCS_DISTLENGHT] = _calcLenght(pcs[FX], pcs[FY], pcs[PCS_TGTX], pcs[PCS_TGTY]);
				pcs[PCS_ALPHA] = (fx32)frdata[ANIFRFRALPHA];
				ct++;
				if (++cd.ainst->particlesCount >= (s32)cd.initParams.particlesPoolSize)
				{
					cd.ainst->particlesCount = (s32)(cd.initParams.particlesPoolSize - 1);
					x = colCount;
					i = pc_count;
					break;
				}
			}
	}
}
//-------------------------------------------------------------------------------------------

void createExplode(fx32 x,
					fx32 y,
					u32 radius,
					u32 count,
					u32 time,
					s32 pr_idx,
                    s32 state)
{
	u32 i;
	s32 ct, a;
	fx32 *pcs, r;
	if(cd.initParams.particlesPoolSize == 0)
	{
		return;
	}
	ct = cd.ainst->particlesCount;
	for (i = 0; i < count; i++)
	{
		pcs = cd.ainst->pcs_fdata[ct];
		pcs[PCS_OFFX] = x;
		pcs[PCS_OFFY] = y;
		pcs[PCS_W] = FX32(radius);
		pcs[PCS_H] = -FX32_ONE;
		{
			fx32 fsin, fcos;
			a = (s32)mthGetRandom(360);
			r = mthGetRandomFx(pcs[PCS_W]);
			fxCordic((a * FX_PI) / 180, &fsin, &fcos, FX_CORDIC_LOW_PRECISION);
			pcs[FX] = x + FX_Mul(r, fsin);
			pcs[FY] = y + FX_Mul(r, fcos);
		}
		pcs[PCS_RESID] = -FX32(2); // id Explode
		pcs[PCS_SPEED] = (fx32)state;
		pcs[PCS_TGTX0] = (fx32)pr_idx;
		pcs[PCS_TGTY0] = (fx32)cd.states[state][stIDANIMATION];
		pcs[PCS_TIMER0] = (fx32)(i * 2);
#ifdef CUSTOMAFFINETRANSFORM_SUPPORT
		pcs[PCS_ROTATIONORIGX] = 0;
		pcs[PCS_ROTATIONORIGY] = 0;
#endif
		pcs[PCS_LIFE] = (fx32)((s32)time);
		pcs[PCS_EXPLODE] = 0;
		pcs[PCS_ALPHA] = (fx32)(15 + mthGetRandom(16));
		ct++;
		if (++cd.ainst->particlesCount >= (s32)cd.initParams.particlesPoolSize)
		{
			cd.ainst->particlesCount = (s32)(cd.initParams.particlesPoolSize - 1);
			break;
		}
	}
}
//-------------------------------------------------------------------------------------------

void createSimpleParticles(u32 size,
							GXRgba color,
							fx32 xpos,
							fx32 ypos,
							u32 radius,
							u32 count,
							fx32 minSpeed,
							fx32 varSpeed,
							u32 minLifeTime,
							u32 varLifeTime,
							u32 exploudTime,
							fx32 *targetX,
							fx32 *targetY)
{
	u32 y;
	s32 ct, a;
	fx32 *pcs, r;
	fx32 fsin, fcos;
	if(cd.initParams.particlesPoolSize == 0)
	{
		return;
	}
	ct = cd.ainst->particlesCount;
	for (y = 0; y < count; y++)
	{
		pcs = cd.ainst->pcs_fdata[ct];
		pcs[PCS_W] = FX32(size);
		pcs[PCS_RESID] = -FX32_ONE;
		pcs[PCS_OFFX] = (fx32)color;
		a = (s32)mthGetRandom(360);
		r = mthGetRandomFx(FX32(radius));
		fxCordic((a * FX_PI) / 180, &fsin, &fcos, FX_CORDIC_LOW_PRECISION);	
		pcs[FX] = xpos + FX_Mul(r, fcos) - FX32(size / 2);
		pcs[FY] = ypos + FX_Mul(r, fsin) - FX32(size / 2);
		pcs[PCS_SPEED] = minSpeed + mthGetRandomFx(varSpeed);
		pcs[PCS_LIFE] = (fx32)(minLifeTime + mthGetRandom(varLifeTime));
		pcs[PCS_EXPLODE] = (fx32)(exploudTime / 2 + mthGetRandom(exploudTime / 2));
		pcs[PCS_TGTX0] = (fx32)MAX_INT;
		pcs[PCS_TGTY0] = (fx32)MAX_INT;
		if(targetX != NULL)
		{
			pcs[PCS_TGTX0] = *targetX;
		}
		if(targetY != NULL)
		{
			pcs[PCS_TGTY0] = *targetY;
		}
		pcs[PCS_TIMER0] = FX32_ONE;
		r = FX32(DISPLAY_VIEW_W_MAX / 10); // trick: cd.ainst->sz_mapH > cd.ainst->sz_mapW ? cd.ainst->sz_mapH : cd.ainst->sz_mapW;
		pcs[PCS_TGTX] = xpos + FX_Mul(r, fcos);
		pcs[PCS_TGTY] = ypos + FX_Mul(r, fsin);
		pcs[PCS_DISTLENGHT] = _calcLenght(pcs[FX], pcs[FY], pcs[PCS_TGTX], pcs[PCS_TGTY]);
		ct++;
		if (++cd.ainst->particlesCount >= (s32)cd.initParams.particlesPoolSize)
		{
			cd.ainst->particlesCount = (s32)(cd.initParams.particlesPoolSize - 1);
			break;
		}
	}
}
//-------------------------------------------------------------------------------------------

void resetParticles()
{
	cd.ainst->particlesCount = 0;
}
//-------------------------------------------------------------------------------------------

s32 getParticlesCount()
{
	return cd.ainst->particlesCount;
}
//-------------------------------------------------------------------------------------------

void _drawParticles(s32 ms)
{
	fx32 *pcs;
	s32 i;
	for (i = 0; i < cd.ainst->particlesCount; i++)
	{
		s32 res_id;
		pcs = cd.ainst->pcs_fdata[i];
		res_id = fx2int(pcs[PCS_RESID]);
		switch(res_id)
		{
			case -2:
			{
				if(ms > 0)
				{
					pcs[PCS_TIMER0] = (fx32)((s32)pcs[PCS_TIMER0] - 1);
				}
				if (pcs[PCS_TIMER0] <= 0)
				{
					const s16 *frdata;
					s32 j, pc_count, fr_count, fr_idx;
					u16 grfr_id;
					pcs[PCS_TIMER0] = 0;
					if(ms > 0)
					{
						pcs[PCS_H] += FX_Mul(PARTICLES_EXPLODE_FRAME_TIME_MS_FX, cd.deltatime_scale);
					}
					fr_idx = fx2int(pcs[PCS_H]);
					fr_count = cd.ppr_anim_data[cd.ainst->pr_idata[(s32)pcs[PCS_TGTX0]][IPARENT_IDX]]
											[(s32)pcs[PCS_TGTY0]]
											[ANIFRCOUNT];
					if(fr_count <= fr_idx)
					{
						s32 a;
						fx32 fsin, fcos, r;  
						pcs[PCS_H] = 0;
						fr_idx = 0;
						a = (s32)mthGetRandom(360);
						r = mthGetRandomFx(pcs[PCS_W]);
						fxCordic((a * FX_PI) / 180, &fsin, &fcos, FX_CORDIC_LOW_PRECISION);	
						pcs[FX] = pcs[PCS_OFFX] + FX_Mul(r, fcos);
						pcs[FY] = pcs[PCS_OFFY] + FX_Mul(r, fsin);
						pcs[PCS_ALPHA] = (fx32)(15 + (s32)mthGetRandom(16));
					}
					if(fr_count > 0 && fr_idx >= 0 && cd.ainst->visible_mask != 0)
					{
						SDK_NULL_ASSERT(cd.ppr_fr_data);
						pc_count = fx2int(cd.ppr_fr_data[cd.ainst->pr_idata[(s32)pcs[PCS_TGTX0]][IPARENT_IDX]]
											[(s32)pcs[PCS_TGTY0]]
											[fr_idx]
											[ANIFRFRCOUNT]);
						for(j = 0; j < pc_count; j++)
						{
							frdata = _getFramePieceData(cd.ainst->pr_idata[(s32)pcs[PCS_TGTX0]][IPARENT_IDX],
															(s32)pcs[PCS_TGTY0],
															fr_idx, j);
							SDK_NULL_ASSERT(cd.fr_data);
							grfr_id = (u16)frdata[ANIFRFRID];
							SDK_ASSERT(grfr_id < NULL_IMG_IDX);
							if (cd.fr_data[grfr_id][RES_IMG_IDX] < NULL_IMG_IDX)
							{
								struct DrawImageFunctionData fd;
								MI_CpuClear8(&fd, sizeof(struct DrawImageFunctionData));
								fd.mType = DIT_Obj;
								fd.mX = pcs[FX] - cd.ainst->worldx_fx + FX32(frdata[ANIFRFROFFX] - (cd.fr_data[grfr_id][RES_W] >> 1));
								fd.mY = pcs[FY] - cd.ainst->worldy_fx + FX32(frdata[ANIFRFROFFY] - (cd.fr_data[grfr_id][RES_H] >> 1));
								fd.mFillColor = (((s32)pcs[PCS_ALPHA]) << 16) | DIFD_DEF_FILL_COLOR;
#ifdef CUSTOMAFFINETRANSFORM_SUPPORT
								fd.mSin = 0;
								fd.mCos = FX32_ONE;
								fd.mDestWidth = FX32(frdata[ANIFRFRWIDTH]);
								fd.mDestHeight = FX32(frdata[ANIFRFRHEIGHT]);
#endif
								fd.mpSrcData = cd.res_img[cd.fr_data[grfr_id][RES_IMG_IDX]];
								_draw(cd.fr_data[grfr_id], &fd);
							}
						}
					}
				}
			}
			break;

			case -1:
				if (fx2int(pcs[PCS_TIMER0]) == 0)
				{
					setColor((GXRgba)pcs[PCS_OFFX]);
					fillRect(pcs[FX], pcs[FY], pcs[PCS_W], pcs[PCS_W]);
				}
				if(ms > 0)
				{
					pcs[PCS_TIMER0] = 0;
				}
			break;

			default:
				if(cd.ainst->visible_mask != 0)
				{
					struct DrawImageFunctionData fd;
					u8 a = (u8)(((s32)pcs[PCS_ALPHA] * ((s32)pcs[PCS_LIFE] + ((s32)(pcs[PCS_ADDITIONAL] - pcs[PCS_LIFE]) >> 4))) / (s32)pcs[PCS_ADDITIONAL]);
					if(a > 31)
					{
						a = 0;
					}
					fd.mType = DIT_Obj;
					fd.text = FALSE;
					fd.mpClipRect = NULL;
					fd.mX = pcs[FX] - cd.ainst->worldx_fx;
					fd.mY = pcs[FY] - cd.ainst->worldy_fx;
					fd.mpSrcData = cd.res_img[res_id];
					fd.mFillColor = (a << 16) | DIFD_DEF_FILL_COLOR;
#ifdef CUSTOMAFFINETRANSFORM_SUPPORT
					fd.mSin = pcs[PCS_SIN];
					fd.mCos = pcs[PCS_COS];
					fd.mAffineOriginX = pcs[PCS_ROTATIONORIGX];
					fd.mAffineOriginY = pcs[PCS_ROTATIONORIGY];
					fd.mDestWidth = pcs[PCS_DW];
					fd.mDestHeight = pcs[PCS_DH];
#endif
					fd.mSrcSizeData[SRC_DATA_X] = (u16)pcs[PCS_OFFX];
					fd.mSrcSizeData[SRC_DATA_Y] = (u16)pcs[PCS_OFFY];
					fd.mSrcSizeData[SRC_DATA_W] = (u16)pcs[PCS_W];
					fd.mSrcSizeData[SRC_DATA_H] = (u16)pcs[PCS_H];
					RENDERFN(DrawImage)(&fd);
				}
		}

		pcs[PCS_LIFE] = (fx32)((s32)pcs[PCS_LIFE] - ms);
		if (pcs[PCS_LIFE] <= 0)
		{
			fx32 tpcs[PCS_COUNT];
			pcs[PCS_LIFE] = (fx32)-1;
			pcs[PCS_EXPLODE] = (fx32)-1;
			cd.ainst->particlesCount--;
			MI_CpuCopy8(cd.ainst->pcs_fdata[i], tpcs, PCS_COUNT * sizeof(fx32));
			MI_CpuCopy8(cd.ainst->pcs_fdata[cd.ainst->particlesCount], cd.ainst->pcs_fdata[i], PCS_COUNT * sizeof(fx32));
			MI_CpuCopy8(tpcs, cd.ainst->pcs_fdata[cd.ainst->particlesCount], PCS_COUNT * sizeof(fx32));
			i--;
		}
		else if(ms > 0)
		{
			pcs[PCS_EXPLODE] = (fx32)((s32)pcs[PCS_EXPLODE] - ms);
			if(res_id != -2)
			{
				if (pcs[PCS_EXPLODE] <= 0)
				{
					pcs[PCS_EXPLODE] = (fx32)-1;
					if (pcs[PCS_TGTX0] != (fx32)MAX_INT)
					{
						pcs[PCS_TGTX] = pcs[PCS_TGTX0];
					}
					if (pcs[PCS_TGTY0] != (fx32)MAX_INT)
					{
						pcs[PCS_TGTY] = pcs[PCS_TGTY0];
					}
					pcs[PCS_DISTLENGHT] = _calcLenght(pcs[FX], pcs[FY], pcs[PCS_TGTX], pcs[PCS_TGTY]);
				}
				_nextPointAtLine(pcs[FX], pcs[FY], pcs[PCS_TGTX], pcs[PCS_TGTY], &pcs[PCS_DISTLENGHT], 
									FX_Mul(pcs[PCS_SPEED], cd.deltatime_scale), pcs);
			}
		}
	}
}
//-------------------------------------------------------------------------------------------

void _updateParticlesWithoutDraw(s32 ms)
{
	fx32 *pcs;
	s32 i;
	for (i = 0; i < cd.ainst->particlesCount; i++)
	{
		s32 res_id;
		pcs = cd.ainst->pcs_fdata[i];
		res_id = fx2int(pcs[PCS_RESID]);
		switch(res_id)
		{
			case -2:
			{
				pcs[PCS_TIMER0] = (fx32)((s32)pcs[PCS_TIMER0] - 1);
				if (pcs[PCS_TIMER0] <= 0)
				{
					s32 fr_count;
					s32 fr_idx;
					pcs[PCS_TIMER0] = 0;
					pcs[PCS_H] += FX_Mul(PARTICLES_EXPLODE_FRAME_TIME_MS_FX, cd.deltatime_scale);
					fr_idx = fx2int(pcs[PCS_H]);
					fr_count = cd.ppr_anim_data[cd.ainst->pr_idata[(s32)pcs[PCS_TGTX0]][IPARENT_IDX]]
											[(s32)pcs[PCS_TGTY0]]
											[ANIFRCOUNT];
					if(fr_count <= fr_idx)
					{
						s32 a;
						fx32 fsin, fcos, r;  
						pcs[PCS_H] = 0;
						a = (s32)mthGetRandom(360);
						r = mthGetRandomFx(pcs[PCS_W]);
						fxCordic((a * FX_PI) / 180, &fsin, &fcos, FX_CORDIC_LOW_PRECISION);	
						pcs[FX] = pcs[PCS_OFFX] + FX_Mul(r, fcos);
						pcs[FY] = pcs[PCS_OFFY] + FX_Mul(r, fsin);
						pcs[PCS_ALPHA] = (fx32)(15 + (s32)mthGetRandom(16));
					}
				}
			}
			break;

			case -1:
			pcs[PCS_TIMER0] = 0;
			break;

			default:
			break;
		}


		pcs[PCS_LIFE] = (fx32)((s32)pcs[PCS_LIFE] - ms);
		if (pcs[PCS_LIFE] <= 0)
		{
			fx32 tpcs[PCS_COUNT];
			pcs[PCS_LIFE] = (fx32)-1;
			pcs[PCS_EXPLODE] = (fx32)-1;
			cd.ainst->particlesCount--;
			MI_CpuCopy8(cd.ainst->pcs_fdata[i], tpcs, PCS_COUNT * sizeof(fx32));
			MI_CpuCopy8(cd.ainst->pcs_fdata[cd.ainst->particlesCount], cd.ainst->pcs_fdata[i], PCS_COUNT * sizeof(fx32));
			MI_CpuCopy8(tpcs, cd.ainst->pcs_fdata[cd.ainst->particlesCount], PCS_COUNT * sizeof(fx32));
			i--;
		}
		else
		{
			pcs[PCS_EXPLODE] = (fx32)((s32)pcs[PCS_EXPLODE] - ms);
			if(res_id != -2)
			{
				if (pcs[PCS_EXPLODE] <= 0)
				{
					pcs[PCS_EXPLODE] = (fx32)-1;
					if (pcs[PCS_TGTX0] != (fx32)MAX_INT)
					{
						pcs[PCS_TGTX] = pcs[PCS_TGTX0];
					}
					if (pcs[PCS_TGTY0] != (fx32)MAX_INT)
					{
						pcs[PCS_TGTY] = pcs[PCS_TGTY0];
					}
					pcs[PCS_DISTLENGHT] = _calcLenght(pcs[FX], pcs[FY], pcs[PCS_TGTX], pcs[PCS_TGTY]);
				}
				_nextPointAtLine(pcs[FX], pcs[FY], pcs[PCS_TGTX], pcs[PCS_TGTY], &pcs[PCS_DISTLENGHT], 
									FX_Mul(pcs[PCS_SPEED], cd.deltatime_scale), pcs);
			}
		}
	}
}
//-------------------------------------------------------------------------------------------

u16 rlGetCount(u32 layer)
{
	(void)layer;
	return 1;//(u16)_getInstance(layer)->all_pr_count;
}
//-------------------------------------------------------------------------------------------

BOOL rlIsAvailable(void)
{
	return cd.rl_available;
}
//-------------------------------------------------------------------------------------------

BOOL rlMove(u32 layer, u32 zone, u16 index_from, u16 index_to)
{
	struct TEngineInstance *ei = _getInstance(layer);
	if(cd.rl_available == TRUE && 
		zone < (u32)ei->zones_ct && index_from < (u16)ei->all_pr_count && index_to < (u16)ei->all_pr_count)
	{
		if(index_from == index_to)
		{
			return TRUE;
		}
		/*
		u16 idx_f = (u16)ei->pr_znlist[ei->zones_ct][index_from];
		if(0xffff != idx_f)
		{
			u16 idx_t = (u16)ei->pr_znlist[ei->zones_ct][index_to];
			if(0xffff != idx_t)
			{
				u16 next_f, next_t;
				next_f = (u16)ei->pr_znlist[ei->zones_ct][index_from] >> 16;
				next_t = (u16)ei->pr_znlist[ei->zones_ct][index_to] >> 16;
			}
		}
		*/
		return FALSE;
	}
	return FALSE;
}
//-------------------------------------------------------------------------------------------

BOOL rlGetGameObjectIndex(s32 pr_id, u32 layer, u32 zone, u16 *const orenderlist_index, u16 *const oindex)
{
	struct TEngineInstance *ei = _getInstance(layer);
	if(zone < (u32)ei->zones_ct && ei->rndr_znlist[zone])
	{
		u16 render_idx = 0;
		u16 rid = 0;
		const struct AllocatorList* rlist;
		const struct ListItem* it;
		rlist = ei->rndr_znlist[zone];
		it = AllocatorList_Begin_Const(rlist);
		while (it != AllocatorList_End_Const(rlist))
		{
			u16 idx = *(u16*)AllocatorList_Val(rlist, it);
			if (idx == (u16)pr_id)
			{
				if (orenderlist_index != NULL)
				{
					*orenderlist_index = rid;
				}
				if (oindex != NULL)
				{
					*oindex = render_idx;
				}
				return TRUE;
			}
			it = AllocatorList_Next_Const(rlist, it);
			render_idx++;
		}
	}
	return FALSE;
}
//-------------------------------------------------------------------------------------------

BOOL rlMoveGameObject(u32 layer, u32 zone, u16 renderlist_index, u16 index_from, u16 index_to)
{
	struct TEngineInstance *ei = _getInstance(layer);
	if(cd.rl_available == TRUE && zone < (u32)ei->zones_ct && renderlist_index < rlGetCount(layer) && ei->rndr_znlist[zone])
	{
		u16 render_idx;
		struct AllocatorList *rlist;
		struct ListItem *it, *it_f, *it_to;
		if(index_from == index_to)
		{
			return FALSE;
		}
		rlist = ei->rndr_znlist[zone];
		if (index_from < AllocatorList_Size(rlist))
		{
			it = AllocatorList_Begin(rlist);
			it_f = it_to = NULL;
			render_idx = 0;
			while (it != AllocatorList_End(rlist))
			{
				if (index_from == render_idx)
				{
					it_f = it;
					if (it_f != NULL && it_to != NULL)
					{
						AllocatorList_Move(rlist, it_f, it_to);
						return TRUE;
					}
					if (it_f != NULL && index_to >= AllocatorList_Size(rlist))
					{
						break;
					}
				}
				else if (index_to == render_idx)
				{
					it_to = it;
					if (it_f != NULL && it_to != NULL)
					{
						AllocatorList_Move(rlist, it_f, it_to);
						return TRUE;
					}
				}
				it = AllocatorList_Next(rlist, it);
				render_idx++;
			}
			if (it_f != NULL && it_to == NULL)
			{
				AllocatorList_Move(rlist, it_f, AllocatorList_Tail(rlist));
				return TRUE;
			}
		}
	}
	return FALSE;
}
//-------------------------------------------------------------------------------------------

u16 rlGetGameObjectsCount(u32 layer, u32 zone, u16 renderlist_index)
{
	struct TEngineInstance *ei = _getInstance(layer);
	u16 ct = 0;
	if(zone < (u32)ei->zones_ct && renderlist_index < rlGetCount(layer) && ei->rndr_znlist[zone])
	{
		return (u16)AllocatorList_Size(ei->rndr_znlist[zone]);
	}
	return ct;
}
//-------------------------------------------------------------------------------------------

s32 getFPS()
{
	return cd.last_fps_tick_ct;
}
//-------------------------------------------------------------------------------------------

void setPauseEngine(BOOL pause)
{
	cd.pause_engine = pause & 0x1;
}
//-------------------------------------------------------------------------------------------

s32 cldShapesCount(u32 layer)
{
	struct TEngineInstance *ei = _getInstance(layer);
	return ei->cld_shape_ct;
}
//-------------------------------------------------------------------------------------------

s32 cldShapeNodesCount(u32 layer, s32 cldShapeIdx)
{
	struct TEngineInstance *ei = _getInstance(layer);
	SDK_ASSERT(cldShapeIdx >= 0 && cldShapeIdx < ei->cld_shape_ct);
	SDK_NULL_ASSERT(ei->cld_sh_nodes_ct);
	return ei->cld_sh_nodes_ct[cldShapeIdx];
}
//-------------------------------------------------------------------------------------------

const struct fxVec2* cldShapeGetNodesArray(u32 layer, s32 cldShapeIdx)
{
	struct TEngineInstance *ei = _getInstance(layer);
	SDK_ASSERT(cldShapeIdx >= 0 && cldShapeIdx < ei->cld_shape_ct);
	SDK_NULL_ASSERT(ei->cld_sh_node);
	return (const struct fxVec2*)ei->cld_sh_node[cldShapeIdx];
}
//-------------------------------------------------------------------------------------------
