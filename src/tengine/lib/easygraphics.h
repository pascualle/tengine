#ifndef _EASYGRAPHICS_H_
#define _EASYGRAPHICS_H_

#include "system.h"

#ifdef USE_CUSTOM_RENDER

#ifdef __cplusplus
extern "C" {
#endif

// legacy
// #define USE_TILEBUFFER

#ifdef KOLIBRIOS_APP
#ifdef USE_TILEBUFFER
#undef USE_TILEBUFFER
#endif
#endif

#define RENDERFN(name) egRender_##name

#define TRANSPARENT_COLOR_DC 0
#define TRANSPARENT_COLOR_256 0

BOOL egRender_IsGraphicsInit(void);

void egRender_PlaneInit(const struct RenderPlaneInitParams* ipBGParams);
BOOL egRender_IsRenderPlaneInit(enum BGSelect iType);
void egRender_PlaneResize(enum BGSelect iType, const struct RenderPlaneSizeParams* ipBGParams);
void egRender_PlaneRelease(enum BGSelect iType);

//void JoinScreenMode(struct PlaneInitBGParams* ipParamsScr1, struct PlaneInitBGParams* ipParamsScr2);
BOOL egRender_IsJoinScreenMode(void);

void egRender_SetActiveBGForGraphics(enum BGSelect iActiveBG);
enum BGSelect egRender_GetActiveBGForGraphics(void);

s32 egRender_GetViewWidth(enum BGSelect iBGType);
s32 egRender_GetViewHeight(enum BGSelect iBGType);
s32 egRender_GetViewLeft(enum BGSelect iBGType);
s32 egRender_GetViewTop(enum BGSelect iBGType);

s32 egRender_GetFrameBufferWidth(enum BGSelect iBGType);
s32 egRender_GetFrameBufferHeight(enum BGSelect iBGType);

BOOL egRender_IsTransferDataVRAMChanged(void);
void egRender_TransferDataToVRAM(void);

void egRender_IncludeToVRAMTransfering(enum BGSelect iBG);
void egRender_ExcludeFromVRAMTransfering(enum BGSelect iBG);

void egRender_SaveGraphicsToMemory(void);
void egRender_RestoreGraphicsFromMemory(fx32 IOffX, fx32 IOffY);

void egRender_SetColor(GXRgba iColor);

void egRender_DrawLine(fx32 iX0, fx32 iY0, fx32 iX1, fx32 iY1);

void egRender_ColorRect(fx32 iX, fx32 iY, fx32 iWidth, fx32 iHeight);

void egRender_DrawImage(const struct DrawImageFunctionData* data);

void egRender_DrawImageToImage(const struct BMPImage *pSrcImage, 
                            s32 iSrcX, s32 iSrcY,
                            s32 iWidth, s32 iHeight,
                            struct BMPImage *pDestImage, s32 iDestX, s32 iDestY);

void egRender_ClearFrameBuffer(enum BGSelect iBG);

void egRender_SetRenderPlaneScale(fx32 val, enum BGSelect iBG);
fx32 egRender_GetRenderPlaneScale(enum BGSelect iBG);

#ifndef NITRO_SDK
typedef void (*TransferFrameBufferCallback)(const struct BMPImage* pFrameBuffer, s32 offX, s32 offY, s32 frameWidth, s32 frameHeight);
void egRender_Init(TransferFrameBufferCallback fn);
#else
void egRender_Init(void);
#endif
void egRender_Release(void);
                       
void egRender_FreezeCurrentFrameBuffer(BOOL val);
void egRender_LostDevice(void);
void egRender_RestoreDevice(void);

#ifdef __cplusplus
} /* extern "C" */
#endif

#endif

#endif /*_EASYGRAPHICS_DC_H_*/
