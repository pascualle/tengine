/*
tengine - cross-platform game engine
------------------------------------

Copyright (c) 2013 Papa Pascualle
Contact Email: pascualle@gmail.com

Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
*/

#include "tengine_low.h"
#include "loadtdata.h"
#include "jobs_low.h"
#include "texts_low.h"
#include "sound_low.h"
#include "render.h"
#include "filesystem_low.h"
#include "loadhelpers.h"
#include "texts.h"
#include "jobs.h"
#include "fxmath.h"
#include "static_allocator.h"
#include "containers/allocator_list.h"
#include "containers/allocator_htable.h"

// ----------------------------------------------------------------------------------

static const char FILENAME_O = 'o'; 
static const char FILENAME_L = 'l'; 
static const char FILENAME_T = 't';

#define CS_MAGIC_NUMBER 23456
#define SIZEOF_PTR 8

#if defined NITRO_SDK
#define FX32_TYPE 2
#else
  #if defined USE_FX32_AS_FLOAT
    #define FX32_TYPE 0
  #else
    #if defined USE_FX32_AS_FIXED  
      #define FX32_TYPE 1
    #endif
  #endif
#endif

extern struct TEngineCommonData cd;
static struct JOBFileStreamTask ld_fst;
static char _gsfileName[MAX_FILENAME];

static void _loadFrames(struct TEngineCommonData *cd, s32 anm_ct, s32 i, s32 j, const u8 **inputstream);
static void _releaseFont(struct TERFont *oFont);
static void _getFileName(s32 idx, s32 type, char *iofileName, struct TEngineCommonData *cd);
static void _loadResource(s32 idx, s32 resIdx, s32 type, u8 value, void **oData, struct TEngineCommonData *cd);
static void _initFont(const u8* iData, struct TERFont *oFont, struct TEngineCommonData *cd);
static void _createZoneArrays(struct TEngineInstance * i);
static void _releaseZoneArrays(struct TEngineInstance * i);

static void _addToLoadList(u8 type, u32 p1, u16 p2);
static void _readCommonData(struct TEngineCommonData *cd, u8 *filedata);
static void _releaseCommonData(struct TEngineCommonData *cd);
static void _readMap(u32 layer, u8 *level, struct TEngineCommonData *cd);
static void _readTextData(u8* language, struct TEngineCommonData *cd);

static void _asynhloadFileInit(const char* fname, enum JFSDMode mode, u16 resIdx, u8 value);
static BOOL _asynhloadFileCheck(u8** data);
static void _initFile_ALS_STATE_LOAD_DATA(s32 l);

static void _readTriggers(struct TEngineInstance *i, const u8 **inputstream, BOOL common_mode, struct TEngineCommonData *cd);
static void _readScripts(struct TEngineInstance *i, const u8 **inputstream, BOOL common_mode, struct TEngineCommonData *cd);

static BOOL _assignRes_begin(struct TEngineCommonData *cd);
static BOOL _assignRes_load(struct TEngineCommonData *cd);
static void _assignRes_end(void);
static void _assignResUpdate(struct TEngineCommonData *cd);

// ----------------------------------------------------------------------------------

enum
{
	ALD_CD = 1,
	ALD_MD,
	ALD_LD
};
// ----------------------------------------------------------------------------------

void _loadDataInit()
{
	MI_CpuFill8(&ld_fst, 0, sizeof(struct JOBFileStreamTask));
	_gsfileName[0] = 0;
}
// ----------------------------------------------------------------------------------

void beginLoadListAsynh(s32 cb_parameter)
{
	if(cd.ld_st != ALS_STATE_READY || cd.agrl_s != 0)
	{
		// call this function only after  EVENT_TYPE_ON_END_ASYNH_LOAD_DATA event
		OS_Warning("beginLoadListAsynh: Trying to call a function during async loading process\n");
		SDK_ASSERT(0);
		return;
	}
	{
		u32 j;
		for (j = 0; j < cd.initParams.layersCount; j++)
		{
			struct TEngineInstance *ei = _getInstance(j);
			if(ei->bgType == BGSELECT_NUM)
			{
				OS_Warning("ASSERT: Please use assignLayerWithRenderPlane function before readMap (assign layer to render plane)\n");
				SDK_ASSERT(0);
				return;
			}
		}
	}
	cd.ld_param = cb_parameter; 
	cd.ld_st = ALS_STATE_LIST;
	cd.ld_ct_t = cd.ld_ct = 0;
}
// ----------------------------------------------------------------------------------

static void _addToLoadList(u8 type, u32 p1, u16 p2)
{
	if(cd.ld_st == ALS_STATE_LIST && (s16)(cd.initParams.layersCount + 2) > cd.ld_ct)
	{
		s32 i;
		for(i = 0; i < cd.ld_ct; i++)
		{
			if(cd.ld_task[i] == type && cd.ld_p1[i] == p1 && cd.ld_p2[i] == p2)
			{
				return;
			}
		}
		cd.ld_task[cd.ld_ct] = type;
		cd.ld_p1[cd.ld_ct] = p1;
		cd.ld_p2[cd.ld_ct] = p2;
		cd.ld_ct++;
		cd.ld_ct_t = cd.ld_ct; 
	}
}
// ----------------------------------------------------------------------------------

void addToLoadListCommonData(void)
{
	_addToLoadList(ALD_CD, 0, 0);
}
// ----------------------------------------------------------------------------------

void addToLoadListMap(u32 layer, u16 level)
{
	_addToLoadList(ALD_MD, layer, level);
}
// ----------------------------------------------------------------------------------

void addToLoadListLanguageData(u32 language_id)
{
	_addToLoadList(ALD_LD, language_id, 0);
}
// ----------------------------------------------------------------------------------

void endLoadListAsynh(BOOL loadGraphicResources)
{
	if(cd.ld_ct > 0 && cd.ld_st == ALS_STATE_LIST)
	{
		s32 i;
		for(i = 0; i < cd.ld_ct / 2; i++)
		{
			s16 t8;
			u16 t16;
			u32 t32;
			t8 = cd.ld_task[i]; 
			cd.ld_task[i] = cd.ld_task[cd.ld_ct - i - 1];
			cd.ld_task[cd.ld_ct - i - 1] = t8;
			t32 = cd.ld_p1[i];
			cd.ld_p1[i] = cd.ld_p1[cd.ld_ct - i - 1];
			cd.ld_p1[cd.ld_ct - i - 1] = t32;
			t16 = cd.ld_p2[i];
			cd.ld_p2[i] = cd.ld_p2[cd.ld_ct - i - 1];
			cd.ld_p2[cd.ld_ct - i - 1] = t16;
		}
		cd.ld_st = ALS_STATE_LOAD_DATA;
		jobSetActive(TRUE);
	}
	else
	{
		cd.ld_st = ALS_STATE_READY;
	}
	cd.ag_st = loadGraphicResources;
}
// ----------------------------------------------------------------------------------

void _asynhloadFileInit(const char* fname, enum JFSDMode mode, u16 resIdx, u8 value)
{
#ifdef JOBS_IN_SEPARATE_THREAD
	jobCriticalSectionBegin();
#endif
	ld_fst.mData.mpFilename = fname;
	ld_fst.mData.mpFile = NULL;
	ld_fst.mData.mpImage = NULL;	
	ld_fst.mData.mStatus = FALSE;
	ld_fst.mData.mMode = (u8)mode;	
	ld_fst.mData.mValue = value; 
	ld_fst.mData.mIdx = resIdx;
	jobAddStreamFileTask(&ld_fst);
#ifdef JOBS_IN_SEPARATE_THREAD
	jobCriticalSectionEnd();
#endif
}
// ----------------------------------------------------------------------------------

BOOL _asynhloadFileCheck(u8** data)
{
	if(ld_fst.mData.mStatus == TRUE)
	{
#ifdef JOBS_IN_SEPARATE_THREAD
		jobCriticalSectionBegin();
#endif
		jobRemoveStreamFileTask(&ld_fst);
		ld_fst.mData.mMode = JFSDM_MODE_NONE;
		if(data)
		{
			*data = ld_fst.mData.mpFile != NULL ? ld_fst.mData.mpFile : (u8*)ld_fst.mData.mpImage;
		}
		ld_fst.mData.mpFile = NULL;
		ld_fst.mData.mpImage = NULL;
		ld_fst.mData.mStatus = FALSE;
#ifdef JOBS_IN_SEPARATE_THREAD
		jobCriticalSectionEnd();
#endif
		return TRUE;
	}
	return FALSE;
}
// ----------------------------------------------------------------------------------

void _initFile_ALS_STATE_LOAD_DATA(s32 l)
{
	switch(cd.ld_task[l])
	{
		case ALD_CD:
			_gsfileName[0] = FILENAME_O;
			_gsfileName[1] = 0;
			_asynhloadFileInit(_gsfileName, JFSDM_MODE_FILE, 0, 0);
		break;
		case ALD_MD:
		{
			_gsfileName[0] = FILENAME_L;
			_gsfileName[1] = 0;
			STD_NumToString(cd.ld_p2[l], &_gsfileName[1], 6);
			_asynhloadFileInit(_gsfileName, JFSDM_MODE_FILE, 0, 0);
		}
		break;
		case ALD_LD:
		{
			_gsfileName[0] = FILENAME_T;
			_gsfileName[1] = 0;
			STD_NumToString((u16)cd.ld_p1[l], &_gsfileName[1], 6);
			_asynhloadFileInit(_gsfileName, JFSDM_MODE_FILE, 0, 0);
		}
	}
}
// ----------------------------------------------------------------------------------

void _updateLoadingProcess()
{
	if(cd.ld_st == ALS_STATE_LOAD_DATA)
	{
		u8* buf;
		if(ld_fst.mData.mMode == JFSDM_MODE_NONE)
		{
			_initFile_ALS_STATE_LOAD_DATA(cd.ld_ct - 1);
		}
		if(_asynhloadFileCheck(&buf))
		{
			switch(cd.ld_task[cd.ld_ct - 1])
			{
				case ALD_CD:
					_readCommonData(&cd, buf);
				break;
				case ALD_MD:
					_readMap(cd.ld_p1[cd.ld_ct - 1], buf, &cd);
				break;
				case ALD_LD:
					_readTextData(buf, &cd);
				break;
				default:
					SDK_ASSERT(0);
			}
			cd.ld_ct--;
		}
		if(cd.ld_ct == 0)
		{
			cd.ld_st = ALS_STATE_READY;
			if(cd._onEventCB != NULL)
			{
				struct EventCallbackData ed;
				ed.eventType =  EVENT_TYPE_ON_END_ASYNH_LOAD_DATA;
				ed.initiatorId = cd.ld_param;
				ed.eventData.mpData = NULL;
				while(--cd.ld_ct_t > -1)
				{
					switch(cd.ld_task[cd.ld_ct_t])
					{
						case ALD_CD:
							ed.layer = -1; 
							ed.eventId = ed.ownerId = LOAD_TYPE_COMMONDATA;
							cd._onEventCB(&ed);
							break;
						case ALD_MD:
							SDK_NULL_ASSERT(cd.instances[cd.ld_p1[cd.ld_ct_t]]->data_file);
							_setActiveLayer(cd.ld_p1[cd.ld_ct_t]); 
							ed.layer = (s32)cd.ld_p1[cd.ld_ct_t];
							ed.eventId = ed.ownerId = LOAD_TYPE_MAPDATA;
							cd.rl_available = TRUE;
							cd._onEventCB(&ed);
							cd.rl_available = FALSE;
							break;
						case ALD_LD:
							ed.layer = -1; 
							ed.eventId = ed.ownerId = LOAD_TYPE_TEXTDATA;
							cd._onEventCB(&ed);
							break;
						default:
							SDK_ASSERT(0);
					}
				}
				ed.layer = -1; 
				ed.eventId = ed.ownerId = LOAD_TYPE_ENDLOADDATATASK;
				cd._onEventCB(&ed);
			}
			cd.ld_param = 0;
		}
	}
	else
	{
		if(cd.ag_st)
		{
			_assignResUpdate(&cd);	
		}
	}
}
// ----------------------------------------------------------------------------------

static void _readCommonData(struct TEngineCommonData *pcd, u8* filedata)
{
	const u8 *inputstream;
	const u8 *inputstream_temp;
	s32 i, j;
	u32 k;

	SDK_ASSERT(pcd->common_data_file == NULL); // only one common data can be loaded

	inputstream = pcd->common_data_file = filedata;
	SDK_NULL_ASSERT(inputstream);
	if (inputstream != NULL)
	{
		pcd->wchar_sz = (u16)_NBytesToInt(inputstream, 2, 0);
		inputstream += 2;
#if defined SDK_DEBUG
		i = _NBytesToInt(inputstream, 2, 0);
		inputstream += 2;
		if (FX32_TYPE != i)
		{
			OS_Warning("Loaded data with fx32 type does not match to tengine fx32 type.\nPlease set appropriate fx32 type in MapEditor settings\n");
		}
		SDK_ASSERT(FX32_TYPE == i);
#else
		inputstream += 2;
#endif
#if defined SDK_DEBUG
		i = _NBytesToInt(inputstream, 2, 0);
		inputstream += 2;
		if (sizeof(fx32) != (size_t)i)
		{
			OS_Warning("Loaded data with sizeof(fx32)=%d does not match to tengine sizeof(fx32)=%zu\n", i, sizeof(fx32));
		}
		SDK_ASSERT(sizeof(fx32) == (size_t)i);
#else
		inputstream += 2;
#endif
#if defined SDK_DEBUG
		if(sizeof(wchar) != (u32)cd.wchar_sz)
		{
			OS_Warning("Platform wchar_t size (%d), does not match to tengine binary data (%d).\nPlease change text char size in MapEditor settings\n", 
					   (s32)sizeof(wchar), cd.wchar_sz);
		}
#endif
		SDK_ASSERT(sizeof(wchar) == (u32)cd.wchar_sz);
		{
			const s32 delay = _NBytesToInt(inputstream, 2, 0);
			if(DEFAULT_DELAY_MS_FX > FX32(delay))
			{
				pcd->delay_fx = DEFAULT_DELAY_MS_FX; 
			}
			else
			{
				pcd->delay_fx = FX32(delay);
			}
			pcd->deltatime_scale = FX_Div(FIXED_TIMESTEP_MS_FX, pcd->delay_fx);
			inputstream += 2;
		}
		pcd->enable_prp_id = (u16)_NBytesToInt(inputstream, 2, 0);
		inputstream += 2;
		pcd->spd_prp_id = (u16)_NBytesToInt(inputstream, 2, 0);
		inputstream += 2;
		pcd->p_w = (u16)_NBytesToInt(inputstream, 2, 0);
		inputstream += 2;
		pcd->p_h = (u16)_NBytesToInt(inputstream, 2, 0);
		inputstream += 2;
		pcd->tl_layers_ct = *inputstream;
		inputstream++;
		pcd->res_img_ct = (u16)_NBytesToInt(inputstream, 2, 0);
		inputstream += 2;
		pcd->res_img_fnames = NULL;
		pcd->res_img = NULL;
		if(pcd->res_img_ct > 0)
		{
			inputstream += *inputstream + 1;
			pcd->res_img_fnames = (const char**)inputstream;
			inputstream += pcd->res_img_ct * SIZEOF_PTR;
			inputstream += *inputstream + 1;			
			pcd->res_img = (struct BMPImage**)inputstream;
			inputstream += pcd->res_img_ct * SIZEOF_PTR;
			for (i = 0; i < pcd->res_img_ct; i++)
			{
				k = *inputstream;
				inputstream++;
				inputstream += *inputstream + 1;
				pcd->res_img_fnames[i] = (char*)inputstream;
				inputstream += k;
				pcd->res_img[i] = NULL;
			}
		}
		pcd->res_fnt_ct = (u16)_NBytesToInt(inputstream, 2, 0);
		inputstream += 2;
		pcd->res_fnt_fnames = NULL;
		pcd->res_fnt = NULL;
		if(pcd->res_fnt_ct > 0)
		{
			inputstream += *inputstream + 1;
			pcd->res_fnt_fnames = (const char**)inputstream;
			inputstream += pcd->res_fnt_ct * SIZEOF_PTR;
			inputstream += *inputstream + 1;
			pcd->res_fnt = (struct TERFont**)inputstream;
			inputstream += pcd->res_fnt_ct * SIZEOF_PTR;
			for (i = 0; i < pcd->res_fnt_ct; i++)
			{
				k = *inputstream;
				inputstream++;
				inputstream += *inputstream + 1;
				pcd->res_fnt_fnames[i] = (char*)inputstream;
				inputstream += k;
				pcd->res_fnt[i] = NULL;
			}
		}
		pcd->res_snd_ct = (u16)_NBytesToInt(inputstream, 2, 0);
		inputstream += 2;
		pcd->res_snd_fnames = NULL;
		pcd->res_snd = NULL;
		if(pcd->res_snd_ct > 0)
		{
			inputstream += *inputstream + 1;
			pcd->res_snd_fnames = (const char**)inputstream;
			inputstream += pcd->res_snd_ct * SIZEOF_PTR;
			inputstream += *inputstream + 1;
			pcd->res_snd = (struct TERSound**)inputstream;
			inputstream += pcd->res_snd_ct * SIZEOF_PTR;
			for (i = 0; i < pcd->res_snd_ct; i++)
			{
				k = *inputstream;
				inputstream++;
				inputstream += *inputstream + 1;
				pcd->res_snd_fnames[i] = (char*)inputstream;
				inputstream += k;
				pcd->res_snd[i] = NULL;
			}
		}
		pcd->anim_ct = *inputstream;
		inputstream++;
		pcd->jn_ct = *inputstream;
		inputstream++;
		pcd->states_ct = *inputstream;
		inputstream++;
		inputstream += *inputstream + 1;
		pcd->states = (const u8**)inputstream;
		inputstream += pcd->states_ct * SIZEOF_PTR;
		for (i = 0; i < pcd->states_ct; i++)
		{
			inputstream += *inputstream + 1;
			pcd->states[i] = (u8*)inputstream;
			inputstream += stCOUNT;
		}
		inputstream += *inputstream + 1;
		pcd->states_map = (u8*)inputstream; 
		inputstream += pcd->states_ct;
		j = _NBytesToInt(inputstream, 2, 0);
		inputstream += 2;
		pcd->fr_data = NULL;
		if(j > 0)
		{
			inputstream += *inputstream + 1;
			pcd->fr_data = (const u16**)inputstream;
			inputstream += j * SIZEOF_PTR;
			for (i = 0; i < j; i++)
			{
				inputstream += *inputstream + 1;
				pcd->fr_data[i] = (u16*)inputstream;
				inputstream += resobjprpCount * sizeof(u16);
			}
		}
		pcd->ppr_count = _NBytesToInt(inputstream, 2, 0);
		inputstream += 2;
		pcd->ppr_decortype = NULL;
		pcd->ppr_anim_data = NULL;
		pcd->ppr_fr_data = NULL;
		pcd->ppr_frfr_data = NULL;
		if(pcd->ppr_count > 0)
		{
			inputstream += *inputstream + 1;
			pcd->ppr_decortype = (s16*)inputstream;
			inputstream += pcd->ppr_count * sizeof(s16);

			inputstream += *inputstream + 1;
			pcd->ppr_anim_data = (s16***)inputstream;
			inputstream += pcd->ppr_count * SIZEOF_PTR;

			inputstream += *inputstream + 1;
			pcd->ppr_fr_data = (const fx32****)inputstream;
			inputstream += pcd->ppr_count * SIZEOF_PTR;

			inputstream += *inputstream + 1;
			pcd->ppr_frfr_data = (const s16*****)inputstream;
			inputstream += pcd->ppr_count * SIZEOF_PTR;

			for (i = 0; i < pcd->ppr_count; i++)
			{
				pcd->ppr_decortype[i] = (s16)_NBytesToInt(inputstream, 2, 0);
				inputstream += 2;

				inputstream += *inputstream + 1;
				pcd->ppr_anim_data[i] = (s16**)inputstream;
				inputstream += pcd->anim_ct * SIZEOF_PTR;

				inputstream += *inputstream + 1;
				pcd->ppr_fr_data[i] = (const fx32***)inputstream;
				inputstream += pcd->anim_ct * SIZEOF_PTR;

				inputstream += *inputstream + 1;
				pcd->ppr_frfr_data[i] = (const s16****)inputstream;
				inputstream += pcd->anim_ct * SIZEOF_PTR;

				for (j = 0; j < pcd->anim_ct; j++)
				{
					s32 a = _NBytesToInt(inputstream, 2, 0);
					inputstream += 2;
					if(a > 0)
					{
						inputstream += *inputstream + 1;
						pcd->ppr_anim_data[i][j] = (s16*)inputstream;
						inputstream += ppranidataCOUNT * sizeof(s16);
						if(pcd->ppr_anim_data[i][j][ANIFRCOUNT] > 0)
						{
							_loadFrames(pcd, pcd->ppr_anim_data[i][j][ANIFRCOUNT], i, j, &inputstream);
						}
					}
                }
			}
		}
		inputstream_temp = inputstream;
		for (j = 0; j < (s32)pcd->initParams.layersCount; j++)
		{
			const u8 *tinputstream = inputstream;
			_readTriggers(pcd->instances[j], &tinputstream, TRUE, pcd);
			pcd->common_data_tr_pt = tinputstream;
			_readScripts(pcd->instances[j], &tinputstream, TRUE, pcd);
			pcd->common_data_sp_pt = pcd->common_data_tr_pt;
			pcd->common_data_tr_pt = inputstream;
			inputstream_temp = tinputstream;
			
			for (i = 0; i < BGSELECT_NUM; i++)
			{
#ifdef USE_CUSTOM_RENDER
				pcd->instances[j]->t_c_dx_fx[i] = -FX32_ONE;
				pcd->instances[j]->t_c_dy_fx[i] = -FX32_ONE;
#endif
				if(pcd->instances[j]->ds_plane_fr_array_beg[i] < 0)
				{
					pcd->instances[j]->ds_plane_fr_array_beg[i] = 0;
				}
				else
				{
					SDK_ASSERT(pcd->instances[j]->ds_plane_fr_array_beg[i] >= 0 && pcd->instances[j]->ds_plane_fr_array_beg[i] <= pcd->tl_layers_ct);
				}

				if(pcd->instances[j]->ds_plane_fr_array_end[i] < 0)
				{
					pcd->instances[j]->ds_plane_fr_array_end[i] = (s32)pcd->tl_layers_ct;
				}
				else
				{
					SDK_ASSERT(pcd->instances[j]->ds_plane_fr_array_end[i] >= 0 && pcd->instances[j]->ds_plane_fr_array_end[i] <= pcd->tl_layers_ct);
				}
			}
			assignLayerWithRenderPlane(j, pcd->instances[j]->bgType, pcd->instances[j]->primary);
		}
		inputstream = inputstream_temp;
		
#if defined SDK_DEBUG
		i = _NBytesToInt(inputstream, 2, 0);
		inputstream += 2;
		if (i != CS_MAGIC_NUMBER)
		{
			OS_Warning("Corrupted Data Error\n");
		}
		SDK_ASSERT(i == CS_MAGIC_NUMBER); // Corrupted Data Error
#else
		inputstream += 2;
#endif
	}
}
// ----------------------------------------------------------------------------------

void _loadFrames(struct TEngineCommonData *pcd, s32 anm_ct, s32 i, s32 j, const u8 **inputstream)
{
	s32 a;
	pcd->ppr_anim_data[i][j][ANIFRCOUNT] = (s16)anm_ct; 
	*inputstream += **inputstream + 1;
	pcd->ppr_fr_data[i][j] = (const fx32**)*inputstream;
	*inputstream += pcd->ppr_anim_data[i][j][ANIFRCOUNT] * SIZEOF_PTR;
	*inputstream += **inputstream + 1;
	pcd->ppr_frfr_data[i][j] = (const s16***)*inputstream;
	*inputstream += pcd->ppr_anim_data[i][j][ANIFRCOUNT] * SIZEOF_PTR;

	for (a = 0; a < pcd->ppr_anim_data[i][j][ANIFRCOUNT]; a++)
	{
		s32 anifrct;
		*inputstream += **inputstream + 1;
		pcd->ppr_fr_data[i][j][a] = (const fx32*)*inputstream;
		*inputstream += (ppranifrfixeddataCOUNT + pcd->jn_ct * 2) * sizeof(fx32);
		anifrct = fx2int(pcd->ppr_fr_data[i][j][a][ANIFRFRCOUNT]);
		if(anifrct > 0)
		{
			s32 b = 0;
			*inputstream += **inputstream + 1;
			pcd->ppr_frfr_data[i][j][a] = (const s16**)*inputstream;
			*inputstream += anifrct * SIZEOF_PTR;
			for(; b < anifrct; b++)
			{
				*inputstream += **inputstream + 1;
				pcd->ppr_frfr_data[i][j][a][b] = (const s16*)*inputstream;
				*inputstream += ppranifrfrdataCOUNT * sizeof(s16);
			}
		}
	}
}
// ----------------------------------------------------------------------------------

void releaseCommonData()
{
	_releaseCommonData(&cd);
}
// ----------------------------------------------------------------------------------

static void _releaseCommonData(struct TEngineCommonData *pcd)
{
	s32 i;
	pcd->res_img_fnames = NULL;
	pcd->res_fnt_fnames = NULL;
	pcd->res_snd_fnames = NULL;
	pcd->fr_data = NULL;
	pcd->ppr_decortype = NULL;
	pcd->ppr_anim_data = NULL;
	pcd->ppr_fr_data = NULL;
	pcd->ppr_frfr_data = NULL;
	i = pcd->res_img_ct;
	while(i > 0)
	{
		i--;
		if (pcd->res_img[i] != NULL)
		{
			SDK_ASSERT(0); //please call releaseMapData before
		}
	}
	i = pcd->res_fnt_ct;
	while(i > 0)
	{
		i--;
		if (pcd->res_fnt[i] != NULL)
		{
			SDK_ASSERT(0); //please call releaseMapData before
		}
	}
	i = pcd->res_snd_ct;
	while(i > 0)
	{
		i--;
		if (pcd->res_snd[i] != NULL)
		{
			SDK_ASSERT(0); //please call releaseMapData before
		}
	}
	pcd->states_map = NULL;
	pcd->states = NULL;
	pcd->res_img = NULL;
    pcd->res_fnt = NULL;
    pcd->res_snd = NULL;
	if(pcd->common_data_file != NULL)
	{
		FREE(pcd->common_data_file);
		pcd->common_data_file = NULL;
		pcd->common_data_tr_pt = NULL;
		pcd->common_data_sp_pt = NULL;
	}
}
// ----------------------------------------------------------------------------------

void releaseTextData(void)
{
	if(cd.text_data_file != NULL)
	{
		FREE(cd.text_data_file);
		cd.text_data_file = NULL;
		cd.textdatatext = NULL;
		cd.textdatatext_sct = NULL;
	}
}
// ----------------------------------------------------------------------------------

static void _readTextData(u8* language, struct TEngineCommonData *pcd)
{
	const u8 *inputstream;
	s32 i, j, c, k;

	SDK_ASSERT(pcd->text_data_file == NULL); // only one language can be loaded
	
	inputstream = pcd->text_data_file = language;
	SDK_NULL_ASSERT(inputstream);
	if (inputstream != NULL)
	{
		i = _NBytesToInt(inputstream, 2, 0);
		inputstream += 2;
		if(i > 0)
		{
			inputstream += *inputstream + 1;
			pcd->textdatatext = (const wchar***)inputstream;
			inputstream += i * SIZEOF_PTR;
			inputstream += *inputstream + 1;
			pcd->textdatatext_sct = (u16*)inputstream;
			inputstream += i * sizeof(u16);
			for(j = 0; j < i; j++)
			{
				c = _NBytesToInt(inputstream, 2, 0);
				inputstream += 2;
				inputstream += *inputstream + 1;
				pcd->textdatatext[j] = (const wchar**)inputstream;
				inputstream += c * SIZEOF_PTR;
				pcd->textdatatext_sct[j] = (u16)c;
				for(k = 0; k < c; k++)
				{
					inputstream += *inputstream + 1;
					pcd->textdatatext[j][k] = (wchar*)inputstream;
					inputstream += (*inputstream + 1 + 1) * sizeof(wchar);
				}
			}
		}
	}
}
// ----------------------------------------------------------------------------------

void _readMap(u32 layer, u8 *level, struct TEngineCommonData *pcd)
{
	s16 uz_db, uz_df, uz_pr;
	s32 all_db_count, all_df_count, i, strm_af_ct, j;
#ifdef TENGINE_LEGACY_CODE
	s32 start_script;
#endif
	s32 camera_pos;
	const u8 *inputstream = NULL;
	const u16 *strm_ppraniarr = NULL;
	
	struct TEngineInstance *ei = _getInstance(layer);
	SDK_NULL_ASSERT(ei);

#ifdef TENGINE_LEGACY_CODE
	start_script = -1;
#endif

	inputstream = ei->data_file = level;
	SDK_NULL_ASSERT(inputstream);
	SDK_ASSERT(ei->ld_img_res_ct == 0); // free memory from resources before!
	SDK_ASSERT(ei->ld_snd_sfx_res_ct == 0);
	SDK_ASSERT(ei->ld_snd_bgm_res_ct == 0);
	SDK_ASSERT(ei->ld_fnt_res_ct == 0);

#ifdef USE_CUSTOM_RENDER
	for (i = 0; i < BGSELECT_NUM; i++)
	{
		ei->t_c_dx_fx[i] = -FX32_ONE;
		ei->t_c_dy_fx[i] = -FX32_ONE;
	}
#endif

	if (inputstream != NULL)
	{
		// _NBytesToInt(inputstream, 2, 0);
		inputstream += 2;
		// _NBytesToInt(inputstream, 2, 0);
		inputstream += 2;
		ei->zones_ct = *inputstream;
		SDK_ASSERT(ei->zones_ct > 0);
		inputstream++;
		ei->maxbgres = (u16)_NBytesToInt(inputstream, 2, 0); 
		inputstream += 2;
		ei->pnt_count = _NBytesToInt(inputstream, 2, 0);
		inputstream += 2;
		if(ei->pnt_count > 0)
		{
			inputstream += *inputstream + 1;
			ei->tile_data = (const s16**)inputstream;
			inputstream += ei->pnt_count * SIZEOF_PTR;
			inputstream += *inputstream + 1;
			ei->fr_img = (const u16***)inputstream;
			inputstream += ei->pnt_count * SIZEOF_PTR;
			for (i = 0; i < ei->pnt_count; i++)
			{
				inputstream += *inputstream + 1;
				ei->tile_data[i] = (s16*)inputstream;
				inputstream += tileprpCount * sizeof(s16);
				if(ei->tile_data[i][TILE_LAYERS] > 0)
				{
					inputstream += *inputstream + 1;
					ei->fr_img[i] = (const u16**)inputstream;
					inputstream += ei->tile_data[i][TILE_LAYERS] * SIZEOF_PTR;
				}
				for (j = 0; j < ei->tile_data[i][TILE_LAYERS]; j++)
				{
					inputstream += *inputstream + 1;
					ei->fr_img[i][j] = (u16*)inputstream;
					inputstream += resfrprpCount * sizeof(u16);
				}
			}
		}
		ei->map_w = _NBytesToInt(inputstream, 2, 0);
		inputstream += 2;
		ei->map_h = _NBytesToInt(inputstream, 2, 0);
		inputstream += 2;

		_calculateLogicSizes(ei, pcd);

		// OS_Printf("ei->map_w=%d map_h=%d\n",ei->map_w,map_h);
		ei->sz_map = ei->map_w * ei->map_h;
		ei->sz_mapH = ei->map_h * pcd->p_h;
		ei->sz_mapW = ei->map_w * pcd->p_w;
		if(ei->maxbgres == 0 && ei->pnt_count == 0)
		{
			ei->primary = FALSE;
			ei->tilemap = NULL;
		}
		else
		{
			inputstream += *inputstream + 1;
			ei->tilemap = (u16*)inputstream;
			inputstream += ei->sz_map * sizeof(u16);
		}
		ei->t_tilemap = ei->tilemap;
#ifdef USE_TILEMAP_ATTRIBUTES
		ei->tileattrmap = NULL;
#endif
		ei->map_img_ct = (u16)_NBytesToInt(inputstream, 2, 0);
		inputstream += 2;
		if(ei->map_img_ct > 0)
		{
			inputstream += *inputstream + 1;
			ei->map_img = (u16*)inputstream;
			inputstream += ei->map_img_ct * sizeof(u16);
		}
		ei->map_fnt_ct = (u16)_NBytesToInt(inputstream, 2, 0);
		inputstream += 2;
		if(ei->map_fnt_ct > 0)
		{
			inputstream += *inputstream + 1;
			ei->map_fnt = (u16*)inputstream;
			inputstream += ei->map_fnt_ct * sizeof(u16);
			for (i = 0; i < ei->map_fnt_ct; i++)
			{
				if(pcd->res_fnt[ei->map_fnt[i]] == NULL)
				{
					pcd->res_fnt[ei->map_fnt[i]] = (struct TERFont*)MALLOC(sizeof(struct TERFont), "_readMap:TERFont");
					MI_CpuFill8(pcd->res_fnt[ei->map_fnt[i]], 0, sizeof(struct TERFont));
				}
				pcd->res_fnt[ei->map_fnt[i]]->mThisRef++;
			}
		}
		ei->map_snd_sfx_ct = (u16)_NBytesToInt(inputstream, 2, 0);
		inputstream += 2;
		if(ei->map_snd_sfx_ct > 0)
		{
			inputstream += *inputstream + 1;
			ei->map_snd_sfx = (u16*)inputstream;
			inputstream += ei->map_snd_sfx_ct * sizeof(u16);
			for (i = 0; i < ei->map_snd_sfx_ct; i++)
			{
				if(pcd->res_snd[ei->map_snd_sfx[i]] == NULL)
				{
					pcd->res_snd[ei->map_snd_sfx[i]] = (struct TERSound*)MALLOC(sizeof(struct TERSound), "_readMap: sfx TERSound");
					MI_CpuFill8(pcd->res_snd[ei->map_snd_sfx[i]], 0, sizeof(struct TERSound));
				}
				pcd->res_snd[ei->map_snd_sfx[i]]->mThisRef++;
			}
		}
		ei->map_snd_bgm_ct = (u16)_NBytesToInt(inputstream, 2, 0);
		inputstream += 2;
		if(ei->map_snd_bgm_ct > 0)
		{
			inputstream += *inputstream + 1;
			ei->map_snd_bgm = (u16*)inputstream;
			inputstream += ei->map_snd_bgm_ct * sizeof(u16);
			for (i = 0; i < ei->map_snd_bgm_ct; i++)
			{
				if(pcd->res_snd[ei->map_snd_bgm[i]] == NULL)
				{
					pcd->res_snd[ei->map_snd_bgm[i]] = (struct TERSound*)MALLOC(sizeof(struct TERSound), "_readMap: bgm TERSound");
					MI_CpuFill8(pcd->res_snd[ei->map_snd_bgm[i]], 0, sizeof(struct TERSound));
				}
				pcd->res_snd[ei->map_snd_bgm[i]]->mThisRef++;
			}
		}
		strm_af_ct = _NBytesToInt(inputstream, 2, 0);
		inputstream += 2;
		if(strm_af_ct > 0)
		{
			SDK_ASSERT(pcd->res_strm_names == 0); //only a single layer with a stream objects is allowed
			
			inputstream += *inputstream + 1;
			pcd->res_strm_names = (const char **)inputstream;
			inputstream += strm_af_ct * SIZEOF_PTR;
			for(j = 0; j < strm_af_ct; j++)
			{
				i = *inputstream;
				inputstream++;
				inputstream += *inputstream + 1;
				pcd->res_strm_names[j] = (const char *)inputstream;
				inputstream += i;
			}
			pcd->strm_obj_ct = _NBytesToInt(inputstream, 2, 0);
			inputstream += 2;
			pcd->strm_type_ct = _NBytesToInt(inputstream, 2, 0); 
			inputstream += 2;
			SDK_ASSERT(pcd->strm_type_ct > 0);
			inputstream += *inputstream + 1;
			strm_ppraniarr = (const u16*)inputstream;
			inputstream += pcd->strm_type_ct * sizeof(u16) * 2;
			
			for(uz_pr = 0; uz_pr < pcd->strm_type_ct * 2; uz_pr += 2)
			{
				i = _NBytesToInt(inputstream, 2, 0);
				inputstream += 2;
				_loadFrames(pcd, i, strm_ppraniarr[uz_pr], strm_ppraniarr[uz_pr + 1], &inputstream);
			}
		}

		all_db_count = uz_db = (s16)_NBytesToInt(inputstream, 2, 0);
		inputstream += 2;
		ei->all_pr_count = uz_pr = (s16)_NBytesToInt(inputstream, 2, 0);
		inputstream += 2;
		all_df_count = uz_df = (s16)_NBytesToInt(inputstream, 2, 0);
		inputstream += 2;

		inputstream += *inputstream + 1;
		ei->db_count = (s16*)inputstream;
		inputstream += ei->zones_ct * sizeof(s16);

		inputstream += *inputstream + 1;
		ei->pr_count = (s16*)inputstream;
		inputstream += ei->zones_ct * sizeof(s16);

		inputstream += *inputstream + 1;
		ei->df_count = (s16*)inputstream;
		inputstream += ei->zones_ct * sizeof(s16);

		for (i = 0; i < ei->zones_ct; i++)
		{
			all_db_count += ei->db_count[i];
			ei->all_pr_count += ei->pr_count[i];
			all_df_count += ei->df_count[i];

			ei->db_count[i] += uz_db;
			ei->pr_count[i] += uz_pr;
			ei->df_count[i] += uz_df;
		}

		ei->pr_idata_ct = _NBytesToInt(inputstream, 2, 0);
		inputstream += 2;
#if defined SDK_DEBUG
		if (ei->pr_idata_ct != all_db_count + ei->all_pr_count + all_df_count)
		{
			OS_Warning("Corrupted Data Error\n");
		}
#endif
		SDK_ASSERT(ei->pr_idata_ct == all_db_count + ei->all_pr_count + all_df_count); // Corrupted Data Error
		ei->pr_idata_ct = all_db_count + ei->all_pr_count + all_df_count;
		if(strm_af_ct > 0)
		{
			pcd->strm_instance = ei;
		}
#if defined SDK_DEBUG
		if (ei->pr_idata_ct == 0)
		{
			OS_Warning("Attempt to load map with no objects. Check current map with MapEditor\n");
		}
#endif
		SDK_ASSERT(ei->pr_idata_ct); // Attempt to load map with no objects. Check current map with MapEditor.
		
		inputstream += *inputstream + 1;
		ei->pr_fdata = (fx32**)inputstream;
		inputstream += ei->pr_idata_ct * SIZEOF_PTR;
		
		inputstream += *inputstream + 1;
		ei->pr_idata = (s32**)inputstream;
		inputstream += ei->pr_idata_ct * SIZEOF_PTR;

		inputstream += *inputstream + 1;
		ei->pr_cfdata = (fx32**)inputstream;
		inputstream += ei->all_pr_count * SIZEOF_PTR;

		inputstream += *inputstream + 1;
		ei->pr_cidata = (s32**)inputstream;
		inputstream += ei->all_pr_count * SIZEOF_PTR;

		inputstream += *inputstream + 1;
		ei->pr_prp = (fx32**)inputstream;
		inputstream += ei->all_pr_count * SIZEOF_PTR;

		inputstream += *inputstream + 1;
		ei->pr_spt = (u16**)inputstream;
		inputstream += ei->all_pr_count * SIZEOF_PTR;

		inputstream += *inputstream + 1;
		ei->pr_textdata = (u16**)inputstream;
		inputstream += ei->all_pr_count * SIZEOF_PTR;

		inputstream += *inputstream + 1;
		ei->pr_textdataalign = (u32*)inputstream;
		inputstream += ei->all_pr_count * SIZEOF_PTR;
		
		inputstream += *inputstream + 1;
		ei->pr_textdatatext = (wchar***)inputstream;
		inputstream += ei->all_pr_count * SIZEOF_PTR;									

		ei->pr_arr_off[OBJ_GAME] = 0;
		ei->pr_arr_off[OBJ_BACK] = (u32)ei->all_pr_count;

		for (i = (s32)ei->pr_arr_off[OBJ_BACK]; i < (s32)(all_db_count + ei->pr_arr_off[OBJ_BACK]); i++)
		{
			inputstream += *inputstream + 1;
			ei->pr_fdata[i] = (fx32*)inputstream;
			inputstream += PR_FDATA_COUNT * sizeof(fx32);
			
			inputstream += *inputstream + 1;
			ei->pr_idata[i] = (s32*)inputstream;
			inputstream += PR_IDATA_COUNT * sizeof(s32);
			
			ei->pr_idata[i][IANIENDFLAG] = 0;
			ei->pr_idata[i][ITEMPSTATE] = -1;
			_setState(ei, (s32)i, ei->pr_idata[i][ISTATE], FALSE);
		}
		for (i = 0; i < ei->all_pr_count; i++)
		{
			ei->pr_cfdata[i] = (fx32*)MALLOC(PR_C_FDATA_COUNT * sizeof(fx32), "readMap:ei->pr_cfdata[i]");
			MI_CpuClear32(ei->pr_cfdata[i], PR_C_FDATA_COUNT * sizeof(fx32));
			ei->pr_cidata[i] = (s32*)MALLOC((PR_C_FIXEDIDATA_COUNT + pcd->jn_ct) * sizeof(s32), "readMap:ei->pr_cidata[i]");
			MI_CpuClear32(ei->pr_cidata[i], (PR_C_FIXEDIDATA_COUNT + pcd->jn_ct) * sizeof(s32));

			inputstream += *inputstream + 1;
			ei->pr_fdata[i] = (fx32*)inputstream;
			inputstream += PR_FDATA_COUNT * sizeof(fx32);
			
			inputstream += *inputstream + 1;
			ei->pr_idata[i] = (s32*)inputstream;
			inputstream += PR_IDATA_COUNT * sizeof(s32);

			ei->pr_cidata[i][ILINKTOOBJ] = _NBytesToInt(inputstream, 2, 0);
			inputstream += 2;
			ei->pr_cidata[i][INODE] = _NBytesToInt(inputstream, 2, 0);
			inputstream += 2;
			{
				u16 n;
				for (n = 0; n < pcd->jn_ct; n++)
				{
					ei->pr_cidata[i][IJOINNODEBEGIN + n] = _NBytesToInt(inputstream, 2, 0);
					inputstream += 2;
				}
			}
			ei->pr_cidata[i][ICUSTOMFLAGS] = 0;
#ifdef CUSTOMAFFINETRANSFORM_SUPPORT
			ei->pr_cfdata[i][FROTSIN] = 0;
			ei->pr_cfdata[i][FROTCOS] = FX32_ONE;
			ei->pr_cfdata[i][FCUSTOMSCALEXVALUE] = 
			ei->pr_cfdata[i][FCUSTOMSCALEYVALUE] = FX32_ONE;
			ei->pr_cidata[i][ICUSTOMFLAGS] |= IRECALCULATELOGICRECTSFLAG;
#endif
			ei->pr_cidata[i][ICUSTOMALPHA] = ALPHA_OPAQ;
			ei->pr_cidata[i][ICUSTOMBLENDCOLOR] = DIFD_DEF_FILL_COLOR;
			inputstream += *inputstream + 1;
			ei->pr_spt[i] = (u16*)inputstream;
			inputstream += SPT_COUNT * sizeof(u16);

			j = _NBytesToInt(inputstream, 2, 0);
			inputstream += 2;
			inputstream += *inputstream + 1;
			ei->pr_prp[i] = (fx32*)inputstream;
			inputstream += j;

			ei->pr_idata[i][IANIFRIDX] = -1;
			ei->pr_idata[i][IANIENDFLAG] = 0;
			ei->pr_idata[i][ITEMPSTATE] = -1;
			ei->pr_cfdata[i][FDISTLENGHT]= -FX32_ONE;
			ei->pr_cfdata[i][FX0_NODE] = ei->pr_fdata[i][FX];
			ei->pr_cfdata[i][FY0_NODE] = ei->pr_fdata[i][FY];
			ei->pr_cidata[i][IPREVNODE] = NONE_MAP_IDX;
			ei->pr_cidata[i][ICLIPRECTOBJ] = NONE_MAP_IDX;

			if(ei->pr_idata[i][IPARENT_IDX] != PARENT_TEXT_OBJ)
			{
				ei->pr_textdata[i] = NULL;
				_setState(ei, (s32)i, ei->pr_idata[i][ISTATE], FALSE);
			}
			else
			{
				if(pcd->enable_prp_id != NONE_MAP_IDX)
				{
					ei->pr_prp[i][pcd->enable_prp_id] = FX32_ONE;
				}
				inputstream += *inputstream + 1;
				ei->pr_textdata[i] = (u16*)inputstream;
#ifdef KOLIBRIOS_APP
				{
					#define _GX_RGBA_B_SHIFT_RGB (1)
					#define _GX_RGBA_B_MASK_RGB (0x003e)
					#define _GX_RGBA_G_SHIFT_RGB (6)
					#define _GX_RGBA_G_MASK_RGB (0x07c0)
					#define _GX_RGBA_R_SHIFT_RGB (11)
					#define _GX_RGBA_R_MASK_RGB (0xf800)
					#define _GX_RGBA_A_SHIFT_RGB (0)
					#define _GX_RGBA_A_MASK_RGB (0x0001)
					const u8 cr = (ei->pr_textdata[i][TXTCOLOR] & _GX_RGBA_R_MASK_RGB) >> _GX_RGBA_R_SHIFT_RGB;
					const u8 cg = (ei->pr_textdata[i][TXTCOLOR] & _GX_RGBA_G_MASK_RGB) >> _GX_RGBA_G_SHIFT_RGB;
					const u8 cb = (ei->pr_textdata[i][TXTCOLOR] & _GX_RGBA_B_MASK_RGB) >> _GX_RGBA_B_SHIFT_RGB;
					const u8 ca = 0x1f * (ei->pr_textdata[i][TXTCOLOR] & _GX_RGBA_A_MASK_RGB);
					ei->pr_textdata[i][TXTCOLOR] = GX_RGBA(cr, cg, cb, ca);
				}
#endif
				inputstream += pprtxtdataCOUNT * sizeof(s16);
				ei->pr_textdataalign[i] = _NBytesToInt(inputstream, 4, 0);
				inputstream += 4;
				inputstream += *inputstream + 1;
				if(ei->pr_textdata[i][TXTCHRCOUNT] == MAX_RES_IDX)
				{
                    s32 intval = _NBytesToInt(inputstream, 4, 0);
                    MI_CpuCopy8(&intval, &ei->pr_textdatatext[i], 4);
					inputstream += 4;
				}
				else
				{
					if(ei->pr_textdata[i][TXTSTRCOUNT] > 0)
					{
						ei->pr_textdatatext[i] = (wchar**)inputstream;
						inputstream += ei->pr_textdata[i][TXTSTRCOUNT] * SIZEOF_PTR;
						for(j = 0; j < ei->pr_textdata[i][TXTSTRCOUNT]; j++)
						{
							inputstream += *inputstream + 1;
							ei->pr_textdatatext[i][j] = (wchar*)inputstream;
							inputstream += (ei->pr_textdata[i][TXTCHRCOUNT] + 1 + 1) * sizeof(wchar);
						}
					}
					else
					{
						ei->pr_textdatatext[i] = NULL;
					}
				}
				//txbSetTextPerPixelAccuracy(i, TRUE, layer);
				ei->pr_idata[i][ISTATE] = TRUE;
			}
		}
		ei->pr_arr_off[OBJ_FORE] = ei->pr_arr_off[OBJ_BACK] + all_db_count;
		for (i = (s32)ei->pr_arr_off[OBJ_FORE]; i < (s32)(all_df_count + ei->pr_arr_off[OBJ_FORE]); i++)
		{
			inputstream += *inputstream + 1;
			ei->pr_fdata[i] = (fx32*)inputstream;
			inputstream += PR_FDATA_COUNT * sizeof(fx32);

			inputstream += *inputstream + 1;
			ei->pr_idata[i] = (s32*)inputstream;
			inputstream += PR_IDATA_COUNT * sizeof(s32);

			ei->pr_idata[i][IANIENDFLAG] = 0;
			ei->pr_idata[i][ITEMPSTATE] = -1;
			_setState(ei, (s32)i, ei->pr_idata[i][ISTATE], FALSE);
		}
		if(strm_af_ct > 0 && pcd->strm_type_ct)
		{
			pcd->strm_obj_data = (struct JOBAnimStreamTask ***)MALLOC(ei->pr_idata_ct * SIZEOF_PTR, "readMap:ei->strm_obj_data");
			for (i = 0; i < ei->pr_idata_ct; i++)
			{
				pcd->strm_obj_data[i] = (struct JOBAnimStreamTask **)MALLOC(pcd->anim_ct * SIZEOF_PTR, "readMap:ei->strm_obj_data[i]");
				for(j = 0; j < pcd->anim_ct; j++)
				{
					pcd->strm_obj_data[i][j] = NULL;
					for(uz_pr = 0; uz_pr < pcd->strm_type_ct * 2; uz_pr += 2)
					{
						s32 ppr_id, ani_id;
						ppr_id = strm_ppraniarr[uz_pr];
						ani_id = strm_ppraniarr[uz_pr + 1];
						if(ppr_id == ei->pr_idata[i][IPARENT_IDX] && ani_id == j)
						{
							pcd->strm_obj_data[i][j] = (struct JOBAnimStreamTask *)MALLOC(sizeof(struct JOBAnimStreamTask), "readMap::ei->strm_obj_data[i][j]");
							MI_CpuFill8(pcd->strm_obj_data[i][j], 0, sizeof(struct JOBAnimStreamTask));
							ei->pr_idata[i][ITEMPSTATE] = -1;
							_setState(ei, i, ei->pr_idata[i][ISTATE], FALSE);
							break;
						}
					}
				}
			}
		}
		_readTriggers(ei, &inputstream, FALSE, pcd);
		{
			const u8* pt = pcd->common_data_tr_pt; 
			SDK_NULL_ASSERT(pcd->common_data_tr_pt);
			_readTriggers(ei, &pt, TRUE, pcd); //read common triggers
		}
		ei->nodes_ct = _NBytesToInt(inputstream, 2, 0);
		inputstream += 2;
		if(ei->nodes_ct > 0)
		{
			inputstream += *inputstream + 1;
			ei->nodes = (u16**)inputstream;
			inputstream += ei->nodes_ct * SIZEOF_PTR;
			inputstream += *inputstream + 1;
			ei->nodes_fdata = (fx32**)inputstream;
			inputstream += ei->nodes_ct * SIZEOF_PTR;
			for (i = 0; i < ei->nodes_ct; i++)
			{
				inputstream += *inputstream + 1;
				ei->nodes_fdata[i] = (fx32*)inputstream;
				inputstream += NODE_FDATA_COUNT * sizeof(fx32);
				inputstream += *inputstream + 1;
				ei->nodes[i] = (u16*)inputstream;
				inputstream += NODE_IDATA_COUNT * sizeof(u16);
			}
		}
		_readScripts(ei, &inputstream, FALSE, pcd);
		{
			const u8* pt = pcd->common_data_sp_pt;
			SDK_NULL_ASSERT(pcd->common_data_sp_pt);
			_readScripts(ei, &pt, TRUE, pcd); // read common scripts
		}
		ei->cld_shape_ct = *inputstream;
		inputstream++;
		if(ei->cld_shape_ct > 0)
		{
			inputstream += *inputstream + 1;
			ei->cld_sh_node = (const struct fxVec2**)inputstream;
			inputstream += ei->cld_shape_ct * SIZEOF_PTR;
			inputstream += *inputstream + 1;
			ei->cld_sh_nodes_ct = (const u16*)inputstream;
			inputstream += ei->cld_shape_ct * sizeof(u16);
			inputstream += *inputstream + 1;
			for (i = 0; i < ei->cld_shape_ct; i++)
			{
				ei->cld_sh_node[i] = (const struct fxVec2*)inputstream;
				inputstream += ei->cld_sh_nodes_ct[i] * sizeof(struct fxVec2);
			}
		}
		ei->t_current_zone = ei->current_zone = *inputstream;
		inputstream++;
#ifdef TENGINE_LEGACY_CODE
		start_script = (s16)_NBytesToInt(inputstream, 2, 0);
#endif
		inputstream += 2;

		camera_pos = (u16)_NBytesToInt(inputstream, 2, 0);
		inputstream += 2;

#if defined SDK_DEBUG
		i = _NBytesToInt(inputstream, 2, 0);
		inputstream += 2;
		if (i != CS_MAGIC_NUMBER)
		{
			OS_Warning("Corrupted Data Error\n");
		}
		SDK_ASSERT(i == CS_MAGIC_NUMBER); // Corrupted Data Error
#else
		inputstream += 2;
#endif

		_setActiveLayer(layer);

		{
			struct fxVec2 pos;
			if (camera_pos != NONE_MAP_IDX)
			{
				pos.x = FX32((camera_pos % (s32)ei->map_w) * pcd->p_w) + ei->logicWidthHalf_fx;
				pos.y = FX32((camera_pos / (s32)ei->map_w) * pcd->p_h) + ei->logicHeightHalf_fx;
			}
			else
			{
				pos.x = -ei->logicWidthHalf_fx;
				pos.y = -ei->logicHeightHalf_fx;
			}
			setCamera(pos);
		}

#ifdef TENGINE_LEGACY_CODE
		if (start_script >= 0)
		{
			startScript(start_script, FALSE, -1, -1);
			pcd->ainst = NULL;
			pcd->a_layer = NONE_MAP_IDX; 
		}
#endif

		_createZoneArrays(ei);
		
		/*
		{
			BOOL t[BGSELECT_NUM + 1];
			BOOL p[BGSELECT_NUM + 1];
			MI_CpuFill8(&t, 0, sizeof(s32) * (BGSELECT_NUM + 1));
			MI_CpuFill8(&p, 0, sizeof(s32) * (BGSELECT_NUM + 1));
			for (j = 0; j < (s32)pcd->instances_count; j++)
			{
				ei = _getInstance(j);
				t[ei->bgType] = TRUE;
				p[ei->bgType] |= ei->primary;
			}
			for (j = 0; j < BGSELECT_NUM; j++)
			{
				if(!t[j] && !p[j])
				{
					OS_Warning("ASSERT: Please use assignLayerWithRenderPlane function before readMap (assign layer to render plane)\n");
					SDK_ASSERT(0);
				}
			}
		}
		*/
	}
}
// ----------------------------------------------------------------------------------

void _calculateLogicSizes(struct TEngineInstance *ei, struct TEngineCommonData *pcd)
{
	fx32 vw_fx, vh_fx;
	SDK_ASSERT(pcd->p_w > 0 && pcd->p_h > 0); // Load CommonData file before!
	vw_fx = FX_Div(FX32(RENDERFN(GetViewWidth)(ei->bgType)), RENDERFN(GetRenderPlaneScale(ei->bgType)));
	vh_fx = FX_Div(FX32(RENDERFN(GetViewHeight)(ei->bgType)), RENDERFN(GetRenderPlaneScale(ei->bgType)));
	ei->d_w = (fx2int(vw_fx) + pcd->p_w - 1) / pcd->p_w;
	ei->d_h = (fx2int(vh_fx) + pcd->p_h - 1) / pcd->p_h;
	if(ei->d_h > ei->map_h)
	{
		ei->d_h = ei->map_h - 1; 	
	}
	if(ei->d_w > ei->map_w)
	{
		ei->d_w = ei->map_w - 1; 	
	}
	ei->l_w = FX32(ei->d_w * pcd->p_w);
	ei->l_h = FX32(ei->d_h * pcd->p_h);
	ei->logicWidthHalf_fx = vw_fx / 2;
	ei->logicHeightHalf_fx = vh_fx / 2;
#ifdef USE_CUSTOM_RENDER
	if(RENDERFN(GetFrameBufferHeight)(ei->bgType) < fx2int(ei->l_h))
	{
#ifdef SDK_DEBUG
		const s32 fbh = RENDERFN(GetFrameBufferHeight)(ei->bgType);
		const s32 l_h = fx2int(ei->l_h);
		OS_Warning("Please increase mFrameBufferHeight8 parameter in RenderPlaneInitParams struct and reinit engine (current frame buffer height = %d, min expected height = %d)\n", fbh, l_h);
#endif
		SDK_ASSERT(0);
	}
	if(RENDERFN(GetFrameBufferWidth)(ei->bgType) < fx2int(ei->l_w))
	{
#ifdef SDK_DEBUG
		const s32 fbw = RENDERFN(GetFrameBufferWidth)(ei->bgType);
		const s32 l_w = fx2int(ei->l_w);
		OS_Warning("Please increase mFrameBufferWidth8 parameter in RenderPlaneInitParams struct and reinit engine (current frame buffer width = %d, min expected width = %d)\n", fbw, l_w);
#endif
		SDK_ASSERT(0);
	}
	ei->shiftTilesMax_fx = FX32(ei->d_w > ei->d_h ? ei->d_w / 2 : ei->d_h / 2);
#endif
}
// ----------------------------------------------------------------------------------

void releaseResources()
{
	if(cd.instances != NULL)
	{
		_freeRes(&cd);
	}
}
// ----------------------------------------------------------------------------------

void releaseMapData(u32 layer)
{
	struct TEngineInstance *ei;
	
	jobSetActive(FALSE);

	if(cd.instances != NULL)
	{
		ei = _getInstance(layer);
		if(ei == NULL)
		{
			return;
		}

		_releaseZoneArrays(ei);

		ei->tile_data = NULL;
		ei->fr_img = NULL;
		ei->map_img = NULL;
		ei->map_img_ct = 0;
		ei->tilemap = NULL;
		ei->t_tilemap = NULL;
#ifdef USE_TILEMAP_ATTRIBUTES
		ei->tileattrmap = NULL;
#endif
		ei->nodes = NULL;
		ei->nodes_fdata = NULL;
		_releaseScriptData(ei);
		_releaseTrigData(ei);
		if(ei->pr_cidata != NULL)
		{
			s32 i;
			if(cd.strm_obj_data != NULL && cd.strm_instance == ei)
			{
				i = ei->pr_idata_ct;
				while(i > 0)
				{
					s32 j;
					i--;
					j = cd.anim_ct;
					while(j > 0)
					{
						j--;
						if(cd.strm_obj_data[i][j])
						{
							jobRemoveStreamVideoTask(cd.strm_obj_data[i][j]);
							FREE(cd.strm_obj_data[i][j]);
						}
					}
					FREE(cd.strm_obj_data[i]);
				}
				FREE(cd.strm_obj_data);
				cd.strm_instance = NULL;
				cd.strm_obj_data = NULL;
				cd.strm_obj_ct = 0;
				cd.res_strm_names = NULL;
			}
			i = ei->all_pr_count;
			while(i > 0)
			{
				i--;
				FREE(ei->pr_cidata[i]);
				ei->pr_cidata[i] = NULL;
				FREE(ei->pr_cfdata[i]);
				ei->pr_cfdata[i] = NULL;
			}
			ei->pr_cfdata = NULL;
			ei->pr_cidata = NULL;
			ei->pr_prp = NULL;
			ei->pr_spt = NULL;
			ei->pr_textdatatext = NULL;
			ei->pr_textdataalign = NULL;
			ei->pr_textdata = NULL;
		}
		ei->pr_idata = NULL;
		ei->pr_fdata = NULL;
		ei->db_count = NULL;
		ei->pr_count = NULL;
		ei->df_count = NULL;
		while(ei->map_snd_bgm_ct > 0)
		{
			ei->map_snd_bgm_ct--;
			SDK_NULL_ASSERT(cd.res_snd[ei->map_snd_bgm[ei->map_snd_bgm_ct]]);
			if(--cd.res_snd[ei->map_snd_bgm[ei->map_snd_bgm_ct]]->mThisRef == 0)
			{
				FREE(cd.res_snd[ei->map_snd_bgm[ei->map_snd_bgm_ct]]);
				cd.res_snd[ei->map_snd_bgm[ei->map_snd_bgm_ct]] = NULL;
			}
		}
		while(ei->map_snd_sfx_ct > 0)
		{
			ei->map_snd_sfx_ct--;
			SDK_NULL_ASSERT(cd.res_snd[ei->map_snd_sfx[ei->map_snd_sfx_ct]]);
			if(--cd.res_snd[ei->map_snd_sfx[ei->map_snd_sfx_ct]]->mThisRef == 0)
			{
				FREE(cd.res_snd[ei->map_snd_sfx[ei->map_snd_sfx_ct]]);
				cd.res_snd[ei->map_snd_sfx[ei->map_snd_sfx_ct]] = NULL;
			}
		}
		while(ei->map_fnt_ct > 0)
		{
			ei->map_fnt_ct--;
			SDK_NULL_ASSERT(cd.res_fnt[ei->map_fnt[ei->map_fnt_ct]]);
			if(--cd.res_fnt[ei->map_fnt[ei->map_fnt_ct]]->mThisRef == 0)
			{
				FREE(cd.res_fnt[ei->map_fnt[ei->map_fnt_ct]]);
				cd.res_fnt[ei->map_fnt[ei->map_fnt_ct]] = NULL;
			}
		}
		ei->map_img_ct = 0;
		if (ei->data_file != NULL)
		{
			FREE(ei->data_file);
			ei->data_file = NULL;
		}
	}
}
// ----------------------------------------------------------------------------------

void _createZoneArrays(struct TEngineInstance *ei)
{
	s32 i, z, j;
	u16** pr_znlist;
	s32 p[LOGIC_ZONES_MAX];
	s32 b[LOGIC_ZONES_MAX];
	s32 f[LOGIC_ZONES_MAX];

	SDK_ASSERT(ei->db_znlist == NULL);
	SDK_ASSERT(ei->df_znlist == NULL);
	SDK_ASSERT(ei->rndr_znlist == NULL);

	ei->db_znlist = (s16**)MALLOC(ei->zones_ct * SIZEOF_PTR, "_createZoneArrays:ei->db_znlist");
	ei->df_znlist = (s16**)MALLOC(ei->zones_ct * SIZEOF_PTR, "_createZoneArrays:ei->df_znlist");
	ei->rndr_znlist = (struct AllocatorList**)MALLOC(ei->zones_ct * SIZEOF_PTR, "_createZoneArrays:ei->rndr_znlist");
	ei->rndr_znlist_allocator = (struct StaticAllocator**)MALLOC(ei->zones_ct * SIZEOF_PTR, "_createZoneArrays:ei->rndr_znlist_pool");
	ei->rndr_znlist_heap = (u8**)MALLOC(ei->zones_ct * SIZEOF_PTR, "_createZoneArrays:ei->rndr_znlist_heap");
	for (i = 0; i < ei->zones_ct; i++)
	{
		ei->db_znlist[i] = NULL;
		if(ei->db_count[i] > 0)
		{
			ei->db_znlist[i] = (s16*)MALLOC(ei->db_count[i] * sizeof(s16), "_crZoneArrays:ei->db_znlist[i]");
		}
		ei->df_znlist[i] = NULL;
		if(ei->df_count[i] > 0)
		{
			ei->df_znlist[i] = (s16*)MALLOC(ei->df_count[i] * sizeof(s16), "_crZoneArrays:ei->df_znlist[i]");
		}
		if(ei->all_pr_count > 0)
		{
			u32 heap_size = AllocatorList_CalculateHeapSize(sizeof(u16), ei->all_pr_count);
			ei->rndr_znlist_heap[i] = (u8*)MALLOC(heap_size, "_crZoneArrays:ei->rndr_znlist_heapp[i]");
			ei->rndr_znlist_allocator[i] = (struct StaticAllocator*)MALLOC(sizeof(struct  StaticAllocator), "_createZoneArrays:ei->rndr_znlist_pool");
			StaticAllocator_Init(ei->rndr_znlist_allocator[i], ei->rndr_znlist_heap[i], heap_size);
			ei->rndr_znlist[i] = (struct AllocatorList*)MALLOC(sizeof(struct AllocatorList), "_crZoneArrays:ei->rndr_znlist[i]");
			AllocatorList_Init(ei->rndr_znlist[i], sizeof(u16), ei->rndr_znlist_allocator[i]);
		}
	}
	pr_znlist = (u16**)MALLOC(ei->zones_ct * SIZEOF_PTR, "_createZoneArrays:pr_znlist");
	for (i = 0; i < ei->zones_ct; i++)
	{
		pr_znlist[i] = NULL;
		if (ei->pr_count[i] > 0)
		{
			pr_znlist[i] = (u16*)MALLOC(ei->pr_count[i] * sizeof(u16), "_crZoneArrays:pr_znlist[i]");
			MI_CpuFill8(pr_znlist[i], 0xff, ei->pr_count[i] * sizeof(u16));
		}
	}
	for (i = 0; i < ei->zones_ct; i++)
	{
		f[i] = b[i] = p[i] = 0;
	}
	for (i = 0; i < ei->pr_idata_ct; i++)
	{
		z = ei->pr_idata[i][IZONE];
		if (i >= (s32)ei->pr_arr_off[OBJ_FORE])
		{
			if (z < 0)
				for (j = 0; j < ei->zones_ct; j++)
				{
					ei->df_znlist[j][f[j]] = (s16)i;
					f[j]++;
				}
			else
			{
				ei->df_znlist[z][f[z]] = (s16)i;
				f[z]++;
			}
		}
		else if (i >= (s32)ei->pr_arr_off[OBJ_BACK])
		{
			if (z < 0)
				for (j = 0; j < ei->zones_ct; j++)
				{
					ei->db_znlist[j][b[j]] = (s16)i;
					b[j]++;
				}
			else
			{
				ei->db_znlist[z][b[z]] = (s16)i;
				b[z]++;
			}
		}
		else
		{			
			if (z < 0)
				for (j = 0; j < ei->zones_ct; j++)
				{
					pr_znlist[j][p[j]] = (u16)i;
					p[j]++;
				}
			else
			{
				pr_znlist[z][p[z]] = (u16)i;
				p[z]++;
			}
		}
	}
	for (i = 0; i < ei->zones_ct; i++)
	{
		for (j = 0; j < ei->pr_count[i]; j++)
		{
			u16 ridx = pr_znlist[i][j];
			if (ridx != 0xffff)
			{
				AllocatorList_PushBack(ei->rndr_znlist[i], &ridx);
			}
		}
	}
	if (pr_znlist != NULL)
	{
		i = ei->zones_ct;
		while (i > 0)
		{
			i--;
			if(pr_znlist[i] != NULL)
			{
				FREE(pr_znlist[i]);
			}
		}
		FREE(pr_znlist);
	}
}
// ----------------------------------------------------------------------------------

void _releaseZoneArrays(struct TEngineInstance *ei)
{
	if (ei->rndr_znlist != NULL)
	{
		s32 i = ei->zones_ct; 
		while(i > 0)
		{
			i--;
			if(ei->rndr_znlist[i] != NULL)
			{
				FREE(ei->rndr_znlist[i]);
				FREE(ei->rndr_znlist_allocator[i]);
				FREE(ei->rndr_znlist_heap[i]);
			}
			/*if(ei->pr_znlist[i] != NULL)
			{
				FREE(ei->pr_znlist[i]);
			}*/
			if(ei->df_znlist[i] != NULL)
			{
				FREE(ei->df_znlist[i]);
			}
			if(ei->db_znlist[i] != NULL)
			{
				FREE(ei->db_znlist[i]);	
			}
		}
	}
	if (ei->rndr_znlist != NULL)
	{
		FREE(ei->rndr_znlist_heap);
		ei->rndr_znlist_heap = NULL;
		FREE(ei->rndr_znlist_allocator);
		ei->rndr_znlist_allocator = NULL;
		FREE(ei->rndr_znlist);
		ei->rndr_znlist = NULL;
	}
	/*if (ei->pr_znlist != NULL)
	{
		FREE(ei->pr_znlist);
		ei->pr_znlist = NULL;
	}*/
	if (ei->df_znlist != NULL)
	{
		FREE(ei->df_znlist);
		ei->df_znlist = NULL;
	}
	if (ei->db_znlist != NULL)
	{
		FREE(ei->db_znlist);
		ei->db_znlist = NULL;
	}
}
// ----------------------------------------------------------------------------------

void _releaseTrigData(struct TEngineInstance *ei)
{
	if(ei->trig_idata != NULL)
	{
		FREE(ei->trig_st);
		FREE(ei->trig_spt);
		FREE(ei->trig_prp);
		FREE(ei->trig_fdata);
		FREE(ei->trig_idata);
		FREE(ei->trig_st_ct);
		FREE(ei->trig_prp_ct);
		ei->trig_st_ct = NULL;
		ei->trig_prp_ct = NULL;
		ei->trig_st = NULL;
		ei->trig_spt = NULL;
		ei->trig_prp = NULL;
		ei->trig_fdata = NULL;
		ei->trig_idata = NULL;
	}
}
// ----------------------------------------------------------------------------------

void _readTriggers(struct TEngineInstance *ei, const u8 **inputstream, BOOL common_mode, struct TEngineCommonData *pcd)
{
	s32 i, j;
	BOOL can_read;
	s32 count, offset, trig_total_count;
	can_read = (common_mode && pcd->common_data_tr_pt != NULL) || !common_mode;
	count = _NBytesToInt(*inputstream, 2, 0);
	*inputstream += 2;
	if (common_mode)
	{
		offset = 0;
		ei->trig_common_count = count;
	}
	else
	{
		offset = ei->trig_common_count;
		trig_total_count = offset + count;
		SDK_ASSERT(ei->trig_idata == NULL);
		SDK_ASSERT(ei->trig_fdata == NULL);
		ei->trig_prp_ct = (u32*)MALLOC(trig_total_count * sizeof(u32), "_readTriggers:t_trig_prp_ct");
		ei->trig_st_ct = (u32*)MALLOC(trig_total_count * sizeof(u32), "_readTriggers:t_trig_st_ct");
		ei->trig_idata = (s32**)MALLOC(trig_total_count * SIZEOF_PTR, "_readTriggers:t_trig_idata");
		ei->trig_fdata = (fx32**)MALLOC(trig_total_count * SIZEOF_PTR, "_readTriggers:t_trig_fdata");
		ei->trig_prp = (fx32**)MALLOC(trig_total_count * SIZEOF_PTR, "_readTriggers:t_trig_prp");
		ei->trig_spt = (u16**)MALLOC(trig_total_count * SIZEOF_PTR, "_readTriggers:t_trig_spt");
		ei->trig_st = (u8**)MALLOC(trig_total_count * SIZEOF_PTR, "_readTriggers:t_trig_st");
	}
	for (i = 0; i < count; i++)
	{
		s32 ii = i + offset;
		*inputstream += **inputstream + 1;
		if (can_read)
		{
			ei->trig_fdata[ii] = (fx32*)*inputstream;
		}
		*inputstream += trprpfdataCOUNT * sizeof(fx32);
		*inputstream += **inputstream + 1;
		if (can_read)
		{
			ei->trig_idata[ii] = (s32*)*inputstream;
		}
		*inputstream += trprpidataCOUNT * sizeof(s32);
		j = **inputstream;
		(*inputstream)++;
		if (can_read)
		{
			ei->trig_st_ct[ii] = (u32)j;
		}
		if (j > 0)
		{
			*inputstream += **inputstream + 1;
			if (can_read)
			{
				ei->trig_st[ii] = (u8*)*inputstream;
			}
		}
		else
		{
			if (can_read)
			{
				ei->trig_st[ii] = NULL;
			}
		}
		*inputstream += j;
		j = **inputstream;
		(*inputstream)++;
		if (j > 0)
		{
			*inputstream += **inputstream + 1;
			if (can_read)
			{
				ei->trig_spt[ii] = (u16*)*inputstream;
			}
			*inputstream += SPT_COUNT * sizeof(u16);
		}
		else
		{
			if (can_read)
			{
				ei->trig_spt[ii] = NULL;
			}
		}
		j = (u16)(_NBytesToInt(*inputstream, 2, 0) * 2);
		*inputstream += 2;
		if (can_read)
		{
			ei->trig_prp_ct[ii] = (u32)(j / sizeof(fx32));
		}
		if (j > 0)
		{
			*inputstream += **inputstream + 1;
			if (can_read)
			{
				ei->trig_prp[ii] = (fx32*)*inputstream;
			}
		}
		else
		{
			if (can_read)
			{
				ei->trig_prp[ii] = NULL;
			}
		}
		*inputstream += j;
	}
}
// ----------------------------------------------------------------------------------

void _releaseScriptData(struct TEngineInstance *ei)
{
	if(ei->spt_bdata != NULL)
	{
		FREE(ei->spt_triglist_ct);
		FREE(ei->spt_triglist);
		FREE(ei->spt_cldobjlist_ct);
		FREE(ei->spt_cldobjlist);
		FREE(ei->spt_cldgrouplist_ct);
		FREE(ei->spt_cldgrouplist);
		FREE(ei->spt_bdata);
		ei->spt_triglist_ct = NULL;
		ei->spt_triglist = NULL;
		ei->spt_cldobjlist_ct = NULL;
		ei->spt_cldobjlist = NULL;
		ei->spt_cldgrouplist_ct = NULL;
		ei->spt_cldgrouplist = NULL;
		ei->spt_bdata = NULL;
	}
}
// ----------------------------------------------------------------------------------

static void _readScripts(struct TEngineInstance *ei, const u8 **inputstream, BOOL common_mode, struct TEngineCommonData *pcd)
{
	s32 i, j;
	s32 count, offset, script_total_count;
	const BOOL can_read = (common_mode && pcd->common_data_sp_pt != NULL) || !common_mode;
	count = _NBytesToInt(*inputstream, 2, 0);
	*inputstream += 2;
	if (common_mode)
	{
		offset = 0;
		ei->script_common_count = count;
	}
	else
	{
		offset = ei->script_common_count;
		script_total_count = offset + count;
		SDK_ASSERT(ei->spt_bdata == NULL);
		ei->spt_bdata = (u16**)MALLOC(script_total_count * SIZEOF_PTR, "_readScripts:t_spt_bdata");
		ei->spt_cldgrouplist = (char**)MALLOC(script_total_count * SIZEOF_PTR, "_readScripts:t_spt_cldgrouplist");
		ei->spt_cldgrouplist_ct = (u32*)MALLOC(script_total_count * sizeof(u32), "_readScripts:t_spt_cldgrouplist_ct");
		ei->spt_cldobjlist = (u16**)MALLOC(script_total_count * SIZEOF_PTR, "_readScripts:t_spt_cldobjlist");
		ei->spt_cldobjlist_ct = (u32*)MALLOC(script_total_count * sizeof(u32), "_readScripts:t_spt_cldobjlist_ct");
		ei->spt_triglist = (u16**)MALLOC(script_total_count * SIZEOF_PTR, "_readScripts:t_spt_triglist");
		ei->spt_triglist_ct = (u32*)MALLOC(script_total_count * sizeof(u32), "_readScripts:t_spt_triglist_ct");
	}
	for (i = 0; i < count; i++)
	{
		s32 ii = i + offset;
		j = **inputstream;
		(*inputstream)++;
		if (can_read)
		{
			ei->spt_cldgrouplist_ct[ii] = (u32)j;
		}
		*inputstream += **inputstream + 1;
		if (can_read)
		{
			ei->spt_cldgrouplist[ii] = (char*)*inputstream;
		}
		*inputstream += j;
		j = **inputstream;
		(*inputstream)++;
		if (can_read)
		{
			ei->spt_cldobjlist_ct[ii] = (u32)j;
		}
		*inputstream += **inputstream + 1;
		if (can_read)
		{
			ei->spt_cldobjlist[ii] = (u16*)*inputstream;
		}
		*inputstream += j * sizeof(u16);
		*inputstream += **inputstream + 1;
		if (can_read)
		{
			ei->spt_bdata[ii] = (u16*) * inputstream;
		}
		*inputstream += sptprpCOUNT * sizeof(u16);
		j = **inputstream;
		(*inputstream)++;
		if (can_read)
		{
			ei->spt_triglist_ct[ii] = (u32)j;
		}
		*inputstream += **inputstream + 1;
		if (can_read)
		{
			ei->spt_triglist[ii] = (u16*)*inputstream;
		}
		*inputstream += j * sizeof(u16);
	}
}
// ----------------------------------------------------------------------------------

void _freeRes(struct TEngineCommonData *pcd)
{
	u32 i;

	pcd->agrl_s = 0;
	pcd->agrl_i = 0;
	pcd->rl_available = FALSE;

#ifdef JOBS_IN_SEPARATE_THREAD
	jobCriticalSectionBegin();	
#endif
	
	jobSetActive(FALSE);
	jobResetTasks();
	jobRemoveStreamFileTask(&ld_fst);

	ld_fst.mData.mMode = JFSDM_MODE_NONE;
	if(ld_fst.mData.mpFile)
	{
		FREE(ld_fst.mData.mpFile);
	}
	if(ld_fst.mData.mpImage != NULL)
	{
		DeleteBMPImage(ld_fst.mData.mpImage);
	}
	ld_fst.mData.mStatus = FALSE;	
	ld_fst.mData.mpImage = NULL;
	ld_fst.mData.mpFile = NULL;

	i = pcd->initParams.layersCount; 
	while(i > 0)
	{
		i--;
		RENDERFN(ClearFrameBuffer)(pcd->instances[i]->bgType);
		pcd->instances[i]->ueof_state = UEOF_EMPTY;
	}
	if(pcd->strm_obj_data != NULL && pcd->strm_obj_ct > 0)
	{
		s32 prid = pcd->strm_instance->pr_idata_ct; 
		while(prid > 0)
		{
			s32 aid; 
			prid--;
			aid = pcd->anim_ct; 
			while(aid > 0)
			{				
				aid--;
				if(pcd->strm_obj_data[prid][aid])
				{
					s32 bfidx = JOB_ANIM_TASK_BUFFER_MAX; 
					while(bfidx > 0)
					{
						bfidx--;
						if(pcd->strm_obj_data[prid][aid]->mpFileData[bfidx] != NULL)
						{
							DeleteBMPImage(pcd->strm_obj_data[prid][aid]->mpFileData[bfidx]);
						}
						pcd->strm_obj_data[prid][aid]->mpLastDrawnImage = NULL;
						pcd->strm_obj_data[prid][aid]->mpFileData[bfidx] = NULL;
						pcd->strm_obj_data[prid][aid]->mData[bfidx].mpImage = NULL;
						pcd->strm_obj_data[prid][aid]->mData[bfidx].mBufferStatus = FALSE;
						pcd->strm_obj_data[prid][aid]->mData[bfidx].mBindStatus = FALSE;
					}
				}
			}
		}
	}

	for (i = 0; i < pcd->initParams.layersCount; i++)
	{
		struct TEngineInstance *ei = pcd->instances[i];
		if(ei->data_file != NULL)
		{
			s32 idx;
#ifdef USE_OPENGL_RENDER
			if(pcd->instances[i]->primary)
			{
				glRender_ReleaseBGLayersData(pcd->instances[i]->bgType);
			}
#endif
			while(ei->ld_snd_bgm_res_ct > 0)
			{
				ei->ld_snd_bgm_res_ct--;
				idx = ei->map_snd_bgm[ei->ld_snd_bgm_res_ct];
				if(idx < NULL_IMG_IDX)
				{
					if(--pcd->res_snd[idx]->mResRef == 0)
					{
						sndReleaseResource(pcd->res_snd[idx]);
					}
				}
			}
			while(ei->ld_snd_sfx_res_ct > 0)
			{
				ei->ld_snd_sfx_res_ct--;
				idx = ei->map_snd_sfx[ei->ld_snd_sfx_res_ct];
				if(idx < NULL_IMG_IDX)
				{
					if(--pcd->res_snd[idx]->mResRef == 0)
					{
						sndReleaseResource(pcd->res_snd[idx]);
					}
				}
			}
			while(ei->ld_fnt_res_ct > 0)
			{
				ei->ld_fnt_res_ct--;
				idx = ei->map_fnt[ei->ld_fnt_res_ct];
				if(idx < NULL_IMG_IDX)
				{
					if(--pcd->res_fnt[idx]->mResRef == 0)
					{
						SDK_ASSERT((pcd->res_fnt[idx]->mAlphabet.mpHTable != NULL));
						_releaseFont(pcd->res_fnt[idx]);
					}
				}
			}
			while(ei->ld_img_res_ct > 0)
			{
				ei->ld_img_res_ct--;
				idx = ei->map_img[ei->ld_img_res_ct];
				if(idx < NULL_IMG_IDX)
				{
					SDK_NULL_ASSERT(pcd->res_img[idx]);
					if(--pcd->res_img[idx]->mRef == 0)
					{
						DeleteBMPImage(pcd->res_img[idx]);
						pcd->res_img[idx] = NULL;
					}
				}
			}
		}
	}
	pcd->snd_instance = NULL;
	sndDeleteDataBuffers();
#ifdef USE_OPENGL_RENDER
	glRender_DeleteTextures();
#endif
#ifdef JOBS_IN_SEPARATE_THREAD
	jobCriticalSectionEnd();	
#endif
#if defined SDK_DEBUG && defined FRAME_ALLOCATOR
	OS_Printf("after free resources dmem = %d\n", GetTotalMALLOCSizeDbg());
#endif
}
// ----------------------------------------------------------------------------------

void _releaseFont(struct TERFont *oFont)
{
	SDK_ASSERT(oFont->mpRawData != NULL);
	SDK_ASSERT(oFont->mAlphabet.mpHTable != NULL); //font is not inited
	SDK_ASSERT(oFont->mAlphabet.mpAlphabetTextureOffsetMap != NULL); //font is not inited
	AllocatorHTable_Release(oFont->mAlphabet.mpHTable);
	FREE(oFont->mAlphabet.mpHTable);
	FREE(oFont->mAlphabet.mpHTableAllocator);
	FREE(oFont->mAlphabet.mpHeap);
	FREE(oFont->mAlphabet.mpAlphabetTextureOffsetMap);
	FREE((u8*)oFont->mpRawData);
	oFont->mAlphabet.mAlphabetSize = 0;
	oFont->mAlphabet.mpHTable = NULL;
	oFont->mAlphabet.mpHTableAllocator = NULL;
	oFont->mAlphabet.mpHeap = NULL;
	oFont->mAlphabet.mpAlphabetTextureOffsetMap = NULL;
	oFont->mpAbcImage = NULL;
	oFont->mpRawData = NULL;
	oFont->mpInfo = NULL;
}
// ----------------------------------------------------------------------------------

static void _assignResUpdate(struct TEngineCommonData *pcd)
{
	switch(pcd->agrl_s)
	{
		case 0:
			if(_assignRes_begin(pcd))
			{
				pcd->agrl_s++;	
			}
		break;
		case 1:
			if(_assignRes_load(pcd))
			{
				pcd->agrl_s++;	
			}
		break;
		case 2:
			_assignRes_end();
			pcd->agrl_s++;
		break;
		case 3:
			{
				u32 i = pcd->initParams.layersCount; 
				while (i > 0)
				{
					struct TEngineInstance *ei = pcd->instances[--i];
					if(ei->data_file != NULL && ei->ueof_state != UEOF_READY)
					{
						--ei->ueof_state;
						return;
					}
				}
				pcd->agrl_s = 0;
				pcd->agrl_i = 0;
				pcd->ag_st = FALSE;
				{
					if(pcd->_onEventCB != NULL)
					{
						struct EventCallbackData ed;
						MI_CpuFill8(&ed, 0, sizeof(struct EventCallbackData));
						ed.eventType = EVENT_TYPE_ON_END_ASYNH_LOAD_RESOURSES;
						pcd->_onEventCB(&ed);
					}
				}
			}
	}
}
// ----------------------------------------------------------------------------------

static BOOL _assignRes_begin(struct TEngineCommonData *pcd)
{
	u32 i, sfx_ct, bgm_ct;
#ifdef USE_OPENGL_RENDER
	s32 gltstrm_ct;
#endif
	if(pcd->ag_st == FALSE || pcd->ld_st != ALS_STATE_READY || pcd->instances == NULL)
	{
		return FALSE;
	}
	sfx_ct = 0;
	bgm_ct = 0;
	pcd->snd_instance = NULL;
	for (i = 0; i < pcd->initParams.layersCount; i++)
	{
		struct TEngineInstance *ei = pcd->instances[i]; 
		RENDERFN(ClearFrameBuffer)(ei->bgType);
		if(pcd->instances[i]->ueof_state != UEOF_EMPTY)
		{
			return FALSE;	
		}
		SDK_ASSERT(ei->ld_img_res_ct == 0);
		SDK_ASSERT(ei->ld_fnt_res_ct == 0);
		SDK_ASSERT(ei->ld_snd_sfx_res_ct == 0);
		SDK_ASSERT(ei->ld_snd_bgm_res_ct == 0);
		if(ei->primary && pcd->snd_instance == NULL && (ei->map_snd_sfx_ct != 0 || ei->map_snd_bgm_ct != 0))
		{
			pcd->snd_instance = ei; 
			sfx_ct = ei->map_snd_sfx_ct;
			bgm_ct = ei->map_snd_bgm_ct;
		}
	}
#if defined SDK_DEBUG && defined FRAME_ALLOCATOR
	OS_Printf("assign resources dmem = %d\n", GetTotalMALLOCSizeDbg());
#else
	OS_Printf("assign resources\n");
#endif
#ifdef USE_OPENGL_RENDER
	gltstrm_ct = pcd->strm_obj_ct;	
	gltstrm_ct *= JOB_ANIM_TASK_BUFFER_MAX;
	pcd->glstrmidx = pcd->res_img_ct;
	glRender_CreateTextures(pcd->glstrmidx + gltstrm_ct);
#endif
	if(sfx_ct > 0 || bgm_ct > 0)
	{
		sndCreateDataBuffers(sfx_ct, bgm_ct);
	}
	pcd->agrl_i = (s32)pcd->initParams.layersCount - 1;
	pcd->agrl_strmprid = 0;
	pcd->agrl_strmaid = 0;
	pcd->agrl_strmbfi = 0;
	return TRUE;
}
// ----------------------------------------------------------------------------------

static BOOL _assignRes_load(struct TEngineCommonData *pcd)
{
	if(pcd->agrl_i >= 0)
	{
		for (; pcd->agrl_i >= 0; pcd->agrl_i--)
		{
			struct TEngineInstance *ei = pcd->instances[pcd->agrl_i];
			if(ei->data_file == NULL)
			{
				continue;
			}
			if(ei->ld_img_res_ct < ei->map_img_ct)
			{
				SDK_ASSERT(ei->map_img != NULL);
				for (; ei->ld_img_res_ct < ei->map_img_ct; ei->ld_img_res_ct++)
				{
					s32 idx = ei->map_img[ei->ld_img_res_ct];
					if(idx < NULL_IMG_IDX)
					{
						if (pcd->res_img[idx] == NULL)
						{
							_loadResource(idx, idx, RES_TYPE_IMAGE, 0, (void**)&pcd->res_img[idx], pcd);
							if(pcd->res_img[idx] != NULL)
							{
								pcd->res_img[idx]->mRef++;
								ei->ld_img_res_ct++;
							}
							return FALSE;
						}
						else
						{
							pcd->res_img[idx]->mRef++;
						}
					}
				}
			}
			if(ei->ld_fnt_res_ct < ei->map_fnt_ct)
			{
				SDK_ASSERT(ei->map_fnt != NULL);
				for (; ei->ld_fnt_res_ct < ei->map_fnt_ct; ei->ld_fnt_res_ct++)
				{
					s32 idx = ei->map_fnt[ei->ld_fnt_res_ct];
					if(idx < NULL_IMG_IDX)
					{
						if (pcd->res_fnt[idx]->mpAbcImage == NULL)
						{
							_loadResource(idx, idx, RES_TYPE_FONT, 0, (void**)&pcd->res_fnt[idx], pcd);
							if(pcd->res_fnt[idx]->mpAbcImage != NULL)
							{
								pcd->res_fnt[idx]->mResRef++;
								ei->ld_fnt_res_ct++;
							}
							return FALSE;
						}
						else
						{
							pcd->res_fnt[idx]->mResRef++;
						}
					}
				}
			}
#ifndef USE_NO_SOUND
			if(pcd->snd_instance == ei && ei->ld_snd_sfx_res_ct < ei->map_snd_sfx_ct)
			{
				SDK_ASSERT(ei->map_snd_sfx != NULL);
				for (; ei->ld_snd_sfx_res_ct < ei->map_snd_sfx_ct; ei->ld_snd_sfx_res_ct++)
				{
					s32 idx = ei->map_snd_sfx[ei->ld_snd_sfx_res_ct];
					if(idx < NULL_IMG_IDX)
					{
						if (sndIsInitedResource(pcd->res_snd[idx]	) == FALSE)
						{
							_loadResource(ei->ld_snd_sfx_res_ct, idx, RES_TYPE_SOUND, 1, (void**)&pcd->res_snd[idx], pcd); // 1 == sfx
							if(sndIsInitedResource(pcd->res_snd[idx]) == TRUE)
							{
								pcd->res_snd[idx]->mResRef++;
								ei->ld_snd_sfx_res_ct++;
							}
							return FALSE;
						}
						else
						{
							pcd->res_snd[idx]->mResRef++;
						}
					}
				}
			}
			if(pcd->snd_instance == ei && ei->ld_snd_bgm_res_ct < ei->map_snd_bgm_ct)
			{
				SDK_ASSERT(ei->map_snd_bgm != NULL);
				for (; ei->ld_snd_bgm_res_ct < ei->map_snd_bgm_ct; ei->ld_snd_bgm_res_ct++)
				{
					s32 idx = ei->map_snd_bgm[ei->ld_snd_bgm_res_ct];
					if(idx < NULL_IMG_IDX)
					{
						if (sndIsInitedResource(pcd->res_snd[idx]	) == FALSE)
						{
							_loadResource(ei->ld_snd_bgm_res_ct, idx, RES_TYPE_SOUND, 0, (void**)&pcd->res_snd[idx], pcd); // 0 == bgm
							if(sndIsInitedResource(pcd->res_snd[idx]) == TRUE)
							{
								pcd->res_snd[idx]->mResRef++;
								ei->ld_snd_bgm_res_ct++;
							}
							return FALSE;
						}
						else
						{
							pcd->res_snd[idx]->mResRef++;
						}
					}
				}
			}
#endif
			//mark res_state as soon as possible (loading screens etc)
			if(ei->data_file)
			{
				_calculateLogicSizes(ei, pcd);
				if(ei->primary)
				{
#ifdef USE_OPENGL_RENDER
					RENDERFN(SetupBGLayersData)(ei->bgType, pcd->tl_layers_ct, ei->maxbgres, (ei->d_h + 1) * (ei->d_w + 1));
#endif
				}
				if(pcd->strm_instance != ei)
				{
					RENDERFN(FreezeCurrentFrameBuffer)(FALSE);
					ei->ueof_state = UEOF_SKIP_FRAME1;
					if(pcd->ainst != NULL)
					{
						ei->tcx_fx[ei->bgType] = ei->c_fx.x;
						ei->tcy_fx[ei->bgType] = ei->c_fx.y;
						_calculateCXY8(ei, &ei->tcx_fx[ei->bgType], &ei->tcy_fx[ei->bgType],
											&ei->tc_dx8[ei->bgType], &ei->tc_dy8[ei->bgType]);
					}
				}
			}
		}
	}
#ifdef JOBS_IN_SEPARATE_THREAD
	jobCriticalSectionBegin();	
#endif
	if(pcd->strm_obj_data != NULL && pcd->strm_obj_ct > 0)
	{
		s32 j;		
		for (; pcd->agrl_strmprid < pcd->strm_instance->pr_idata_ct; pcd->agrl_strmprid++)
		{
			for (; pcd->agrl_strmaid < pcd->anim_ct; pcd->agrl_strmaid++)
			{				
				if(pcd->strm_obj_data[pcd->agrl_strmprid][pcd->agrl_strmaid])
				{
					s32 ppr_idx, fr_idx;
					const char *filename = NULL;
					ppr_idx = pcd->strm_instance->pr_idata[pcd->agrl_strmprid][IPARENT_IDX];
					fr_idx = 0;
					while(filename == NULL)
					{
						const s32 pc_count = fx2int(pcd->ppr_fr_data[ppr_idx][pcd->agrl_strmaid][fr_idx][ANIFRFRCOUNT]);
						for(j = 0; j < pc_count; j++)
						{
							const s16* frdata = _getFramePieceData(ppr_idx, pcd->agrl_strmaid, fr_idx, j);
							if(_isStreamFrame(frdata[ANIFRFRDATA]))
							{
								filename = pcd->res_strm_names[frdata[ANIFRFRID]];
								break;
							}
						}
						fr_idx++;
					}
					fr_idx = 0;
					for(; pcd->agrl_strmbfi < JOB_ANIM_TASK_BUFFER_MAX;)
					{
						char fullPath[MAX_FILEPATH];
						struct BMPImage *pImg;
						STD_StrCpy(fullPath, filename);
						STD_StrCat(fullPath, FILERES_EXTENTION);
						LoadBMPImage(fullPath, &pImg, FALSE);
						pcd->strm_obj_data[pcd->agrl_strmprid][pcd->agrl_strmaid]->mpFileData[pcd->agrl_strmbfi] = pImg;
#ifdef USE_OPENGL_RENDER
						glRender_CreateEmptyTexture(pImg, pcd->glstrmidx);
#endif
						pImg->mOpaqType = pcd->glstrmidx;
						pcd->strm_obj_data[pcd->agrl_strmprid][pcd->agrl_strmaid]->mData[pcd->agrl_strmbfi].mBufferStatus = FALSE;
						pcd->strm_obj_data[pcd->agrl_strmprid][pcd->agrl_strmaid]->mData[pcd->agrl_strmbfi].mBindStatus = FALSE;
						pcd->strm_obj_data[pcd->agrl_strmprid][pcd->agrl_strmaid]->mData[pcd->agrl_strmbfi].mFrpcdata[RES_X] = 0;
						pcd->strm_obj_data[pcd->agrl_strmprid][pcd->agrl_strmaid]->mData[pcd->agrl_strmbfi].mFrpcdata[RES_Y] = 0;
						pcd->strm_obj_data[pcd->agrl_strmprid][pcd->agrl_strmaid]->mData[pcd->agrl_strmbfi].mFrpcdata[RES_W] = pImg->mWidth;
						pcd->strm_obj_data[pcd->agrl_strmprid][pcd->agrl_strmaid]->mData[pcd->agrl_strmbfi].mFrpcdata[RES_H] = pImg->mHeight;
						pcd->strm_obj_data[pcd->agrl_strmprid][pcd->agrl_strmaid]->mData[pcd->agrl_strmbfi].mpImage = pImg;
						pcd->strm_obj_data[pcd->agrl_strmprid][pcd->agrl_strmaid]->mpLastDrawnImage = NULL;
						/*if(pcd->strm_obj_data[pcd->agrl_strmprid][pcd->agrl_strmaid]->mData[pcd->agrl_strmbfi].mpFilename != NULL)
						{
							fr_idx = 1;
						}*/
#ifdef USE_OPENGL_RENDER
						pcd->glstrmidx++;
#endif
						pcd->agrl_strmbfi++;
#ifdef JOBS_IN_SEPARATE_THREAD
						jobCriticalSectionEnd();	
#endif
						return FALSE;
					}
					if(fr_idx)
					{
						jobAddStreamVideoTask(pcd->strm_obj_data[pcd->agrl_strmprid][pcd->agrl_strmaid]);
					}
				}
			}
			pcd->agrl_strmaid = 0;
			pcd->agrl_strmbfi = 0;
		}
		RENDERFN(FreezeCurrentFrameBuffer)(FALSE);
		pcd->strm_instance->ueof_state = UEOF_SKIP_FRAME1;
		if(pcd->ainst != NULL)
		{
			pcd->strm_instance->tcx_fx[pcd->strm_instance->bgType] = pcd->strm_instance->c_fx.x;
			pcd->strm_instance->tcy_fx[pcd->strm_instance->bgType] = pcd->strm_instance->c_fx.y;
			_calculateCXY8(pcd->strm_instance, &pcd->strm_instance->tcx_fx[pcd->strm_instance->bgType], &pcd->strm_instance->tcy_fx[pcd->strm_instance->bgType],
										&pcd->strm_instance->tc_dx8[pcd->strm_instance->bgType], &pcd->strm_instance->tc_dy8[pcd->strm_instance->bgType]);
		}
	}
#ifdef JOBS_IN_SEPARATE_THREAD
	jobCriticalSectionEnd();
#endif
	return TRUE;
}
// ----------------------------------------------------------------------------------

static void _assignRes_end()
{
#if defined SDK_DEBUG && defined FRAME_ALLOCATOR
	OS_Printf("end load resources dmem = %d\n", GetTotalMALLOCSizeDbg());
#else
	OS_Printf("end load resources\n");
#endif
}
// ----------------------------------------------------------------------------------

void _getFileName(s32 idx, s32 type, char *iofileName, struct TEngineCommonData *pcd)
{
	switch(type)
	{
		case RES_TYPE_IMAGE:
			STD_StrCpy(iofileName, pcd->res_img_fnames[idx]);
			STD_StrCat(iofileName, FILERES_EXTENTION); 
		break;
		case RES_TYPE_FONT:
			STD_StrCpy(iofileName, pcd->res_fnt_fnames[idx]);
			STD_StrCat(iofileName, FONT_EXTENTION);
		break;
		case RES_TYPE_SOUND:
			STD_StrCpy(iofileName, pcd->res_snd_fnames[idx]);
			STD_StrCat(iofileName, SOUND_EXTENTION);
		break;
		default:
			SDK_ASSERT(0); // wrong type
	}
}
// ----------------------------------------------------------------------------------

void _loadResource(s32 idx, s32 resIdx, s32 type, u8 value, void **oData, struct TEngineCommonData *pcd)
{
	u8 *buf;	
	if(ld_fst.mData.mMode == JFSDM_MODE_NONE)
	{
		enum JFSDMode mode;
		_getFileName(resIdx, type, _gsfileName, pcd);
		switch(type)
		{
			case RES_TYPE_IMAGE:
				mode = JFSDM_MODE_IMG;
			break;
			case RES_TYPE_SOUND:
				mode = JFSDM_MODE_SND;
			break;
			default:
				mode = JFSDM_MODE_FILE;
		}
		_asynhloadFileInit(_gsfileName, mode, (u16)idx, value);
	}
	if(_asynhloadFileCheck(&buf))
	{
		switch(type)
		{
			case RES_TYPE_IMAGE:
			{
#ifdef USE_OPENGL_RENDER
				struct BMPImage* img = (struct BMPImage*)buf;
				SDK_ASSERT(img->mType < BMP_TYPE_NUM);
#endif
				*oData = buf;
#ifdef USE_OPENGL_RENDER
				glRender_CreateEmptyTexture(img, idx);
				glRender_LoadTextureToVRAM(img, idx);
				if(img->mpA5Data != NULL)
				{
					FREE(img->mpA5Data);
					img->mpA5Data = NULL;
				}
				if(img->data.mpData256 != NULL)
				{
					FREE(img->data.mpData256);
					img->data.mpData256 = NULL;
				}
				img->mOpaqType = (u16)idx;
#endif
			}
			break;
			case RES_TYPE_FONT:
				_initFont(buf, (struct TERFont*)*oData, pcd);
			break;
			case RES_TYPE_SOUND:
				sndInitResource(buf, (struct TERSound*)*oData);
			break;
			default:
				SDK_ASSERT(0); // wrong type
		}
	}
}
// ----------------------------------------------------------------------------------

void _initFont(const u8* ipData, struct TERFont *oFont, struct TEngineCommonData *pcd)
{
	s32 intVal, i;
	u32 hsize;
	char resName[MAX_FILENAME];
	SDK_NULL_ASSERT(oFont);
	SDK_NULL_ASSERT(ipData);
	SDK_ASSERT(ipData[0] == 'B' &&
		        ipData[1] == 'M' &&
		        ipData[2] == 'F' &&
				ipData[3] == 3); //Unsupportet font data format
	SDK_ASSERT(oFont->mpRawData == NULL); //font is already assigned
	oFont->mpRawData = ipData;
	ipData += 4;
	//block1
	SDK_ASSERT(*ipData == 1);
	ipData++;
	intVal = _NBytesToInt(ipData, 4, 0);
	ipData += 4;
	oFont->mpInfo = ipData;
	ipData += intVal;
	//block2
	SDK_ASSERT(*ipData == 2);
	ipData++;
	intVal = _NBytesToInt(ipData, 4, 0);
	ipData += 4 + intVal;
	//block3
	SDK_ASSERT(*ipData == 3);
	ipData += 5;
	intVal = 0;
	while(*ipData != '.')
	{
		resName[intVal] = (char)(*ipData);
		intVal++;
		ipData++;
	}
	resName[intVal] = 0;
	ipData += 5;
	//block4
	SDK_ASSERT(*ipData == 4);
	ipData++;
	intVal = _NBytesToInt(ipData, 4, 0);
	ipData += 4;
	SDK_ASSERT(oFont->mAlphabet.mpHTable == NULL); //font is already assigned
	SDK_ASSERT(oFont->mAlphabet.mpAlphabetTextureOffsetMap == NULL); //font is already assigned
	oFont->mAlphabet.mAlphabetSize = (u16)(intVal / 20);
	hsize = AllocatorHTable_CalculateHeapSize(oFont->mAlphabet.mAlphabetSize);
	oFont->mAlphabet.mpAlphabetTextureOffsetMap = (struct AlphabetTextureOffsetMap*)MALLOC(oFont->mAlphabet.mAlphabetSize * sizeof(struct AlphabetTextureOffsetMap), "oFont->mAlphabet.mpAlphabetTextureOffsetMap");
	oFont->mAlphabet.mpHeap = (u8*)MALLOC(hsize, "_initFont:mpHeap");
	oFont->mAlphabet.mpHTableAllocator = (struct StaticAllocator*)MALLOC(sizeof(struct StaticAllocator), "_initFont:mpHTableAllocator");
	StaticAllocator_Init(oFont->mAlphabet.mpHTableAllocator, oFont->mAlphabet.mpHeap, hsize);
	oFont->mAlphabet.mpHTable = (struct AllocatorHTable*)MALLOC(sizeof(struct AllocatorHTable), "_initFont:mpHTable");
	AllocatorHTable_Init(oFont->mAlphabet.mpHTable, oFont->mAlphabet.mAlphabetSize, oFont->mAlphabet.mpHTableAllocator);
	oFont->mpAbcImage = NULL;
	for(i = 0; i < pcd->res_img_ct; i++)
	{
		if(STD_StrCmp(pcd->res_img_fnames[i], resName) == 0)
		{
			oFont->mpAbcImage = pcd->res_img[i];
			break;
		}
	}
	SDK_NULL_ASSERT(oFont->mpAbcImage);
	for(intVal = 0; intVal < oFont->mAlphabet.mAlphabetSize; intVal++)
	{
		const u32 charCode = (wchar)_NBytesToInt(ipData, 4, 0);
		ipData += 4;
		oFont->mAlphabet.mpAlphabetTextureOffsetMap[intVal].mX = (s16)_NBytesToInt(ipData, 2, 0);
		ipData += 2;
		oFont->mAlphabet.mpAlphabetTextureOffsetMap[intVal].mY = (s16)_NBytesToInt(ipData, 2, 0);
		ipData += 2;
		oFont->mAlphabet.mpAlphabetTextureOffsetMap[intVal].mW = (s16)_NBytesToInt(ipData, 2, 0);
		ipData += 2;
		oFont->mAlphabet.mpAlphabetTextureOffsetMap[intVal].mH = (s16)_NBytesToInt(ipData, 2, 0);
		ipData += 2;
		oFont->mAlphabet.mpAlphabetTextureOffsetMap[intVal].mX_off = (s16)_NBytesToInt(ipData, 2, 0);
		ipData += 2;
		oFont->mAlphabet.mpAlphabetTextureOffsetMap[intVal].mY_off = (s16)_NBytesToInt(ipData, 2, 0);
		ipData += 2;
		if (charCode == W_T(' '))
		{
			oFont->mSpaceCharSize = (u16)_NBytesToInt(ipData, 2, 0);
		}
		ipData += 4;
		AllocatorHTable_Push(oFont->mAlphabet.mpHTable, charCode, &oFont->mAlphabet.mpAlphabetTextureOffsetMap[intVal]);
	}
	if(oFont->mAlphabet.mAlphabetSize > 0)
	{
		oFont->mAlphabet.mFixedHeight = Font_GetSize(oFont);
		oFont->mAlphabet.mFixedWidth = 0;
	}
	else
	{
		oFont->mAlphabet.mFixedHeight = 0;
		oFont->mAlphabet.mFixedWidth = 0;
	}
}
// ----------------------------------------------------------------------------------
