#ifndef A_GAME_PAD_H
#define A_GAME_PAD_H

#include "platform.h"

#if defined ANDROID_NDK || defined  WINDOWS_APP || defined  NIX_APP || defined IOS_APP || defined EMSCRIPTEN_APP || defined KOLIBRIOS_APP

#ifdef __cplusplus
extern "C" {
#endif

s32 PAD_Read(void);
void onKeyDown(s32 key);
void onKeyUp(s32 key);

#ifdef __cplusplus
} /* extern "C" */
#endif

#endif
#endif
