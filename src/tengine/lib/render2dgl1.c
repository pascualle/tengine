/*
tengine - cross-platform game engine
------------------------------------

Copyright (c) 2013 Papa Pascualle
Contact Email: pascualle@gmail.com

Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
*/

#ifndef NITRO_SDK

#include "lib/render2dgl1.h"
#include "lib/gx_helpers.h"
#ifdef SDK_DEBUG
#include "lib/tengine_low.h"
#endif

#ifdef USE_OPENGL_1_RENDER

#if defined EMSCRIPTEN_APP
 ------
 error: please use USE_OPENGL_2_RENDER
 ------
#endif

#include "filesystem.h"
#include "loadhelpers.h"
#include "texts.h"
#include "fxmath.h"
#include "lib/jobs_low.h"
#include "static_allocator.h"
#include "containers/allocator_htable.h"
#include "containers/allocator_list.h"
#ifdef USE_EXTERNAL_API
#include "external.h"
#endif

// experimental VBO
#if defined WINDOWS_APP || defined IOS_APP
	#define USE_GL_GLEXT
#endif

//#define USE_FX32
//#define DEBUG_BG_CHUNKS

#ifdef ANDROID_NDK
 #include <GLES/gl.h>
 #ifdef USE_GL_GLEXT
  #include <GLES/glext.h>
 #endif
#else
 #ifdef IOS_APP
  #include "OpenGLES/ES1/gl.h"
  #ifdef USE_GL_GLEXT
   #include "OpenGLES/ES1/glext.h"
  #endif
 #else
  #include "platform.h"
  #include <GL/gl.h>
  #ifdef USE_GL_GLEXT
   #include "gl/glext.h"
  #endif
 #endif
#endif

#ifdef USE_FX32
	#undef FX_FX32_TO_F32
	#undef FX_F32_TO_FX32
	typedef GLfixed GLfx32;
	typedef GLubyte GLcolor;
	#define GL_COORD_TYPE GL_FIXED
	#define GL_TEXCOLOR_TYPE GL_UNSIGNED_BYTE
	#define glOrtho_x glOrthox
	#define glClearColor_x glClearColorx
	#define glColor4_x glColor4x
	#define glPointSize_x glPointSizex
	#define glLineWidth_x glLineWidthx
	#define GX_COLOR_CHANNEL(c) ((u8)(((c) * 0xff) / 0x1f))
	#define FX_FX32_TO_F32(c) ((GLfx32)(c))
	#define FX_F32_TO_FX32(c) ((fx32)(c))
#else
	#define GL_COORD_TYPE GL_FLOAT
	#define GL_TEXCOLOR_TYPE GL_FLOAT
	typedef BOOL EGLboolean;
	typedef GLfloat GLfx32;
	typedef GLfloat GLcolor;
	#define glOrtho_x glOrtho
	#define glClearColor_x glClearColor
	#define glColor4_x glColor4f
	#define glPointSize_x glPointSize
	#define glLineWidth_x glLineWidth
	#define GX_COLOR_CHANNEL(c) ((GLcolor)((GLfx32)(c) / (f32)0x1f))
#endif

#define F32_EPSILON FX_FX32_TO_F32(FX32(0.00001))

#ifdef __BORLANDC__
#pragma warn -8064
#endif

#define DIFD_DEF_FILL_COLOR_FULL_ALPHA ((ALPHA_OPAQ << 16) | DIFD_DEF_FILL_COLOR)

static const s32 NULL_CLIP_RECT = ((((u16)-8192)) << 16) | ((u16)-8192);

enum RGLLineStyle
{
	RGLLSSolid = 0xFFFF,
	RGLLSDot = 0xAAAA,
	RGLLSDash = 0xCCCC
};

enum RGLRenderTypes
{
	RGL_PIXEL = 0,
	RGL_LINE,
	RGL_FILLRECT,
	RGL_IMAGE,
	RGL_EXTERNAL_POS_COLOR
};

struct TRGLColor
{
	GLcolor r;
	GLcolor g;
	GLcolor b;
	GLcolor a;
	GXRgba rgba;
};

enum RGLExDClipIdx
{
	EXDICLIPRECTXY = 0,
	EXDICLIPRECTWH,
	EXDICLIPRECTCOUNT
};

struct RenderColorArrayItem
{
	GLcolor* mpImgColorArrayPtr;
	s32 mColorCt;
	u32 mFillWithColor;
};

struct RenderTexVertexArrayItem
{
	GLfx32* mpImgVertexArrayPtr;
	GLfx32* mpImgTexArrayPtr;
	s32 mVertexCt;
};

struct TRGLRenderItem
{
	struct RenderTexVertexArrayItem tv;
	struct RenderColorArrayItem c;
	const struct BMPImage* mpImageHeader;
	struct TRGLRenderItem* mpNext;
	s32 mClipRect[EXDICLIPRECTCOUNT];
#ifdef USE_GL_GLEXT
	GLuint mVBOBufferIdx;
#endif
	u8 mType;
};

struct TRGLBGItem
{
	struct RenderTexVertexArrayItem tv;
#ifdef USE_TILEMAP_ATTRIBUTES
	struct RenderColorArrayItem c;
#endif
	const struct BMPImage* mpImageHeader;
#ifdef USE_GL_GLEXT
	GLuint mVBOBufferIdx;
#endif
};

static struct RenderPlane
{
	s32 mOffX;
	s32	mOffY;
	s32	mViewWidth;
	s32	mViewHeight;
	s32 mRenderListSize;
	u32 mBGMaxLayers;
	u32 mBGMaxTextures;
	u32 mBGMaxElements;
#ifdef USE_TILEMAP_ATTRIBUTES
	s32* mpBGVertexCt;
	s32* mpBGColorCt;
#endif
	s32 mImgVertexCt;
	s32 mImgColorCt;
	u8* mpBGDataAllocatorHeap;
	struct StaticAllocator mBGDataAllocator;
	struct AllocatorHTable* mpBGTexturesTable;
	struct TRGLRenderItem *mpRenderListPool;
	struct TRGLRenderItem *mpRenderList;
	struct TRGLRenderItem *mpRenderListTail;
#ifdef USE_GL_GLEXT
	GLuint *mpBGVBOBuffer;
	GLuint *mpVBOBuffer;
	GLuint mBGVBOBufferLastIdx;
	GLuint mObjVBOBufferLastIdx;
#endif
	struct TRGLColor mColor;
	GLfx32 *mpImgVertexPool;
	GLfx32 *mpImgTexCoordPool;
	GLcolor *mpImgColorPool;
	GLfx32 *mpBGVertexPool;
	GLfx32 *mpBGTexCoordPool;
#ifdef USE_TILEMAP_ATTRIBUTES
	GLcolor* mpBGColorPool;
#endif
	GLfx32 mScale;
	GLfx32 mLineWidth;
	s32 mDraw;
	s32 mMaxObjOnScene;
}gsPlane[BGSELECT_NUM];

static enum BGSelect gsActivePlane = BGSELECT_NUM;
static BOOL gsGraphicsInit = FALSE;
static s32 gsActiveClipRect[EXDICLIPRECTCOUNT];
static GLuint *mpTexture = NULL;
static GLuint mTextureCount = 0;

static s32 mCurrentTextureID = -1;

static s32 sgScreenWidth = -1;
static s32 sgScreenHeight = -1;
static BOOL gsLostDevice = TRUE;
static BOOL gsFreezeCurrentFrameBuffer = FALSE;
static BOOL gsEnableTexure = FALSE;
static BOOL gsEnableColor = FALSE;
static BOOL sgRenderListReady = FALSE;
#ifdef USE_TILEMAP_ATTRIBUTES
static s32 sgBGChunkSize = 1;
static const s32 gsBGMaxFillColor = 255;
#endif

static BOOL _glRender_CheckForCleanup(enum BGSelect iType);
static void _glRender_AddToRenderList(struct TRGLRenderItem* item);
static void _glRender_CheckError(const char* op);

#ifdef USE_GL_GLEXT
#ifdef WINDOWS_APP
static PFNGLGENBUFFERSPROC glGenBuffers = NULL;
static PFNGLBINDBUFFERPROC glBindBuffer = NULL;
static PFNGLBUFFERDATAPROC glBufferData = NULL;
static PFNGLBUFFERSUBDATAPROC glBufferSubData = NULL;
static PFNGLDELETEBUFFERSPROC glDeleteBuffers = NULL;
#endif
#endif

static GLuint s_disable_caps[] =
{
	GL_FOG,
	GL_LIGHTING,
	GL_CULL_FACE,
	GL_ALPHA_TEST,
	GL_BLEND,
	GL_COLOR_LOGIC_OP,
	GL_DITHER,
	GL_STENCIL_TEST,
	GL_DEPTH_TEST,
	GL_COLOR_MATERIAL,
	GL_LINE_SMOOTH,
	GL_POINT_SMOOTH,
	0
};

#ifdef SDK_DEBUG
extern struct TEngineCommonData cd;
#endif

//----------------------------------------------------------------------------------
//----------------------------------------------------------------------------------
//----------------------------------------------------------------------------------

void _glRender_CheckError(const char* op)
{
#ifdef SDK_DEBUG
	GLenum error = glGetError();
	if (error != GL_NO_ERROR)
	{
		OS_Warning("after %s() glError (0x%x)\n", op, error);
	}
#else
	(void)op;
#endif
}
//----------------------------------------------------------------------------------

void glRender_Init()
{
	s32 type;
	for(type = 0; type < BGSELECT_NUM; type++)
	{
		MI_CpuFill8(&gsPlane[type], 0, sizeof(struct RenderPlane));
		gsPlane[type].mScale = FX_FX32_TO_F32(FX32_ONE);
		gsPlane[type].mLineWidth = FX_FX32_TO_F32(FX32_ONE);
	}
	gsGraphicsInit = TRUE;
	sgScreenWidth = 0;
	sgScreenHeight = 0;
	mCurrentTextureID = -1;
	mTextureCount = 0;
	gsActiveClipRect[EXDICLIPRECTXY] = NULL_CLIP_RECT;
	gsActiveClipRect[EXDICLIPRECTWH] = NULL_CLIP_RECT;
}
//----------------------------------------------------------------------------------

void glRender_Release()
{
	s32 type;
	SDK_ASSERT(mTextureCount == 0); // please call glRender_DeleteTextures() before
	for(type = 0; type < BGSELECT_NUM; type++)
	{
		gsPlane[type].mScale = FX_FX32_TO_F32(FX32_ONE);
		gsPlane[type].mLineWidth = FX_FX32_TO_F32(FX32_ONE);
	}
	gsGraphicsInit = FALSE;
}
//----------------------------------------------------------------------------------

void glRender_LostDevice(void)
{
	gsLostDevice = TRUE;
}
//----------------------------------------------------------------------------------

void glRender_RestoreDevice(void)
{
    GLuint *start = s_disable_caps;

	gsLostDevice = FALSE;

#if defined USE_GL_GLEXT && defined WINDOWS_APP
	glGenBuffers = (PFNGLGENBUFFERSPROC)wglGetProcAddress("glGenBuffers");
	SDK_NULL_ASSERT(glGenBuffers); // unsupportet extention, please switch off USE_GL_GLEXT in crossgl.h
	glBindBuffer = (PFNGLBINDBUFFERPROC)wglGetProcAddress("glBindBuffer");
	SDK_NULL_ASSERT(glBindBuffer);
	glBufferData = (PFNGLBUFFERDATAPROC)wglGetProcAddress("glBufferData");
	SDK_NULL_ASSERT(glBufferData);
	glBufferSubData = (PFNGLBUFFERSUBDATAPROC)wglGetProcAddress("glBufferSubData");
	SDK_NULL_ASSERT(glBufferSubData);
	glDeleteBuffers = (PFNGLDELETEBUFFERSPROC)wglGetProcAddress("glDeleteBuffers");
	SDK_NULL_ASSERT(glDeleteBuffers);
#endif

    gsEnableTexure = FALSE;
    gsEnableColor = FALSE;
    glEnableClientState(GL_VERTEX_ARRAY);
    glDisableClientState(GL_COLOR_ARRAY);
    glDisableClientState(GL_TEXTURE_COORD_ARRAY);
    
#ifdef SDK_DEBUG
    glClearColor_x(FX_FX32_TO_F32(FX_F32_TO_FX32(0.5f)), FX_FX32_TO_F32(FX_F32_TO_FX32(0.0f)), FX_FX32_TO_F32(FX_F32_TO_FX32(0.5f)), FX_FX32_TO_F32(FX_F32_TO_FX32(1.0f)));
#else
    glClearColor_x(0, 0, 0, FX_FX32_TO_F32(FX_F32_TO_FX32(1.0f)));
#endif

    while(*start)
    {
        glDisable(*start++);
        _glRender_CheckError("glDisable");
    }
    glHint(GL_POINT_SMOOTH_HINT, GL_FASTEST);
	glHint(GL_LINE_SMOOTH_HINT, GL_FASTEST);
    _glRender_CheckError("glHint");

	mCurrentTextureID = -1;
}
//----------------------------------------------------------------------------------

void glRender_SetRenderPlaneScale(fx32 val, enum BGSelect iBG)
{
	SDK_ASSERT(val > 0);
	SDK_ASSERT(iBG != BGSELECT_NUM);
	gsPlane[iBG].mScale = FX_FX32_TO_F32(val);
	gsPlane[iBG].mLineWidth = FX_FX32_TO_F32(FX32_ONE); 
	while(gsPlane[iBG].mScale > gsPlane[iBG].mLineWidth)
	{
		gsPlane[iBG].mLineWidth += FX_FX32_TO_F32(FX32_ONE);
	}
}
//----------------------------------------------------------------------------------

fx32 glRender_GetRenderPlaneScale(enum BGSelect iBG)
{
	SDK_ASSERT(iBG != BGSELECT_NUM);
	return FX_F32_TO_FX32(gsPlane[iBG].mScale);
}
//----------------------------------------------------------------------------------

void glRender_Resize(s32 w, s32 h)
{
	sgScreenWidth = w;
	sgScreenHeight = h;
	if(gsLostDevice == FALSE)
	{
        glRender_RestoreDevice();
	}
}
//----------------------------------------------------------------------------------

void glRender_DrawFrame()
{
	BOOL clear;
	s32 plane;

	SDK_ASSERT(sgScreenWidth > 0);
	SDK_ASSERT(gsGraphicsInit == TRUE); // please init graphics system before

	clear = FALSE;

	glPushMatrix();

	for(plane = 0; plane < BGSELECT_NUM; plane++)
	{
		if(gsPlane[plane].mpImgVertexPool != NULL && (gsPlane[plane].mDraw == 1 || gsPlane[plane].mDraw == 2))
		{
			++gsPlane[plane].mDraw;

			if(clear == FALSE)
			{
				if(!glIsEnabled(GL_BLEND))
				{
					glEnable(GL_BLEND);
					glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
				}
				glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
				clear = TRUE;
			}

			glViewport((GLint)(gsPlane[plane].mOffX),
						sgScreenHeight - 
						(GLint)(gsPlane[plane].mViewHeight) - 
						(GLint)(gsPlane[plane].mOffY),
						(GLint)(gsPlane[plane].mViewWidth),
						(GLint)(gsPlane[plane].mViewHeight));
			glMatrixMode(GL_PROJECTION);
			glLoadIdentity();
			glOrtho_x(0, 
						FX_FX32_TO_F32(FX_Div(FX32(gsPlane[plane].mViewWidth), FX_F32_TO_FX32(gsPlane[plane].mScale))),
						FX_FX32_TO_F32(FX_Div(FX32(gsPlane[plane].mViewHeight), FX_F32_TO_FX32(gsPlane[plane].mScale))),
						0, -FX_FX32_TO_F32(FX32_ONE), 0);
			glMatrixMode(GL_MODELVIEW);
			glLoadIdentity();

			if(gsPlane[plane].mBGMaxLayers > 0)
			{
				u32 j;
				if(gsEnableTexure == FALSE)
				{
					glEnable(GL_TEXTURE_2D);
					glEnableClientState(GL_TEXTURE_COORD_ARRAY);
					gsEnableTexure = TRUE;
				}
#ifndef USE_TILEMAP_ATTRIBUTES
				if (gsEnableColor == TRUE)
				{
					glDisableClientState(GL_COLOR_ARRAY);
					gsEnableColor = FALSE;
					glColor4_x(FX_FX32_TO_F32(FX32_ONE),
						FX_FX32_TO_F32(FX32_ONE),
						FX_FX32_TO_F32(FX32_ONE),
						FX_FX32_TO_F32(FX32_ONE));
				}
#endif
				for(j = 0; j < gsPlane[plane].mBGMaxLayers; j++)
				{
					const struct AllocatorHTable* ht = &gsPlane[plane].mpBGTexturesTable[j];
					const struct HTableItem* it = AllocatorHTable_Begin_Const(ht);
					while (it != AllocatorHTable_End_Const(ht))
					{
#ifdef USE_TILEMAP_ATTRIBUTES
						const struct AllocatorHTable* ot = (const struct AllocatorHTable*)AllocatorHTable_Val(ht, it);
						const struct HTableItem* oit = AllocatorHTable_Begin_Const(ot);
						while (oit != AllocatorHTable_End_Const(ot))
						{
							const struct AllocatorList* l = (const struct AllocatorList*)AllocatorHTable_Val(ot, oit);
							const struct ListItem* lit = AllocatorList_Begin_Const(l);
							while (lit != AllocatorList_End_Const(l))
							{
								const struct TRGLBGItem* item = (const struct TRGLBGItem*)AllocatorList_Val(l, lit);
#else
								const struct TRGLBGItem* item = (const struct TRGLBGItem*)AllocatorHTable_Val(ht, it);
#endif
								SDK_ASSERT(item->tv.mVertexCt > 0);
								
								if (item->mpImageHeader->mOpaqType != mCurrentTextureID)
								{
									mCurrentTextureID = item->mpImageHeader->mOpaqType;
									SDK_NULL_ASSERT(mpTexture);
									glBindTexture(GL_TEXTURE_2D, mpTexture[mCurrentTextureID]);
								}
#ifdef USE_TILEMAP_ATTRIBUTES
#ifdef DEBUG_BG_CHUNKS
								if (item->tv.mVertexCt > 0)
#else
								if (item->c.mFillWithColor != DIFD_DEF_FILL_COLOR_FULL_ALPHA)
#endif
								{
									if (gsEnableColor == FALSE)
									{
										glEnableClientState(GL_COLOR_ARRAY);
										gsEnableColor = TRUE;
									}
#ifdef USE_GL_GLEXT
									glBindBuffer(GL_ARRAY_BUFFER, gsPlane[plane].mpBGVBOBuffer[item->mVBOBufferIdx + 2]);
									glBufferData(GL_ARRAY_BUFFER, item->c.mColorCt * sizeof(GLcolor), item->c.mpImgColorArrayPtr, GL_DYNAMIC_DRAW);
									glColorPointer(4, GL_TEXCOLOR_TYPE, 0, NULL);
#else
									glColorPointer(4, GL_TEXCOLOR_TYPE, 0, curr->c.mpImgColorArrayPtr);
#endif
								}
								else
								{
									if (gsEnableColor == TRUE)
									{
										glDisableClientState(GL_COLOR_ARRAY);
										gsEnableColor = FALSE;
									}
									glColor4_x(FX_FX32_TO_F32(FX32_ONE),
										FX_FX32_TO_F32(FX32_ONE),
										FX_FX32_TO_F32(FX32_ONE),
										FX_FX32_TO_F32(FX32_ONE));
								}
#endif
#ifdef USE_GL_GLEXT
								glBindBuffer(GL_ARRAY_BUFFER, gsPlane[plane].mpBGVBOBuffer[item->mVBOBufferIdx + 1]);
								glBufferData(GL_ARRAY_BUFFER, item->tv.mVertexCt * sizeof(GLfx32), item->tv.mpImgTexArrayPtr, GL_DYNAMIC_DRAW);
								glTexCoordPointer(2, GL_COORD_TYPE, 0, NULL);

								glBindBuffer(GL_ARRAY_BUFFER, gsPlane[plane].mpBGVBOBuffer[item->mVBOBufferIdx]);
								glBufferData(GL_ARRAY_BUFFER, item->tv.mVertexCt * sizeof(GLfx32), item->tv.mpImgVertexArrayPtr, GL_DYNAMIC_DRAW);
								glVertexPointer(2, GL_COORD_TYPE, 0, NULL);

								glDrawArrays(GL_TRIANGLE_STRIP, 0, item->tv.mVertexCt / 2);
#else
								glTexCoordPointer(2, GL_COORD_TYPE, 0, item->tv.mpImgTexArrayPtr);
								glVertexPointer(2, GL_COORD_TYPE, 0, item->tv.mpImgVertexArrayPtr);
								glDrawArrays(GL_TRIANGLE_STRIP, 0, item->tv.mVertexCt / 2);
#endif

#ifdef USE_TILEMAP_ATTRIBUTES
								lit = AllocatorList_Next_Const(l, lit);
							}
							oit = AllocatorHTable_Next_Const(ot, oit);
						}
#endif
						it = AllocatorHTable_Next_Const(ht, it);
					}
				}
			}

			if(gsPlane[plane].mRenderListSize >= 0 && sgRenderListReady == TRUE)
			{
				const struct TRGLRenderItem* curr = gsPlane[plane].mpRenderList;
				while(curr != NULL)
				{
					SDK_ASSERT(gsPlane[plane].mpRenderListTail->mpNext == NULL);
					if(curr->tv.mVertexCt <= 0)
					{
						curr = curr->mpNext;
						continue;
					}

					switch(curr->mType)
					{
						case RGL_IMAGE:
							if(mTextureCount > 0)
							{
#ifdef JOBS_IN_SEPARATE_THREAD
								jobCriticalSectionBegin();	
#endif
								if(gsEnableTexure == FALSE)
								{
									glEnable(GL_TEXTURE_2D);
									glEnableClientState(GL_TEXTURE_COORD_ARRAY);
									gsEnableTexure = TRUE;
								}
								if(curr->mpImageHeader->mOpaqType != mCurrentTextureID)
								{
									mCurrentTextureID = curr->mpImageHeader->mOpaqType;
									SDK_NULL_ASSERT(mpTexture);
									glBindTexture(GL_TEXTURE_2D, mpTexture[mCurrentTextureID]);
								}
								if(curr->mClipRect[EXDICLIPRECTXY] != NULL_CLIP_RECT)
								{
									GLint scsrx, scsry;
									scsrx = (GLint)(((gsPlane[plane].mOffX + (s16)((curr->mClipRect[EXDICLIPRECTXY] >> 16) & 0xffff)) * gsPlane[plane].mScale) / FX_FX32_TO_F32(FX32_ONE));
									scsry = sgScreenHeight -
										(GLint)(((curr->mClipRect[EXDICLIPRECTWH] & 0xffff) * gsPlane[plane].mScale) / FX_FX32_TO_F32(FX32_ONE)) -
										(GLint)(((gsPlane[plane].mOffY + (s16)(curr->mClipRect[EXDICLIPRECTXY] & 0xffff)) * gsPlane[plane].mScale) / FX_FX32_TO_F32(FX32_ONE));
									glEnable(GL_SCISSOR_TEST);
									glScissor(scsrx, scsry,
										(GLint)((((curr->mClipRect[EXDICLIPRECTWH] >> 16) & 0xffff) * gsPlane[plane].mScale) / FX_FX32_TO_F32(FX32_ONE)),
												(GLint)(((curr->mClipRect[EXDICLIPRECTWH] & 0xffff) * gsPlane[plane].mScale) / FX_FX32_TO_F32(FX32_ONE)));
								}
								
								if(curr->c.mFillWithColor != DIFD_DEF_FILL_COLOR_FULL_ALPHA)
								{
									if(gsEnableColor == FALSE)
									{
										glEnableClientState(GL_COLOR_ARRAY);
										gsEnableColor = TRUE;
									}
#ifdef USE_GL_GLEXT
									glBindBuffer(GL_ARRAY_BUFFER, gsPlane[plane].mpVBOBuffer[curr->mVBOBufferIdx + 2]);
									if(curr->tv.mVertexCt > 0)
									{
										glBufferData(GL_ARRAY_BUFFER, curr->c.mColorCt * sizeof(GLcolor), curr->c.mpImgColorArrayPtr, GL_DYNAMIC_DRAW);
									}
									glColorPointer(4, GL_TEXCOLOR_TYPE, 0, NULL);
#else
									glColorPointer(4, GL_TEXCOLOR_TYPE, 0, curr->c.mpImgColorArrayPtr);
#endif
								}
								else
								{
									if(gsEnableColor == TRUE)
									{
										glDisableClientState(GL_COLOR_ARRAY);
										gsEnableColor = FALSE;
									}
									glColor4_x(FX_FX32_TO_F32(FX32_ONE),
													FX_FX32_TO_F32(FX32_ONE),
													FX_FX32_TO_F32(FX32_ONE),
													FX_FX32_TO_F32(FX32_ONE));
								}
#ifdef USE_GL_GLEXT
								glBindBuffer(GL_ARRAY_BUFFER, gsPlane[plane].mpVBOBuffer[curr->mVBOBufferIdx + 1]);
								glBufferData(GL_ARRAY_BUFFER, curr->tv.mVertexCt * sizeof(GLfx32), curr->tv.mpImgTexArrayPtr, GL_DYNAMIC_DRAW);
								glTexCoordPointer(2, GL_COORD_TYPE, 0, NULL);
								glBindBuffer(GL_ARRAY_BUFFER, gsPlane[plane].mpVBOBuffer[curr->mVBOBufferIdx]);
								glBufferData(GL_ARRAY_BUFFER, curr->tv.mVertexCt * sizeof(GLfx32), curr->tv.mpImgVertexArrayPtr, GL_DYNAMIC_DRAW);
								glVertexPointer(2, GL_COORD_TYPE, 0, NULL);
								glDrawArrays(GL_TRIANGLE_STRIP, 0, curr->tv.mVertexCt / 2);
#else
								glTexCoordPointer(2, GL_COORD_TYPE, 0, curr->tv.mpImgTexArrayPtr);
								glVertexPointer(2, GL_COORD_TYPE, 0, curr->tv.mpImgVertexArrayPtr);
								glDrawArrays(GL_TRIANGLE_STRIP, 0, curr->tv.mVertexCt / 2);
#endif
								if(curr->mClipRect[EXDICLIPRECTXY] != NULL_CLIP_RECT)
								{
									glDisable(GL_SCISSOR_TEST);
								}
#ifdef JOBS_IN_SEPARATE_THREAD
								jobCriticalSectionEnd();	
#endif
							}
							break;

						case RGL_LINE:
							if(gsEnableTexure == TRUE)
							{
								glDisable(GL_TEXTURE_2D);
								glDisableClientState(GL_TEXTURE_COORD_ARRAY);
								gsEnableTexure = FALSE;
							}
							if(gsEnableColor == FALSE)
							{
								glEnableClientState(GL_COLOR_ARRAY);
								gsEnableColor = TRUE;
							}
							glLineWidth_x(gsPlane[gsActivePlane].mLineWidth);
#ifdef USE_GL_GLEXT
							glBindBuffer(GL_ARRAY_BUFFER, gsPlane[plane].mpVBOBuffer[curr->mVBOBufferIdx + 2]);
							glBufferData(GL_ARRAY_BUFFER, curr->c.mColorCt * sizeof(GLcolor), curr->c.mpImgColorArrayPtr, GL_DYNAMIC_DRAW);
							glColorPointer(4, GL_TEXCOLOR_TYPE, 0, NULL);
							glBindBuffer(GL_ARRAY_BUFFER, gsPlane[plane].mpVBOBuffer[curr->mVBOBufferIdx]);
							glBufferData(GL_ARRAY_BUFFER, curr->tv.mVertexCt * sizeof(GLfx32), curr->tv.mpImgVertexArrayPtr, GL_DYNAMIC_DRAW);
							glVertexPointer(2, GL_COORD_TYPE, 0, NULL);
							glDrawArrays(GL_LINES, 0, curr->tv.mVertexCt / 2);
#else
							glColorPointer(4, GL_TEXCOLOR_TYPE, 0, curr->c.mpImgColorArrayPtr);
							glVertexPointer(2, GL_COORD_TYPE, 0, curr->tv.mpImgVertexArrayPtr);
							glDrawArrays(GL_LINES, 0, curr->tv.mVertexCt / 2);
#endif
						break;
						
						case RGL_FILLRECT:
							if(gsEnableTexure == TRUE)
							{
								glDisable(GL_TEXTURE_2D);
								glDisableClientState(GL_TEXTURE_COORD_ARRAY);
								gsEnableTexure = FALSE;
							}
							if(gsEnableColor == FALSE)
							{
								glEnableClientState(GL_COLOR_ARRAY);
								gsEnableColor = TRUE;
							}
#ifdef USE_GL_GLEXT
							glBindBuffer(GL_ARRAY_BUFFER, gsPlane[plane].mpVBOBuffer[curr->mVBOBufferIdx + 2]);
							glBufferData(GL_ARRAY_BUFFER, curr->c.mColorCt * sizeof(GLcolor), curr->c.mpImgColorArrayPtr, GL_DYNAMIC_DRAW);
							glColorPointer(4, GL_TEXCOLOR_TYPE, 0, NULL);
							glBindBuffer(GL_ARRAY_BUFFER, gsPlane[plane].mpVBOBuffer[curr->mVBOBufferIdx]);
							glBufferData(GL_ARRAY_BUFFER, curr->tv.mVertexCt * sizeof(GLfx32), curr->tv.mpImgVertexArrayPtr, GL_DYNAMIC_DRAW);
							glVertexPointer(2, GL_COORD_TYPE, 0, NULL);
							glDrawArrays(GL_TRIANGLE_STRIP, 0, curr->tv.mVertexCt / 2);
#else
							glColorPointer(4, GL_TEXCOLOR_TYPE, 0, curr->c.mpImgColorArrayPtr);
							glVertexPointer(2, GL_COORD_TYPE, 0, curr->tv.mpImgVertexArrayPtr);
							glDrawArrays(GL_TRIANGLE_STRIP, 0, curr->tv.mVertexCt / 2);
#endif
						break;

						case RGL_EXTERNAL_POS_COLOR:
							if(curr->mClipRect[EXDICLIPRECTXY] != NULL_CLIP_RECT)
							{
								GLint scsrx, scsry;
								scsrx = (GLint)(((gsPlane[plane].mOffX + (s16)((curr->mClipRect[EXDICLIPRECTXY] >> 16) & 0xffff)) * gsPlane[plane].mScale) / FX_FX32_TO_F32(FX32_ONE));
								scsry = sgScreenHeight -
									(GLint)(((curr->mClipRect[EXDICLIPRECTWH] & 0xffff) * gsPlane[plane].mScale) / FX_FX32_TO_F32(FX32_ONE)) -
									(GLint)(((gsPlane[plane].mOffY + (s16)(curr->mClipRect[EXDICLIPRECTXY] & 0xffff)) * gsPlane[plane].mScale) / FX_FX32_TO_F32(FX32_ONE));
								glEnable(GL_SCISSOR_TEST);
								glScissor(scsrx, scsry,
									(GLint)((((curr->mClipRect[EXDICLIPRECTWH] >> 16) & 0xffff) * gsPlane[plane].mScale) / FX_FX32_TO_F32(FX32_ONE)),
									(GLint)(((curr->mClipRect[EXDICLIPRECTWH] & 0xffff) * gsPlane[plane].mScale) / FX_FX32_TO_F32(FX32_ONE)));
							}

							if(gsEnableTexure == TRUE)
							{
								glDisable(GL_TEXTURE_2D);
								glDisableClientState(GL_TEXTURE_COORD_ARRAY);
								gsEnableTexure = FALSE;
							}
							if(gsEnableColor == FALSE)
							{
								glEnableClientState(GL_COLOR_ARRAY);
								gsEnableColor = TRUE;
							}
#ifdef USE_GL_GLEXT
							glBindBuffer(GL_ARRAY_BUFFER, gsPlane[plane].mpVBOBuffer[curr->mVBOBufferIdx + 2]);
							glBufferData(GL_ARRAY_BUFFER, curr->c.mColorCt * sizeof(GLcolor), curr->c.mpImgColorArrayPtr, GL_DYNAMIC_DRAW);
							glColorPointer(4, GL_TEXCOLOR_TYPE, 0, NULL);
							glBindBuffer(GL_ARRAY_BUFFER, gsPlane[plane].mpVBOBuffer[curr->mVBOBufferIdx]);
							glBufferData(GL_ARRAY_BUFFER, curr->tv.mVertexCt * sizeof(GLfx32), curr->tv.mpImgVertexArrayPtr, GL_DYNAMIC_DRAW);
							glVertexPointer(2, GL_COORD_TYPE, 0, NULL);
							glDrawArrays(GL_TRIANGLES, 0, curr->tv.mVertexCt / 2);
#else
							glColorPointer(4, GL_TEXCOLOR_TYPE, 0, curr->c.mpImgColorArrayPtr);
							glVertexPointer(2, GL_COORD_TYPE, 0, curr->tv.mpImgVertexArrayPtr);
							glDrawArrays(GL_TRIANGLES, 0, curr->tv.mVertexCt / 2);
#endif
							if(curr->mClipRect[EXDICLIPRECTXY] != NULL_CLIP_RECT)
							{
								glDisable(GL_SCISSOR_TEST);
							}
							break;

						default:
							SDK_ASSERT(0);// error sortList member type;

					}
					curr = curr->mpNext;
				}
			}
		}
	}

	glPopMatrix();
}
//----------------------------------------------------------------------------------

BOOL glRender_IsGraphicsInit(void)
{
	return gsGraphicsInit;
}
//----------------------------------------------------------------------------------

void glRender_PlaneInit(const struct RenderPlaneInitParams* ipParams)
{
    if(ipParams == NULL)
    {
        SDK_ASSERT(0);
        return;
    }

	SDK_ASSERT(ipParams->mBGType != BGSELECT_NUM);

	gsPlane[ipParams->mBGType].mMaxObjOnScene = (s32)ipParams->mMaxRenderObjectsOnPlane;

	gsPlane[ipParams->mBGType].mpRenderListPool = (struct TRGLRenderItem *)MALLOC(gsPlane[ipParams->mBGType].mMaxObjOnScene * sizeof(struct TRGLRenderItem), "PlaneInit::mpRenderListPool");
	gsPlane[ipParams->mBGType].mpImgVertexPool = (GLfx32*)MALLOC(gsPlane[ipParams->mBGType].mMaxObjOnScene * 12 * sizeof(GLfx32), "PlaneInit::mpImgVertexPool"); 
	gsPlane[ipParams->mBGType].mpImgTexCoordPool = (GLfx32*)MALLOC(gsPlane[ipParams->mBGType].mMaxObjOnScene * 12 * sizeof(GLfx32), "PlaneInit::mpImgTexCoordPool"); 
	gsPlane[ipParams->mBGType].mpImgColorPool = (GLcolor*)MALLOC(gsPlane[ipParams->mBGType].mMaxObjOnScene * 24 * sizeof(GLcolor), "PlaneInit::mpImgColorPool");

	gsPlane[ipParams->mBGType].mColor.r = 0;
	gsPlane[ipParams->mBGType].mColor.g = 0;
	gsPlane[ipParams->mBGType].mColor.b = 0;
	gsPlane[ipParams->mBGType].mColor.a = 0;
	gsPlane[ipParams->mBGType].mOffX = ipParams->mX; 
	gsPlane[ipParams->mBGType].mOffY = ipParams->mY;
	gsPlane[ipParams->mBGType].mViewWidth = ipParams->mSizes.mViewWidth;
	gsPlane[ipParams->mBGType].mViewHeight = ipParams->mSizes.mViewHeight;

	SDK_ASSERT(gsPlane[ipParams->mBGType].mpBGTexCoordPool == NULL);
	SDK_ASSERT(gsPlane[ipParams->mBGType].mpBGVertexPool == NULL);
	glRender_ClearFrameBuffer(ipParams->mBGType);
}
//----------------------------------------------------------------------------------

BOOL glRender_IsRenderPlaneInit(enum BGSelect iType)
{
	SDK_ASSERT(iType < BGSELECT_NUM);
	return gsGraphicsInit && gsPlane[iType].mpImgVertexPool != NULL;
}
//----------------------------------------------------------------------------------

void glRender_PlaneResize(enum BGSelect iType, const struct RenderPlaneSizeParams* ipParams)
{
	SDK_ASSERT(iType < BGSELECT_NUM);
	gsPlane[iType].mViewWidth  = ipParams->mViewWidth;
	gsPlane[iType].mViewHeight = ipParams->mViewHeight;
}
//----------------------------------------------------------------------------------

void glRender_PlaneRelease(enum BGSelect iType)
{
	SDK_ASSERT(iType < BGSELECT_NUM);
	if(gsPlane[iType].mpImgVertexPool != NULL)
	{
		FREE(gsPlane[iType].mpImgColorPool);
		FREE(gsPlane[iType].mpImgTexCoordPool);
		FREE(gsPlane[iType].mpImgVertexPool);
		FREE(gsPlane[iType].mpRenderListPool);
		gsPlane[iType].mpImgColorPool = NULL;
		gsPlane[iType].mpImgTexCoordPool = NULL;
		gsPlane[iType].mpImgVertexPool = NULL;
		gsPlane[iType].mpRenderListPool = NULL;
	}
}
//----------------------------------------------------------------------------------

void glRender_SetupBGLayersData(enum BGSelect bgType, u32 maxLayers, u32 maxTextures, u32 maxElements)
{
	u32 i;
	SDK_ASSERT(bgType < BGSELECT_NUM);
	SDK_ASSERT(gsGraphicsInit);
	for (i = 0; i < gsPlane[bgType].mBGMaxLayers; i++)
	{
		SDK_ASSERT(gsPlane[bgType].mpBGVertexPool == NULL); //please call glRender_ReleaseBGLayersData before
		SDK_ASSERT(gsPlane[bgType].mpBGTexCoordPool == NULL);
	}
	
	maxLayers = gsPlane[bgType].mBGMaxLayers > maxLayers ? gsPlane[bgType].mBGMaxLayers : maxLayers;
	maxTextures = gsPlane[bgType].mBGMaxTextures > maxTextures ? gsPlane[bgType].mBGMaxTextures : maxTextures;
	maxElements = gsPlane[bgType].mBGMaxElements > maxElements ? gsPlane[bgType].mBGMaxElements : maxElements; 

	if(maxLayers)
	{
#ifdef USE_TILEMAP_ATTRIBUTES
		i = AllocatorHTable_CalculateHeapSize(gsBGMaxFillColor) * maxLayers * maxTextures + 
			StaticAllocator_CalculateHeapSize(sizeof(struct AllocatorHTable)) * maxTextures +
			StaticAllocator_CalculateHeapSize(sizeof(struct AllocatorList)) * maxElements +
			AllocatorList_CalculateHeapSize(sizeof(struct TRGLBGItem), maxElements) * maxLayers;
		gsPlane[bgType].mpBGDataAllocatorHeap = (u8*)MALLOC(i, "SetupBGLayersData:mpBGDataAllocatorHeap");
		StaticAllocator_Init(&gsPlane[bgType].mBGDataAllocator, gsPlane[bgType].mpBGDataAllocatorHeap, i);
#else
		i = AllocatorHTable_CalculateHeapSize(maxTextures) * maxLayers + StaticAllocator_CalculateHeapSize(sizeof(struct TRGLBGItem)) * maxTextures * maxLayers;
		gsPlane[bgType].mpBGDataAllocatorHeap = (u8*)MALLOC(i, "SetupBGLayersData:mpBGDataAllocatorHeap");
		StaticAllocator_Init(&gsPlane[bgType].mBGDataAllocator, gsPlane[bgType].mpBGDataAllocatorHeap, i);
#endif

#ifdef USE_GL_GLEXT
		SDK_ASSERT(gsPlane[bgType].mpBGVBOBuffer == NULL); //please call glRender_ReleaseBGLayersData before
#ifdef USE_TILEMAP_ATTRIBUTES
		gsPlane[bgType].mpBGVBOBuffer = (GLuint*)MALLOC(maxLayers * maxElements * maxTextures * sizeof(GLuint) * 3, "SetupBGLayersData:mpBGVBOBuffer");
		glGenBuffers(maxLayers * maxElements * maxTextures * 3, gsPlane[bgType].mpBGVBOBuffer);
#else
		gsPlane[bgType].mpBGVBOBuffer = (GLuint*)MALLOC(maxLayers * (maxTextures * sizeof(GLuint) * 2), "SetupBGLayersData:mpBGVBOBuffer");
		glGenBuffers(maxLayers * (maxTextures * 2), gsPlane[bgType].mpBGVBOBuffer);
#endif
#endif
#ifdef USE_TILEMAP_ATTRIBUTES
		sgBGChunkSize = mthSqrtI(maxElements) / 6;
		if (sgBGChunkSize < 16)
		{
			sgBGChunkSize = 16;
		}
		i = maxLayers * maxElements * sgBGChunkSize;
		gsPlane[bgType].mpBGVertexPool = (GLfx32*)MALLOC(i * 12 * sizeof(GLfx32), "SetupBGLayersData:mpBGVertexPool");
		gsPlane[bgType].mpBGTexCoordPool = (GLfx32*)MALLOC(i * 12 * sizeof(GLfx32), "SetupBGLayersData:mpBGTexCoordPool");
		gsPlane[bgType].mpBGColorPool = (GLcolor*)MALLOC(i * 24 * sizeof(GLcolor), "SetupBGLayersData::mpBGColorPool");
		gsPlane[bgType].mpBGVertexCt = (s32*)MALLOC(maxLayers * sizeof(s32), "SetupBGLayersData::mpBGVertexCt");
		gsPlane[bgType].mpBGColorCt = (s32*)MALLOC(maxLayers * sizeof(s32), "SetupBGLayersData::mpBGColorCt");
#else
		i = maxLayers * (12 * maxElements) * maxTextures * sizeof(GLfx32);
		gsPlane[bgType].mpBGVertexPool = (GLfx32*)MALLOC(i, "SetupBGLayersData:mpBGVertexPool");
		gsPlane[bgType].mpBGTexCoordPool = (GLfx32*)MALLOC(i, "SetupBGLayersData:mpBGTexCoordPool");
#endif
		gsPlane[bgType].mpBGTexturesTable = (struct AllocatorHTable*)MALLOC(sizeof(struct AllocatorHTable) * maxLayers, "SetupBGLayersData:mpBGTexturesTable");
	}

	for(i = 0; i < maxLayers; i++)
	{
		AllocatorHTable_Init(&gsPlane[bgType].mpBGTexturesTable[i], maxTextures, &gsPlane[bgType].mBGDataAllocator);
#ifdef USE_TILEMAP_ATTRIBUTES
		gsPlane[bgType].mpBGVertexCt[i] = 0;
		gsPlane[bgType].mpBGColorCt[i] = 0;
#endif
	}
	gsPlane[bgType].mBGMaxLayers = maxLayers;
	gsPlane[bgType].mBGMaxTextures = maxTextures;
	gsPlane[bgType].mBGMaxElements = maxElements;
}
//----------------------------------------------------------------------------------

void glRender_ReleaseBGLayersData(enum BGSelect bgType)
{
	SDK_ASSERT(bgType < BGSELECT_NUM);
	if(gsPlane[bgType].mBGMaxLayers > 0)
	{
		FREE(gsPlane[bgType].mpBGTexturesTable);
#ifdef USE_TILEMAP_ATTRIBUTES
		FREE(gsPlane[bgType].mpBGColorCt);
		FREE(gsPlane[bgType].mpBGVertexCt);
		FREE(gsPlane[bgType].mpBGColorPool);
#endif
		FREE(gsPlane[bgType].mpBGTexCoordPool);
		FREE(gsPlane[bgType].mpBGVertexPool);
		gsPlane[bgType].mpBGTexturesTable = NULL;
		gsPlane[bgType].mpBGTexCoordPool = NULL;
		gsPlane[bgType].mpBGVertexPool = NULL;
#ifdef USE_TILEMAP_ATTRIBUTES
		gsPlane[bgType].mpBGColorPool = NULL;
#endif

#ifdef USE_GL_GLEXT
//#if defined ANDROID_NDK
        // do nothing
//#else
#ifdef USE_TILEMAP_ATTRIBUTES
		glDeleteBuffers(gsPlane[bgType].mBGMaxLayers * gsPlane[bgType].mBGMaxTextures * gsPlane[bgType].mBGMaxElements * 3, gsPlane[bgType].mpBGVBOBuffer);
#else
		glDeleteBuffers(gsPlane[bgType].mBGMaxLayers * gsPlane[bgType].mBGMaxTextures * 2, gsPlane[bgType].mpBGVBOBuffer);
#endif
		FREE(gsPlane[bgType].mpBGVBOBuffer);
		gsPlane[bgType].mpBGVBOBuffer = NULL;
#endif
		FREE(gsPlane[bgType].mpBGDataAllocatorHeap);
		gsPlane[bgType].mpBGDataAllocatorHeap = NULL;
		StaticAllocator_Release(&gsPlane[bgType].mBGDataAllocator);
	}
	gsPlane[bgType].mBGMaxLayers = 0;
	gsPlane[bgType].mBGMaxTextures = 0;
	gsPlane[bgType].mBGMaxElements = 0;
}
//----------------------------------------------------------------------------------

void glRender_SetActiveBGForGraphics(enum BGSelect iActiveBG)
{
   if(gsActivePlane != iActiveBG)
   {
		gsActivePlane = iActiveBG;
		SDK_ASSERT(gsActivePlane != BGSELECT_NUM);
   }
}
//----------------------------------------------------------------------------------

enum BGSelect glRender_GetActiveBGForGraphics(void)
{
	return gsActivePlane; 
}
//----------------------------------------------------------------------------------

void glRender_CreateTextures(s32 texturesCount)
{	
#ifdef USE_GL_GLEXT
	s32 i;
#endif
	mCurrentTextureID = -1;
	if(texturesCount > 0)
	{
		mTextureCount = (GLuint)texturesCount;
		SDK_ASSERT(mpTexture == NULL);
		mpTexture = (GLuint*)MALLOC(sizeof(GLuint) * mTextureCount, "glRender_CreateTextures:mpTexture");
		glGenTextures(mTextureCount, mpTexture);
		_glRender_CheckError("glGenTextures");
#ifdef SDK_DEBUG
		OS_Printf("glRender_CreateTextures = %d\n", mTextureCount);
#endif
	}
#ifdef USE_GL_GLEXT
	for(i = 0; i < BGSELECT_NUM; i++)
	{
		if(gsPlane[i].mpImgVertexPool != NULL)
		{
			SDK_ASSERT(gsPlane[i].mpVBOBuffer == NULL);
			gsPlane[i].mpVBOBuffer = (GLuint*)MALLOC(gsPlane[i].mMaxObjOnScene * sizeof(GLuint) * 3, "glRender_CreateTextures:mpVBOBuffer");
			glGenBuffers(gsPlane[i].mMaxObjOnScene * 3, gsPlane[i].mpVBOBuffer);
		}
	}
#endif
	glShadeModel(GL_FLAT);
	sgRenderListReady = TRUE;
}
//----------------------------------------------------------------------------------

void glRender_DeleteTextures()
{
#ifdef USE_GL_GLEXT
	s32 i = BGSELECT_NUM;
	while(i > 0)
	{
		i--;
		if(gsPlane[i].mpVBOBuffer != NULL)
		{
//#if defined ANDROID_NDK
			// do nothing
//#else
			glDeleteBuffers(gsPlane[i].mMaxObjOnScene * 3, gsPlane[i].mpVBOBuffer);
//#endif
			FREE(gsPlane[i].mpVBOBuffer);
			gsPlane[i].mpVBOBuffer = NULL;
		}
	}
#endif
	sgRenderListReady = FALSE;
	if(mTextureCount > 0)
	{
//#if defined ANDROID_NDK
        // do nothing
//#else
		SDK_NULL_ASSERT(mpTexture);
		glDeleteTextures(mTextureCount, mpTexture);
		_glRender_CheckError("glDeleteTextures");
//#endif
		FREE(mpTexture);
		mTextureCount = 0;
		mpTexture = NULL;
	}		
}
//----------------------------------------------------------------------------------

s32 glRender_GetViewWidth(enum BGSelect iBGType)
{
	SDK_ASSERT(iBGType < BGSELECT_NUM);
	return gsPlane[iBGType].mViewWidth;
}
//----------------------------------------------------------------------------------

s32 glRender_GetViewHeight(enum BGSelect iBGType)
{
	SDK_ASSERT(iBGType < BGSELECT_NUM);
	return gsPlane[iBGType].mViewHeight;
}
//----------------------------------------------------------------------------------

s32 glRender_GetFrameBufferWidth(enum BGSelect iBGType)
{
	SDK_ASSERT(iBGType < BGSELECT_NUM);
	return gsPlane[iBGType].mViewWidth;
}
//----------------------------------------------------------------------------------

s32 glRender_GetViewLeft(enum BGSelect iBGType)
{
	SDK_ASSERT(iBGType < BGSELECT_NUM);
	return gsPlane[iBGType].mOffX;	
}
//----------------------------------------------------------------------------------

s32 glRender_GetViewTop(enum BGSelect iBGType)
{
	SDK_ASSERT(iBGType < BGSELECT_NUM);
	return gsPlane[iBGType].mOffY;
}
//----------------------------------------------------------------------------------

s32 glRender_GetFrameBufferHeight(enum BGSelect iBGType)
{
	SDK_ASSERT(iBGType < BGSELECT_NUM);
	return gsPlane[iBGType].mViewHeight;
}
//----------------------------------------------------------------------------------

void glRender_LoadTextureToVRAM(struct BMPImage *pImage, s32 resId)
{
	u8* buf8888;
	SDK_NULL_ASSERT(pImage);
	SDK_ASSERT(pImage->mType == BMP_TYPE_DC16 || pImage->mType == BMP_TYPE_DC32); 

	buf8888 = NULL;
	mCurrentTextureID = -1;

	if(pImage->mType == BMP_TYPE_DC32)
	{
		buf8888 = pImage->data.mpData256;
	}
	else
#if !(defined WINDOWS_APP || defined NIX_APP || defined EMSCRIPTEN_APP || defined KOLIBRIOS_APP)
	if(pImage->mA5DataSize > 0)
#endif
	{
		s32 i, j;
		u16 d5551;
		u8 *buf;
		buf8888 = (u8*)MALLOC(pImage->mWidth2n * pImage->mHeight2n * 4, "glRender:buf8888");
		for(i = 0; i < pImage->mHeight; i++)
		{
			buf = buf8888 + pImage->mWidth2n * i * 4;
			for(j = 0; j < pImage->mWidth; j++)
			{				
				d5551 = *(pImage->data.mpDataDC16 + i * pImage->mWidth2n + j);
				*(buf++) = (u8)(((((d5551 & GX_RGBA_R_MASK) >> GX_RGBA_R_SHIFT)) * 0xff) / 0x1f);
				*(buf++) = (u8)(((((d5551 & GX_RGBA_G_MASK) >> GX_RGBA_G_SHIFT)) * 0xff) / 0x1f);
				*(buf++) = (u8)(((((d5551 & GX_RGBA_B_MASK) >> GX_RGBA_B_SHIFT)) * 0xff) / 0x1f);
				*(buf++) = (u8)(((d5551 & GX_RGBA_A_MASK) >> GX_RGBA_A_SHIFT) * 0xff); 
			}
		}
		if(pImage->mA5DataSize > 0)
		{
			s32 k, h;
			buf = buf8888;
			h = k = 0;
			i = pImage->mHeight * pImage->mWidth;
			while(i > 0)
			{	
				d5551 = *(pImage->mpA5Data + k);				
				k++;				
				buf += 3;
				*(buf++) = (u8)(((((d5551 & 0xf800) >> 11)) * 0xff) / 0x1f);
				if(--i < 0)
				{
					break;
				}
				if(i % pImage->mWidth == 0)
				{
					h++;
					buf = buf8888 + pImage->mWidth2n * h * 4;
					if(buf8888 + pImage->mWidth2n * pImage->mHeight2n * 4 <= buf)
					{
						break;	
					}
				}
				buf += 3;
				*(buf++) = (u8)(((((d5551 & 0x07c0) >> 6)) * 0xff) / 0x1f);
				if(--i < 0)
				{
					break;
				}
				if(i % pImage->mWidth == 0)
				{
					h++;
					buf = buf8888 + pImage->mWidth2n * h * 4;
					if(buf8888 + pImage->mWidth2n * pImage->mHeight2n * 4 <= buf)
					{
						break;	
					}
				}
				buf += 3;
				*(buf++) = (u8)(((((d5551 & 0x003e) >> 1)) * 0xff) / 0x1f);
				if(--i < 0)
				{
					break;
				}
				if(i % pImage->mWidth == 0)
				{
					h++;
					buf = buf8888 + pImage->mWidth2n * h * 4;
					if(buf8888 + pImage->mWidth2n * pImage->mHeight2n * 4 <= buf)
					{
						break;	
					}
				}
			}
		}
	}
#if !(defined WINDOWS_APP || defined NIX_APP || defined EMSCRIPTEN_APP || defined KOLIBRIOS_APP)
	else
	{
		buf8888 = NULL;
	}
#endif

#ifdef JOBS_IN_SEPARATE_THREAD
	jobCriticalSectionBegin();	
#endif
	SDK_NULL_ASSERT(mpTexture);
	glBindTexture(GL_TEXTURE_2D, mpTexture[resId]);
	_glRender_CheckError("glBindTexture");
	if(buf8888 != NULL)
	{
		glTexSubImage2D(GL_TEXTURE_2D,
						0,
						0,
						0,
						pImage->mWidth2n,
						pImage->mHeight2n,
						GL_RGBA,
						GL_UNSIGNED_BYTE,
						buf8888);
	}
#if !(defined WINDOWS_APP || defined NIX_APP || defined EMSCRIPTEN_APP || defined KOLIBRIOS_APP)
	else
	{
		glTexSubImage2D(GL_TEXTURE_2D,
						0,
						0,
						0,
						pImage->mWidth2n,
						pImage->mHeight2n,
						GL_RGBA,
						GL_UNSIGNED_SHORT_5_5_5_1,
						pImage->data.mpDataDC16);
	}
#endif
	_glRender_CheckError("glTexSubImage2D");
#ifdef JOBS_IN_SEPARATE_THREAD
	jobCriticalSectionEnd();	
#endif
	if(buf8888 != NULL && pImage->mType == BMP_TYPE_DC16)
	{
		FREE(buf8888);
	}
}
//----------------------------------------------------------------------------------

void glRender_CreateEmptyTexture(const struct BMPImage *iImgHeader, s32 resId)
{
	SDK_NULL_ASSERT(iImgHeader);
	SDK_ASSERT(iImgHeader->mType == BMP_TYPE_DC16 || iImgHeader->mType == BMP_TYPE_DC32);
	mCurrentTextureID = -1;
	SDK_NULL_ASSERT(mpTexture);
	glBindTexture(GL_TEXTURE_2D, mpTexture[resId]);
	_glRender_CheckError("glBindTexture");
	glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE);
	if(iImgHeader->mFilterType == BMP_FILTER_NEAREST)
	{
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	}
	else
	{
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	}
#if defined ANDROID_NDK || defined IOS_APP
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
#else
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP);
#endif
#if !(defined WINDOWS_APP || defined NIX_APP || defined EMSCRIPTEN_APP || defined KOLIBRIOS_APP)
	if((iImgHeader->mA5DataSize > 0 && iImgHeader->mType == BMP_TYPE_DC16) || iImgHeader->mType == BMP_TYPE_DC32)
#endif
	{
		glTexImage2D(GL_TEXTURE_2D,
						0,
						GL_RGBA,
						iImgHeader->mWidth2n,
						iImgHeader->mHeight2n,
						0,
						GL_RGBA,
						GL_UNSIGNED_BYTE,
						NULL);
	}
#if !(defined WINDOWS_APP || defined NIX_APP || defined EMSCRIPTEN_APP || defined KOLIBRIOS_APP)
	else
	{
		glTexImage2D(GL_TEXTURE_2D,
						0,
						GL_RGBA,
						iImgHeader->mWidth2n,
						iImgHeader->mHeight2n,
						0,
						GL_RGBA,
						GL_UNSIGNED_SHORT_5_5_5_1,
						NULL);
	}
#endif
	_glRender_CheckError("glTexImage2D");
}
//----------------------------------------------------------------------------------

void glRender_ClearFrameBuffer(enum BGSelect iType)
{
	if(iType < BGSELECT_NUM)
	{
		u32 i;
		StaticAllocator_Reset(&gsPlane[gsActivePlane].mBGDataAllocator);
		gsPlane[iType].mRenderListSize = -1;
		for(i = 0; i < gsPlane[iType].mBGMaxLayers; i++)
		{
			AllocatorHTable_Init(&gsPlane[iType].mpBGTexturesTable[i], gsPlane[iType].mBGMaxTextures, &gsPlane[iType].mBGDataAllocator);
#ifdef USE_TILEMAP_ATTRIBUTES
			gsPlane[iType].mpBGColorCt[i] = 0;
			gsPlane[iType].mpBGVertexCt[i] = 0;
#endif
		}
		gsPlane[iType].mpRenderList = NULL;
		gsPlane[iType].mImgColorCt = 0;
		gsPlane[iType].mImgVertexCt = 0;
		gsPlane[iType].mDraw = 0;
#ifdef USE_GL_GLEXT
		gsPlane[iType].mObjVBOBufferLastIdx = 0;
		gsPlane[iType].mBGVBOBufferLastIdx = 0;
#endif
	}
}
//----------------------------------------------------------------------------------

void glRender_FreezeCurrentFrameBuffer(BOOL val)
{
	gsFreezeCurrentFrameBuffer = val;
}
//----------------------------------------------------------------------------------

BOOL _glRender_CheckForCleanup(enum BGSelect iType)
{
#ifdef SDK_DEBUG
	SDK_ASSERT(cd.trig_visible_mask == 0);
#endif
	if(gsFreezeCurrentFrameBuffer == TRUE)
	{
		if(gsPlane[iType].mDraw > 0)
		{
			gsPlane[iType].mDraw = 1;
		}
		return FALSE;
	}
	if(gsPlane[iType].mDraw > 1)
	{
		glRender_ClearFrameBuffer(iType);
	}
	return TRUE;
}
//----------------------------------------------------------------------------------

void _glRender_AddToRenderList(struct TRGLRenderItem* item)
{
	SDK_ASSERT(gsActivePlane != BGSELECT_NUM);
	switch(item->mType)
	{
		case RGL_PIXEL:
		case RGL_LINE:
		case RGL_FILLRECT:
		case RGL_IMAGE:
		case RGL_EXTERNAL_POS_COLOR:
			if(gsPlane[gsActivePlane].mpRenderList == NULL)
			{
				item->mpNext = NULL;
				gsPlane[gsActivePlane].mpRenderListTail = gsPlane[gsActivePlane].mpRenderList = item;
			}
			else
			{
				item->mpNext = NULL;
				gsPlane[gsActivePlane].mpRenderListTail->mpNext = item;
				gsPlane[gsActivePlane].mpRenderListTail = item;
			}
		break;
		default:
			SDK_ASSERT(0);
	}
}
//----------------------------------------------------------------------------------------------------------------

void glRender_SetColor(GXRgba iColor)
{
	SDK_ASSERT(gsActivePlane != BGSELECT_NUM);
	if(_glRender_CheckForCleanup(gsActivePlane) == FALSE)
	{
		return;
	}
	gsPlane[gsActivePlane].mColor.r = GX_COLOR_CHANNEL((iColor & GX_RGBA_R_MASK) >> GX_RGBA_R_SHIFT);
	gsPlane[gsActivePlane].mColor.g = GX_COLOR_CHANNEL((iColor & GX_RGBA_G_MASK) >> GX_RGBA_G_SHIFT);
	gsPlane[gsActivePlane].mColor.b = GX_COLOR_CHANNEL((iColor & GX_RGBA_B_MASK) >> GX_RGBA_B_SHIFT);
	gsPlane[gsActivePlane].mColor.a = GX_COLOR_CHANNEL(0x1f * (iColor & GX_RGBA_A_MASK));
	gsPlane[gsActivePlane].mColor.rgba = iColor;
}
//----------------------------------------------------------------------------------------------------------------

void glRender_DrawLine(fx32 iX0, fx32 iY0, fx32 iX1, fx32 iY1)
{
	BOOL newItem;
	struct TRGLRenderItem *item;
	
	SDK_ASSERT(gsActivePlane != BGSELECT_NUM);
	if(!gxHelper_preDrawLine(FX32(gsPlane[gsActivePlane].mViewWidth), FX32(gsPlane[gsActivePlane].mViewHeight), &iX0, &iY0, &iX1, &iY1))
	{
		return;
	}
	
	if(_glRender_CheckForCleanup(gsActivePlane) == FALSE)
	{
		return;
	}

	newItem = FALSE;
	if(!(gsPlane[gsActivePlane].mRenderListSize >= 0 && 
		gsPlane[gsActivePlane].mpRenderListPool[gsPlane[gsActivePlane].mRenderListSize].mType == RGL_LINE &&
		gsPlane[gsActivePlane].mpRenderListPool[gsPlane[gsActivePlane].mRenderListSize].c.mFillWithColor == (u32)gsPlane[gsActivePlane].mColor.rgba))
	{
		gsPlane[gsActivePlane].mRenderListSize++;
		newItem = TRUE;
	}

	SDK_ASSERT(gsPlane[gsActivePlane].mRenderListSize <= gsPlane[gsActivePlane].mMaxObjOnScene);
	if(gsPlane[gsActivePlane].mRenderListSize > gsPlane[gsActivePlane].mMaxObjOnScene)
	{
		OS_Warning("glRender: RenderListSize overflow\n");
		SDK_ASSERT(0);
		gsPlane[gsActivePlane].mRenderListSize = gsPlane[gsActivePlane].mMaxObjOnScene - 1;
		return;
	}

	SDK_ASSERT(gsPlane[gsActivePlane].mImgVertexCt + 4 <= gsPlane[gsActivePlane].mMaxObjOnScene * 12);
	if(gsPlane[gsActivePlane].mImgVertexCt + 4 > gsPlane[gsActivePlane].mMaxObjOnScene * 12)
	{
		OS_Warning("glRender: RenderList vertex pool overflow\n");
		SDK_ASSERT(0);
		return;
	}

	gsPlane[gsActivePlane].mDraw = 1;
	item = &gsPlane[gsActivePlane].mpRenderListPool[gsPlane[gsActivePlane].mRenderListSize];
	if(newItem)
	{
		item->mpImageHeader = NULL;
		item->tv.mpImgVertexArrayPtr = &gsPlane[gsActivePlane].mpImgVertexPool[gsPlane[gsActivePlane].mImgVertexCt];
		item->tv.mpImgTexArrayPtr = NULL;
		item->tv.mVertexCt = 0;
		item->c.mpImgColorArrayPtr = &gsPlane[gsActivePlane].mpImgColorPool[gsPlane[gsActivePlane].mImgColorCt];
		item->c.mFillWithColor = (u32)gsPlane[gsActivePlane].mColor.rgba;
		item->c.mColorCt = 0;
#ifdef USE_GL_GLEXT
		item->mVBOBufferIdx = gsPlane[gsActivePlane].mObjVBOBufferLastIdx;
		gsPlane[gsActivePlane].mObjVBOBufferLastIdx += 3;
#endif
		item->mType = RGL_LINE;
		_glRender_AddToRenderList(item);
	}
	item->tv.mpImgVertexArrayPtr[0 + item->tv.mVertexCt] = FX_FX32_TO_F32(iX0);
	item->tv.mpImgVertexArrayPtr[1 + item->tv.mVertexCt] = FX_FX32_TO_F32(iY0);
	item->tv.mpImgVertexArrayPtr[2 + item->tv.mVertexCt] = FX_FX32_TO_F32(iX1);
	item->tv.mpImgVertexArrayPtr[3 + item->tv.mVertexCt] = FX_FX32_TO_F32(iY1);
	item->c.mpImgColorArrayPtr[0 + item->c.mColorCt] = item->c.mpImgColorArrayPtr[4 + item->c.mColorCt] = gsPlane[gsActivePlane].mColor.r;
	item->c.mpImgColorArrayPtr[1 + item->c.mColorCt] = item->c.mpImgColorArrayPtr[5 + item->c.mColorCt] = gsPlane[gsActivePlane].mColor.g;
	item->c.mpImgColorArrayPtr[2 + item->c.mColorCt] = item->c.mpImgColorArrayPtr[6 + item->c.mColorCt] = gsPlane[gsActivePlane].mColor.b;
	item->c.mpImgColorArrayPtr[3 + item->c.mColorCt] = item->c.mpImgColorArrayPtr[7 + item->c.mColorCt] = gsPlane[gsActivePlane].mColor.a;
	gsPlane[gsActivePlane].mImgVertexCt += 4;
	item->tv.mVertexCt += 4;
	gsPlane[gsActivePlane].mImgColorCt += 8;
	item->c.mColorCt += 8;
}
//----------------------------------------------------------------------------------------------------------------

void glRender_ColorRect(fx32 iX, fx32 iY, fx32 iWidth, fx32 iHeight)
{
	BOOL newItem;
	struct TRGLRenderItem *item;

	SDK_ASSERT(gsActivePlane != BGSELECT_NUM);
	if(!gxHelper_preDrawSquare(FX32(gsPlane[gsActivePlane].mViewWidth), FX32(gsPlane[gsActivePlane].mViewHeight), &iX, &iY, &iWidth, &iHeight))
	{
		return;
	}

	if(_glRender_CheckForCleanup(gsActivePlane) == FALSE)
	{
		return;
	}

	newItem = FALSE;
	if(!(gsPlane[gsActivePlane].mRenderListSize >= 0 && 
		gsPlane[gsActivePlane].mpRenderListPool[gsPlane[gsActivePlane].mRenderListSize].mType == RGL_FILLRECT &&
		gsPlane[gsActivePlane].mpRenderListPool[gsPlane[gsActivePlane].mRenderListSize].c.mFillWithColor == (u32)gsPlane[gsActivePlane].mColor.rgba))
	{
		gsPlane[gsActivePlane].mRenderListSize++;
		newItem = TRUE;
	}

	SDK_ASSERT(gsPlane[gsActivePlane].mRenderListSize <= gsPlane[gsActivePlane].mMaxObjOnScene);
	if(gsPlane[gsActivePlane].mRenderListSize > gsPlane[gsActivePlane].mMaxObjOnScene)
	{
		OS_Warning("glRender: RenderListSize overflow\n");
		SDK_ASSERT(0);
		gsPlane[gsActivePlane].mRenderListSize = gsPlane[gsActivePlane].mMaxObjOnScene - 1;
		return;
	}

	SDK_ASSERT(gsPlane[gsActivePlane].mImgVertexCt + 12 <= gsPlane[gsActivePlane].mMaxObjOnScene * 12);
	if(gsPlane[gsActivePlane].mImgVertexCt + 12 > gsPlane[gsActivePlane].mMaxObjOnScene * 12)
	{
		OS_Warning("glRender: RenderList vertex pool overflow\n");
		SDK_ASSERT(0);
		return;
	}

	gsPlane[gsActivePlane].mDraw = 1;
	item = &gsPlane[gsActivePlane].mpRenderListPool[gsPlane[gsActivePlane].mRenderListSize];

	if(newItem)
	{
		item->mpImageHeader = NULL;
		item->tv.mpImgVertexArrayPtr = &gsPlane[gsActivePlane].mpImgVertexPool[gsPlane[gsActivePlane].mImgVertexCt];
		item->tv.mpImgTexArrayPtr = NULL;
		item->tv.mVertexCt = 0;
		item->c.mpImgColorArrayPtr = &gsPlane[gsActivePlane].mpImgColorPool[gsPlane[gsActivePlane].mImgColorCt];
		item->c.mFillWithColor = (u32)gsPlane[gsActivePlane].mColor.rgba;
		item->c.mColorCt = 0;
#ifdef USE_GL_GLEXT
		item->mVBOBufferIdx = gsPlane[gsActivePlane].mObjVBOBufferLastIdx;
		gsPlane[gsActivePlane].mObjVBOBufferLastIdx += 3;
#endif
		item->mType = RGL_FILLRECT;
		_glRender_AddToRenderList(item);
	}
			
	if(item->tv.mVertexCt > 0)
	{				
		item->tv.mpImgVertexArrayPtr[0 + item->tv.mVertexCt] = item->tv.mpImgVertexArrayPtr[6 + item->tv.mVertexCt - 8];
		item->tv.mpImgVertexArrayPtr[1 + item->tv.mVertexCt] = item->tv.mpImgVertexArrayPtr[7 + item->tv.mVertexCt - 8];
		item->tv.mpImgVertexArrayPtr[2 + item->tv.mVertexCt] = FX_FX32_TO_F32(iX);
		item->tv.mpImgVertexArrayPtr[3 + item->tv.mVertexCt] = FX_FX32_TO_F32(iY);
		MI_CpuFill8(&item->c.mpImgColorArrayPtr[item->c.mColorCt], 0, sizeof(GLcolor) * 8);
		gsPlane[gsActivePlane].mImgVertexCt += 4;
		item->tv.mVertexCt += 4;
		gsPlane[gsActivePlane].mImgColorCt += 8;
		item->c.mColorCt += 8;
	}
	item->tv.mpImgVertexArrayPtr[0 + item->tv.mVertexCt] = item->tv.mpImgVertexArrayPtr[2 + item->tv.mVertexCt] = FX_FX32_TO_F32(iX);
	item->tv.mpImgVertexArrayPtr[3 + item->tv.mVertexCt] = item->tv.mpImgVertexArrayPtr[7 + item->tv.mVertexCt] = FX_FX32_TO_F32(iY + iHeight);
	item->tv.mpImgVertexArrayPtr[4 + item->tv.mVertexCt] = item->tv.mpImgVertexArrayPtr[6 + item->tv.mVertexCt] = FX_FX32_TO_F32(iX + iWidth);
	item->tv.mpImgVertexArrayPtr[1 + item->tv.mVertexCt] = item->tv.mpImgVertexArrayPtr[5 + item->tv.mVertexCt] = FX_FX32_TO_F32(iY);
	item->c.mpImgColorArrayPtr[0 + item->c.mColorCt] = item->c.mpImgColorArrayPtr[4 + item->c.mColorCt] = item->c.mpImgColorArrayPtr[8 + item->c.mColorCt] = item->c.mpImgColorArrayPtr[12 + item->c.mColorCt] = gsPlane[gsActivePlane].mColor.r;
	item->c.mpImgColorArrayPtr[1 + item->c.mColorCt] = item->c.mpImgColorArrayPtr[5 + item->c.mColorCt] = item->c.mpImgColorArrayPtr[9 + item->c.mColorCt] = item->c.mpImgColorArrayPtr[13 + item->c.mColorCt] = gsPlane[gsActivePlane].mColor.g;
	item->c.mpImgColorArrayPtr[2 + item->c.mColorCt] = item->c.mpImgColorArrayPtr[6 + item->c.mColorCt] = item->c.mpImgColorArrayPtr[10 + item->c.mColorCt] = item->c.mpImgColorArrayPtr[14 + item->c.mColorCt] = gsPlane[gsActivePlane].mColor.b;
	item->c.mpImgColorArrayPtr[3 + item->c.mColorCt] = item->c.mpImgColorArrayPtr[7 + item->c.mColorCt] = item->c.mpImgColorArrayPtr[11 + item->c.mColorCt] = item->c.mpImgColorArrayPtr[15 + item->c.mColorCt] = gsPlane[gsActivePlane].mColor.a;
	gsPlane[gsActivePlane].mImgVertexCt += 8;
	item->tv.mVertexCt += 8;
	gsPlane[gsActivePlane].mImgColorCt += 16;
	item->c.mColorCt += 16;
}
//----------------------------------------------------------------------------------------------------------------

#ifdef USE_EXTERNAL_API
void glRender_ExternalSetActiveClipRect(const struct ExDRect* data)
{
	SDK_ASSERT(gsActivePlane != BGSELECT_NUM);
	if(data == NULL)
	{
		gsActiveClipRect[EXDICLIPRECTXY] = NULL_CLIP_RECT;
		gsActiveClipRect[EXDICLIPRECTWH] = NULL_CLIP_RECT;
	}
	else
	{
		gsActiveClipRect[EXDICLIPRECTXY] = ((u16)(data->x) << 16) | (u16)data->y;
		gsActiveClipRect[EXDICLIPRECTWH] = ((u16)(data->w) << 16) | (u16)data->h;
	}
}
//----------------------------------------------------------------------------------------------------------------

void glRender_ExternalPosColorData(const struct TRGLColorVertexData* data)
{
	BOOL newItem;
	struct TRGLRenderItem *item;

	SDK_NULL_ASSERT(data);
	SDK_ASSERT(gsActivePlane != BGSELECT_NUM);

	if(_glRender_CheckForCleanup(gsActivePlane) == FALSE)
	{
		return;
	}

	newItem = FALSE;
	if(!(gsPlane[gsActivePlane].mRenderListSize >= 0 &&
		 gsPlane[gsActivePlane].mpRenderListPool[gsPlane[gsActivePlane].mRenderListSize].mType == RGL_EXTERNAL_POS_COLOR &&
		 gsPlane[gsActivePlane].mpRenderListPool[gsPlane[gsActivePlane].mRenderListSize].mClipRect[EXDICLIPRECTXY] == gsActiveClipRect[EXDICLIPRECTXY] &&
		 gsPlane[gsActivePlane].mpRenderListPool[gsPlane[gsActivePlane].mRenderListSize].mClipRect[EXDICLIPRECTWH] == gsActiveClipRect[EXDICLIPRECTWH]))
	{
		gsPlane[gsActivePlane].mRenderListSize++;
		newItem = TRUE;
	}

	SDK_ASSERT(gsPlane[gsActivePlane].mRenderListSize <= gsPlane[gsActivePlane].mMaxObjOnScene);
	if(gsPlane[gsActivePlane].mRenderListSize > gsPlane[gsActivePlane].mMaxObjOnScene)
	{
		OS_Warning("glRender: RenderListSize overflow\n");
		SDK_ASSERT(0);
		gsPlane[gsActivePlane].mRenderListSize = gsPlane[gsActivePlane].mMaxObjOnScene - 1;
		return;
	}

	SDK_ASSERT(gsPlane[gsActivePlane].mImgVertexCt + 2 <= gsPlane[gsActivePlane].mMaxObjOnScene * 12);
	if(gsPlane[gsActivePlane].mImgVertexCt + 2 > gsPlane[gsActivePlane].mMaxObjOnScene * 12)
	{
		OS_Warning("glRender: RenderList vertex pool overflow\n");
		SDK_ASSERT(0);
		return;
	}

	gsPlane[gsActivePlane].mDraw = 1;
	item = &gsPlane[gsActivePlane].mpRenderListPool[gsPlane[gsActivePlane].mRenderListSize];
	if(newItem)
	{
		item->mpImageHeader = NULL;
		item->mClipRect[EXDICLIPRECTXY] = gsActiveClipRect[EXDICLIPRECTXY];
		item->mClipRect[EXDICLIPRECTWH] = gsActiveClipRect[EXDICLIPRECTWH];
		item->tv.mpImgVertexArrayPtr = &gsPlane[gsActivePlane].mpImgVertexPool[gsPlane[gsActivePlane].mImgVertexCt];
		item->tv.mpImgTexArrayPtr = NULL;
		item->c.mpImgColorArrayPtr = &gsPlane[gsActivePlane].mpImgColorPool[gsPlane[gsActivePlane].mImgColorCt];
		item->tv.mVertexCt = 0;
		item->c.mColorCt = 0;
#ifdef USE_GL_GLEXT
		item->mVBOBufferIdx = gsPlane[gsActivePlane].mObjVBOBufferLastIdx;
		gsPlane[gsActivePlane].mObjVBOBufferLastIdx += 3;
#endif
		item->mType = RGL_EXTERNAL_POS_COLOR;
		_glRender_AddToRenderList(item);
	}
	item->tv.mpImgVertexArrayPtr[0 + item->tv.mVertexCt] = (data->pos[0]);
	item->tv.mpImgVertexArrayPtr[1 + item->tv.mVertexCt] = (data->pos[1]);
	item->c.mpImgColorArrayPtr[0 + item->c.mColorCt] = (data->color[0]);
	item->c.mpImgColorArrayPtr[1 + item->c.mColorCt] = (data->color[1]);
	item->c.mpImgColorArrayPtr[2 + item->c.mColorCt] = (data->color[2]);
	item->c.mpImgColorArrayPtr[3 + item->c.mColorCt] = (data->color[3]);
	gsPlane[gsActivePlane].mImgVertexCt += 2;
	item->tv.mVertexCt += 2;
	gsPlane[gsActivePlane].mImgColorCt += 4;
	item->c.mColorCt += 4;
}
//----------------------------------------------------------------------------------------------------------------
#endif

static void glRender_SetColorColorArrayItem16(const struct DrawImageFunctionData* data, struct RenderColorArrayItem* item)
{
	GLcolor cr, cg, cb, ca;
	GXRgba ocolor = data->mFillColor & 0xFFFF;
	cr = GX_COLOR_CHANNEL((ocolor & GX_RGBA_R_MASK) >> GX_RGBA_R_SHIFT);
	cg = GX_COLOR_CHANNEL((ocolor & GX_RGBA_G_MASK) >> GX_RGBA_G_SHIFT);
	cb = GX_COLOR_CHANNEL((ocolor & GX_RGBA_B_MASK) >> GX_RGBA_B_SHIFT);
	ca = GX_COLOR_CHANNEL(data->mFillColor >> 16);
	item->mpImgColorArrayPtr[0 + item->mColorCt] = item->mpImgColorArrayPtr[4 + item->mColorCt] = item->mpImgColorArrayPtr[8 + item->mColorCt] = item->mpImgColorArrayPtr[12 + item->mColorCt] = cr;
	item->mpImgColorArrayPtr[1 + item->mColorCt] = item->mpImgColorArrayPtr[5 + item->mColorCt] = item->mpImgColorArrayPtr[9 + item->mColorCt] = item->mpImgColorArrayPtr[13 + item->mColorCt] = cg;
	item->mpImgColorArrayPtr[2 + item->mColorCt] = item->mpImgColorArrayPtr[6 + item->mColorCt] = item->mpImgColorArrayPtr[10 + item->mColorCt] = item->mpImgColorArrayPtr[14 + item->mColorCt] = cb;
	item->mpImgColorArrayPtr[3 + item->mColorCt] = item->mpImgColorArrayPtr[7 + item->mColorCt] = item->mpImgColorArrayPtr[11 + item->mColorCt] = item->mpImgColorArrayPtr[15 + item->mColorCt] = ca;
}
//----------------------------------------------------------------------------------------------------------------

static void glRender_SetColorColorArrayItem8(const struct DrawImageFunctionData* data, struct RenderColorArrayItem* item)
{
	GLcolor cr, cg, cb, ca;
	GXRgba ocolor = data->mFillColor & 0xFFFF;
	cr = GX_COLOR_CHANNEL((ocolor & GX_RGBA_R_MASK) >> GX_RGBA_R_SHIFT);
	cg = GX_COLOR_CHANNEL((ocolor & GX_RGBA_G_MASK) >> GX_RGBA_G_SHIFT);
	cb = GX_COLOR_CHANNEL((ocolor & GX_RGBA_B_MASK) >> GX_RGBA_B_SHIFT);
	ca = GX_COLOR_CHANNEL(data->mFillColor >> 16);
	item->mpImgColorArrayPtr[0 + item->mColorCt] = item->mpImgColorArrayPtr[4 + item->mColorCt] = cr;
	item->mpImgColorArrayPtr[1 + item->mColorCt] = item->mpImgColorArrayPtr[5 + item->mColorCt] = cg;
	item->mpImgColorArrayPtr[2 + item->mColorCt] = item->mpImgColorArrayPtr[6 + item->mColorCt] = cb;
	item->mpImgColorArrayPtr[3 + item->mColorCt] = item->mpImgColorArrayPtr[7 + item->mColorCt] = ca;
}
//----------------------------------------------------------------------------------------------------------------

void glRender_DrawImage(const struct DrawImageFunctionData* data)
{
	SDK_ASSERT(gsActivePlane != BGSELECT_NUM);
	switch(data->mType)
	{
		case DIT_TileBG1:
		case DIT_TileBG2:
		case DIT_TileBG3:
		case DIT_TileBG4:
		case DIT_TileBG5:
		{
			// row order from right to left
			if (gsPlane[gsActivePlane].mBGMaxLayers > 0)
			{
				struct TRGLBGItem* item;
				struct AllocatorHTable* txtTable;
				struct StaticAllocator* allocator;
#ifdef USE_TILEMAP_ATTRIBUTES
				struct AllocatorHTable *opacityTable;
				struct AllocatorList* itemList;
				s32 addChunk;
#endif
				GLfx32 *qdata, *txtr;
				fx32 yb, xl;
				s32 txtr_idx;
#ifdef USE_TILEMAP_ATTRIBUTES
				const s32 chunkVertexSize = sgBGChunkSize * 12;
				const s32 chunkColorSize = sgBGChunkSize * 24;

				if (GX_COLOR_CHANNEL(data->mFillColor >> 16) == 0)
				{
					return;
				}
#endif
				if (_glRender_CheckForCleanup(gsActivePlane) == FALSE)
				{
					return;
				}
				gsPlane[gsActivePlane].mDraw = 1;

				txtTable = &gsPlane[gsActivePlane].mpBGTexturesTable[data->mType];
				allocator = &gsPlane[gsActivePlane].mBGDataAllocator;

#ifdef USE_TILEMAP_ATTRIBUTES
				item = NULL;
				itemList = NULL;
				addChunk = -1;

				opacityTable = (struct AllocatorHTable*)AllocatorHTable_Val(txtTable, AllocatorHTable_Find(txtTable, data->mpSrcData->mOpaqType));
				if (opacityTable != NULL)
				{
					itemList = (struct AllocatorList*)AllocatorHTable_Val(opacityTable, AllocatorHTable_Find(opacityTable, data->mFillColor));
				}

				if (opacityTable == NULL)
				{
					opacityTable = (struct AllocatorHTable*)StaticAllocator_Malloc(allocator, sizeof(struct AllocatorHTable));
					AllocatorHTable_Init(opacityTable, gsBGMaxFillColor, allocator);
					AllocatorHTable_Push(txtTable, data->mpSrcData->mOpaqType, opacityTable);
				}
				
				if (itemList == NULL)
				{
					itemList = (struct AllocatorList*)StaticAllocator_Malloc(allocator, sizeof(struct AllocatorList));
					AllocatorList_Init(itemList, sizeof(struct TRGLBGItem), allocator);
					AllocatorHTable_Push(opacityTable, data->mFillColor, itemList);
				}
				else
				{
					item = AllocatorList_Val(itemList, AllocatorList_Tail(itemList));
					if (item->tv.mVertexCt > 0)
					{						
						const s32 itemChunkSize = ((item->tv.mVertexCt + chunkVertexSize - 1) / chunkVertexSize) * chunkVertexSize;
						const GLfx32* ptrPool = gsPlane[gsActivePlane].mpBGVertexPool + gsPlane[gsActivePlane].mpBGVertexCt[data->mType];

						SDK_ASSERT(gsPlane[gsActivePlane].mpBGVertexCt[data->mType] + sgBGChunkSize < (s32)gsPlane[gsActivePlane].mBGMaxElements * (s32)gsPlane[gsActivePlane].mBGMaxLayers * chunkVertexSize);

						if (item->tv.mVertexCt + 12 > itemChunkSize)
						{
							if (item->tv.mpImgVertexArrayPtr + itemChunkSize == ptrPool)
							{
								addChunk = item->tv.mVertexCt;
							}
							else
							{
								item = NULL;
							}
						}
					}
				}

				if (item == NULL)
				{
					item = AllocatorList_Val(itemList, AllocatorList_PushBack(itemList, NULL));
					item->mpImageHeader = data->mpSrcData;
					
					item->c.mpImgColorArrayPtr = gsPlane[gsActivePlane].mpBGColorPool + data->mType * gsPlane[gsActivePlane].mBGMaxElements * sgBGChunkSize * 24 + gsPlane[gsActivePlane].mpBGColorCt[data->mType];
					item->c.mColorCt = 0;
					gsPlane[gsActivePlane].mpBGColorCt[data->mType] += chunkColorSize;

					item->c.mFillWithColor = data->mFillColor;

					txtr_idx = data->mType * gsPlane[gsActivePlane].mBGMaxElements * sgBGChunkSize * 12 + gsPlane[gsActivePlane].mpBGVertexCt[data->mType];
					item->tv.mpImgVertexArrayPtr = gsPlane[gsActivePlane].mpBGVertexPool + txtr_idx;
					item->tv.mpImgTexArrayPtr = gsPlane[gsActivePlane].mpBGTexCoordPool + txtr_idx;
					item->tv.mVertexCt = 0;

					SDK_ASSERT(gsPlane[gsActivePlane].mpBGVertexCt[data->mType] + sgBGChunkSize < (s32)gsPlane[gsActivePlane].mBGMaxElements * (s32)gsPlane[gsActivePlane].mBGMaxLayers * chunkVertexSize);
					gsPlane[gsActivePlane].mpBGVertexCt[data->mType] += chunkVertexSize;
#ifdef USE_GL_GLEXT
					item->mVBOBufferIdx = gsPlane[gsActivePlane].mBGVBOBufferLastIdx;
					gsPlane[gsActivePlane].mBGVBOBufferLastIdx += 3;
#endif
				}
#else
				item = (struct TRGLBGItem*)AllocatorHTable_Val(txtTable, AllocatorHTable_Find(txtTable, data->mpSrcData->mOpaqType));
				
				if(item == NULL)
				{
					item = (struct TRGLBGItem*)StaticAllocator_Malloc(allocator, sizeof(struct TRGLBGItem));
					AllocatorHTable_Push(txtTable, data->mpSrcData->mOpaqType, item);

					item->mpImageHeader = data->mpSrcData;

					txtr_idx = data->mType * (12 * gsPlane[gsActivePlane].mBGMaxElements) * gsPlane[gsActivePlane].mBGMaxTextures + 
						(AllocatorHTable_Size(txtTable) - 1) * (12 * gsPlane[gsActivePlane].mBGMaxElements);

					item->tv.mpImgVertexArrayPtr = gsPlane[gsActivePlane].mpBGVertexPool + txtr_idx;
					item->tv.mpImgTexArrayPtr = gsPlane[gsActivePlane].mpBGTexCoordPool + txtr_idx;
					item->tv.mVertexCt = 0;
#ifdef USE_GL_GLEXT
					item->mVBOBufferIdx = gsPlane[gsActivePlane].mBGVBOBufferLastIdx;
					gsPlane[gsActivePlane].mBGVBOBufferLastIdx += 2;
#endif
				}
#endif
				qdata = &item->tv.mpImgVertexArrayPtr[item->tv.mVertexCt];
				txtr = &item->tv.mpImgTexArrayPtr[item->tv.mVertexCt];

				SDK_ASSERT((u32)(item->tv.mVertexCt + 12) <= gsPlane[gsActivePlane].mBGMaxElements * gsPlane[gsActivePlane].mBGMaxLayers * 12); // BGRenderList vertex pool overflow

				yb = FX32_ONE / data->mpSrcData->mHeight2n / 2;
				xl = FX32_ONE / data->mpSrcData->mWidth2n / 2;

				txtr_idx = 0;

				if(item->tv.mVertexCt > 0)
				{
					const GLfx32* prev_qdata = &item->tv.mpImgVertexArrayPtr[item->tv.mVertexCt - 2];
					const GLfx32* prev_txtr = &item->tv.mpImgTexArrayPtr[item->tv.mVertexCt - 2];

					qdata[0] = qdata[2] = FX_FX32_TO_F32(data->mX);
					qdata[1] = FX_FX32_TO_F32(data->mY + FX32(data->mSrcSizeData[SRC_DATA_H]));
					txtr[0] = txtr[2] = FX_FX32_TO_F32(FX32(data->mSrcSizeData[SRC_DATA_X]) / data->mpSrcData->mWidth2n + xl);
					txtr[3] = FX_FX32_TO_F32(FX32(data->mSrcSizeData[SRC_DATA_Y]) / data->mpSrcData->mHeight2n + yb);

					if (prev_qdata[0] - qdata[0] == FX_FX32_TO_F32(FX32(data->mSrcSizeData[SRC_DATA_W])) && 
						qdata[1] - prev_qdata[1] == FX_FX32_TO_F32(FX32(data->mSrcSizeData[SRC_DATA_H])) &&
						fxAbs(prev_txtr[0] - txtr[0] - FX_FX32_TO_F32(FX32(data->mSrcSizeData[SRC_DATA_W]) / data->mpSrcData->mWidth2n)) < F32_EPSILON && 
						fxAbs(txtr[3] - prev_txtr[1]) < F32_EPSILON)
					{						
						qdata[3] = FX_FX32_TO_F32(data->mY);
						txtr[1] = FX_FX32_TO_F32(FX32((data->mSrcSizeData[SRC_DATA_H] + data->mSrcSizeData[SRC_DATA_Y])) / data->mpSrcData->mHeight2n - yb);
#ifdef USE_TILEMAP_ATTRIBUTES
#ifdef DEBUG_BG_CHUNKS
						{
							struct DrawImageFunctionData fd;
							fd.mFillColor = ((ALPHA_OPAQ << 16) | ((u16)(((u32)item->tv.mpImgTexArrayPtr) % DIFD_DEF_FILL_COLOR)));
							glRender_SetColorColorArrayItem8(&fd, &item->c);
							item->c.mColorCt += 8;
						}
#else
						if (data->mFillColor != DIFD_DEF_FILL_COLOR_FULL_ALPHA)
						{
							glRender_SetColorColorArrayItem8(data, &item->c);
							item->c.mColorCt += 8;
						}
#endif
#endif
						txtr_idx = 1;
					}
					else
					{
						qdata[0] = prev_qdata[0];
						qdata[1] = prev_qdata[1];
						qdata[2] = FX_FX32_TO_F32(data->mX + FX32(data->mSrcSizeData[SRC_DATA_W]));
						qdata[3] = FX_FX32_TO_F32(data->mY + FX32(data->mSrcSizeData[SRC_DATA_H]));
						txtr[0] = txtr[1] = txtr[2] = txtr[3] = 0;
#ifdef USE_TILEMAP_ATTRIBUTES
#ifdef DEBUG_BG_CHUNKS
#else
						if (data->mFillColor != DIFD_DEF_FILL_COLOR_FULL_ALPHA)
#endif
						{
							MI_CpuFill8(&item->c.mpImgColorArrayPtr[item->c.mColorCt], 0, sizeof(GLcolor) * 8);
							item->c.mColorCt += 8;
						}
#endif
					}
					qdata += 4;
					txtr += 4;
					item->tv.mVertexCt += 4;
				}
				if (txtr_idx == 0)
				{
					qdata[4] = qdata[6] = FX_FX32_TO_F32(data->mX);
					qdata[1] = qdata[5] = FX_FX32_TO_F32(data->mY + FX32(data->mSrcSizeData[SRC_DATA_H]));
					qdata[0] = qdata[2] = FX_FX32_TO_F32(data->mX + FX32(data->mSrcSizeData[SRC_DATA_W]));
					qdata[3] = qdata[7] = FX_FX32_TO_F32(data->mY);
					txtr[4] = txtr[6] = FX_FX32_TO_F32(FX32(data->mSrcSizeData[SRC_DATA_X]) / data->mpSrcData->mWidth2n + xl);
					txtr[1] = txtr[5] = FX_FX32_TO_F32(FX32((data->mSrcSizeData[SRC_DATA_H] + data->mSrcSizeData[SRC_DATA_Y])) / data->mpSrcData->mHeight2n - yb);
					txtr[0] = txtr[2] = FX_FX32_TO_F32(FX32((data->mSrcSizeData[SRC_DATA_W] + data->mSrcSizeData[SRC_DATA_X])) / data->mpSrcData->mWidth2n - xl);
					txtr[3] = txtr[7] = FX_FX32_TO_F32(FX32(data->mSrcSizeData[SRC_DATA_Y]) / data->mpSrcData->mHeight2n + yb);
					item->tv.mVertexCt += 8;
#ifdef USE_TILEMAP_ATTRIBUTES
#ifdef DEBUG_BG_CHUNKS
					{
						struct DrawImageFunctionData fd;
						fd.mFillColor = ((ALPHA_OPAQ << 16) | ((u16)(((u32)item->tv.mpImgTexArrayPtr) % DIFD_DEF_FILL_COLOR)));
						glRender_SetColorColorArrayItem16(&fd, &item->c);
						item->c.mColorCt += 16;
					}
#else
					if (data->mFillColor != DIFD_DEF_FILL_COLOR_FULL_ALPHA)
					{
						glRender_SetColorColorArrayItem16(data, &item->c);
						item->c.mColorCt += 16;
					}
#endif
#endif
				}
#ifdef USE_TILEMAP_ATTRIBUTES
				if (addChunk != -1)
				{
					if ((addChunk + chunkVertexSize - 1) / chunkVertexSize != (item->tv.mVertexCt + chunkVertexSize - 1) / chunkVertexSize)
					{
						gsPlane[gsActivePlane].mpBGVertexCt[data->mType] += chunkVertexSize;
						gsPlane[gsActivePlane].mpBGColorCt[data->mType] += chunkColorSize;
					}
				}
#endif
			}
		}
		break;

		case DIT_Obj:
		{
			struct TRGLRenderItem *item;
			BOOL newItem;
			fx32 xl, yt, rt, yb;
#ifdef CUSTOMAFFINETRANSFORM_SUPPORT
			fx32 x0, y0;
#endif
			if(_glRender_CheckForCleanup(gsActivePlane) == FALSE)
			{
				return;
			}

			if(data->mpClipRect == NULL)
			{
				gsActiveClipRect[EXDICLIPRECTXY] = NULL_CLIP_RECT;
				gsActiveClipRect[EXDICLIPRECTWH] = NULL_CLIP_RECT;
			}
			else
			{
				gsActiveClipRect[EXDICLIPRECTXY] = data->mpClipRect[EXDICLIPRECTXY];
				gsActiveClipRect[EXDICLIPRECTWH] = data->mpClipRect[EXDICLIPRECTWH];
			}

			newItem = FALSE;
			if(!(gsPlane[gsActivePlane].mRenderListSize >= 0 && 
					gsPlane[gsActivePlane].mpRenderListPool[gsPlane[gsActivePlane].mRenderListSize].mpImageHeader != NULL &&
					gsPlane[gsActivePlane].mpRenderListPool[gsPlane[gsActivePlane].mRenderListSize].mpImageHeader->mOpaqType == data->mpSrcData->mOpaqType &&
				gsPlane[gsActivePlane].mpRenderListPool[gsPlane[gsActivePlane].mRenderListSize].mClipRect[EXDICLIPRECTXY] == gsActiveClipRect[EXDICLIPRECTXY] &&
				gsPlane[gsActivePlane].mpRenderListPool[gsPlane[gsActivePlane].mRenderListSize].mClipRect[EXDICLIPRECTWH] == gsActiveClipRect[EXDICLIPRECTWH] &&
					gsPlane[gsActivePlane].mpRenderListPool[gsPlane[gsActivePlane].mRenderListSize].c.mFillWithColor == data->mFillColor))
			{
				gsPlane[gsActivePlane].mRenderListSize++;
				newItem = TRUE;
			}

			SDK_ASSERT(gsPlane[gsActivePlane].mRenderListSize <= gsPlane[gsActivePlane].mMaxObjOnScene); // "mRenderListSize > RENDERLIST_MAX";
			if(gsPlane[gsActivePlane].mRenderListSize > gsPlane[gsActivePlane].mMaxObjOnScene)
			{
				OS_Warning("glRender: RenderListSize overflow\n");
				SDK_ASSERT(0);
				gsPlane[gsActivePlane].mRenderListSize = gsPlane[gsActivePlane].mMaxObjOnScene - 1;
				return;
			}

			SDK_ASSERT(gsPlane[gsActivePlane].mImgVertexCt + 12 <= gsPlane[gsActivePlane].mMaxObjOnScene * 12);
			if(gsPlane[gsActivePlane].mImgVertexCt + 12 > gsPlane[gsActivePlane].mMaxObjOnScene * 12)
			{
				OS_Warning("glRender: RenderList vertex pool overflow\n");
				SDK_ASSERT(0);
				return;
			}

			item = &gsPlane[gsActivePlane].mpRenderListPool[gsPlane[gsActivePlane].mRenderListSize];
			gsPlane[gsActivePlane].mDraw = 1;

			if(newItem)
			{
				item->mpImageHeader = data->mpSrcData;
				item->mClipRect[EXDICLIPRECTXY] = gsActiveClipRect[EXDICLIPRECTXY];
				item->mClipRect[EXDICLIPRECTWH] = gsActiveClipRect[EXDICLIPRECTWH];
				item->c.mFillWithColor = data->mFillColor;
				item->c.mpImgColorArrayPtr = &gsPlane[gsActivePlane].mpImgColorPool[gsPlane[gsActivePlane].mImgColorCt];
				item->c.mColorCt = 0;
				item->tv.mpImgVertexArrayPtr = &gsPlane[gsActivePlane].mpImgVertexPool[gsPlane[gsActivePlane].mImgVertexCt];
				item->tv.mpImgTexArrayPtr = &gsPlane[gsActivePlane].mpImgTexCoordPool[gsPlane[gsActivePlane].mImgVertexCt];
				item->tv.mVertexCt = 0;
#ifdef USE_GL_GLEXT
				item->mVBOBufferIdx = gsPlane[gsActivePlane].mObjVBOBufferLastIdx;
				gsPlane[gsActivePlane].mObjVBOBufferLastIdx += 3;
#endif
				item->mType = RGL_IMAGE;
				_glRender_AddToRenderList(item);
			}

#ifdef CUSTOMAFFINETRANSFORM_SUPPORT
			xl = data->mAffineOriginX;
			yt = data->mAffineOriginY;
			rt = data->mDestWidth + data->mAffineOriginX;
			yb = data->mDestHeight + data->mAffineOriginY;
			x0 = data->mX - data->mAffineOriginX + FX_Mul(xl, data->mCos) + FX_Mul(yt, -data->mSin);
			y0 = data->mY - data->mAffineOriginY + FX_Mul(xl, data->mSin) + FX_Mul(yt, data->mCos);
#else
			xl = data->mX;
			yt = data->mY;
			rt = data->mX + FX32(data->mSrcSizeData[SRC_DATA_W]);
			yb = data->mY + FX32(data->mSrcSizeData[SRC_DATA_H]);
#endif
			
			if (data->mFillColor != DIFD_DEF_FILL_COLOR_FULL_ALPHA)
			{
				if (item->tv.mVertexCt > 0)
				{
					MI_CpuFill8(&item->c.mpImgColorArrayPtr[item->c.mColorCt], 0, sizeof(GLcolor) * 8);
					gsPlane[gsActivePlane].mImgColorCt += 8;
					item->c.mColorCt += 8;
				}
				glRender_SetColorColorArrayItem16(data, &item->c);
				gsPlane[gsActivePlane].mImgColorCt += 16;
				item->c.mColorCt += 16;
			}

			if(item->tv.mVertexCt > 0)
			{				
#ifdef CUSTOMAFFINETRANSFORM_SUPPORT
				item->tv.mpImgVertexArrayPtr[0 + item->tv.mVertexCt] = item->tv.mpImgVertexArrayPtr[6 + item->tv.mVertexCt - 8];
				item->tv.mpImgVertexArrayPtr[1 + item->tv.mVertexCt] = item->tv.mpImgVertexArrayPtr[7 + item->tv.mVertexCt - 8];
				item->tv.mpImgVertexArrayPtr[2 + item->tv.mVertexCt] = FX_FX32_TO_F32(x0);
				item->tv.mpImgVertexArrayPtr[3 + item->tv.mVertexCt] = FX_FX32_TO_F32(y0);
#else
				item->tv.mpImgVertexArrayPtr[0 + item->tv.mVertexCt] = item->tv.mpImgVertexArrayPtr[/*6*/0 + item->tv.mVertexCt - /*8*/2];
				item->tv.mpImgVertexArrayPtr[1 + item->tv.mVertexCt] = item->tv.mpImgVertexArrayPtr[/*7*/1 + item->tv.mVertexCt - /*8*/2];
				item->tv.mpImgVertexArrayPtr[2 + item->tv.mVertexCt] = FX_FX32_TO_F32(xl);
				item->tv.mpImgVertexArrayPtr[3 + item->tv.mVertexCt] = FX_FX32_TO_F32(yt);
#endif
				MI_CpuFill8(&item->tv.mpImgTexArrayPtr[item->tv.mVertexCt], 0, sizeof(GLfx32) * 4);
				gsPlane[gsActivePlane].mImgVertexCt += 4;
				item->tv.mVertexCt += 4;
			}
#ifdef CUSTOMAFFINETRANSFORM_SUPPORT
			item->tv.mpImgVertexArrayPtr[0 + item->tv.mVertexCt] = FX_FX32_TO_F32(x0);
			item->tv.mpImgVertexArrayPtr[1 + item->tv.mVertexCt] = FX_FX32_TO_F32(y0);
			item->tv.mpImgVertexArrayPtr[2 + item->tv.mVertexCt] = FX_FX32_TO_F32(data->mX - data->mAffineOriginX + FX_Mul(xl, data->mCos) + FX_Mul(yb, -data->mSin));
			item->tv.mpImgVertexArrayPtr[3 + item->tv.mVertexCt] = FX_FX32_TO_F32(data->mY - data->mAffineOriginY + FX_Mul(xl, data->mSin) + FX_Mul(yb, data->mCos));
			item->tv.mpImgVertexArrayPtr[4 + item->tv.mVertexCt] = FX_FX32_TO_F32(data->mX - data->mAffineOriginX + FX_Mul(rt, data->mCos) + FX_Mul(yt, -data->mSin));
			item->tv.mpImgVertexArrayPtr[5 + item->tv.mVertexCt] = FX_FX32_TO_F32(data->mY - data->mAffineOriginY + FX_Mul(rt, data->mSin) + FX_Mul(yt, data->mCos));
			item->tv.mpImgVertexArrayPtr[6 + item->tv.mVertexCt] = FX_FX32_TO_F32(data->mX - data->mAffineOriginX + FX_Mul(rt, data->mCos) + FX_Mul(yb, -data->mSin));
			item->tv.mpImgVertexArrayPtr[7 + item->tv.mVertexCt] = FX_FX32_TO_F32(data->mY - data->mAffineOriginY + FX_Mul(rt, data->mSin) + FX_Mul(yb, data->mCos));
#else
			item->tv.mpImgVertexArrayPtr[0 + item->tv.mVertexCt] = item->tv.mpImgVertexArrayPtr[2 + item->tv.mVertexCt] = FX_FX32_TO_F32(xl);
			item->tv.mpImgVertexArrayPtr[3 + item->tv.mVertexCt] = item->tv.mpImgVertexArrayPtr[7 + item->tv.mVertexCt] = FX_FX32_TO_F32(yb);
			item->tv.mpImgVertexArrayPtr[4 + item->tv.mVertexCt] = item->tv.mpImgVertexArrayPtr[6 + item->tv.mVertexCt] = FX_FX32_TO_F32(rt);
			item->tv.mpImgVertexArrayPtr[1 + item->tv.mVertexCt] = item->tv.mpImgVertexArrayPtr[5 + item->tv.mVertexCt] = FX_FX32_TO_F32(yt);
#endif
			if(data->text) // fxFloor(data->mY) == data->mY && fxFloor(data->mX) == data->mX
			{
				item->tv.mpImgTexArrayPtr[0 + item->tv.mVertexCt] = item->tv.mpImgTexArrayPtr[2 + item->tv.mVertexCt] = FX_FX32_TO_F32(FX32(data->mSrcSizeData[SRC_DATA_X]) / data->mpSrcData->mWidth2n);
				item->tv.mpImgTexArrayPtr[4 + item->tv.mVertexCt] = item->tv.mpImgTexArrayPtr[6 + item->tv.mVertexCt] = FX_FX32_TO_F32(FX32(data->mSrcSizeData[SRC_DATA_W] + data->mSrcSizeData[SRC_DATA_X]) / data->mpSrcData->mWidth2n);
				item->tv.mpImgTexArrayPtr[3 + item->tv.mVertexCt] = item->tv.mpImgTexArrayPtr[7 + item->tv.mVertexCt] = FX_FX32_TO_F32(FX32(data->mSrcSizeData[SRC_DATA_H] + data->mSrcSizeData[SRC_DATA_Y]) / data->mpSrcData->mHeight2n);
				item->tv.mpImgTexArrayPtr[1 + item->tv.mVertexCt] = item->tv.mpImgTexArrayPtr[5 + item->tv.mVertexCt] = FX_FX32_TO_F32(FX32(data->mSrcSizeData[SRC_DATA_Y]) / data->mpSrcData->mHeight2n);
			}
			else
			{
				yb = FX32_ONE / data->mpSrcData->mHeight2n / 2;
				xl = FX32_ONE / data->mpSrcData->mWidth2n / 2;
				item->tv.mpImgTexArrayPtr[0 + item->tv.mVertexCt] = item->tv.mpImgTexArrayPtr[2 + item->tv.mVertexCt] = FX_FX32_TO_F32(FX32(data->mSrcSizeData[SRC_DATA_X]) / data->mpSrcData->mWidth2n + xl);
				item->tv.mpImgTexArrayPtr[4 + item->tv.mVertexCt] = item->tv.mpImgTexArrayPtr[6 + item->tv.mVertexCt] = FX_FX32_TO_F32(FX32(data->mSrcSizeData[SRC_DATA_W] + data->mSrcSizeData[SRC_DATA_X]) / data->mpSrcData->mWidth2n - xl);
				item->tv.mpImgTexArrayPtr[3 + item->tv.mVertexCt] = item->tv.mpImgTexArrayPtr[7 + item->tv.mVertexCt] = FX_FX32_TO_F32(FX32(data->mSrcSizeData[SRC_DATA_H] + data->mSrcSizeData[SRC_DATA_Y]) / data->mpSrcData->mHeight2n - yb);
				item->tv.mpImgTexArrayPtr[1 + item->tv.mVertexCt] = item->tv.mpImgTexArrayPtr[5 + item->tv.mVertexCt] = FX_FX32_TO_F32(FX32(data->mSrcSizeData[SRC_DATA_Y]) / data->mpSrcData->mHeight2n + yb);
			}
			gsPlane[gsActivePlane].mImgVertexCt += 8;
			item->tv.mVertexCt += 8;
		}
	}
}
//----------------------------------------------------------------------------------------------------------------

#endif /*USE_OPENGL_1_RENDER*/
#endif /*NITRO_SDK*/
