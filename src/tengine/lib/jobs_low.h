#ifndef JOBS_LOW_H
#define JOBS_LOW_H

#include "tengine_low.h"

#ifdef __cplusplus
extern "C" {
#endif

#define JOB_ANIM_TASK_BUFFER_MAX 2

struct JOBAnimStreamTask
{
	struct AnimStreamData mData[JOB_ANIM_TASK_BUFFER_MAX];
	struct BMPImage *mpFileData[JOB_ANIM_TASK_BUFFER_MAX];
	const struct BMPImage *mpLastDrawnImage;
};

struct JOBFileStreamTask
{
	struct FileStreamData mData;
};

enum JFSDMode
{
	JFSDM_MODE_NONE = 0,
	JFSDM_MODE_IMG,
	JFSDM_MODE_FILE,
	JFSDM_MODE_SND
};

#ifdef JOBS_IN_SEPARATE_THREAD
BOOL jobCriticalSectionBegin(void);	
BOOL jobCriticalSectionEnd(void);

BOOL jobBindTexturesToVRAM(void);
#endif

void jobUpdateAnimStreamTasksAndLoadTextures(void);
void jobUpdateFileStreamTasks(void);

void jobUpdateAudioStreamTasks(void);

#ifdef __cplusplus
} /* extern "C" */
#endif

#endif
