/*
tengine - cross-platform game engine
------------------------------------

Copyright (c) 2014 Papa Pascualle
Contact Email: pascualle@gmail.com

Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
*/

#include "system.h"
#include "memory.h"
#include "loadhelpers.h"
#include "sound.h"
#include "tengine.h"
#include "tengine_low.h"
#include "sound_low.h"
#include "jobs_low.h"
#include "filesystem_low.h"
#include "texts.h"
#include "jobs.h"
#ifdef USE_OPENAL_SOUND
#if defined IOS_APP
#include "OpenAL/alc.h"
#else
#include "AL/alc.h"
#endif
#if defined  WINDOWS_APP && defined __BORLANDC__
// use Coff2Omf tool to convert default OpenAL32.lib
#pragma link "../../../lib/win32/openAL/libs/Win32/Borland/OpenAL32.lib"
#endif
#endif
#ifdef USE_INFINITY_SOUND
#include <math.h>
#include "libc/include/sound.h"
#endif

//--------------------------------------------------------------------------------------

#define SOUND_CHANNEL_COUNT 16
// SOUND_TYPE_BGM1 + SOUND_TYPE_BGM2
#define STREAM_CHANNEL_COUNT 2

#ifndef USE_INFINITY_SOUND
#define STREAM_BUFFERS_COUNT (3 * STREAM_CHANNEL_COUNT) 
#define STREAM_BUFFER_SIZE (4096 * 4)
#endif

static struct SoundHandle gsSound[SOUND_CHANNEL_COUNT];
static struct SoundHandle gsMusic[STREAM_CHANNEL_COUNT];

static BOOL gsSoundInit = FALSE;

extern struct TEngineCommonData cd;

#ifdef NITRO_SDK
const static s32 SEQUENCE_ARC_NO = 0;
const static s32 STREAM_THREAD_PRIO = 10;

//--------------------------------------------------------------------------------------

static NNSSndArc		gsSndArc;
static unsigned char	*gspSndHeap = NULL;
static NNSSndHandle		gsSndHandle[SOUND_CHANNEL_COUNT];
static NNSSndStrmHandle	gsBgmHandle[STREAM_CHANNEL_COUNT];
static NNSSndHeapHandle	gsHHeap;
static SNDPlayerInfo	gsPlayerInfo;

//--------------------------------------------------------------------------------------

BOOL _StrmCallbackLoop(NNSSndArcStrmCallbackStatus status, const NNSSndArcStrmCallbackInfo* info, NNSSndArcStrmCallbackParam* param, void* arg);
void _InnerStartSeqArc(s32 i);
#endif

#if defined USE_OPENAL_SOUND || defined USE_SLES_SOUND || defined USE_INFINITY_SOUND
struct WavFileFmtStruct
{
	u8 chunkID[4];
	u32 chunkSize;
	s16 formatTag;
	u16 channels;
	u32 samplesPerSec;
	u32 avgBytesPerSec;
	u16 blockAlign;
	u16 bitsPerSample;
};

struct WavStreamFileStruct
{
	char mName[MAX_FILENAME];
	s32 mResId;
#if defined USE_OPENAL_SOUND
	s32 mFormat;
	s32 mSamplesPerSec;
#endif
#ifdef USE_INFINITY_SOUND
	s32 mChannels;
#endif
	u32 mDataOffset;
	u32 mDataSize;
};
#endif

#if defined USE_OPENAL_SOUND
static ALCdevice *gspALCdevice = NULL;
static ALCcontext *gspALCcontext = NULL;
static ALuint *gspSndBuffer = NULL;
static ALuint gsBgmBuffer[STREAM_BUFFERS_COUNT];
static ALuint gsALSndBufferCount = 0;
static ALuint gsSndSource[SOUND_CHANNEL_COUNT];
static ALuint gsBgmSource[STREAM_CHANNEL_COUNT];
static u8 *gspTempStreamBuffer[STREAM_CHANNEL_COUNT];

static BOOL _init_OpenAL(void);
static void _term_OpenAL(void);
static void _alSnd_ClearErrors(void);
static BOOL _alSnd_CheckError(const char* op);
static BOOL _isHandleValid(struct SoundHandle* ipHandle);
#endif

#ifdef USE_SLES_SOUND
struct WavDataStruct
{
	u8* mData;
	s32 mDataSize;
};
struct QueueItem
{
	u32 bid;
	struct QueueItem *next;
};

static SLObjectItf gspSLdevice = NULL;
static SLEngineItf gspSLengine = NULL;
static SLObjectItf gspSLoutput = NULL;
static struct WavDataStruct *gspWavSfxData = NULL;
static SLuint32 gsSLWavSfxFileCount = 0;
static SLObjectItf gsSndSource[SOUND_CHANNEL_COUNT];
static SLObjectItf gsBgmSource[STREAM_CHANNEL_COUNT];
static struct WavDataStruct *gspWavBgmData[STREAM_BUFFERS_COUNT];

static BOOL _getPlayer(struct SoundHandle* ipHandle, SLBufferQueueItf* p);

static struct QueueItem *gspHeadQueue[STREAM_CHANNEL_COUNT];
static struct QueueItem gsQueue[STREAM_CHANNEL_COUNT][STREAM_BUFFERS_COUNT / STREAM_CHANNEL_COUNT];
static void _resetQueue(s32 idx);
static void _shiftQueue(s32 idx);
static u32 _getCuttentIDInQueue(s32 idx);

static BOOL _init_OpenSL(void);
static void _term_OpenSL(void);
#endif

#ifdef USE_INFINITY_SOUND
struct WavDataStruct
{
	u8* mData;
	s32 mDataSize;
	s32	mChannels;
};
enum ISNotifyCode
{
	ISNCCode = 0,
	ISNCHandle = 2,
	ISNCOffset = 3
};

static void* gspSLdevice = NULL;
static struct WavDataStruct *gspWavSfxData = NULL;
static struct WavDataStruct *gspWavBgmData[STREAM_CHANNEL_COUNT];
static u32 gsSLWavSfxFileCount = 0;
static BOOL gsStreamAudioTask = FALSE;
static BOOL gsCreateBuffer = FALSE;
static BOOL gsPlayBuffer = FALSE;
static SNDBUF gsMixBuffHandle;
static s32 gsMixBuffSize = 0;
static s16* gspMixBuffer = NULL;

static void _getNotify(void* event);
static s32 _init_IS(BOOL check);
static void _term_IS(void);
#endif

#if defined USE_OPENAL_SOUND || defined USE_SLES_SOUND || defined USE_INFINITY_SOUND
static u32 gsWavStreamFileCount = 0;
static struct WavStreamFileStruct *gspWavStreamFile = NULL;
static struct WavStreamFileStruct *_sndGetStreamData(s32 id);
static s32 _sndReadWavBlock(struct SoundHandle* handle, u32 bid);
static s32 _sndProcessWavBlockStreaming(struct SoundHandle* handle, BOOL forceFromBegin, u32 bid);
#endif

static void _sndStop(struct SoundHandle* ipHandle);

//--------------------------------------------------------------------------------------

BOOL sndIsSoundSystemInit(void)
{
	return gsSoundInit;
}
//--------------------------------------------------------------------------------------

void sndSetVolume(struct SoundHandle* ipHandle, fx32 iVolume)
{
	SDK_NULL_ASSERT(ipHandle);
#ifdef USE_NO_SOUND
	(void)ipHandle;
	(void)iVolume;
#else
	if(iVolume > FX32_ONE)
	{
		iVolume = FX32_ONE;	
	}
	else if(iVolume < 0)
	{
		iVolume = 0;
	}
#endif
#ifdef NITRO_SDK
	if(!ipHandle->Active)
	{
		return;
	}
	ipHandle->Volume = iVolume;
	iVolume = (iVolume * 127) >> FX32_SHIFT;
	if(ipHandle->pSndHandle != NULL)
	{
		NNS_SndPlayerMoveVolume(ipHandle->pSndHandle, iVolume, /*iFrames*/0);
	}
	else 
	if(ipHandle->pBgmHandle != NULL)
	{
		NNS_SndArcStrmMoveVolume(ipHandle->pBgmHandle, iVolume, /*iFrames*/0);
	}
#endif
#ifdef USE_OPENAL_SOUND
	{
		const ALint handle = ipHandle->BgmHandle >= 0 ? ipHandle->BgmHandle : ipHandle->SndHandle;
		if(_isHandleValid(ipHandle))
		{
			alSourcef(gsSndSource[handle], AL_GAIN, (float)iVolume / (float)FX32_ONE);
			ipHandle->Volume = iVolume;
		}
	}
#endif
#ifdef USE_SLES_SOUND
	(void)ipHandle;
	(void)iVolume;
#endif
#ifdef USE_INFINITY_SOUND
	ipHandle->Volume = iVolume;
#endif
}
//--------------------------------------------------------------------------------------

fx32 sndGetVolume(const struct SoundHandle* ipHandle)
{
	SDK_NULL_ASSERT(ipHandle);
	return ipHandle->Volume;
}
//--------------------------------------------------------------------------------------

enum SoundType sndGetSoundType(const struct SoundHandle* ipHandle)
{
	SDK_NULL_ASSERT(ipHandle);
	return ipHandle->Type;
}
//--------------------------------------------------------------------------------------

s32 sndGetSoundId(const struct SoundHandle* ipHandle)
{
	SDK_NULL_ASSERT(ipHandle);
	return ipHandle->Id;
}
//--------------------------------------------------------------------------------------

void sndStop(struct SoundHandle* ipHandle)
{
	SDK_NULL_ASSERT(ipHandle);
#if defined USE_OPENAL_SOUND || defined USE_SLES_SOUND || defined USE_INFINITY_SOUND
#ifdef JOBS_IN_SEPARATE_THREAD
	jobCriticalSectionBegin();	
#endif
#endif
	_sndStop(ipHandle);
#if defined USE_OPENAL_SOUND || defined USE_SLES_SOUND || defined USE_INFINITY_SOUND
#ifdef JOBS_IN_SEPARATE_THREAD
	jobCriticalSectionEnd();	
#endif
#endif
}
//--------------------------------------------------------------------------------------

void _sndStop(struct SoundHandle* ipHandle)
{
#ifdef USE_NO_SOUND
	(void)ipHandle;
#else
	struct EventCallbackData ed;
	BOOL sendEvent;
	SDK_NULL_ASSERT(ipHandle);
	ed.eventType = EVENT_TYPE_ON_SOUND;
	ed.eventData.mpSoundHandle = ipHandle;
	ed.layer = -1;
	ed.initiatorId = ed.ownerId = ed.eventId = SOUND_TYPE_STOP;
	sendEvent = FALSE;
#endif
#ifdef NITRO_SDK
	if(!ipHandle->Active)
	{
		return;
	}   
	if(ipHandle->pSndHandle != NULL)
	{
		NNS_SndPlayerStopSeq(ipHandle->pSndHandle, 0);
		sendEvent = TRUE;
	}
	else if(ipHandle->pBgmHandle != NULL)
	{
		NNS_SndArcStrmStop(ipHandle->pBgmHandle, 0);
		sendEvent = TRUE;
		jobRemoveStreamAudioTask();
	}
	ipHandle->Active = FALSE;
#endif
#ifdef USE_OPENAL_SOUND
	if(ipHandle->BgmHandle >= 0 && _isHandleValid(ipHandle))
	{
		ALint queued;
		ALuint bid;
		alSourceStop(gsBgmSource[ipHandle->BgmHandle]);
		if(!_alSnd_CheckError("sndStop")) 
		{
			OS_Warning("Error: can't stop openAL sources\n");
		}
		alGetSourcei(gsBgmSource[ipHandle->BgmHandle], AL_BUFFERS_QUEUED, &queued);
		while(queued--)
		{
			alSourceUnqueueBuffers(gsBgmSource[ipHandle->BgmHandle], 1, &bid);
			if(!_alSnd_CheckError("sndStop")) 
			{
				OS_Warning("Error: can't stop openAL queue buffer\n");
			}
		}
		jobRemoveStreamAudioTask();
		sendEvent = TRUE;
	}
	else if(ipHandle->SndHandle >= 0 && _isHandleValid(ipHandle))
	{
		alSourceStop(gsSndSource[ipHandle->SndHandle]);
		if(!_alSnd_CheckError("sndStop")) 
		{
			OS_Warning("Error: can't stop openAL sources\n");
		}
		sendEvent = TRUE;
	}
#endif
#ifdef USE_SLES_SOUND
	if(gspSLdevice != NULL)
	{
		SLBufferQueueItf playerQueue;
		if(_getPlayer(ipHandle, &playerQueue))
		{
			(*playerQueue)->Clear(playerQueue);
			if(ipHandle->BgmHandle >= 0)
			{
				jobRemoveStreamAudioTask();
			}
			sendEvent = TRUE;
		}
	}
#endif
#ifdef USE_INFINITY_SOUND
	if(gspSLdevice != NULL && (ipHandle->BgmHandle >= 0 || ipHandle->SndHandle >= 0))
	{
		sendEvent = TRUE;
	}
	ipHandle->pBegin = ipHandle->pEnd = NULL;
#endif
#if defined USE_OPENAL_SOUND || defined USE_SLES_SOUND || defined USE_INFINITY_SOUND
	ipHandle->BgmHandle = -1;
	ipHandle->SndHandle = -1;
	if(ipHandle->FHandle >= 0)
	{
		FILESYSTEMFN(fclose)(ipHandle->FHandle);
		ipHandle->FHandle = -1;
		sendEvent = TRUE;
	}
	ipHandle->pStreamData = NULL;
#endif
	ipHandle->PlayingPos = 0;
	ipHandle->StrmLength = 0;
#ifndef USE_NO_SOUND
	if(cd._onEventCB != NULL && sendEvent)
	{
		cd._onEventCB(&ed);
	}
#endif
}
//--------------------------------------------------------------------------------------

void sndPause(struct SoundHandle* ipHandle, BOOL iPause)
{
#ifdef USE_NO_SOUND
	(void)ipHandle;
	(void)iPause;
#else
	struct EventCallbackData ed;
	BOOL sendEvent;
	SDK_NULL_ASSERT(ipHandle);
	ed.eventType = EVENT_TYPE_ON_SOUND;
	ed.eventData.mpSoundHandle = ipHandle;
	ed.layer = -1;
	sendEvent = FALSE;
#endif
#ifdef NITRO_SDK 
	if(!ipHandle->Active)
	{
		return;
	}
	if(ipHandle->pSndHandle != NULL)
	{
		ed.initiatorId = ed.ownerId = ed.eventId = (iPause == TRUE) ? SOUND_TYPE_PAUSE : SOUND_TYPE_PLAY;
		NNS_SndPlayerPause(ipHandle->pSndHandle, iPause);
		sendEvent = TRUE;
	}
	else 
	if(ipHandle->pBgmHandle != NULL) 
	{
		if(iPause)
		{
			ed.initiatorId = ed.ownerId = ed.eventId = SOUND_TYPE_PAUSE;
			ipHandle->PlayingPos = NNS_SndArcStrmGetCurrentPlayingPos(ipHandle->pBgmHandle);
			ipHandle->StrmLength = NNS_SndArcStrmGetTimeLength(ipHandle->pBgmHandle);
			NNS_SndArcStrmStop(ipHandle->pBgmHandle, /*iFadeFrames*/0);
			sendEvent = TRUE;
		}
		else
		{
			ed.initiatorId = ed.ownerId = ed.eventId = SOUND_TYPE_PLAY;
			if(ipHandle->StrmLength > ipHandle->PlayingPos)
			{
				switch(ipHandle->Type)
				{
					case SOUND_TYPE_BGM1:
					{
						ipHandle->Active = NNS_SndArcStrmStartEx2(ipHandle->pBgmHandle, 0, -1, ipHandle->Id, ipHandle->PlayingPos, NULL, NULL, ipHandle->Looped ? _StrmCallbackLoop : NULL, NULL);
						sendEvent = TRUE;
					}
					break;
					case SOUND_TYPE_BGM2:
					{
						ipHandle->Active = NNS_SndArcStrmStartEx2(ipHandle->pBgmHandle, 1, -1, ipHandle->Id, ipHandle->PlayingPos, NULL, NULL, ipHandle->Looped ? _StrmCallbackLoop : NULL, NULL);
						sendEvent = TRUE;
					}
					break;
					case SOUND_TYPE_SFX: break;
					case SOUND_TYPE_NONE: break;
				}
			}
		}
	}
#endif
#ifdef USE_OPENAL_SOUND
	if(_isHandleValid(ipHandle))
	{
		ALint handle = ipHandle->BgmHandle >= 0 ? ipHandle->BgmHandle : ipHandle->SndHandle;
		if(iPause)
		{
			ed.initiatorId = ed.ownerId = ed.eventId = SOUND_TYPE_PAUSE;
			alSourcePause(gsSndSource[handle]);
		}
		else
		{
			ed.initiatorId = ed.ownerId = ed.eventId = SOUND_TYPE_PLAY;
			alSourcePlay(gsSndSource[handle]);
		}
		sendEvent = TRUE;
	}
#endif
#ifdef USE_SLES_SOUND
	(void)ipHandle;
	(void)iPause;
#endif
#ifdef USE_INFINITY_SOUND
	(void)ipHandle;
	(void)iPause;
#endif
#ifndef USE_NO_SOUND
	if(cd._onEventCB != NULL && sendEvent)
	{
		cd._onEventCB(&ed);
	}
#endif
}
//--------------------------------------------------------------------------------------

void sndReleaseSoundSystem()
{
	sndLostDevice();
	gsSoundInit = FALSE;
}
//--------------------------------------------------------------------------------------

void sndLostDevice(void)
{
#ifdef NITRO_SDK 
	if(gspSndHeap != NULL)
	{
		FREE(gspSndHeap);
	}
	gspSndHeap = NULL;
#endif
#ifdef USE_OPENAL_SOUND
	SDK_ASSERT(gspSndBuffer == NULL); //please call sndDeleteDataBuffers() before
	SDK_ASSERT(gspWavStreamFile == NULL);
	if(gspALCdevice)
	{
		sndStopAll();
		_alSnd_ClearErrors();
		alDeleteSources(STREAM_CHANNEL_COUNT, gsBgmSource);
		if(!_alSnd_CheckError("sndLostDevice"))
		{
			OS_Warning("Error: alDeleteSources function\n");
		}
		alDeleteSources(SOUND_CHANNEL_COUNT, gsSndSource);
		if(!_alSnd_CheckError("sndLostDevice"))
		{
			OS_Warning("Error: alDeleteSources function\n");
		}
		_term_OpenAL();
	}
#endif
#ifdef USE_SLES_SOUND
	SDK_ASSERT(gspWavSfxData == NULL); //please call sndDeleteDataBuffers() before
	SDK_ASSERT(gspWavStreamFile == NULL);
	if(gspSLdevice != NULL)
	{
		s32 i;
		sndStopAll();
		i = SOUND_CHANNEL_COUNT;
		while(i > 0)
		{
			i--;
			if(gsSndSource[i] != NULL)
			{
				SLuint32 playerState;
				(*gsSndSource[i])->GetState(gsSndSource[i], &playerState);
				if(playerState == SL_OBJECT_STATE_REALIZED)
				{
					SLPlayItf player;
					SLresult result;
					result = (*gsSndSource[i])->GetInterface(gsSndSource[i], SL_IID_PLAY, (void*)&player);
					if (result == SL_RESULT_SUCCESS)
					{
						result = (*player)->SetPlayState(player, SL_PLAYSTATE_STOPPED);
					}
				}
				(*gsSndSource[i])->Destroy(gsSndSource[i]);
				gsSndSource[i] = NULL;
			}
		}
		i = STREAM_CHANNEL_COUNT;
		while(i > 0)
		{
			i--;
			if(gsBgmSource[i] != NULL)
			{
				SLuint32 playerState;
				(*gsBgmSource[i])->GetState(gsBgmSource[i], &playerState);
				if(playerState == SL_OBJECT_STATE_REALIZED)
				{
					SLPlayItf player;
					SLresult result;
					result = (*gsBgmSource[i])->GetInterface(gsBgmSource[i], SL_IID_PLAY, (void*)&player);
					if (result == SL_RESULT_SUCCESS)
					{
						result = (*player)->SetPlayState(player, SL_PLAYSTATE_STOPPED);
					}
				}
				(*gsBgmSource[i])->Destroy(gsBgmSource[i]);
				gsBgmSource[i] = NULL;
			}
		}
		_term_OpenSL();
	}
#endif
#ifdef USE_INFINITY_SOUND
	SDK_ASSERT(gspWavSfxData == NULL); //please call sndDeleteDataBuffers() before
	SDK_ASSERT(gspWavStreamFile == NULL);
	if(gspSLdevice != NULL)
	{
		sndStopAll();
		if(gsStreamAudioTask == TRUE)
		{
			jobRemoveStreamAudioTask();
			gsStreamAudioTask = FALSE;
		}
		gspSLdevice = NULL;
	}
#endif
}
//--------------------------------------------------------------------------------------

#ifdef NITRO_SDK 
void sndInitSoundSystem(u32 iHeapSize, const char *ipSoundArchivePath)
{
	s32 i;
	BOOL allocationResult;
	SDK_NULL_ASSERT(ipSoundArchivePath);
	gsSoundInit = TRUE;
	for(i = 0; i < SOUND_CHANNEL_COUNT; i++)
	{
		gsSound[i].pBgmHandle = NULL;
		gsSound[i].pSndHandle = &gsSndHandle[i];
		gsSound[i].Active = FALSE;
		gsSound[i].Looped = FALSE;
		gsSound[i].Volume = -FX32_ONE;
		gsSound[i].Type = SOUND_TYPE_NONE;
		NNS_SndHandleInit(gsSound[i].pSndHandle);  
	}

	for(i = 0; i < STREAM_CHANNEL_COUNT; i++)
	{
		gsMusic[i].pBgmHandle = &gsBgmHandle[i];
		gsMusic[i].pSndHandle = NULL;
		gsMusic[i].Active = FALSE;
		gsMusic[i].Looped = FALSE;
		gsMusic[i].Volume = -FX32_ONE;
		gsMusic[i].Type = SOUND_TYPE_NONE;
		NNS_SndStrmHandleInit(gsMusic[i].pBgmHandle);
	}

	NNS_SndInit();
		 
	gspSndHeap = MALLOC(iHeapSize, "InitSoundSystem:gspSndHeap");
	gsHHeap = NNS_SndHeapCreate(gspSndHeap, iHeapSize);

	NNS_SndArcInit(&gsSndArc, ipSoundArchivePath, gsHHeap, FALSE);
	NNS_SndArcStrmInit(STREAM_THREAD_PRIO, gsHHeap); 

	NNS_SndArcPlayerSetup(gsHHeap);

	allocationResult = NNS_SndArcLoadSeqArc(SEQUENCE_ARC_NO, gsHHeap);
	SDK_ASSERT(allocationResult); // increace HeapSize 

	allocationResult = NNS_SndArcLoadBank(SEQUENCE_ARC_NO, gsHHeap);
	SDK_ASSERT(allocationResult); // increace HeapSize
}
#endif
//--------------------------------------------------------------------------------------

#ifdef USE_OPENAL_SOUND

BOOL _init_OpenAL()
{
#ifdef EMSCRIPTEN_APP
#ifdef SDK_DEBUG
	ALint major, minor;
#endif
#endif
	SDK_ASSERT(gspALCdevice == NULL);
	SDK_ASSERT(gspALCcontext == NULL);
#ifdef EMSCRIPTEN_APP
#ifdef SDK_DEBUG
	alcGetIntegerv(NULL, ALC_MAJOR_VERSION, 1, &major);
	alcGetIntegerv(NULL, ALC_MAJOR_VERSION, 1, &minor);
	if(major != 1)
	{
		OS_Printf("ALC verification error: %s\n", "Unsupported ALC version");
		SDK_ASSERT(major == 1);
	}
	OS_Printf("ALC version: %i.%i\n", major, minor);
	OS_Printf("Default device: %s\n", alcGetString(NULL, ALC_DEFAULT_DEVICE_SPECIFIER));
#endif
#endif
	gspALCdevice = alcOpenDevice(NULL);
	if(gspALCdevice == NULL)
	{
		return FALSE;
	}
	gspALCcontext = alcCreateContext(gspALCdevice, NULL);
	if(gspALCcontext == NULL)
	{
		alcCloseDevice(gspALCdevice);
		return FALSE;
	}
	alcMakeContextCurrent(gspALCcontext);
#ifdef SDK_DEBUG
	SDK_ASSERT(alGetString(AL_VERSION));
	OS_Printf("OpenAL version: %s\n", alGetString(AL_VERSION));
	OS_Printf("OpenAL vendor: %s\n", alGetString(AL_VENDOR));
	OS_Printf("OpenAL renderer: %s\n", alGetString(AL_RENDERER));
#endif
	return TRUE;
}
//--------------------------------------------------------------------------------------

void _term_OpenAL(void)
{
	alcMakeContextCurrent(NULL);
	if(gspALCcontext != NULL)
	{
		alcDestroyContext(gspALCcontext);
	}
	if(gspALCdevice != NULL)
	{
		alcCloseDevice(gspALCdevice);
	}
	gspALCdevice = NULL;
	gspALCcontext = NULL;
}
//--------------------------------------------------------------------------------------

void _alSnd_ClearErrors(void)
{
#ifdef SDK_DEBUG
	alGetError();
#endif
}
//--------------------------------------------------------------------------------------

BOOL _alSnd_CheckError(const char* op)
{
#ifdef SDK_DEBUG
	ALenum ErrCode;
	if((ErrCode = alGetError()) != AL_NO_ERROR)
	{
		OS_Warning("after %s() openAL error: %s\n", op, alGetString(ErrCode));
		return FALSE;
	}
#else
	(void)op;
#endif
	return TRUE;
}
//--------------------------------------------------------------------------------------

BOOL _isHandleValid(struct SoundHandle* ipHandle)
{
	ALint state;
	SDK_NULL_ASSERT(ipHandle);
	if(gspALCdevice == NULL)
	{
		return FALSE;
	}
	SDK_ASSERT(ipHandle->BgmHandle >= 0 || ipHandle->SndHandle >= 0);
	if(ipHandle->BgmHandle >= 0)
	{
		alGetSourcei(gsBgmSource[ipHandle->BgmHandle], AL_SOURCE_STATE, &state);
		switch(state)
		{
			case AL_INITIAL:
			case AL_PLAYING:
			case AL_PAUSED:
				return TRUE;
		}
	}
	else if(ipHandle->SndHandle >= 0)
	{
		alGetSourcei(gsSndSource[ipHandle->SndHandle], AL_SOURCE_STATE, &state);
		switch(state)
		{
			case AL_INITIAL:
			case AL_PLAYING:
			case AL_PAUSED:
				return TRUE;
		}
	}
	return FALSE;
}
//--------------------------------------------------------------------------------------

void sndInitSoundSystem()
{
	s32 i;
	const ALfloat listenerPos[] = {0.0f, 0.0f, 0.0f};
	const ALfloat listenerVel[] = {0.0f, 0.0f, 0.0f};
	const ALfloat listenerOri[] = {0.0f, 0.0f, -1.0f, 0.0f, 1.0f, 0.0f};

	if(_init_OpenAL() == FALSE)
	{
		_term_OpenAL();
		OS_Warning("sndInitSoundSystem error: %s\n", "can't init openAL engine");
		gsSoundInit = FALSE;
		return;
	}

	gsSoundInit = TRUE;

	alListenerfv(AL_POSITION, listenerPos);
	alListenerfv(AL_VELOCITY, listenerVel);
	alListenerfv(AL_ORIENTATION, listenerOri);
	
	_alSnd_ClearErrors();

	alGenSources(SOUND_CHANNEL_COUNT, gsSndSource);
	if(!_alSnd_CheckError("sndInitSoundSystem")) 
	{
		OS_Warning("sndInitSoundSystem error: %s\n", "can't generate openAL sources");
		SDK_ASSERT(0);
	}

	alGenSources(STREAM_CHANNEL_COUNT, gsBgmSource);
	if(!_alSnd_CheckError("sndInitSoundSystem")) 
	{
		OS_Warning("sndInitSoundSystem error: %s\n", "can't generate openAL sources");
		SDK_ASSERT(0);
	}

	for(i = 0; i < SOUND_CHANNEL_COUNT; i++)
	{
		gsSound[i].SndHandle = -1;
		gsSound[i].BgmHandle = -1;
		gsSound[i].Looped = FALSE;
		gsSound[i].Volume = -FX32_ONE;
		gsSound[i].Type = SOUND_TYPE_NONE;
		gsSound[i].pStreamData = NULL;
		gsSound[i].FHandle = -1;
	}
	for(i = 0; i < STREAM_CHANNEL_COUNT; i++)
	{
		gsMusic[i].SndHandle = -1;
		gsMusic[i].BgmHandle = -1;
		gsMusic[i].Looped = FALSE;
		gsMusic[i].Volume = -FX32_ONE;
		gsMusic[i].Type = SOUND_TYPE_NONE;
		gsMusic[i].pStreamData = NULL;
		gsMusic[i].FHandle = -1;
	}
}
#endif
//--------------------------------------------------------------------------------------

#ifdef USE_SLES_SOUND

BOOL _init_OpenSL()
{
	SLresult result;
	SLInterfaceID pIDs[1];
	SLboolean pIDsRequired[1];
	SLInterfaceID *pOutputMixIDs = NULL;
	SLboolean *pOutputMixRequired = NULL;

	SDK_ASSERT(gspSLdevice == NULL);
	SDK_ASSERT(gspSLengine == NULL);
	SDK_ASSERT(gspSLoutput == NULL);

	pIDs[0] = SL_IID_ENGINE;
	pIDsRequired[0] = SL_BOOLEAN_TRUE;

	result = slCreateEngine(
		&gspSLdevice,
		0,		/* additional options count */
		NULL,	/* ptr to additional options */
		1,		/* interfaces count */
		pIDs,	/* ID interfaces array */
		pIDsRequired /* corresponded table for results (per each interfaces) */
	);
	if(result != SL_RESULT_SUCCESS)
	{
		gspSLdevice = NULL;
		return FALSE;
	}
	result = (*gspSLdevice)->Realize(gspSLdevice, SL_BOOLEAN_FALSE); // SL_BOOLEAN_FALSE = synch mode
	if(result != SL_RESULT_SUCCESS)
	{
		_term_OpenSL();
		return FALSE;
	}
	result = (*gspSLdevice)->GetInterface(gspSLdevice, SL_IID_ENGINE, (void*)&gspSLengine);
	if(result != SL_RESULT_SUCCESS)
	{
		_term_OpenSL();
		return FALSE;
	}
	result = (*gspSLengine)->CreateOutputMix(gspSLengine, &gspSLoutput, 0, pOutputMixIDs, pOutputMixRequired);
	if(result != SL_RESULT_SUCCESS)
	{
		_term_OpenSL();
		return FALSE;
	}
	result = (*gspSLoutput)->Realize(gspSLoutput, SL_BOOLEAN_FALSE);
	if(result != SL_RESULT_SUCCESS)
	{
		_term_OpenSL();
		return FALSE;
	}
	return TRUE;
}
//--------------------------------------------------------------------------------------

void _term_OpenSL(void)
{
	if(gspSLoutput != NULL)
	{
		(*gspSLoutput)->Destroy(gspSLoutput);
		gspSLoutput = NULL;
	}
	if(gspSLdevice != NULL)
	{
		(*gspSLdevice)->Destroy(gspSLdevice);
		gspSLdevice = NULL;
	}
	gspSLengine = NULL;
}
//--------------------------------------------------------------------------------------

BOOL _getPlayer(struct SoundHandle* ipHandle, SLBufferQueueItf* p)
{
	SLObjectItf obj;
	SDK_NULL_ASSERT(ipHandle);
	obj = NULL;
	if(ipHandle->BgmHandle >= 0)
	{
		obj = gsBgmSource[ipHandle->BgmHandle];
	}
	else if(ipHandle->SndHandle >= 0)
	{
		obj = gsSndSource[ipHandle->SndHandle];
	}
	if(obj)
	{
		SLuint32 playerState;
		(*obj)->GetState(obj, &playerState);
		if(playerState == SL_OBJECT_STATE_REALIZED)
		{
			if((*obj)->GetInterface(obj, SL_IID_BUFFERQUEUE, (void*)p) == SL_RESULT_SUCCESS)
			{
				return TRUE;
			}
		}
	}
	return FALSE;
}
//--------------------------------------------------------------------------------------

void _resetQueue(s32 idx)
{
	u32 i;
	gspHeadQueue[idx] = &gsQueue[idx][0];	
	for(i = 0; i < STREAM_BUFFERS_COUNT / STREAM_CHANNEL_COUNT; i++)
	{
		if(STREAM_BUFFERS_COUNT / STREAM_CHANNEL_COUNT > i + 1)
		{
			gsQueue[idx][i].next = &gsQueue[idx][i + 1];
		}
		else
		{
			gsQueue[idx][i].next = &gsQueue[idx][0];
		}
		gsQueue[idx][i].bid = idx * (STREAM_BUFFERS_COUNT / STREAM_CHANNEL_COUNT) + i;
	}
}
//--------------------------------------------------------------------------------------

void _shiftQueue(s32 idx)
{
	gspHeadQueue[idx] = gspHeadQueue[idx]->next;
}
//--------------------------------------------------------------------------------------

u32 _getCuttentIDInQueue(s32 idx)
{
	return gspHeadQueue[idx]->bid;
}
//--------------------------------------------------------------------------------------

void sndInitSoundSystem()
{
	s32 i;

	if(!_init_OpenSL())
	{
		OS_Warning("Error: can't init openSL ES engine\n");
		gsSoundInit = FALSE;
		return;
	}

	gsSoundInit = TRUE;

	{
		s32 err_ct;
		SLresult result;
		SLDataFormat_PCM formatPCM;
		SLInterfaceID pIDs[2];
		SLboolean pIDsRequired[2];
		SLDataLocator_AndroidSimpleBufferQueue locatorBufferQueue;
		SLDataSource audioSrc;
		SLDataLocator_OutputMix locatorOutMix;
		SLDataSink audioSnk;

		/*
		So why not use a MIME source instead of a PCM source? Well, this is
		because a buffer queue works only with PCM data. Although improvements can be expected
		in the future, audio file decoding still need to be performed by hand. Trying to connect a
		MIME source to a buffer queue (like we are going to do with the recorder) will cause an
		SL_RESULT_FEATURE_UNSUPPORTED error
		*/
		formatPCM.formatType = SL_DATAFORMAT_PCM; 
		/* restriction1: mono only */
		formatPCM.numChannels = 1; 
		/* restriction2: 44khz only */
		formatPCM.samplesPerSec = SL_SAMPLINGRATE_44_1; 
		/* restriction3: 16 bit obly */
		formatPCM.bitsPerSample = SL_PCMSAMPLEFORMAT_FIXED_16;
		formatPCM.containerSize = SL_PCMSAMPLEFORMAT_FIXED_16;
		formatPCM.channelMask = SL_SPEAKER_FRONT_CENTER;
		formatPCM.endianness = SL_BYTEORDER_LITTLEENDIAN;
		/*
		NOTE: OpenSL ES has been updated in NDK R7 and now allows decoding
		compressed files such as MP3 files to PCM buffers
		*/

		locatorBufferQueue.locatorType = SL_DATALOCATOR_ANDROIDSIMPLEBUFFERQUEUE;
		locatorBufferQueue.numBuffers = STREAM_BUFFERS_COUNT / STREAM_CHANNEL_COUNT;

		audioSrc.pLocator = &locatorBufferQueue;
		audioSrc.pFormat = &formatPCM;

		locatorOutMix.locatorType = SL_DATALOCATOR_OUTPUTMIX;
		locatorOutMix.outputMix = gspSLoutput;

		audioSnk.pLocator = &locatorOutMix;
		audioSnk.pFormat = NULL;

		pIDs[1] = SL_IID_BUFFERQUEUE;
		pIDsRequired[1] = SL_BOOLEAN_TRUE;
		pIDs[0] = SL_IID_PLAY;
		pIDsRequired[0] = SL_BOOLEAN_TRUE;
		
		err_ct = 0;
		for(i = 0; i < STREAM_CHANNEL_COUNT; i++)
		{
			result = (*gspSLengine)->CreateAudioPlayer(gspSLengine, &gsBgmSource[i], &audioSrc, &audioSnk, 2, pIDs, pIDsRequired);
			if(result != SL_RESULT_SUCCESS)
			{
				gsBgmSource[i] = NULL;
				err_ct++;
			}
			else
			{
				BOOL error = TRUE;
				result = (*gsBgmSource[i])->Realize(gsBgmSource[i], SL_BOOLEAN_FALSE);
				if(result == SL_RESULT_SUCCESS)
				{
					SLPlayItf player;
					result = (*gsBgmSource[i])->GetInterface(gsBgmSource[i], SL_IID_PLAY, (void*)&player);
					if (result == SL_RESULT_SUCCESS)
					{
						/* This does not actually mean that a sound is played. The queue is empty so that would not be possible */
						result = (*player)->SetPlayState(player, SL_PLAYSTATE_PLAYING);
						if (result == SL_RESULT_SUCCESS) 
						{
							error = FALSE;	
						}
					}
				}
				if(error)
				{
					(*gsBgmSource[i])->Destroy(gsBgmSource[i]);
					gsBgmSource[i] = NULL;
					err_ct++;
				}
			}
		}
		if(err_ct == STREAM_CHANNEL_COUNT)
		{
			OS_Warning("Error: can't create bgm AudioPlayer objects\n");
		}

		/* one buf */
		locatorBufferQueue.locatorType = SL_DATALOCATOR_ANDROIDSIMPLEBUFFERQUEUE;
		locatorBufferQueue.numBuffers = 1;

		err_ct = 0;
		for(i = 0; i < SOUND_CHANNEL_COUNT; i++)
		{
			result = (*gspSLengine)->CreateAudioPlayer(gspSLengine, &gsSndSource[i], &audioSrc, &audioSnk, 2, pIDs, pIDsRequired);
			if(result != SL_RESULT_SUCCESS)
			{
				gsSndSource[i] = NULL;
				err_ct++;
			}
			else
			{
				BOOL error = TRUE;
				result = (*gsSndSource[i])->Realize(gsSndSource[i], SL_BOOLEAN_FALSE);
				if(result == SL_RESULT_SUCCESS)
				{
					SLPlayItf player;
					result = (*gsSndSource[i])->GetInterface(gsSndSource[i], SL_IID_PLAY, (void*)&player);
					if (result == SL_RESULT_SUCCESS)
					{
						/* This does not actually mean that a sound is played. The queue is empty so that would not be possible */
						result = (*player)->SetPlayState(player, SL_PLAYSTATE_PLAYING);
						if (result == SL_RESULT_SUCCESS) 
						{
							error = FALSE;	
						}
					}
				}
				if(error)
				{
					(*gsSndSource[i])->Destroy(gsSndSource[i]);
					gsSndSource[i] = NULL;
					err_ct++;
				}
			}
		}
		if(err_ct == SOUND_CHANNEL_COUNT)
		{
			OS_Warning("Error: can't create sfx AudioPlayer objects\n");
		}
	}

	for(i = 0; i < SOUND_CHANNEL_COUNT; i++)
	{
		gsSound[i].SndHandle = -1;
		gsSound[i].BgmHandle = -1;
		gsSound[i].Looped = FALSE;
		gsSound[i].Volume = -FX32_ONE;
		gsSound[i].Type = SOUND_TYPE_NONE;
		gsSound[i].pStreamData = NULL;
		gsSound[i].FHandle = -1;
	}
	for(i = 0; i < STREAM_CHANNEL_COUNT; i++)
	{
		gsMusic[i].SndHandle = -1;
		gsMusic[i].BgmHandle = -1;
		gsMusic[i].Looped = FALSE;
		gsMusic[i].Volume = -FX32_ONE;
		gsMusic[i].Type = SOUND_TYPE_NONE;
		gsMusic[i].pStreamData = NULL;
		gsMusic[i].FHandle = -1;
		_resetQueue(i);
	}
}
#endif
//--------------------------------------------------------------------------------------

void sndRestoreDevice(void)
{
#ifndef NITRO_SDK 
	sndInitSoundSystem();
#endif
}
//--------------------------------------------------------------------------------------

#if defined USE_OPENAL_SOUND || defined USE_SLES_SOUND || defined USE_INFINITY_SOUND
struct WavStreamFileStruct *_sndGetStreamData(s32 id)
{
	u32 i;
	for(i = 0; i < gsWavStreamFileCount; i++)
	{
		if(gspWavStreamFile[i].mResId == id)
		{
			return &gspWavStreamFile[i];
		}
	}
	return NULL;
}
//--------------------------------------------------------------------------------------

s32 _sndProcessWavBlockStreaming(struct SoundHandle* handle, BOOL forceFromBegin, u32 bid)
{
	SDK_NULL_ASSERT(handle);
	SDK_NULL_ASSERT(handle->pStreamData);
	if(handle->FHandle < 0)
	{
#if defined USE_OPENAL_SOUND || defined USE_SLES_SOUND
		s32 i;
#endif
		s32 res;
		if((handle->FHandle = FILESYSTEMFN(fopen)(handle->pStreamData->mName)) < 0)
		{
			SDK_ASSERT(0);
			return 0;
		}
		FILESYSTEMFN(fseek)(handle->pStreamData->mDataOffset, handle->FHandle);
#ifdef USE_OPENAL_SOUND 
		_alSnd_ClearErrors();
		res = 0;
		for(i = 0; i < STREAM_BUFFERS_COUNT / STREAM_CHANNEL_COUNT; i++)
		{
			ALuint nbid = gsBgmBuffer[handle->BgmHandle * (STREAM_BUFFERS_COUNT / STREAM_CHANNEL_COUNT) + i]; 
			res += _sndReadWavBlock(handle, nbid);
			alSourceQueueBuffers(gsBgmSource[handle->BgmHandle], 1, &nbid);
			if(!_alSnd_CheckError("sndReadWavBlock:alSourceQueueBuffers"))
			{
				FILESYSTEMFN(fclose)(handle->FHandle);
				handle->FHandle = -1;
				SDK_ASSERT(0);
				return 0;
			}
		}
		alSourcePlay(gsBgmSource[handle->BgmHandle]);
#endif
#ifdef USE_SLES_SOUND
		{
			BOOL error;
			SLBufferQueueItf playerQueue;	
			error = TRUE;
			res = 0;
			if(_getPlayer(handle, &playerQueue))
			{
				for(i = 0; i < STREAM_BUFFERS_COUNT / STREAM_CHANNEL_COUNT; i++)
				{
					SLresult result;
					error = TRUE;
					bid = handle->BgmHandle * (STREAM_BUFFERS_COUNT / STREAM_CHANNEL_COUNT) + i;
					res += _sndReadWavBlock(handle, bid);
					result = (*playerQueue)->Enqueue(playerQueue, gspWavBgmData[bid]->mData, gspWavBgmData[bid]->mDataSize);
					if(result == SL_RESULT_SUCCESS)
					{
						error = FALSE;
					}
					else
					{
						break;
					}
				}
				_resetQueue(handle->BgmHandle);
			}
			if(error == TRUE)
			{
				FILESYSTEMFN(fclose)(handle->FHandle);
				handle->FHandle = -1;
				SDK_ASSERT(0);
				return 0;
			}
		}
#endif
#ifdef USE_INFINITY_SOUND 
		res = _sndReadWavBlock(handle, bid);
#endif
		return res; 
	}
	else
	{
		if(forceFromBegin)
		{
			handle->PlayingPos = 0;
			FILESYSTEMFN(fseek)(handle->pStreamData->mDataOffset, handle->FHandle);
		}
	}
	return _sndReadWavBlock(handle, bid);
}
#endif
//--------------------------------------------------------------------------------------

#if defined USE_OPENAL_SOUND || defined USE_SLES_SOUND || defined USE_INFINITY_SOUND
s32 _sndReadWavBlock(struct SoundHandle* handle, u32 bid)
{
	s32 curr, total;
#ifdef USE_INFINITY_SOUND
	const s32 strm_buf_size = (gsMixBuffSize * handle->pStreamData->mChannels) / 2;
#else
	const s32 strm_buf_size = STREAM_BUFFER_SIZE;
#endif
	total = 0;
#if defined USE_SLES_SOUND || defined USE_INFINITY_SOUND
	gspWavBgmData[bid]->mDataSize = 0;
#endif
	while(total < strm_buf_size) 
	{
#ifdef USE_OPENAL_SOUND		
		curr = FILESYSTEMFN(fread)(&gspTempStreamBuffer[handle->BgmHandle][0], strm_buf_size, handle->FHandle);
#endif
#if defined USE_SLES_SOUND || defined USE_INFINITY_SOUND
		curr = FILESYSTEMFN(fread)(gspWavBgmData[bid]->mData, strm_buf_size, handle->FHandle);
#endif
		if(curr == 0)
		{
			break;
		}
		else if (curr < 0)
		{
			SDK_ASSERT(0);
			return 0;
		}
		else
		{
			total += curr;
		}
	}
	if(total > 0)
	{
		handle->PlayingPos += total;
		if(handle->PlayingPos > handle->pStreamData->mDataSize)
		{
			curr = handle->PlayingPos - handle->pStreamData->mDataSize;
			handle->PlayingPos -= curr;
			total -= curr;
			if(total <= 0)
			{
				return 0;
			}
		}
#ifdef USE_OPENAL_SOUND 
		_alSnd_ClearErrors();
		alBufferData(bid, handle->pStreamData->mFormat, &gspTempStreamBuffer[handle->BgmHandle][0], total, handle->pStreamData->mSamplesPerSec);
		if(!_alSnd_CheckError("sndReadWavBlock:alBufferData"))
		{
			SDK_ASSERT(0);
		}
#endif
#if defined USE_SLES_SOUND || defined USE_INFINITY_SOUND
		gspWavBgmData[bid]->mDataSize = total;
#endif
	}
	return total;
}
#endif
//--------------------------------------------------------------------------------------

void sndCreateDataBuffers(u32 sfxFileCount, u32 bgmFileCount)
{
#if defined USE_NO_SOUND || defined NITRO_SDK
	(void)sfxFileCount;
	(void)bgmFileCount;
#endif
#ifdef USE_OPENAL_SOUND
	if(gspALCdevice != NULL)
	{
		SDK_ASSERT(gspSndBuffer == NULL);
		SDK_ASSERT(gspWavStreamFile == NULL);
		gsALSndBufferCount = sfxFileCount;
		_alSnd_ClearErrors();
		if(gsALSndBufferCount > 0)
		{
			gspSndBuffer = (ALuint*)MALLOC(gsALSndBufferCount * sizeof(ALuint), "snd::gspSndBuffer");
			alGenBuffers(gsALSndBufferCount, gspSndBuffer);
			if(!_alSnd_CheckError("sndCreateSoundDataBuffers"))
			{
				SDK_ASSERT(0);
			}
#ifdef SDK_DEBUG
			OS_Printf("Create SFX buffers = %d\n", gsALSndBufferCount);
#endif
		}
		gsWavStreamFileCount = bgmFileCount;
		if(gsWavStreamFileCount > 0)
		{
			u32 i;
			gspWavStreamFile = (struct WavStreamFileStruct *)MALLOC(sizeof(struct WavStreamFileStruct) * gsWavStreamFileCount, "gspWavStreamFile");
			for(i = 0; i < gsWavStreamFileCount; i++)
			{
				MI_CpuClear8(&gspWavStreamFile[i], sizeof(struct WavStreamFileStruct));
				gspWavStreamFile[i].mResId = -1;
			}
			for(i = 0; i < STREAM_CHANNEL_COUNT; i++)
			{
				gspTempStreamBuffer[i] = (u8*)MALLOC(STREAM_BUFFER_SIZE, "gspTempStreamBuffer"); 
			}
			alGenBuffers(STREAM_BUFFERS_COUNT, gsBgmBuffer);
			if(!_alSnd_CheckError("sndCreateSoundDataBuffers"))
			{
				SDK_ASSERT(0);
			}
#ifdef SDK_DEBUG
			OS_Printf("Create BGM buffers = %d\n", STREAM_BUFFERS_COUNT);
#endif
		}
	}
#endif
#if defined USE_SLES_SOUND || defined USE_INFINITY_SOUND
	if(gspSLdevice != NULL)
	{
		SDK_ASSERT(gspWavSfxData == NULL);
		SDK_ASSERT(gspWavStreamFile == NULL);
		gsSLWavSfxFileCount = sfxFileCount;
		if(gsSLWavSfxFileCount > 0)
		{
			gspWavSfxData = (struct WavDataStruct*)MALLOC(gsSLWavSfxFileCount * sizeof(struct WavDataStruct), "gspWavSfxData");
		}
		gsWavStreamFileCount = bgmFileCount;
		if(gsWavStreamFileCount > 0)
		{
			u32 i, strm_bf_ct, strm_bf_size;
			gspWavStreamFile = (struct WavStreamFileStruct *)MALLOC(sizeof(struct WavStreamFileStruct) * gsWavStreamFileCount, "gspWavStreamFile");
			for(i = 0; i < gsWavStreamFileCount; i++)
			{
				MI_CpuClear8(&gspWavStreamFile[i], sizeof(struct WavStreamFileStruct));
				gspWavStreamFile[i].mResId = -1;
			}
#ifdef USE_INFINITY_SOUND
			strm_bf_ct = STREAM_CHANNEL_COUNT;
			strm_bf_size = (u32)gsMixBuffSize;
#else
			strm_bf_ct = STREAM_BUFFERS_COUNT;
			strm_bf_size = STREAM_BUFFER_SIZE;
#endif
			for(i = 0; i < strm_bf_ct; i++)
			{
				gspWavBgmData[i] = (struct WavDataStruct*)MALLOC(sizeof(struct WavDataStruct), "gspWavBgmData");
				gspWavBgmData[i]->mData = NULL;
				if(strm_bf_size > 0)
				{
					gspWavBgmData[i]->mData = (u8*)MALLOC(strm_bf_size, "gspWavBgmData->mData");
				}
				gspWavBgmData[i]->mDataSize = 0;
			}
		}
#ifdef USE_INFINITY_SOUND
		SDK_ASSERT(gspMixBuffer == NULL);
		if(gsMixBuffSize > 0)
		{
			gspMixBuffer = (s16*)MALLOC(gsMixBuffSize, "gspMixBuffer");
		}
#endif
	}
#endif
}
//--------------------------------------------------------------------------------------

void sndDeleteDataBuffers()
{
#ifdef USE_OPENAL_SOUND
	if(gspALCdevice != NULL)
	{
		s32 i;
		for(i = 0; i < SOUND_CHANNEL_COUNT; i++)
		{
			_sndStop(&gsSound[i]);
		}
		for(i = 0; i < STREAM_CHANNEL_COUNT; i++)
		{
			_sndStop(&gsMusic[i]);
		}
		_alSnd_ClearErrors();
		if(gsWavStreamFileCount > 0 && gspWavStreamFile != NULL)
		{
			for(i = 0; i < STREAM_CHANNEL_COUNT; i++)
			{
				alSourcei(gsBgmSource[i], AL_BUFFER, 0);
				if(!_alSnd_CheckError("sndDeleteDataBuffers"))
				{
					OS_Warning("Error: can't clear queue buffers\n");
				}
			}
			alDeleteBuffers(STREAM_BUFFERS_COUNT, gsBgmBuffer);
			if(!_alSnd_CheckError("sndDeleteDataBuffers"))
			{
				OS_Warning("Error: alDeleteBuffers BGM function\n");
			}
			i = STREAM_CHANNEL_COUNT;
			while(i > 0)
			{
				i--;
				FREE(gspTempStreamBuffer[i]);
			}
			FREE(gspWavStreamFile);
			gspWavStreamFile = NULL;
			gsWavStreamFileCount = 0;
		}
		if(gsALSndBufferCount > 0 && gspSndBuffer != NULL)
		{
			for(i = 0; i < SOUND_CHANNEL_COUNT; i++)
			{
				alSourcei(gsSndSource[i], AL_BUFFER, 0);
				if(!_alSnd_CheckError("sndDeleteDataBuffers"))
				{
					OS_Warning("Error: can't clear sfx buffers\n");
				}
			}
			alDeleteBuffers(gsALSndBufferCount, gspSndBuffer);
			if(!_alSnd_CheckError("sndDeleteDataBuffers"))
			{
				OS_Warning("Error: alDeleteBuffers SFX function\n");
			}
			gsALSndBufferCount = 0;
			FREE(gspSndBuffer);
			gspSndBuffer = NULL;
		}
	}
#endif
#if defined USE_SLES_SOUND || defined USE_INFINITY_SOUND
	if(gspSLdevice != NULL)
	{
		s32 i;
#ifdef USE_INFINITY_SOUND
		if(gsStreamAudioTask == TRUE)
		{
			jobRemoveStreamAudioTask();
			gsStreamAudioTask = FALSE;
		}
		if(gspMixBuffer != NULL)
		{
			if(gsCreateBuffer == TRUE)
			{
				StopBuffer(gsMixBuffHandle);
			}
			FREE(gspMixBuffer);
			gspMixBuffer = NULL;
			gsMixBuffSize = 0;
		}
		_term_IS();
#endif
		for(i = 0; i < SOUND_CHANNEL_COUNT; i++)
		{
			_sndStop(&gsSound[i]);
		}
		for(i = 0; i < STREAM_CHANNEL_COUNT; i++)
		{
			_sndStop(&gsMusic[i]);
		}
		if(gsWavStreamFileCount > 0 && gspWavStreamFile != NULL)
		{
#ifdef USE_INFINITY_SOUND
			i = STREAM_CHANNEL_COUNT;
#else
			i = STREAM_BUFFERS_COUNT;
#endif
			while(i > 0)
			{
				i--;
				if(gspWavBgmData[i]->mData != NULL)
				{
					FREE(gspWavBgmData[i]->mData);
				}
				FREE(gspWavBgmData[i]);
				gspWavBgmData[i] = NULL;
			}
			FREE(gspWavStreamFile);
			gspWavStreamFile = NULL;
			gsWavStreamFileCount = 0;
		}
		if(gsSLWavSfxFileCount > 0 && gspWavSfxData != NULL)
		{
			gsSLWavSfxFileCount = 0;
			FREE(gspWavSfxData);
			gspWavSfxData = NULL;
		}
	}
#endif
}
//--------------------------------------------------------------------------------------

BOOL sndIsBGMPlaying()
{
#ifdef USE_OPENAL_SOUND
	s32 i;
	for(i = 0; i < STREAM_CHANNEL_COUNT && gspALCdevice; i++)
	{
		if(gsMusic[i].BgmHandle >= 0)
		{
			return TRUE;
		}
	}
#endif
#if defined USE_SLES_SOUND || defined USE_INFINITY_SOUND
	s32 i;
	for(i = 0; i < STREAM_CHANNEL_COUNT && gspSLdevice; i++)
	{
		if(gsMusic[i].BgmHandle >= 0)
		{
			return TRUE;
		}
	}
#endif
	return FALSE;
}
//--------------------------------------------------------------------------------------

void sndUpdateSoundStream()
{
#ifdef NITRO_SDK 
	s32 i;
	NNS_SndMain();
	for(i = 0; i < SOUND_CHANNEL_COUNT; i++)
	{
		if(gsSound[i].Active)
		{
			if(!NNS_SndPlayerCountPlayingSeqByPlayerNo(i))
			{
				if(gsSound[i].Looped)
				{
					_InnerStartSeqArc(i);
				}
				else
				{
					gsSound[i].Active = FALSE;	
				}
			}		
		}
	}
#endif
#ifdef USE_OPENAL_SOUND
	s32 i;
	for(i = 0; i < STREAM_CHANNEL_COUNT && gspALCdevice; i++)
	{
		if(gsMusic[i].BgmHandle >= 0)
		{
			if(gsMusic[i].FHandle < 0)
			{
				_sndProcessWavBlockStreaming(&gsMusic[i], FALSE, 0);
			}
			else
			{
				ALint processed;
				ALuint bid;
				BOOL endOfFile;
				ALuint mid = gsBgmSource[gsMusic[i].BgmHandle];
				alGetSourcei(mid, AL_BUFFERS_QUEUED, &processed);
				endOfFile = gsMusic[i].PlayingPos >= gsMusic[i].pStreamData->mDataSize;
				if(endOfFile && gsMusic[i].Looped == FALSE && processed == 0)
				{
					_sndStop(&gsMusic[i]);
					return;
				}
				alGetSourcei(mid, AL_BUFFERS_PROCESSED, &processed);
				while(_alSnd_CheckError("sndUpdateSoundStream::alGetSourcei") && processed--)
				{
					_alSnd_ClearErrors();
					alSourceUnqueueBuffers(mid, 1, &bid);
					if(!_alSnd_CheckError("sndUpdateSoundStream:alSourceUnqueueBuffers"))
					{
						_sndStop(&gsMusic[i]);
						break;
					}
					if(endOfFile == FALSE && _sndProcessWavBlockStreaming(&gsMusic[i], FALSE, bid) != 0)
					{
						alSourceQueueBuffers(mid, 1, &bid);
						if(!_alSnd_CheckError("sndUpdateSoundStream:alSourceQueueBuffers"))
						{
							_sndStop(&gsMusic[i]);
							break;
						}
					}
					else
					{
						if(gsMusic[i].Looped)
						{
							if(_sndProcessWavBlockStreaming(&gsMusic[i], TRUE, bid) != 0)
							{
								alSourceQueueBuffers(mid, 1, &bid);
								if(!_alSnd_CheckError("sndUpdateSoundStream:alSourceQueueBuffers"))
								{
									_sndStop(&gsMusic[i]);
									break;
								}
							}
							else
							{
								_sndStop(&gsMusic[i]);
								break;
							}
						}
					}
				}
				if(gsMusic[i].BgmHandle >= 0)
				{
					ALint state;
					alGetSourcei(mid, AL_SOURCE_STATE, &state);
					if(state == AL_STOPPED)
					{
						alSourcePlay(mid);
					}
				}
			}
		}
	}
#endif
#ifdef USE_SLES_SOUND
	s32 i;
	for(i = 0; i < STREAM_CHANNEL_COUNT && gspSLdevice; i++)
	{
		if(gsMusic[i].BgmHandle >= 0)
		{
			if(gsMusic[i].FHandle < 0)
			{
				_sndProcessWavBlockStreaming(&gsMusic[i], FALSE, 0);
			}
			else
			{
				SLBufferQueueItf playerQueue;
				if(_getPlayer(&gsMusic[i], &playerQueue))
				{
					SLresult result;
					SLBufferQueueState qstate;
					result = (*playerQueue)->GetState(playerQueue, &qstate); 
					if(result == SL_RESULT_SUCCESS)
					{
						s32 processed;
						BOOL endOfFile = gsMusic[i].PlayingPos >= gsMusic[i].pStreamData->mDataSize;
						if(endOfFile && gsMusic[i].Looped == FALSE && qstate.count == 0)
						{
							_sndStop(&gsMusic[i]);
							return;
						}
						processed = STREAM_BUFFERS_COUNT / STREAM_CHANNEL_COUNT - qstate.count;
						while(processed > 0)
						{
							u32 bid;
							processed--;
							bid = _getCuttentIDInQueue(gsMusic[i].BgmHandle);
							_shiftQueue(gsMusic[i].BgmHandle);
							if(endOfFile == FALSE && _sndProcessWavBlockStreaming(&gsMusic[i], FALSE, bid) != 0)
							{
								result = (*playerQueue)->Enqueue(playerQueue, gspWavBgmData[bid]->mData, gspWavBgmData[bid]->mDataSize);
								if(result != SL_RESULT_SUCCESS)
								{
									_sndStop(&gsMusic[i]);
									break;
								}
							}
							else
							{
								if(gsMusic[i].Looped)
								{
									if(_sndProcessWavBlockStreaming(&gsMusic[i], TRUE, bid) != 0)
									{
										result = (*playerQueue)->Enqueue(playerQueue, gspWavBgmData[bid]->mData, gspWavBgmData[bid]->mDataSize);
										if(result != SL_RESULT_SUCCESS)
										{
											_sndStop(&gsMusic[i]);
											break;
										}
									}
									else
									{
										_sndStop(&gsMusic[i]);
										break;
									}
								}
							}
						}
					}
				}
			}
		}
	}
#endif
#ifdef USE_INFINITY_SOUND
	if(gspSLdevice != NULL && gsMixBuffSize > 0)
	{
		s32 i, chan;
		s16* leftout;
		s16* rightout;
		u32 evnt[6];
		const s32 step = 2; //PCM_2_16_44
		
		if(gsPlayBuffer == FALSE)
		{
			gsPlayBuffer = TRUE;
			i = _init_IS(FALSE);
			SDK_ASSERT(i <= gsMixBuffSize);
		}
		
		_getNotify(evnt);
		if(evnt[ISNCCode] != 0xFF000001)
		{
			return;
		}
		if(evnt[ISNCHandle] != gsMixBuffHandle)
		{
			return;
		};

		for(chan = 0; chan < STREAM_CHANNEL_COUNT; chan++)
		{
			if(gsMusic[chan].BgmHandle >= 0)
			{
				if(gsMusic[chan].FHandle < 0)
				{
					_sndProcessWavBlockStreaming(&gsMusic[chan], FALSE, 0);
				}
				else
				{
					BOOL endOfFile = gsMusic[chan].PlayingPos >= gsMusic[chan].pStreamData->mDataSize;
					if(endOfFile == TRUE &&gsMusic[chan].Looped == FALSE)
					{
						_sndStop(&gsMusic[chan]);
						continue;
					}
					_sndProcessWavBlockStreaming(&gsMusic[chan], endOfFile, chan);
				}
				gsMusic[chan].pBegin = (s16*)gspWavBgmData[chan]->mData;
				gsMusic[chan].pEnd = (s16*)(gspWavBgmData[chan]->mData + gspWavBgmData[chan]->mDataSize);
				gsMusic[chan].Channels = gsMusic[chan].pStreamData->mChannels;
				SDK_ASSERT(gsMusic[chan].Channels <= 2);
				SDK_ASSERT(gsMusic[chan].Channels > 0);
			}
		}
		
		leftout = gspMixBuffer;
		rightout = gspMixBuffer + 1;
		
		for(i = 0; i < (s32)(gsMixBuffSize / (sizeof(s16) * step)); i++)
		{
			s32 dl, dr, ch;
			dl = dr = 0;
			for(chan = 0; chan < SOUND_CHANNEL_COUNT; chan++)
			{
				if(gsSound[chan].SndHandle >= 0)
				{
					ch = gsSound[chan].Channels;
					dl += (*gsSound[chan].pBegin * gsSound[chan].Volume) >> FX32_SHIFT;
					ch--;
					dr += (*(gsSound[chan].pBegin + ch) * gsSound[chan].Volume) >> FX32_SHIFT;
					gsSound[chan].pBegin += gsSound[chan].Channels;
					if(gsSound[chan].pBegin >= gsSound[chan].pEnd)
					{
						gsSound[chan].SndHandle = -1;
					}
				}
			}
			for(chan = 0; chan < STREAM_CHANNEL_COUNT; chan++)
			{
				if(gsMusic[chan].pBegin != NULL)
				{
					ch = gsMusic[chan].Channels;
					dl += (*gsMusic[chan].pBegin * gsMusic[chan].Volume) >> FX32_SHIFT;
					ch--;
					dr += (*(gsMusic[chan].pBegin + ch) * gsMusic[chan].Volume) >> FX32_SHIFT;
					gsMusic[chan].pBegin += gsMusic[chan].Channels;
					if(gsMusic[chan].pBegin >= gsMusic[chan].pEnd)
					{
						gsMusic[chan].pBegin = NULL;
					}
				}
			}
			if (dl > 0x7fff)
				*leftout = 0x7fff;
			else if (dl < -0x8000)
				*leftout = -0x8000;
			else
				*leftout = dl;
			if (dr > 0x7fff)
				*rightout = 0x7fff;
			else if (dr < -0x8000)
				*rightout = -0x8000;
			else
				*rightout = dr;
			leftout += step;
			rightout += step;
		}
		SetBuffer(gsMixBuffHandle, gspMixBuffer, (s32)evnt[ISNCOffset], gsMixBuffSize);
	}
#endif
}
//--------------------------------------------------------------------------------------

struct SoundHandle* sndPlay(const struct SoundData *ipSoundData)
{
	struct EventCallbackData ed;
	SDK_NULL_ASSERT(ipSoundData);

#ifdef USE_OPENAL_SOUND
	if(gspALCdevice == NULL)
	{
		return NULL;
	}
#endif
#if defined USE_SLES_SOUND || defined USE_INFINITY_SOUND
    if(gspSLdevice == NULL)
	{
		return NULL;
	}
#ifdef USE_INFINITY_SOUND
	if(gsStreamAudioTask == FALSE && (gsSLWavSfxFileCount > 0 || gsWavStreamFileCount > 0))
	{
		gsPlayBuffer = FALSE;
		jobAddStreamAudioTask();
		gsStreamAudioTask = TRUE;
	}
#endif
#endif

	ed.eventType = EVENT_TYPE_ON_SOUND;
	ed.layer = -1;
	ed.initiatorId = ed.ownerId = ed.eventId = SOUND_TYPE_PLAY;

	switch(ipSoundData->Type)
	{
		case SOUND_TYPE_SFX:
		{
			s32 i;
#ifdef USE_OPENAL_SOUND
			if(gsALSndBufferCount == 0)
			{
				return NULL;
			}
#endif
#if defined USE_SLES_SOUND || defined USE_INFINITY_SOUND
			if(gsSLWavSfxFileCount == 0)
			{
				return NULL;
			}
#endif
			for(i = 0; i < SOUND_CHANNEL_COUNT; i++)
			{
#ifdef NITRO_SDK 
				if(NNS_SndPlayerCountPlayingSeqByPlayerNo(i) == 0)
				{
					gsSound[i].Volume = ipSoundData->Volume;
					gsSound[i].Type = ipSoundData->Type;
					gsSound[i].Looped = ipSoundData->Looped;
					gsSound[i].Id = ipSoundData->Id;
					_InnerStartSeqArc(i);
					ed.eventData.mpSoundHandle = &gsSound[i];
					if(cd._onEventCB != NULL)
					{
						cd._onEventCB(&ed);
					}
					return &gsSound[i];
				}
#endif
#ifdef USE_OPENAL_SOUND
				ALint iState;
				_alSnd_ClearErrors();
				alGetSourcei(gsSndSource[i], AL_SOURCE_STATE, &iState);
				_alSnd_CheckError("alGetSourcei");
				switch(iState)
				{
					case AL_PLAYING:
					case AL_PAUSED:
					break;

					default:
					{
						ALfloat SourcePos[] = {0.0f, 0.0f, 0.0f};
						ALfloat SourceVel[] = {0.0f, 0.0f, 0.0f};

						gsSound[i].Volume = ipSoundData->Volume;
						gsSound[i].Type = ipSoundData->Type;
						gsSound[i].Looped = ipSoundData->Looped;
						gsSound[i].Id = ipSoundData->Id;
						gsSound[i].SndHandle = i;
						gsSound[i].BgmHandle = -1;

						alSourcefv(gsSndSource[i], AL_POSITION, SourcePos);
						alSourcefv(gsSndSource[i], AL_VELOCITY, SourceVel);
						alSourcei(gsSndSource[i], AL_BUFFER, gspSndBuffer[ipSoundData->Id]);
						alSourcei(gsSndSource[i], AL_LOOPING, gsSound[i].Looped);
						alSourcef(gsSndSource[i], AL_PITCH, 1.0f);
						alSourcef(gsSndSource[i], AL_GAIN, (float)gsSound[i].Volume / (float)FX32_ONE);
						if(!_alSnd_CheckError("sndPlay:SOUND_TYPE_SFX"))
						{
							SDK_ASSERT(0);
							return NULL;
						}
						alSourcePlay(gsSndSource[i]);
						if(!_alSnd_CheckError("sndPlay:SOUND_TYPE_SFX"))
						{
							SDK_ASSERT(0);
							return NULL;
						}
						ed.eventData.mpSoundHandle = &gsSound[i];
						if(cd._onEventCB != NULL)
						{
							cd._onEventCB(&ed);
						}
						return &gsSound[i];
					}
				}
#endif
#ifdef USE_SLES_SOUND
				SLresult result;
				SLuint32 playerState;
				BOOL error = TRUE;
				if(gsSndSource[i] != NULL)
				{
					(*gsSndSource[i])->GetState(gsSndSource[i], &playerState);
					if(playerState == SL_OBJECT_STATE_REALIZED)
					{
						SLBufferQueueItf playerQueue;
						result = (*gsSndSource[i])->GetInterface(gsSndSource[i], SL_IID_BUFFERQUEUE, (void*)&playerQueue);
						if(result == SL_RESULT_SUCCESS)
						{
							SLBufferQueueState qstate;
							result = (*playerQueue)->GetState(playerQueue, &qstate); 
							if(result == SL_RESULT_SUCCESS)
							{
								if(qstate.count == 0) 
								{ 
									if(result == SL_RESULT_SUCCESS)
									{
										result = (*playerQueue)->Enqueue(playerQueue, gspWavSfxData[ipSoundData->Id].mData, gspWavSfxData[ipSoundData->Id].mDataSize);
										if(result == SL_RESULT_SUCCESS)
										{
											gsSound[i].Volume = ipSoundData->Volume;
											gsSound[i].Type = ipSoundData->Type;
											gsSound[i].Looped = ipSoundData->Looped;
											gsSound[i].Id = ipSoundData->Id;
											gsSound[i].SndHandle = i;
											gsSound[i].BgmHandle = -1;
											ed.eventData.mpSoundHandle = &gsSound[i];
											if(cd._onEventCB != NULL)
											{
												cd._onEventCB(&ed);
											}
											return &gsSound[i];
										}
									}
								}
								else
								{
									error = FALSE;
								}
							}
						}
					}
					if(error)
					{
						OS_Warning("Error: trying to play sfx sound");
					}
				}
#endif
#ifdef USE_INFINITY_SOUND
				if(gsSound[i].SndHandle < 0)
				{
#ifdef JOBS_IN_SEPARATE_THREAD
					jobCriticalSectionBegin();
#endif
					gsSound[i].Volume = ipSoundData->Volume;
					gsSound[i].Type = ipSoundData->Type;
					gsSound[i].Looped = ipSoundData->Looped;
					gsSound[i].Id = ipSoundData->Id;
					gsSound[i].pBegin = (s16*)gspWavSfxData[ipSoundData->Id].mData;
					gsSound[i].pEnd = (s16*)(gspWavSfxData[ipSoundData->Id].mData + gspWavSfxData[ipSoundData->Id].mDataSize);
					gsSound[i].Channels = gspWavSfxData[ipSoundData->Id].mChannels;
					SDK_ASSERT(gsSound[i].Channels <= 2);
					SDK_ASSERT(gsSound[i].Channels > 0);
					gsSound[i].SndHandle = i;
					gsSound[i].BgmHandle = -1;
#ifdef JOBS_IN_SEPARATE_THREAD
					jobCriticalSectionEnd();
#endif
					return &gsSound[i];
				}
#endif
			}
		}
		break;

		case SOUND_TYPE_BGM1:
		{
#if defined USE_SLES_SOUND || defined USE_OPENAL_SOUND || defined USE_INFINITY_SOUND
#ifdef USE_OPENAL_SOUND
			ALfloat SourcePos[] = {0.0f, 0.0f, 0.0f};
			ALfloat SourceVel[] = {0.0f, 0.0f, 0.0f};
#endif
			if(gsWavStreamFileCount == 0)
			{
				return NULL;
			}
#endif
			sndStop(&gsMusic[0]);
#ifdef JOBS_IN_SEPARATE_THREAD
			jobCriticalSectionBegin();
#endif
			gsMusic[0].Volume = ipSoundData->Volume;
			gsMusic[0].Type = ipSoundData->Type;
			gsMusic[0].Looped = ipSoundData->Looped;
			gsMusic[0].Id = ipSoundData->Id;
#ifdef NITRO_SDK 
			gsMusic[0].Active = 
			 NNS_SndArcStrmStartEx2(gsMusic[0].pBgmHandle, 0, -1, gsMusic[0].Id, gsMusic[0].PlayingPos, NULL, NULL, ipSoundData->Looped ? _StrmCallbackLoop : NULL, NULL);
			if((gsMusic[0].Volume >= 0) && (gsMusic[0].Active))
			{
				u8 v = (u8)((gsMusic[0].Volume * 127) >> FX32_SHIFT);
				NNS_SndArcStrmSetVolume(gsMusic[0].pBgmHandle, v);
			}
#endif
#if defined USE_SLES_SOUND || defined USE_OPENAL_SOUND || defined USE_INFINITY_SOUND
#ifdef USE_OPENAL_SOUND
			alSourcefv(gsBgmSource[0], AL_POSITION, SourcePos);
			alSourcefv(gsBgmSource[0], AL_VELOCITY, SourceVel);
			alSourcei(gsBgmSource[0], AL_LOOPING, AL_FALSE);
			alSourcef(gsBgmSource[0], AL_PITCH, 1.0f);
			alSourcef(gsBgmSource[0], AL_GAIN, (float)gsMusic[0].Volume / (float)FX32_ONE);
#endif
			gsMusic[0].pStreamData = _sndGetStreamData(gsMusic[0].Id);
			gsMusic[0].StrmLength = gsMusic[0].pStreamData->mDataSize;
			SDK_NULL_ASSERT(gsMusic[0].pStreamData);
#endif
#if defined USE_SLES_SOUND || defined USE_OPENAL_SOUND || defined USE_INFINITY_SOUND
			gsMusic[0].SndHandle = -1;
			gsMusic[0].BgmHandle = 0;
#endif
			gsMusic[0].PlayingPos = 0;
#ifndef USE_INFINITY_SOUND
			jobAddStreamAudioTask();
#endif
#ifdef JOBS_IN_SEPARATE_THREAD
			jobCriticalSectionEnd();
#endif
			ed.eventData.mpSoundHandle = &gsMusic[0];
			if(cd._onEventCB != NULL)
			{
				cd._onEventCB(&ed);
			}
			return &gsMusic[0];
		}

		case SOUND_TYPE_BGM2:
		{
#if defined USE_SLES_SOUND || defined USE_OPENAL_SOUND || defined USE_INFINITY_SOUND
#ifdef USE_OPENAL_SOUND
			ALfloat SourcePos[] = {0.0f, 0.0f, 0.0f};
			ALfloat SourceVel[] = {0.0f, 0.0f, 0.0f};
#endif
			if(gsWavStreamFileCount == 0)
			{
				return NULL;
			}
#endif
			sndStop(&gsMusic[1]);
#ifdef JOBS_IN_SEPARATE_THREAD
			jobCriticalSectionBegin();
#endif
			gsMusic[1].Volume = ipSoundData->Volume;
			gsMusic[1].Type = ipSoundData->Type;
			gsMusic[1].Looped = ipSoundData->Looped;
			gsMusic[1].Id = ipSoundData->Id;						
#ifdef NITRO_SDK 
			gsMusic[1].Active = 
			 NNS_SndArcStrmStartEx2(gsMusic[1].pBgmHandle, 1, -1, gsMusic[1].Id, gsMusic[1].PlayingPos, NULL, NULL, ipSoundData->Looped ? _StrmCallbackLoop : NULL, NULL);
			if((gsMusic[1].Volume >= 0) && (gsMusic[1].Active))
			{
				u8 v = (u8)((gsMusic[1].Volume * 127) >> FX32_SHIFT);
				NNS_SndArcStrmSetVolume(gsMusic[1].pBgmHandle, v);          	
			}
#endif
#ifdef USE_OPENAL_SOUND
			alSourcefv(gsBgmSource[1], AL_POSITION, SourcePos);
			alSourcefv(gsBgmSource[1], AL_VELOCITY, SourceVel);
			alSourcei(gsBgmSource[1], AL_LOOPING, AL_FALSE);
			alSourcef(gsBgmSource[1], AL_PITCH, 1.0f);
			alSourcef(gsBgmSource[1], AL_GAIN, (float)gsMusic[1].Volume / (float)FX32_ONE);
#endif
#if defined USE_SLES_SOUND || defined USE_OPENAL_SOUND || defined USE_INFINITY_SOUND
			gsMusic[1].pStreamData = _sndGetStreamData(gsMusic[1].Id);
			gsMusic[1].StrmLength = gsMusic[1].pStreamData->mDataSize;
			SDK_NULL_ASSERT(gsMusic[1].pStreamData);
#endif
#if defined USE_SLES_SOUND || defined USE_OPENAL_SOUND || defined USE_INFINITY_SOUND
			gsMusic[1].SndHandle = -1;
			gsMusic[1].BgmHandle = 0;
#endif
			gsMusic[1].PlayingPos = 0;
#ifndef USE_INFINITY_SOUND
			jobAddStreamAudioTask();
#endif
#ifdef JOBS_IN_SEPARATE_THREAD
			jobCriticalSectionEnd();
#endif
			ed.eventData.mpSoundHandle = &gsMusic[1];
			if(cd._onEventCB != NULL)
			{
				cd._onEventCB(&ed);
			}
			return &gsMusic[1];
		}

		case SOUND_TYPE_NONE:
		break;
	}
	return NULL;
}
//--------------------------------------------------------------------------------------

#ifdef NITRO_SDK 
void _InnerStartSeqArc(s32 i)
{
	gsSound[i].PlayingPos = 0;
	gsSound[i].Active = NNS_SndArcPlayerStartSeqArcEx(gsSound[i].pSndHandle, i, -1, -1, SEQUENCE_ARC_NO, gsSound[i].Id);
	if((gsSound[i].Volume >= 0) && (gsSound[i].Active))
	{
		u8 v = (u8)((gsSound[i].Volume * 127) >> FX32_SHIFT);
		NNS_SndPlayerSetVolume(gsSound[i].pSndHandle, v);
	}
}
#endif
//--------------------------------------------------------------------------------------

void sndStopAll()
{
	s32 i;
	for(i = 0; i < SOUND_CHANNEL_COUNT; i++)
	{
		sndStop(&gsSound[i]);
	}
	for(i = 0; i < STREAM_CHANNEL_COUNT; i++)
	{
		sndStop(&gsMusic[i]);
	}
}
//--------------------------------------------------------------------------------------

void sndPauseAll(BOOL iPause)
{
	s32 i;
	for(i = 0; i < SOUND_CHANNEL_COUNT; i++)
	{
		sndPause(&gsSound[i], iPause);
	}
	for(i = 0; i < STREAM_CHANNEL_COUNT; i++)
	{
		sndPause(&gsMusic[i], iPause);
	}
}
//--------------------------------------------------------------------------------------

#ifdef NITRO_SDK 
BOOL _StrmCallbackLoop(
	NNSSndArcStrmCallbackStatus status,
	const NNSSndArcStrmCallbackInfo* info,
	NNSSndArcStrmCallbackParam* param,
	void* arg)
{
#pragma unused (info)    
#pragma unused (arg)
	
	if(status == NNS_SND_ARC_STRM_CALLBACK_DATA_END)
	{
		param->offset = 0;
		return TRUE;
	}
	return FALSE;
}
#endif
//--------------------------------------------------------------------------------------

u8* sndLoadWavData(const char* pFname, s32 resId, BOOL isSfx)
{
#if defined USE_NO_SOUND || defined NITRO_SDK 
	(void)pFname;
	(void)resId;
	(void)isSfx;
#endif
#if defined USE_OPENAL_SOUND || defined USE_SLES_SOUND || defined USE_INFINITY_SOUND
	u8* pFile;
	u8* p;
	u32 szFile;
#ifdef USE_OPENAL_SOUND
	ALenum format;
#endif
#ifdef USE_INFINITY_SOUND
	s32 format;
#endif
	const struct WavFileFmtStruct* wh;
	void* waveformData;
	u32 waveformDataSize;
	const u32 WAV_HEADER_SIZE = 1024;
#ifdef USE_OPENAL_SOUND
	if (gspALCdevice == NULL)
#endif
#if defined USE_SLES_SOUND || defined USE_INFINITY_SOUND
		if (gspSLdevice == NULL)
#endif
		{
			return NULL;
		}
	if (isSfx)
	{
#ifdef USE_OPENAL_SOUND
		SDK_ASSERT(gsALSndBufferCount != 0);
#endif
#if defined USE_SLES_SOUND || defined USE_INFINITY_SOUND
		SDK_ASSERT(gsSLWavSfxFileCount != 0);
#endif
		p = pFile = LoadFile(pFname, &szFile);
	}
	else
	{
#if defined USE_OPENAL_SOUND || defined USE_SLES_SOUND || defined USE_INFINITY_SOUND
		SDK_ASSERT(gsWavStreamFileCount != 0);
#endif
		p = pFile = (u8*)MALLOC(WAV_HEADER_SIZE, "sndLoadWavData:pFile");
		LoadFileToSpecificMemory(pFname, p, WAV_HEADER_SIZE);
		szFile = WAV_HEADER_SIZE;
	}

	if (!(p[0] == 'R' && p[1] == 'I' && p[2] == 'F' && p[3] == 'F'))
	{
		SDK_ASSERT(0); // wrong format!
		FREE(pFile);
		return NULL;
	}
	p += 4;
	wh = (const struct WavFileFmtStruct*)p;
	while (!(wh->chunkID[0] == 'f' && wh->chunkID[1] == 'm' && wh->chunkID[2] == 't' && wh->chunkID[3] == ' '))
	{
		p += 4;
		wh = (const struct WavFileFmtStruct*)p;
	}
	p += sizeof(struct WavFileFmtStruct);
	while (!(p[0] == 'd' && p[1] == 'a' && p[2] == 't' && p[3] == 'a'))
	{
		p++;
		if (isSfx == 0 && p - pFile > (s32)szFile)
		{
			SDK_ASSERT(0); // "data" section not found, please increase WAV_HEADER_SIZE size
			FREE(pFile);
			return NULL;
		}
	}
	p += 4;
	waveformDataSize = p[3] << 24;
	waveformDataSize |= p[2] << 16;
	waveformDataSize |= p[1] << 8;
	waveformDataSize |= p[0];
	p += 4;
	waveformData = p;

	switch(wh->bitsPerSample)
	{
#ifdef USE_OPENAL_SOUND
		case 8:
			format = AL_FORMAT_STEREO8;
		break;
#endif
		case 16:
#ifdef USE_OPENAL_SOUND
			format = AL_FORMAT_STEREO16;
#endif
#ifdef USE_INFINITY_SOUND
			format = PCM_2_16_44;
#endif
		break;
		default:
#ifdef USE_OPENAL_SOUND
			OS_Warning("Error data format: %s\n", "only 8 or 16 bitsPerSample is allowed");
#endif
#if defined USE_SLES_SOUND || defined USE_INFINITY_SOUND
			OS_Warning("Error data format: %s\n", "only 16 bitsPerSample is allowed");
#endif
			SDK_ASSERT(0); // 8 or 16 bit sound only!
			FREE(pFile);
			return NULL;
	}
	switch(wh->channels)
	{
		case 1:
#ifdef USE_OPENAL_SOUND
			if(format == AL_FORMAT_STEREO8)
			{
				format = AL_FORMAT_MONO8;
			}
			if(format == AL_FORMAT_STEREO16)
			{
				format = AL_FORMAT_MONO16;
			}
#endif
#ifdef USE_INFINITY_SOUND
			format = PCM_1_16_44;
#endif
#if defined USE_OPENAL_SOUND || defined USE_INFINITY_SOUND
		case 2:
#endif
		break;
		default:
			OS_Warning("Error data format: %s\n", "only 1 channel (mono) is allowed");
			SDK_ASSERT(0); // only 1 channel (mono) is allowed
			FREE(pFile);
			return NULL;
	}
#if defined USE_SLES_SOUND || defined USE_INFINITY_SOUND
	if(wh->samplesPerSec != 44100)
	{
		OS_Warning("Error data format: %s\n", "only 44100 samplesPerSec is allowed");
		FREE(pFile);
		return NULL;
	}
#endif
	if(isSfx)
	{
#ifdef USE_OPENAL_SOUND
		_alSnd_ClearErrors();
		alBufferData(gspSndBuffer[resId], format, waveformData, waveformDataSize, wh->samplesPerSec);
		if(!_alSnd_CheckError("sndLoadData:alBufferData"))
		{
			SDK_ASSERT(0); //something wrong
		}
#ifdef SDK_DEBUG
		{
			ALint val;
			alGetBufferi(gspSndBuffer[resId], AL_FREQUENCY, &val);
			SDK_ASSERT(val == (ALint)wh->samplesPerSec);
			alGetBufferi(gspSndBuffer[resId], AL_SIZE, &val);
			SDK_ASSERT(val == (ALint)waveformDataSize);
			alGetBufferi(gspSndBuffer[resId], AL_BITS, &val);
			SDK_ASSERT(val == wh->bitsPerSample);
			alGetBufferi(gspSndBuffer[resId], AL_CHANNELS, &val);
			SDK_ASSERT(val == wh->channels);
		}
#endif
#endif
		OS_Printf("sound system: sfx file %s ok\n", pFname);
#if defined USE_SLES_SOUND || defined USE_INFINITY_SOUND
		gspWavSfxData[resId].mDataSize = waveformDataSize;
		gspWavSfxData[resId].mData = (u8*)waveformData;
#ifdef USE_INFINITY_SOUND
		gspWavSfxData[resId].mChannels = wh->channels;
#endif
		return pFile;
#endif
	}
	else
	{
		u32 i;
		for(i = 0; i < gsWavStreamFileCount; i++)
		{
			if(gspWavStreamFile[i].mResId < 0)
			{
				s32 sz;
				gspWavStreamFile[i].mResId = resId;
				gspWavStreamFile[i].mDataOffset = (u32)((u8*)waveformData - pFile);
				gspWavStreamFile[i].mDataSize = waveformDataSize; 
#ifdef USE_OPENAL_SOUND
				gspWavStreamFile[i].mFormat = format;
				gspWavStreamFile[i].mSamplesPerSec = wh->samplesPerSec;
#endif
#ifdef USE_INFINITY_SOUND
				(void)format;
				gspWavStreamFile[i].mChannels = wh->channels;
#endif
				sz = STD_StrLen(pFname);
				if(sz > MAX_FILENAME)
				{
					gspWavStreamFile[i].mResId = -1;
					SDK_ASSERT(0);
					break;
				}
				MI_CpuCopy8(&pFname[0], &gspWavStreamFile[i].mName[0], sz);
				gspWavStreamFile[i].mName[sz] = 0;
				OS_Printf("sound system: bgm file %s ok\n", pFname);
				break;
			}
		}
	}
	FREE(pFile);
	return NULL;
#else
	return NULL;
#endif
}
//--------------------------------------------------------------------------------------

void sndInitResource(const u8* data, struct TERSound *res)
{
#if defined USE_NO_SOUND || defined NITRO_SDK 
	(void)res;
	(void)data;
#endif
#ifdef USE_OPENAL_SOUND
	(void)data;
#endif
	res->mInit = TRUE;
#if defined USE_SLES_SOUND || defined USE_INFINITY_SOUND
	SDK_ASSERT(res->mpFileData == NULL);
	if(data)
	{
		SDK_NULL_ASSERT(res);
		res->mpFileData = data;
	}
#endif
}
//--------------------------------------------------------------------------------------

void sndReleaseResource(struct TERSound *res)
{
	res->mInit = FALSE;
#if defined USE_SLES_SOUND || defined USE_INFINITY_SOUND
	if(res->mpFileData)
	{
		FREE((void*)res->mpFileData);
		res->mpFileData = NULL;
	}
#endif
}
//--------------------------------------------------------------------------------------

BOOL sndIsInitedResource(struct TERSound *res)
{
#ifdef USE_NO_SOUND
	(void)res;
	return FALSE;
#else
	return res->mInit;
#endif
}
//--------------------------------------------------------------------------------------

#ifdef USE_NO_SOUND
void sndInitSoundSystem(void)
{
}
#endif
//--------------------------------------------------------------------------------------

#ifdef USE_INFINITY_SOUND
void sndInitSoundSystem(void)
{   
	s32 i;
	s32 ver = -1;

	SDK_ASSERT(gspSLdevice == NULL);
	SDK_ASSERT(gspMixBuffer == NULL);
	SDK_ASSERT(gsCreateBuffer == FALSE);

	if(InitSound(&ver))
	{
		gsSoundInit = FALSE;
		OS_Warning("sndInitSoundSystem error: can't init Infinity engine\n");
		return;
	}
	OS_Printf("Infinity sound version: %d, %d\n", (ver >> 16), (ver & 0xFFFF));
	if(SOUND_VERSION > (ver & 0xFFFF) || SOUND_VERSION < (ver >> 16))
	{
		gsSoundInit = FALSE;
		OS_Warning("sndInitSoundSystem error: sound version mismatch\n");
		return;
	}
	gsMixBuffSize = _init_IS(TRUE);
	if(gsMixBuffSize == 0)
	{
		_term_IS();
		gsSoundInit = FALSE;
		return;
	}
	_term_IS();
	gsSoundInit = TRUE;
	gspSLdevice = &gsSoundInit;

	for(i = 0; i < SOUND_CHANNEL_COUNT; i++)
	{
		gsSound[i].SndHandle = -1;
		gsSound[i].BgmHandle = -1;
		gsSound[i].Looped = FALSE;
		gsSound[i].Volume = -FX32_ONE;
		gsSound[i].Type = SOUND_TYPE_NONE;
		gsSound[i].pStreamData = NULL;
		gsSound[i].FHandle = -1;
		gsSound[i].pBegin = NULL;
		gsSound[i].pEnd = NULL;
	}
	for(i = 0; i < STREAM_CHANNEL_COUNT; i++)
	{
		gsMusic[i].SndHandle = -1;
		gsMusic[i].BgmHandle = -1;
		gsMusic[i].Looped = FALSE;
		gsMusic[i].Volume = -FX32_ONE;               
		gsMusic[i].Type = SOUND_TYPE_NONE;
		gsMusic[i].pStreamData = NULL;
		gsMusic[i].FHandle = -1;
    }
}
//--------------------------------------------------------------------------------------

s32 _init_IS(BOOL check)
{
	s32 err, size;
	SDK_ASSERT(gsCreateBuffer == FALSE);
	err = CreateBuffer(PCM_2_16_44 | PCM_RING, 0, &gsMixBuffHandle);
	if(err)
	{
		if(check == TRUE)
		{
			OS_Warning("sndInitSoundSystem error: create buffer error (%d)\n", err);
		}
		return 0;
	}	
	gsCreateBuffer = TRUE;
	err = GetBufferSize(gsMixBuffHandle, &size);
	if(err)
	{
		if(check == TRUE)
		{
			OS_Warning("sndInitSoundSystem error: get buffer size (%d)\n", err);
			_term_IS();
		}
		return 0;
	};
	size = size / 2;
	if(check == TRUE)
	{
		OS_Printf("Infinity sound mixer size: %d\n", size);
	}
	err = PlayBuffer(gsMixBuffHandle, 0);
	if(err)
	{
		if(check == TRUE)
		{
			OS_Warning("sndInitSoundSystem error: play buffer error (%d)\n", err);
			_term_IS();
		}
		return 0;
	}
	return size;
}
//--------------------------------------------------------------------------------------

void _term_IS(void)
{
	if(gsCreateBuffer == TRUE)
	{
		DestroyBuffer(gsMixBuffHandle);
	}
	gsCreateBuffer = FALSE;
}
//--------------------------------------------------------------------------------------

void _getNotify(void* event)
{
	__asm__("int $0x40"::"a"(68), "b"(14), "c"(event));
}
#endif
//--------------------------------------------------------------------------------------
