/*
tengine - cross-platform game engine
------------------------------------

Copyright (c) 2013 Papa Pascualle
Contact Email: pascualle@gmail.com

Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
*/

#include "system.h"
#include "filesystem.h"
#include "filesystem_low.h"

#ifdef NITRO_SDK

#include "ds_filesystem.h"
#include "texts.h"
#include "memory.h"

//-------------------------------------------------------------------------------------------

static void *fs_p_table;
static char gSysFSPath[MAX_FILEPATH] = {0};
static FSFile gsFileHandler[MAX_OPENFILES];
static s32 gSysFSPathLenght = 0;
static BOOL gsFileHandlerState[MAX_OPENFILES] = {0};

extern BOOL gsFilesystemError;

//-------------------------------------------------------------------------------------------

void nitroFS_InitFileSystem(struct InitFileSystemData* data)
{
	u32 need_size;
	SDK_ASSERT(gSysFSPathLenght == 0);
	SDK_NULL_ASSERT(data->mpPath);
	STD_StrCpy(gSysFSPath, data->mpPath);
	gSysFSPathLenght = STD_StrLen(gSysFSPath); 
	// Enables FIFO interrupt in the communications with ARM7
	(void)OS_EnableIrqMask(OS_IE_SPFIFO_RECV);
	FS_Init(FS_DMA_NOT_USE);
	need_size = FS_GetTableSize();
	fs_p_table = MALLOC(need_size, "InitFileSystem:p_table");
	SDK_ASSERT(fs_p_table != NULL);
	FS_LoadTable(fs_p_table, need_size);
	gsFilesystemError = FALSE;
}
//-------------------------------------------------------------------------------------------

void nitroFS_ReleaseFileSystem()
{
	nitroFS_lostDevice();
	FREE(fs_p_table);
	gSysFSPathLenght = 0;
	gSysFSPath[0] = 0;
}
//-------------------------------------------------------------------------------------------

void nitroFS_lostDevice()
{
	s16 i = MAX_OPENFILES;
	while(i > 0)
	{
		i--;
		if(gsFileHandlerState[i])
		{
			BOOL bSuccess = FS_CloseFile(&gsFileHandler[i]);
			SDK_ASSERT(bSuccess);
			gsFileHandlerState[i] = FALSE;
		}
	}
}
//-------------------------------------------------------------------------------------------

BOOL nitroFS_IsFileSystemInit()
{
	return gSysFSPathLenght != 0;	
}
//-------------------------------------------------------------------------------------------

s16 nitroFS_fopen(const char* filename)
{
	s16 i;
	BOOL bSuccess;
	for(i = 0; i < MAX_OPENFILES; i++)
	{
		if(gsFileHandlerState[i] == FALSE)
		{
			char fullPath[MAX_FILEPATH];
			STD_StrCpy(fullPath, gSysFSPath);
			STD_StrCpy(fullPath + gSysFSPathLenght, filename);
			FS_InitFile(&gsFileHandler[i]);
			bSuccess = FS_OpenFile(&gsFileHandler[i], fullPath);
			if(bSuccess)
			{
				gsFileHandlerState[i] = TRUE;
				return i;
			}
			else
			{
				return -1;
			}
		}
	}
	return -1;
}
//-------------------------------------------------------------------------------------------

s32 nitroFS_fsize(u8 id)
{
	SDK_ASSERT(id < MAX_OPENFILES);
	SDK_ASSERT(gsFileHandlerState[id] == TRUE);
	return (s32)FS_GetLength(&gsFileHandler[id]);
}
//-------------------------------------------------------------------------------------------

BOOL nitroFS_fseek(u32 off, u8 id)
{
	SDK_ASSERT(id < MAX_OPENFILES);
	SDK_ASSERT(gsFileHandlerState[id] == TRUE);
	FS_SeekFile(&gsFileHandler[id], (s32)off, FS_SEEK_SET);
	return TRUE;
}
//---------------------------------------------------------------------------

s32 nitroFS_fread(unsigned char* buf, u32 size, u8 id)
{
	SDK_ASSERT(id < MAX_OPENFILES);
	SDK_ASSERT(gsFileHandlerState[id] == TRUE);
	return FS_ReadFile(&gsFileHandler[id], buf, (s32)size);
}
//-------------------------------------------------------------------------------------------

void nitroFS_fclose(s16 id)
{
	SDK_ASSERT(id < MAX_OPENFILES && id >= 0);
	if(gsFileHandlerState[id] == TRUE)
	{
		BOOL bSuccess = FS_CloseFile(&gsFileHandler[id]);
		SDK_ASSERT(bSuccess);
		gsFileHandlerState[id] = FALSE;
	}
}
//-------------------------------------------------------------------------------------------

#endif
