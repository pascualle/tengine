/*
tengine - cross-platform game engine
------------------------------------

Copyright (c) 2013 Papa Pascualle
Contact Email: pascualle@gmail.com

Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
*/

#ifndef PLATFORM_LOW_H_INCLUDED
#define PLATFORM_LOW_H_INCLUDED

#include "platform.h"
#include "tengine_low.h"

#ifdef __cplusplus
extern "C" {
#endif

#ifdef EMSCRIPTEN_APP
struct GLFWwindow;
#endif

struct TEnginePlatformData
{
	u32 tickPreviousTime;
	fx32 timeAccumulatorMSFx;
	BOOL renderDevice;
#ifndef WINDOWS_APP
	BOOL tengineInit;
#endif
#if defined WINDOWS_APP
	HDC hDC;
#elif defined EMSCRIPTEN_APP
	struct GLFWwindow* glWnd;
#endif
};

struct TEnginePlatformData* Platform_GetPlatformData(void);
void Platform_InitTick(void);
void Platform_Tick(void);
u32 Platform_GetTime(void);
void Platform_lostRenderDevice(void);
void Platform_restoreRenderDevice(void);

#ifdef __cplusplus
}
#endif

#endif /*PLATFORM_LOW_H_INCLUDED*/

