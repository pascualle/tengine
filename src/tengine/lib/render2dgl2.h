#ifndef RENDER2dGL2_H_INCLUDED
#define RENDER2dGL2_H_INCLUDED

#include "system.h"

#ifdef __cplusplus
extern "C" {
#endif

#ifndef NITRO_SDK
#ifdef USE_OPENGL_2_RENDER

#ifdef USE_EXTERNAL_API
struct ExDRect;
#endif

#define RENDERFN(name) glRender_##name
	
void glRender_CreateTextures(s32 texturesCount);

void glRender_LoadTextureToVRAM(struct BMPImage *pImage, s32 resId);
void glRender_CreateEmptyTexture(const struct BMPImage *iImgHeader, s32 resId);

// data from mapeditor
void glRender_SetupBGLayersData(enum BGSelect iBgType, u32 iMaxLayers, u32 iMaxTextures, u32 iMaxElements);
void glRender_ReleaseBGLayersData(enum BGSelect iBgType);

BOOL glRender_IsGraphicsInit(void);

void glRender_PlaneInit(const struct RenderPlaneInitParams* ipParams);
BOOL glRender_IsRenderPlaneInit(enum BGSelect iType);
void glRender_PlaneResize(enum BGSelect iType, const struct RenderPlaneSizeParams* ipParams);
void glRender_PlaneRelease(enum BGSelect iType);

void glRender_SetActiveBGForGraphics(enum BGSelect iActiveBG);
enum BGSelect glRender_GetActiveBGForGraphics(void);

s32 glRender_GetViewWidth(enum BGSelect iBGType);
s32 glRender_GetViewHeight(enum BGSelect iBGType);
s32 glRender_GetViewLeft(enum BGSelect iBGType);
s32 glRender_GetViewTop(enum BGSelect iBGType);

s32 glRender_GetFrameBufferWidth(enum BGSelect iBGType);
s32 glRender_GetFrameBufferHeight(enum BGSelect iBGType);

void glRender_SetColor(GXRgba iColor);

void glRender_DrawLine(fx32 iX0, fx32 iY0, fx32 iX1, fx32 iY1);

void glRender_ColorRect(fx32 iX, fx32 iY, fx32 iWidth, fx32 iHeight);

void glRender_DrawImage(const struct DrawImageFunctionData* data);

void glRender_Resize(s32 w, s32 h);

void glRender_SetRenderPlaneScale(fx32 val, enum BGSelect iBG);
fx32 glRender_GetRenderPlaneScale(enum BGSelect iBG);

void glRender_DeleteTextures(void);

void glRender_DrawFrame(void);

void glRender_ClearFrameBuffer(enum BGSelect iBG);

void glRender_Init(void);
void glRender_Release(void);

void glRender_FreezeCurrentFrameBuffer(BOOL val);
void glRender_LostDevice(void);
void glRender_RestoreDevice(void);

#ifdef USE_EXTERNAL_API
void glRender_ExternalSetActiveClipRect(const struct ExDRect* data);
void glRender_ExternalPosColorData(const struct TRGLColorVertexData* data);
#endif

#endif //USE_OPENGL_2_RENDER
#endif //NITRO_SDK

#ifdef __cplusplus
}
#endif

#endif //RENDER2dGL2_H_INCLUDED
