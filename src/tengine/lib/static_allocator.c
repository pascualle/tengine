/*
tengine - cross-platform game engine
------------------------------------

Copyright (c) 2013 Papa Pascualle
Contact Email: pascualle@gmail.com

Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
*/

#include "static_allocator.h"
#include "memory.h"

#define STATIC_ALLOCATOR_MIN_DATA_SIZE 8

#ifdef STATIC_ALLOCATOR_HEAP_MEM_DEBUG_DEEP
#define STATIC_ALLOCATOR_HEAP_MEM_DEBUG
#endif

#ifndef STATIC_ALLOCATOR_HEAP_GUARD_SIZE
#ifdef STATIC_ALLOCATOR_HEAP_MEM_DEBUG
#define STATIC_ALLOCATOR_HEAP_GUARD_SIZE 4
#else
#define STATIC_ALLOCATOR_HEAP_GUARD_SIZE 0
#endif
#endif

#define STATIC_ALLOCATOR_HEAP_USAGE_FLAG 0xF0000000
#define STATIC_ALLOCATOR_HEAP_SIZE_MASK 0x0FFFFFFF

#ifndef STATIC_ALLOCATOR_HEAP_GUARD_MARK
#define STATIC_ALLOCATOR_HEAP_GUARD_MARK 0xc0
#endif

#define STATIC_ALLOCATOR_HEAP_INTERNAL_DATA_SIZEDATA_SIZE (sizeof(u32))
#define STATIC_ALLOCATOR_HEAP_INTERNAL_DATA_BEGIN_SIZE (STATIC_ALLOCATOR_HEAP_GUARD_SIZE + STATIC_ALLOCATOR_HEAP_INTERNAL_DATA_SIZEDATA_SIZE)
#define STATIC_ALLOCATOR_HEAP_INTERNAL_DATA_END_SIZE (STATIC_ALLOCATOR_HEAP_INTERNAL_DATA_SIZEDATA_SIZE + STATIC_ALLOCATOR_HEAP_GUARD_SIZE)
#define STATIC_ALLOCATOR_HEAP_INTERNAL_DATA_SIZE (STATIC_ALLOCATOR_HEAP_INTERNAL_DATA_BEGIN_SIZE + STATIC_ALLOCATOR_HEAP_INTERNAL_DATA_END_SIZE)
#define STATIC_ALLOCATOR_HEAP_MIN_SIZE (STATIC_ALLOCATOR_MIN_DATA_SIZE + STATIC_ALLOCATOR_HEAP_INTERNAL_DATA_SIZE)

//----------------------------------------------------------------------------------------------

static BOOL StaticAllocator_WriteGuarder_(struct StaticAllocator* heap, u32* pos)
{
#if STATIC_ALLOCATOR_HEAP_GUARD_SIZE == 0
	(void)heap;
	(void)pos;
#else
	u32 i = 0;
	for(; i < STATIC_ALLOCATOR_HEAP_GUARD_SIZE; i++)
	{
		heap->internal_buf[(*pos) + i] = STATIC_ALLOCATOR_HEAP_GUARD_MARK;
	}
	(*pos) += STATIC_ALLOCATOR_HEAP_GUARD_SIZE;
#endif
	return TRUE;
}
//----------------------------------------------------------------------------------------------

static BOOL StaticAllocator_ReadGuarder_(struct StaticAllocator* heap, u32* pos)
{
#if STATIC_ALLOCATOR_HEAP_GUARD_SIZE == 0
	(void)heap;
	(void)pos;
#else
	u32 i = 0;
	for(; i < STATIC_ALLOCATOR_HEAP_GUARD_SIZE; i++)
	{
		if(heap->internal_buf[(*pos) + i] != STATIC_ALLOCATOR_HEAP_GUARD_MARK)
		{
			SDK_ASSERT(0);
			return FALSE;
		}
	}
	(*pos) += STATIC_ALLOCATOR_HEAP_GUARD_SIZE;
#endif
	return TRUE;
}
//----------------------------------------------------------------------------------------------

u32 StaticAllocator_CalculateHeapSize(u32 fsize)
{
	fsize = ((fsize + STATIC_ALLOCATOR_MIN_DATA_SIZE - 1) / STATIC_ALLOCATOR_MIN_DATA_SIZE) * STATIC_ALLOCATOR_MIN_DATA_SIZE;
	fsize += STATIC_ALLOCATOR_HEAP_INTERNAL_DATA_SIZE;
	return STATIC_ALLOCATOR_HEAP_MIN_SIZE > fsize ? STATIC_ALLOCATOR_HEAP_MIN_SIZE : fsize;
}
//----------------------------------------------------------------------------------------------

static BOOL StaticAllocator_ReadSizeBlock_(struct StaticAllocator* heap, u32* pos, u32* size)
{
	u32* val = ((u32*)(heap->internal_buf + (*pos)));
	(*pos) += STATIC_ALLOCATOR_HEAP_INTERNAL_DATA_SIZEDATA_SIZE;
	(*size) = (val[0] & STATIC_ALLOCATOR_HEAP_SIZE_MASK);
#ifdef STATIC_ALLOCATOR_HEAP_MEM_DEBUG_DEEP
	if((*size) > (heap->max_size - (*pos)))
	{
		SDK_ASSERT(0);
	}
#endif
	return ((val[0] & STATIC_ALLOCATOR_HEAP_USAGE_FLAG) != 0) ? TRUE : FALSE;
}
//----------------------------------------------------------------------------------------------

static void StaticAllocator_WriteSizeAndMoveToEndBlock_(struct StaticAllocator* heap, u32* pos, u32 size, BOOL usage)
{
	u8 *ptr;
	u32 *valb, *vale;
#ifdef STATIC_ALLOCATOR_HEAP_MEM_DEBUG_DEEP
	if(size > (heap->max_size - (*pos)))
	{
		SDK_ASSERT(0);
	}
#endif
	ptr = heap->internal_buf + *pos;
	valb = (u32*)ptr;
	ptr += STATIC_ALLOCATOR_HEAP_INTERNAL_DATA_SIZEDATA_SIZE + size;
	vale = (u32*)ptr;
	*pos += STATIC_ALLOCATOR_HEAP_INTERNAL_DATA_SIZEDATA_SIZE + size + STATIC_ALLOCATOR_HEAP_INTERNAL_DATA_SIZEDATA_SIZE;
	valb[0] = size & STATIC_ALLOCATOR_HEAP_SIZE_MASK;
	vale[0] = size & STATIC_ALLOCATOR_HEAP_SIZE_MASK;
	if(usage == TRUE)
	{
		valb[0] |= STATIC_ALLOCATOR_HEAP_USAGE_FLAG;
		vale[0] |= STATIC_ALLOCATOR_HEAP_USAGE_FLAG;
	}
}
//----------------------------------------------------------------------------------------------

void StaticAllocator_Init(struct StaticAllocator* pheap, const void* static_array, u32 array_size)
{
	u32 cpos = STATIC_ALLOCATOR_HEAP_MIN_SIZE;
	MI_CpuClear8(pheap, sizeof(struct StaticAllocator));
	if(cpos > array_size)
	{
		OS_Printf("StaticAllocator_Init: error, please increase heap size\n");
		SDK_ASSERT(0);
		return;
	}
	cpos = 0;
	pheap->max_size = array_size;
	pheap->internal_buf = (u8*)static_array;
	StaticAllocator_WriteGuarder_(pheap, &cpos);
	StaticAllocator_WriteSizeAndMoveToEndBlock_(pheap, &cpos, pheap->max_size - STATIC_ALLOCATOR_HEAP_INTERNAL_DATA_SIZE, FALSE);
	StaticAllocator_WriteGuarder_(pheap, &cpos);
	pheap->inited = TRUE;
}
//----------------------------------------------------------------------------------------------

void StaticAllocator_Release(struct StaticAllocator* allocator)
{
	SDK_NULL_ASSERT(allocator);
	allocator->inited = FALSE;
}
//----------------------------------------------------------------------------------------------

void StaticAllocator_Reset(struct StaticAllocator* allocator)
{
	if(allocator->inited == TRUE)
	{
		StaticAllocator_Init(allocator, allocator->internal_buf, allocator->max_size);
	}
}
//----------------------------------------------------------------------------------------------

#ifdef STATIC_ALLOCATOR_HEAP_MEM_DEBUG_DEEP
static void StaticAllocator_CheckAllGuarders_(struct StaticAllocator* pStaticAllocator)
{
	u32 cpos = 0;
	while(cpos != pStaticAllocator->max_size)
	{
		u32 size = 0;
		StaticAllocator_ReadGuarder_(pStaticAllocator, &cpos);
		StaticAllocator_ReadSize_(pStaticAllocator, &cpos, &size);
		StaticAllocator_ReadGuarder_(pStaticAllocator, &cpos);
		if(size > (pStaticAllocator->max_size - cpos))
		{
			SDK_ASSERT(0);
		}
	}
}
#endif
//----------------------------------------------------------------------------------------------

static void* StaticAllocator_Malloc_Result_(struct StaticAllocator* pStaticAllocator, u32 *pos, u32 csize, u32 size)
{
	void* result;
	s32 tail = (s32)(csize - STATIC_ALLOCATOR_HEAP_INTERNAL_DATA_SIZE - size);
	result = (void*)(pStaticAllocator->internal_buf + *pos);
	*pos -= STATIC_ALLOCATOR_HEAP_INTERNAL_DATA_SIZEDATA_SIZE;
	if(tail > 0)
	{
		StaticAllocator_WriteSizeAndMoveToEndBlock_(pStaticAllocator, pos, size, TRUE);
		StaticAllocator_WriteGuarder_(pStaticAllocator, pos);
		pStaticAllocator->last_pos = *pos;
		StaticAllocator_WriteGuarder_(pStaticAllocator, pos);
		StaticAllocator_WriteSizeAndMoveToEndBlock_(pStaticAllocator, pos, (u32)tail, FALSE);
		StaticAllocator_WriteGuarder_(pStaticAllocator, pos);
	}
	else
	{
		size = csize;
		StaticAllocator_WriteSizeAndMoveToEndBlock_(pStaticAllocator, pos, size, TRUE);
		StaticAllocator_WriteGuarder_(pStaticAllocator, pos);
		pStaticAllocator->last_pos = *pos;
	}
	{
		u32 cpos = pStaticAllocator->last_pos - (STATIC_ALLOCATOR_HEAP_GUARD_SIZE + STATIC_ALLOCATOR_HEAP_INTERNAL_DATA_SIZEDATA_SIZE);
		StaticAllocator_ReadSizeBlock_(pStaticAllocator, &cpos, &csize);
		cpos -= STATIC_ALLOCATOR_HEAP_INTERNAL_DATA_SIZEDATA_SIZE + csize + STATIC_ALLOCATOR_HEAP_INTERNAL_DATA_SIZEDATA_SIZE;
		if(cpos == pStaticAllocator->head_pos)
		{
			pStaticAllocator->head_pos = pStaticAllocator->last_pos;
		}
	}
	pStaticAllocator->alloc_count++;
	pStaticAllocator->alloc_size += size + STATIC_ALLOCATOR_HEAP_INTERNAL_DATA_SIZE;
#ifdef STATIC_ALLOCATOR_HEAP_MEM_DEBUG_DEEP
	StaticAllocator_CheckAllGuarders_(pStaticAllocator);
#endif
	return result;
}
//----------------------------------------------------------------------------------------------

void* StaticAllocator_Malloc(struct StaticAllocator* pStaticAllocator, u32 fsize)
{
	u32 rsize, size;
	if(fsize == 0)
	{
		return NULL;
	}
	rsize = StaticAllocator_CalculateHeapSize(fsize);
	size = rsize - STATIC_ALLOCATOR_HEAP_INTERNAL_DATA_SIZE;
#ifdef STATIC_ALLOCATOR_HEAP_MEM_DEBUG_DEEP
	StaticAllocator_CheckAllGuarders_(pStaticAllocator);
#endif
	if(pStaticAllocator->inited != TRUE)
	{
		OS_Warning("StaticAllocator_Malloc: memory allocation error, please call StaticAllocator_Init() function before using this function\n");
		SDK_ASSERT(0);
		return NULL;
	}
	if(((pStaticAllocator->alloc_size + rsize) > pStaticAllocator->max_size) || (size >= STATIC_ALLOCATOR_HEAP_SIZE_MASK))
	{
		OS_Warning("StaticAllocator_Malloc: memory allocation error, please increase heap size\n");
		SDK_ASSERT(0);
		return NULL;
	}
	{
		// 0|pos[2]->____...____<-pos[1]|last_pos|pos[0]->______...______|max_size
		u32 csize;
		u32 cpos[3];
		BOOL update_head = TRUE;
		cpos[1] = pStaticAllocator->last_pos;
		cpos[2] = pStaticAllocator->head_pos;
		while(cpos[1] > cpos[2])
		{
			if(cpos[2] < pStaticAllocator->max_size)
			{
				BOOL isInUse;
				StaticAllocator_ReadGuarder_(pStaticAllocator, &cpos[2]);
				isInUse = StaticAllocator_ReadSizeBlock_(pStaticAllocator, &cpos[2], &csize);
				if(isInUse == FALSE)
				{
					update_head = FALSE;
					if(csize >= size)
					{
						return StaticAllocator_Malloc_Result_(pStaticAllocator, &cpos[2], csize, size);
					}
				}
				cpos[2] += csize + STATIC_ALLOCATOR_HEAP_INTERNAL_DATA_SIZEDATA_SIZE;
				StaticAllocator_ReadGuarder_(pStaticAllocator, &cpos[2]);
				if(update_head == TRUE)
				{
					pStaticAllocator->head_pos = cpos[2];
				}
			}
			if(cpos[1] > 0)
			{
				BOOL isInUse;
				cpos[1] -= STATIC_ALLOCATOR_HEAP_GUARD_SIZE + STATIC_ALLOCATOR_HEAP_INTERNAL_DATA_SIZEDATA_SIZE;
				isInUse = StaticAllocator_ReadSizeBlock_(pStaticAllocator, &cpos[1], &csize);
				cpos[1] -= STATIC_ALLOCATOR_HEAP_INTERNAL_DATA_SIZEDATA_SIZE + csize;
				if(isInUse == FALSE && csize >= size)
				{
					return StaticAllocator_Malloc_Result_(pStaticAllocator, &cpos[1], csize, size);
				}
				cpos[1] -= STATIC_ALLOCATOR_HEAP_INTERNAL_DATA_SIZEDATA_SIZE;
			}
		}
		cpos[0] = pStaticAllocator->last_pos;
		while(cpos[0] < pStaticAllocator->max_size)
		{
			BOOL isInUse;
			StaticAllocator_ReadGuarder_(pStaticAllocator, &cpos[0]);
			isInUse = StaticAllocator_ReadSizeBlock_(pStaticAllocator, &cpos[0], &csize);
			if(isInUse == FALSE && csize >= size)
			{
				return StaticAllocator_Malloc_Result_(pStaticAllocator, &cpos[0], csize, size);
			}
			cpos[0] += csize + STATIC_ALLOCATOR_HEAP_INTERNAL_DATA_SIZEDATA_SIZE;
			StaticAllocator_ReadGuarder_(pStaticAllocator, &cpos[0]);
		}
	}
	OS_Warning("StaticAllocator_Malloc: memory allocation error, please increase heap size\n");
	SDK_ASSERT(0);
	return NULL;
}
//----------------------------------------------------------------------------------------------

void StaticAllocator_Free(struct StaticAllocator* pStaticAllocator, void* ptr)
{
	u32 csize;
	u32 nsize;
	u32 lpos;
	BOOL cuse;
	u32 gpos = (u32)((u8*)ptr - pStaticAllocator->internal_buf);
	u32 cpos = gpos;
#ifdef STATIC_ALLOCATOR_HEAP_MEM_DEBUG_DEEP
	StaticAllocator_CheckAllGuarders_(pStaticAllocator);
#endif
	if(ptr == NULL)
	{
		SDK_ASSERT(0);
		return;
	}
	if(gpos >= pStaticAllocator->max_size)
	{
		SDK_ASSERT(0);
		return;
	}
	if (pStaticAllocator->alloc_count == 0)
	{
		// reset mode
		return;
	}
	gpos -= STATIC_ALLOCATOR_HEAP_INTERNAL_DATA_BEGIN_SIZE;
	cpos = gpos;
	StaticAllocator_ReadGuarder_(pStaticAllocator, &cpos);
	cuse = StaticAllocator_ReadSizeBlock_(pStaticAllocator, &cpos, &csize);
	if(cuse == FALSE)
	{
		SDK_ASSERT(0);
		return;
	}
	pStaticAllocator->alloc_count--;
	pStaticAllocator->alloc_size -= (csize + STATIC_ALLOCATOR_HEAP_INTERNAL_DATA_SIZE);
	cpos -= STATIC_ALLOCATOR_HEAP_INTERNAL_DATA_SIZEDATA_SIZE;
	StaticAllocator_WriteSizeAndMoveToEndBlock_(pStaticAllocator, &cpos, csize, FALSE);
	lpos = gpos;
	if(gpos > 0)
	{
		cuse = FALSE;
		cpos = gpos;
		while(cuse == FALSE && cpos > 0)
		{
			cpos = gpos - (STATIC_ALLOCATOR_HEAP_GUARD_SIZE + STATIC_ALLOCATOR_HEAP_INTERNAL_DATA_SIZEDATA_SIZE);
			cuse = StaticAllocator_ReadSizeBlock_(pStaticAllocator, &cpos, &csize);
			if(cuse == TRUE)
			{
				break;
			}
			cpos -= STATIC_ALLOCATOR_HEAP_INTERNAL_DATA_SIZEDATA_SIZE + csize + STATIC_ALLOCATOR_HEAP_INTERNAL_DATA_SIZEDATA_SIZE;
			gpos = cpos;
		}
	}
	cuse = FALSE;
	cpos = lpos;
	while(cuse == FALSE && cpos < pStaticAllocator->max_size)
	{
		StaticAllocator_ReadGuarder_(pStaticAllocator, &cpos);
		cuse = StaticAllocator_ReadSizeBlock_(pStaticAllocator, &cpos, &csize);
		if(cuse == TRUE)
		{
			break;
		}
		cpos += csize + STATIC_ALLOCATOR_HEAP_INTERNAL_DATA_SIZEDATA_SIZE;
		StaticAllocator_ReadGuarder_(pStaticAllocator, &cpos);
		lpos = cpos;
	}
	nsize = lpos - gpos - STATIC_ALLOCATOR_HEAP_INTERNAL_DATA_SIZE;
	cpos = gpos;
	StaticAllocator_WriteGuarder_(pStaticAllocator, &cpos);
	StaticAllocator_WriteSizeAndMoveToEndBlock_(pStaticAllocator, &cpos, nsize, FALSE);
	if(gpos < pStaticAllocator->head_pos)
	{
		pStaticAllocator->head_pos = gpos;
	}
#ifdef STATIC_ALLOCATOR_HEAP_MEM_DEBUG_DEEP
	StaticAllocator_CheckAllGuarders_(pStaticAllocator);
#endif
}
//----------------------------------------------------------------------------------------------

u32 StaticAllocator_Check(struct StaticAllocator* pStaticAllocator, void* ptr)
{
	u32 csize;
	BOOL cuse;
	u32 gpos = (u32)((u8*)ptr - pStaticAllocator->internal_buf);
	u32 cpos = gpos;
#ifdef STATIC_ALLOCATOR_HEAP_MEM_DEBUG_DEEP
	StaticAllocator_CheckAllGuarders_(pStaticAllocator);
#endif
	if (ptr == NULL)
	{
		SDK_ASSERT(0);
		return 0;
	}
	if (gpos >= pStaticAllocator->max_size)
	{
		SDK_ASSERT(0);
		return 0;
	}
	gpos -= STATIC_ALLOCATOR_HEAP_INTERNAL_DATA_BEGIN_SIZE;
	cpos = gpos;
	StaticAllocator_ReadGuarder_(pStaticAllocator, &cpos);
	cuse = StaticAllocator_ReadSizeBlock_(pStaticAllocator, &cpos, &csize);
	if (cuse == FALSE)
	{
		SDK_ASSERT(0);
		return 0;
	}
	return csize;
}
//----------------------------------------------------------------------------------------------
