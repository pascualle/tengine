/*
tengine - cross-platform game engine
------------------------------------

Copyright (c) 2013 Papa Pascualle
Contact Email: pascualle@gmail.com

Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
*/

#include "hash.h"

//---------------------------------------------------------------------------

#define CRC16_POLYNOMIAL 0x8005

//---------------------------------------------------------------------------

static s32 is_table_initialized;
static u16 table[256];

//---------------------------------------------------------------------------

static u16 reverse_bits(u16 data, s32 bits)
{
    s32 i;
    u16 r = 0;
    for (i = 0; i < bits; i++)
    {
        r = (r << 1) | (data & 1);
        data >>= 1;
    }
    return r;
}
//---------------------------------------------------------------------------

u16 crc16_init(void)
{
    if (!is_table_initialized)
    {
        s32 i;
        for (i = 0; i < 256; i++)
        {
            s32 j;
            u16 crc = (u16)(reverse_bits((u16)i, 8) << 8);
			for (j = 0; j < 8; j++)
            {
                s32 divide = crc & 0x8000;
                crc <<= 1;
                if (divide)
                    crc ^= CRC16_POLYNOMIAL;
            }
            crc = reverse_bits(crc, 16);
            table[i] = crc;
        }
        is_table_initialized = 1;
    }
    return 0;
}
//---------------------------------------------------------------------------

u16 crc16_update(u16 crc, const void *data, size_t size)
{
    const char *p = (const char*)data;
    while (size--)
    {
        s32 idx = (crc ^ *p++) & 0xFF;
        crc = (crc >> 8) ^ table[idx];
    }
    return crc;
}
//---------------------------------------------------------------------------

u16 crc16_finalize(u16 crc)
{
    return crc;
}
//---------------------------------------------------------------------------
