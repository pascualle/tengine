/*
tengine - cross-platform game engine
------------------------------------

Copyright (c) 2013 Papa Pascualle
Contact Email: pascualle@gmail.com

Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
*/

#ifndef NITRO_SDK

#include "lib/render2dgl2.h"
#include "lib/gx_helpers.h"
#ifdef SDK_DEBUG
#include "lib/tengine_low.h"
#endif

#ifdef USE_OPENGL_2_RENDER

#include "filesystem.h"
#include "loadhelpers.h"
#include "texts.h"
#include "lib/jobs_low.h"
#include "static_allocator.h"
#include "containers/allocator_htable.h"
#include "containers/allocator_list.h"
#ifdef USE_EXTERNAL_API
#include "external.h"
#endif

#ifdef ANDROID_NDK
 #include <GLES2/gl2.h>
 #define glClearColor_x glClearColor
 #define glLineWidth_x glLineWidth
#else
 #ifdef IOS_APP
  #include "OpenGLES/ES2/gl.h"
  #define glClearColor_x glClearColor
  #define glLineWidth_x glLineWidth
 #else
  #include <GLES2/gl2.h>
  #if defined WINDOWS_APP || defined NIX_APP || defined EMSCRIPTEN_APP || defined KOLIBRIOS_APP
   #define glClearColor_x glClearColor
   #define glLineWidth_x glLineWidth
  #endif
 #endif
#endif

#ifdef __BORLANDC__
#pragma warn -8064
#endif

//#define DEBUG_BG_CHUNKS

#define GX_COLOR_CHANNEL(c) (((f32)(c)) / (f32)0x1f)
#define GX_float_TO_FX32(c) (FX32(c))

#define DIFD_DEF_FILL_COLOR_FULL_ALPHA ((ALPHA_OPAQ << 16) | DIFD_DEF_FILL_COLOR)

static const s32 NULL_CLIP_RECT = ((((u16)-8192)) << 16) | ((u16)-8192);

enum RGLLineStyle
{
	RGLLSSolid = 0xFFFF,
	RGLLSDot = 0xAAAA,
	RGLLSDash = 0xCCCC
};

enum RGLRenderTypes
{
	RGL_PIXEL = 0,
	RGL_LINE,
	RGL_FILLRECT,
	RGL_IMAGE,
	RGL_EXTERNAL_POS_COLOR
};

struct TRGLColor
{
	float r;
	float g;
	float b;
	float a;
	GXRgba rgba;
};

struct TRGLUnlitVertexData
{
	float pos[2];
	float uv0[2];
};

struct TRGLLitVertexData
{
	float pos[2];
	float uv0[2];
	float color[4];
};

enum RGLExDClipIdx
{
	EXDICLIPRECTXY = 0,
	EXDICLIPRECTWH,
	EXDICLIPRECTCOUNT
};

struct TRGLRenderItem
{
	const struct BMPImage *mpImageHeader;
	struct TRGLRenderItem *mpNext;
	const void *mpDataPtr;
	s32 mObjDataSize;
	GLuint mVBOBufferIdx;
	u32 mFillWithColor;
	s32 mClipRect[EXDICLIPRECTCOUNT];
	u8 mType;
};

struct TRGLBGItem
{
	u32 mObjDataSize;
	GLuint mVBOBufferIdx;
#ifdef USE_TILEMAP_ATTRIBUTES
	u32 mFillWithColor;
#endif
	float *mpDataPtr;
	const struct BMPImage *mpImageHeader;
};

static struct RenderPlane
{
	s32 mOffX;
	s32	mOffY;
	s32	mViewWidth;
	s32	mViewHeight;
	s32 mRenderListSize;
	s32 mObjTextureListSize;
	u32 mBGMaxLayers;
	u32 mBGMaxTextures;
	u32 mBGMaxElements;
	s32 mTotalObjDataSize;
#ifdef USE_TILEMAP_ATTRIBUTES
	s32* mpBGVertexCt;
#endif
	u8* mpBGDataAllocatorHeap;
	struct StaticAllocator mBGDataAllocator;
	struct AllocatorHTable *mpBGTexturesTable;
	struct TRGLRenderItem *mpRenderListPool;
	struct TRGLRenderItem *mpRenderList;
	struct TRGLRenderItem *mpRenderListTail;
	GLuint *mpBGVBOBuffer;
	GLuint *mpVBOBuffer;
	u8 *mpObjDataPool;
	float *mpBGDataPool;
	GLuint mBGVBOBufferLastIdx;
	GLuint mObjVBOBufferLastIdx;
	struct TRGLColor mColor;
	float mScale;
	float mLineWidth;
	s32 mDraw;
	s32 mMaxObjOnScene;
}gsPlane[BGSELECT_NUM];

enum VertexAttrib
{
	VertexAttrib_Position,
	VertexAttrib_UV0,
	VertexAttrib_Color0,
	VertexAttrib_MAX
};

enum Uniform
{
	Uniform_Texture0,
	Uniform_MVP,
	Uniform_MAX
};

struct Binging
{
	GLint attrib[VertexAttrib_MAX];
	GLint uniform[Uniform_MAX];
};

struct Shader
{
	struct Binging binging;
	GLuint handle;
};

enum ShaderType
{
	ShaderType_None = -1,
	ShaderType_Lit = 0,
	ShaderType_Unlit,
	ShaderType_Color,
	ShaderType_MAX
};

struct TMatrix44
{
	float data[4][4];
};

static struct Shader gsShader[ShaderType_MAX];
static struct TMatrix44 gsMVP[BGSELECT_NUM];
static enum BGSelect gsActivePlane = BGSELECT_NUM;
static BOOL gsGraphicsInit = FALSE;
static s32 gsActiveClipRect[EXDICLIPRECTCOUNT];
static GLuint *mpTexture = NULL;
static GLuint mTextureCount = 0;
static BOOL sgRenderListReady = FALSE;
static s32 mCurrentTextureID = -1;
static enum ShaderType mShaderType = ShaderType_MAX;
static s32 sgScreenWidth = -1;
static s32 sgScreenHeight = -1;
static BOOL gsLostDevice = TRUE;
static BOOL gsFreezeCurrentFrameBuffer = FALSE;
#ifdef USE_TILEMAP_ATTRIBUTES
static u32 sgBGChunkSize = 1;
static const u32 gsBGMaxFillColor = 255;
#endif

static BOOL _glRender_CheckForCleanup(enum BGSelect iType);
static void _glRender_AddToRenderList(struct TRGLRenderItem* item);
static void _glRender_CheckError(const char* op);

#ifdef WINDOWS_APP
static PROC glGenBuffers_fn = NULL;
static PROC glBindBuffer_fn = NULL;
static PROC glBufferData_fn = NULL;
static PROC glBufferSubData_fn = NULL;
static PROC glDeleteBuffers_fn = NULL;
static PROC glGetProgramiv_fn = NULL;
static PROC glValidateProgram_fn = NULL;
static PROC glGetShaderiv_fn = NULL;
static PROC glGetShaderInfoLog_fn = NULL;
static PROC glCompileShader_fn = NULL;
static PROC glShaderSource_fn = NULL;
static PROC glCreateShader_fn = NULL;
static PROC glDeleteShader_fn = NULL;
static PROC glLinkProgram_fn = NULL;
static PROC glAttachShader_fn = NULL;
static PROC glDetachShader_fn = NULL;
static PROC glCreateProgram_fn = NULL;
static PROC glDeleteProgram_fn = NULL;
static PROC glGetAttribLocation_fn = NULL;
static PROC glVertexAttribPointer_fn = NULL;
static PROC glEnableVertexAttribArray_fn = NULL;
static PROC glDisableVertexAttribArray_fn = NULL;
static PROC glUseProgram_fn = NULL;
static PROC glUniform1i_fn = NULL;
static PROC glGetUniformLocation_fn = NULL;
static PROC glUniformMatrix4fv_fn = NULL;
static PROC glActiveTexture_fn = NULL;
#else
#define glGenBuffers_fn glGenBuffers
#define glBindBuffer_fn glBindBuffer
#define glBufferData_fn glBufferData
#define glBufferSubData_fn glBufferSubData
#define glDeleteBuffers_fn glDeleteBuffers
#define glGetProgramiv_fn glGetProgramiv
#define glValidateProgram_fn glValidateProgram
#define glGetShaderiv_fn glGetShaderiv
#define glGetShaderInfoLog_fn glGetShaderInfoLog
#define glCompileShader_fn glCompileShader
#define glShaderSource_fn glShaderSource
#define glCreateShader_fn glCreateShader
#define glDeleteShader_fn glDeleteShader
#define glLinkProgram_fn glLinkProgram
#define glAttachShader_fn glAttachShader
#define glDetachShader_fn glDetachShader
#define glCreateProgram_fn glCreateProgram
#define glDeleteProgram_fn glDeleteProgram
#define glGetAttribLocation_fn glGetAttribLocation
#define glVertexAttribPointer_fn glVertexAttribPointer
#define glEnableVertexAttribArray_fn glEnableVertexAttribArray
#define glDisableVertexAttribArray_fn glDisableVertexAttribArray
#define glUseProgram_fn glUseProgram
#define glUniform1i_fn glUniform1i
#define glGetUniformLocation_fn glGetUniformLocation
#define glUniformMatrix4fv_fn glUniformMatrix4fv
#define glActiveTexture_fn glActiveTexture
#endif

#define F32_EPSILON FX_FX32_TO_F32(FX32(0.00001))

static const char* gsShaderVertexAttribNames[VertexAttrib_MAX] = 
{
	"a_Position",
	"a_TextureCoordinates",
	"a_Color"
};

static const char* gsShaderUniformNames[VertexAttrib_MAX] = 
{
	"u_TextureUnit",
	"u_MVP"
};

static GLuint s_disable_caps[] =
{
	GL_CULL_FACE,
	GL_BLEND,
	GL_DITHER,
	GL_STENCIL_TEST,
	GL_DEPTH_TEST,
	0
};

#ifdef SDK_DEBUG
extern struct TEngineCommonData cd;
#endif

static void _glRender_buildProgram(struct Shader* shader, const u8* vertex_shader_source, const GLint vertex_shader_source_length,
								const u8* fragment_shader_source, const GLint fragment_shader_source_length);
static void _glRender_BindShader(enum ShaderType shaderType, enum BGSelect bgType);
static void _glRender_ColorPointer(enum ShaderType shaderType, GLint count, GLuint type, GLint stride, const void* ptr);
static void _glRender_TexCoordPointer(enum ShaderType shaderType, GLint count, GLuint type, GLint stride, const void* ptr);
static void _glRender_VertexPointer(enum ShaderType shaderType, GLint count, GLuint type, GLint stride, const void* ptr);
static void _glRender_LoadShader(enum ShaderType type, const char* vsh, const char* fsh);
static void _glRender_Ortho_impl(struct TMatrix44 *m, const float left, const float top, const float right, const float bottom, const float zNear, const float zFar);
static void _glRender_CalculateOrthoMatrix(enum BGSelect plane);

//----------------------------------------------------------------------------------
//----------------------------------------------------------------------------------
//----------------------------------------------------------------------------------

void _glRender_CheckError(const char* op)
{
#ifdef SDK_DEBUG
	GLenum error = glGetError();
	if (error != GL_NO_ERROR)
	{
		OS_Warning("after %s() glError (0x%x)\n", op, error);
	}
#else
	(void)op;
#endif
}
//----------------------------------------------------------------------------------

void glRender_Init()
{
	s32 type;
	for(type = 0; type < BGSELECT_NUM; type++)
	{
		MI_CpuFill8(&gsPlane[type], 0, sizeof(struct RenderPlane));
		gsPlane[type].mScale = FX_FX32_TO_F32(FX32_ONE);
		gsPlane[type].mLineWidth = FX_FX32_TO_F32(FX32_ONE);
	}
	for(type = 0; type < ShaderType_MAX; type++)
	{
		MI_CpuClear8(&gsShader[type], sizeof(struct Shader));
	}
	gsGraphicsInit = TRUE;
	sgScreenWidth = 0;
	sgScreenHeight = 0;
	mCurrentTextureID = -1;
	mTextureCount = 0;
	gsActiveClipRect[EXDICLIPRECTXY] = NULL_CLIP_RECT;
	gsActiveClipRect[EXDICLIPRECTWH] = NULL_CLIP_RECT;
}
//----------------------------------------------------------------------------------

void glRender_Release()
{
	s32 type;
	SDK_ASSERT(mTextureCount == 0); // please call glRender_DeleteTextures() before
	for(type = 0; type < BGSELECT_NUM; type++)
	{
		gsPlane[type].mScale = FX_FX32_TO_F32(FX32_ONE);
		gsPlane[type].mLineWidth = FX_FX32_TO_F32(FX32_ONE);
	}
	gsGraphicsInit = FALSE;
}
//----------------------------------------------------------------------------------

void glRender_LostDevice(void)
{
	gsLostDevice = TRUE;
}
//----------------------------------------------------------------------------------

void _glRender_BindShader(enum ShaderType shaderType, enum BGSelect bgType)
{
	if (mShaderType != shaderType)
	{
		mShaderType = shaderType;
		if (shaderType != ShaderType_None)
		{
			SDK_ASSERT(bgType < BGSELECT_NUM);
			glUseProgram_fn(gsShader[shaderType].handle);
			glUniformMatrix4fv_fn(gsShader[shaderType].binging.uniform[Uniform_MVP], 1, GL_FALSE, &gsMVP[bgType].data[0][0]);
		}
		else
		{
			glUseProgram_fn(0);
		}
		//reset old vertex attrib
		glDisableVertexAttribArray_fn(0);
		glDisableVertexAttribArray_fn(1);
		glDisableVertexAttribArray_fn(2);
		glBindBuffer_fn(GL_ELEMENT_ARRAY_BUFFER, 0);
		glBindBuffer_fn(GL_ARRAY_BUFFER, 0);
	}
}
//----------------------------------------------------------------------------------

void _glRender_ColorPointer(enum ShaderType shaderType, GLint count, GLuint type, GLint stride, const void* ptr)
{
	glEnableVertexAttribArray_fn(gsShader[shaderType].binging.attrib[VertexAttrib_Color0]);
	glVertexAttribPointer_fn(gsShader[shaderType].binging.attrib[VertexAttrib_Color0], count, type, GL_FALSE, stride, ptr);
}
//----------------------------------------------------------------------------------

void _glRender_TexCoordPointer(enum ShaderType shaderType, GLint count, GLuint type, GLint stride, const void* ptr)
{
	glEnableVertexAttribArray_fn(gsShader[shaderType].binging.attrib[VertexAttrib_UV0]);
	glVertexAttribPointer_fn(gsShader[shaderType].binging.attrib[VertexAttrib_UV0], count, type, GL_FALSE, stride, ptr);
}
//----------------------------------------------------------------------------------

void _glRender_VertexPointer(enum ShaderType shaderType, GLint count, GLuint type, GLint stride, const void* ptr)
{
	glEnableVertexAttribArray_fn(gsShader[shaderType].binging.attrib[VertexAttrib_Position]);
	glVertexAttribPointer_fn(gsShader[shaderType].binging.attrib[VertexAttrib_Position], count, type, GL_FALSE, stride, ptr);
}
//----------------------------------------------------------------------------------

void _glRender_CalculateOrthoMatrix(enum BGSelect plane)
{
	if(gsPlane[plane].mpObjDataPool != NULL)
	{
#if defined USE_OSMESA
		_glRender_Ortho_impl(&gsMVP[plane],
								0,
								FX_FX32_TO_F32(FX_Div(FX32(gsPlane[plane].mViewHeight), GX_float_TO_FX32(gsPlane[plane].mScale))),
								FX_FX32_TO_F32(FX_Div(FX32(gsPlane[plane].mViewWidth), GX_float_TO_FX32(gsPlane[plane].mScale))),
								0,
								-FX_FX32_TO_F32(FX32_ONE), 0);
#else
		_glRender_Ortho_impl(&gsMVP[plane],
							 0,
							 0,
							 FX_FX32_TO_F32(FX_Div(FX32(gsPlane[plane].mViewWidth), GX_float_TO_FX32(gsPlane[plane].mScale))),
							 FX_FX32_TO_F32(FX_Div(FX32(gsPlane[plane].mViewHeight), GX_float_TO_FX32(gsPlane[plane].mScale))),
							 -FX_FX32_TO_F32(FX32_ONE), 0);
#endif
	}
}
//----------------------------------------------------------------------------------

void _glRender_Ortho_impl(struct TMatrix44 *m, const float l, const float t, const float r, const float b, const float zn, const float zf)
{
	m->data[0][0] = 2.0f / (r - l);
	m->data[0][1] = 0.0f;
	m->data[0][2] = 0.0f;
	m->data[0][3] = 0.0f;
	m->data[1][0] = 0.0f;
	m->data[1][1] = 2.0f / (t - b);
	m->data[1][2] = 0.0f;
	m->data[1][3] = 0.0f;
	m->data[2][0] = 0.0f;
	m->data[2][1] = 0.0f;
	m->data[2][2] = 1.0f / (zn - zf);
	m->data[2][3] = 0.0f;
	m->data[3][0] = (l + r) / (l - r);
	m->data[3][1] = (t + b) / (b - t);
	m->data[3][2] = zn / (zn - zf);
	m->data[3][3] = 1.0f;
}
//----------------------------------------------------------------------------------

static void _glRender_LoadShader(enum ShaderType type, const char* vsh, const char* fsh)
{
	s32 it, end;
	u32 fsh_l, vsh_l;
	u8* pvsh;
	u8*	pfsh;
#if defined WINDOWS_APP || defined NIX_APP || defined KOLIBRIOS_APP
	u32 add_p_l;
	char* pvsh_d;
	char* pfsh_d;
	const char *non_es_prefix = "\
#define lowp \n\
#define mediump \n\
#define highp \n\
";
	add_p_l = STD_StrLen(non_es_prefix);
#endif
	SDK_ASSERT(gsShader[type].handle == 0);
	MI_CpuClear8(&gsShader[type], sizeof(struct Shader));
	pvsh = LoadFile(vsh, &vsh_l);
	pfsh = LoadFile(fsh, &fsh_l);
#if defined WINDOWS_APP || defined NIX_APP || defined KOLIBRIOS_APP
	pvsh_d = (char*)MALLOC(vsh_l + add_p_l + 1, "pvsh_d");
	STD_StrCpy(pvsh_d, non_es_prefix);
	MI_CpuCopy8(pvsh, pvsh_d + add_p_l, vsh_l);
	pvsh_d[vsh_l + add_p_l] = '\0';
	pfsh_d = (char*)MALLOC(fsh_l + add_p_l + 1, "pfsh_d");
	STD_StrCpy(pfsh_d, non_es_prefix);
	MI_CpuCopy8(pfsh, pfsh_d + add_p_l, fsh_l);
	pfsh_d[fsh_l + add_p_l] = '\0';
	/*it = STD_StrLen(pfsh_d);*/
	_glRender_buildProgram(&gsShader[type], (u8*)pvsh_d, (GLint)(vsh_l + add_p_l), (u8*)pfsh_d, (GLint)(fsh_l + add_p_l));
	FREE(pfsh_d);
	FREE(pvsh_d);
#else
	_glRender_buildProgram(&gsShader[type], pvsh, (GLint)vsh_l, pfsh, (GLint)fsh_l);
#endif
	FREE(pfsh);
	FREE(pvsh);
	for(it = 0, end = VertexAttrib_MAX; it != end; ++it)
	{
		gsShader[type].binging.attrib[it] = glGetAttribLocation_fn(gsShader[type].handle, gsShaderVertexAttribNames[it]);
	}
	for(it = 0, end = Uniform_MAX; it != end; ++it)
	{
		gsShader[type].binging.uniform[it] = glGetUniformLocation_fn(gsShader[type].handle, gsShaderUniformNames[it]);
	}
	glUseProgram_fn(gsShader[type].handle);
	if(gsShader[type].binging.uniform[Uniform_Texture0] != -1)
	{
		glUniform1i_fn(gsShader[type].binging.uniform[Uniform_Texture0], 0);
	}
}
//----------------------------------------------------------------------------------

void glRender_RestoreDevice(void)
{
    GLuint *start = s_disable_caps;

	gsLostDevice = FALSE;
	
#if defined WINDOWS_APP
	glGenBuffers_fn = (PROC)wglGetProcAddress("glGenBuffers");
	SDK_NULL_ASSERT(glGenBuffers_fn);
	glBindBuffer_fn = (PROC)wglGetProcAddress("glBindBuffer");
	SDK_NULL_ASSERT(glBindBuffer_fn);
	glBufferData_fn = (PROC)wglGetProcAddress("glBufferData");
	SDK_NULL_ASSERT(glBufferData_fn);
	glBufferSubData_fn = (PROC)wglGetProcAddress("glBufferSubData");
	SDK_NULL_ASSERT(glBufferSubData_fn);
	glDeleteBuffers_fn = (PROC)wglGetProcAddress("glDeleteBuffers");
	SDK_NULL_ASSERT(glDeleteBuffers_fn);
	glGetProgramiv_fn = (PROC)wglGetProcAddress("glGetProgramiv");
	SDK_NULL_ASSERT(glGetProgramiv_fn);
	glValidateProgram_fn = (PROC)wglGetProcAddress("glValidateProgram");
	SDK_NULL_ASSERT(glValidateProgram_fn);
	glGetShaderInfoLog_fn = (PROC)wglGetProcAddress("glGetShaderInfoLog");
	SDK_NULL_ASSERT(glGetShaderInfoLog_fn);
	glGetShaderiv_fn = (PROC)wglGetProcAddress("glGetShaderiv");
	SDK_NULL_ASSERT(glGetShaderiv_fn);
	glCompileShader_fn = (PROC)wglGetProcAddress("glCompileShader");
	SDK_NULL_ASSERT(glCompileShader_fn);
	glShaderSource_fn = (PROC)wglGetProcAddress("glShaderSource");
	SDK_NULL_ASSERT(glShaderSource_fn);
	glCreateShader_fn = (PROC)wglGetProcAddress("glCreateShader");
	SDK_NULL_ASSERT(glCreateShader_fn);
	glDeleteShader_fn = (PROC)wglGetProcAddress("glDeleteShader");
	SDK_NULL_ASSERT(glCreateShader_fn);
	glLinkProgram_fn = (PROC)wglGetProcAddress("glLinkProgram");
	SDK_NULL_ASSERT(glLinkProgram_fn);
	glAttachShader_fn = (PROC)wglGetProcAddress("glAttachShader");
	SDK_NULL_ASSERT(glAttachShader_fn);
	glDetachShader_fn = (PROC)wglGetProcAddress("glDetachShader");
	SDK_NULL_ASSERT(glDetachShader_fn);
	glCreateProgram_fn = (PROC)wglGetProcAddress("glCreateProgram");
	SDK_NULL_ASSERT(glCreateProgram_fn);
	glDeleteProgram_fn = (PROC)wglGetProcAddress("glDeleteProgram");
	SDK_NULL_ASSERT(glDeleteProgram_fn);
	glGetAttribLocation_fn = (PROC)wglGetProcAddress("glGetAttribLocation");
	SDK_NULL_ASSERT(glGetAttribLocation_fn);
	glVertexAttribPointer_fn = (PROC)wglGetProcAddress("glVertexAttribPointer");
	SDK_NULL_ASSERT(glVertexAttribPointer_fn);
	glEnableVertexAttribArray_fn = (PROC)wglGetProcAddress("glEnableVertexAttribArray");
	SDK_NULL_ASSERT(glEnableVertexAttribArray_fn);
	glDisableVertexAttribArray_fn = (PROC)wglGetProcAddress("glDisableVertexAttribArray");
	SDK_NULL_ASSERT(glDisableVertexAttribArray_fn);
	glUseProgram_fn = (PROC)wglGetProcAddress("glUseProgram");
	SDK_NULL_ASSERT(glUseProgram_fn);
	glUniform1i_fn = (PROC)wglGetProcAddress("glUniform1i");
	SDK_NULL_ASSERT(glUniform1i_fn);
	glGetUniformLocation_fn = (PROC)wglGetProcAddress("glGetUniformLocation");
	SDK_NULL_ASSERT(glGetUniformLocation_fn);
	glUniformMatrix4fv_fn = (PROC)wglGetProcAddress("glUniformMatrix4fv");
	SDK_NULL_ASSERT(glUniformMatrix4fv_fn);
	glActiveTexture_fn = (PROC)wglGetProcAddress("glActiveTexture");
	SDK_NULL_ASSERT(glActiveTexture_fn);
#endif
    
#ifdef SDK_DEBUG
    glClearColor_x(FX_FX32_TO_F32(FX32(0.5f)), FX_FX32_TO_F32(FX32(0.5f)), FX_FX32_TO_F32(FX32(0.5f)), FX_FX32_TO_F32(FX32_ONE));
#else
    glClearColor_x(0, 0, 0, FX_FX32_TO_F32(FX32_ONE));
#endif

    while(*start)
    {
        glDisable(*start++);
        _glRender_CheckError("glDisable");
    }

	glActiveTexture_fn(GL_TEXTURE0);
	_glRender_CheckError("glActiveTexture");

	mCurrentTextureID = -1;
	mShaderType = ShaderType_MAX;
}
//----------------------------------------------------------------------------------

void glRender_SetRenderPlaneScale(fx32 val, enum BGSelect iBG)
{
	SDK_ASSERT(val > 0);
	SDK_ASSERT(iBG < BGSELECT_NUM);
	gsPlane[iBG].mScale = FX_FX32_TO_F32(val);
	gsPlane[iBG].mLineWidth = FX_FX32_TO_F32(FX32_ONE); 
	while(gsPlane[iBG].mScale > gsPlane[iBG].mLineWidth)
	{
		gsPlane[iBG].mLineWidth += FX_FX32_TO_F32(FX32_ONE);
	}
	_glRender_CalculateOrthoMatrix(iBG);
}
//----------------------------------------------------------------------------------

fx32 glRender_GetRenderPlaneScale(enum BGSelect iBG)
{
	SDK_ASSERT(iBG < BGSELECT_NUM);
	return GX_float_TO_FX32(gsPlane[iBG].mScale);
}
//----------------------------------------------------------------------------------

void glRender_Resize(s32 w, s32 h)
{
	sgScreenWidth = w;
	sgScreenHeight = h;
	if(gsLostDevice == FALSE)
	{
        glRender_RestoreDevice();
	}
}
//----------------------------------------------------------------------------------

void glRender_DrawFrame()
{
	struct TRGLRenderItem *curr;
	BOOL clear;
	s32 plane;

	SDK_ASSERT(sgScreenWidth > 0);
	SDK_ASSERT(gsGraphicsInit == TRUE); // please init graphics system before

	clear = FALSE;
	for(plane = 0; plane < BGSELECT_NUM; plane++)
	{
		if(gsPlane[plane].mpObjDataPool != NULL && (gsPlane[plane].mDraw == 1 || gsPlane[plane].mDraw == 2))
		{
			++gsPlane[plane].mDraw;

			if(clear == FALSE)
			{
				if(!glIsEnabled(GL_BLEND))
				{
					glEnable(GL_BLEND);
					glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
				}
				glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
				clear = TRUE;
			}

			glViewport((GLint)(gsPlane[plane].mOffX),
						sgScreenHeight - 
						(GLint)(gsPlane[plane].mViewHeight) - 
						(GLint)(gsPlane[plane].mOffY),
						(GLint)(gsPlane[plane].mViewWidth),
						(GLint)(gsPlane[plane].mViewHeight));

			if (gsPlane[plane].mBGMaxLayers > 0)
			{
				u32 j;
#ifndef USE_TILEMAP_ATTRIBUTES
				_glRender_BindShader(ShaderType_Unlit, plane);
#endif
				for (j = 0; j < gsPlane[plane].mBGMaxLayers; j++)
				{
					const struct AllocatorHTable* ht = &gsPlane[plane].mpBGTexturesTable[j];
					const struct HTableItem* it = AllocatorHTable_Begin_Const(ht);
					while (it != AllocatorHTable_End_Const(ht))
					{
#ifdef USE_TILEMAP_ATTRIBUTES
						const struct AllocatorHTable* ot = (const struct AllocatorHTable*)AllocatorHTable_Val(ht, it);
						const struct HTableItem* oit = AllocatorHTable_Begin_Const(ot);
						while (oit != AllocatorHTable_End_Const(ot))
						{
							const struct AllocatorList* l = (const struct AllocatorList*)AllocatorHTable_Val(ot, oit);
							const struct ListItem* lit = AllocatorList_Begin_Const(l);
							while (lit != AllocatorList_End_Const(l))
							{
								const struct TRGLBGItem* item = (const struct TRGLBGItem*)AllocatorList_Val(l, lit);
#else
								const struct TRGLBGItem* item = (const struct TRGLBGItem*)AllocatorHTable_Val(ht, it);
#endif
								if (item->mpImageHeader->mOpaqType != mCurrentTextureID)
								{
									mCurrentTextureID = item->mpImageHeader->mOpaqType;
									SDK_NULL_ASSERT(mpTexture);
									glBindTexture(GL_TEXTURE_2D, mpTexture[mCurrentTextureID]);
									_glRender_CheckError("glBindTexture");
								}
#ifdef USE_TILEMAP_ATTRIBUTES
#ifdef DEBUG_BG_CHUNKS
								if (item->mObjDataSize > 0)
#else
								if (item->mFillWithColor != DIFD_DEF_FILL_COLOR_FULL_ALPHA)
#endif
								{									
									if (item->mObjDataSize > 0)
									{
										_glRender_BindShader(ShaderType_Lit, plane);
										glBindBuffer_fn(GL_ARRAY_BUFFER, gsPlane[plane].mpBGVBOBuffer[item->mVBOBufferIdx]);
										glBufferData_fn(GL_ARRAY_BUFFER, item->mObjDataSize * sizeof(struct TRGLLitVertexData), item->mpDataPtr, GL_DYNAMIC_DRAW);
										_glRender_ColorPointer(ShaderType_Lit, 4, GL_FLOAT, sizeof(struct TRGLLitVertexData), (const void*)16);
										_glRender_TexCoordPointer(ShaderType_Lit, 2, GL_FLOAT, sizeof(struct TRGLLitVertexData), (const void*)8);
										_glRender_VertexPointer(ShaderType_Lit, 2, GL_FLOAT, sizeof(struct TRGLLitVertexData), (const void*)0);
										glDrawArrays(GL_TRIANGLE_STRIP, 0, item->mObjDataSize);
									}
								}
								else
								{
									if (item->mObjDataSize > 0)
									{
										_glRender_BindShader(ShaderType_Unlit, plane);
										glBindBuffer_fn(GL_ARRAY_BUFFER, gsPlane[plane].mpBGVBOBuffer[item->mVBOBufferIdx]);
										glBufferData_fn(GL_ARRAY_BUFFER, item->mObjDataSize * sizeof(struct TRGLUnlitVertexData), item->mpDataPtr, GL_DYNAMIC_DRAW);
										_glRender_TexCoordPointer(ShaderType_Unlit, 2, GL_FLOAT, sizeof(struct TRGLUnlitVertexData), (const void*)8);
										_glRender_VertexPointer(ShaderType_Unlit, 2, GL_FLOAT, sizeof(struct TRGLUnlitVertexData), (const void*)0);
										glDrawArrays(GL_TRIANGLE_STRIP, 0, item->mObjDataSize);
									}
								}
#else
								if (item->mObjDataSize > 0)
								{
									glBindBuffer_fn(GL_ARRAY_BUFFER, gsPlane[plane].mpBGVBOBuffer[item->mVBOBufferIdx]);
									glBufferData_fn(GL_ARRAY_BUFFER, item->mObjDataSize * sizeof(struct TRGLUnlitVertexData), item->mpDataPtr, GL_DYNAMIC_DRAW);
									_glRender_TexCoordPointer(ShaderType_Unlit, 2, GL_FLOAT, sizeof(struct TRGLUnlitVertexData), (const void*)8);
									_glRender_VertexPointer(ShaderType_Unlit, 2, GL_FLOAT, sizeof(struct TRGLUnlitVertexData), (const void*)0);
									glDrawArrays(GL_TRIANGLE_STRIP, 0, item->mObjDataSize);
								}
#endif
#ifdef USE_TILEMAP_ATTRIBUTES
								lit = AllocatorList_Next_Const(l, lit);
							}
							oit = AllocatorHTable_Next_Const(ot, oit);
						}
#endif
						it = AllocatorHTable_Next_Const(ht, it);
					}
				}
			}

			if(gsPlane[plane].mRenderListSize >= 0 && sgRenderListReady == TRUE)
			{
				curr = gsPlane[plane].mpRenderList;
				while(curr != NULL)
				{
					SDK_ASSERT(gsPlane[plane].mpRenderListTail->mpNext == NULL);			
					switch(curr->mType)
					{
						case RGL_IMAGE:
							if(mTextureCount > 0)
							{
#ifdef JOBS_IN_SEPARATE_THREAD
								jobCriticalSectionBegin();	
#endif
								if(curr->mpImageHeader->mOpaqType != mCurrentTextureID)
								{
									mCurrentTextureID = curr->mpImageHeader->mOpaqType;
									SDK_NULL_ASSERT(mpTexture);
									glBindTexture(GL_TEXTURE_2D, mpTexture[mCurrentTextureID]);
									_glRender_CheckError("glBindTexture");
								}

								if(curr->mClipRect[EXDICLIPRECTXY] != NULL_CLIP_RECT)
								{
									GLint scsrx, scsry;
									scsrx = (GLint)(((gsPlane[plane].mOffX + (s16)((curr->mClipRect[EXDICLIPRECTXY] >> 16) & 0xffff)) * gsPlane[plane].mScale) / FX_FX32_TO_F32(FX32_ONE));
									scsry = sgScreenHeight -
										(GLint)(((curr->mClipRect[EXDICLIPRECTWH] & 0xffff) * gsPlane[plane].mScale) / FX_FX32_TO_F32(FX32_ONE)) -
										(GLint)(((gsPlane[plane].mOffY + (s16)(curr->mClipRect[EXDICLIPRECTXY] & 0xffff)) * gsPlane[plane].mScale) / FX_FX32_TO_F32(FX32_ONE));
									glEnable(GL_SCISSOR_TEST);
									glScissor(scsrx, scsry,
										(GLint)((((curr->mClipRect[EXDICLIPRECTWH] >> 16) & 0xffff) * gsPlane[plane].mScale) / FX_FX32_TO_F32(FX32_ONE)),
										(GLint)(((curr->mClipRect[EXDICLIPRECTWH] & 0xffff) * gsPlane[plane].mScale) / FX_FX32_TO_F32(FX32_ONE)));
								}
								if(curr->mFillWithColor != DIFD_DEF_FILL_COLOR_FULL_ALPHA)
								{
									_glRender_BindShader(ShaderType_Lit, plane);
									glBindBuffer_fn(GL_ARRAY_BUFFER, gsPlane[plane].mpVBOBuffer[curr->mVBOBufferIdx]);
									if(curr->mObjDataSize > 0)
									{
										glBufferData_fn(GL_ARRAY_BUFFER, curr->mObjDataSize * sizeof(struct TRGLLitVertexData), curr->mpDataPtr, GL_DYNAMIC_DRAW);
										curr->mObjDataSize = -curr->mObjDataSize;
									}
									_glRender_ColorPointer(ShaderType_Lit, 4, GL_FLOAT, sizeof(struct TRGLLitVertexData), (const void*)16);
									_glRender_TexCoordPointer(ShaderType_Lit, 2, GL_FLOAT, sizeof(struct TRGLLitVertexData), (const void*)8);
									_glRender_VertexPointer(ShaderType_Lit, 2, GL_FLOAT, sizeof(struct TRGLLitVertexData), (const void*)0);
									glDrawArrays(GL_TRIANGLE_STRIP, 0, -curr->mObjDataSize);
								}
								else
								{
									_glRender_BindShader(ShaderType_Unlit, plane);
									glBindBuffer_fn(GL_ARRAY_BUFFER, gsPlane[plane].mpVBOBuffer[curr->mVBOBufferIdx]);
									if(curr->mObjDataSize > 0)
									{
										glBufferData_fn(GL_ARRAY_BUFFER, curr->mObjDataSize * sizeof(struct TRGLUnlitVertexData), curr->mpDataPtr, GL_DYNAMIC_DRAW);
										curr->mObjDataSize = -curr->mObjDataSize;
									}
									_glRender_TexCoordPointer(ShaderType_Unlit, 2, GL_FLOAT, sizeof(struct TRGLUnlitVertexData), (const void*)8);
									_glRender_VertexPointer(ShaderType_Unlit, 2, GL_FLOAT, sizeof(struct TRGLUnlitVertexData), (const void*)0);
									glDrawArrays(GL_TRIANGLE_STRIP, 0, -curr->mObjDataSize);
								}
								if(curr->mClipRect[EXDICLIPRECTXY] != NULL_CLIP_RECT)
								{
									glDisable(GL_SCISSOR_TEST);
								}
#ifdef JOBS_IN_SEPARATE_THREAD
								jobCriticalSectionEnd();	
#endif
							}
							break;
							
						case RGL_LINE:
							_glRender_BindShader(ShaderType_Color, plane);
							glLineWidth_x(gsPlane[gsActivePlane].mLineWidth);
							glBindBuffer_fn(GL_ARRAY_BUFFER, gsPlane[plane].mpVBOBuffer[curr->mVBOBufferIdx]);
							if(curr->mObjDataSize > 0)
							{
								glBufferData_fn(GL_ARRAY_BUFFER, curr->mObjDataSize * sizeof(struct TRGLColorVertexData), curr->mpDataPtr, GL_DYNAMIC_DRAW);
								curr->mObjDataSize = -curr->mObjDataSize;
							}
							_glRender_ColorPointer(ShaderType_Color, 4, GL_FLOAT, sizeof(struct TRGLColorVertexData), (const void*)8);
							_glRender_VertexPointer(ShaderType_Color, 2, GL_FLOAT, sizeof(struct TRGLColorVertexData), (const void*)0);
							glDrawArrays(GL_LINES, 0, -curr->mObjDataSize);
						break;

						case RGL_FILLRECT:
							_glRender_BindShader(ShaderType_Color, plane);
							glBindBuffer_fn(GL_ARRAY_BUFFER, gsPlane[plane].mpVBOBuffer[curr->mVBOBufferIdx]);
							if(curr->mObjDataSize > 0)
							{
								glBufferData_fn(GL_ARRAY_BUFFER, curr->mObjDataSize * sizeof(struct TRGLColorVertexData), curr->mpDataPtr, GL_DYNAMIC_DRAW);
								curr->mObjDataSize = -curr->mObjDataSize;
							}
							_glRender_ColorPointer(ShaderType_Color, 4, GL_FLOAT, sizeof(struct TRGLColorVertexData), (const void*)8);
							_glRender_VertexPointer(ShaderType_Color, 2, GL_FLOAT, sizeof(struct TRGLColorVertexData), (const void*)0);
							glDrawArrays(GL_TRIANGLE_STRIP, 0, -curr->mObjDataSize);
						break;
						
						case RGL_EXTERNAL_POS_COLOR:
							if(curr->mClipRect[EXDICLIPRECTXY] != NULL_CLIP_RECT)
							{
								GLint scsrx, scsry;
								scsrx = (GLint)(((gsPlane[plane].mOffX + (s16)((curr->mClipRect[EXDICLIPRECTXY] >> 16) & 0xffff)) * gsPlane[plane].mScale) / FX_FX32_TO_F32(FX32_ONE));
								scsry = sgScreenHeight -
									(GLint)(((curr->mClipRect[EXDICLIPRECTWH] & 0xffff) * gsPlane[plane].mScale) / FX_FX32_TO_F32(FX32_ONE)) -
									(GLint)(((gsPlane[plane].mOffY + (s16)(curr->mClipRect[EXDICLIPRECTXY] & 0xffff)) * gsPlane[plane].mScale) / FX_FX32_TO_F32(FX32_ONE));
								glEnable(GL_SCISSOR_TEST);
								glScissor(scsrx, scsry,
									(GLint)((((curr->mClipRect[EXDICLIPRECTWH] >> 16) & 0xffff) * gsPlane[plane].mScale) / FX_FX32_TO_F32(FX32_ONE)),
									(GLint)(((curr->mClipRect[EXDICLIPRECTWH] & 0xffff) * gsPlane[plane].mScale) / FX_FX32_TO_F32(FX32_ONE)));
							}
							_glRender_BindShader(ShaderType_Color, plane);
							glBindBuffer_fn(GL_ARRAY_BUFFER, gsPlane[plane].mpVBOBuffer[curr->mVBOBufferIdx]);
							if(curr->mObjDataSize > 0)
							{
								glBufferData_fn(GL_ARRAY_BUFFER, curr->mObjDataSize * sizeof(struct TRGLColorVertexData), curr->mpDataPtr, GL_DYNAMIC_DRAW);
								curr->mObjDataSize = -curr->mObjDataSize;
							}
							_glRender_ColorPointer(ShaderType_Color, 4, GL_FLOAT, sizeof(struct TRGLColorVertexData), (const void*)8);
							_glRender_VertexPointer(ShaderType_Color, 2, GL_FLOAT, sizeof(struct TRGLColorVertexData), (const void*)0);
							glDrawArrays(GL_TRIANGLES, 0, -curr->mObjDataSize);
							if(curr->mClipRect[EXDICLIPRECTXY] != NULL_CLIP_RECT)
							{
								glDisable(GL_SCISSOR_TEST);
							}
							break;

						default:
							SDK_ASSERT(0);// error sortList member type;

					}
					curr = curr->mpNext;
				}
			}
		}
	}
	
	_glRender_BindShader(ShaderType_None, BGSELECT_NUM);
}
//----------------------------------------------------------------------------------

BOOL glRender_IsGraphicsInit(void)
{
	return gsGraphicsInit;
}
//----------------------------------------------------------------------------------

void glRender_PlaneInit(const struct RenderPlaneInitParams* ipParams)
{
    if(ipParams == NULL)
    {
        SDK_ASSERT(0);
        return;
    }

	SDK_ASSERT(ipParams->mBGType != BGSELECT_NUM);

	gsPlane[ipParams->mBGType].mMaxObjOnScene = (s32)ipParams->mMaxRenderObjectsOnPlane;
	gsPlane[ipParams->mBGType].mpRenderListPool = (struct TRGLRenderItem *)MALLOC(gsPlane[ipParams->mBGType].mMaxObjOnScene * sizeof(struct TRGLRenderItem), "PlaneInit::mpRenderListPool");
	gsPlane[ipParams->mBGType].mpObjDataPool = (u8*)MALLOC(gsPlane[ipParams->mBGType].mMaxObjOnScene * 6 * sizeof(struct TRGLLitVertexData), "PlaneInit::mpObjDataPool"); // 2|/|+2|+2
	gsPlane[ipParams->mBGType].mColor.r = 0.0f;
	gsPlane[ipParams->mBGType].mColor.g = 0.0f;
	gsPlane[ipParams->mBGType].mColor.b = 0.0f;
	gsPlane[ipParams->mBGType].mColor.a = 0.0f;
	gsPlane[ipParams->mBGType].mOffX = ipParams->mX; 
	gsPlane[ipParams->mBGType].mOffY = ipParams->mY;
	gsPlane[ipParams->mBGType].mViewWidth = ipParams->mSizes.mViewWidth;
	gsPlane[ipParams->mBGType].mViewHeight = ipParams->mSizes.mViewHeight;
	SDK_ASSERT(gsPlane[ipParams->mBGType].mpBGTexturesTable == NULL);
	SDK_ASSERT(gsPlane[ipParams->mBGType].mpBGDataPool == NULL);

	_glRender_CalculateOrthoMatrix(ipParams->mBGType);

	glRender_ClearFrameBuffer(ipParams->mBGType);
}
//----------------------------------------------------------------------------------

BOOL glRender_IsRenderPlaneInit(enum BGSelect iType)
{
	SDK_ASSERT(iType < BGSELECT_NUM);
	return gsGraphicsInit && gsPlane[iType].mpObjDataPool != NULL;
}
//----------------------------------------------------------------------------------

void glRender_PlaneResize(enum BGSelect iType, const struct RenderPlaneSizeParams* ipParams)
{
	SDK_ASSERT(iType < BGSELECT_NUM);
	gsPlane[iType].mViewWidth  = ipParams->mViewWidth;
	gsPlane[iType].mViewHeight = ipParams->mViewHeight;
	_glRender_CalculateOrthoMatrix(iType);
}
//----------------------------------------------------------------------------------

void glRender_PlaneRelease(enum BGSelect iType)
{
	SDK_ASSERT(iType < BGSELECT_NUM);
	if(gsPlane[iType].mpObjDataPool != NULL)
	{
		FREE(gsPlane[iType].mpObjDataPool);
		FREE(gsPlane[iType].mpRenderListPool);
		gsPlane[iType].mpObjDataPool = NULL;
		gsPlane[iType].mpRenderListPool = NULL;
	}
}
//----------------------------------------------------------------------------------

void glRender_SetupBGLayersData(enum BGSelect bgType, u32 maxLayers, u32 maxTextures, u32 maxElements)
{
	u32 i;
	SDK_ASSERT(bgType < BGSELECT_NUM);
	SDK_ASSERT(gsGraphicsInit);
	for(i = 0; i < gsPlane[bgType].mBGMaxLayers; i++)
	{
		SDK_ASSERT(gsPlane[bgType].mpBGTexturesTable == NULL); //please call glRender_ReleaseBGLayersData before
	}
	
	maxLayers = gsPlane[bgType].mBGMaxLayers > maxLayers ? gsPlane[bgType].mBGMaxLayers : maxLayers;
	maxTextures = gsPlane[bgType].mBGMaxTextures > maxTextures ? gsPlane[bgType].mBGMaxTextures : maxTextures;
	maxElements = gsPlane[bgType].mBGMaxElements > maxElements ? gsPlane[bgType].mBGMaxElements : maxElements; 

	SDK_ASSERT(gsPlane[bgType].mpBGDataPool == NULL);

	if(maxLayers)
	{
#ifdef USE_TILEMAP_ATTRIBUTES
		i = AllocatorHTable_CalculateHeapSize(gsBGMaxFillColor) * maxLayers * maxTextures +
			StaticAllocator_CalculateHeapSize(sizeof(struct AllocatorHTable)) * maxTextures +
			StaticAllocator_CalculateHeapSize(sizeof(struct AllocatorList)) * maxElements +
			AllocatorList_CalculateHeapSize(sizeof(struct TRGLBGItem), maxElements) * maxLayers;
		gsPlane[bgType].mpBGDataAllocatorHeap = (u8*)MALLOC(i, "SetupBGLayersData:mpBGDataAllocatorHeap");
		StaticAllocator_Init(&gsPlane[bgType].mBGDataAllocator, gsPlane[bgType].mpBGDataAllocatorHeap, i);
#else
		i = AllocatorHTable_CalculateHeapSize(maxTextures) * maxLayers + StaticAllocator_CalculateHeapSize(sizeof(struct TRGLBGItem)) * maxTextures * maxLayers;
		gsPlane[bgType].mpBGDataAllocatorHeap = (u8*)MALLOC(i, "SetupBGLayersData:mpBGDataAllocatorHeap");
		StaticAllocator_Init(&gsPlane[bgType].mBGDataAllocator, gsPlane[bgType].mpBGDataAllocatorHeap, i);
#endif
		
		SDK_ASSERT(gsPlane[bgType].mpBGVBOBuffer == NULL); //please call glRender_ReleaseBGLayersData before
#ifdef USE_TILEMAP_ATTRIBUTES
		gsPlane[bgType].mpBGVBOBuffer = (GLuint*)MALLOC(maxLayers * maxElements * maxTextures * sizeof(GLuint), "SetupBGLayersData:mpBGVBOBuffer");
		glGenBuffers_fn(maxLayers * maxElements * maxTextures, gsPlane[bgType].mpBGVBOBuffer);
#else
		gsPlane[bgType].mpBGVBOBuffer = (GLuint*)MALLOC(maxLayers * maxTextures * sizeof(GLuint), "SetupBGLayersData:mpBGVBOBuffer");
		glGenBuffers_fn(maxLayers * maxTextures, gsPlane[bgType].mpBGVBOBuffer);
#endif

#ifdef USE_TILEMAP_ATTRIBUTES
		sgBGChunkSize = mthSqrtI(maxElements) / 6;
		if (sgBGChunkSize < 16)
		{
			sgBGChunkSize = 16;
		}
		i = maxLayers * (6 * maxElements) * sgBGChunkSize * sizeof(struct TRGLLitVertexData);
		gsPlane[bgType].mpBGVertexCt = (s32*)MALLOC(maxLayers * sizeof(s32), "SetupBGLayersData::mpBGVertexCt");
#else
		i = maxLayers * (6 * maxElements) * maxTextures * sizeof(struct TRGLUnlitVertexData);
#endif
		gsPlane[bgType].mpBGDataPool = (float*)MALLOC(i, "SetupBGLayersData:mpBGDataPool");
		gsPlane[bgType].mpBGTexturesTable = (struct AllocatorHTable*)MALLOC(sizeof(struct AllocatorHTable) * maxLayers, "SetupBGLayersData:mpBGTexturesTable");
	}

	for (i = 0; i < maxLayers; i++)
	{
		AllocatorHTable_Init(&gsPlane[bgType].mpBGTexturesTable[i], maxTextures, &gsPlane[bgType].mBGDataAllocator);
#ifdef USE_TILEMAP_ATTRIBUTES
		gsPlane[bgType].mpBGVertexCt[i] = 0;
#endif
	}
	gsPlane[bgType].mBGMaxLayers = maxLayers;
	gsPlane[bgType].mBGMaxTextures = maxTextures;
	gsPlane[bgType].mBGMaxElements = maxElements;
}
//----------------------------------------------------------------------------------

void glRender_ReleaseBGLayersData(enum BGSelect bgType)
{
	SDK_ASSERT(bgType < BGSELECT_NUM);
	if (gsPlane[bgType].mBGMaxLayers > 0)
	{
		FREE(gsPlane[bgType].mpBGTexturesTable);
		FREE(gsPlane[bgType].mpBGDataPool);
#ifdef USE_TILEMAP_ATTRIBUTES
		FREE(gsPlane[bgType].mpBGVertexCt);
#endif
		gsPlane[bgType].mpBGTexturesTable = NULL;
		gsPlane[bgType].mpBGDataPool = NULL;
#ifdef USE_TILEMAP_ATTRIBUTES
		glDeleteBuffers_fn(gsPlane[bgType].mBGMaxLayers * gsPlane[bgType].mBGMaxTextures * gsPlane[bgType].mBGMaxElements, gsPlane[bgType].mpBGVBOBuffer);
#else
		glDeleteBuffers_fn(gsPlane[bgType].mBGMaxLayers * gsPlane[bgType].mBGMaxTextures, gsPlane[bgType].mpBGVBOBuffer);
#endif
		FREE(gsPlane[bgType].mpBGVBOBuffer);
		gsPlane[bgType].mpBGVBOBuffer = NULL;

		FREE(gsPlane[bgType].mpBGDataAllocatorHeap);
		gsPlane[bgType].mpBGDataAllocatorHeap = NULL;
		StaticAllocator_Release(&gsPlane[bgType].mBGDataAllocator);
	}
	gsPlane[bgType].mBGMaxLayers = 0;
	gsPlane[bgType].mBGMaxTextures = 0;
	gsPlane[bgType].mBGMaxElements = 0;
}
//----------------------------------------------------------------------------------

void glRender_SetActiveBGForGraphics(enum BGSelect iActiveBG)
{
   if(gsActivePlane != iActiveBG)
   {
		gsActivePlane = iActiveBG;
		SDK_ASSERT(gsActivePlane != BGSELECT_NUM);
   }
}
//----------------------------------------------------------------------------------

enum BGSelect glRender_GetActiveBGForGraphics(void)
{
	return gsActivePlane; 
}
//----------------------------------------------------------------------------------

void glRender_CreateTextures(s32 texturesCount)
{	
	s32 i;
	mCurrentTextureID = -1;
	mShaderType = ShaderType_MAX;
	_glRender_LoadShader(ShaderType_Lit, "lit.vsh", "lit.fsh");
	_glRender_LoadShader(ShaderType_Unlit, "unlit.vsh", "unlit.fsh");
	_glRender_LoadShader(ShaderType_Color, "color.vsh", "color.fsh");	
	if(texturesCount > 0)
	{
		mTextureCount = (GLuint)texturesCount;
		SDK_ASSERT(mpTexture == NULL);
		mpTexture = (GLuint*)MALLOC(sizeof(GLuint) * mTextureCount, "glRender_CreateTextures:mpTexture");
		glGenTextures(mTextureCount, mpTexture);
		_glRender_CheckError("glGenTextures");
#ifdef SDK_DEBUG
		OS_Printf("glRender_CreateTextures = %d\n", mTextureCount);
#endif
	}
	for(i = 0; i < BGSELECT_NUM; i++)
	{
		if(gsPlane[i].mpObjDataPool != NULL)
		{
			SDK_ASSERT(gsPlane[i].mpVBOBuffer == NULL);
			gsPlane[i].mpVBOBuffer = (GLuint*)MALLOC(gsPlane[i].mMaxObjOnScene * sizeof(GLuint), "glRender_CreateTextures:mpVBOBuffer");
			glGenBuffers_fn(gsPlane[i].mMaxObjOnScene, gsPlane[i].mpVBOBuffer);
			_glRender_CheckError("glGenBuffers");
		}
	}
	sgRenderListReady = TRUE;
}
//----------------------------------------------------------------------------------

void glRender_DeleteTextures()
{
	s32 i = BGSELECT_NUM;
	sgRenderListReady = FALSE;
	while(i > 0)
	{
		i--;
		if(gsPlane[i].mpVBOBuffer != NULL)
		{
//#if defined ANDROID_NDK
			// do nothing
//#else
			glBindBuffer_fn(GL_ELEMENT_ARRAY_BUFFER, 0);
			glBindBuffer_fn(GL_ARRAY_BUFFER, 0);
			glDeleteBuffers_fn(gsPlane[i].mMaxObjOnScene, gsPlane[i].mpVBOBuffer);
//#endif
			FREE(gsPlane[i].mpVBOBuffer);
			gsPlane[i].mpVBOBuffer = NULL;
		}
	}
	if(mTextureCount > 0)
	{
//#if defined ANDROID_NDK
        // do nothing
//#else
		SDK_NULL_ASSERT(mpTexture);
		glDeleteTextures(mTextureCount, mpTexture);
		_glRender_CheckError("glDeleteTextures");
//#endif
		FREE(mpTexture);
		mTextureCount = 0;
		mpTexture = NULL;
	}
//#if defined ANDROID_NDK
	// do nothing
//#else
	i = ShaderType_MAX;
	while(i > 0)
	{
		i--;
		if(gsShader[i].handle > 0)
		{
			glDeleteProgram_fn(gsShader[i].handle);
			_glRender_CheckError("glDeleteProgram");
			gsShader[i].handle = 0;
		}
	}
//#endif
}
//----------------------------------------------------------------------------------

s32 glRender_GetViewWidth(enum BGSelect iBGType)
{
	SDK_ASSERT(iBGType < BGSELECT_NUM);
	return gsPlane[iBGType].mViewWidth;
}
//----------------------------------------------------------------------------------

s32 glRender_GetViewHeight(enum BGSelect iBGType)
{
	SDK_ASSERT(iBGType < BGSELECT_NUM);
	return gsPlane[iBGType].mViewHeight;
}
//----------------------------------------------------------------------------------

s32 glRender_GetFrameBufferWidth(enum BGSelect iBGType)
{
	SDK_ASSERT(iBGType < BGSELECT_NUM);
	return gsPlane[iBGType].mViewWidth;
}
//----------------------------------------------------------------------------------

s32 glRender_GetViewLeft(enum BGSelect iBGType)
{
	SDK_ASSERT(iBGType < BGSELECT_NUM);
	return gsPlane[iBGType].mOffX;	
}
//----------------------------------------------------------------------------------

s32 glRender_GetViewTop(enum BGSelect iBGType)
{
	SDK_ASSERT(iBGType < BGSELECT_NUM);
	return gsPlane[iBGType].mOffY;
}
//----------------------------------------------------------------------------------

s32 glRender_GetFrameBufferHeight(enum BGSelect iBGType)
{
	SDK_ASSERT(iBGType < BGSELECT_NUM);
	return gsPlane[iBGType].mViewHeight;
}
//----------------------------------------------------------------------------------

void glRender_LoadTextureToVRAM(struct BMPImage *pImage, s32 resId)
{
	u8* buf8888;
	SDK_NULL_ASSERT(pImage);
	SDK_ASSERT(pImage->mType == BMP_TYPE_DC16 || pImage->mType == BMP_TYPE_DC32);

	mCurrentTextureID = -1;

	if(pImage->mType == BMP_TYPE_DC32)
	{
		buf8888 = pImage->data.mpData256;
	}
	else
#if !(defined WINDOWS_APP || defined NIX_APP || defined EMSCRIPTEN_APP || defined KOLIBRIOS_APP)
	if(pImage->mA5DataSize > 0)
#endif
	{
		s32 i, j;
		u16 d5551;
		u8 *buf;
		buf8888 = (u8*)MALLOC(pImage->mWidth2n * pImage->mHeight2n * 4, "glRender:buf8888");
		for(i = 0; i < pImage->mHeight; i++)
		{
			buf = buf8888 + pImage->mWidth2n * i * 4;
			for(j = 0; j < pImage->mWidth; j++)
			{				
				d5551 = *(pImage->data.mpDataDC16 + i * pImage->mWidth2n + j);
				*(buf++) = (u8)(((((d5551 & GX_RGBA_R_MASK) >> GX_RGBA_R_SHIFT)) * 0xff) / 0x1f);
				*(buf++) = (u8)(((((d5551 & GX_RGBA_G_MASK) >> GX_RGBA_G_SHIFT)) * 0xff) / 0x1f);
				*(buf++) = (u8)(((((d5551 & GX_RGBA_B_MASK) >> GX_RGBA_B_SHIFT)) * 0xff) / 0x1f);
				*(buf++) = (u8)(((d5551 & GX_RGBA_A_MASK) >> GX_RGBA_A_SHIFT) * 0xff); 
			}
		}
		if(pImage->mA5DataSize > 0)
		{
			s32 k, h;
			buf = buf8888;
			h = k = 0;
			i = pImage->mHeight * pImage->mWidth;
			while(i > 0)
			{	
				d5551 = *(pImage->mpA5Data + k);				
				k++;				
				buf += 3;
				*(buf++) = (u8)(((((d5551 & 0xf800) >> 11)) * 0xff) / 0x1f);
				if(--i < 0)
				{
					break;
				}
				if(i % pImage->mWidth == 0)
				{
					h++;
					buf = buf8888 + pImage->mWidth2n * h * 4;
					if(buf8888 + pImage->mWidth2n * pImage->mHeight2n * 4 <= buf)
					{
						break;	
					}
				}
				buf += 3;
				*(buf++) = (u8)(((((d5551 & 0x07c0) >> 6)) * 0xff) / 0x1f);
				if(--i < 0)
				{
					break;
				}
				if(i % pImage->mWidth == 0)
				{
					h++;
					buf = buf8888 + pImage->mWidth2n * h * 4;
					if(buf8888 + pImage->mWidth2n * pImage->mHeight2n * 4 <= buf)
					{
						break;	
					}
				}
				buf += 3;
				*(buf++) = (u8)(((((d5551 & 0x003e) >> 1)) * 0xff) / 0x1f);
				if(--i < 0)
				{
					break;
				}
				if(i % pImage->mWidth == 0)
				{
					h++;
					buf = buf8888 + pImage->mWidth2n * h * 4;
					if(buf8888 + pImage->mWidth2n * pImage->mHeight2n * 4 <= buf)
					{
						break;	
					}
				}
			}
		}
	}
#if !(defined WINDOWS_APP || defined NIX_APP || defined EMSCRIPTEN_APP || defined KOLIBRIOS_APP)
	else
	{
		buf8888 = NULL;
	}
#endif

#ifdef JOBS_IN_SEPARATE_THREAD
	jobCriticalSectionBegin();	
#endif
	SDK_NULL_ASSERT(mpTexture);
	glBindTexture(GL_TEXTURE_2D, mpTexture[resId]);
	_glRender_CheckError("glBindTexture");
	if(buf8888 != NULL)
	{
		glTexSubImage2D(GL_TEXTURE_2D,
						0,
						0,
						0,
						pImage->mWidth2n,
						pImage->mHeight2n,
						GL_RGBA,
						GL_UNSIGNED_BYTE,
						buf8888);
	}
#if !(defined WINDOWS_APP || defined NIX_APP || defined EMSCRIPTEN_APP || defined KOLIBRIOS_APP)
	else
	{
		glTexSubImage2D(GL_TEXTURE_2D,
						0,
						0,
						0,
						pImage->mWidth2n,
						pImage->mHeight2n,
						GL_RGBA,
						GL_UNSIGNED_SHORT_5_5_5_1,
						pImage->data.mpDataDC16);
	}
#endif
	_glRender_CheckError("glTexSubImage2D");
#ifdef JOBS_IN_SEPARATE_THREAD
	jobCriticalSectionEnd();	
#endif
	if(buf8888 != NULL && pImage->mType == BMP_TYPE_DC16)
	{
		FREE(buf8888);
	}
}
//----------------------------------------------------------------------------------

void glRender_CreateEmptyTexture(const struct BMPImage *iImgHeader, s32 resId)
{
	SDK_NULL_ASSERT(iImgHeader);
	SDK_ASSERT(iImgHeader->mType == BMP_TYPE_DC16 || iImgHeader->mType == BMP_TYPE_DC32);
	mCurrentTextureID = -1;
	SDK_NULL_ASSERT(mpTexture);
	glBindTexture(GL_TEXTURE_2D, mpTexture[resId]);
	_glRender_CheckError("glBindTexture");
	if(iImgHeader->mFilterType == BMP_FILTER_NEAREST)
	{
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	}
	else
	{
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	}
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	_glRender_CheckError("glTexParameteri");
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	_glRender_CheckError("glTexParameteri");
#if !(defined WINDOWS_APP || defined NIX_APP || defined EMSCRIPTEN_APP || defined KOLIBRIOS_APP)
	if((iImgHeader->mA5DataSize > 0 && iImgHeader->mType == BMP_TYPE_DC16) || iImgHeader->mType == BMP_TYPE_DC32)
#endif
	{
		glTexImage2D(GL_TEXTURE_2D,
						0,
						GL_RGBA,
						iImgHeader->mWidth2n,
						iImgHeader->mHeight2n,
						0,
						GL_RGBA,
						GL_UNSIGNED_BYTE,
						NULL);
	}
#if !(defined WINDOWS_APP || defined NIX_APP || defined EMSCRIPTEN_APP || defined KOLIBRIOS_APP)
	else
	{	
		glTexImage2D(GL_TEXTURE_2D,
						0,
						GL_RGBA,
						iImgHeader->mWidth2n,
						iImgHeader->mHeight2n,
						0,
						GL_RGBA,
						GL_UNSIGNED_SHORT_5_5_5_1,
						NULL);
	}
#endif
}
//----------------------------------------------------------------------------------

void glRender_ClearFrameBuffer(enum BGSelect iType)
{	
	if(iType < BGSELECT_NUM)
	{
		u32 i;
		StaticAllocator_Reset(&gsPlane[gsActivePlane].mBGDataAllocator);
		gsPlane[iType].mRenderListSize = -1;
		for(i = 0; i < gsPlane[iType].mBGMaxLayers; i++)
		{
			AllocatorHTable_Init(&gsPlane[iType].mpBGTexturesTable[i], gsPlane[iType].mBGMaxTextures, &gsPlane[iType].mBGDataAllocator);
#ifdef USE_TILEMAP_ATTRIBUTES
			//gsPlane[iType].mpBGColorCt[i] = 0;
			gsPlane[iType].mpBGVertexCt[i] = 0;
#endif
		}
		gsPlane[iType].mpRenderList = NULL;
		gsPlane[iType].mTotalObjDataSize = 0;
		gsPlane[iType].mDraw = 0;
		gsPlane[iType].mObjVBOBufferLastIdx = 0;
		gsPlane[iType].mBGVBOBufferLastIdx = 0;
	}
}
//----------------------------------------------------------------------------------

void glRender_FreezeCurrentFrameBuffer(BOOL val)
{
	gsFreezeCurrentFrameBuffer = val;
}
//----------------------------------------------------------------------------------

BOOL _glRender_CheckForCleanup(enum BGSelect iType)
{
#ifdef SDK_DEBUG
	SDK_ASSERT(cd.trig_visible_mask == 0);
#endif
	if(gsFreezeCurrentFrameBuffer == TRUE)
	{
		if(gsPlane[iType].mDraw > 0)
		{
			gsPlane[iType].mDraw = 1;
		}
		return FALSE;
	}
	if(gsPlane[iType].mDraw > 1)
	{
		glRender_ClearFrameBuffer(iType);
	}
	return TRUE;
}
//----------------------------------------------------------------------------------

void _glRender_AddToRenderList(struct TRGLRenderItem* item)
{
	SDK_ASSERT(gsActivePlane != BGSELECT_NUM);
	switch(item->mType)
	{
		case RGL_PIXEL:
		case RGL_LINE:
		case RGL_FILLRECT:
		case RGL_IMAGE:
		case RGL_EXTERNAL_POS_COLOR:
			if(gsPlane[gsActivePlane].mpRenderList == NULL)
			{
				item->mpNext = NULL;
				gsPlane[gsActivePlane].mpRenderListTail = gsPlane[gsActivePlane].mpRenderList = item;
			}
			else
			{
				item->mpNext = NULL;
				gsPlane[gsActivePlane].mpRenderListTail->mpNext = item;
				gsPlane[gsActivePlane].mpRenderListTail = item;
			}
		break;
		default:
			SDK_ASSERT(0);
	}
}
//----------------------------------------------------------------------------------------------------------------

void glRender_SetColor(GXRgba iColor)
{
	SDK_ASSERT(gsActivePlane != BGSELECT_NUM);
	if(_glRender_CheckForCleanup(gsActivePlane) == FALSE)
	{
		return;
	}
	gsPlane[gsActivePlane].mColor.r = GX_COLOR_CHANNEL((iColor & GX_RGBA_R_MASK) >> GX_RGBA_R_SHIFT);
	gsPlane[gsActivePlane].mColor.g = GX_COLOR_CHANNEL((iColor & GX_RGBA_G_MASK) >> GX_RGBA_G_SHIFT);
	gsPlane[gsActivePlane].mColor.b = GX_COLOR_CHANNEL((iColor & GX_RGBA_B_MASK) >> GX_RGBA_B_SHIFT);
	gsPlane[gsActivePlane].mColor.a = GX_COLOR_CHANNEL(0x1f * (iColor & GX_RGBA_A_MASK));
	gsPlane[gsActivePlane].mColor.rgba = iColor;
}
//----------------------------------------------------------------------------------------------------------------

void glRender_DrawLine(fx32 iX0, fx32 iY0, fx32 iX1, fx32 iY1)
{
	BOOL newItem;
	struct TRGLRenderItem *item;
	struct TRGLColorVertexData *e;
	
	SDK_ASSERT(gsActivePlane != BGSELECT_NUM);
	if(!gxHelper_preDrawLine(FX32(gsPlane[gsActivePlane].mViewWidth), FX32(gsPlane[gsActivePlane].mViewHeight), &iX0, &iY0, &iX1, &iY1))
	{
		return;
	}
	
	if(_glRender_CheckForCleanup(gsActivePlane) == FALSE)
	{
		return;
	}

	newItem = FALSE;
	if(!(gsPlane[gsActivePlane].mRenderListSize >= 0 && 
		gsPlane[gsActivePlane].mpRenderListPool[gsPlane[gsActivePlane].mRenderListSize].mType == RGL_LINE &&
		gsPlane[gsActivePlane].mpRenderListPool[gsPlane[gsActivePlane].mRenderListSize].mFillWithColor == (u32)gsPlane[gsActivePlane].mColor.rgba))
	{
		gsPlane[gsActivePlane].mRenderListSize++;
		newItem = TRUE;
	}

	SDK_ASSERT(gsPlane[gsActivePlane].mRenderListSize <= gsPlane[gsActivePlane].mMaxObjOnScene);
	if(gsPlane[gsActivePlane].mRenderListSize > gsPlane[gsActivePlane].mMaxObjOnScene)
	{
		OS_Warning("glRender: RenderListSize overflow\n");
		SDK_ASSERT(0);
		gsPlane[gsActivePlane].mRenderListSize = gsPlane[gsActivePlane].mMaxObjOnScene - 1;
		return;
	}

	SDK_ASSERT(gsPlane[gsActivePlane].mTotalObjDataSize + 2 <= gsPlane[gsActivePlane].mMaxObjOnScene * 6);
	if(gsPlane[gsActivePlane].mTotalObjDataSize + 2 > gsPlane[gsActivePlane].mMaxObjOnScene * 6)
	{
		OS_Warning("glRender: RenderList vertex pool overflow\n");
		SDK_ASSERT(0);
		return;
	}

	gsPlane[gsActivePlane].mDraw = 1;
	item = &gsPlane[gsActivePlane].mpRenderListPool[gsPlane[gsActivePlane].mRenderListSize];
	if(newItem)
	{
		item->mpImageHeader = NULL;
		item->mpDataPtr = &gsPlane[gsActivePlane].mpObjDataPool[gsPlane[gsActivePlane].mTotalObjDataSize * sizeof(struct TRGLLitVertexData)];
		item->mObjDataSize = 0;
		item->mVBOBufferIdx = gsPlane[gsActivePlane].mObjVBOBufferLastIdx;
		gsPlane[gsActivePlane].mObjVBOBufferLastIdx++;
		item->mType = RGL_LINE;
		item->mFillWithColor = (u32)gsPlane[gsActivePlane].mColor.rgba;
		_glRender_AddToRenderList(item);
	}
	e = (struct TRGLColorVertexData*)item->mpDataPtr;
	e[0 + item->mObjDataSize].pos[0] = FX_FX32_TO_F32(iX0);
	e[0 + item->mObjDataSize].pos[1] = FX_FX32_TO_F32(iY0);
	e[1 + item->mObjDataSize].pos[0] = FX_FX32_TO_F32(iX1);
	e[1 + item->mObjDataSize].pos[1] = FX_FX32_TO_F32(iY1);
	e[0 + item->mObjDataSize].color[0] = e[1 + item->mObjDataSize].color[0] = gsPlane[gsActivePlane].mColor.r;
	e[0 + item->mObjDataSize].color[1] = e[1 + item->mObjDataSize].color[1] = gsPlane[gsActivePlane].mColor.g;
	e[0 + item->mObjDataSize].color[2] = e[1 + item->mObjDataSize].color[2] = gsPlane[gsActivePlane].mColor.b;
	e[0 + item->mObjDataSize].color[3] = e[1 + item->mObjDataSize].color[3] = gsPlane[gsActivePlane].mColor.a;
	gsPlane[gsActivePlane].mTotalObjDataSize += 2;
	item->mObjDataSize += 2;
}
//----------------------------------------------------------------------------------------------------------------

void glRender_ColorRect(fx32 iX, fx32 iY, fx32 iWidth, fx32 iHeight)
{
	BOOL newItem;
	struct TRGLRenderItem *item;
	struct TRGLColorVertexData *e;

	SDK_ASSERT(gsActivePlane != BGSELECT_NUM);
	if(!gxHelper_preDrawSquare(FX32(gsPlane[gsActivePlane].mViewWidth), FX32(gsPlane[gsActivePlane].mViewHeight), &iX, &iY, &iWidth, &iHeight))
	{
		return;
	}

	if(_glRender_CheckForCleanup(gsActivePlane) == FALSE)
	{
		return;
	}

	newItem = FALSE;
	if(!(gsPlane[gsActivePlane].mRenderListSize >= 0 && 
		gsPlane[gsActivePlane].mpRenderListPool[gsPlane[gsActivePlane].mRenderListSize].mType == RGL_FILLRECT &&
		gsPlane[gsActivePlane].mpRenderListPool[gsPlane[gsActivePlane].mRenderListSize].mFillWithColor == (u32)gsPlane[gsActivePlane].mColor.rgba))
	{
		gsPlane[gsActivePlane].mRenderListSize++;
		newItem = TRUE;
	}

	SDK_ASSERT(gsPlane[gsActivePlane].mRenderListSize <= gsPlane[gsActivePlane].mMaxObjOnScene);
	if(gsPlane[gsActivePlane].mRenderListSize > gsPlane[gsActivePlane].mMaxObjOnScene)
	{
		OS_Warning("glRender: RenderListSize overflow\n");
		SDK_ASSERT(0);
		gsPlane[gsActivePlane].mRenderListSize = gsPlane[gsActivePlane].mMaxObjOnScene - 1;
		return;
	}

	//SDK_ASSERT(gsPlane[gsActivePlane].mTotalObjDataSize + 6 <= gsPlane[gsActivePlane].mMaxObjOnScene * 6);
	if(gsPlane[gsActivePlane].mTotalObjDataSize + 6 > gsPlane[gsActivePlane].mMaxObjOnScene * 6)
	{
		//OS_Warning("glRender: RenderList vertex pool overflow\n");
		OS_Warning("glRender: RenderList vertex pool overflow %d %d\n", gsPlane[gsActivePlane].mTotalObjDataSize + 6, gsPlane[gsActivePlane].mMaxObjOnScene * 6);
		SDK_ASSERT(0);
		return;
	}

	gsPlane[gsActivePlane].mDraw = 1;
	item = &gsPlane[gsActivePlane].mpRenderListPool[gsPlane[gsActivePlane].mRenderListSize];

	if(newItem)
	{
		item->mpImageHeader = NULL;
		item->mpDataPtr = &gsPlane[gsActivePlane].mpObjDataPool[gsPlane[gsActivePlane].mTotalObjDataSize * sizeof(struct TRGLLitVertexData)];
		item->mObjDataSize = 0;
		item->mVBOBufferIdx = gsPlane[gsActivePlane].mObjVBOBufferLastIdx;
		gsPlane[gsActivePlane].mObjVBOBufferLastIdx++;
		item->mType = RGL_FILLRECT;
		item->mFillWithColor = (u32)gsPlane[gsActivePlane].mColor.rgba;
		_glRender_AddToRenderList(item);
	}

	e = (struct TRGLColorVertexData*)item->mpDataPtr;		
	if(item->mObjDataSize > 0)
	{	
		e[item->mObjDataSize] = e[item->mObjDataSize - 1];
		item->mObjDataSize++;
		e[item->mObjDataSize].pos[0] = FX_FX32_TO_F32(iX);
		e[item->mObjDataSize].pos[1] = FX_FX32_TO_F32(iY);
		e[item->mObjDataSize].color[0] = e[item->mObjDataSize].color[1] =
		e[item->mObjDataSize].color[2] = e[item->mObjDataSize].color[3] = 0;
		gsPlane[gsActivePlane].mTotalObjDataSize += 2;
		item->mObjDataSize++;
	}
	e[0 + item->mObjDataSize].pos[0] = e[1 + item->mObjDataSize].pos[0] = FX_FX32_TO_F32(iX);
	e[1 + item->mObjDataSize].pos[1] = e[3 + item->mObjDataSize].pos[1] = FX_FX32_TO_F32(iY + iHeight);
	e[2 + item->mObjDataSize].pos[0] = e[3 + item->mObjDataSize].pos[0] = FX_FX32_TO_F32(iX + iWidth);
	e[0 + item->mObjDataSize].pos[1] = e[2 + item->mObjDataSize].pos[1] = FX_FX32_TO_F32(iY);
	e[0 + item->mObjDataSize].color[0] = e[1 + item->mObjDataSize].color[0] =
	e[2 + item->mObjDataSize].color[0] = e[3 + item->mObjDataSize].color[0] = gsPlane[gsActivePlane].mColor.r;
	e[0 + item->mObjDataSize].color[1] = e[1 + item->mObjDataSize].color[1] =
	e[2 + item->mObjDataSize].color[1] = e[3 + item->mObjDataSize].color[1] = gsPlane[gsActivePlane].mColor.g;
	e[0 + item->mObjDataSize].color[2] = e[1 + item->mObjDataSize].color[2] =
	e[2 + item->mObjDataSize].color[2] = e[3 + item->mObjDataSize].color[2] = gsPlane[gsActivePlane].mColor.b;
	e[0 + item->mObjDataSize].color[3] = e[1 + item->mObjDataSize].color[3] =
	e[2 + item->mObjDataSize].color[3] = e[3 + item->mObjDataSize].color[3] = gsPlane[gsActivePlane].mColor.a;
	gsPlane[gsActivePlane].mTotalObjDataSize += 4;
	item->mObjDataSize += 4;
}
//----------------------------------------------------------------------------------------------------------------

#ifdef USE_EXTERNAL_API
void glRender_ExternalSetActiveClipRect(const struct ExDRect* data)
{
	SDK_ASSERT(gsActivePlane != BGSELECT_NUM);
	if(data == NULL)
	{
		gsActiveClipRect[EXDICLIPRECTXY] = NULL_CLIP_RECT;
		gsActiveClipRect[EXDICLIPRECTWH] = NULL_CLIP_RECT;
	}
	else
	{
		gsActiveClipRect[EXDICLIPRECTXY] = ((u16)(data->x) << 16) | (u16)data->y;
		gsActiveClipRect[EXDICLIPRECTWH] = ((u16)(data->w) << 16) | (u16)data->h;
	}
}
//----------------------------------------------------------------------------------------------------------------

void glRender_ExternalPosColorData(const struct TRGLColorVertexData* data)
{
	BOOL newItem;
	struct TRGLRenderItem *item;
	struct TRGLColorVertexData *e;

	SDK_NULL_ASSERT(data);
	SDK_ASSERT(gsActivePlane != BGSELECT_NUM);

	if(_glRender_CheckForCleanup(gsActivePlane) == FALSE)
	{
		return;
	}

	newItem = FALSE;
	if(!(gsPlane[gsActivePlane].mRenderListSize >= 0 &&
		 gsPlane[gsActivePlane].mpRenderListPool[gsPlane[gsActivePlane].mRenderListSize].mType == RGL_EXTERNAL_POS_COLOR &&
		 gsPlane[gsActivePlane].mpRenderListPool[gsPlane[gsActivePlane].mRenderListSize].mClipRect[EXDICLIPRECTXY] == gsActiveClipRect[EXDICLIPRECTXY] &&
		 gsPlane[gsActivePlane].mpRenderListPool[gsPlane[gsActivePlane].mRenderListSize].mClipRect[EXDICLIPRECTWH] == gsActiveClipRect[EXDICLIPRECTWH]))
	{
		gsPlane[gsActivePlane].mRenderListSize++;
		newItem = TRUE;
	}

	SDK_ASSERT(gsPlane[gsActivePlane].mRenderListSize <= gsPlane[gsActivePlane].mMaxObjOnScene);
	if(gsPlane[gsActivePlane].mRenderListSize > gsPlane[gsActivePlane].mMaxObjOnScene)
	{
		OS_Warning("glRender: RenderListSize overflow\n");
		SDK_ASSERT(0);
		gsPlane[gsActivePlane].mRenderListSize = gsPlane[gsActivePlane].mMaxObjOnScene - 1;
		return;
	}

	SDK_ASSERT(gsPlane[gsActivePlane].mTotalObjDataSize + 1 <= gsPlane[gsActivePlane].mMaxObjOnScene * 6);
	if(gsPlane[gsActivePlane].mTotalObjDataSize + 1 > gsPlane[gsActivePlane].mMaxObjOnScene * 6)
	{
		OS_Warning("glRender: RenderList vertex pool overflow\n");
		SDK_ASSERT(0);
		return;
	}

	gsPlane[gsActivePlane].mDraw = 1;
	item = &gsPlane[gsActivePlane].mpRenderListPool[gsPlane[gsActivePlane].mRenderListSize];
	if(newItem)
	{
		item->mpImageHeader = NULL;
		item->mClipRect[EXDICLIPRECTXY] = gsActiveClipRect[EXDICLIPRECTXY];
		item->mClipRect[EXDICLIPRECTWH] = gsActiveClipRect[EXDICLIPRECTWH];
		item->mpDataPtr = &gsPlane[gsActivePlane].mpObjDataPool[gsPlane[gsActivePlane].mTotalObjDataSize * sizeof(struct TRGLLitVertexData)];
		item->mObjDataSize = 0;
		item->mVBOBufferIdx = gsPlane[gsActivePlane].mObjVBOBufferLastIdx;
		gsPlane[gsActivePlane].mObjVBOBufferLastIdx++;
		item->mType = RGL_EXTERNAL_POS_COLOR;
		_glRender_AddToRenderList(item);
	}
	e = (struct TRGLColorVertexData*)item->mpDataPtr;
	MI_CpuCopy8(&data->color[0], &e[item->mObjDataSize].color[0], sizeof(float) * 4);
	MI_CpuCopy8(&data->pos[0], &e[item->mObjDataSize].pos[0], sizeof(float) * 2);
	gsPlane[gsActivePlane].mTotalObjDataSize += 1;
	item->mObjDataSize += 1;
}
//----------------------------------------------------------------------------------------------------------------
#endif

static void _glRender_SetColorColorArrayItem16(const void* dataPtr, const u32 fillColor, const u32 offset)
{
	float cr, cg, cb, ca;
	struct TRGLLitVertexData* e = (struct TRGLLitVertexData*)dataPtr;
	GXRgba ocolor = fillColor & 0xFFFF;
	cr = GX_COLOR_CHANNEL((ocolor & GX_RGBA_R_MASK) >> GX_RGBA_R_SHIFT);
	cg = GX_COLOR_CHANNEL((ocolor & GX_RGBA_G_MASK) >> GX_RGBA_G_SHIFT);
	cb = GX_COLOR_CHANNEL((ocolor & GX_RGBA_B_MASK) >> GX_RGBA_B_SHIFT);
	ca = GX_COLOR_CHANNEL(fillColor >> 16);
	e[0 + offset].color[0] = e[1 + offset].color[0] = e[2 + offset].color[0] = e[3 + offset].color[0] = cr;
	e[0 + offset].color[1] = e[1 + offset].color[1] = e[2 + offset].color[1] = e[3 + offset].color[1] = cg;
	e[0 + offset].color[2] = e[1 + offset].color[2] = e[2 + offset].color[2] = e[3 + offset].color[2] = cb;
	e[0 + offset].color[3] = e[1 + offset].color[3] = e[2 + offset].color[3] = e[3 + offset].color[3] = ca;
}
//----------------------------------------------------------------------------------------------------------------

static void _glRender_SetColorColorArrayItem8(const void* dataPtr, const u32 fillColor, const u32 offset)
{
	float cr, cg, cb, ca;
	struct TRGLLitVertexData* e = (struct TRGLLitVertexData*)dataPtr;
	GXRgba ocolor = fillColor & 0xFFFF;
	cr = GX_COLOR_CHANNEL((ocolor & GX_RGBA_R_MASK) >> GX_RGBA_R_SHIFT);
	cg = GX_COLOR_CHANNEL((ocolor & GX_RGBA_G_MASK) >> GX_RGBA_G_SHIFT);
	cb = GX_COLOR_CHANNEL((ocolor & GX_RGBA_B_MASK) >> GX_RGBA_B_SHIFT);
	ca = GX_COLOR_CHANNEL(fillColor >> 16);
	e[0 + offset].color[0] = e[1 + offset].color[0] = cr;
	e[0 + offset].color[1] = e[1 + offset].color[1] = cg;
	e[0 + offset].color[2] = e[1 + offset].color[2] = cb;
	e[0 + offset].color[3] = e[1 + offset].color[3] = ca;
}
//----------------------------------------------------------------------------------------------------------------

void glRender_DrawImage(const struct DrawImageFunctionData* data)
{
	SDK_ASSERT(gsActivePlane != BGSELECT_NUM);
	switch(data->mType)
	{
		case DIT_TileBG1:
		case DIT_TileBG2:
		case DIT_TileBG3:
		case DIT_TileBG4:
		case DIT_TileBG5:
		{
			if(gsPlane[gsActivePlane].mBGMaxLayers > 0)
			{
				struct TRGLBGItem* item;
				struct AllocatorHTable* txtTable;
				struct StaticAllocator* allocator;
#ifdef USE_TILEMAP_ATTRIBUTES
				struct AllocatorHTable* opacityTable;
				struct AllocatorList* itemList;
				s32 addChunk;
#endif
				float *fdata;
				fx32 yb, xl;
				s32 txtr_idx;
#ifdef USE_TILEMAP_ATTRIBUTES
				const u32 lit_item_size = sizeof(struct TRGLLitVertexData) / sizeof(float);
#ifdef DEBUG_BG_CHUNKS
				const u32 item_size = lit_item_size;
#else
				const u32 item_size = (data->mFillColor == DIFD_DEF_FILL_COLOR_FULL_ALPHA ? sizeof(struct TRGLUnlitVertexData) : sizeof(struct TRGLLitVertexData)) / sizeof(float);
#endif
				const u32 chunkVertexSize = sgBGChunkSize * 6;

				if (GX_COLOR_CHANNEL(data->mFillColor >> 16) == 0)
				{
					return;
				}
#else
				const u32 item_size = sizeof(struct TRGLUnlitVertexData) / sizeof(float);
#endif
				if(_glRender_CheckForCleanup(gsActivePlane) == FALSE)
				{
					return;
				}
				gsPlane[gsActivePlane].mDraw = 1;

				txtTable = &gsPlane[gsActivePlane].mpBGTexturesTable[data->mType];
				allocator = &gsPlane[gsActivePlane].mBGDataAllocator;

#ifdef USE_TILEMAP_ATTRIBUTES
				item = NULL;
				itemList = NULL;
				addChunk = -1;

				opacityTable = (struct AllocatorHTable*)AllocatorHTable_Val(txtTable, AllocatorHTable_Find(txtTable, data->mpSrcData->mOpaqType));
				if (opacityTable != NULL)
				{
					itemList = (struct AllocatorList*)AllocatorHTable_Val(opacityTable, AllocatorHTable_Find(opacityTable, data->mFillColor));
				}

				if (opacityTable == NULL)
				{
					opacityTable = (struct AllocatorHTable*)StaticAllocator_Malloc(allocator, sizeof(struct AllocatorHTable));
					AllocatorHTable_Init(opacityTable, gsBGMaxFillColor, allocator);
					AllocatorHTable_Push(txtTable, data->mpSrcData->mOpaqType, opacityTable);
				}

				if (itemList == NULL)
				{
					itemList = (struct AllocatorList*)StaticAllocator_Malloc(allocator, sizeof(struct AllocatorList));
					AllocatorList_Init(itemList, sizeof(struct TRGLBGItem), allocator);
					AllocatorHTable_Push(opacityTable, data->mFillColor, itemList);
				}
				else
				{
					item = AllocatorList_Val(itemList, AllocatorList_Tail(itemList));
					if (item->mObjDataSize > 0)
					{
						const u32 itemChunkSize = ((item->mObjDataSize + chunkVertexSize - 1) / chunkVertexSize) * chunkVertexSize;
						const float* ptrPool = gsPlane[gsActivePlane].mpBGDataPool + gsPlane[gsActivePlane].mpBGVertexCt[data->mType] * lit_item_size;

						SDK_ASSERT(gsPlane[gsActivePlane].mpBGVertexCt[data->mType] + sgBGChunkSize < (s32)gsPlane[gsActivePlane].mBGMaxElements * (s32)gsPlane[gsActivePlane].mBGMaxLayers * chunkVertexSize);

						if (item->mObjDataSize + 6 > itemChunkSize)
						{
							if (item->mpDataPtr + itemChunkSize * lit_item_size == ptrPool)
							{
								addChunk = item->mObjDataSize;
							}
							else
							{
								item = NULL;
							}
						}
					}
				}

				if (item == NULL)
				{
					item = AllocatorList_Val(itemList, AllocatorList_PushBack(itemList, NULL));
					item->mpImageHeader = data->mpSrcData;

					item->mFillWithColor = data->mFillColor;

					txtr_idx = data->mType * gsPlane[gsActivePlane].mBGMaxElements * sgBGChunkSize * 6 + gsPlane[gsActivePlane].mpBGVertexCt[data->mType];
					item->mpDataPtr = gsPlane[gsActivePlane].mpBGDataPool + txtr_idx * lit_item_size;
					item->mObjDataSize = 0;

					SDK_ASSERT(gsPlane[gsActivePlane].mpBGVertexCt[data->mType] + sgBGChunkSize < (s32)gsPlane[gsActivePlane].mBGMaxElements * (s32)gsPlane[gsActivePlane].mBGMaxLayers * chunkVertexSize);
					gsPlane[gsActivePlane].mpBGVertexCt[data->mType] += chunkVertexSize;

					item->mVBOBufferIdx = gsPlane[gsActivePlane].mBGVBOBufferLastIdx;
					gsPlane[gsActivePlane].mBGVBOBufferLastIdx++;
				}
#else
				item = (struct TRGLBGItem*)AllocatorHTable_Val(txtTable, AllocatorHTable_Find(txtTable, data->mpSrcData->mOpaqType));

				if (item == NULL)
				{
					item = (struct TRGLBGItem*)StaticAllocator_Malloc(allocator, sizeof(struct TRGLBGItem));
					AllocatorHTable_Push(txtTable, data->mpSrcData->mOpaqType, item);

					item->mpImageHeader = data->mpSrcData;

					txtr_idx = data->mType * (6 * gsPlane[gsActivePlane].mBGMaxElements) * gsPlane[gsActivePlane].mBGMaxTextures + 
						(AllocatorHTable_Size(txtTable) - 1) * (6 * gsPlane[gsActivePlane].mBGMaxElements);
					item->mpDataPtr = gsPlane[gsActivePlane].mpBGDataPool + txtr_idx * item_size;
					item->mObjDataSize = 0;

					item->mVBOBufferIdx = gsPlane[gsActivePlane].mBGVBOBufferLastIdx;
					gsPlane[gsActivePlane].mBGVBOBufferLastIdx++;
				}
#endif
				fdata = item->mpDataPtr + item->mObjDataSize * item_size;
				SDK_ASSERT(item->mObjDataSize + 6 <= gsPlane[gsActivePlane].mBGMaxElements * gsPlane[gsActivePlane].mBGMaxLayers * 6); // BGRenderList vertex pool overflow
				
				yb = FX32_ONE / data->mpSrcData->mHeight2n / 2;
				xl = FX32_ONE / data->mpSrcData->mWidth2n / 2;

				txtr_idx = 0;
				
				#define posX 0
				#define posY 1
				#define uv0X 2
				#define uv0Y 3

				if(item->mObjDataSize > 0)
				{
					const float* prev_fdata = item->mpDataPtr + (item->mObjDataSize - 1) * item_size;

					*(fdata + item_size * 0 + posX) = *(fdata + item_size * 1 + posX) = FX_FX32_TO_F32(data->mX);
					*(fdata + item_size * 0 + posY) = FX_FX32_TO_F32(data->mY + FX32(data->mSrcSizeData[SRC_DATA_H]));
					*(fdata + item_size * 0 + uv0X) = *(fdata + item_size * 1 + uv0X) = FX_FX32_TO_F32(FX32(data->mSrcSizeData[SRC_DATA_X]) / data->mpSrcData->mWidth2n + xl);
					*(fdata + item_size * 1 + uv0Y) = FX_FX32_TO_F32(FX32(data->mSrcSizeData[SRC_DATA_Y]) / data->mpSrcData->mHeight2n + yb);

					if (*(prev_fdata + item_size * 0 + posX) - *(fdata + item_size * 0 + posX) == FX_FX32_TO_F32(FX32(data->mSrcSizeData[SRC_DATA_W])) &&
						*(fdata + item_size * 0 + posY) - *(prev_fdata + item_size * 0 + posY) == FX_FX32_TO_F32(FX32(data->mSrcSizeData[SRC_DATA_H])) &&
						fxAbs(*(prev_fdata + item_size * 0 + uv0X) - *(fdata + item_size * 0 + uv0X) - FX_FX32_TO_F32(FX32(data->mSrcSizeData[SRC_DATA_W]) / data->mpSrcData->mWidth2n)) < F32_EPSILON &&
						fxAbs(*(fdata + item_size * 1 + uv0Y) - *(prev_fdata + item_size * 0 + uv0Y)) < F32_EPSILON)
					{
						*(fdata + item_size * 1 + posY) = FX_FX32_TO_F32(data->mY);
						*(fdata + item_size * 0 + uv0Y) = FX_FX32_TO_F32(FX32((data->mSrcSizeData[SRC_DATA_H] + data->mSrcSizeData[SRC_DATA_Y])) / data->mpSrcData->mHeight2n - yb);
#ifdef USE_TILEMAP_ATTRIBUTES
#ifdef DEBUG_BG_CHUNKS
						{
							_glRender_SetColorColorArrayItem8(item->mpDataPtr, ((ALPHA_OPAQ << 16) | ((u16)(((u32)item->mpDataPtr) % DIFD_DEF_FILL_COLOR))), item->mObjDataSize);
						}
#else
						if (data->mFillColor != DIFD_DEF_FILL_COLOR_FULL_ALPHA)
						{
							_glRender_SetColorColorArrayItem8(item->mpDataPtr, data->mFillColor, item->mObjDataSize);
						}
#endif
#endif
						txtr_idx = 1;
					}
					else
					{
						MI_CpuCopy8(prev_fdata, fdata, item_size * sizeof(float));
						*(fdata + item_size * 1 + posX) = FX_FX32_TO_F32(data->mX + FX32(data->mSrcSizeData[SRC_DATA_W]));
						*(fdata + item_size * 1 + posY) = FX_FX32_TO_F32(data->mY + FX32(data->mSrcSizeData[SRC_DATA_H]));
						*(fdata + item_size * 0 + uv0X) = *(fdata + item_size * 0 + uv0Y) = *(fdata + item_size * 1 + uv0X) = *(fdata + item_size * 1 + uv0Y) = 0;
#ifdef USE_TILEMAP_ATTRIBUTES
#ifdef DEBUG_BG_CHUNKS
#else
						if (data->mFillColor != DIFD_DEF_FILL_COLOR_FULL_ALPHA)
#endif
						{
							MI_CpuFill8(fdata + item_size * 0 + 4, 0, sizeof(float) * 4);
							MI_CpuFill8(fdata + item_size * 1 + 4, 0, sizeof(float) * 4);
						}
#endif
					}
					fdata += item_size * 2;
					item->mObjDataSize += 2;
				}
				if (txtr_idx == 0)
				{
					*(fdata + item_size * 2 + posX) = *(fdata + item_size * 3 + posX) = FX_FX32_TO_F32(data->mX);
					*(fdata + item_size * 0 + posY) = *(fdata + item_size * 2 + posY) = FX_FX32_TO_F32(data->mY + FX32(data->mSrcSizeData[SRC_DATA_H]));
					*(fdata + item_size * 0 + posX) = *(fdata + item_size * 1 + posX) = FX_FX32_TO_F32(data->mX + FX32(data->mSrcSizeData[SRC_DATA_W]));
					*(fdata + item_size * 1 + posY) = *(fdata + item_size * 3 + posY) = FX_FX32_TO_F32(data->mY);
					*(fdata + item_size * 2 + uv0X) = *(fdata + item_size * 3 + uv0X) = FX_FX32_TO_F32(FX32(data->mSrcSizeData[SRC_DATA_X]) / data->mpSrcData->mWidth2n + xl);
					*(fdata + item_size * 0 + uv0Y) = *(fdata + item_size * 2 + uv0Y) = FX_FX32_TO_F32(FX32((data->mSrcSizeData[SRC_DATA_H] + data->mSrcSizeData[SRC_DATA_Y])) / data->mpSrcData->mHeight2n - yb);
					*(fdata + item_size * 0 + uv0X) = *(fdata + item_size * 1 + uv0X) = FX_FX32_TO_F32(FX32((data->mSrcSizeData[SRC_DATA_W] + data->mSrcSizeData[SRC_DATA_X])) / data->mpSrcData->mWidth2n - xl);
					*(fdata + item_size * 1 + uv0Y) = *(fdata + item_size * 3 + uv0Y) = FX_FX32_TO_F32(FX32(data->mSrcSizeData[SRC_DATA_Y]) / data->mpSrcData->mHeight2n + yb);
					
#ifdef USE_TILEMAP_ATTRIBUTES
#ifdef DEBUG_BG_CHUNKS
					{
						_glRender_SetColorColorArrayItem16(item->mpDataPtr, ((ALPHA_OPAQ << 16) | ((u16)(((u32)item->mpDataPtr) % DIFD_DEF_FILL_COLOR))), item->mObjDataSize);
					}
#else
					if (data->mFillColor != DIFD_DEF_FILL_COLOR_FULL_ALPHA)
					{
						_glRender_SetColorColorArrayItem16(item->mpDataPtr, data->mFillColor, item->mObjDataSize);
					}
#endif
#endif
					item->mObjDataSize += 4;
				}
#ifdef USE_TILEMAP_ATTRIBUTES
				if (addChunk != -1)
				{
					if ((addChunk + chunkVertexSize - 1) / chunkVertexSize != (item->mObjDataSize + chunkVertexSize - 1) / chunkVertexSize)
					{
						gsPlane[gsActivePlane].mpBGVertexCt[data->mType] += chunkVertexSize;
					}
				}
#endif
			}
		}
		break;

		case DIT_Obj:
		{
			struct TRGLRenderItem *item;
			BOOL newItem;
			fx32 xl, yt, rt, yb;
#ifdef CUSTOMAFFINETRANSFORM_SUPPORT
			fx32 x0, y0;
#endif
			if(_glRender_CheckForCleanup(gsActivePlane) == FALSE)
			{
				return;
			}

			if(data->mpClipRect == NULL)
			{
				gsActiveClipRect[EXDICLIPRECTXY] = NULL_CLIP_RECT;
				gsActiveClipRect[EXDICLIPRECTWH] = NULL_CLIP_RECT;
			}
			else
			{
				gsActiveClipRect[EXDICLIPRECTXY] = data->mpClipRect[EXDICLIPRECTXY];
				gsActiveClipRect[EXDICLIPRECTWH] = data->mpClipRect[EXDICLIPRECTWH];
			}

			newItem = FALSE;
			if(!(gsPlane[gsActivePlane].mRenderListSize >= 0 &&
				gsPlane[gsActivePlane].mpRenderListPool[gsPlane[gsActivePlane].mRenderListSize].mpImageHeader != NULL &&
				gsPlane[gsActivePlane].mpRenderListPool[gsPlane[gsActivePlane].mRenderListSize].mpImageHeader->mOpaqType == data->mpSrcData->mOpaqType &&
				gsPlane[gsActivePlane].mpRenderListPool[gsPlane[gsActivePlane].mRenderListSize].mClipRect[EXDICLIPRECTXY] == gsActiveClipRect[EXDICLIPRECTXY] &&
				gsPlane[gsActivePlane].mpRenderListPool[gsPlane[gsActivePlane].mRenderListSize].mClipRect[EXDICLIPRECTWH] == gsActiveClipRect[EXDICLIPRECTWH] &&
				gsPlane[gsActivePlane].mpRenderListPool[gsPlane[gsActivePlane].mRenderListSize].mFillWithColor == data->mFillColor))
			{
				gsPlane[gsActivePlane].mRenderListSize++;
				newItem = TRUE;
			}

			SDK_ASSERT(gsPlane[gsActivePlane].mRenderListSize <= gsPlane[gsActivePlane].mMaxObjOnScene); // "mRenderListSize > RENDERLIST_MAX";
			if(gsPlane[gsActivePlane].mRenderListSize > gsPlane[gsActivePlane].mMaxObjOnScene)
			{
				OS_Warning("glRender: RenderListSize overflow\n");
				SDK_ASSERT(0);
				gsPlane[gsActivePlane].mRenderListSize = gsPlane[gsActivePlane].mMaxObjOnScene - 1;
				return;
			}

			SDK_ASSERT(gsPlane[gsActivePlane].mTotalObjDataSize + 6 <= gsPlane[gsActivePlane].mMaxObjOnScene * 6);
			if(gsPlane[gsActivePlane].mTotalObjDataSize + 6 > gsPlane[gsActivePlane].mMaxObjOnScene * 6)
			{
				OS_Warning("glRender: RenderList vertex pool overflow\n");
				SDK_ASSERT(0);
				return;
			}

			item = &gsPlane[gsActivePlane].mpRenderListPool[gsPlane[gsActivePlane].mRenderListSize];
			gsPlane[gsActivePlane].mDraw = 1;

			if(newItem)
			{
				item->mpImageHeader = data->mpSrcData;
				item->mClipRect[EXDICLIPRECTXY] = gsActiveClipRect[EXDICLIPRECTXY];
				item->mClipRect[EXDICLIPRECTWH] = gsActiveClipRect[EXDICLIPRECTWH];
				item->mpDataPtr = &gsPlane[gsActivePlane].mpObjDataPool[gsPlane[gsActivePlane].mTotalObjDataSize * sizeof(struct TRGLLitVertexData)];
				item->mObjDataSize = 0;
				item->mVBOBufferIdx = gsPlane[gsActivePlane].mObjVBOBufferLastIdx;
				gsPlane[gsActivePlane].mObjVBOBufferLastIdx++;
				item->mType = RGL_IMAGE;
				item->mFillWithColor = data->mFillColor;
				_glRender_AddToRenderList(item);
			}

#ifdef CUSTOMAFFINETRANSFORM_SUPPORT
			xl = data->mAffineOriginX;
			yt = data->mAffineOriginY;
			rt = data->mDestWidth + data->mAffineOriginX;
			yb = data->mDestHeight + data->mAffineOriginY;
			x0 = data->mX - data->mAffineOriginX + FX_Mul(xl, data->mCos) + FX_Mul(yt, -data->mSin);
			y0 = data->mY - data->mAffineOriginY + FX_Mul(xl, data->mSin) + FX_Mul(yt, data->mCos);
#else
			xl = data->mX;
			yt = data->mY;
			rt = data->mX + FX32(data->mSrcSizeData[SRC_DATA_W]);
			yb = data->mY + FX32(data->mSrcSizeData[SRC_DATA_H]);
#endif
			
			if(item->mFillWithColor != DIFD_DEF_FILL_COLOR_FULL_ALPHA)
			{
				struct TRGLLitVertexData* e = (struct TRGLLitVertexData*)item->mpDataPtr;
				if (item->mObjDataSize > 0)
				{
#ifdef CUSTOMAFFINETRANSFORM_SUPPORT
					e[item->mObjDataSize] = e[item->mObjDataSize - 1];
					item->mObjDataSize++;
					e[item->mObjDataSize].pos[0] = FX_FX32_TO_F32(x0);
					e[item->mObjDataSize].pos[1] = FX_FX32_TO_F32(y0);
#else
					e[item->mObjDataSize] = e[item->mObjDataSize - 1];
					item->mObjDataSize++;
					e[item->mObjDataSize].pos[0] = FX_FX32_TO_F32(xl);
					e[item->mObjDataSize].pos[1] = FX_FX32_TO_F32(yt);
#endif
					e[item->mObjDataSize].color[0] = e[item->mObjDataSize].color[1] =
					e[item->mObjDataSize].color[2] = e[item->mObjDataSize].color[3] = 0;
					gsPlane[gsActivePlane].mTotalObjDataSize += 2;
					item->mObjDataSize++;
				}
				_glRender_SetColorColorArrayItem16(item->mpDataPtr, data->mFillColor, item->mObjDataSize);
#ifdef CUSTOMAFFINETRANSFORM_SUPPORT
				e[0 + item->mObjDataSize].pos[0] = FX_FX32_TO_F32(x0);
				e[0 + item->mObjDataSize].pos[1] = FX_FX32_TO_F32(y0);
				e[1 + item->mObjDataSize].pos[0] = FX_FX32_TO_F32(data->mX - data->mAffineOriginX + FX_Mul(xl, data->mCos) + FX_Mul(yb, -data->mSin));
				e[1 + item->mObjDataSize].pos[1] = FX_FX32_TO_F32(data->mY - data->mAffineOriginY + FX_Mul(xl, data->mSin) + FX_Mul(yb, data->mCos)); 
				e[2 + item->mObjDataSize].pos[0] = FX_FX32_TO_F32(data->mX - data->mAffineOriginX + FX_Mul(rt, data->mCos) + FX_Mul(yt, -data->mSin));
				e[2 + item->mObjDataSize].pos[1] = FX_FX32_TO_F32(data->mY - data->mAffineOriginY + FX_Mul(rt, data->mSin) + FX_Mul(yt, data->mCos));
				e[3 + item->mObjDataSize].pos[0] = FX_FX32_TO_F32(data->mX - data->mAffineOriginX + FX_Mul(rt, data->mCos) + FX_Mul(yb, -data->mSin));
				e[3 + item->mObjDataSize].pos[1] = FX_FX32_TO_F32(data->mY - data->mAffineOriginY + FX_Mul(rt, data->mSin) + FX_Mul(yb, data->mCos));
#else
				e[0 + item->mObjDataSize].pos[0] = e[1 + item->mObjDataSize].pos[0] = FX_FX32_TO_F32(xl);
				e[1 + item->mObjDataSize].pos[1] = e[3 + item->mObjDataSize].pos[1] = FX_FX32_TO_F32(yb);
				e[2 + item->mObjDataSize].pos[0] = e[3 + item->mObjDataSize].pos[0] = FX_FX32_TO_F32(rt);
				e[0 + item->mObjDataSize].pos[1] = e[2 + item->mObjDataSize].pos[1] = FX_FX32_TO_F32(yt);
#endif
				if(data->text) // fxFloor(data->mY) == data->mY && fxFloor(data->mX) == data->mX
				{
					e[0 + item->mObjDataSize].uv0[0] = e[1 + item->mObjDataSize].uv0[0] = FX_FX32_TO_F32(FX32(data->mSrcSizeData[SRC_DATA_X]) / data->mpSrcData->mWidth2n);
					e[2 + item->mObjDataSize].uv0[0] = e[3 + item->mObjDataSize].uv0[0] = FX_FX32_TO_F32(FX32(data->mSrcSizeData[SRC_DATA_W] + data->mSrcSizeData[SRC_DATA_X]) / data->mpSrcData->mWidth2n); 
					e[1 + item->mObjDataSize].uv0[1] = e[3 + item->mObjDataSize].uv0[1] = FX_FX32_TO_F32(FX32(data->mSrcSizeData[SRC_DATA_H] + data->mSrcSizeData[SRC_DATA_Y]) / data->mpSrcData->mHeight2n);
					e[0 + item->mObjDataSize].uv0[1] = e[2 + item->mObjDataSize].uv0[1] = FX_FX32_TO_F32(FX32(data->mSrcSizeData[SRC_DATA_Y]) / data->mpSrcData->mHeight2n);
				}
				else
				{
					yb = FX32_ONE / data->mpSrcData->mHeight2n / 2;
					xl = FX32_ONE / data->mpSrcData->mWidth2n / 2;
					e[0 + item->mObjDataSize].uv0[0] = e[1 + item->mObjDataSize].uv0[0] = FX_FX32_TO_F32(FX32(data->mSrcSizeData[SRC_DATA_X]) / data->mpSrcData->mWidth2n + xl);
					e[2 + item->mObjDataSize].uv0[0] = e[3 + item->mObjDataSize].uv0[0] = FX_FX32_TO_F32(FX32(data->mSrcSizeData[SRC_DATA_W] + data->mSrcSizeData[SRC_DATA_X]) / data->mpSrcData->mWidth2n - xl); 
					e[1 + item->mObjDataSize].uv0[1] = e[3 + item->mObjDataSize].uv0[1] = FX_FX32_TO_F32(FX32(data->mSrcSizeData[SRC_DATA_H] + data->mSrcSizeData[SRC_DATA_Y]) / data->mpSrcData->mHeight2n - yb);
					e[0 + item->mObjDataSize].uv0[1] = e[2 + item->mObjDataSize].uv0[1] = FX_FX32_TO_F32(FX32(data->mSrcSizeData[SRC_DATA_Y]) / data->mpSrcData->mHeight2n + yb);
				}
			}
			else
			{
				struct TRGLUnlitVertexData *e = (struct TRGLUnlitVertexData*)item->mpDataPtr;
				if(item->mObjDataSize > 0)
				{				
#ifdef CUSTOMAFFINETRANSFORM_SUPPORT
					e[item->mObjDataSize] = e[item->mObjDataSize - 1];
					item->mObjDataSize++;
					e[item->mObjDataSize].pos[0] = FX_FX32_TO_F32(x0);
					e[item->mObjDataSize].pos[1] = FX_FX32_TO_F32(y0);
#else
					e[item->mObjDataSize] = e[item->mObjDataSize - 1];
					item->mObjDataSize++;
					e[item->mObjDataSize].pos[0] = FX_FX32_TO_F32(xl);
					e[item->mObjDataSize].pos[1] = FX_FX32_TO_F32(yt);
#endif
					gsPlane[gsActivePlane].mTotalObjDataSize += 2;
					item->mObjDataSize++;
				}	
#ifdef CUSTOMAFFINETRANSFORM_SUPPORT
				e[0 + item->mObjDataSize].pos[0] = FX_FX32_TO_F32(x0);
				e[0 + item->mObjDataSize].pos[1] = FX_FX32_TO_F32(y0);
				e[1 + item->mObjDataSize].pos[0] = FX_FX32_TO_F32(data->mX - data->mAffineOriginX + FX_Mul(xl, data->mCos) + FX_Mul(yb, -data->mSin));
				e[1 + item->mObjDataSize].pos[1] = FX_FX32_TO_F32(data->mY - data->mAffineOriginY + FX_Mul(xl, data->mSin) + FX_Mul(yb, data->mCos)); 
				e[2 + item->mObjDataSize].pos[0] = FX_FX32_TO_F32(data->mX - data->mAffineOriginX + FX_Mul(rt, data->mCos) + FX_Mul(yt, -data->mSin));
				e[2 + item->mObjDataSize].pos[1] = FX_FX32_TO_F32(data->mY - data->mAffineOriginY + FX_Mul(rt, data->mSin) + FX_Mul(yt, data->mCos));
				e[3 + item->mObjDataSize].pos[0] = FX_FX32_TO_F32(data->mX - data->mAffineOriginX + FX_Mul(rt, data->mCos) + FX_Mul(yb, -data->mSin));
				e[3 + item->mObjDataSize].pos[1] = FX_FX32_TO_F32(data->mY - data->mAffineOriginY + FX_Mul(rt, data->mSin) + FX_Mul(yb, data->mCos));
#else
				e[0 + item->mObjDataSize].pos[0] = e[1 + item->mObjDataSize].pos[0] = FX_FX32_TO_F32(xl);
				e[1 + item->mObjDataSize].pos[1] = e[3 + item->mObjDataSize].pos[1] = FX_FX32_TO_F32(yb);
				e[2 + item->mObjDataSize].pos[0] = e[3 + item->mObjDataSize].pos[0] = FX_FX32_TO_F32(rt);
				e[0 + item->mObjDataSize].pos[1] = e[2 + item->mObjDataSize].pos[1] = FX_FX32_TO_F32(yt);
#endif
				if(data->text) // fxFloor(data->mY) == data->mY && fxFloor(data->mX) == data->mX
				{
					e[0 + item->mObjDataSize].uv0[0] = e[1 + item->mObjDataSize].uv0[0] = FX_FX32_TO_F32(FX32(data->mSrcSizeData[SRC_DATA_X]) / data->mpSrcData->mWidth2n);
					e[2 + item->mObjDataSize].uv0[0] = e[3 + item->mObjDataSize].uv0[0] = FX_FX32_TO_F32(FX32(data->mSrcSizeData[SRC_DATA_W] + data->mSrcSizeData[SRC_DATA_X]) / data->mpSrcData->mWidth2n); 
					e[1 + item->mObjDataSize].uv0[1] = e[3 + item->mObjDataSize].uv0[1] = FX_FX32_TO_F32(FX32(data->mSrcSizeData[SRC_DATA_H] + data->mSrcSizeData[SRC_DATA_Y]) / data->mpSrcData->mHeight2n);
					e[0 + item->mObjDataSize].uv0[1] = e[2 + item->mObjDataSize].uv0[1] = FX_FX32_TO_F32(FX32(data->mSrcSizeData[SRC_DATA_Y]) / data->mpSrcData->mHeight2n);
				}
				else
				{
					yb = FX32_ONE / data->mpSrcData->mHeight2n / 2;
					xl = FX32_ONE / data->mpSrcData->mWidth2n / 2;
					e[0 + item->mObjDataSize].uv0[0] = e[1 + item->mObjDataSize].uv0[0] = FX_FX32_TO_F32(FX32(data->mSrcSizeData[SRC_DATA_X]) / data->mpSrcData->mWidth2n + xl);
					e[2 + item->mObjDataSize].uv0[0] = e[3 + item->mObjDataSize].uv0[0] = FX_FX32_TO_F32(FX32(data->mSrcSizeData[SRC_DATA_W] + data->mSrcSizeData[SRC_DATA_X]) / data->mpSrcData->mWidth2n - xl); 
					e[1 + item->mObjDataSize].uv0[1] = e[3 + item->mObjDataSize].uv0[1] = FX_FX32_TO_F32(FX32(data->mSrcSizeData[SRC_DATA_H] + data->mSrcSizeData[SRC_DATA_Y]) / data->mpSrcData->mHeight2n - yb);
					e[0 + item->mObjDataSize].uv0[1] = e[2 + item->mObjDataSize].uv0[1] = FX_FX32_TO_F32(FX32(data->mSrcSizeData[SRC_DATA_Y]) / data->mpSrcData->mHeight2n + yb);
				}
			}
			gsPlane[gsActivePlane].mTotalObjDataSize += 4;
			item->mObjDataSize += 4;
		}
	}
}
//----------------------------------------------------------------------------------------------------------------

static GLuint _glRender_compileShader(const GLenum type, const u8* source, const GLint length)
{
	GLuint shader_object_id;
	GLint compile_status;
	SDK_ASSERT(source != NULL);
	shader_object_id = glCreateShader_fn(type);
	_glRender_CheckError("glCreateShader");
	SDK_ASSERT(shader_object_id != 0);
	glShaderSource_fn(shader_object_id, 1, (const char**)&source, &length);
	_glRender_CheckError("glShaderSource");
	glCompileShader_fn(shader_object_id);
	_glRender_CheckError("glCompileShader");
	glGetShaderiv_fn(shader_object_id, GL_COMPILE_STATUS, &compile_status);
	if(compile_status == 0)
	{
		s32 ilen = 0;
		glGetShaderiv_fn(shader_object_id, GL_INFO_LOG_LENGTH, &ilen);
		if(ilen > 0)
		{
			char *info = (char*)MALLOC(ilen * sizeof(char), "info");
			OS_Warning("glRender_compileShader: error\n");
			glGetShaderInfoLog_fn(shader_object_id, ilen, &ilen, info);
			OS_Warning("> %s", info);
			FREE(info);
		}
	}
	SDK_ASSERT(compile_status != 0);
	return shader_object_id;
}
//----------------------------------------------------------------------------------------------------------------

static GLuint _glRender_linkProgram(const GLuint vertex_shader, const GLuint fragment_shader)
{
	GLint link_status;
	GLuint program_object_id = glCreateProgram_fn();
	SDK_ASSERT(program_object_id != 0);
	glAttachShader_fn(program_object_id, vertex_shader);
	_glRender_CheckError("glAttachShader");
	glAttachShader_fn(program_object_id, fragment_shader);
	_glRender_CheckError("glAttachShader");
	glLinkProgram_fn(program_object_id);
	_glRender_CheckError("glLinkProgram");
	glGetProgramiv_fn(program_object_id, GL_LINK_STATUS, &link_status);
	if(link_status == 0)
	{
		s32 ilen = 0;
		glGetShaderiv_fn(program_object_id, GL_INFO_LOG_LENGTH, &ilen);
		if(ilen > 0)
		{
			char *info = (char*)MALLOC(ilen * sizeof(char), "info");
			OS_Warning("glRender_compileShader: error\n");
			glGetShaderInfoLog_fn(program_object_id, ilen, &ilen, info);
			OS_Warning("> %s", info);
			FREE(info);
		}
	}
	SDK_ASSERT(link_status != 0);
	return program_object_id;
}
//----------------------------------------------------------------------------------------------------------------

#ifdef SDK_DEBUG
static GLint _glRender_validateProgram(const GLuint program)
{
	GLint validate_status;
	glValidateProgram_fn(program);
	glGetProgramiv_fn(program, GL_VALIDATE_STATUS, &validate_status);
	OS_Printf("Results of validating program: %d\n", validate_status);
	return validate_status;
}
#endif
//----------------------------------------------------------------------------------------------------------------

void _glRender_buildProgram(struct Shader* shader, const u8* vertex_shader_source, const GLint vertex_shader_source_length,
								const u8* fragment_shader_source, const GLint fragment_shader_source_length)
{
	GLuint vshobj;
	GLuint fshobj;
	SDK_ASSERT(vertex_shader_source != NULL);
	SDK_ASSERT(fragment_shader_source != NULL);
	vshobj = _glRender_compileShader(GL_VERTEX_SHADER, vertex_shader_source, vertex_shader_source_length);
	fshobj = _glRender_compileShader(GL_FRAGMENT_SHADER, fragment_shader_source, fragment_shader_source_length);
	shader->handle = _glRender_linkProgram(vshobj, fshobj);
#ifdef SDK_DEBUG
	_glRender_validateProgram(shader->handle);
#endif
	glDetachShader_fn(shader->handle, fshobj);
	_glRender_CheckError("glDetachShader");
	glDetachShader_fn(shader->handle, vshobj);
	_glRender_CheckError("glDetachShader");
	glDeleteShader_fn(fshobj);
	_glRender_CheckError("glDeleteShader");
	glDeleteShader_fn(vshobj);
	_glRender_CheckError("glDeleteShader");
}
//----------------------------------------------------------------------------------------------------------------

#endif /*USE_OPENGL_2_RENDER*/
#endif /*NITRO_SDK*/
