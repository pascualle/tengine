#ifndef STDIO_FILESYSTEM_H
#define STDIO_FILESYSTEM_H

#if defined  WINDOWS_APP || defined  NIX_APP || defined IOS_APP || defined EMSCRIPTEN_APP || defined KOLIBRIOS_APP

#include "platform.h"

#ifdef __cplusplus
extern "C" {
#endif

struct InitFileSystemData;

#define FILESYSTEMFN(name) stdioFS_##name

void stdioFS_InitFileSystem(struct InitFileSystemData* data);
void stdioFS_ReleaseFileSystem(void);
BOOL stdioFS_IsFileSystemInit(void);

s16 stdioFS_fopen(const char* filename);
s32 stdioFS_fsize(s16 id);
BOOL stdioFS_fseek(u32 off, s16 id);
s32 stdioFS_fread(u8* buf, u32 size, s16 id);
void stdioFS_fclose(s16 id);

void stdioFS_lostDevice(void);

#ifdef __cplusplus
}
#endif
#endif
#endif // STDIO_FILESYSTEM_H
