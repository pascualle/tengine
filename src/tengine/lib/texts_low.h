/*
tengine - cross-platform game engine
------------------------------------

Copyright (c) 2013 Papa Pascualle
Contact Email: pascualle@gmail.com

Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
*/

#ifndef TEXTS_LOW_H
#define TEXTS_LOW_H

#include "platform.h"

#ifdef __cplusplus
extern "C" {
#endif

#define TEFONT_INFO_NAME_SHIFT 14
#define TEFONT_INFO_FLAGS_SHIFT 2
#define TEFONT_INFO_SPACING_H_SHIFT 11
#define TEFONT_INFO_SPACING_V_SHIFT 12

struct AllocatorHTable;
struct StaticAllocator;

struct AlphabetTextureOffsetMap
{
	s16 mX;
	s16 mY;
	s16 mW;
	s16 mH;
	s16 mX_off;
	s16 mY_off;
};

enum TextAlignment
{
	TEXT_ALIGNMENT_LEFT = 0x00011000,
	TEXT_ALIGNMENT_RIGHT = 0x00110000,
	TEXT_ALIGNMENT_TOP = 0x00000011,
	TEXT_ALIGNMENT_BOTTOM = 0x00000110,
	TEXT_ALIGNMENT_VCENTER = 0x00000111,
	TEXT_ALIGNMENT_HCENTER = 0x00111000
};

struct TextAlphabet
{
	u16	mFixedWidth;
	u16	mFixedHeight;
	u16	mAlphabetSize;
	struct AllocatorHTable *mpHTable;
	struct StaticAllocator *mpHTableAllocator;
	struct AlphabetTextureOffsetMap *mpAlphabetTextureOffsetMap;	// pointer to sequence of Rect that contain X, Y offset on 2d texture and width and height of each char. Can be NULL, in this case it is expected, that 2d texture contains chars with fixed width and height
	u8* mpHeap;
};

struct TERFont
{
	s32 mResRef;
	s32 mThisRef;
	u16 mSpaceCharSize;
	struct TextAlphabet mAlphabet;
	const struct BMPImage *mpAbcImage;
	const u8 *mpInfo;
	const u8 *mpRawData;
};

#ifdef __cplusplus
} /* extern "C" */
#endif

#endif //TEXTS_LOW_H
