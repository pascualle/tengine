/*
tengine - cross-platform game engine
------------------------------------

Copyright (c) 2013 Papa Pascualle
Contact Email: pascualle@gmail.com

Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
*/

#ifndef NITRO_SDK

#include "system.h"
#include "memory.h"
#include "network.h"

#ifdef USE_NETWORK

// cool samples == http://cs.baylor.edu/~donahoo/practical/CSockets/winsock.html == //

#ifdef WINDOWS_APP
#include <WinSock.h>
#include <fcntl.h>
#endif

#if defined NIX_APP
#include <sys/socket.h>
#include <sys/types.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include <unistd.h>
#include <errno.h>
#include <sys/fcntl.h>
#endif

#if defined ANDROID_NDK || defined EMSCRIPTEN_APP
#include <sys/socket.h>
#include <sys/types.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include <unistd.h>
#include <errno.h>
#include <fcntl.h>
#endif

#if defined IOS_APP
#include <sys/socket.h>
#include <sys/types.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include <unistd.h>
#include <errno.h>
#include <sys/fcntl.h>
#endif

struct NW_internal_sender
{
    BOOL in_use;
    SOCK_T mySocket;
    ADDRIN_T myAddress;
}
NWsenderUDP[MAX_NETWORK_CLIENTS];

#ifdef WINDOWS_APP
    WSADATA WsaData;
#endif

//----------------------------------------------------------------------------------------------------------------

BOOL NWInitNetwork()
{
    s32 i;
#ifdef WINDOWS_APP
    s32 err;
    err = WSAStartup(0x0101, &(WsaData));
    if(err == SOCKET_ERROR)
    {
        CERBER_PRINTF("WSAStartup() failed " );
        return FALSE;
    }
#endif
    for(i = 0; i < MAX_NETWORK_CLIENTS; i++)
    {
        NWsenderUDP[i].in_use = FALSE;
    }
    return TRUE;
}
//----------------------------------------------------------------------------------------------------------------

BOOL NWstartUDPreciever(u16 port, BOOL (*CB_)(u32 addr, struct broadcastCB_* datain, struct broadcastCB_* dataout))
{
    s32 err;
    ssize_t bytesRecv, bytesSent;
    socklen_t receiveAddrSize;
    s32 UDPsock;
    ADDRIN_T localSin;
    ADDRIN_T adrin;
    receiveAddrSize = sizeof(ADDRIN_T);

    MI_CpuClear8((void*)&(localSin), receiveAddrSize);
    localSin.sin_family = AF_INET;
    localSin.sin_port = htons(port);
    localSin.sin_addr.s_addr = htonl(INADDR_ANY);

    UDPsock = socket(AF_INET,SOCK_DGRAM,IPPROTO_UDP);
    if (UDPsock < 0)
    {
        CERBER_PRINTF("Socket create failed ");
        return FALSE;
    }
    err = bind( UDPsock, (ADDR_T*)&(localSin), receiveAddrSize );
    if (err < 0)
    {
        CERBER_PRINTF("Socket binding failed ");
        return FALSE;
    }
    for(;;)
    {
        char in_data[MAX_DATABUF_SIZE];
        char out_data[MAX_DATABUF_SIZE];
        struct broadcastCB_ inData;
        struct broadcastCB_ outData;
        inData.data = (u8*)in_data;
        outData.data = (u8*)out_data;
        inData.length = 0;
        outData.length = 0;

        bytesRecv = recvfrom(UDPsock, in_data, MAX_DATABUF_SIZE, 0, (ADDR_T*)&adrin, &receiveAddrSize);

        if(bytesRecv >= 0)
        {
			BOOL isReplyNeed;
#ifdef SDK_DEBUG
            u32* income_addr = (u32*)&(adrin.sin_addr);
            OS_Printf("UDP Listner recieved %d bytes from %X \n", (s32)bytesRecv, (*income_addr));
#endif            
			inData.length = (s32)bytesRecv;
            isReplyNeed = CB_(*(u32*)(&adrin.sin_addr), &inData, &outData);
            if(isReplyNeed == TRUE)
            {
                bytesSent = sendto(UDPsock, out_data, outData.length, 0,(ADDR_T*) &adrin, sizeof(ADDRIN_T));
                if(bytesSent == outData.length)
                {
                    OS_Printf("UDP Listner broadcast send %d bytes \n", (s32)bytesSent);
                }
                else
                {
                    CERBER_PRINTF((const char *)"Broadcast answering failed ");
                    return FALSE;
                }
            }
        }
        else
        {
            CERBER_PRINTF((const char *)"Broadcast recieving failed ");
            return FALSE;
        }
    }
    //return TRUE;
}
//----------------------------------------------------------------------------------------------------------------

BOOL NWinitUDPsender(u32 addr, u16 port, UDPconn* conn)
{
    s32 err,i;
    struct NW_internal_sender* instance = NULL;
    for(i=0; i < MAX_NETWORK_CLIENTS;i++ )
    {
        if(NWsenderUDP[i].in_use == FALSE)
        {
            instance = &(NWsenderUDP[i]);
            NWsenderUDP[i].in_use = TRUE;
            (*conn) = i;
            break;
        }
    }
    if(instance == NULL)
    {
        OS_Warning("UDP Max connections;");
        return FALSE;
    }

    MI_CpuClear8(&(instance->myAddress), sizeof(ADDRIN_T));
    instance->myAddress.sin_family = AF_INET;
    instance->myAddress.sin_addr.s_addr = addr;
    instance->myAddress.sin_port = htons(port);

    instance->mySocket = socket(AF_INET,SOCK_DGRAM,IPPROTO_UDP);
	if (instance->mySocket
#ifdef WINDOWS_APP
							== INVALID_SOCKET)
#else
							< 0)
#endif
    {
        CERBER_PRINTF("Socket create failed ");
        return FALSE;
    }
    if(addr == 0xFFFFFFFF) //must configure if sending broadcast
    {
        BOOL val = TRUE;
        instance->myAddress.sin_addr.s_addr = INADDR_BROADCAST;
        err = setsockopt(instance->mySocket, SOL_SOCKET, SO_BROADCAST,(char *)&val,sizeof(BOOL));
        if (err < 0)
        {
            CERBER_PRINTF("Socket opt set failed ");
            return FALSE;
        }
    }

    return TRUE;
}
//----------------------------------------------------------------------------------------------------------------

BOOL NWsendUDP(UDPconn conn, struct broadcastCB_* outData)
{
	ssize_t nBytesSent;
    struct NW_internal_sender* instance;
    if((conn < MAX_NETWORK_CLIENTS) && (NWsenderUDP[conn].in_use == TRUE))
    {
        instance = &(NWsenderUDP[conn]);
    }
    else
    {
        OS_Warning("Invalid connetion ID;");
        return FALSE;
	}
	nBytesSent = sendto(instance->mySocket, (char*)outData->data, outData->length, 0,
											(ADDR_T*) &(instance->myAddress), sizeof(ADDR_T));
    if(nBytesSent != outData->length)
    {
        CERBER_PRINTF("Sending failed ");
        return FALSE;
    }
    return TRUE;
}
//----------------------------------------------------------------------------------------------------------------

BOOL NWcloseUDPsender(UDPconn conn)
{
    if((conn < MAX_NETWORK_CLIENTS) && (NWsenderUDP[conn].in_use == TRUE))
    {
        CLOSESOCK(NWsenderUDP[conn].mySocket);
        NWsenderUDP[conn].in_use = FALSE;
    }
    else
    {
        OS_Warning("Faked connetion ID;");
        return FALSE;
    }
    return TRUE;
}
//----------------------------------------------------------------------------------------------------------------

#define LISTENQ  1024

BOOL BatInit()
{
#ifdef WINDOWS_APP
    s32 err;
    err = WSAStartup(0x0101, &(WsaData));
    if(err == SOCKET_ERROR)
    {
        CERBER_PRINTF("WSAStartup() failed " );
        return FALSE;
    }
#endif
    return TRUE;
}
//----------------------------------------------------------------------------------------------------------------

BOOL BatConnect(s32 addr, s32 port, BatConn* conn)
{
	//s32 optval = 1;
    s32 ret;
    s32 addrsize = sizeof(conn->addr);
	
	MI_CpuClear8(&conn->addr, sizeof(addrsize));

	conn->sock = socket(AF_INET, SOCK_STREAM, 0);

	if (conn->sock < 0)
	{
		CERBER_PRINTF("Create socket failed.");
		return FALSE;
	}
	
	/*
	ret = setsockopt(conn->sock, SOL_SOCKET, SO_REUSEADDR, (const char *)&optval , sizeof(int));
	if (ret < 0)
	{
		CERBER_PRINTF("Set socopt failed.");
		return FALSE;
	}*/


    conn->addr.sin_family = AF_INET;
    conn->addr.sin_addr.s_addr = addr;
    conn->addr.sin_port = htons((u16)port);

    ret = connect(conn->sock, (ADDR_T*)&conn->addr, addrsize);
	if (ret < 0)
	{
		if( NET_ERR != EWOULDBLOCK )
		{
			CERBER_PRINTF("Accept failed.");
			return FALSE;
		}		
	}


#ifdef WINDOWS_APP
	{
		u_long NonBlock = 1;
		if (ioctlsocket(conn->sock, FIONBIO, &NonBlock) < 0){
			return FALSE;
		}
	}
#else
	if(fcntl(conn->sock, F_GETFL) & O_NONBLOCK) {
	}else{
		if(fcntl(conn->sock, F_SETFL, fcntl(conn->sock, F_GETFL) | O_NONBLOCK) < 0) {
			return FALSE;
		}
	}
#endif

	return TRUE;
}
//----------------------------------------------------------------------------------------------------------------

BOOL BatInitAcceptSocket(s32 port, BatConn* conn)
{
    s32 optval = 1;
    s32 addrsize = sizeof(conn->addr);
    s32 ret;
    (void)port;

    conn->sock = socket(AF_INET, SOCK_STREAM, 0);

    if (conn->sock < 0)
    {
		CERBER_PRINTF("Create socket failed.");
        return FALSE;
    }

    ret = setsockopt(conn->sock, SOL_SOCKET, SO_REUSEADDR, (const char *)&optval , sizeof(int));
    if (ret < 0)
    {
		CERBER_PRINTF("Set socopt failed.");
        return FALSE;
    }

#ifdef WINDOWS_APP
	{
		u_long NonBlock = 1;
		if (ioctlsocket(conn->sock, FIONBIO, &NonBlock) == SOCKET_ERROR){
			CERBER_PRINTF("Set nonblocking failed.");
			return FALSE;
		}
	}
#else
	if(fcntl(conn->sock, F_GETFL) & O_NONBLOCK) {
	}else{
		if(fcntl(conn->sock, F_SETFL, fcntl(conn->sock, F_GETFL) | O_NONBLOCK) < 0) {
			return FALSE;
		}
	}
#endif

    MI_CpuClear8(&conn->addr, sizeof(addrsize));
    conn->addr.sin_family = AF_INET;
    conn->addr.sin_addr.s_addr = INADDR_ANY;
    conn->addr.sin_port = htons((u16)port);

    ret = bind(conn->sock, (ADDR_T*)&conn->addr, addrsize);
    if (ret  < 0)
    {
		CERBER_PRINTF("Bind socket failed.");
        return FALSE;
    }

    ret = listen(conn->sock, LISTENQ);

    if (ret < 0)
    {
		CERBER_PRINTF("Start listen failed.");
        return FALSE;
    }
    return TRUE;
}
//----------------------------------------------------------------------------------------------------------------

BOOL BatAccept(BatConn* conn, BatConn* nconn)
{
#ifdef WINDOWS_APP
    s32 clientlen;
#else
    socklen_t clientlen;
#endif
	clientlen = sizeof(nconn->addr);
    nconn->sock = accept(conn->sock, (struct sockaddr *)&nconn->addr, &clientlen);
    if(nconn->sock < 0)
    {
        nconn->isValid = FALSE;
        nconn->isConnected = FALSE;

		if( NET_ERR != EWOULDBLOCK )
		{
			CERBER_PRINTF("Accept failed.");
		}

        return FALSE;
    }
    nconn->isConnected = TRUE;
    nconn->isValid = TRUE;
    return TRUE;
}
//----------------------------------------------------------------------------------------------------------------

s32 BatReceive(BatConn* conn, void* buf, s32 bufMaxSize)
{
	s32 ret;
#ifdef WINDOWS_APP
    s32 addrsize;
#else
    socklen_t addrsize;
#endif
	addrsize = sizeof(ADDR_T);
    ret = recvfrom(conn->sock, (char*)buf, bufMaxSize, 0/*MSG_PEEK*/, (ADDR_T*)&conn->addr, &addrsize);
	if (ret < 0 && NET_ERR != EWOULDBLOCK)
	{
		CERBER_PRINTF("Recieve failed.");
	}else if(NET_ERR != EWOULDBLOCK)
	{
		//s32 k = 0;
	}
    return ret;
}
//----------------------------------------------------------------------------------------------------------------

s32 BatSend(BatConn* conn, void* buf, s32 size)
{
    s32 addrsize = sizeof(conn->addr);
    s32 ret = sendto(conn->sock, (char*)buf, size, 0, (ADDR_T*)&conn->addr, addrsize);
	if (ret < 0)
	{
		CERBER_PRINTF("Send failed.");
	}
    return ret;
}
//----------------------------------------------------------------------------------------------------------------

BOOL BatInitUdpSender(u32 addr, u16 port, BatConn* conn)
{
	s32 err;
    MI_CpuClear8(&conn->addr, sizeof(ADDRIN_T));
	conn->addr.sin_family = AF_INET;
    conn->addr.sin_addr.s_addr = INADDR_LOOPBACK;
    conn->addr.sin_port = htons(port);
    conn->isConnected = FALSE;
    conn->isValid = FALSE;
    conn->sock = socket(AF_INET,SOCK_DGRAM,IPPROTO_UDP);
    if (conn->sock < 0)
    {
        CERBER_PRINTF("Socket create failed ");
        return FALSE;
    }
    if(addr == 0xFFFFFFFF) //must configure if sending broadcast
    {
        BOOL val = TRUE;
        conn->addr.sin_addr.s_addr = INADDR_BROADCAST;
        err = setsockopt(conn->sock, SOL_SOCKET, SO_BROADCAST,(char *)&val,sizeof(BOOL));
        if (err < 0)
        {
            CERBER_PRINTF("Socket opt set failed ");
            return FALSE;
        }
    }
    conn->isValid = TRUE;
    return TRUE;
}
//----------------------------------------------------------------------------------------------------------------

BOOL BatInitUdpReciever(u16 port, BatConn* conn)
{
    s32 err;
    s32 addrsize = sizeof(conn->addr);

    conn->isValid = FALSE;
    conn->isConnected = FALSE;

    MI_CpuClear8((void*)&(conn->addr), addrsize);
    conn->addr.sin_family = AF_INET;
    conn->addr.sin_port = htons(port);
    conn->addr.sin_addr.s_addr = htonl(INADDR_ANY);

    conn->sock= socket(AF_INET,SOCK_DGRAM,IPPROTO_UDP);
    if (conn->sock < 0)
    {
		CERBER_PRINTF("Socket create failed ");
		conn->isValid = FALSE;
		return FALSE;
    }

#ifdef WINDOWS_APP
	{
		u_long NonBlock = 1;
		if (ioctlsocket(conn->sock, FIONBIO, &NonBlock) == SOCKET_ERROR){
			CERBER_PRINTF("Set nonblocking failed.");
			return FALSE;
		}
	}
#else
	if(fcntl(conn->sock, F_GETFL) & O_NONBLOCK) {
	}else{
		if(fcntl(conn->sock, F_SETFL, fcntl(conn->sock, F_GETFL) | O_NONBLOCK) < 0) {
			return FALSE;
		}
	}
#endif

    err = bind( conn->sock, (ADDR_T*)&(conn->addr), addrsize );
    if (err < 0)
    {
		CERBER_PRINTF("Socket binding failed ");
		conn->isValid = FALSE;
		return FALSE;
    }

    conn->isValid = TRUE;
    return TRUE;
}
//----------------------------------------------------------------------------------------------------------------

BOOL BatClose(BatConn* conn)
{
    CLOSESOCK(conn->sock);
	return TRUE;
}
//----------------------------------------------------------------------------------------------------------------

#endif /* USE_NETWORK */
#endif /* !NITRO_SDK */