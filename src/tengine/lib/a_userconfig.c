/*
tengine - cross-platform game engine
------------------------------------

Copyright (c) 2013 Papa Pascualle
Contact Email: pascualle@gmail.com

Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
*/

#include "system.h"

#if (defined ANDROID_NDK && defined NDK_NATIVE_API10) || defined IOS_APP

#include <sys/stat.h>
#include <errno.h>
#include "lib/platform_low.h"
#include "lib/filesystem_low.h"
#include "texts.h"
#include "userconfig.h"

#if defined ANDROID_NDK && defined NDK_NATIVE_API10
extern struct AAssetManager* gpAssetManager;
extern char gInternalPath[MAX_INTERNALFILEPATH];
#endif
static char gConfigPath[MAX_INTERNALFILEPATH] = { 0 };
static BOOL gsUserConfigError = FALSE;
static const char gPathSlash[] = "/";
static const char gTengineConfigFileName[] = "config.bin";
static const char gHeaderMark[] = "CB";

struct UserConfigHeader
{
	char a[2];
	u32 size;
};

//-------------------------------------------------------------------------------------------

s32 InitUserConfig(const char* appName)
{
#if defined IOS_APP
    STD_StrCpy(gConfigPath, getenv("HOME"));
    STD_StrCat(gConfigPath, gPathSlash);
    STD_StrCat(gConfigPath, "Documents");
    STD_StrCat(gConfigPath, gPathSlash);
    STD_StrCat(gConfigPath, gTengineConfigFileName);

    return UCR_SUCCESS;
#else
    s32 res;
    struct stat sb;

    STD_StrCpy(gConfigPath, gInternalPath);
    STD_StrCat(gConfigPath, gPathSlash);
    STD_StrCat(gConfigPath, gTengineConfigFileName);

    res = stat(gInternalPath, &sb);
    if (0 == res && (sb.st_mode & S_IFDIR))
    {
        return UCR_SUCCESS;
    }
    else if (ENOENT == errno)
    {
        res = mkdir(gInternalPath, 0770);
    }
    if (0 == res)
    {
        return UCR_SUCCESS;
    }
    gsUserConfigError = TRUE;
    return UCR_INIT_ERROR;
#endif
}
//-------------------------------------------------------------------------------------------

s32 SaveUserConfig(const void* pdata, u32 size)
{
    FILE* f;
    size_t write_bytes;
    struct UserConfigHeader uch;
    if (gsUserConfigError)
    {
        return UCR_INIT_ERROR;
    }
    f = fopen(gConfigPath, "wb");
    if (f == NULL)
    {
        return UCR_WRITE_ERROR;
    }
    uch.a[0] = gHeaderMark[0];
    uch.a[1] = gHeaderMark[1];
    uch.size = size;
    fwrite(&uch, sizeof(struct UserConfigHeader), 1, f);
    write_bytes = fwrite(pdata, 1, size, f);
    if (size != (u32)write_bytes)
    {
        fclose(f);
        return UCR_WRITE_ERROR;
    }
    fclose(f);
    return UCR_SUCCESS;
}
//-------------------------------------------------------------------------------------------

s32 LoadUserConfig(void* pdata, u32 size) {
    s32 res;
    FILE *f;
    size_t read_bytes;
    struct stat sb;
    struct UserConfigHeader uch;
    if (gsUserConfigError)
    {
        return UCR_INIT_ERROR;
    }
    res = stat(gConfigPath, &sb);
    if (0 == res && (sb.st_mode & S_IFREG))
    {
        f = fopen(gConfigPath, "rb");
        if (f == NULL)
        {
            return UCR_READ_ERROR;
        }
        fread(&uch, sizeof(struct UserConfigHeader), 1, f);
        if (uch.a[0] != gHeaderMark[0] || uch.a[1] != gHeaderMark[1])
        {
            fclose(f);
            return UCR_WRONG_FILE_STRUCTURE;
        }
        if (uch.size != size)
        {
            fclose(f);
            return UCR_WRONG_DATA_SIZE;
        }
        read_bytes = fread(pdata, 1, size, f);
        if (size != (u32) read_bytes)
        {
            fclose(f);
            return UCR_READ_ERROR;
        }
        fclose(f);
        return UCR_SUCCESS;
    }
    else
    {
        return UCR_CONFIG_NOT_EXIST;
    }
}
//-------------------------------------------------------------------------------------------
#endif
