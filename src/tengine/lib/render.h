#ifndef RENDER_H_INCLUDED
#define RENDER_H_INCLUDED

#ifdef USE_CUSTOM_RENDER
	#include "lib/easygraphics.h"
#endif
#ifdef USE_OPENGL_1_RENDER
	#include "lib/render2dgl1.h"
#endif
#ifdef USE_OPENGL_2_RENDER
	#include "render2dgl2.h"
#endif

#endif