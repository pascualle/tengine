#ifndef GXHELPRES_H_INCLUDED
#define GXHELPRES_H_INCLUDED

#include "system.h"

#ifdef __cplusplus
extern "C" {
#endif

s32 gxHelper_preDrawLine(fx32 iDestW, fx32 iDestH, fx32 *iX0, fx32 *iY0, fx32 *iX1, fx32 *iY1);
s32 gxHelper_preDrawImage(s32 iDestW, s32 iDestH, s32 *iX, s32 *iY, const struct BMPImage* pSrcData, s32 *iSrcX, s32 *iSrcY, s32 *iSrcW, s32 *iSrcH);
s32 gxHelper_preDrawSquare(fx32 iDestW, fx32 iDestH, fx32 *iX, fx32 *iY, fx32 *iWidth, fx32 *iHeight);

#ifdef __cplusplus
} /* extern "C" */
#endif

#endif
