//---------------------------------------------------------------------------

#include "easygraphics.h"
#include "gx_helpers.h"
#include "memory.h"
#include "fxmath.h"
#include "filesystem.h"

#ifdef USE_CUSTOM_RENDER

//---------------------------------------------------------------------------

static struct BGPlane
{
	s32 offX;
	s32 offY;
	u32 fillLineSize;
	u32 bufSizeBytes;
	struct BMPImage buf;
#ifdef USE_TILEBUFFER
	struct BMPImage saveBuf;
#endif
	union ClearLineData
	{
		GXRgba	*pClearLineDC;
		u8		*pClearLine8;
	}clearLine;
	union FillLineData
	{
		GXRgba	*pFillLineDC;
		u8		*pFillLine8;
	}fillLine;
#ifdef NITRO_SDK
	union VRAMData
	{
		GXRgba	*pVRAMPrtDC;
		u8		*pVRAMPrt8;
	}VRAMPrt;
	void (*_copyProcDef)(u32, const void*, void*, u32);
	void (*_copyProcCur)(u32, const void*, void*, u32);
	void (*_flushProc)(const void*, u32);
	void (*_affineProc)(const MtxFx22*, int, int, int, int);
#else
	TransferFrameBufferCallback _copyProcDef;
	TransferFrameBufferCallback _copyProcCur;
#endif
}gsPlane[BGSELECT_NUM] = {0};

static union
{
	GXRgba	dc;
	u8		index;		
}gsCurrentColor;

enum DrawFunctionName
{
	DT_NONE,
	DT_MIRRORX,
	DT_MIRRORY,
	DT_COUNT
};

static BOOL gsGraphicsInit = FALSE;
static enum BGSelect gsActivePlane = BGSELECT_NUM;

#ifndef NITRO_SDK
static TransferFrameBufferCallback gsInitCopyProcFn = NULL;
#endif

//---------------------------------------------------------------------------

static inline s32 _getPtrAtPoint(s32 x, s32 y);
static inline void *_defCopyFn(u32 p1, u32 p2, s32 w);

#ifdef USE_TILEBUFFER
static void _saveGraphicsToMemory(void);
static void _restoreGraphicsFromMemory(s32 iX, s32 iY);
#endif
static void _initGraphicsPointers(void);
static void _initPlane(const struct RenderPlaneInitParams* ipParams, BOOL joinMode);
static void _setPlaneSizes(enum BGSelect iType, const struct RenderPlaneSizeParams* ipParams);
static void _transferDataToVRAMJoin(void);
static void _transferDataToVRAMSingle(void);
static void _markPlaneToVRAMTransfering(enum BGSelect iBGType);
//---------------------------------------------------------------------------

#ifdef NITRO_SDK
static void _dummyCopyFn(u32, const void*, void*, u32);
static void _dummyAffineFn(const MtxFx22*, int ,int ,int ,int);
static void (*_activeAffineProc)(const MtxFx22*, int ,int ,int ,int) = _dummyAffineFn;
static void _dummyAffineFn(const MtxFx22* a1, int a2, int a3, int a4, int a5){(void)a1;(void)a2;(void)a3;(void)a4;(void)a5;}
static void _dummyFlushFn(const void*, u32);
#else
static void _dummyCopyFn(const struct BMPImage*, s32, s32, s32, s32);
#endif
static void _dummyVoidFn(void);

#ifdef USE_TILEBUFFER
static void _dummyRestoreFn(s32, s32);
static void (*_activeSaveMemFn)(void) = _dummyVoidFn;
static void (*_activeRestoreMemFn)(s32, s32) = _dummyRestoreFn;
static void _dummyRestoreFn(s32 a1, s32 a2){(void)a1;(void)a2;}
#endif

static void (*_transferToVRAM)(void);
	
#ifdef NITRO_SDK
static void _dummyCopyFn(u32, const void*, void*, u32){}
static void _dummyFlushFn(const void* a1, u32 a2){(void)a1;(void)a2;}
#else
static void _dummyCopyFn(const struct BMPImage* v1, s32 v2, s32 v3, s32 v4, s32 v5){(void)v1;(void)v2;(void)v3;(void)v4;(void)v5;}
#endif

static void _dummyVoidFn(void){}

//static void (*_drawPixel[BMP_TYPE_NUM])(s32 iX, s32 iY);
//static void _drawPixel256(s32 iX, s32 iY);
//static void _drawPixelDC(s32 iX, s32 iY);

static void (*_drawLine[BMP_TYPE_NUM])(s32 x1, s32 y1, s32 x2, s32 y2);
static void _drawLine256(s32 x1, s32 y1, s32 x2, s32 y2);
static void _drawLineDC(s32 x1, s32 y1, s32 x2, s32 y2);

//static void (*_clearRect[BMP_TYPE_NUM])(s32 iX, s32 iY, s32 iWidth, s32 iHeight);
//static void _clearRectDC(s32 iX, s32 iY, s32 iWidth, s32 iHeight);
//static void _clearRect256(s32 iX, s32 iY, s32 iWidth, s32 iHeight);

static void (*_colorRect[BMP_TYPE_NUM])(s32 iX, s32 iY, s32 iWidth, s32 iHeight);
void _colorRectDC(s32 iX, s32 iY, s32 iWidth, s32 iHeight);
void _colorRect256(s32 iX, s32 iY, s32 iWidth, s32 iHeight);

static void (*_drawImageFn[BMP_TYPE_NUM][DT_COUNT])(s32 iScreenX, s32 iScreenY, const struct BMPImage* pSrcData,
											s32 iSrcX, s32 iSrcY, s32 iSrcW, s32 iSrcH, const u32 data);
static void _drawImageDC(s32 iScreenX, s32 iScreenY,const struct BMPImage* pSrcData,
							s32 iSrcX, s32 iSrcY, s32 iSrcW, s32 iSrcH, const u32 data);
static void _drawImageDCMirrorX(s32 iScreenX, s32 iScreenY, const struct BMPImage* pSrcData,
							s32 iSrcX, s32 iSrcY, s32 iSrcW, s32 iSrcH, const u32 data);
static void _drawImageDCMirrorY(s32 iScreenX, s32 iScreenY, const struct BMPImage* pSrcData,
							s32 iSrcX, s32 iSrcY, s32 iSrcW, s32 iSrcH, const u32 data);
static void _drawImage256(s32 iScreenX, s32 iScreenY,const struct BMPImage* pSrcData,
							s32 iSrcX, s32 iSrcY, s32 iSrcW, s32 iSrcH, const u32 data);
static void _drawImage256MirrorX(s32 iScreenX, s32 iScreenY, const struct BMPImage* pSrcData,
							s32 iSrcX, s32 iSrcY, s32 iSrcW, s32 iSrcH, const u32 data);
static void _drawImage256MirrorY(s32 iScreenX, s32 iScreenY, const struct BMPImage* pSrcData,
							s32 iSrcX, s32 iSrcY, s32 iSrcW, s32 iSrcH, const u32 data);
/*static void _drawImage256ToImageDC(const BMPImage *pSrcImage,
                            s32 iSrcX, s32 iSrcY,
                            s32 iWidth, s32 iHeight,
							BMPImage *pDestImage, s32 iDestX, s32 iDestY);*/
static void (*_drawImageToImage[BMP_TYPE_NUM])(const struct BMPImage *pSrcImage,
						s32 iSrcX, s32 iSrcY, s32 iWidth, s32 iHeight,
						struct BMPImage *pDestImage, s32 iDestX, s32 iDestY);
/*static void _drawImageToImageDC(const struct BMPImage *pSrcImage,
						s32 iSrcX, s32 iSrcY, s32 iWidth, s32 iHeight,
						struct BMPImage *pDestImage, s32 iDestX, s32 iDestY);*/
/*static void _drawImageToImage256(const struct BMPImage *pSrcImage,
						s32 iSrcX, s32 iSrcY, s32 iWidth, s32 iHeight,
						struct BMPImage *pDestImage, s32 iDestX, s32 iDestY);*/
static void (*_drawOpaqImageToImage[BMP_TYPE_NUM])(const struct BMPImage *pSrcImage,
                        s32 iSrcX, s32 iSrcY, s32 iWidth, s32 iHeight, 
                        struct BMPImage *pDestImage, s32 iDestX, s32 iDestY);
static void _drawOpaqImageToImageDC(const struct BMPImage *pSrcImage, 
                        s32 iSrcX, s32 iSrcY, s32 iWidth, s32 iHeight, 
                        struct BMPImage *pDestImage, s32 iDestX, s32 iDestY);
static void _drawOpaqImageToImage256(const struct BMPImage *pSrcImage, 
                        s32 iSrcX, s32 iSrcY, s32 iWidth, s32 iHeight, 
                        struct BMPImage *pDestImage, s32 iDestX, s32 iDestY);
//---------------------------------------------------------------------------

BOOL egRender_IsGraphicsInit()
{
    return gsGraphicsInit;
}
//---------------------------------------------------------------------------
/*
static void JoinScreenMode(struct PlaneInitBGParams* ipParamsScr1, struct PlaneInitBGParams* ipParamsScr2)
{
    if(ipParamsScr1 == NULL || ipParamsScr2 == NULL)
    {
        //pointers must be not NULL
        SDK_ASSERT(0);
        return;
    }

    if(gspClearLine == NULL)
    {
        SDK_ASSERT(0); // please init graphics
        return;
    }
    
    if(gsBufSizeW < SCREEN_WIDTH || gsBufSizeH < SCREEN_HEIGHT * 2)
    {
        SDK_ASSERT(0); // in this mode size of gsBufSizeW must be at least 256 and size gsBufSizeH must be at least 192*2
        return;
    }
    
    if(GX_DISP_SELECT_MAIN_SUB == GX_GetDispSelect())
    {
        _initPlane(ipParamsScr1, FALSE);
        _initPlane(ipParamsScr2, TRUE);
        gsActiveBG = ipParamsScr1->mBGType;
        gsActiveSubBG = ipParamsScr2->mBGType;
    }
    else
    {
        _initPlane(ipParamsScr1, TRUE);
        _initPlane(ipParamsScr2, FALSE);
        gsActiveBG = ipParamsScr2->mBGType;
        gsActiveSubBG = ipParamsScr1->mBGType;
    }
    _transferToVRAM = _transferDataToVRAMJoin;
    gspActiveBuf.bufDC = gspBuf[gsActiveBG];
    SDK_NULL_ASSERT(gspActiveBuf.bufDC);
    _activeSaveMemFn = _saveGraphicsToMemory;
    _activeRestoreMemFn = _restoreGraphicsFromMemory;
    _activeAffineProc = _affineProc[gsActiveBG];
}
//---------------------------------------------------------------------------
*/

void egRender_PlaneInit(const struct RenderPlaneInitParams* ipParams)
{
	SDK_ASSERT(gsGraphicsInit);    
    if(ipParams == NULL)
    {
        //pointer must be not NULL
        SDK_ASSERT(0);
        return;
    }

    if(egRender_IsJoinScreenMode())
    {
        SDK_ASSERT(0); // reinit graphics
        return;
    }

	egRender_PlaneRelease(ipParams->mBGType);
	
    _transferToVRAM = _transferDataToVRAMSingle;

	gsPlane[ipParams->mBGType].offX = ipParams->mX;
	gsPlane[ipParams->mBGType].offY = ipParams->mY;
	gsPlane[ipParams->mBGType].buf.mType = ipParams->mColorType;

	_setPlaneSizes(ipParams->mBGType, &ipParams->mSizes);

	gsPlane[ipParams->mBGType].clearLine.pClearLine8 = (u8*)MALLOC(gsPlane[ipParams->mBGType].buf.mWidth * (ipParams->mColorType == BMP_TYPE_256 ? 1 : sizeof(GXRgba)), "egPlaneInit:pClearLine8");
	gsPlane[ipParams->mBGType].fillLine.pFillLine8 = (u8*)MALLOC(gsPlane[ipParams->mBGType].buf.mWidth * (ipParams->mColorType == BMP_TYPE_256 ? 1 : sizeof(GXRgba)), "egPlaneInit:pFillLine8");
    if(ipParams->mColorType == BMP_TYPE_256)
    {
    	MI_CpuFill8(gsPlane[ipParams->mBGType].clearLine.pClearLine8, TRANSPARENT_COLOR_256, (u32)gsPlane[ipParams->mBGType].buf.mWidth);	
    	gsPlane[ipParams->mBGType].fillLine.pFillLine8[0] = TRANSPARENT_COLOR_256;
    }
    else
    {
    	MI_CpuFill16(gsPlane[ipParams->mBGType].clearLine.pClearLineDC, TRANSPARENT_COLOR_DC, gsPlane[ipParams->mBGType].buf.mWidth * sizeof(GXRgba));	
    	gsPlane[ipParams->mBGType].fillLine.pFillLineDC[0] = TRANSPARENT_COLOR_DC;
    }

	_initPlane(ipParams, FALSE);
}
//---------------------------------------------------------------------------

static void _setPlaneSizes(enum BGSelect iType, const struct RenderPlaneSizeParams* ipParams)
{
    SDK_ASSERT(iType != BGSELECT_NUM);
	gsPlane[iType].buf.mWidth2n = ipParams->mFrameBufferWidth8;
	gsPlane[iType].buf.mHeight2n = ipParams->mFrameBufferHeight8;
	if((ipParams->mFrameBufferWidth8 % 8) != 0 || (ipParams->mFrameBufferWidth8 % 8) != 0)
	{
		OS_Warning("RenderPlaneSizeParams: mFrameBufferWidth8 and mFrameBufferWidth8 must be multiple of 8");
		SDK_ASSERT(0);
	}
    gsPlane[iType].buf.mWidth = (u16)ipParams->mViewWidth;
	gsPlane[iType].buf.mHeight = (u16)ipParams->mViewHeight;
	if(gsPlane[iType].buf.mWidth < gsPlane[iType].buf.mWidth || gsPlane[iType].buf.mHeight < gsPlane[iType].buf.mHeight) 
	{
		OS_Warning("RenderPlaneSizeParams: Buffer sizes must be greater than View sizes");
		SDK_ASSERT(0);
	}
	gsPlane[iType].bufSizeBytes = (gsPlane[iType].buf.mWidth * gsPlane[iType].buf.mHeight) * (gsPlane[iType].buf.mType == BMP_TYPE_256 ? 1 : sizeof(GXRgba));
#ifdef USE_TILEBUFFER
	gsPlane[iType].saveBuf.mWidth = gsPlane[iType].buf.mWidth;
	gsPlane[iType].saveBuf.mHeight = gsPlane[iType].buf.mHeight;
	gsPlane[iType].saveBuf.mWidth2n = gsPlane[iType].buf.mWidth;
	gsPlane[iType].saveBuf.mHeight2n = gsPlane[iType].buf.mHeight;
#endif
}
//---------------------------------------------------------------------------

BOOL egRender_IsRenderPlaneInit(enum BGSelect iType)
{
    SDK_ASSERT(iType != BGSELECT_NUM);
	return gsGraphicsInit && gsPlane[iType].buf.data.mpData256 != NULL;
}
//---------------------------------------------------------------------------

static void _initPlane(const struct RenderPlaneInitParams* ipParams, BOOL joinMode)
{
	s32 size2n;
	gsPlane[ipParams->mBGType].buf.mOpaqType = 0;
	size2n = (gsPlane[ipParams->mBGType].buf.mWidth2n * gsPlane[ipParams->mBGType].buf.mHeight2n) *
				 (gsPlane[ipParams->mBGType].buf.mType == BMP_TYPE_256 ? 1 : (s32)sizeof(GXRgba));
#ifdef NITRO_SDK
	switch(ipParams->mColorType)
	{
   		case BMP_TYPE_DC16:
		   
			gsPlane[ipParams->mBGType].buf.mType = BMP_TYPE_DC16;
			
			switch(ipParams->mBGType)
			{
			   case BGSELECT_MAIN2:
		            if(gsPlane[ipParams->mBGType].buf.data.mpDataDC16 == NULL)
		            {
						if(!joinMode)
						{
							gsPlane[ipParams->mBGType].buf.data.mpDataDC16 = MALLOC(size2n, "_initPlane:.buf.mpDataDC16");
						}
						G2_SetBG2ControlDCBmp(GX_BG_SCRSIZE_DCBMP_256x256, GX_BG_AREAOVER_XLU, ipParams->mScreenBase);
						G2_SetBG2Priority(ipParams->mBGPriority);
						gsPlane[ipParams->mBGType].VRAMPrt.pVRAMPrtDC = G2_GetBG2ScrPtr();
						gsPlane[ipParams->mBGType]._affineProc = G2_SetBG2Affine;
						gsPlane[ipParams->mBGType]._flushProc = DC_FlushRange;
						gsPlane[ipParams->mBGType]._copyProcDef = MI_DmaCopy32;
		            }
		       break;
		       case BGSELECT_MAIN3:
		            if(gsPlane[ipParams->mBGType].buf.data.mpDataDC16 == NULL)
		            {
						if(!joinMode)
						{
							gsPlane[ipParams->mBGType].buf.data.mpDataDC16 = MALLOC(size2n, "_initPlane:.buf.mpDataDC16");
						}
						G2_SetBG3ControlDCBmp(GX_BG_SCRSIZE_DCBMP_256x256, GX_BG_AREAOVER_XLU, ipParams->mScreenBase);
						G2_SetBG3Priority(ipParams->mBGPriority);
						gsPlane[ipParams->mBGType].VRAMPrt.pVRAMPrtDC = G2_GetBG3ScrPtr();
						gsPlane[ipParams->mBGType]._affineProc = G2_SetBG3Affine;
						gsPlane[ipParams->mBGType]._flushProc = DC_FlushRange;
						gsPlane[ipParams->mBGType]._copyProcDef = MI_DmaCopy32;
		            }        
		       break;
		       case BGSELECT_SUB2:
		            if(gsPlane[ipParams->mBGType].buf.data.mpDataDC16 == NULL)
		            {
						if(!joinMode)
						{
							gsPlane[ipParams->mBGType].buf.data.mpDataDC16 = MALLOC(size2n, "_initPlane:.buf.mpDataDC16");
						}
						G2S_SetBG2ControlDCBmp(GX_BG_SCRSIZE_DCBMP_256x256, GX_BG_AREAOVER_XLU, ipParams->mScreenBase);
						G2S_SetBG2Priority(ipParams->mBGPriority);
						gsPlane[ipParams->mBGType].VRAMPrt.pVRAMPrtDC = G2S_GetBG2ScrPtr();
						gsPlane[ipParams->mBGType]._affineProc = G2S_SetBG2Affine;
						gsPlane[ipParams->mBGType]._flushProc = DC_FlushRange;
						gsPlane[ipParams->mBGType]._copyProcDef = MI_DmaCopy32;
		            }
		       break;
		       case BGSELECT_SUB3:
		            if(gsPlane[ipParams->mBGType].buf.data.mpDataDC16 == NULL)
		            {
						if(!joinMode)
						{
							gsPlane[ipParams->mBGType].buf.data.mpDataDC16 = MALLOC(size2n, "_initPlane:.buf.mpDataDC16");
						}
						G2S_SetBG3ControlDCBmp(GX_BG_SCRSIZE_DCBMP_256x256, GX_BG_AREAOVER_XLU, ipParams->mScreenBase);
						G2S_SetBG3Priority(ipParams->mBGPriority);
						gsPlane[ipParams->mBGType].VRAMPrt.pVRAMPrtDC = G2S_GetBG3ScrPtr();
						gsPlane[ipParams->mBGType]._affineProc = G2S_SetBG3Affine;
						gsPlane[ipParams->mBGType]._flushProc = DC_FlushRange;
						gsPlane[ipParams->mBGType]._copyProcDef = MI_DmaCopy32;
		            }
		       break;
		       default:
		           SDK_ASSERT(0); //wrong parameter
		}
		break;
		
		case BMP_TYPE_256:

			gsPlane[ipParams->mBGType].buf.mType = BMP_TYPE_256;
			
			switch(ipParams->mBGType)
			{
				case BGSELECT_MAIN3:
					if(!joinMode)
					{
						gsPlane[ipParams->mBGType].buf.data.mpData256 = MALLOC(size2n, "_initPlane:.buf.mpData256");
					}
					G2_SetBG3Control256Bmp(GX_BG_SCRSIZE_256BMP_256x256, GX_BG_AREAOVER_XLU, ipParams->mScreenBase);
					G2_SetBG3Priority(ipParams->mBGPriority);
					gsPlane[ipParams->mBGType].VRAMPrt.pVRAMPrtDC = G2_GetBG3ScrPtr();
					gsPlane[ipParams->mBGType]._affineProc = G2_SetBG3Affine;
					gsPlane[ipParams->mBGType]._flushProc = DC_FlushRange;
					gsPlane[ipParams->mBGType]._copyProcDef = MI_DmaCopy32;
				break;
				default:
				   SDK_ASSERT(0); //wrong parameter
			}
   		break;
   		default:
   			SDK_ASSERT(0); //wrong parameter
	}
#else
	(void)joinMode;
	switch(ipParams->mColorType)
	{
		case BMP_TYPE_DC16:
			gsPlane[ipParams->mBGType].buf.data.mpDataDC16 = (u16*)MALLOC(size2n, "eg:_initPlane:buf.mpDataDC16");
		break;
		case BMP_TYPE_256:
			gsPlane[ipParams->mBGType].buf.data.mpData256 = (u8*)MALLOC(size2n, "eg:_initPlane:buf.mpData256");
		break;
		default:
			SDK_ASSERT(0); // wrong color format
	}
	gsPlane[ipParams->mBGType]._copyProcDef = gsInitCopyProcFn;
#endif
#ifdef USE_TILEBUFFER
	gsPlane[ipParams->mBGType].saveBuf.mOpaqType = gsPlane[ipParams->mBGType].buf.mOpaqType;
	gsPlane[ipParams->mBGType].saveBuf.mType = gsPlane[ipParams->mBGType].buf.mType;
	gsPlane[ipParams->mBGType].saveBuf.data.mpData256 = (u8*)MALLOC(gsPlane[ipParams->mBGType].bufSizeBytes, "eg:_initPlane:saveBuf.mpData256");
#endif
}
//---------------------------------------------------------------------------

void egRender_PlaneResize(enum BGSelect iType, const struct RenderPlaneSizeParams* ipBGParams)
{
	SDK_ASSERT(ipBGParams);
    SDK_ASSERT(iType != BGSELECT_NUM);
	SDK_ASSERT(gsPlane[iType].buf.mWidth2n); // init plane!
	SDK_ASSERT(gsPlane[iType].buf.mHeight2n);
	if(ipBGParams->mViewWidth <= gsPlane[iType].buf.mWidth2n && ipBGParams->mViewHeight <= gsPlane[iType].buf.mHeight2n)
	{
		_setPlaneSizes(iType, ipBGParams);
	}
	else
#ifdef FRAME_ALLOCATOR
	{
		OS_Warning("FRAME_ALLOCATOR assertion: PlaneResize function cannot reallocate memory for new buffer size");
		SDK_ASSERT(0);	
	}
#else
	{
		_setPlaneSizes(iType, ipBGParams);
		SDK_ASSERT(0); // todo: recreate buffers
	}
#endif
}
//---------------------------------------------------------------------------

s32 egRender_GetViewWidth(enum BGSelect iType)
{
    SDK_ASSERT(iType != BGSELECT_NUM);
    return gsPlane[iType].buf.mWidth;
}
//---------------------------------------------------------------------------

s32 egRender_GetViewHeight(enum BGSelect iType)
{
	SDK_ASSERT(iType != BGSELECT_NUM);
	return gsPlane[iType].buf.mHeight;
}
//---------------------------------------------------------------------------

s32 egRender_GetViewLeft(enum BGSelect iType)
{
	SDK_ASSERT(iType != BGSELECT_NUM);
	return gsPlane[iType].offX;	
}
//----------------------------------------------------------------------------------

s32 egRender_GetViewTop(enum BGSelect iType)
{
	SDK_ASSERT(iType != BGSELECT_NUM);
	return gsPlane[iType].offY;
}
//----------------------------------------------------------------------------------

s32 egRender_GetFrameBufferWidth(enum BGSelect iType)
{
	SDK_ASSERT(iType != BGSELECT_NUM);
	return gsPlane[iType].buf.mWidth2n;
}
//---------------------------------------------------------------------------

s32 egRender_GetFrameBufferHeight(enum BGSelect iType)
{
	SDK_ASSERT(iType != BGSELECT_NUM);
	return gsPlane[iType].buf.mHeight2n;
}
//---------------------------------------------------------------------------

BOOL egRender_IsJoinScreenMode()
{
    return _transferToVRAM == _transferDataToVRAMJoin;
}
//---------------------------------------------------------------------------

void egRender_IncludeToVRAMTransfering(enum BGSelect iBGType)
{
	SDK_ASSERT(iBGType != BGSELECT_NUM);
	if(gsPlane[iBGType].buf.data.mpDataDC16 != NULL)
	{
#ifdef NITRO_SDK
		gsPlane[iBGType]._copyProcDef = MI_DmaCopy32;
#else
		gsPlane[iBGType]._copyProcDef = gsInitCopyProcFn;
#endif
		if(gsPlane[iBGType]._copyProcCur != _dummyCopyFn)
		{
			gsPlane[iBGType]._copyProcCur = gsPlane[iBGType]._copyProcDef;
		}
	}
}
//---------------------------------------------------------------------------

void _markPlaneToVRAMTransfering(enum BGSelect iBGType)
{
	SDK_ASSERT(iBGType != BGSELECT_NUM);
	gsPlane[iBGType]._copyProcCur = gsPlane[iBGType]._copyProcDef;
#ifdef NITRO_SDK
	gsPlane[iBGType]._flushProc = DC_FlushRange;
#endif
}
//---------------------------------------------------------------------------

void egRender_ExcludeFromVRAMTransfering(enum BGSelect iBGType)
{
	SDK_ASSERT(iBGType != BGSELECT_NUM);
	gsPlane[iBGType]._copyProcDef = _dummyCopyFn;
	gsPlane[iBGType]._copyProcCur = _dummyCopyFn;
}
//---------------------------------------------------------------------------

void egRender_FreezeCurrentFrameBuffer(BOOL val)
{
	(void)val;
}
//---------------------------------------------------------------------------

void egRender_LostDevice()
{
}
//---------------------------------------------------------------------------

void egRender_RestoreDevice()
{
}
//---------------------------------------------------------------------------

void egRender_PlaneRelease(enum BGSelect iType)
{
	SDK_ASSERT(iType != BGSELECT_NUM);
	if(gsGraphicsInit == FALSE)
	{
		return;
	}
#ifdef USE_TILEBUFFER
	if(gsPlane[iType].saveBuf.data.mpData256 != NULL)
	{
	   FREE(gsPlane[iType].saveBuf.data.mpData256);
	}
#endif
	if(gsPlane[iType].buf.data.mpData256 != NULL)
	{
	   FREE(gsPlane[iType].buf.data.mpData256);
	}
	if(gsPlane[iType].fillLine.pFillLine8 != NULL)
	{
	   FREE(gsPlane[iType].fillLine.pFillLine8);
	}
	if(gsPlane[iType].clearLine.pClearLine8 != NULL)
	{
	   FREE(gsPlane[iType].clearLine.pClearLine8);
	}
	MI_CpuClear8(&gsPlane[iType], sizeof(struct BGPlane));
	gsPlane[iType]._copyProcDef = _dummyCopyFn;
	gsPlane[iType]._copyProcCur = _dummyCopyFn;
#ifdef NITRO_SDK
	gsPlane[iType]._flushProc = _dummyFlushFn;
	gsPlane[iType]._affineProc = _dummyAffineFn;
#endif
}
//---------------------------------------------------------------------------

#ifdef NITRO_SDK
void egRender_Init()
#else
void egRender_Init(TransferFrameBufferCallback fn)
#endif
{
	gsGraphicsInit = TRUE;
	gsCurrentColor.dc = COLOR888TO1555(255, 255, 255);
	_initGraphicsPointers();
#ifndef NITRO_SDK
	gsInitCopyProcFn = fn; 
#endif
}
//---------------------------------------------------------------------------

void egRender_Release()
{
	if(gsGraphicsInit == TRUE)
	{
		egRender_PlaneRelease(BGSELECT_MAIN2);
		egRender_PlaneRelease(BGSELECT_MAIN3);
		egRender_PlaneRelease(BGSELECT_SUB2);
		egRender_PlaneRelease(BGSELECT_SUB3);
		gsGraphicsInit = FALSE;
	}
	_initGraphicsPointers();
}
//---------------------------------------------------------------------------

static void _initGraphicsPointers()
{
	MI_CpuClear8(&gsPlane[BGSELECT_MAIN2], sizeof(struct BGPlane));
	MI_CpuClear8(&gsPlane[BGSELECT_MAIN3], sizeof(struct BGPlane));
	MI_CpuClear8(&gsPlane[BGSELECT_SUB2], sizeof(struct BGPlane));
	MI_CpuClear8(&gsPlane[BGSELECT_SUB3], sizeof(struct BGPlane));

	gsPlane[BGSELECT_MAIN2]._copyProcDef = _dummyCopyFn;
	gsPlane[BGSELECT_MAIN3]._copyProcDef = _dummyCopyFn;
	gsPlane[BGSELECT_SUB2]._copyProcDef = _dummyCopyFn;
	gsPlane[BGSELECT_SUB3]._copyProcDef = _dummyCopyFn;
	gsPlane[BGSELECT_MAIN2]._copyProcCur = _dummyCopyFn;
	gsPlane[BGSELECT_MAIN3]._copyProcCur = _dummyCopyFn;
	gsPlane[BGSELECT_SUB2]._copyProcCur = _dummyCopyFn;
	gsPlane[BGSELECT_SUB3]._copyProcCur = _dummyCopyFn;
#ifdef NITRO_SDK
	gsPlane[BGSELECT_MAIN2]._flushProc = _dummyFlushFn;
	gsPlane[BGSELECT_MAIN3]._flushProc = _dummyFlushFn;
	gsPlane[BGSELECT_SUB2]._flushProc = _dummyFlushFn;   
	gsPlane[BGSELECT_SUB3]._flushProc = _dummyFlushFn;
	gsPlane[BGSELECT_MAIN2]._affineProc = _dummyAffineFn;
	gsPlane[BGSELECT_MAIN3]._affineProc = _dummyAffineFn;
	gsPlane[BGSELECT_SUB2]._affineProc = _dummyAffineFn;   
	gsPlane[BGSELECT_SUB3]._affineProc = _dummyAffineFn;
	_activeAffineProc = _dummyAffineFn;
#else
	gsInitCopyProcFn = NULL;
#endif

#ifdef USE_TILEBUFFER
	_activeSaveMemFn = _dummyVoidFn;
	_activeRestoreMemFn = _dummyRestoreFn;
#endif
	_transferToVRAM = _dummyVoidFn;

	_drawImageFn[BMP_TYPE_256][DT_NONE] = _drawImage256;
	_drawImageFn[BMP_TYPE_DC16][DT_NONE] = _drawImageDC;
	_drawImageFn[BMP_TYPE_256][DT_MIRRORX] = _drawImage256MirrorX;
	_drawImageFn[BMP_TYPE_DC16][DT_MIRRORX] = _drawImageDCMirrorX;
	_drawImageFn[BMP_TYPE_256][DT_MIRRORY] = _drawImage256MirrorY;
	_drawImageFn[BMP_TYPE_DC16][DT_MIRRORY] = _drawImageDCMirrorY;
	_drawImageToImage[BMP_TYPE_256] = _drawOpaqImageToImage256;
	_drawImageToImage[BMP_TYPE_DC16] = _drawOpaqImageToImageDC;
	_drawOpaqImageToImage[BMP_TYPE_256] = _drawOpaqImageToImage256;
	_drawOpaqImageToImage[BMP_TYPE_DC16] = _drawOpaqImageToImageDC;
	//_drawPixel[BMP_TYPE_256] = _drawPixel256;
	//_drawPixel[BMP_TYPE_DC16] = _drawPixelDC;
	_drawLine[BMP_TYPE_256] = _drawLine256;
	_drawLine[BMP_TYPE_DC16] = _drawLineDC;
	//_clearRect[BMP_TYPE_256] = _clearRect256;
	//_clearRect[BMP_TYPE_DC16] = _clearRectDC;
	_colorRect[BMP_TYPE_256] = _colorRect256;
	_colorRect[BMP_TYPE_DC16] = _colorRectDC;

	gsActivePlane = BGSELECT_NUM;
}
//---------------------------------------------------------------------------

void egRender_SetActiveBGForGraphics(enum BGSelect iActiveBG)
{
   if(gsActivePlane != iActiveBG && !egRender_IsJoinScreenMode())
   {
		SDK_ASSERT(iActiveBG != BGSELECT_NUM);
		SDK_NULL_ASSERT(gsPlane[iActiveBG].clearLine.pClearLineDC); // main screen is not initialized 
		gsActivePlane = iActiveBG;
#ifdef USE_TILEBUFFER
		_activeSaveMemFn = _saveGraphicsToMemory;
		_activeRestoreMemFn = _restoreGraphicsFromMemory;
#endif
#ifdef NITRO_SDK
		_activeAffineProc = gsPlane[iActiveBG]._affineProc;
#endif
   }
}
//---------------------------------------------------------------------------

enum BGSelect egRender_GetActiveBGForGraphics()
{
    return gsActivePlane;
}
//---------------------------------------------------------------------------

BOOL egRender_IsTransferDataVRAMChanged()
{
	return gsPlane[BGSELECT_MAIN2]._copyProcCur != _dummyCopyFn ||
			gsPlane[BGSELECT_MAIN3]._copyProcCur != _dummyCopyFn ||
			gsPlane[BGSELECT_SUB2]._copyProcCur != _dummyCopyFn ||
			gsPlane[BGSELECT_SUB3]._copyProcCur != _dummyCopyFn;
}
//---------------------------------------------------------------------------

void egRender_TransferDataToVRAM()
{
    _transferToVRAM();
}
//---------------------------------------------------------------------------

static void _transferDataToVRAMJoin()
{
	SDK_ASSERT(0); // todo: implement this function for DS
#ifdef NITRO_SDK
/*
	GXRgba *sm1, *dm1;
	GXRgba *ss1, *ds1;
	int i = 0;
	sm1 = gspBuf[gsActiveBG] + gsOffY[gsActiveBG] * gsBufSizeW + gsOffX[gsActiveBG];
	dm1 = gspVRAMPrt[gsActiveBG];
	ss1 = gspBuf[gsActiveBG] + (gsOffY[gsActiveBG] + SCREEN_HEIGHT) * gsBufSizeW + gsOffX[gsActiveBG];
	ds1 = gspVRAMPrt[gsActiveSubBG];
	do
	{   
	   _copyProc[gsActiveBG](DMA_NO, sm1, dm1, 512); //256 * sizeof(rgba)
	   _copyProc[gsActiveBG](DMA_NO, ss1, ds1, 512); //256 * sizeof(rgba)
		sm1 += gsBufSizeW;
		dm1 += 256;
		ss1 += gsBufSizeW;
		ds1 += 256;
	}while(++i != SCREEN_HEIGHT);
*/
#else
	_transferDataToVRAMSingle();
#endif
}
//---------------------------------------------------------------------------

static void _transferDataToVRAMSingle()
{
#ifdef NITRO_SDK	
	gsPlane[BGSELECT_MAIN2]._flushProc(gsPlane[BGSELECT_MAIN2].buf.data.mpDataDC16, gsPlane[BGSELECT_MAIN2].bufSizeBytes);
	gsPlane[BGSELECT_MAIN2]._copyProcCur(DMA_NO, gsPlane[BGSELECT_MAIN2].buf.data.mpDataDC16, gsPlane[BGSELECT_MAIN2].VRAMPrt.pVRAMPrtDC, gsPlane[BGSELECT_MAIN2].bufSizeBytes);
	gsPlane[BGSELECT_MAIN3]._flushProc(gsPlane[BGSELECT_MAIN3].buf.data.mpDataDC16, gsPlane[BGSELECT_MAIN3].bufSizeBytes);
	gsPlane[BGSELECT_MAIN3]._copyProcCur(DMA_NO, gsPlane[BGSELECT_MAIN3].buf.data.mpDataDC16, gsPlane[BGSELECT_MAIN3].VRAMPrt.pVRAMPrtDC, gsPlane[BGSELECT_MAIN3].bufSizeBytes);
	gsPlane[BGSELECT_SUB2]._flushProc(gsPlane[BGSELECT_SUB2].buf.data.mpDataDC16, gsPlane[BGSELECT_SUB2].bufSizeBytes);
	gsPlane[BGSELECT_SUB2]._copyProcCur(DMA_NO, gsPlane[BGSELECT_SUB2].buf.data.mpDataDC16, gsPlane[BGSELECT_SUB2].VRAMPrt.pVRAMPrtDC, gsPlane[BGSELECT_SUB2].bufSizeBytes);
	gsPlane[BGSELECT_SUB3]._flushProc(gsPlane[BGSELECT_SUB3].buf.data.mpDataDC16, gsPlane[BGSELECT_SUB3].bufSizeBytes);
	gsPlane[BGSELECT_SUB3]._copyProcCur(DMA_NO, gsPlane[BGSELECT_SUB3].buf.data.mpDataDC16, gsPlane[BGSELECT_SUB3].VRAMPrt.pVRAMPrtDC, gsPlane[BGSELECT_SUB3].bufSizeBytes);
#else
	gsPlane[BGSELECT_MAIN2]._copyProcCur(&gsPlane[BGSELECT_MAIN2].buf, gsPlane[BGSELECT_MAIN2].offX, gsPlane[BGSELECT_MAIN2].offY, gsPlane[BGSELECT_MAIN2].buf.mWidth, gsPlane[BGSELECT_MAIN2].buf.mHeight);
	gsPlane[BGSELECT_MAIN3]._copyProcCur(&gsPlane[BGSELECT_MAIN3].buf, gsPlane[BGSELECT_MAIN3].offX, gsPlane[BGSELECT_MAIN3].offY, gsPlane[BGSELECT_MAIN3].buf.mWidth, gsPlane[BGSELECT_MAIN3].buf.mHeight);
	gsPlane[BGSELECT_SUB2]._copyProcCur(&gsPlane[BGSELECT_SUB2].buf, gsPlane[BGSELECT_SUB2].offX, gsPlane[BGSELECT_SUB2].offY, gsPlane[BGSELECT_SUB2].buf.mWidth, gsPlane[BGSELECT_SUB2].buf.mHeight);
	gsPlane[BGSELECT_SUB3]._copyProcCur(&gsPlane[BGSELECT_SUB3].buf, gsPlane[BGSELECT_SUB3].offX, gsPlane[BGSELECT_SUB3].offY ,gsPlane[BGSELECT_SUB3].buf.mWidth, gsPlane[BGSELECT_SUB3].buf.mHeight);
#endif
	gsPlane[BGSELECT_MAIN2]._copyProcCur = _dummyCopyFn;
	gsPlane[BGSELECT_MAIN3]._copyProcCur = _dummyCopyFn;
	gsPlane[BGSELECT_SUB2]._copyProcCur = _dummyCopyFn;
	gsPlane[BGSELECT_SUB3]._copyProcCur = _dummyCopyFn;
#ifdef NITRO_SDK
	gsPlane[BGSELECT_MAIN2]._flushProc = _dummyFlushFn;
	gsPlane[BGSELECT_MAIN3]._flushProc = _dummyFlushFn;
	gsPlane[BGSELECT_SUB2]._flushProc = _dummyFlushFn;
	gsPlane[BGSELECT_SUB3]._flushProc = _dummyFlushFn;
#endif
}
//---------------------------------------------------------------------------

/*void egRender_SetScale(fx32 iScale)
{
#ifdef NITRO_SDK
	MtxFx22 mtx;
    fx32    rScale = FX_Inv(iScale);
    mtx._00 = rScale;
    mtx._01 = 0;
    mtx._10 = 0;
    mtx._11 = rScale;
	_activeAffineProc(&mtx, /iOriginX/0, /iOriginY/0, 0, 0);
#else
	glRender_SetScale(iScale);
#endif
}*/
//---------------------------------------------------------------------------

void egRender_SetRenderPlaneScale(fx32 val, enum BGSelect iBG)
{
	(void)val;
	(void)iBG;	
}
//---------------------------------------------------------------------------

fx32 egRender_GetRenderPlaneScale(enum BGSelect iBG)
{
	(void)iBG;
	return FX32_ONE;
}
//---------------------------------------------------------------------------

static inline s32 _getPtrAtPoint(s32 x, s32 y)
{
	return y * gsPlane[gsActivePlane].buf.mWidth2n + x;
}
//---------------------------------------------------------------------------

void egRender_ClearFrameBuffer(enum BGSelect iBG)
{
	if(iBG != BGSELECT_NUM && gsPlane[iBG].buf.data.mpData256 != NULL)
	{
		MI_CpuClearFast(gsPlane[iBG].buf.data.mpData256, gsPlane[iBG].bufSizeBytes);
		_markPlaneToVRAMTransfering(iBG);
	}
}
//---------------------------------------------------------------------------

void egRender_SaveGraphicsToMemory()
{
#ifdef USE_TILEBUFFER
    // current screen is not initialized 										
    SDK_ASSERT(gsActivePlane != BGSELECT_NUM);
    _activeSaveMemFn();
#endif
}
//---------------------------------------------------------------------------

#ifdef USE_TILEBUFFER
void _saveGraphicsToMemory()
{    
	// current screen is not initialized
    SDK_ASSERT(gsActivePlane != BGSELECT_NUM);
    SDK_NULL_ASSERT(gsPlane[gsActivePlane].saveBuf.data.mpData256); 
    SDK_NULL_ASSERT(gsPlane[gsActivePlane].buf.data.mpData256);
	if(gsPlane[gsActivePlane].buf.mWidth2n == gsPlane[gsActivePlane].saveBuf.mWidth2n)
	{
		MI_CpuCopyFast(gsPlane[gsActivePlane].buf.data.mpData256, gsPlane[gsActivePlane].saveBuf.data.mpData256, gsPlane[gsActivePlane].bufSizeBytes);
	}
	else
	{
		_drawOpaqImageToImage[gsPlane[gsActivePlane].buf.mType](&gsPlane[gsActivePlane].buf, 0, 0, 
					gsPlane[gsActivePlane].saveBuf.mWidth2n, gsPlane[gsActivePlane].saveBuf.mHeight2n, &gsPlane[gsActivePlane].saveBuf, 0, 0);
	}
}
#endif
//---------------------------------------------------------------------------

void egRender_RestoreGraphicsFromMemory(fx32 IOffX, fx32 IOffY)
{
#ifdef USE_TILEBUFFER
	// current screen is not initialized										
    SDK_ASSERT(gsActivePlane != BGSELECT_NUM);
    _activeRestoreMemFn(IOffX >> FX32_SHIFT, IOffY >> FX32_SHIFT);
#else
	(void)IOffX;
	(void)IOffY;
#endif
}
//---------------------------------------------------------------------------

#ifdef USE_TILEBUFFER
static void _restoreGraphicsFromMemory(s32 iX, s32 iY)
{
    s32 sw, dw, sh, dh, dx, dy;
    if(gsPlane[gsActivePlane].saveBuf.data.mpData256 == NULL)
    {
        return;
    }
    //screen is not initialized
    SDK_ASSERT(gsActivePlane != BGSELECT_NUM);
    SDK_NULL_ASSERT(gsPlane[gsActivePlane].buf.data.mpData256);
    if(iX == 0 && iY == 0 && gsPlane[gsActivePlane].buf.mWidth2n == gsPlane[gsActivePlane].saveBuf.mWidth2n)
    {
    	MI_CpuCopyFast(gsPlane[gsActivePlane].saveBuf.data.mpData256, gsPlane[gsActivePlane].buf.data.mpData256, gsPlane[gsActivePlane].bufSizeBytes);
		_markPlaneToVRAMTransfering(gsActivePlane);
		return;
    }
    dx = dy = 0;
	sw = dw = gsPlane[gsActivePlane].saveBuf.mWidth;
	sh = dh = gsPlane[gsActivePlane].saveBuf.mHeight;
    if(gxHelper_preDrawImage(dw, dh, &iX, &iY, &gsPlane[gsActivePlane].saveBuf, &dx, &dy, &sw, &sh))
    {
		_drawOpaqImageToImage[gsPlane[gsActivePlane].saveBuf.mType](&gsPlane[gsActivePlane].saveBuf, iX, iY, sw, sh, &gsPlane[gsActivePlane].buf, dx, dy);
		_markPlaneToVRAMTransfering(gsActivePlane);
    }
}
#endif
//---------------------------------------------------------------------------

void egRender_SetColor(GXRgba iColor)
{
   gsCurrentColor.dc = iColor;
}
//---------------------------------------------------------------------------

void egRender_DrawLine(fx32 x1, fx32 y1, fx32 x2, fx32 y2)
{	
	 // current screen is not initialized
    SDK_ASSERT(gsActivePlane != BGSELECT_NUM);
	if(gxHelper_preDrawLine(FX32(gsPlane[gsActivePlane].buf.mWidth), FX32(gsPlane[gsActivePlane].buf.mHeight), 
								&x1, &y1, &x2, &y2))
	{
		_drawLine[gsPlane[gsActivePlane].buf.mType](fx2int(x1), fx2int(y1), fx2int(x2), fx2int(y2));
	}
}
//---------------------------------------------------------------------------

static void _drawLineDC(s32 x1, s32 y1, s32 x2, s32 y2)
{
     s32 incX, incY, dx, dy;

	 if(gsCurrentColor.dc == TRANSPARENT_COLOR_DC)
	 {
	   return;
	 }	 
	 
     dx = x2 - x1;
     dy = y2 - y1;
     if(dx == 0 && dy == 0)
     {
	 	_markPlaneToVRAMTransfering(gsActivePlane);
		gsPlane[gsActivePlane].buf.data.mpDataDC16[_getPtrAtPoint(x1, y1)] = gsCurrentColor.dc;
     	return;
	 }
	 
     if(MATH_ABS(dx) > MATH_ABS(dy))
     {
        if(y1 < y2)
        {
            incY = MATH_ABS((dy << 10) / dx);
        }
        else
        {
            incY = -MATH_ABS((dy << 10) / dx);
        }
        if(x1 < x2)
        {
            incX = 1;
        }
        else
        {
            incX = -1;
        }
        y1 <<= 10;
        y1 += 1 << 9;
        do
        {
        	gsPlane[gsActivePlane].buf.data.mpDataDC16[_getPtrAtPoint(x1, y1 >> 10)] = gsCurrentColor.dc;
        	x1 += incX;
        	y1 += incY;
        }
        while(x1 != x2);
     }
     else
     {
        if(x1 < x2)
        {
            incX = MATH_ABS((dx << 10) / dy);
        }
        else
        {
            incX = -MATH_ABS((dx << 10) / dy);
        }
        if(y1 < y2)
        {
            incY = 1;
        }
        else
        {
            incY = -1;
        }
        x1 <<= 10;
		x1 += 1 << 9;
        do
        {
           gsPlane[gsActivePlane].buf.data.mpDataDC16[_getPtrAtPoint(x1 >> 10, y1)] = gsCurrentColor.dc;
           x1 += incX;
           y1 += incY;
        }
        while(y1 != y2);
	 }
	 _markPlaneToVRAMTransfering(gsActivePlane);
}
//---------------------------------------------------------------------------

static void _drawLine256(s32 x1, s32 y1, s32 x2, s32 y2)
{
     s32 incX, incY, dx, dy;
     	
	 if(gsCurrentColor.index == TRANSPARENT_COLOR_256)
	 {
	   return;
	 }
	 
     dx = x2 - x1;
     dy = y2 - y1;
     if(dx == 0 && dy == 0)
     {
	 	_markPlaneToVRAMTransfering(gsActivePlane);
		gsPlane[gsActivePlane].buf.data.mpData256[_getPtrAtPoint(x1, y1)] = gsCurrentColor.index;
     	return;
	 }
	 
     if(MATH_ABS(dx) > MATH_ABS(dy))
     {
        if(y1 < y2)
        {
            incY = MATH_ABS((dy << 10) / dx);
        }
        else
        {
            incY = -MATH_ABS((dy << 10) / dx);
        }
        if(x1 < x2)
        {
            incX = 1;
        }
        else
        {
            incX = -1;
        }
        y1 <<= 10;
        y1 += 1 << 9;
        do
        {
        	gsPlane[gsActivePlane].buf.data.mpData256[_getPtrAtPoint(x1, y1 >> 10)] = gsCurrentColor.index;
        	x1 += incX;
        	y1 += incY;
        }
        while(x1 != x2);
     }
     else
     {
        if(x1 < x2)
        {
            incX = MATH_ABS((dx << 10) / dy);
        }
        else
        {
            incX = -MATH_ABS((dx << 10) / dy);
        }
        if(y1 < y2)
        {
            incY = 1;
        }
        else
        {
            incY = -1;
        }
        x1 <<= 10;
		x1 += 1 << 9;
        do
        {
           gsPlane[gsActivePlane].buf.data.mpData256[_getPtrAtPoint(x1 >> 10, y1)] = gsCurrentColor.index;
           x1 += incX;
           y1 += incY;
        }
        while(y1 != y2);
	 }
	 _markPlaneToVRAMTransfering(gsActivePlane);
}
//---------------------------------------------------------------------------

static inline void *_defCopyFn(u32 p1, u32 p2, s32 w)
{
#ifdef NITRO_SDK
	if((p1 & 3) == 0 && (p2 & 3) == 0 && (w & 3) == 0)
	{
		if(w >= 0x20)
		{
			return MI_CpuCopyFast;	
		}
		else
		{
			return MI_CpuCopy32;
		}
	}
	else
	{
		if((p1 & 1) == 0 && (p2 & 1) == 0 && (w & 1) == 0)
		{
			return MI_CpuCopy16;
		}
		else
		{
			return MI_CpuCopy8;
		}
	}
	SDK_ASSERT(0); //something wrong
#else
	(void)p1;
	(void)p2;
	(void)w;
	return (void*)MI_CpuCopy8;
#endif
}
//---------------------------------------------------------------------------

/*void egRender_ClearRect(s32 iX, s32 iY, s32 iWidth, s32 iHeight)
{
	 // current screen is not initialized
    SDK_ASSERT(gsActivePlane != BGSELECT_NUM);
	 _clearRect[gsPlane[gsActivePlane].buf.mType](iX, iY, iWidth, iHeight);
}
//---------------------------------------------------------------------------

void _clearRectDC(s32 iX, s32 iY, s32 iWidth, s32 iHeight)
{
	if(gxHelper_preDrawSquare(gsPlane[gsActivePlane].buf.mWidth, gsPlane[gsActivePlane].buf.mHeight, &iX, &iY, &iWidth, &iHeight))
    {
	    u32 sd1;
	    GXRgba* dbuf;
		void (*copyProc)(const void* src, void* dest, u32 size);
		u32 p1 = (u32)gsPlane[gsActivePlane].buf.data.mpDataDC16 + iY * gsPlane[gsActivePlane].buf.mWidth2n + iX;
		u32 p2 = (u32)gsPlane[gsActivePlane].pClearLineDC;
		sd1 = iWidth * sizeof(GXRgba);
		copyProc = (void (*)(const void* src, void* dest, u32 size))_defCopyFn(p1, p2, iWidth);
		--iY;
		do
		{
			dbuf = gsPlane[gsActivePlane].buf.data.mpDataDC16 + (iY + iHeight) * gsPlane[gsActivePlane].buf.mWidth2n + iX;
			copyProc(gsPlane[gsActivePlane].pClearLineDC, dbuf, sd1);
		}
		while(--iHeight > 0);
		_markPlaneToVRAMTransfering(gsActivePlane);
     }
}
//---------------------------------------------------------------------------

void _clearRect256(s32 iX, s32 iY, s32 iWidth, s32 iHeight)
{
    if(gxHelper_preDrawSquare(gsPlane[gsActivePlane].buf.mWidth, gsPlane[gsActivePlane].buf.mHeight, &iX, &iY, &iWidth, &iHeight))
    {
	    u8* dbuf;
		void (*copyProc)(const void* src, void* dest, u32 size);
		u32 p1 = (u32)gsPlane[gsActivePlane].buf.data.mpData256 + iY * gsPlane[gsActivePlane].buf.mWidth2n + iX;
		u32 p2 = (u32)gsPlane[gsActivePlane].pClearLine8;
		copyProc = (void (*)(const void* src, void* dest, u32 size))_defCopyFn(p1, p2, iWidth);
         --iY;
		do
		{
			dbuf = gsPlane[gsActivePlane].buf.data.mpData256 + (iY + iHeight) * gsPlane[gsActivePlane].buf.mWidth2n + iX;
			copyProc(gsPlane[gsActivePlane].pClearLine8, dbuf, (u32)iWidth);
		}
		while(--iHeight > 0);
		_markPlaneToVRAMTransfering(gsActivePlane);
     }
}*/
//---------------------------------------------------------------------------

void egRender_ColorRect(fx32 iX, fx32 iY, fx32 iWidth, fx32 iHeight)
{
	 // current screen is not initialized
    SDK_ASSERT(gsActivePlane != BGSELECT_NUM);
	if(gxHelper_preDrawSquare(FX32(gsPlane[gsActivePlane].buf.mWidth), FX32(gsPlane[gsActivePlane].buf.mHeight),
								&iX, &iY, &iWidth, &iHeight))
	{
		const s32 width = fx2int(iWidth);
		const s32 height = fx2int(iHeight);
		if(width == 0 || height == 0)
		{
			return;
		}
		_colorRect[gsPlane[gsActivePlane].buf.mType](fx2int(iX), fx2int(iY), width, height);
	}
}
//---------------------------------------------------------------------------

void _colorRectDC(s32 iX, s32 iY, s32 iWidth, s32 iHeight)
{
	u32 sd1, p1, p2;
	GXRgba* dbuf;
	void (*copyProc)(const void* src, void* dest, u32 size);
	if(gsCurrentColor.dc == TRANSPARENT_COLOR_DC)
	{
		return;
	}
	p1 = (u32)gsPlane[gsActivePlane].buf.data.mpDataDC16 + iY * gsPlane[gsActivePlane].buf.mWidth2n + iX;
	p2 = (u32)gsPlane[gsActivePlane].fillLine.pFillLineDC;
	sd1 = iWidth * sizeof(GXRgba);
	copyProc = (void (*)(const void* src, void* dest, u32 size))_defCopyFn(p1, p2, iWidth);
	if(gsPlane[gsActivePlane].fillLine.pFillLineDC[0] != gsCurrentColor.dc || gsPlane[gsActivePlane].fillLineSize != sd1)
	{
		MI_CpuFill16(gsPlane[gsActivePlane].fillLine.pFillLineDC, gsCurrentColor.dc, sd1);
		gsPlane[gsActivePlane].fillLineSize = sd1;
	}
	--iY;
	do
	{
		dbuf = gsPlane[gsActivePlane].buf.data.mpDataDC16 + (iY + iHeight) * gsPlane[gsActivePlane].buf.mWidth2n + iX;
		copyProc(gsPlane[gsActivePlane].fillLine.pFillLineDC, dbuf, sd1);
	}
	while(--iHeight > 0);
	_markPlaneToVRAMTransfering(gsActivePlane);
}
//---------------------------------------------------------------------------

void _colorRect256(s32 iX, s32 iY, s32 iWidth, s32 iHeight)
{
    u8* dbuf;
    u32 p1, p2;
	void (*copyProc)(const void* src, void* dest, u32 size);
	if(gsCurrentColor.dc == TRANSPARENT_COLOR_256)
	{
		return;
	}
	p1 = (u32)gsPlane[gsActivePlane].buf.data.mpData256 + iY * gsPlane[gsActivePlane].buf.mWidth2n + iX;
	p2 = (u32)gsPlane[gsActivePlane].fillLine.pFillLine8;
	copyProc = (void (*)(const void* src, void* dest, u32 size))_defCopyFn(p1, p2, iWidth);
	if(gsPlane[gsActivePlane].fillLine.pFillLine8[0] != gsCurrentColor.index || gsPlane[gsActivePlane].fillLineSize != (u32)iWidth)
	{
		MI_CpuFill8(gsPlane[gsActivePlane].fillLine.pFillLine8, gsCurrentColor.index, iWidth);
		gsPlane[gsActivePlane].fillLineSize = iWidth;
	}
	 --iY;
	do
	{
		dbuf = gsPlane[gsActivePlane].buf.data.mpData256 + (iY + iHeight) * gsPlane[gsActivePlane].buf.mWidth2n + iX;
		copyProc(gsPlane[gsActivePlane].fillLine.pFillLine8, dbuf, (u32)iWidth);
	}
	while(--iHeight > 0);
	_markPlaneToVRAMTransfering(gsActivePlane);
}
//---------------------------------------------------------------------------

void egRender_DrawImage(const struct DrawImageFunctionData* data)
{
	s32 iScreenX, iScreenY, iSrcX, iSrcY, iSrcW, iSrcH;	
	iScreenX = fx2int(data->mX);
	iScreenY = fx2int(data->mY);
	iSrcX = data->mSrcSizeData[SRC_DATA_X];
	iSrcY = data->mSrcSizeData[SRC_DATA_Y];
	iSrcW = data->mSrcSizeData[SRC_DATA_W];
	iSrcH = data->mSrcSizeData[SRC_DATA_H];
    SDK_ASSERT(gsActivePlane != BGSELECT_NUM); // current screen is not initialized
	if(gxHelper_preDrawImage(gsPlane[gsActivePlane].buf.mWidth, gsPlane[gsActivePlane].buf.mHeight,
									&iScreenX, &iScreenY, data->mpSrcData, &iSrcX, &iSrcY, &iSrcW, &iSrcH))
	{
		SDK_ASSERT(data->mpSrcData->mType <= BMP_TYPE_DC16);
		_drawImageFn[data->mpSrcData->mType][DT_NONE](iScreenX, iScreenY, data->mpSrcData, iSrcX, iSrcY, iSrcW, iSrcH, data->mFillColor);
		_markPlaneToVRAMTransfering(gsActivePlane);
    }
}
//---------------------------------------------------------------------------

void egRender_DrawImageToImage(const struct BMPImage *pSrcImage, 
                            s32 iSrcX, s32 iSrcY,
                            s32 iWidth, s32 iHeight,
                            struct BMPImage *pDestImage, s32 iDestX, s32 iDestY)
{
	// current screen is not initialized
    SDK_ASSERT(gsActivePlane != BGSELECT_NUM);
	SDK_ASSERT(pSrcImage->mType == pDestImage->mType);
	if(gxHelper_preDrawImage(pDestImage->mWidth, pDestImage->mHeight, &iDestX, &iDestY, pSrcImage, &iSrcX, &iSrcY, &iWidth, &iHeight))
	{
		_drawImageToImage[pSrcImage->mType](pSrcImage, iSrcX, iSrcY, iWidth, iHeight, pDestImage, iDestX, iDestY);
	}
}
//---------------------------------------------------------------------------

//android
//r=     1111100000000000
//g=          11111000000 = 0x7C0 (0x0000ff00)
//       1111100000000000 = 0xF800 (0x00ff0000)
//b=               111110
//rb=    1111100000111110 = 0xF83E (0x00ff00ff)
//  111110000011111000000 = 0x1F07C0
//       0000100001000010 = 0x842 (компенсация 0)

//ds
//r=     0111110000000000 = 0x7C00
//g=           1111100000 = 0x3E0 (0x0000ff00)
//       0111110000000000 = 0x7C00 (0x00ff0000)
//b=                11111 = 0x1F
//rb=    0111110000011111 = 0x7C1F (0x00ff00ff)
//  011111000001111100000 = 0xF83E0
//       0000010000100001 = 0x421 (компенсация 0)
#if defined NITRO_SDK || defined KOLIBRIOS_APP	
#define BlendingMacro()																			\
			{																					\
				if(sclr != TRANSPARENT_COLOR_DC)												\
				{																				\
					if(pSrcData->mA5DataSize != 0)												\
					{																			\
						a = (alpha * ((*abuf >> ap) & 0x1f)) / 0x1f;							\
					}																			\
					rb = (((sclr & 0x7C1F) * a) + ((*dbuf & 0x7C1F) * (0x1f - a))) & 0xF83E0;	\
					g = (((sclr & 0x3E0) * a) + ((*dbuf & 0x3E0) * (0x1f - a))) & 0x7C00;		\
					*dbuf = (GXRgba)(((sclr & 0x1) | 0x8000 | ((rb | g) >> 5)) + 0x421);			\
				}																				\
			}
#else
#define BlendingMacro()																			\
			{																					\
				if(sclr != TRANSPARENT_COLOR_DC)												\
				{																				\
					if(pSrcData->mA5DataSize != 0)												\
					{																			\
						a = (alpha * ((*abuf >> ap) & 0x1f)) / 0x1f;							\
					}																			\
					rb = (((sclr & 0xF83E) * a) + ((*dbuf & 0xF83E) * (0x1f - a))) & 0x1F07C0;	\
					g = (((sclr & 0x7C0) * a) + ((*dbuf & 0x7C0) * (0x1f - a))) & 0xF800;		\
					*dbuf = (GXRgba)(((sclr & 0x1) | ((rb | g) >> 5)) + 0x842);					\
				}																				\
			}
#endif
//-------------------------------------------------------------------------------------------

static s32 _intMin(s32 v1, s32 v2)
{
	return v2 ^ ((v1 ^ v2) & -(v1 < v2));
}
//---------------------------------------------------------------------------

void _drawImageDC(s32 iScreenX, s32 iScreenY, const struct BMPImage* pSrcData,
							s32 iSrcX, s32 iSrcY, s32 iSrcW, s32 iSrcH, const u32 data)
{
	GXRgba* wptr;
    GXRgba* sbuf;
	GXRgba* dbuf;
	const u32 alpha = data >> 16;
	const GXRgb fillColor = data & 0xffff;
	if(alpha == 0x1f && pSrcData->mA5DataSize == 0 && fillColor == GX_RGBA(0x1f, 0x1f, 0x1f, 1))
	{
		do
		{
			sbuf = pSrcData->data.mpDataDC16 + iSrcY * pSrcData->mWidth2n + iSrcX;
			dbuf = gsPlane[gsActivePlane].buf.data.mpDataDC16 + iScreenY * gsPlane[gsActivePlane].buf.mWidth2n + iScreenX;
			wptr = sbuf + iSrcW;
			do
			{
				if(*sbuf != TRANSPARENT_COLOR_DC)
				{
					*dbuf = *sbuf;
				}
				++dbuf;
			}
			while(++sbuf < wptr);
			++iScreenY;
			++iSrcY;
		}
		while(--iSrcH > 0);
	}
	else
	{
		u32 rb, g;
		u32 a = alpha;
		if(fillColor == GX_RGBA(0x1f ,0x1f ,0x1f, 1))
		{
			do
			{
				s32 ap = iSrcY * pSrcData->mWidth + iSrcX;
				u16* abuf = pSrcData->mpA5Data + ap / 3;
				sbuf = pSrcData->data.mpDataDC16 + iSrcY * pSrcData->mWidth2n + iSrcX;
				dbuf = gsPlane[gsActivePlane].buf.data.mpDataDC16 + iScreenY * gsPlane[gsActivePlane].buf.mWidth2n + iScreenX;
				ap = 11 - (ap % 3) * 5;
				wptr = sbuf + iSrcW;
				do
				{
					const GXRgba sclr = *sbuf;
					BlendingMacro();
					++dbuf;
					ap -= 5;
					if(ap < 0)
					{
						++abuf;
						ap = 11;
					}
				}
				while(++sbuf < wptr);
				++iScreenY;
				++iSrcY;
			}
			while(--iSrcH > 0);
		}
		else
		{
			const s32 cr = (fillColor & GX_RGBA_R_MASK) >> GX_RGBA_R_SHIFT;
			const s32 cg = (fillColor & GX_RGBA_G_MASK) >> GX_RGBA_G_SHIFT;
			const s32 cb = (fillColor & GX_RGBA_B_MASK) >> GX_RGBA_B_SHIFT;
			do
			{
				s32 ap = iSrcY * pSrcData->mWidth + iSrcX;
				u16* abuf = pSrcData->mpA5Data + ap / 3;
				sbuf = pSrcData->data.mpDataDC16 + iSrcY * pSrcData->mWidth2n + iSrcX;
				dbuf = gsPlane[gsActivePlane].buf.data.mpDataDC16 + iScreenY * gsPlane[gsActivePlane].buf.mWidth2n + iScreenX;
				ap = 11 - (ap % 3) * 5;
				wptr = sbuf + iSrcW;
				do
				{
					GXRgba sclr = *sbuf;
					const s32 s_cr = 0x1f - ((sclr & GX_RGBA_R_MASK) >> GX_RGBA_R_SHIFT);
					const s32 s_cg = 0x1f - ((sclr & GX_RGBA_G_MASK) >> GX_RGBA_G_SHIFT);
					const s32 s_cb = 0x1f - ((sclr & GX_RGBA_B_MASK) >> GX_RGBA_B_SHIFT);
					sclr = GX_RGBA(_intMin(s_cr + cr, 0x1f), _intMin(s_cg + cg, 0x1f), _intMin(s_cb + cb, 0x1f), 1);
					BlendingMacro();
					++dbuf;
					ap -= 5;
					if(ap < 0)
					{
						++abuf;
						ap = 11;
					}
				}
				while(++sbuf < wptr);
				++iScreenY;
				++iSrcY;
			}
			while(--iSrcH > 0);
		}
    }
}
//---------------------------------------------------------------------------

static void _drawImage256(s32 iScreenX, s32 iScreenY, const struct BMPImage* pSrcData,
								s32 iSrcX, s32 iSrcY, s32 iSrcW, s32 iSrcH, const u32 data)
{
    u8* wptr; 
    u8* sbuf;
    u8* dbuf;
 	(void)data;
    do
    {
        sbuf = pSrcData->data.mpData256 + iSrcY * pSrcData->mWidth2n + iSrcX; 
        dbuf = gsPlane[gsActivePlane].buf.data.mpData256 + iScreenY * gsPlane[gsActivePlane].buf.mWidth2n + iScreenX; 
    	wptr = sbuf + iSrcW;
    	do
    	{	
			if(*sbuf != TRANSPARENT_COLOR_256)
			{
				*dbuf = *sbuf;
            }
			++dbuf;
    	}
    	while(++sbuf < wptr);
    	++iScreenY;
    	++iSrcY;
    }
    while(--iSrcH > 0);
}
//---------------------------------------------------------------------------

void _drawImageDCMirrorX(s32 iScreenX, s32 iScreenY, const struct BMPImage* pSrcData,
							s32 iSrcX, s32 iSrcY, s32 iSrcW, s32 iSrcH, const u32 data)
{
	(void)iScreenX;
	(void)iScreenY;
	(void)pSrcData;
	(void)iSrcX;
	(void)iSrcY;
	(void)iSrcW;
	(void)iSrcH;
	(void)data;
	SDK_ASSERT(0);
/*
	GXRgba* wptr;
	GXRgba* sbuf;
	GXRgba* dbuf;
	u32 rb, g;
	const u32 alpha = data >> 16;
	const GXRgb fillColor = data & 0xffff;
	if(alpha == 0x1f && pSrcData->mA5DataSize == 0)
	{
		do
		{
			sbuf = pSrcData->data.mpDataDC16 + iSrcY * pSrcData->mWidth2n + iSrcX + iSrcW - 1;
			dbuf = gsPlane[gsActivePlane].buf.data.mpDataDC16 + iScreenY * gsPlane[gsActivePlane].buf.mWidth2n + iScreenX;
			wptr = sbuf - iSrcW;
			do
			{
				if(*sbuf != TRANSPARENT_COLOR_DC)
				{
					*dbuf = *sbuf;
				}
				++dbuf;
			}
			while(--sbuf > wptr);
			++iScreenY;
			++iSrcY;
		}
		while(--iSrcH > 0);
	}
	else
	{
		u32 a = alpha;
		do
		{
			s32 ap = iSrcY * pSrcData->mWidth + iSrcX + iSrcW - 1;
			u16* abuf = pSrcData->mpA5Data + ap / 3;
			sbuf = pSrcData->data.mpDataDC16 + iSrcY * pSrcData->mWidth2n + iSrcX + iSrcW - 1;
			dbuf = gsPlane[gsActivePlane].buf.data.mpDataDC16 + iScreenY * gsPlane[gsActivePlane].buf.mWidth2n + iScreenX;
			wptr = sbuf - iSrcW;
			ap = 11 - (ap % 3) * 5;
			do
			{
				const GXRgba sclr = *sbuf;
				BlendingMacro();
				++dbuf;
				ap += 5;
				if(ap == 16)
				{
					--abuf;
					ap = 1;
				}
			}
			while(--sbuf > wptr);
			++iScreenY;
			++iSrcY;
		}
		while(--iSrcH > 0);
    }
*/
}
//---------------------------------------------------------------------------

void _drawImage256MirrorX(s32 iScreenX, s32 iScreenY, const struct BMPImage* pSrcData,
							s32 iSrcX, s32 iSrcY, s32 iSrcW, s32 iSrcH, const u32 data)
{
	(void)iScreenX;
	(void)iScreenY;
	(void)pSrcData;
	(void)iSrcX;
	(void)iSrcY;
	(void)iSrcW;
	(void)iSrcH;
	(void)data;
	SDK_ASSERT(0);
}
//---------------------------------------------------------------------------

void _drawImageDCMirrorY(s32 iScreenX, s32 iScreenY, const struct BMPImage* pSrcData,
							s32 iSrcX, s32 iSrcY, s32 iSrcW, s32 iSrcH, const u32 data)
{
	(void)iScreenX;
	(void)iScreenY;
	(void)pSrcData;
	(void)iSrcX;
	(void)iSrcY;
	(void)iSrcW;
	(void)iSrcH;
	(void)data;
	SDK_ASSERT(0);
/*
	GXRgba* wptr;
	GXRgba* sbuf;
	GXRgba* dbuf;
	u32 rb, g;
	const u32 alpha = data >> 16;
	const GXRgb fillColor = data & 0xffff;
	iSrcY += iSrcH - 1;
	if(alpha == 0x1f && pSrcData->mA5DataSize == 0)
	{
		do
		{
			sbuf = pSrcData->data.mpDataDC16 + iSrcY * pSrcData->mWidth2n + iSrcX;
			dbuf = gsPlane[gsActivePlane].buf.data.mpDataDC16 + iScreenY * gsPlane[gsActivePlane].buf.mWidth2n + iScreenX;
			wptr = sbuf + iSrcW;
			do
			{
				if(*sbuf != TRANSPARENT_COLOR_DC)
				{
					*dbuf = *sbuf;
				}
				++dbuf;
			}
			while(++sbuf < wptr);
			++iScreenY;
			--iSrcY;
		}
		while(--iSrcH > 0);
	}
	else
	{
		u32 a = alpha;
		do
		{
			s32 ap = iSrcY * pSrcData->mWidth + iSrcX;
			u16* abuf = pSrcData->mpA5Data + ap / 3;
			sbuf = pSrcData->data.mpDataDC16 + iSrcY * pSrcData->mWidth2n + iSrcX;
			dbuf = gsPlane[gsActivePlane].buf.data.mpDataDC16 + iScreenY * gsPlane[gsActivePlane].buf.mWidth2n + iScreenX;
			wptr = sbuf + iSrcW;
			ap = 11 - (ap % 3) * 5;
			do
			{
				const GXRgba sclr = *sbuf;
				BlendingMacro();
				++dbuf;
				ap -= 5;
				if(ap < 0)
				{
					++abuf;
                    ap = 11;
				}
			}
			while(++sbuf < wptr);
			++iScreenY;
			--iSrcY;
		}
		while(--iSrcH > 0);
    }
*/
}
//---------------------------------------------------------------------------

void _drawImage256MirrorY(s32 iScreenX, s32 iScreenY, const struct BMPImage* pSrcData,
							s32 iSrcX, s32 iSrcY, s32 iSrcW, s32 iSrcH, const u32 data)
{
	(void)iScreenX;
	(void)iScreenY;
	(void)pSrcData;
	(void)iSrcX;
	(void)iSrcY;
	(void)iSrcW;
	(void)iSrcH;
	(void)data;
	SDK_ASSERT(0);
}
//---------------------------------------------------------------------------

/*void _drawImageToImageDC(const struct BMPImage *pSrcImage,
						s32 iSrcX, s32 iSrcY, s32 iWidth, s32 iHeight,
						struct BMPImage *pDestImage, s32 iDestX, s32 iDestY)
{
	GXRgba* wptr;
	GXRgba* sbuf;
	GXRgba* dbuf;
	do
	{
		sbuf = pSrcImage->data.mpDataDC16 + iSrcY * pSrcImage->mWidth2n + iSrcX;
		dbuf = pDestImage->data.mpDataDC16 + iDestY * pDestImage->mWidth2n + iDestX;
		wptr = sbuf + iWidth;
		do
		{
			if(*sbuf != TRANSPARENT_COLOR_DC)
			{
				*dbuf = *sbuf;
			}
			++dbuf;
		}
		while( ++sbuf < wptr );
		++iDestY;
		++iSrcY;
	}
	while( --iHeight > 0 );
}*/
//---------------------------------------------------------------------------

/*void _drawImageToImage256(const struct BMPImage *pSrcImage,
						s32 iSrcX, s32 iSrcY, s32 iWidth, s32 iHeight,
						struct BMPImage *pDestImage, s32 iDestX, s32 iDestY)
{
	(void)pSrcImage;
	(void)iSrcX;
	(void)iSrcY;
	(void)iWidth;
	(void)iHeight;
	(void)pDestImage;
	(void)iDestX;
	(void)iDestY;
	SDK_ASSERT(0);
}*/
//---------------------------------------------------------------------------

static void _drawOpaqImageToImageDC(const struct BMPImage *pSrcImage, 
                        s32 iSrcX, s32 iSrcY, s32 iWidth, s32 iHeight, 
                        struct BMPImage *pDestImage, s32 iDestX, s32 iDestY)
{
	u32 size;
	void (*copyProc)(const void* src, void* dest, u32 size);
	size = (u32)iWidth << 1;
	if( (((u32)pSrcImage->data.mpDataDC16 + iSrcY * iWidth + iSrcX) & 3) == 0 &&
		(((u32)pDestImage->data.mpDataDC16 + iDestY * pDestImage->mWidth2n + iDestX) & 3) == 0 &&
		(size & 3) == 0 )
	{
		if(size >= 0x20)
		{
			copyProc = MI_CpuCopyFast;
		}
		else
		{
			copyProc = MI_CpuCopy32;
		}
	}
	else
	{
		copyProc = MI_CpuCopy16;
	}
	do
	{
		copyProc(pSrcImage->data.mpDataDC16 + iSrcY * pSrcImage->mWidth2n + iSrcX,
					pDestImage->data.mpDataDC16 + iDestY * pDestImage->mWidth2n + iDestX, size);
		++iDestY;
		++iSrcY;
	}
	while(--iHeight > 0);
}
//---------------------------------------------------------------------------

static void _drawOpaqImageToImage256(const struct BMPImage *pSrcImage, 
                        s32 iSrcX, s32 iSrcY, s32 iWidth, s32 iHeight, 
                        struct BMPImage *pDestImage, s32 iDestX, s32 iDestY)
{
	void (*copyProc)(const void* src, void* dest, u32 size);
	u32 p1 = (u32)pSrcImage->data.mpData256 + iSrcY * iWidth + iSrcX;
	u32 p2 = (u32)pDestImage->data.mpData256 + iDestY * pDestImage->mWidth2n + iDestX;	
	copyProc = (void (*)(const void* src, void* dest, u32 size))_defCopyFn(p1, p2, iWidth);
	do
	{
		copyProc(pSrcImage->data.mpData256 + iSrcY * pSrcImage->mWidth2n + iSrcX, 
					pDestImage->data.mpData256 + iDestY * pDestImage->mWidth2n + iDestX, (u32)iWidth);
		++iDestY;
		++iSrcY;
	}
	while(--iHeight > 0);
}
//---------------------------------------------------------------------------
#endif