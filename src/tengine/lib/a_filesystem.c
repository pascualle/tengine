/*
tengine - cross-platform game engine
------------------------------------

Copyright (c) 2013 Papa Pascualle
Contact Email: pascualle@gmail.com

Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
*/

#include "system.h"
#include "filesystem.h"
#include "filesystem_low.h"

#ifdef ANDROID_NDK

#include "a_filesystem.h"
#include "texts.h"
#ifdef NDK_NATIVE_API10
#include "android_native_app_glue.h"
#endif

#define CUSTOM_PKG_ACCESS

//---------------------------------------------------------------------------

#ifdef CUSTOM_PKG_ACCESS

#ifdef NDK_NATIVE_API10

static AAssetManager* gpsAssetManager = NULL;
static AAsset* gspFileHandler[MAX_OPENFILES];
#define RES_PATH "data/"
#define RES_PATH_LEN 5
#else

#include "zip.h"
#include "zipint.h" 

static struct zip *gpsZipPKG = NULL;
static struct zip_file* gspFileHandler[MAX_OPENFILES];
static int gsFileIndex[MAX_OPENFILES];

#define RES_PATH "assets/data/"
#define RES_PATH_LEN 12

#endif //NDK_NATIVE_API10

//-------------------------------------------------------------------------------------------

static BOOL gsFileHandlerState[MAX_OPENFILES] = {0};

extern BOOL gsFilesystemError;

static BOOL _androidFS_checkErrors(void);

//---------------------------------------------------------------------------

BOOL _androidFS_checkErrors()
{
#ifdef NDK_NATIVE_API10
	if(gpsAssetManager == NULL)
#else
	if(gpsZipPKG == NULL)
#endif
	{
		OS_Warning("Please init file system\n");
		return FALSE;
	}
	return TRUE;
}
//---------------------------------------------------------------------------

void androidFS_InitFileSystem(struct InitFileSystemData* data)
{
#ifdef NDK_NATIVE_API10
	if(gpsAssetManager != NULL)
	{
		s16 i = MAX_OPENFILES;
		while(i > 0)
		{
			i--;
			SDK_ASSERT(gsFileHandlerState[i] == FALSE);
		}
	}
	gpsAssetManager = data->mpAssetManager;
	OS_Printf("init file system");
#else
	int error;
	SDK_ASSERT(gpsZipPKG == NULL);
	jreleaseFileSystem();
	gpsZipPKG = zip_open(data->mpPath, 0, &error);
	if(gpsZipPKG == NULL)
	{
		OS_Warning("Failed to open apk: %i\n", error);
	}
	OS_Printf("init file system with path: %s\n", pkgname);
#endif
	gsFilesystemError = FALSE;
}
//---------------------------------------------------------------------------

void androidFS_ReleaseFileSystem()
{
	androidFS_lostDevice();
#ifdef NDK_NATIVE_API10
	if(gpsAssetManager)
#else
	if(gpsZipPKG)
#endif
	{
#ifdef NDK_NATIVE_API10
		gpsAssetManager = NULL;
#else
		zip_close(gpsZipPKG);
		gpsZipPKG = NULL;
#endif
	}
}
//---------------------------------------------------------------------------

void androidFS_lostDevice()
{
	s16 i = MAX_OPENFILES;
	while(i > 0)
	{
		i--;
		if(gsFileHandlerState[i])
		{
#ifdef NDK_NATIVE_API10
			AAsset_close(gspFileHandler[i]);
#else
			zip_fclose(gspFileHandler[i]);
#endif
			gsFileHandlerState[i] = FALSE;
		}
	}
}
//-------------------------------------------------------------------------------------------

BOOL androidFS_IsFileSystemInit(void)
{
#ifdef NDK_NATIVE_API10
	return gpsAssetManager != NULL;
#else
	return gpsZipPKG != NULL;
#endif	
}
//-------------------------------------------------------------------------------------------

s16 androidFS_fopen(const char* filename)
{
	if(_androidFS_checkErrors())
	{
		s16 i;
		for(i = 0; i < MAX_OPENFILES; i++)
		{
			if(gsFileHandlerState[i] == FALSE)
			{
				char fullPath[MAX_FILEPATH];
				STD_StrCpy(fullPath, RES_PATH);
				STD_StrCpy(fullPath + RES_PATH_LEN, filename);
#ifdef NDK_NATIVE_API10
				gspFileHandler[i] = AAssetManager_open(gpsAssetManager, fullPath, AASSET_MODE_UNKNOWN);
				if(gspFileHandler[i] == NULL)
				{
					OS_Printf("file fopen result is FALSE\n");
					return -1;
				}
				else
				{
					gsFileHandlerState[i] = TRUE;
					return i;
				}
#else
				gsFileIndex[i] = zip_name_locate(gpsZipPKG, fullPath, 0);
				if(gsFileIndex[i] >= 0)
				{
					gspFileHandler[i] = zip_fopen_index(gpsZipPKG, gsCurrentIndex, 0/*ZIP_FL_COMPRESSED*/);
					if(gspFileHandler[i] != NULL)
					{
						gsFileHandlerState[i] = TRUE;
						return i;
					}
					gsFileIndex[i] = -1;
				}
				{
					int zep, sep;
					zep = sep = 0;			
					zip_error_get(gpsZipPKG, &zep, &sep); 
					OS_Printf("file fopen result is FALSE, errors: zep=%d, sep=%d\n", zep, sep);
				}
#endif
			}
		}
		OS_Warning("Maximum number of open files\n");
	}
	return -1;
}
//---------------------------------------------------------------------------

s32 androidFS_fsize(s16 id)
{
	if(id < MAX_OPENFILES && _androidFS_checkErrors() && gsFileHandlerState[id] == TRUE)
	{
#ifdef NDK_NATIVE_API10
		return AAsset_getLength(gspFileHandler[id]);
#else
		{
			struct zip_stat sb;
			if(0 == zip_stat_index(gpsZipPKG, gsFileIndex[id], 0, &sb))
			{
				return sb.size;
			}
		}
#endif
	}
	return 0;
}
//---------------------------------------------------------------------------

BOOL androidFS_fseek(u32 off, s16 id)
{
	if(id < MAX_OPENFILES && _androidFS_checkErrors() && gsFileHandlerState[id] == TRUE)
	{
#ifdef NDK_NATIVE_API10
			AAsset_seek(gspFileHandler[id], off, SEEK_SET);
#else
			!!! todo: create fseek for zip_***
#endif
		return TRUE;
	}
	return FALSE;
}
//---------------------------------------------------------------------------

s32 androidFS_fread(unsigned char* buf, u32 size, s16 id)
{
	if(id < MAX_OPENFILES && _androidFS_checkErrors() && gsFileHandlerState[id] == TRUE)
	{
#ifdef NDK_NATIVE_API10
		return AAsset_read(gspFileHandler[id], buf, size);
#else
		return zip_fread(gspFileHandler[id], buf, size);
#endif
		//OS_Warning("general file fread error\n");
	}
	return -1;
}
//---------------------------------------------------------------------------

void androidFS_fclose(s16 id)
{
	if(id < MAX_OPENFILES && _androidFS_checkErrors() && gsFileHandlerState[id] == TRUE)
	{
#ifdef NDK_NATIVE_API10
		AAsset_close(gspFileHandler[id]);
#else
		zip_fclose(gspFileHandler[id]);
#endif
		gsFileHandlerState[id] = FALSE;
	}
}
//---------------------------------------------------------------------------

#else // java pkg access

#include <jni.h>

static JavaVM *gJavaVM;
static BOOL gAttached = FALSE;

static jobject gInterfaceObject = NULL;
const char *kInterfacePath = "com/app/main/FileReader";

//---------------------------------------------------------------------------

typedef struct
{
	JNIEnv *env;
	jclass interfaceClass;
}JavaInstance;

//---------------------------------------------------------------------------

static void _detachFromJava();
static BOOL _attachToJava(JavaInstance *ji);
static void _initClassHelper(JNIEnv *env, const char *path, jobject *objptr);
//---------------------------------------------------------------------------

void androidFS_initFileSystem(const char*)
{
}
//---------------------------------------------------------------------------

void androidFS_releaseFileSystem()
{
}
//---------------------------------------------------------------------------

jint JNI_OnLoad(JavaVM* vm, void* reserved)
{
	JNIEnv *env;
	gJavaVM = vm;
	OS_Printf("JNI_OnLoad called\n");
	if((*gJavaVM)->GetEnv(gJavaVM, (void**)&env, JNI_VERSION_1_4) != JNI_OK)
	{
		OS_Warning("Failed to get the environment using GetEnv()\n");
		return -1;
	}
	
	_initClassHelper(env, kInterfacePath, &gInterfaceObject);
	
	return JNI_VERSION_1_4;
}
//---------------------------------------------------------------------------

void _initClassHelper(JNIEnv *env, const char *path, jobject *objptr)
{
	jclass cls = (*env)->FindClass(env, path);
	if(!cls)
	{
		OS_Warning("initClassHelper: failed to get %s class reference\n", path);
		return;
	}
	jmethodID constr = (*env)->GetMethodID(env, cls, "<init>", "()V");
	if(!constr)
	{
		OS_Warning("initClassHelper: failed to get %s constructor\n", path);
		return;
	}
	jobject obj = (*env)->NewObject(env, cls, constr);
	if(!obj)
	{
		OS_Warning("initClassHelper: failed to create a %s object\n", path);
		return;
	}
	(*objptr) = (*env)->NewGlobalRef(env, obj);
	(*env)->DeleteLocalRef(env, obj);
}
//---------------------------------------------------------------------------

static BOOL _attachToJava(JavaInstance *ji)
{
	int status;
	status = (*gJavaVM)->GetEnv(gJavaVM, (void **)&(ji->env), JNI_VERSION_1_4);
	if(status < 0)
	{
		OS_Warning("callback_handler: failed to get JNI environment, assuming native thread\n");
		status = (*gJavaVM)->AttachCurrentThread(gJavaVM, &(ji->env), NULL);
		if(status < 0)
		{
			OS_Warning("callback_handler: failed to attach current thread\n");
			gAttached = FALSE;
			return FALSE;
		}
		gAttached = TRUE;
	}
	
	ji->interfaceClass = (*(ji->env))->GetObjectClass(ji->env, gInterfaceObject);
	if(!ji->interfaceClass)
	{
		OS_Warning("callback_handler: failed to get class reference\n");
		_detachFromJava();
		return FALSE;
	}
	return TRUE; 
}
//---------------------------------------------------------------------------

static void _detachFromJava()
{
	if(gAttached)
	{
		(*gJavaVM)->DetachCurrentThread(gJavaVM);
		gAttached = FALSE;
	}
}
//---------------------------------------------------------------------------

BOOL androidFS_fopen(const char* filename)
{
	JavaInstance ji;
	BOOL res = FALSE;
	if(_attachToJava(&ji))
	{
		jstring js = (*(ji.env))->NewStringUTF(ji.env, filename);
		jmethodID method = (*(ji.env))->GetStaticMethodID(ji.env, ji.interfaceClass, "open", "(Ljava/lang/String;)I");
		if(!method)
		{
			OS_Warning("callback_handler: failed to get method [open]\n");
		}
		else
		{
			(*(ji.env))->CallStaticIntMethod(ji.env, ji.interfaceClass, method, js);
			res = TRUE;
		}
		(*(ji.env))->DeleteLocalRef(ji.env, js);
		_detachFromJava();
	}
	return res;
}
//---------------------------------------------------------------------------

s32 androidFS_fsize()
{
	JavaInstance ji;
	s32 res = -1;
	if(_attachToJava(&ji))
	{
		jmethodID method = (*(ji.env))->GetStaticMethodID(ji.env, ji.interfaceClass, "size", "()I");
		if(!method)
		{
			OS_Warning("callback_handler: failed to get method [size]\n");
			_detachFromJava();
			return FALSE;
		}
		res = (*(ji.env))->CallStaticIntMethod(ji.env, ji.interfaceClass, method);
		_detachFromJava();
	}
	return res;
}
//---------------------------------------------------------------------------

BOOL androidFS_fread(unsigned char* buf, s32 size)
{
	JavaInstance ji;
	BOOL res = FALSE;
	if(_attachToJava(&ji))
	{
		jbyte *jbarr;
		jbyteArray jarr;
		jmethodID method = (*(ji.env))->GetStaticMethodID(ji.env, ji.interfaceClass, "read", "([B)I");
		if(!method)
		{
			OS_Warning("callback_handler: failed to get method [read]\n");
			_detachFromJava();
			return res;
		}
		jarr = (*(ji.env))->NewByteArray(ji.env, size);
		res = (*(ji.env))->CallStaticIntMethod(ji.env, ji.interfaceClass, method, jarr);
		jbarr = (*(ji.env))->GetByteArrayElements(ji.env, jarr, NULL);
		if(res)
		{
			memcpy(buf, jbarr, size);
			OS_Printf("read and copy %d bytes to buffer\n", size);
		}
		(*(ji.env))->ReleaseByteArrayElements(ji.env, jarr, jbarr, JNI_ABORT);
		(*(ji.env))->DeleteLocalRef(ji.env, jarr);
		_detachFromJava();
	}
	return res;
}
//---------------------------------------------------------------------------

void androidFS_fclose()
{
	JavaInstance ji;
	if(_attachToJava(&ji))
	{
		jmethodID method = (*(ji.env))->GetStaticMethodID(ji.env, ji.interfaceClass, "close", "()V");
		if(!method)
		{
			OS_Warning("callback_handler: failed to get method [close]\n");
		}
		else
		{
			(*(ji.env))->CallStaticVoidMethod(ji.env, ji.interfaceClass, method);
        }
		_detachFromJava();
	}
}
//---------------------------------------------------------------------------
#endif // CUSTOM_PKG_ACCESS
#endif //ANDROID_NDK
