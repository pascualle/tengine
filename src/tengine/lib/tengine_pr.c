/*
tengine - cross-platform game engine
------------------------------------

Copyright (c) 2013 Papa Pascualle
Contact Email: pascualle@gmail.com

Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
*/

#include "tengine.h"
#include "texts.h"
#include "jobs_low.h"
#include "jobs.h"
#include "fxmath.h"

// -------------------------------------------------------------------------------------------

extern struct TEngineCommonData cd;
extern const fx32 fakeTextFrData[ppranifrfixeddataCOUNT];

// -------------------------------------------------------------------------------------------

static u16 _prGetAnimationId(struct TEngineInstance *ei, s32 pr_id);
static void _calcFrameSizes(struct TEngineInstance *i, s32 pr_id);
static void _updateStreamAnimation(struct TEngineInstance *ei, s32 id, s32 anim_id, s32 ani_fr_idx);
static void _addXYToPosition(struct TEngineInstance *a, s32 pr_id, fx32 xv, fx32 yv);
static void _addXY(struct TEngineInstance *a, s32 pr_id, fx32 xv, fx32 yv);
#ifdef TENGINE_LEGACY_CODE
static BOOL _logicalOper(fx32 val1, fx32 val2, s32 math_oper);
#endif
static void _doTrigger(s32 trig_id, s32 owner, s32 second);
static BOOL _setNextDirectorPos(s32 pr, s32 node);
static void _processMovementBetweenPathNodes(s32 pr, fx32 in_speed);
static void _setCustomRotation(struct TEngineInstance *l, s32 pr_id, fx32 deg_angle);
static fx32 _getCustomRotation(struct TEngineInstance *l, s32 pr_id);
static void _resetRotationToDefault(struct TEngineInstance *l, s32 pr_id);
static void _addAlphaDeltaRecursive(struct TEngineInstance *l, s32 pr_id, s32 da);
static void _setCustomScale(struct TEngineInstance *l, s32 pr_id, struct fxVec2 scaleRatio);
static void _getCustomScale(struct TEngineInstance *l, s32 pr_id, struct fxVec2 *scaleRatio);
static void _resetScaleToDefault(struct TEngineInstance *l, s32 pr_id);
static void _setCustomAlpha(struct TEngineInstance *l, s32 pr_id, u8 alpha);
static u8 _getCustomAlpha(struct TEngineInstance *l, s32 pr_id);
static void _resetAlphaToDefault(struct TEngineInstance *l, s32 pr_id);
static void _calculatePositionAndRotationJN(struct TEngineInstance *a, const fx32* pt, s32 pr_id, s32 idx1, s32 idx2, fx32 pos[2]);
static void _calculatePositionAndRotation(struct TEngineInstance *a, const fx32* pt, s32 pr_id, s32 idx1, s32 idx2, s32 idx3, fx32 pos[2]);
static BOOL _prGetJoinNodePosition(struct TEngineInstance *inst, s32 pr_id, u8 idx, struct fxVec2* const pos);
static BOOL _prSetJoinTo(struct TEngineInstance *inst, s32 pr_id, s32 pr_id_to, s32 idx);
#ifdef CUSTOMAFFINETRANSFORM_SUPPORT
static void _addRotationDeltaRecursive(struct TEngineInstance *inst, s32 pr_id, fx32 da);
static void _addScaleDeltaRecursive(struct TEngineInstance *inst, s32 pr_id, fx32 dax, fx32 day);
static void _calcLogicRectCoortinates(struct TEngineInstance *inst, s32 pr_id);
#endif
#ifdef TENGINE_LEGACY_CODE
static BOOL _onEndAnimation(s32 mp);
static void _onChangeDirector(s32 pr, s32 dir);
static BOOL _checkSptCollide(u16 script_id, u16 pr, u16 opr);
static BOOL _checkSptDirector(u16 script_id, u16 pr, u16 dir);
static BOOL _checkSptProperty(u16 script_id, u16 pr);
#endif
// -------------------------------------------------------------------------------------------

u16 getPrpSpeedIdx(void)
{
	return cd.spd_prp_id;
}
// -------------------------------------------------------------------------------------------

u16 getPrpEnabledIdx(void)
{
	return cd.enable_prp_id;
}
// -------------------------------------------------------------------------------------------

void checkToMove(s32 *mp, struct fxVec2 position, s32 checkDirection)
{
	s32 type, pos, x, y;

	if(cd.ainst->tilemap == NULL)
	{
		return;
	}

	x = fx2int(position.x);
	y = fx2int(position.y);
	switch(checkDirection)
	{
	case CHECK_DOWN:
		y += CHECK_OFFSET;
		break;
	case CHECK_UP:
		y -= CHECK_OFFSET;
	}

	SDK_NULL_ASSERT(mp);
	SDK_NULL_ASSERT(cd.ainst->tile_data);

	mp[TYPE] = 0; // solid 0 idx
	mp[POSITION] = -1;

	if (x < 0 || x > cd.ainst->sz_mapW || y < 0 || y > cd.ainst->sz_mapH)
		return;

	mp[POSITION] = pos = (y / (s32)cd.p_h) * (s32)cd.ainst->map_w + x / (s32)cd.p_w;
	if (pos < cd.ainst->sz_map)
		type = cd.ainst->tilemap[pos];
	else
		return;

	if (type < cd.ainst->pnt_count)
	{
		type = cd.ainst->tile_data[type][TILE_PRP];
	}
	else
	{
		if (type == NONE_MAP_IDX)
		{
			type = NONE_BGTYPE;
        }
	}
	mp[TYPE] = type;
}
// ---------------------------------------------------------------------------

void _prGetFramePiecesInfo(struct TEngineInstance *ei, s32 id, s32 *pc_count, s32 *anim_id)
{
	SDK_NULL_ASSERT(ei->pr_idata);
	SDK_NULL_ASSERT(cd.ppr_fr_data);
	*anim_id = _prGetAnimationId(ei, id);

	if(ei->pr_idata[id][IANIFRIDX] >= 0)
	{
		const fx32* pt = cd.ppr_fr_data[ei->pr_idata[id][IPARENT_IDX]]
							[*anim_id]
							[ei->pr_idata[id][IANIFRIDX]];
		*pc_count = fx2int(pt[ANIFRFRCOUNT]);
	}
	else
	{
		*pc_count = 0;
	}
}
// ----------------------------------------------------------------------------------

const s16 *_getFramePieceData(s32 ppr_id, s32 anim_id, s32 frame_idx, s32 pc_idx)
{
	SDK_NULL_ASSERT(cd.ppr_frfr_data);
	SDK_ASSERT(frame_idx >= 0);
	SDK_ASSERT(pc_idx >= 0);
	return cd.ppr_frfr_data[ppr_id][anim_id][frame_idx][pc_idx];
}
// ----------------------------------------------------------------------------------

static void _prInnerAlign(struct TEngineInstance *ei, s32 pr_id)
{
	SDK_NULL_ASSERT(ei->pr_idata);
	SDK_NULL_ASSERT(cd.ppr_anim_data);
	if(ei->all_pr_count > pr_id)
	{
		s32 aid, p;
		fx32 x, y;
		aid = _prGetAnimationId(ei, pr_id);
		x = ei->pr_fdata[pr_id][FX];
		y = ei->pr_fdata[pr_id][FY];
		p = ei->pr_idata[pr_id][IPARENT_IDX];
		SDK_NULL_ASSERT(ei->pr_cfdata);
		ei->pr_cfdata[pr_id][FVBTOP0] = FX32(cd.ppr_anim_data[p][aid][ANIVROFFYLT]) + y;
		ei->pr_cfdata[pr_id][FVBLEFT0] = FX32(cd.ppr_anim_data[p][aid][ANIVROFFXLT]) + x;
		ei->pr_cfdata[pr_id][FVBBOTTOM0] = FX32(cd.ppr_anim_data[p][aid][ANIVROFFYRB]) + y;
		ei->pr_cfdata[pr_id][FVBRIGHT0] = FX32(cd.ppr_anim_data[p][aid][ANIVROFFXRB]) + x;
    }
	_calcFrameSizes(ei, pr_id);
}
// ----------------------------------------------------------------------------------

static void _calcFrameSizes(struct TEngineInstance *ei, s32 pr_id)
{
	s32 aidx;
	SDK_NULL_ASSERT(ei->pr_idata);
	aidx = ei->pr_idata[pr_id][IANIFRIDX];
	if(aidx >= 0)
	{
		s32 p, aid;
		fx32 x, y;
		SDK_NULL_ASSERT(cd.ppr_fr_data);
		p = ei->pr_idata[pr_id][IPARENT_IDX];
		aid = _prGetAnimationId(ei, pr_id);
		x = ei->pr_fdata[pr_id][FX];
		y = ei->pr_fdata[pr_id][FY];
		ei->pr_fdata[pr_id][FTOP] = cd.ppr_fr_data[p][aid][aidx][ANIFROFFT] + y;
		ei->pr_fdata[pr_id][FLEFT] = cd.ppr_fr_data[p][aid][aidx][ANIFROFFL] + x;
		ei->pr_fdata[pr_id][FCURRWIDTH] = cd.ppr_fr_data[p][aid][aidx][ANIFRW];
		ei->pr_fdata[pr_id][FCURRHEIGHT] = cd.ppr_fr_data[p][aid][aidx][ANIFRH];
		if(ei->all_pr_count > pr_id)
		{	
			ei->pr_cfdata[pr_id][FCBLEFT0] = cd.ppr_fr_data[p][aid][aidx][ANIFRCROFFXLT] + x;
			ei->pr_cfdata[pr_id][FCBTOP0] = cd.ppr_fr_data[p][aid][aidx][ANIFRCROFFYLT] + y;
			ei->pr_cfdata[pr_id][FCBRIGHT0] = cd.ppr_fr_data[p][aid][aidx][ANIFRCROFFXRB] + x;
			ei->pr_cfdata[pr_id][FCBBOTTOM0] = cd.ppr_fr_data[p][aid][aidx][ANIFRCROFFYRB] + y;
		}
	}
	else
	{
		fx32 x, y;
		x = ei->pr_fdata[pr_id][FX];
		y = ei->pr_fdata[pr_id][FY];
		ei->pr_fdata[pr_id][FTOP] = y;
		ei->pr_fdata[pr_id][FLEFT] = x;
		ei->pr_fdata[pr_id][FCURRWIDTH] = 0;
		ei->pr_fdata[pr_id][FCURRHEIGHT] = 0;
		if(ei->all_pr_count > pr_id)
		{
			ei->pr_cfdata[pr_id][FCBLEFT0] = x;
			ei->pr_cfdata[pr_id][FCBTOP0] =  y;
			ei->pr_cfdata[pr_id][FCBRIGHT0] = x;
			ei->pr_cfdata[pr_id][FCBBOTTOM0] = y;
#ifdef CUSTOMAFFINETRANSFORM_SUPPORT
			ei->pr_cfdata[pr_id][FCBLTX] = x;
			ei->pr_cfdata[pr_id][FCBLTY] = y;
			ei->pr_cfdata[pr_id][FCBRTX] = x;
			ei->pr_cfdata[pr_id][FCBRTY] = y;
			ei->pr_cfdata[pr_id][FCBRBX] = x;
			ei->pr_cfdata[pr_id][FCBRBY] = y;
			ei->pr_cfdata[pr_id][FCBLBX] = x;
			ei->pr_cfdata[pr_id][FCBLBY] = y;
			ei->pr_cfdata[pr_id][FVBLTX] = x;
			ei->pr_cfdata[pr_id][FVBLTY] = y;
			ei->pr_cfdata[pr_id][FVBRTX] = x;
			ei->pr_cfdata[pr_id][FVBRTY] = y;
			ei->pr_cfdata[pr_id][FVBRBX] = x;
			ei->pr_cfdata[pr_id][FVBRBY] = y;
			ei->pr_cfdata[pr_id][FVBLBX] = x;
			ei->pr_cfdata[pr_id][FVBLBY] = y;
#endif
		}
	}
}
// ----------------------------------------------------------------------------------

void _updateAnimation(s32 id, s32 ms)
{
	SDK_NULL_ASSERT(cd.ainst->pr_idata);
	SDK_NULL_ASSERT(cd.ppr_decortype);
	if(cd.ainst->pr_idata[id][IANIFRIDX] >= 0)
	{
 		s32 frame, anim_id, ppr_idx, act, aend;
		cd.ainst->pr_idata[id][ITIMER] -= ms;
		if(cd.ainst->pr_idata[id][ITIMER] <= 0)
		{
			ppr_idx = cd.ainst->pr_idata[id][IPARENT_IDX];
			anim_id = _prGetAnimationId(cd.ainst, id);
			frame = cd.ainst->pr_idata[id][IANIFRIDX];
			act = fx2int(cd.ppr_fr_data[ppr_idx][anim_id][frame][ANIEVENTID]);
			if(cd._onEventCB != NULL && act >= 0)
			{
				struct EventCallbackData ed;
				ed.eventType = EVENT_TYPE_ON_ANIMATION;
				ed.layer = (s32)cd.a_layer;
				ed.eventId = act;
				ed.ownerId = id;
				ed.initiatorId = anim_id;
				ed.eventData.mpData = NULL;
				cd._onEventCB(&ed);
			}
			frame++;
			act = cd.ppr_anim_data[ppr_idx][anim_id][ANIFRCOUNT];
			aend = 0;
			if(frame >= act)
			{
				frame = (cd.ppr_anim_data[ppr_idx][anim_id][ANILOOP] == 0) ? act - 1 : 0;
				aend = 1;
			}
			if(frame != cd.ainst->pr_idata[id][IANIFRIDX])
			{
				cd.ainst->pr_idata[id][IANIFRIDX] = frame;
				_calcFrameSizes(cd.ainst, id);
			}
			cd.ainst->pr_idata[id][ITIMER] = fx2int(cd.ppr_fr_data[ppr_idx][anim_id][frame][ANIFRTIME]);
			if(cd.strm_obj_data != NULL && cd.strm_obj_data[id][anim_id])
			{
				_updateStreamAnimation(cd.ainst, id, anim_id, frame);
			}
			if(cd.ppr_decortype[ppr_idx] == 0 && aend == 1 && cd.ainst->pr_idata[id][IANIENDFLAG] == 0)
			{
#ifdef TENGINE_LEGACY_CODE
				_onEndAnimation(id);
#endif
				cd.ainst->pr_idata[id][IANIENDFLAG] = 1;
			}
			_addXYToPosition(cd.ainst, id, 0, 0); // update nodes
		}
    }
}
// ----------------------------------------------------------------------------------

void _updateStreamAnimation(struct TEngineInstance *ei, s32 id, s32 anim_id, s32 ani_fr_idx)
{
	s32 i, ppr_idx;
	SDK_NULL_ASSERT(cd.strm_obj_data[id][anim_id]);
	ppr_idx = ei->pr_idata[id][IPARENT_IDX];
#ifdef JOBS_IN_SEPARATE_THREAD
	jobCriticalSectionBegin();	
#endif
	for(i = 0; i < JOB_ANIM_TASK_BUFFER_MAX; i++)
	{
		s32 pc_count, j;
		if(ani_fr_idx >= cd.ppr_anim_data[ppr_idx][anim_id][ANIFRCOUNT])
		{
			if(cd.ppr_anim_data[ppr_idx][anim_id][ANILOOP])
			{
				ani_fr_idx = 0;
			}
			else
			{
				break;
			}
		}
		pc_count = fx2int(cd.ppr_fr_data[ppr_idx][anim_id][ani_fr_idx][ANIFRFRCOUNT]);
		for(j = 0; j < pc_count; j++)
		{
			const s16* frdata = _getFramePieceData(ppr_idx, anim_id, ani_fr_idx, j);
			if(_isStreamFrame(frdata[ANIFRFRDATA]))
			{
				s32 k;
				const char *frfn, *fn;
				frfn = cd.res_strm_names[frdata[ANIFRFRID]]; 
				for(k = i; k < JOB_ANIM_TASK_BUFFER_MAX; k++)
				{
					fn = cd.strm_obj_data[id][anim_id]->mData[k].mpFilename;
					if(fn != NULL && STD_StrCmp(frfn, fn) == 0)
					{
						if(k != i)
						{
							struct AnimStreamData ttd;
							ttd = cd.strm_obj_data[id][anim_id]->mData[k];
							cd.strm_obj_data[id][anim_id]->mData[k] = cd.strm_obj_data[id][anim_id]->mData[i];
							cd.strm_obj_data[id][anim_id]->mData[i] = ttd; 
						}
						break;
					}
				}
				fn = cd.strm_obj_data[id][anim_id]->mData[i].mpFilename; 
				if(fn == NULL || STD_StrCmp(frfn, fn) != 0)
				{
					cd.strm_obj_data[id][anim_id]->mData[i].mpFilename = frfn;
					cd.strm_obj_data[id][anim_id]->mData[i].mBufferStatus = FALSE;
					cd.strm_obj_data[id][anim_id]->mData[i].mBindStatus = FALSE;
				}
				break; // only one stream frame per one animation frame can be
			}
		}
		++ani_fr_idx;
	}
#ifdef JOBS_IN_SEPARATE_THREAD
	jobCriticalSectionEnd();	
#endif
}
// ----------------------------------------------------------------------------------

void prSetState(s32 pr_id, s32 state, enum StateBlendMode mode)
{
	SDK_NULL_ASSERT(cd.ainst->pr_idata);
	if(!prIsTextBox(pr_id))
	{
#ifdef SDK_DEBUG
		if(state < 0 || state >= cd.states_ct)
		{
			OS_Warning("Set state error: %s", "wrong STATE ID\n");
			SDK_ASSERT(0);
		}
#endif
		cd.ainst->pr_idata[pr_id][ITEMPSTATE] = (mode << 16) | state;
#ifdef SDK_DEBUG
		// state validation
		{
			s32 aid, p;
			SDK_NULL_ASSERT(cd.states);
			SDK_NULL_ASSERT(cd.ainst->pr_idata);
			SDK_ASSERT(state >= 0); // check with editor for unassigned states
			aid = cd.states[state][stIDANIMATION];
			p = cd.ainst->pr_idata[pr_id][IPARENT_IDX];
			if(cd.ppr_anim_data[p][aid] == NULL)
			{
				OS_Warning("Set state error: %s", "please set correct STATE for object (or initial state for Group Object Type in MapEditor)\n");
			}
			SDK_ASSERT(cd.ppr_anim_data[p][aid] != NULL); // wrong state
			(void)aid;
			(void)p;
		}
#endif
    }
}
// ----------------------------------------------------------------------------------

void prSetStateLayer(s32 pr_id, s32 state, enum StateBlendMode mode, u32 layer)
{
	struct TEngineInstance *l = _getInstance(layer);
	SDK_NULL_ASSERT(l->pr_idata);
	if(!prIsTextBoxLayer(pr_id, layer))
	{
#ifdef SDK_DEBUG
		if(state < 0 || state >= cd.states_ct)
		{
			OS_Warning("Set state error: %s", "wrong STATE ID\n");
			SDK_ASSERT(0);
		}
#endif
		l->pr_idata[pr_id][ITEMPSTATE] = (mode << 16) | state;
#ifdef SDK_DEBUG
		// state validation
		{
			s32 aid, p;
			SDK_NULL_ASSERT(cd.states);
			SDK_NULL_ASSERT(l->pr_idata);
			SDK_ASSERT(state >= 0); // check with editor for unassigned states
			aid = cd.states[state][stIDANIMATION];
			p = l->pr_idata[pr_id][IPARENT_IDX];
			if(cd.ppr_anim_data[p][aid] == NULL)
			{
				OS_Warning("Set state error: %s", "please set correct STATE for object (or initial state for Group Object Type in MapEditor)\n");
			}
			SDK_ASSERT(cd.ppr_anim_data[p][aid] != NULL); // wrong state
			(void)aid;
			(void)p;
		}
#endif
    }
}
// ----------------------------------------------------------------------------------

void _setState(struct TEngineInstance *ei, s32 pr_id, s32 statedata, BOOL updateNodes)
{
	s32 aid, p, state;
	SDK_NULL_ASSERT(ei->pr_idata);
	SDK_NULL_ASSERT(cd.ppr_anim_data);
	state = statedata & 0xFFFF;
#ifdef SDK_DEBUG
	if(state == 0xFFFF)
	{
		OS_Warning("Set state error: %s", "please set correct STATE for object (or initial state for Group Object Type in MapEditor)\n");
	}
	SDK_ASSERT(state != 0xFFFF);
#endif
	aid = _prGetAnimationId(ei, pr_id);
	p = ei->pr_idata[pr_id][IPARENT_IDX];
#ifdef SDK_DEBUG
	if(cd.ppr_anim_data[p][aid] == NULL)
	{
		OS_Warning("Set state error: %s", "wrong current STATE ID\n");	
	}
	SDK_NULL_ASSERT(cd.ppr_anim_data[p][aid]); // wrong current state
#endif
	if(ei->pr_idata[pr_id][ISTATE] != state && cd.strm_obj_data && cd.strm_obj_data[pr_id][aid] != NULL)
	{
		jobRemoveStreamVideoTask(cd.strm_obj_data[pr_id][aid]);
	}
	ei->pr_idata[pr_id][ITEMPSTATE] = -1;
	ei->pr_idata[pr_id][ISTATE] = state;
	switch(statedata >> 16)
	{
		case SBM_FROM_BEGIN_ANIMATION:
		{
			s32 act_prev, act_curr, k;
			act_prev = cd.ppr_anim_data[p][aid][ANIFRCOUNT];
			aid = _prGetAnimationId(ei, pr_id);
#ifdef SDK_DEBUG
			if(cd.ppr_anim_data[p][aid] == NULL)
			{
				OS_Warning("Set state error: %s", "wrong STATE ID\n");	
			}
			SDK_NULL_ASSERT(cd.ppr_anim_data[p][aid]); // wrong next state
#endif
			act_curr = cd.ppr_anim_data[p][aid][ANIFRCOUNT];
			k = (ei->pr_idata[pr_id][IANIFRIDX] << 10) / act_prev;
			ei->pr_idata[pr_id][IANIFRIDX] = (k * act_curr) >> 10;
			if(ei->pr_idata[pr_id][IANIFRIDX] >= act_curr)
			{
				ei->pr_idata[pr_id][IANIFRIDX] = act_curr - 1;
			}
		}
		break;
		case SBM_FROM_END_ANIMATION:
		{
			s32 act_prev, act_curr, k;
			act_prev = cd.ppr_anim_data[p][aid][ANIFRCOUNT];
			aid = _prGetAnimationId(ei, pr_id);
#ifdef SDK_DEBUG
			if(cd.ppr_anim_data[p][aid] == NULL)
			{
				OS_Warning("Set state error: %s", "wrong STATE ID\n");	
			}
			SDK_NULL_ASSERT(cd.ppr_anim_data[p][aid]); // wrong next state
#endif
			act_curr = cd.ppr_anim_data[p][aid][ANIFRCOUNT];
			k = (ei->pr_idata[pr_id][IANIFRIDX] << 10) / act_prev;
			ei->pr_idata[pr_id][IANIFRIDX] = act_curr - ((k * act_curr) >> 10);
			if(ei->pr_idata[pr_id][IANIFRIDX] >= act_curr)
			{
				ei->pr_idata[pr_id][IANIFRIDX] = act_curr - 1;
			}
		}
		break;
		case SBM_RANDOM:
		{
			s32 act_curr;
			aid = _prGetAnimationId(ei, pr_id);
#ifdef SDK_DEBUG
			if(cd.ppr_anim_data[p][aid] == NULL)
			{
				OS_Warning("Set state error: %s", "wrong STATE ID\n");	
			}
			SDK_NULL_ASSERT(cd.ppr_anim_data[p][aid]); // wrong next state
#endif
			act_curr = cd.ppr_anim_data[p][aid][ANIFRCOUNT];
			ei->pr_idata[pr_id][IANIFRIDX] = (s32)mthGetRandom(act_curr);
		}
		break;
		default:
			ei->pr_idata[pr_id][IANIFRIDX] = 0;
			aid = _prGetAnimationId(ei, pr_id);
#ifdef SDK_DEBUG
			if(cd.ppr_anim_data[p][aid] == NULL)
			{
				OS_Warning("Set state error: %s", "wrong STATE ID\n");	
			}
			SDK_NULL_ASSERT(cd.ppr_anim_data[p][aid]); // wrong next state
#endif
	}
	if(cd.ppr_anim_data[p][aid] != NULL)
	{
		ei->pr_idata[pr_id][ITIMER] = fx2int(cd.ppr_fr_data[p][aid][0][ANIFRTIME]);
		if(cd.strm_obj_data && cd.strm_obj_data[pr_id][aid] != NULL)
		{
			_updateStreamAnimation(ei, pr_id, aid, ei->pr_idata[pr_id][IANIFRIDX]);
			jobAddStreamVideoTask(cd.strm_obj_data[pr_id][aid]);
		}
		_prInnerAlign(ei, pr_id);
		if(updateNodes)
		{
			_addXYToPosition(ei, pr_id, 0, 0);
		}
	}
	if (ei->all_pr_count > pr_id)
	{
		ei->pr_cidata[pr_id][ICUSTOMFLAGS] |= IRECALCULATELOGICRECTSFLAG;
		ei->pr_idata[pr_id][IANIENDFLAG] = 0;
	}
}
// ----------------------------------------------------------------------------------

s32 prGetState(s32 pr_id)
{
	SDK_ASSERT(pr_id >= 0);
	SDK_NULL_ASSERT(cd.ainst->pr_idata);
	if(cd.ainst->pr_idata[pr_id][ITEMPSTATE] != -1)
	{
		return cd.ainst->pr_idata[pr_id][ITEMPSTATE] & 0xFFFF;
	}
	else
	{
		return cd.ainst->pr_idata[pr_id][ISTATE];
	}
}
// ----------------------------------------------------------------------------------

s32 prGetStateLayer(s32 pr_id, u32 layer)
{
	struct TEngineInstance *l = _getInstance(layer);
	SDK_ASSERT(pr_id >= 0);
	SDK_NULL_ASSERT(l->pr_idata);
	if(l->pr_idata[pr_id][ITEMPSTATE] != -1)
	{
		return l->pr_idata[pr_id][ITEMPSTATE] & 0xFFFF;
	}
	else
	{
		return l->pr_idata[pr_id][ISTATE];
	}
}
// ----------------------------------------------------------------------------------

void prSetProperty(s32 pr_id, s32 prp_id, fx32 val)
{
	SDK_ASSERT(cd.ainst->all_pr_count > pr_id && pr_id >= 0);
	SDK_ASSERT(prp_id >= 0);
	SDK_NULL_ASSERT(cd.ainst->pr_prp);
	cd.ainst->pr_prp[pr_id][prp_id] = val;
}
// ----------------------------------------------------------------------------------

void prSetPropertyLayer(s32 pr_id, s32 prp_id, fx32 val, u32 layer)
{
	struct TEngineInstance *l = _getInstance(layer);
	SDK_ASSERT(l->all_pr_count > pr_id && pr_id >= 0);
	SDK_ASSERT(prp_id >= 0);
	SDK_NULL_ASSERT(l->pr_prp);
	l->pr_prp[pr_id][prp_id] = val;
}
// ----------------------------------------------------------------------------------

fx32 prGetProperty(s32 pr_id, s32 prp_id)
{
	SDK_ASSERT(cd.ainst->all_pr_count > pr_id && pr_id >= 0);
	SDK_ASSERT(prp_id >= 0);
	SDK_NULL_ASSERT(cd.ainst->pr_prp);
	return cd.ainst->pr_prp[pr_id][prp_id];
}
// ----------------------------------------------------------------------------------

fx32 prGetPropertyLayer(s32 pr_id, s32 prp_id, u32 layer)
{
	struct TEngineInstance *l = _getInstance(layer);
	SDK_ASSERT(l->all_pr_count > pr_id && pr_id >= 0);
	SDK_ASSERT(prp_id >= 0);
	SDK_NULL_ASSERT(l->pr_prp);
	return l->pr_prp[pr_id][prp_id];
}
// ----------------------------------------------------------------------------------

void low_addXYToPosition(s32 pr_id, fx32 xv, fx32 yv, u32 layer)
{
	struct TEngineInstance *l = _getInstance(layer);
	_addXYToPosition(l, pr_id, xv, yv);
}
// ---------------------------------------------------------------------------

s32 prGetGroupIdx(s32 pr_id)
{
	SDK_ASSERT(cd.ainst->all_pr_count > pr_id && pr_id >= 0);
	SDK_NULL_ASSERT(cd.ainst->pr_idata);
	return cd.ainst->pr_idata[pr_id][IPARENT_IDX];
}
// ----------------------------------------------------------------------------------

s32 prGetGroupIdxLayer(s32 pr_id, u32 layer)
{
	struct TEngineInstance *l = _getInstance(layer);
	SDK_ASSERT(l->all_pr_count > pr_id && pr_id >= 0);
	return l->pr_idata[pr_id][IPARENT_IDX];
}
// ----------------------------------------------------------------------------------

BOOL prChangeType(s32 pr_id, s32 group_id)
{
	SDK_NULL_ASSERT(cd.ainst->pr_idata);
	if(group_id >= 0 && group_id < cd.ppr_count)
	{
		cd.ainst->pr_idata[pr_id][IPARENT_IDX] = group_id;
		return TRUE;
	}
	return FALSE;
}
// ----------------------------------------------------------------------------------

BOOL prChangeTypeLayer(s32 pr_id, s32 group_id, u32 layer)
{
	if(group_id >= 0 && group_id < cd.ppr_count)
	{
		_getInstance(layer)->pr_idata[pr_id][IPARENT_IDX] = group_id;
		return TRUE;
	}
	return FALSE;
}
// ----------------------------------------------------------------------------------

BOOL _prGetJoinNodePosition(struct TEngineInstance *inst, s32 pr_id, u8 idx, struct fxVec2* const pos)
{
	SDK_NULL_ASSERT(pos);
	if (inst->all_pr_count > pr_id &&
		idx < cd.jn_ct &&
		inst->pr_idata[pr_id][IPARENT_IDX] != PARENT_TEXT_OBJ)
	{
		const fx32* pt;
		fx32 apos[2];
		pt = cd.ppr_fr_data[inst->pr_idata[pr_id][IPARENT_IDX]][_prGetAnimationId(inst, pr_id)][inst->pr_idata[pr_id][IANIFRIDX]];
		_calculatePositionAndRotationJN(inst, pt, pr_id, ANIFRJNBEGIN + idx * 2, ANIFRJNBEGIN + 1 + idx * 2, apos);
		pos->x = apos[FX];
		pos->y = apos[FY];
	}
	return FALSE;
}
// ----------------------------------------------------------------------------------

s32 prGetJoinNodeCount(void)
{
	return cd.jn_ct;
}
// ----------------------------------------------------------------------------------

BOOL prGetJoinNodePosition(s32 pr_id, u8 idx, struct fxVec2* const pos)
{
	return _prGetJoinNodePosition(cd.ainst, pr_id, idx, pos);
}
// ----------------------------------------------------------------------------------

BOOL prGetJoinNodePositionLayer(s32 pr_id, u8 idx, struct fxVec2* const pos, u32 layer)
{
	struct TEngineInstance *l = _getInstance(layer);
	return _prGetJoinNodePosition(l, pr_id, idx, pos);
}
// ----------------------------------------------------------------------------------

// if idx < 0 -- break joint
BOOL _prSetJoinTo(struct TEngineInstance *inst, s32 pr_id, s32 pr_id_to, s32 idx)
{
	SDK_ASSERT(pr_id != pr_id_to);
	if (inst->all_pr_count > pr_id && inst->all_pr_count > pr_id_to && idx < cd.jn_ct && 
		inst->pr_idata[pr_id_to][IPARENT_IDX] != PARENT_TEXT_OBJ)
	{
		s32 i;
		SDK_NULL_ASSERT(inst->pr_cidata);
		for (i = 0; i < cd.jn_ct; i++)
		{
			if (inst->pr_cidata[pr_id_to][IJOINNODEBEGIN + i] == pr_id)
			{
				inst->pr_cidata[inst->pr_cidata[pr_id_to][IJOINNODEBEGIN + i]][ILINKTOOBJ] = NONE_MAP_IDX;
				inst->pr_cidata[pr_id_to][IJOINNODEBEGIN + i] = NONE_MAP_IDX;
			}
		}
		if (idx >= 0)
		{
			inst->pr_cidata[pr_id_to][IJOINNODEBEGIN + idx] = pr_id;
			inst->pr_cidata[pr_id][ILINKTOOBJ] = pr_id_to;
		}
		return TRUE;
	}
	return FALSE;
}
// ----------------------------------------------------------------------------------

BOOL prSetJoinTo(s32 pr_id, s32 pr_id_to, s32 idx)
{
	return _prSetJoinTo(cd.ainst, pr_id, pr_id_to, idx);
}
// ----------------------------------------------------------------------------------

BOOL prSetJoinToLayer(s32 pr_id, s32 pr_id_to, s32 idx, u32 layer)
{
	struct TEngineInstance *l = _getInstance(layer);
	return _prSetJoinTo(l, pr_id, pr_id_to, idx);
}
// ----------------------------------------------------------------------------------

s32 prGetJoinedTo(s32 pr_id)
{
	if(cd.ainst->all_pr_count > pr_id)
	{
		SDK_NULL_ASSERT(cd.ainst->pr_cidata);
		return cd.ainst->pr_cidata[pr_id][ILINKTOOBJ];
	}
	return NONE_MAP_IDX;
}
// ----------------------------------------------------------------------------------

s32 prGetJoinedToLayer(s32 pr_id, u32 layer)
{
	if(_getInstance(layer)->all_pr_count > pr_id)
	{
		return _getInstance(layer)->pr_cidata[pr_id][ILINKTOOBJ];
	}
	return NONE_MAP_IDX;
}
// ----------------------------------------------------------------------------------

s32 prGetJoinedChild(s32 pr_id, u8 idx)
{
	SDK_ASSERT(cd.ainst->all_pr_count > pr_id);
	if(idx < 3)
	{
		SDK_NULL_ASSERT(cd.ainst->pr_cidata);
		return cd.ainst->pr_cidata[pr_id][IJOINNODEBEGIN + idx];
	}
	return NONE_MAP_IDX;
}
// ----------------------------------------------------------------------------------

s32 prGetJoinedChildLayer(s32 pr_id, u8 idx, u32 layer)
{
	struct TEngineInstance *l = _getInstance(layer);
	if(l->all_pr_count > pr_id && idx < 3)
	{
		SDK_NULL_ASSERT(l->pr_cidata);
		return l->pr_cidata[pr_id][IJOINNODEBEGIN + idx];
	}
	return NONE_MAP_IDX;
}
// ----------------------------------------------------------------------------------

s32 prGetTargetPathNode(s32 pr_id)
{
	if(cd.ainst->all_pr_count > pr_id)
	{
		SDK_NULL_ASSERT(cd.ainst->pr_cidata);
		return cd.ainst->pr_cidata[pr_id][INODE];
	}
	return NONE_MAP_IDX;
}
// ----------------------------------------------------------------------------------

static void _prTeleportToPathNode(s32 pr_id, s32 pathnode_id, struct TEngineInstance* ainst)
{
	if (ainst->all_pr_count > pr_id && ainst->nodes_ct > pathnode_id)
	{
		fx32 x, y, ox, oy;
		SDK_NULL_ASSERT(ainst->pr_idata);
		SDK_NULL_ASSERT(ainst->pr_cidata);
		SDK_NULL_ASSERT(ainst->nodes_fdata);
		x = ainst->nodes_fdata[pathnode_id][FX];
		y = ainst->nodes_fdata[pathnode_id][FY];
		ox = ainst->pr_fdata[pr_id][FX];
		oy = ainst->pr_fdata[pr_id][FY];
		_addXYToPosition(ainst, pr_id, x - ox, y - oy);
		ainst->pr_cidata[pr_id][IPREVNODE] = NONE_MAP_IDX;
		ainst->pr_cidata[pr_id][INODE] = pathnode_id;
		ainst->pr_cfdata[pr_id][FX0_NODE] = ainst->pr_fdata[pr_id][FX];
		ainst->pr_cfdata[pr_id][FY0_NODE] = ainst->pr_fdata[pr_id][FY];
	}
}
// ----------------------------------------------------------------------------------

void prTeleportToPathNodeLayer(s32 pr_id, s32 pathnode_id, u32 layer)
{
	struct TEngineInstance* l = _getInstance(layer);
	_prTeleportToPathNode(pr_id, pathnode_id, l);
}
// ----------------------------------------------------------------------------------

void prTeleportToPathNode(s32 pr_id, s32 pathnode_id)
{
	_prTeleportToPathNode(pr_id, pathnode_id, cd.ainst);
}
// ----------------------------------------------------------------------------------

void prSetOppositeState(s32 pr_id, enum StateBlendMode mode)
{
	s32 st;
	SDK_NULL_ASSERT(cd.ainst->pr_idata);
	st = prGetState(pr_id);
	if(cd.states_map[st] < 0xff)
	{
		prSetState(pr_id, cd.states_map[st], mode);
	}
}
// ----------------------------------------------------------------------------------

char prGetCurrentDirection(s32 pr_id)
{
	s32 state;
	SDK_NULL_ASSERT(cd.states);
	state = prGetState(pr_id);
	return (char)cd.states[state][stDIRECTION];
}
// ----------------------------------------------------------------------------------

#ifdef CUSTOMAFFINETRANSFORM_SUPPORT
void _calcLogicRectCoortinates(struct TEngineInstance *a, s32 pr_id)
{
	if((a->pr_cidata[pr_id][ICUSTOMFLAGS] & IRECALCULATELOGICRECTSFLAG) == IRECALCULATELOGICRECTSFLAG)
	{
		if((a->pr_cidata[pr_id][ICUSTOMFLAGS] & ICUSTOMROTATIONFLAG) == ICUSTOMROTATIONFLAG ||
			(a->pr_cidata[pr_id][ICUSTOMFLAGS] & ICUSTOMSCALEFLAG) == ICUSTOMSCALEFLAG)
		{
			fx32 fsin, fcos, x, y, xl, yt, rt, yb;
			fsin = a->pr_cfdata[pr_id][FROTSIN];
			fcos = a->pr_cfdata[pr_id][FROTCOS];
			x = a->pr_fdata[pr_id][FX];
			y = a->pr_fdata[pr_id][FY];
			xl = FX_Mul((a->pr_cfdata[pr_id][FVBLEFT0] - x), fxAbs(a->pr_cfdata[pr_id][FCUSTOMSCALEXVALUE]));
			yt = FX_Mul((a->pr_cfdata[pr_id][FVBTOP0] - y), fxAbs(a->pr_cfdata[pr_id][FCUSTOMSCALEYVALUE]));
			rt = FX_Mul((a->pr_cfdata[pr_id][FVBRIGHT0] - x), fxAbs(a->pr_cfdata[pr_id][FCUSTOMSCALEXVALUE]));
			yb = FX_Mul((a->pr_cfdata[pr_id][FVBBOTTOM0] - y), fxAbs(a->pr_cfdata[pr_id][FCUSTOMSCALEYVALUE]));
			a->pr_cfdata[pr_id][FVBLTX] = x + FX_Mul(xl, fcos) + FX_Mul(yt, -fsin);
			a->pr_cfdata[pr_id][FVBLTY] = y + FX_Mul(xl, fsin) + FX_Mul(yt, fcos);
			a->pr_cfdata[pr_id][FVBLBX] = x + FX_Mul(xl, fcos) + FX_Mul(yb, -fsin);
			a->pr_cfdata[pr_id][FVBLBY] = y + FX_Mul(xl, fsin) + FX_Mul(yb, fcos);
			a->pr_cfdata[pr_id][FVBRTX] = x + FX_Mul(rt, fcos) + FX_Mul(yt, -fsin);
			a->pr_cfdata[pr_id][FVBRTY] = y + FX_Mul(rt, fsin) + FX_Mul(yt, fcos);
			a->pr_cfdata[pr_id][FVBRBX] = x + FX_Mul(rt, fcos) + FX_Mul(yb, -fsin);
			a->pr_cfdata[pr_id][FVBRBY] = y + FX_Mul(rt, fsin) + FX_Mul(yb, fcos);
			xl = FX_Mul((a->pr_cfdata[pr_id][FCBLEFT0] - x), fxAbs(a->pr_cfdata[pr_id][FCUSTOMSCALEXVALUE]));
			yt = FX_Mul((a->pr_cfdata[pr_id][FCBTOP0] - y), fxAbs(a->pr_cfdata[pr_id][FCUSTOMSCALEYVALUE]));
			rt = FX_Mul((a->pr_cfdata[pr_id][FCBRIGHT0] - x), fxAbs(a->pr_cfdata[pr_id][FCUSTOMSCALEXVALUE]));
			yb = FX_Mul((a->pr_cfdata[pr_id][FCBBOTTOM0] - y), fxAbs(a->pr_cfdata[pr_id][FCUSTOMSCALEYVALUE]));
			a->pr_cfdata[pr_id][FCBLTX] = x + FX_Mul(xl, fcos) + FX_Mul(yt, -fsin);
			a->pr_cfdata[pr_id][FCBLTY] = y + FX_Mul(xl, fsin) + FX_Mul(yt, fcos);
			a->pr_cfdata[pr_id][FCBLBX] = x + FX_Mul(xl, fcos) + FX_Mul(yb, -fsin);
			a->pr_cfdata[pr_id][FCBLBY] = y + FX_Mul(xl, fsin) + FX_Mul(yb, fcos);
			a->pr_cfdata[pr_id][FCBRTX] = x + FX_Mul(rt, fcos) + FX_Mul(yt, -fsin);
			a->pr_cfdata[pr_id][FCBRTY] = y + FX_Mul(rt, fsin) + FX_Mul(yt, fcos);
			a->pr_cfdata[pr_id][FCBRBX] = x + FX_Mul(rt, fcos) + FX_Mul(yb, -fsin);
			a->pr_cfdata[pr_id][FCBRBY] = y + FX_Mul(rt, fsin) + FX_Mul(yb, fcos);
		}
		else
		{
			a->pr_cfdata[pr_id][FVBRTX] = a->pr_cfdata[pr_id][FVBRIGHT0];
			a->pr_cfdata[pr_id][FVBRTY] = a->pr_cfdata[pr_id][FVBTOP0];
			a->pr_cfdata[pr_id][FVBLBX] = a->pr_cfdata[pr_id][FVBLEFT0];
			a->pr_cfdata[pr_id][FVBLBY] = a->pr_cfdata[pr_id][FVBBOTTOM0];
			a->pr_cfdata[pr_id][FVBLTX] = a->pr_cfdata[pr_id][FVBLBX];
			a->pr_cfdata[pr_id][FVBLTY] = a->pr_cfdata[pr_id][FVBRTY];
			a->pr_cfdata[pr_id][FVBRBX] = a->pr_cfdata[pr_id][FVBRTX];
			a->pr_cfdata[pr_id][FVBRBY] = a->pr_cfdata[pr_id][FVBLBY];
			a->pr_cfdata[pr_id][FCBRTX] = a->pr_cfdata[pr_id][FCBRIGHT0];
			a->pr_cfdata[pr_id][FCBRTY] = a->pr_cfdata[pr_id][FCBTOP0];
			a->pr_cfdata[pr_id][FCBLBX] = a->pr_cfdata[pr_id][FCBLEFT0];
			a->pr_cfdata[pr_id][FCBLBY] = a->pr_cfdata[pr_id][FCBBOTTOM0];
			a->pr_cfdata[pr_id][FCBLTX] = a->pr_cfdata[pr_id][FCBLBX];
			a->pr_cfdata[pr_id][FCBLTY] = a->pr_cfdata[pr_id][FCBRTY];
			a->pr_cfdata[pr_id][FCBRBX] = a->pr_cfdata[pr_id][FCBRTX];
			a->pr_cfdata[pr_id][FCBRBY] = a->pr_cfdata[pr_id][FCBLBY];
		}
		a->pr_cidata[pr_id][ICUSTOMFLAGS] &= ~IRECALCULATELOGICRECTSFLAG;
	}
}
#endif
// ----------------------------------------------------------------------------------

static void _prGetCollideRect(s32 pr_id, fx32 rect[RECT_SIZE], struct TEngineInstance* ainst)
{
	if (ainst->all_pr_count > pr_id)
	{
		SDK_NULL_ASSERT(ainst->pr_cfdata);
#ifdef CUSTOMAFFINETRANSFORM_SUPPORT
		_calcLogicRectCoortinates(ainst, pr_id);
		rect[RECT_LEFT_TOP_X] = ainst->pr_cfdata[pr_id][FCBLTX];
		rect[RECT_LEFT_TOP_Y] = ainst->pr_cfdata[pr_id][FCBLTY];
		rect[RECT_RIGHT_TOP_X] = ainst->pr_cfdata[pr_id][FCBRTX];
		rect[RECT_RIGHT_TOP_Y] = ainst->pr_cfdata[pr_id][FCBRTY];
		rect[RECT_RIGHT_BOTTOM_X] = ainst->pr_cfdata[pr_id][FCBRBX];
		rect[RECT_RIGHT_BOTTOM_Y] = ainst->pr_cfdata[pr_id][FCBRBY];
		rect[RECT_LEFT_BOTTOM_X] = ainst->pr_cfdata[pr_id][FCBLBX];
		rect[RECT_LEFT_BOTTOM_Y] = ainst->pr_cfdata[pr_id][FCBLBY];
#else
		rect[RECT_LEFT_TOP_Y] = ainst->pr_cfdata[pr_id][FCBTOP0];
		rect[RECT_LEFT_TOP_X] = ainst->pr_cfdata[pr_id][FCBLEFT0];
		rect[RECT_RIGHT_TOP_X] = ainst->pr_cfdata[pr_id][FCBRIGHT0];
		rect[RECT_RIGHT_TOP_Y] = rect[RECT_LEFT_TOP_Y];
		rect[RECT_RIGHT_BOTTOM_X] = rect[RECT_RIGHT_TOP_X];
		rect[RECT_RIGHT_BOTTOM_Y] = ainst->pr_cfdata[pr_id][FCBBOTTOM0];
		rect[RECT_LEFT_BOTTOM_X] = rect[RECT_LEFT_TOP_X];
		rect[RECT_LEFT_BOTTOM_Y] = rect[RECT_RIGHT_BOTTOM_Y];
#endif
	}
}
// ----------------------------------------------------------------------------------

void prGetCollideRectLayer(s32 pr_id, fx32 rect[RECT_SIZE], u32 layer)
{
	struct TEngineInstance* l = _getInstance(layer);
	_prGetCollideRect(pr_id, rect, l);
}
// ----------------------------------------------------------------------------------

void prGetCollideRect(s32 pr_id, fx32 rect[RECT_SIZE])
{
	_prGetCollideRect(pr_id, rect, cd.ainst);
}
// ----------------------------------------------------------------------------------

static void _prGetViewRect(s32 pr_id, fx32 rect[RECT_SIZE], struct TEngineInstance* ainst)
{
	if (ainst->all_pr_count > pr_id)
	{
		SDK_NULL_ASSERT(ainst->pr_cfdata);
#ifdef CUSTOMAFFINETRANSFORM_SUPPORT
		_calcLogicRectCoortinates(ainst, pr_id);
		rect[RECT_LEFT_TOP_X] = ainst->pr_cfdata[pr_id][FVBLTX];
		rect[RECT_LEFT_TOP_Y] = ainst->pr_cfdata[pr_id][FVBLTY];
		rect[RECT_RIGHT_TOP_X] = ainst->pr_cfdata[pr_id][FVBRTX];
		rect[RECT_RIGHT_TOP_Y] = ainst->pr_cfdata[pr_id][FVBRTY];
		rect[RECT_RIGHT_BOTTOM_X] = ainst->pr_cfdata[pr_id][FVBRBX];
		rect[RECT_RIGHT_BOTTOM_Y] = ainst->pr_cfdata[pr_id][FVBRBY];
		rect[RECT_LEFT_BOTTOM_X] = ainst->pr_cfdata[pr_id][FVBLBX];
		rect[RECT_LEFT_BOTTOM_Y] = ainst->pr_cfdata[pr_id][FVBLBY];
#else
		rect[RECT_LEFT_TOP_Y] = ainst->pr_cfdata[pr_id][FVBTOP0];
		rect[RECT_LEFT_TOP_X] = ainst->pr_cfdata[pr_id][FVBLEFT0];
		rect[RECT_RIGHT_TOP_X] = ainst->pr_cfdata[pr_id][FVBRIGHT0];
		rect[RECT_RIGHT_TOP_Y] = rect[RECT_LEFT_TOP_Y];
		rect[RECT_RIGHT_BOTTOM_X] = rect[RECT_RIGHT_TOP_X];
		rect[RECT_RIGHT_BOTTOM_Y] = ainst->pr_cfdata[pr_id][FVBBOTTOM0];
		rect[RECT_LEFT_BOTTOM_X] = rect[RECT_LEFT_TOP_X];
		rect[RECT_LEFT_BOTTOM_Y] = rect[RECT_RIGHT_BOTTOM_Y];
#endif
	}
}
// ----------------------------------------------------------------------------------

void prGetViewRectLayer(s32 pr_id, fx32 rect[RECT_SIZE], u32 layer)
{
	struct TEngineInstance* l = _getInstance(layer);
	_prGetViewRect(pr_id, rect, l);
}
// ----------------------------------------------------------------------------------

void prGetViewRect(s32 pr_id, fx32 rect[RECT_SIZE])
{
	_prGetViewRect(pr_id, rect, cd.ainst);
}
// ----------------------------------------------------------------------------------

void low_getCollideRect(s32 pr_id, fx32 rect[RECT_SIZE], u32 layer)
{
	struct TEngineInstance *l = _getInstance(layer);
	if(l->all_pr_count > pr_id)
	{
		SDK_NULL_ASSERT(l->pr_cfdata);
#ifdef CUSTOMAFFINETRANSFORM_SUPPORT
		_calcLogicRectCoortinates(l, pr_id);
		rect[RECT_LEFT_TOP_X] = l->pr_cfdata[pr_id][FCBLTX];
		rect[RECT_LEFT_TOP_Y] = l->pr_cfdata[pr_id][FCBLTY];
		rect[RECT_RIGHT_TOP_X] = l->pr_cfdata[pr_id][FCBRTX];
		rect[RECT_RIGHT_TOP_Y] = l->pr_cfdata[pr_id][FCBRTY];
		rect[RECT_RIGHT_BOTTOM_X] = l->pr_cfdata[pr_id][FCBRBX];
		rect[RECT_RIGHT_BOTTOM_Y] = l->pr_cfdata[pr_id][FCBRBY];
		rect[RECT_LEFT_BOTTOM_X] = l->pr_cfdata[pr_id][FCBLBX];
		rect[RECT_LEFT_BOTTOM_Y] = l->pr_cfdata[pr_id][FCBLBY];
#else
		rect[RECT_LEFT_TOP_Y] = l->pr_cfdata[pr_id][FCBTOP0];
		rect[RECT_LEFT_TOP_X] = l->pr_cfdata[pr_id][FCBLEFT0];
		rect[RECT_RIGHT_TOP_X] = l->pr_cfdata[pr_id][FCBRIGHT0];
		rect[RECT_RIGHT_TOP_Y] = rect[RECT_LEFT_TOP_Y];
		rect[RECT_RIGHT_BOTTOM_X] = rect[RECT_RIGHT_TOP_X];
		rect[RECT_RIGHT_BOTTOM_Y] = l->pr_cfdata[pr_id][FCBBOTTOM0];
		rect[RECT_LEFT_BOTTOM_X] = rect[RECT_LEFT_TOP_X];
		rect[RECT_LEFT_BOTTOM_Y] = rect[RECT_RIGHT_BOTTOM_Y];
#endif
	}
}
// ----------------------------------------------------------------------------------

void low_getViewRect(s32 pr_id, fx32 rect[RECT_SIZE], u32 layer)
{
	struct TEngineInstance *l = _getInstance(layer);
	if(l->all_pr_count > pr_id)
	{
		SDK_NULL_ASSERT(l->pr_cfdata);
#ifdef CUSTOMAFFINETRANSFORM_SUPPORT
		_calcLogicRectCoortinates(l, pr_id);
		rect[RECT_LEFT_TOP_X] = l->pr_cfdata[pr_id][FVBLTX];
		rect[RECT_LEFT_TOP_Y] = l->pr_cfdata[pr_id][FVBLTY];
		rect[RECT_RIGHT_TOP_X] = l->pr_cfdata[pr_id][FVBRTX];
		rect[RECT_RIGHT_TOP_Y] = l->pr_cfdata[pr_id][FVBRTY];
		rect[RECT_RIGHT_BOTTOM_X] = l->pr_cfdata[pr_id][FVBRBX];
		rect[RECT_RIGHT_BOTTOM_Y] = l->pr_cfdata[pr_id][FVBRBY];
		rect[RECT_LEFT_BOTTOM_X] = l->pr_cfdata[pr_id][FVBLBX];
		rect[RECT_LEFT_BOTTOM_Y] = l->pr_cfdata[pr_id][FVBLBY];
#else
		rect[RECT_LEFT_TOP_Y] = l->pr_cfdata[pr_id][FVBTOP0];
		rect[RECT_LEFT_TOP_X] = l->pr_cfdata[pr_id][FVBLEFT0];
		rect[RECT_RIGHT_TOP_X] = l->pr_cfdata[pr_id][FVBRIGHT0];
		rect[RECT_RIGHT_TOP_Y] = rect[RECT_LEFT_TOP_Y];
		rect[RECT_RIGHT_BOTTOM_X] = rect[RECT_RIGHT_TOP_X];
		rect[RECT_RIGHT_BOTTOM_Y] = l->pr_cfdata[pr_id][FVBBOTTOM0];
		rect[RECT_LEFT_BOTTOM_X] = rect[RECT_LEFT_TOP_X];
		rect[RECT_LEFT_BOTTOM_Y] = rect[RECT_RIGHT_BOTTOM_Y];
#endif
	}
}
// ----------------------------------------------------------------------------------

void prTeleportToTileNo(s32 id, s32 tileNo, char align)
{
	s32 x, y;
	struct fxVec2 pos;
	SDK_ASSERT(cd.ainst->map_w >= 0);
	y = tileNo / cd.ainst->map_w;
	x = tileNo % cd.ainst->map_w;
	if (x <= cd.ainst->map_w && x >= 0 && y >= 0 && y <= cd.ainst->map_h)
	{
		pos.x = FX32(x * cd.p_w);
		pos.y = FX32(y * cd.p_h);
		pos.y = getYBGTileAt(pos.y, align);
		pos.x = getXBGTileAt(pos.x, align);
		prSetPosition(id, pos);
    }
}
// ----------------------------------------------------------------------------------

#ifdef TENGINE_LEGACY_CODE
void prRunAssignedScript(s32 pr, u16 pr_spt_id)
{
	SDK_NULL_ASSERT(cd.ainst->pr_spt);
	startScript(cd.ainst->pr_spt[pr][pr_spt_id], TRUE, pr, -1);
}
// ----------------------------------------------------------------------------------
#endif

fx32 getYBGTileAt(fx32 y, char align)
{
	y = (fx2int(y) / cd.p_h) * cd.p_h;
	switch(align)
	{
		case ALIGN_DOWN:
			y += cd.p_h;
			break;
		case ALIGN_LEFT:
		case ALIGN_RIGHT:
		case ALIGN_CENTER:
			y += cd.p_h >> 1;
	}
	return FX32(y);
}
// ---------------------------------------------------------------------------

fx32 getXBGTileAt(fx32 x, char align)
{
	x = (fx2int(x) / cd.p_w) * cd.p_w;
	switch(align)
	{
		case ALIGN_RIGHT:
			x += cd.p_w;
			break;
		case ALIGN_UP:
		case ALIGN_DOWN:
		case ALIGN_CENTER:
			x += cd.p_w >> 1;
	}
	return FX32(x);
}
// ---------------------------------------------------------------------------

struct fxVec2 prGetPosition(s32 pr_id)
{
	struct fxVec2 pos;
	SDK_NULL_ASSERT(cd.ainst->pr_fdata);
	pos.x = cd.ainst->pr_fdata[pr_id][FX];
	pos.y = cd.ainst->pr_fdata[pr_id][FY];
	return pos;
}
// ----------------------------------------------------------------------------------

struct fxVec2 prGetPositionLayer(s32 pr_id, u32 layer)
{
	struct fxVec2 pos;
	struct TEngineInstance *l = _getInstance(layer);
	SDK_NULL_ASSERT(l->pr_fdata);
	pos.x = l->pr_fdata[pr_id][FX];
	pos.y = l->pr_fdata[pr_id][FY];
	return pos;
}
// ---------------------------------------------------------------------------

void prSetPosition(s32 id, struct fxVec2 pos)
{
	SDK_NULL_ASSERT(cd.ainst->pr_fdata);
	SDK_ASSERT(cd.ainst->all_pr_count > id);
	prAddXYToPosition(id, pos.x - cd.ainst->pr_fdata[id][FX], pos.y - cd.ainst->pr_fdata[id][FY]);
	if(cd.ainst->all_pr_count > id)
	{
		SDK_NULL_ASSERT(cd.ainst->pr_cidata);
		SDK_NULL_ASSERT(cd.ainst->pr_cfdata);
		cd.ainst->pr_cidata[id][IPREVNODE] = NONE_MAP_IDX;
		cd.ainst->pr_cidata[id][INODE] = NONE_MAP_IDX;
		cd.ainst->pr_cfdata[id][FX0_NODE] = cd.ainst->pr_fdata[id][FX];
		cd.ainst->pr_cfdata[id][FY0_NODE] = cd.ainst->pr_fdata[id][FY];
	}
}
// ----------------------------------------------------------------------------------

void prSetPositionLayer(s32 id, struct fxVec2 pos, u32 layer)
{
	struct TEngineInstance *l = _getInstance(layer);
	SDK_NULL_ASSERT(l->pr_fdata);
	SDK_ASSERT(l->all_pr_count > id);
	low_addXYToPosition(id, pos.x - l->pr_fdata[id][FX], pos.y - l->pr_fdata[id][FY], layer);
	if(l->all_pr_count > id)
	{
		SDK_NULL_ASSERT(cd.ainst->pr_cidata);
		SDK_NULL_ASSERT(cd.ainst->pr_cfdata);
		l->pr_cidata[id][IPREVNODE] = NONE_MAP_IDX;
		l->pr_cidata[id][INODE] = NONE_MAP_IDX;
		l->pr_cfdata[id][FX0_NODE] = l->pr_fdata[id][FX];
		l->pr_cfdata[id][FY0_NODE] = l->pr_fdata[id][FY];
	}
}
// ---------------------------------------------------------------------------

void prAddXYToPosition(s32 pr_id, fx32 xv, fx32 yv)
{
	SDK_ASSERT(cd.ainst->all_pr_count > pr_id);
	cd.ainst->pr_cfdata[pr_id][FDISTLENGHT] = -FX32_ONE;
	_addXYToPosition(cd.ainst, pr_id, xv, yv);
}
// ---------------------------------------------------------------------------

void _addXYToPosition(struct TEngineInstance *a, s32 pr_id, fx32 xv, fx32 yv)
{
	if(a->all_pr_count > pr_id)
	{
		SDK_NULL_ASSERT(a->pr_cidata);
		if(a->pr_cidata[pr_id][ILINKTOOBJ] != MAX_RES_IDX)
		{
			return;
		}
	}
	_addXY(a, pr_id, xv, yv);
}
// ---------------------------------------------------------------------------

void _calculatePositionAndRotation(struct TEngineInstance *a, const fx32* pt, s32 pr_id, s32 idx1, s32 idx2, s32 idx3, fx32 pos[2])
{
	_calculatePositionAndRotationJN(a, pt, pr_id, idx1, idx2, pos);
	pos[FX] -= a->pr_fdata[a->pr_cidata[pr_id][idx3]][FX];
	pos[FY] -= a->pr_fdata[a->pr_cidata[pr_id][idx3]][FY];
}
// ---------------------------------------------------------------------------

void _calculatePositionAndRotationJN(struct TEngineInstance *a, const fx32* pt, s32 pr_id, s32 idxNX, s32 idxNY, fx32 pos[2])
{
	fx32 offx, offy;
#ifdef CUSTOMAFFINETRANSFORM_SUPPORT
	SDK_NULL_ASSERT(a->pr_cidata);
	if ((a->pr_cidata[pr_id][ICUSTOMFLAGS] & ICUSTOMROTATIONFLAG) == ICUSTOMROTATIONFLAG ||
		(a->pr_cidata[pr_id][ICUSTOMFLAGS] & ICUSTOMSCALEFLAG) == ICUSTOMSCALEFLAG)
	{
		fx32 fsin, fcos, sx, sy;
		SDK_NULL_ASSERT(a->pr_cfdata);
		sx = FX_Mul(pt[idxNX], fxAbs(a->pr_cfdata[pr_id][FCUSTOMSCALEXVALUE]));
		sy = FX_Mul(pt[idxNY], fxAbs(a->pr_cfdata[pr_id][FCUSTOMSCALEYVALUE]));
		fsin = a->pr_cfdata[pr_id][FROTSIN];
		fcos = a->pr_cfdata[pr_id][FROTCOS];
		offx = FX_Mul(sx, fcos) + FX_Mul(sy, -fsin);
		offy = FX_Mul(sx, fsin) + FX_Mul(sy, fcos);
	}
	else
	{
		offx = pt[idxNX];
		offy = pt[idxNY];
	}
#else
	offx = pt[idxNX];
	offy = pt[idxNY];
#endif
	SDK_NULL_ASSERT(a->pr_fdata);
	pos[FX] = a->pr_fdata[pr_id][FX] + offx;
	pos[FY] = a->pr_fdata[pr_id][FY] + offy;
}
// ---------------------------------------------------------------------------

void _addXY(struct TEngineInstance *a, s32 pr_id, fx32 xv, fx32 yv)
{
	if(a->all_pr_count > pr_id)
	{
		s32 i;
		const fx32* pt;
		fx32 pos[2];
		SDK_NULL_ASSERT(a->pr_idata);
		SDK_NULL_ASSERT(a->pr_fdata);
		SDK_NULL_ASSERT(a->pr_cfdata);
		SDK_NULL_ASSERT(a->pr_cidata);
		a->pr_fdata[pr_id][FX] += xv;
		a->pr_fdata[pr_id][FY] += yv;
		a->pr_fdata[pr_id][FLEFT] += xv;
		a->pr_fdata[pr_id][FTOP] += yv;
		a->pr_cfdata[pr_id][FCBLEFT0] += xv;
		a->pr_cfdata[pr_id][FCBTOP0] += yv;
		a->pr_cfdata[pr_id][FCBRIGHT0] += xv;
		a->pr_cfdata[pr_id][FCBBOTTOM0] += yv;
		a->pr_cfdata[pr_id][FVBLEFT0] += xv;
		a->pr_cfdata[pr_id][FVBTOP0] += yv;
		a->pr_cfdata[pr_id][FVBRIGHT0] += xv;
		a->pr_cfdata[pr_id][FVBBOTTOM0] += yv;
		if(xv != 0 || yv != 0)
		{
			a->pr_cidata[pr_id][ICUSTOMFLAGS] |= IRECALCULATELOGICRECTSFLAG;
		}
		if(a->pr_idata[pr_id][IPARENT_IDX] == PARENT_TEXT_OBJ)
		{
			pt = fakeTextFrData;
		}
		else
		{
			pt = cd.ppr_fr_data[a->pr_idata[pr_id][IPARENT_IDX]]
								[_prGetAnimationId(a, pr_id)]
								[a->pr_idata[pr_id][IANIFRIDX]];
        }
		for (i = 0; i < cd.jn_ct; i++)
		{
			if (a->pr_cidata[pr_id][IJOINNODEBEGIN + i] != MAX_RES_IDX)
			{
				_calculatePositionAndRotation(a, pt, pr_id, ANIFRJNBEGIN + i * 2, ANIFRJNBEGIN + 1 + i * 2, IJOINNODEBEGIN + i, pos);
				_addXY(a, a->pr_cidata[pr_id][IJOINNODEBEGIN + i], pos[FX], pos[FY]);
			}
		}

	}
	else
	{
		SDK_NULL_ASSERT(a->pr_fdata);
		a->pr_fdata[pr_id][FX] += xv;
		a->pr_fdata[pr_id][FY] += yv;
		a->pr_fdata[pr_id][FLEFT] += xv;
		a->pr_fdata[pr_id][FTOP] += yv;
	}
}
// ----------------------------------------------------------------------------------

u16 prGetAnimationId(s32 pr_id)
{
	return _prGetAnimationId(cd.ainst, pr_id);
}
// ----------------------------------------------------------------------------------

static u16 _prGetAnimationId(struct TEngineInstance *ei, s32 pr_id)
{
	s32 state;
	SDK_NULL_ASSERT(cd.states);
	SDK_NULL_ASSERT(ei->pr_idata);
	state = ei->pr_idata[pr_id][ISTATE];
	SDK_ASSERT(state >= 0); // check with editor for unassigned states
	return cd.states[state][stIDANIMATION];
}
// ----------------------------------------------------------------------------------

s32 getZonesCount(u32 layer)
{
	return _getInstance(layer)->zones_ct;
}
// ----------------------------------------------------------------------------------

void setCurrentZone(u32 layer, s32 zone)
{
	struct TEngineInstance *ei = _getInstance(layer);
	if(ei->zones_ct > zone)
	{
		ei->t_current_zone = zone;
	}
}
// ----------------------------------------------------------------------------------

s32 getCurrentZone(u32 layer)
{
	return _getInstance(layer)->t_current_zone;
}
// ----------------------------------------------------------------------------------

const struct AllocatorList* low_getZoneGameObjList(u32 layer, s32 zone)
{
	struct TEngineInstance *ei = _getInstance(layer);
	if(zone >= 0)
	{
		SDK_NULL_ASSERT(ei->rndr_znlist);
		return ei->rndr_znlist[zone];
	}
	return NULL;
}
// ----------------------------------------------------------------------------------

// common processing

// ----------------------------------------------------------------------------------
#ifdef TENGINE_LEGACY_CODE
void onCollide(s32 pr, s32 opr)
{
	s32 st;
	SDK_ASSERT(pr >= 0);
	SDK_NULL_ASSERT(cd.ainst->pr_spt);
	st = cd.ainst->pr_spt[pr][SPT_ONCOLLIDE];
	startScript(st, FALSE, pr, opr);
}
// ----------------------------------------------------------------------------------

BOOL _checkSptCollide(u16 st, u16 pr, u16 opr)
{
	BOOL find;
	u32 i, len;
	s32 p;
	find = cd.ainst->spt_bdata[st][IMULTI] != NONE_MAP_IDX;
	if (find)
	{
		len = cd.ainst->spt_cldgrouplist[st] == NULL ? 0 : cd.ainst->spt_cldgrouplist_ct[st];
		if(opr != NONE_MAP_IDX)
		{
			p = cd.ainst->pr_idata[opr][IPARENT_IDX];
			if (len > 0)
			{
				find = FALSE;
				for (i = 0; i < len; i++)
					if (p == cd.ainst->spt_cldgrouplist[st][i])
					{
						find = TRUE;
						break;
					}
			}
			if (find)
			{
				len = cd.ainst->spt_cldobjlist[st] == NULL ? 0 : cd.ainst->spt_cldobjlist_ct[st];
				if (len > 0)
				{
					find = FALSE;
					for (i = 0; i < len; i++)
						if (opr == cd.ainst->spt_cldobjlist[st][i])
						{
							find = TRUE;
							break;
						}
				}
			}
		}
		if (find && pr != NONE_MAP_IDX)
		{
			p = cd.ainst->spt_bdata[st][IINITIATORPRP];
			if (p != NONE_MAP_IDX)
				find = _logicalOper(cd.ainst->pr_prp[pr][p], FX32(cd.ainst->spt_bdata[st][IINITIATORPVAL]), cd.ainst->spt_bdata[st][IINITIATORPOPER]);
		}
		if (find && opr != NONE_MAP_IDX)
		{
			p = cd.ainst->spt_bdata[st][ISECONDPRP];
			if (p != NONE_MAP_IDX)
				find = _logicalOper(cd.ainst->pr_prp[opr][p], FX32(cd.ainst->spt_bdata[st][ISECONDPVAL]), cd.ainst->spt_bdata[st][ISECONDPOPER]);
		}
		if (find && pr != NONE_MAP_IDX)
		{
			p = cd.ainst->spt_bdata[st][IINITIATORSTATE];
			if (p != NONE_MAP_IDX)
					find = _logicalOper(cd.ainst->pr_idata[pr][ISTATE], p, cd.ainst->spt_bdata[st][IINITIATORSOPER]);
		}
		if (find && opr != NONE_MAP_IDX)
		{
			p = cd.ainst->spt_bdata[st][ISECONDSTATE];
			if (p != NONE_MAP_IDX)
				find = _logicalOper(cd.ainst->pr_idata[opr][ISTATE], p, cd.ainst->spt_bdata[st][ISECONDSOPER]);
		}
		return find;
	}
	return FALSE;
}
// ----------------------------------------------------------------------------------

BOOL _checkSptDirector(u16 st, u16 pr, u16 dir)
{
	s32 p;
	BOOL find;
	find = cd.ainst->spt_bdata[st][IMULTI] != NONE_MAP_IDX;
	if(pr != NONE_MAP_IDX)
	{
		if (find)
		{
			p = cd.ainst->spt_bdata[st][IINITIATORPRP];
			if (p != NONE_MAP_IDX)
				find = _logicalOper(cd.ainst->pr_prp[pr][p], FX32(cd.ainst->spt_bdata[st][IINITIATORPVAL]), cd.ainst->spt_bdata[st][IINITIATORPOPER]);
		}
		if (find)
		{
			p = cd.ainst->spt_bdata[st][IINITIATORSTATE];
			if (p != NONE_MAP_IDX)
				find = _logicalOper(cd.ainst->pr_idata[pr][ISTATE], p, cd.ainst->spt_bdata[st][IINITIATORSOPER]);
		}
	}
	if (find && dir != NONE_MAP_IDX)
	{
		p = cd.ainst->spt_bdata[st][ISECONDPRP];
		if (p != NONE_MAP_IDX)
			find = dir == p;
	}
	return find;
}
// ----------------------------------------------------------------------------------

BOOL _checkSptProperty(u16 st, u16 pr)
{
	s32 p;
	BOOL find;
	find = cd.ainst->spt_bdata[st][IMULTI] != NONE_MAP_IDX;
	if (find && pr != NONE_MAP_IDX)
	{
		p = cd.ainst->spt_bdata[st][IINITIATORPRP];
		if (p != NONE_MAP_IDX)
			find = _logicalOper(cd.ainst->pr_prp[pr][p], FX32(cd.ainst->spt_bdata[st][IINITIATORPVAL]), cd.ainst->spt_bdata[st][IINITIATORPOPER]);
	}
	return find;
}
// ----------------------------------------------------------------------------------

BOOL _logicalOper(fx32 val1, fx32 val2, s32 math_oper)
{
	switch(math_oper)
	{
	case SMT_LESSEQ:
		return val1 <= val2;
	case SMT_EQUAL:
		return val1 == val2;
	case SMT_MOREEQ:
		return val1 >= val2;
	case SMT_NOT:
		return val1 != val2;
	}
	return FALSE;
}
// ----------------------------------------------------------------------------------

void startScript(s32 script_id, BOOL force, s32 owner, s32 second)
{
	u16 spt;
	u32 i, len;
	s32 idx;
	
	spt = (u16)script_id;
	if (spt == NONE_MAP_IDX)
	{
		return;	
	}
	
	SDK_NULL_ASSERT(cd.ainst->spt_bdata);	
	
	if(!force)
	{
		switch(cd.ainst->spt_bdata[spt][ISPTTYPE])
		{	
			case SPTTYPE_COLLIDE:
				if(!_checkSptCollide(spt, (u16)owner, (u16)second))
				{
					return;
				}
			break;
			case SPTTYPE_DIRECTOR:
				if(!_checkSptDirector(spt, (u16)owner, (u16)second))
				{
					return;
				}
				second = -1;
			break;
			case SPTTYPE_PROPERTY:
				if(!_checkSptProperty(spt, (u16)owner))
				{
					return;
				}
				second = -1;
		}
		if (cd.ainst->spt_bdata[spt][IMULTI] == NONE_MAP_IDX)
		{
			return;
		}
		if (cd.ainst->spt_bdata[spt][IMULTI] == 0)
		{
			cd.ainst->spt_bdata[spt][IMULTI] = NONE_MAP_IDX;
		}
	}
	len = cd.ainst->spt_triglist_ct[spt];
	for (i = 0; i < len; i++)
	{
		_doTrigger(cd.ainst->spt_triglist[spt][i], owner, second);
	}
	if (cd.ainst->spt_bdata[spt][IGOTOZONE] != NONE_MAP_IDX)
	{
		cd.ainst->t_current_zone = cd.ainst->current_zone = cd.ainst->spt_bdata[spt][IGOTOZONE];
		//if (cd.ainst->_onChangeZoneCB != NULL)
		//{
		//	cd.ainst->_onChangeZoneCB();
		//}
	}
	idx = cd.ainst->spt_bdata[spt][ISWITCHTO];
	if (idx != NONE_MAP_IDX)
	{
		if (cd.ainst->_onChangeHeroCB != NULL)
		{
			cd.ainst->_onChangeHeroCB(idx, owner);
		}
	}
	if (cd.ainst->_onScriptCB != NULL)
	{
		struct ScriptCallbackData dt;
		dt.scriptId = spt;
		dt.initiatorId = owner;
		dt.gotoLevel =  cd.ainst->spt_bdata[spt][IENDLEVEL];
		dt.textMessageId = cd.ainst->spt_bdata[spt][IMESSAGE];
		cd.ainst->_onScriptCB(&dt);
	}
}
// ----------------------------------------------------------------------------------

void _onChangeDirector(s32 pr, s32 dir)
{
	s32 st;
	st = cd.ainst->pr_spt[pr][SPT_ONCHANGENODE];
	startScript(st, FALSE, pr, dir);
}
// ----------------------------------------------------------------------------------

static BOOL _onEndAnimation(s32 mp)
{
	s32 st = cd.ainst->pr_spt[mp][SPT_ONENDANIM];
	if (st != NONE_MAP_IDX)
	{
		cd.ainst->pr_spt[mp][SPT_ONENDANIM] = NONE_MAP_IDX;
#ifdef TENGINE_LEGACY_CODE
		startScript(st, FALSE, mp, -1);
#endif
		return TRUE;
	}
	return FALSE;
}
#endif
// ----------------------------------------------------------------------------------

void _doTrigger(s32 trig_id, s32 owner, s32 second)
{
	s32 i;
	struct TEngineInstance *ei = cd.ainst;
	SDK_NULL_ASSERT(ei->trig_idata);
	SDK_NULL_ASSERT(ei->trig_fdata);
	switch(ei->trig_idata[trig_id][ITRIG_TYPE])
	{
	case TRIG_MAP_TYPE:
		{
			s32 idx, len;
			fx32 x, y;
			x = ei->trig_fdata[trig_id][FX];
			y = ei->trig_fdata[trig_id][FY];
			len = ei->trig_idata[trig_id][ITRIG_MCOUNT_POBJ];
			idx = (fx2int(y) / cd.p_h) * ei->map_w + fx2int(x) / cd.p_w;
			for (i = 0; i < len; i++)
			{
				if (idx >= 0 && idx < ei->map_w * ei->map_h && ei->tilemap != NULL)
				{
					ei->tilemap[idx] = (u16)ei->trig_idata[trig_id][ITRIG_MIDX_POPER];
                }
				switch(ei->trig_idata[trig_id][ITRIG_MDIR_PTILE])
				{
				case TRIG_UP_DIR:
					idx -= ei->map_w;
					break;
				case TRIG_DOWN_DIR:
					idx += ei->map_w;
					break;
				case TRIG_LEFT_DIR:
					idx--;
					break;
				case TRIG_RIGHT_DIR:
					idx++;
				}
			}
			refreshAllScreen();
		}
	break;

	case TRIG_PROPERTIES_TYPE:
		{
			s32 pt = ei->trig_idata[trig_id][ITRIG_TARGET];
			if (TRIG_CHANGE_PRP_INITIATOR == pt)
			{
				pt = (u16)owner;
			}
			else if (TRIG_CHANGE_PRP_SECOND == pt)
			{
				pt = (u16)second;
			}
			if (pt != NONE_MAP_IDX)
			{
				if (ei->trig_st_ct[trig_id] > 0)
				{
					i = ei->trig_st[trig_id][mthGetRandom(ei->trig_st_ct[trig_id])];
					if (TRIG_CHANGE_PRP_OPPOSITESTATE == i)
					{
						prSetOppositeState(pt, SBM_NONE);
					}
					else
					{
						prSetState(pt, i, SBM_NONE);
					}
				}
				if (ei->trig_spt[trig_id] != NULL)
				{
					for (i = 0; i < SPT_COUNT; i++)
					{
						u16 prpname = ei->trig_spt[trig_id][i];
						if (prpname != NONE_MAP_IDX)
						{
							if (TRIG_CHANGE_PRP_BLANKACTION == prpname)
							{
								prpname = NONE_MAP_IDX;
							}
							ei->pr_spt[pt][i] = prpname;
						}
					}
				}
				i = ei->trig_idata[trig_id][ITRIG_MCOUNT_POBJ] & 0xffff;
				if (i != NONE_MAP_IDX)
				{
					fx32 x, y, ox, oy;
					x = ei->nodes_fdata[i][FX];
					y = ei->nodes_fdata[i][FY];
					ox = ei->pr_fdata[pt][FX];
					oy = ei->pr_fdata[pt][FY];
					prAddXYToPosition(pt, x - ox, y - oy);
					ei->pr_cidata[pt][IPREVNODE] = NONE_MAP_IDX;
					ei->pr_cidata[pt][INODE] = i;
					ei->pr_cfdata[pt][FX0_NODE] = ei->pr_fdata[pt][FX];
					ei->pr_cfdata[pt][FY0_NODE] = ei->pr_fdata[pt][FY];
				}

				i = ei->trig_idata[trig_id][ITRIG_MDIR_PTILE] & 0xffff;
				if (i != NONE_MAP_IDX)
				{
					prTeleportToTileNo(pt, i, ALIGN_CENTER);
                }

				{
					s32 len;
					len = (s32)ei->trig_prp_ct[trig_id];
					for (i = 0; i < len; i += 2)
					{
						fx32 val, val1;
						const s32 var = fx2int(ei->trig_prp[trig_id][i]);
						val = ei->trig_prp[trig_id][i + 1];
						switch(ei->trig_idata[trig_id][ITRIG_MIDX_POPER])
						{
						case TRIG_CHANGE_PRP_TRANSMIT:
							prSetProperty(pt, var, val);
							break;
						case TRIG_CHANGE_PRP_DECREASE:
							val1 = ei->pr_prp[pt][var];
							val1 -= val;
							prSetProperty(pt, var, val1);
							break;
						case TRIG_CHANGE_PRP_INCREASE:
							val1 = ei->pr_prp[pt][var];
							val1 += val;
							prSetProperty(pt, var, val1);
						}
					}
				}

			}
		} // end case trPROPERTIES
	} // end switch(trigger->Type)
}
// ----------------------------------------------------------------------------------

void _processMovementBetweenPathNodes(s32 pr, fx32 in_speed)
{
	s32 n;
	if(cd.ainst->all_pr_count <= pr)
	{
    	return;
	}
	SDK_NULL_ASSERT(cd.ainst->nodes_fdata);
	SDK_NULL_ASSERT(cd.ainst->pr_cidata);
	n = cd.ainst->pr_cidata[pr][INODE];
	if(NONE_MAP_IDX != n)
	{	
		fx32 x0, y0, tx, ty, px, py, px0, py0;
		fx32 out_moving[2];
		tx = cd.ainst->nodes_fdata[n][FX];
		ty = cd.ainst->nodes_fdata[n][FY];
		x0 = cd.ainst->pr_cfdata[pr][FX0_NODE];
		y0 = cd.ainst->pr_cfdata[pr][FY0_NODE];
		out_moving[FX] = px = px0 = cd.ainst->pr_fdata[pr][FX];
		out_moving[FY] = py = py0 = cd.ainst->pr_fdata[pr][FY];
		if(cd.ainst->nodes[n][NODE_RECALCULATE_DIST_FLAG] != 0 || cd.ainst->pr_cfdata[pr][FDISTLENGHT] < 0)
		{
			cd.ainst->nodes[n][NODE_RECALCULATE_DIST_FLAG] = 0;
			cd.ainst->pr_cfdata[pr][FDISTLENGHT] = _calcLenght(x0, y0, tx, ty);
		}
		if(!_nextPointAtLine(x0, y0, tx, ty, &cd.ainst->pr_cfdata[pr][FDISTLENGHT], in_speed, out_moving))
		{
			if(!_setNextDirectorPos(pr, n))
			{
				px = tx - px0;
				py = ty - py0;
				_addXYToPosition(cd.ainst, pr, px, py);
				return;
			}
		}
		px = out_moving[FX] - px0;
		py = out_moving[FY] - py0;
		_addXYToPosition(cd.ainst, pr, px, py);
    }
}
// ----------------------------------------------------------------------------------

fx32 _calcLenght(fx32 x0, fx32 y0, fx32 tx, fx32 ty)
{
	s32 dx, dy;
	dx = fx2int(tx - x0);
	dy = fx2int(ty - y0);
	return FX32(mthSqrtI(dx * dx + dy * dy));
}
// ----------------------------------------------------------------------------------

BOOL _nextPointAtLine(fx32 x0, fx32 y0, fx32 tx, fx32 ty, fx32 *l, fx32 speed, fx32 pos[2])
{
	SDK_ASSERT(speed >= 0);
	if(*l < 0)
	{
		*l = _calcLenght(x0, y0, tx, ty);
    }
	if(*l > 0)
	{
		fx32 dx, dy, s, dx_s, dy_s;
		dx_s = FX_Div(FX_Mul(speed, (tx - x0)), *l);
		dy_s = FX_Div(FX_Mul(speed, (ty - y0)), *l);
		pos[FX] += dx_s;
		pos[FY] += dy_s;
		dx = tx - pos[FX];
		dy = ty - pos[FY];
		s = speed + speed + FX32_ONE;
		if(fxAbs(dx) > s || fxAbs(dy) > s)
		{
			return TRUE;
		}
		else
		{
			fx32 l1, l2, ls;
			l2 = FX_Mul(dx, dx) + FX_Mul(dy, dy);
			dx += dx_s;
			dy += dy_s;			
			l1 = FX_Mul(dx, dx) + FX_Mul(dy, dy);
			ls = l1 - l2;
			if(ls > 0 && ls > fxAbs(dx_s) / 2 && ls > fxAbs(dy_s) / 2)
			{
				return TRUE;
			}
		}
	}
	return FALSE;
}
// ----------------------------------------------------------------------------------

BOOL nextPointAtLine(struct fxVec2 start, struct fxVec2 end, fx32 speed, struct fxVec2 *pos)
{
	BOOL res;
	fx32 p[2];
	fx32 l = -FX32_ONE;
	SDK_NULL_ASSERT(pos);
	p[FX] = pos->x;
	p[FY] = pos->y;
	res = _nextPointAtLine(start.x, start.y, end.x, end.y, &l, speed, p);
	pos->x = p[FX];
	pos->y = p[FY];
	return res;
}
// ----------------------------------------------------------------------------------

void setPathNodePosition(s32 pn_id, struct fxVec2 pos)
{
	if(cd.ainst->nodes_ct > pn_id)
	{
		SDK_NULL_ASSERT(cd.ainst->nodes_fdata);
		SDK_NULL_ASSERT(cd.ainst->nodes);
		cd.ainst->nodes_fdata[pn_id][FX] = pos.x;
		cd.ainst->nodes_fdata[pn_id][FY] = pos.y;
		cd.ainst->nodes[pn_id][NODE_RECALCULATE_DIST_FLAG] = 1;
	}
}
// ----------------------------------------------------------------------------------

struct fxVec2 getPathNodePosition(s32 pn_id)
{
	if(cd.ainst->nodes_ct > pn_id)
	{
		struct fxVec2 pos;
		SDK_NULL_ASSERT(cd.ainst->nodes_fdata);
		pos.x = cd.ainst->nodes_fdata[pn_id][FX];
		pos.y = cd.ainst->nodes_fdata[pn_id][FY];
		return pos;
	}
	else
	{
		struct fxVec2 pos;
		pos.x = pos.y = 0;
		return pos;
	}
}
// ----------------------------------------------------------------------------------

static BOOL _setNextDirectorPos(s32 pr, s32 node)
{
	s32 direction, next_node, prev_node;
	BOOL atSameNode;
	SDK_NULL_ASSERT(cd.ainst->pr_fdata);
	SDK_NULL_ASSERT(cd.ainst->pr_cfdata);
	SDK_NULL_ASSERT(cd.ainst->pr_cidata);
	if (node == NONE_MAP_IDX)
	{
		return FALSE;
	}
	prev_node = cd.ainst->pr_cidata[pr][IPREVNODE];
	atSameNode = node == prev_node;
	cd.ainst->pr_cfdata[pr][FX0_NODE] = cd.ainst->pr_fdata[pr][FX];
	cd.ainst->pr_cfdata[pr][FY0_NODE] = cd.ainst->pr_fdata[pr][FY];
	cd.ainst->pr_cfdata[pr][FDISTLENGHT] = -FX32_ONE;
	if (!atSameNode)
	{
		const s32 trig = cd.ainst->nodes[node][NODE_ID_TRIGGER];
#ifdef TENGINE_LEGACY_CODE
		_onChangeDirector(pr, node);
#endif
		if (trig != NONE_MAP_IDX)
		{
			_doTrigger(trig, pr, -1);
        }
	}
	node = cd.ainst->pr_cidata[pr][INODE];
	if (node == NONE_MAP_IDX)
	{
		return FALSE;
	}
	direction = prGetCurrentDirection(pr);
	if (direction > 0)
	{
		next_node = cd.ainst->nodes[node][direction / 2 - 1];
		if (next_node != node && next_node != NONE_MAP_IDX)
		{
			cd.ainst->nodes[next_node][NODE_RECALCULATE_DIST_FLAG] = 1;
		}
	}
	else
	{
		next_node = node;
    }
	cd.ainst->pr_cidata[pr][IPREVNODE] = node;
	cd.ainst->pr_cidata[pr][INODE] = next_node;
	if (!atSameNode && cd._onEventCB != NULL)
	{
		struct EventCallbackData ed;
		ed.eventType = EVENT_TYPE_ON_DIRECTOR;
		ed.layer = (s32)cd.a_layer;
		ed.eventId = cd.ainst->nodes[node][NODE_ID_EVENT];
		ed.ownerId = node;
		ed.initiatorId = pr;
		ed.eventData.mpData = NULL;
		cd._onEventCB(&ed);
	}
	return next_node != node;
}
// ----------------------------------------------------------------------------------

void _processMovement(s32 pr)
{
	if(cd.spd_prp_id != NONE_MAP_IDX)
	{
		s32 next_node = prGetTargetPathNode(pr);
		if(next_node != NONE_MAP_IDX)
		{
			fx32 spd = prGetProperty(pr, cd.spd_prp_id);
			if(spd == 0)
			{
				_addXYToPosition(cd.ainst, pr, 0, 0);
				return;
			}
			_processMovementBetweenPathNodes(pr, FX_Mul(spd, cd.deltatime_scale));
		}
	}
}
//----------------------------------------------------------------------------------

void prSetCustomRotation(s32 pr_id, fx32 deg_angle)
{
	_setCustomRotation(cd.ainst, pr_id, deg_angle);
}
//----------------------------------------------------------------------------------

void prSetCustomRotationLayer(s32 pr_id, fx32 deg_angle, u32 layer)
{
	struct TEngineInstance *l = _getInstance(layer);
	_setCustomRotation(l, pr_id, deg_angle);
}
//----------------------------------------------------------------------------------

void _setCustomRotation(struct TEngineInstance *l, s32 pr_id, fx32 deg_angle)
{
#ifdef CUSTOMAFFINETRANSFORM_SUPPORT
	if(l->all_pr_count > pr_id)
	{
		SDK_NULL_ASSERT(l->pr_cfdata);
		if(deg_angle != l->pr_cfdata[pr_id][FCUSTOMROTATIONVALUE])
		{
			fx32 da;
			SDK_NULL_ASSERT(l->pr_cfdata);
			da = deg_angle - l->pr_cfdata[pr_id][FCUSTOMROTATIONVALUE];
			_addRotationDeltaRecursive(l, pr_id, da);
			_addXY(l, pr_id, 0, 0);
		}
	}
#else
	(void)l;
	(void)pr_id;
	(void)deg_angle;
#endif
}
//----------------------------------------------------------------------------------

fx32 prGetCustomRotation(s32 pr_id)
{
	return _getCustomRotation(cd.ainst, pr_id);
}
//----------------------------------------------------------------------------------

fx32 prGetCustomRotationLayer(s32 pr_id, u32 layer)
{
	struct TEngineInstance *l = _getInstance(layer);
	return _getCustomRotation(l, pr_id);
}
//----------------------------------------------------------------------------------

fx32 _getCustomRotation(struct TEngineInstance *l, s32 pr_id)
{
#ifdef CUSTOMAFFINETRANSFORM_SUPPORT
	if(l->all_pr_count > pr_id)
	{
		SDK_NULL_ASSERT(l->pr_cfdata);
		return l->pr_cfdata[pr_id][FCUSTOMROTATIONVALUE];
	}
#else
	(void)pr_id;
	(void)l;
#endif
	return 0;
}
//----------------------------------------------------------------------------------

#ifdef CUSTOMAFFINETRANSFORM_SUPPORT
void _addRotationDeltaRecursive(struct TEngineInstance *inst, s32 pr_id, fx32 da)
{
	u16 n;
	SDK_NULL_ASSERT(inst->pr_cfdata);
	SDK_NULL_ASSERT(inst->pr_cidata);
	inst->pr_cfdata[pr_id][FCUSTOMROTATIONVALUE] += da;
	inst->pr_cidata[pr_id][ICUSTOMFLAGS] |= ICUSTOMROTATIONFLAG;
	inst->pr_cidata[pr_id][ICUSTOMFLAGS] |= IRECALCULATELOGICRECTSFLAG;
	fxCordic(FX_Mul(FX_Div(inst->pr_cfdata[pr_id][FCUSTOMROTATIONVALUE], FX32(180)), FX_PI),
			 &inst->pr_cfdata[pr_id][FROTSIN], &inst->pr_cfdata[pr_id][FROTCOS], FX_CORDIC_HIGH_PRECISION);
	for (n = 0; n < cd.jn_ct; n++)
	{
		if (inst->pr_cidata[pr_id][IJOINNODEBEGIN + n] != MAX_RES_IDX)
		{
			_addRotationDeltaRecursive(inst, inst->pr_cidata[pr_id][IJOINNODEBEGIN + n], da);
		}
	}
}
#endif
//----------------------------------------------------------------------------------

void prResetRotationToDefault(s32 pr_id)
{
	_resetRotationToDefault(cd.ainst, pr_id);
}
//----------------------------------------------------------------------------------

void prResetRotationToDefaultLayer(s32 pr_id, u32 layer)
{
	struct TEngineInstance *l = _getInstance(layer);
	_resetRotationToDefault(l, pr_id);
}
//----------------------------------------------------------------------------------

void _resetRotationToDefault(struct TEngineInstance *l, s32 pr_id)
{
#ifdef CUSTOMAFFINETRANSFORM_SUPPORT
	if(l->all_pr_count > pr_id)
	{
		u16 n;
		SDK_NULL_ASSERT(l->pr_cfdata);
		SDK_NULL_ASSERT(l->pr_cidata);
		if((l->pr_cidata[pr_id][ICUSTOMFLAGS] & ICUSTOMROTATIONFLAG) == ICUSTOMROTATIONFLAG)
		{
			l->pr_cidata[pr_id][ICUSTOMFLAGS] |= IRECALCULATELOGICRECTSFLAG;
		}
		l->pr_cidata[pr_id][ICUSTOMFLAGS] &= ~ICUSTOMROTATIONFLAG;
		l->pr_cfdata[pr_id][FCUSTOMROTATIONVALUE] = 0;
		l->pr_cfdata[pr_id][FROTSIN] = 0;
		l->pr_cfdata[pr_id][FROTCOS] = FX32_ONE;
		for (n = 0; n < cd.jn_ct; n++)
		{
			if (l->pr_cidata[pr_id][IJOINNODEBEGIN + n] != MAX_RES_IDX)
			{
				_resetRotationToDefault(l, l->pr_cidata[pr_id][IJOINNODEBEGIN + n]);
			}
		}
	}
#else
	(void)l;
	(void)pr_id;
#endif
}
//----------------------------------------------------------------------------------

#ifdef CUSTOMAFFINETRANSFORM_SUPPORT
void _addScaleDeltaRecursive(struct TEngineInstance *l, s32 pr_id, fx32 dax, fx32 day)
{
	u16 n;
	SDK_NULL_ASSERT(l->pr_cfdata);
	SDK_NULL_ASSERT(l->pr_cidata);
	l->pr_cfdata[pr_id][FCUSTOMSCALEXVALUE] += dax;
	l->pr_cfdata[pr_id][FCUSTOMSCALEYVALUE] += day;
	l->pr_cidata[pr_id][ICUSTOMFLAGS] |= ICUSTOMSCALEFLAG;
	l->pr_cidata[pr_id][ICUSTOMFLAGS] |= IRECALCULATELOGICRECTSFLAG;
	for (n = 0; n < cd.jn_ct; n++)
	{
		if (l->pr_cidata[pr_id][IJOINNODEBEGIN + n] != MAX_RES_IDX)
		{
			_addScaleDeltaRecursive(l, l->pr_cidata[pr_id][IJOINNODEBEGIN + n], dax, day);
		}
	}
}
#endif
//----------------------------------------------------------------------------------

void prSetCustomScale(s32 pr_id, struct fxVec2 scaleRatio)
{
	_setCustomScale(cd.ainst, pr_id, scaleRatio);
}
//----------------------------------------------------------------------------------

void prSetCustomScaleLayer(s32 pr_id, struct fxVec2 scaleRatio, u32 layer)
{
	struct TEngineInstance *l = _getInstance(layer);
	_setCustomScale(l, pr_id, scaleRatio);
}
//----------------------------------------------------------------------------------

void _setCustomScale(struct TEngineInstance *l, s32 pr_id, struct fxVec2 scaleRatio)
{
#ifdef CUSTOMAFFINETRANSFORM_SUPPORT
	if(l->all_pr_count > pr_id)
	{
		SDK_NULL_ASSERT(l->pr_cfdata);
		if(scaleRatio.x != l->pr_cfdata[pr_id][FCUSTOMSCALEXVALUE] ||
		   scaleRatio.y != l->pr_cfdata[pr_id][FCUSTOMSCALEYVALUE])
		{
			fx32 dax, day;
			SDK_NULL_ASSERT(l->pr_cfdata);
			dax = scaleRatio.x - l->pr_cfdata[pr_id][FCUSTOMSCALEXVALUE];
			day = scaleRatio.y - l->pr_cfdata[pr_id][FCUSTOMSCALEYVALUE];
			_addScaleDeltaRecursive(l, pr_id, dax, day);
			_addXY(l, pr_id, 0, 0);
		}
	}
#else
	(void)l;
	(void)pr_id;
	(void)scaleRatio;
#endif
}
//----------------------------------------------------------------------------------

struct fxVec2 prGetCustomScale(s32 pr_id)
{
	struct fxVec2 res;
	_getCustomScale(cd.ainst, pr_id, &res);
	return res;
}
//----------------------------------------------------------------------------------

struct fxVec2 prGetCustomScaleLayer(s32 pr_id, u32 layer)
{
	struct fxVec2 res;
	struct TEngineInstance *l = _getInstance(layer);
	_getCustomScale(l, pr_id, &res);
	return res;
}
//----------------------------------------------------------------------------------

void _getCustomScale(struct TEngineInstance *l, s32 pr_id, struct fxVec2 *res)
{
#ifdef CUSTOMAFFINETRANSFORM_SUPPORT
	if(l->all_pr_count > pr_id)
	{
		SDK_NULL_ASSERT(res);
		SDK_NULL_ASSERT(l->pr_cfdata);
		res->x = l->pr_cfdata[pr_id][FCUSTOMSCALEXVALUE]; 
		res->y = l->pr_cfdata[pr_id][FCUSTOMSCALEYVALUE];
		return;
	}
#else
	(void)l;
	(void)pr_id;
#endif
	res->x = res->y = FX32_ONE;
}
//----------------------------------------------------------------------------------

void prResetScaleToDefault(s32 pr_id)
{
	_resetScaleToDefault(cd.ainst, pr_id);
}
//----------------------------------------------------------------------------------

void prResetScaleToDefaultLayer(s32 pr_id, u32 layer)
{
	struct TEngineInstance *l = _getInstance(layer);
	_resetScaleToDefault(l, pr_id);
}
//----------------------------------------------------------------------------------

void _resetScaleToDefault(struct TEngineInstance *l, s32 pr_id)
{
#ifdef CUSTOMAFFINETRANSFORM_SUPPORT
	if(l->all_pr_count > pr_id)
	{
		u16 n;
		SDK_NULL_ASSERT(l->pr_cfdata);
		SDK_NULL_ASSERT(l->pr_cidata);
		if((l->pr_cidata[pr_id][ICUSTOMFLAGS] & ICUSTOMSCALEFLAG) == ICUSTOMSCALEFLAG)
		{
			l->pr_cidata[pr_id][ICUSTOMFLAGS] |= IRECALCULATELOGICRECTSFLAG;
		}
		l->pr_cidata[pr_id][ICUSTOMFLAGS] &= ~ICUSTOMSCALEFLAG;
		l->pr_cfdata[pr_id][FCUSTOMSCALEYVALUE] =
		l->pr_cfdata[pr_id][FCUSTOMSCALEXVALUE] = FX32_ONE;
		for (n = 0; n < cd.jn_ct; n++)
		{
			if (l->pr_cidata[pr_id][IJOINNODEBEGIN + n] != MAX_RES_IDX)
			{
				_resetScaleToDefault(l, l->pr_cidata[pr_id][IJOINNODEBEGIN + n]);
			}
		}
	}
#else
	(void)l;
	(void)pr_id;
#endif
}
//----------------------------------------------------------------------------------

void prSetCustomAlpha(s32 pr_id, u8 alpha)
{
	_setCustomAlpha(cd.ainst, pr_id, alpha);
}
//----------------------------------------------------------------------------------

void prSetCustomAlphaLayer(s32 pr_id, u8 alpha, u32 layer)
{
	struct TEngineInstance *l = _getInstance(layer);
	_setCustomAlpha(l, pr_id, alpha);
}
//----------------------------------------------------------------------------------

void _setCustomAlpha(struct TEngineInstance *l, s32 pr_id, u8 alpha)
{
	if(l->all_pr_count > pr_id)
	{
		s32 a;
		SDK_NULL_ASSERT(l->pr_cidata);
		a = alpha;
		if(a > ALPHA_OPAQ)
		{
			a = ALPHA_OPAQ;
		}
		a -= l->pr_cidata[pr_id][ICUSTOMALPHA];
		_addAlphaDeltaRecursive(l, pr_id, a);
	}
}
//----------------------------------------------------------------------------------

void _addAlphaDeltaRecursive(struct TEngineInstance *l, s32 pr_id, s32 da)
{
	u16 n;
	SDK_NULL_ASSERT(l->pr_cidata);
	l->pr_cidata[pr_id][ICUSTOMALPHA] += da;
	if(l->pr_cidata[pr_id][ICUSTOMALPHA] > ALPHA_OPAQ)
	{
		l->pr_cidata[pr_id][ICUSTOMALPHA] = ALPHA_OPAQ;	
	}
	else
	if(l->pr_cidata[pr_id][ICUSTOMALPHA] < 0)
	{
		l->pr_cidata[pr_id][ICUSTOMALPHA] = 0;	
	}
	l->pr_cidata[pr_id][ICUSTOMFLAGS] |= ICUSTOMALPHAFLAG;
	for (n = 0; n < cd.jn_ct; n++)
	{
		if (l->pr_cidata[pr_id][IJOINNODEBEGIN + n] != MAX_RES_IDX)
		{
			_addAlphaDeltaRecursive(l, l->pr_cidata[pr_id][IJOINNODEBEGIN + n], da);
		}
	}
}
//----------------------------------------------------------------------------------

u8 prGetCustomAlpha(s32 pr_id)
{
	return _getCustomAlpha(cd.ainst, pr_id);
}
//----------------------------------------------------------------------------------

u8 prGetCustomAlphaLayer(s32 pr_id, u32 layer)
{
	struct TEngineInstance *l = _getInstance(layer);
	return _getCustomAlpha(l, pr_id);
}
//----------------------------------------------------------------------------------

u8 _getCustomAlpha(struct TEngineInstance *l, s32 pr_id)
{
	if(l->all_pr_count > pr_id)
	{
		SDK_NULL_ASSERT(l->pr_cidata);
		return (u8)l->pr_cidata[pr_id][ICUSTOMALPHA];
	}
	return ALPHA_OPAQ;
}
//----------------------------------------------------------------------------------

void prResetAlphaToDefault(s32 pr_id)
{
	_resetAlphaToDefault(cd.ainst, pr_id);
}
//----------------------------------------------------------------------------------

void prResetAlphaToDefaultLayer(s32 pr_id, u32 layer)
{
	struct TEngineInstance *l = _getInstance(layer);
	_resetAlphaToDefault(l, pr_id);
}
//----------------------------------------------------------------------------------

void _resetAlphaToDefault(struct TEngineInstance *l, s32 pr_id)
{
	if(l->all_pr_count > pr_id)
	{
		u16 n;
		SDK_NULL_ASSERT(l->pr_cidata);
		l->pr_cidata[pr_id][ICUSTOMFLAGS] &= ~ICUSTOMALPHAFLAG;
		l->pr_cidata[pr_id][ICUSTOMALPHA] = ALPHA_OPAQ;
		for (n = 0; n < cd.jn_ct; n++)
		{
			if (l->pr_cidata[pr_id][IJOINNODEBEGIN + n] != MAX_RES_IDX)
			{
				_resetAlphaToDefault(l, l->pr_cidata[pr_id][IJOINNODEBEGIN + n]);
			}
		}
	}
}
//----------------------------------------------------------------------------------

void prBlendWithColor(s32 pr_id, GXRgba color)
{
	if(cd.ainst->all_pr_count > pr_id)
	{
		SDK_NULL_ASSERT(cd.ainst->pr_cidata);
		cd.ainst->pr_cidata[pr_id][ICUSTOMBLENDCOLOR] = color;
	}
}
//----------------------------------------------------------------------------------

void prBlendWithColorLayer(s32 pr_id, GXRgba color, u32 layer)
{
	struct TEngineInstance *l = _getInstance(layer);
	if(l->all_pr_count > pr_id)
	{
		SDK_NULL_ASSERT(l->pr_cidata);
		l->pr_cidata[pr_id][ICUSTOMBLENDCOLOR] = color;
	}
}
//----------------------------------------------------------------------------------

BOOL prHasAlphaCollideMap(s32 pr_id)
{
	if(cd.ainst->all_pr_count > pr_id)
	{
		s32 anim_frame;
		SDK_NULL_ASSERT(cd.ainst->pr_idata);
		anim_frame = cd.ainst->pr_idata[pr_id][IANIFRIDX]; 
		if(anim_frame >= 0)
		{
			s32 i, pc_count, anim_id, ppr_idx;
			const s16 *frdata;
			SDK_NULL_ASSERT(cd.ppr_fr_data);
			ppr_idx = cd.ainst->pr_idata[pr_id][IPARENT_IDX];
			anim_id = _prGetAnimationId(cd.ainst, pr_id);
			pc_count = fx2int(cd.ppr_fr_data[ppr_idx][anim_id][anim_frame][ANIFRFRCOUNT]);
			for(i = 0; i < pc_count; i++)
			{
				frdata = _getFramePieceData(ppr_idx, anim_id, anim_frame, i);
				SDK_NULL_ASSERT(cd.fr_data);
				if(_isStreamFrame(frdata[ANIFRFRDATA]))
				{
					SDK_NULL_ASSERT(cd.strm_obj_data);
					SDK_NULL_ASSERT(cd.strm_obj_data[pr_id][anim_id]);
					if(cd.strm_obj_data[pr_id][anim_id]->mData[0].mpImage)
					{
						return cd.strm_obj_data[pr_id][anim_id]->mData[0].mpImage->mACMDataSize > 0;
					}
				}
				else
				{
					u16 grfr_id;
					grfr_id = (u16)frdata[ANIFRFRID];
					if(cd.fr_data[grfr_id][RES_IMG_IDX] < NULL_IMG_IDX)
					{
						return cd.res_img[cd.fr_data[grfr_id][RES_IMG_IDX]]->mACMDataSize > 0;
					}
				}
			}
		}
	}
	return FALSE;
}
//----------------------------------------------------------------------------------

BOOL prGetAlphaCollideMapValue(s32 pr_id, struct fxVec2 pos)
{
	if(cd.ainst->all_pr_count > pr_id)
	{
		s32 anim_frame;
		SDK_NULL_ASSERT(cd.ainst->pr_idata);
		anim_frame = cd.ainst->pr_idata[pr_id][IANIFRIDX]; 
		if(anim_frame >= 0)
		{
			s32 anim_id, ppr_idx;
			SDK_NULL_ASSERT(cd.ppr_fr_data);
#ifdef CUSTOMAFFINETRANSFORM_SUPPORT
			{
				fx32 fsin, fcos, x, y, xl, yt, crv;
				SDK_NULL_ASSERT(cd.ainst->pr_cfdata);
				SDK_NULL_ASSERT(cd.ainst->pr_fdata);
				x = cd.ainst->pr_fdata[pr_id][FX];
				y = cd.ainst->pr_fdata[pr_id][FY];
				xl = pos.x - x;
				yt = pos.y - y;
				crv = cd.ainst->pr_cfdata[pr_id][FCUSTOMROTATIONVALUE];
				if(crv == 0)
				{
					pos.x = x + FX_Div(xl, fxAbs(cd.ainst->pr_cfdata[pr_id][FCUSTOMSCALEXVALUE]));
					pos.y = y + FX_Div(yt, fxAbs(cd.ainst->pr_cfdata[pr_id][FCUSTOMSCALEYVALUE]));
				}
				else
				{
					fxCordic(FX_Mul(FX_Div(-crv, FX32(180)), FX_PI), &fsin, &fcos, FX_CORDIC_MIDDLE_PRECISION);
					pos.x = FX_Mul(xl, fcos) + FX_Mul(yt, -fsin);
					pos.y = FX_Mul(xl, fsin) + FX_Mul(yt, fcos);
					pos.x = x + FX_Div(pos.x, fxAbs(cd.ainst->pr_cfdata[pr_id][FCUSTOMSCALEXVALUE]));
					pos.y = y + FX_Div(pos.y, fxAbs(cd.ainst->pr_cfdata[pr_id][FCUSTOMSCALEYVALUE]));
				}
			}
#endif	
			ppr_idx = cd.ainst->pr_idata[pr_id][IPARENT_IDX];
			anim_id = _prGetAnimationId(cd.ainst, pr_id);
			pos.x -= cd.ppr_fr_data[ppr_idx][anim_id][anim_frame][ANIFROFFL] + cd.ainst->pr_fdata[pr_id][FX]; 
			pos.y -= cd.ppr_fr_data[ppr_idx][anim_id][anim_frame][ANIFROFFT] + cd.ainst->pr_fdata[pr_id][FY];
			if(pos.x >= 0 && pos.x < cd.ppr_fr_data[ppr_idx][anim_id][anim_frame][ANIFRW] &&  
				pos.y >= 0 && pos.y < cd.ppr_fr_data[ppr_idx][anim_id][anim_frame][ANIFRH])
			{
				s32 i, pc_count;
				const s16 *frdata;
				pc_count = fx2int(cd.ppr_fr_data[ppr_idx][anim_id][anim_frame][ANIFRFRCOUNT]);
				for(i = 0; i < pc_count; i++)
				{
					s32 fx, fy;
					struct BMPImage *img;
					frdata = _getFramePieceData(ppr_idx, anim_id, anim_frame, i);
					SDK_NULL_ASSERT(cd.fr_data);
					fx = fx2int(cd.ppr_fr_data[ppr_idx][anim_id][anim_frame][ANIFROFFL] + pos.x) - frdata[ANIFRFROFFX];
					fy = fx2int(cd.ppr_fr_data[ppr_idx][anim_id][anim_frame][ANIFROFFT] + pos.y) - frdata[ANIFRFROFFY];
					if(_isStreamFrame(frdata[ANIFRFRDATA]))
					{
						SDK_NULL_ASSERT(cd.strm_obj_data);
						SDK_NULL_ASSERT(cd.strm_obj_data[pr_id][anim_id]);
						if(cd.strm_obj_data[pr_id][anim_id]->mData[0].mpImage)
						{
							img = cd.strm_obj_data[pr_id][anim_id]->mData[0].mpImage;
							if(img != NULL && img->mACMDataSize > 0)
							{
								if(fx >= 0 && fx < img->mWidth && fy >= 0 && fy < img->mHeight)
								{
									fx /= *(img->mpACMData + sizeof(u16));
									fy /= *(img->mpACMData + sizeof(u16));
									return (img->mpACMData[sizeof(u16) * 2 + fy * (*(img->mpACMData)) +  fx / 8] >> fx % 8) & 0x1;
								}	
							}
						}
					}
					else
					{
						u16 grfr_id;
						grfr_id = (u16)frdata[ANIFRFRID];
						if(cd.fr_data[grfr_id][RES_IMG_IDX] < NULL_IMG_IDX)
						{
							img = cd.res_img[cd.fr_data[grfr_id][RES_IMG_IDX]];
							if(img != NULL && img->mACMDataSize > 0)
							{
								fx += cd.fr_data[grfr_id][RES_X];
								fy += cd.fr_data[grfr_id][RES_Y];
								if(fx >= cd.fr_data[grfr_id][RES_X] && fx < cd.fr_data[grfr_id][RES_W] + cd.fr_data[grfr_id][RES_X] &&
										fy >= cd.fr_data[grfr_id][RES_Y] && fy < cd.fr_data[grfr_id][RES_H] + cd.fr_data[grfr_id][RES_Y])
								{
									fx /= *(img->mpACMData + sizeof(u16));
									fy /= *(img->mpACMData + sizeof(u16));
									return (img->mpACMData[sizeof(u16) * 2 + fy * (*(img->mpACMData)) +  fx / 8] >> fx % 8) & 0x1;
								}	
							}
						}
					}
				}
			}
		}
	}
	return FALSE;
}
//----------------------------------------------------------------------------------

BOOL low_getCurrentAnimationFrameInfo(s32 pr_id, struct GameObjectAnimationFrameInfo *const goafi, u32 layer)
{
	struct TEngineInstance *l;
	SDK_NULL_ASSERT(goafi);
	l = _getInstance(layer);
	SDK_NULL_ASSERT(l->pr_idata);
	SDK_NULL_ASSERT(l->pr_fdata);
	goafi->index = l->pr_idata[pr_id][IANIFRIDX];
	if(goafi->index >= 0)
	{
		goafi->animation_id = _prGetAnimationId(l, pr_id);
		goafi->time_remain = l->pr_idata[pr_id][ITIMER];
		goafi->left = l->pr_fdata[pr_id][FLEFT];
		goafi->top = l->pr_fdata[pr_id][FTOP];
		goafi->width = l->pr_fdata[pr_id][FCURRWIDTH];
		goafi->height = l->pr_fdata[pr_id][FCURRHEIGHT];
	}
	else
	{
		if(prIsTextBox(pr_id))
		{
			goafi->left = l->pr_fdata[pr_id][FX] - FX32(l->pr_textdata[pr_id][TXTWIDTH] / 2);
			goafi->top = l->pr_fdata[pr_id][FY] - FX32(l->pr_textdata[pr_id][TXTHEIGHT] / 2);
			goafi->width = FX32(l->pr_textdata[pr_id][TXTWIDTH]);
			goafi->height = FX32(l->pr_textdata[pr_id][TXTHEIGHT]);
			goafi->animation_id = -1;
			goafi->time_remain = 0;
		}
	}
	return FALSE;
}
//----------------------------------------------------------------------------------

void low_getAnimationFrameInfo(s32 gr_id, s32 anim_id, s32 frame_id, struct GameObjectAnimationFrameInfo *const goafi)
{
	SDK_NULL_ASSERT(goafi);
	SDK_NULL_ASSERT(cd.ppr_fr_data);
	goafi->index = frame_id;
	goafi->animation_id = anim_id;
	goafi->time_remain = 0;
	goafi->left = 0;
	goafi->top = 0;
	goafi->width = cd.ppr_fr_data[gr_id][anim_id][frame_id][ANIFRW];
	goafi->height = cd.ppr_fr_data[gr_id][anim_id][frame_id][ANIFRH];
}
//----------------------------------------------------------------------------------

BOOL prGetCurrentAnimationFrameInfo(s32 pr_id, struct GameObjectAnimationFrameInfo *const goafi)
{
	return low_getCurrentAnimationFrameInfo(pr_id, goafi, cd.a_layer); 
}
//----------------------------------------------------------------------------------

void low_setClipRect(s32 pr_id, s32 clip_pr_id, u32 layer)
{
	struct TEngineInstance *l;
	l = _getInstance(layer);
	if(l->all_pr_count > pr_id && pr_id >= 0)
	{
		SDK_NULL_ASSERT(l->pr_cidata);
		l->pr_cidata[pr_id][ICLIPRECTOBJ] = (u16)clip_pr_id;
	}
}
//----------------------------------------------------------------------------------
