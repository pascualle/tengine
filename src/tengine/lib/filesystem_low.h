#ifndef FILESYSTEM_LOW_H_INCLUDED
#define FILESYSTEM_LOW_H_INCLUDED

#define DMA_NO 3

#define	MAX_OPENFILES 3

#ifdef NITRO_SDK
#define	MAX_FILENAME 64
#define	MAX_FILEPATH 128
#endif
#ifdef ANDROID_NDK
#define	MAX_FILENAME 64
#define	MAX_FILEPATH 128
#define	MAX_INTERNALFILEPATH 512
#ifdef NDK_NATIVE_API10
struct AAssetManager;
#endif
#endif
#if defined WINDOWS_APP || defined  NIX_APP || defined EMSCRIPTEN_APP || defined KOLIBRIOS_APP
#define	MAX_FILENAME 64
#define	MAX_FILEPATH 1024
#endif
#ifdef IOS_APP
#define	MAX_FILENAME 64
#define	MAX_FILEPATH 1024
#define	MAX_INTERNALFILEPATH 1024
#endif

#define	FILERES_EXTENTION ".res"
#define	FONT_EXTENTION ".fnt"
#define	SOUND_EXTENTION ".wav"

#ifdef ANDROID_NDK
 #include "a_filesystem.h"
#endif
#if defined WINDOWS_APP || defined NIX_APP || defined IOS_APP || defined EMSCRIPTEN_APP || defined KOLIBRIOS_APP
 #include "stdio_filesystem.h"
#endif
#ifdef NITRO_SDK
 #include "ds_filesystem.h"
#endif

#endif