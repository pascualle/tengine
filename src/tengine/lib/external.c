/*
tengine - cross-platform game engine
------------------------------------

Copyright (c) 2013 Papa Pascualle
Contact Email: pascualle@gmail.com

Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
*/

#ifdef USE_EXTERNAL_API

#include "external.h"
#include "texts.h"
#include "fxmath.h"
#include "lib/texts_low.h"
#include "lib/render.h"
#include "lib/tengine_low.h"

#define TEXT_MARGIN 1

extern struct TEngineCommonData cd;

//---------------------------------------------------------------------------

void ExD_AddRenderExternalPosColorData(const struct TRGLColorVertexData* data)
{
	if(cd.ainst->visible_mask != 0)
	{
		SDK_NULL_ASSERT(data);
		SDK_NULL_ASSERT(cd.ainst);
		SDK_ASSERT(cd.ainst->bgType != BGSELECT_NUM);
		RENDERFN(ExternalPosColorData)(data);
	}
}
//---------------------------------------------------------------------------

void ExD_SetActiveClipRect(const struct ExDRect* data)
{
	if(cd.ainst->visible_mask != 0)
	{
		SDK_NULL_ASSERT(cd.ainst);
		SDK_ASSERT(cd.ainst->bgType != BGSELECT_NUM);
		RENDERFN(ExternalSetActiveClipRect)(data);
	}
}
//---------------------------------------------------------------------------

const struct TERFont* ExD_GetFont(u32 font_id)
{
	SDK_NULL_ASSERT(cd.res_fnt); // Something wrong with MapEditor generated data. Possible wrong path to res data in editor.ini
	return cd.res_fnt[font_id];
}
//---------------------------------------------------------------------------

u32 ExD_GetTextWidth(u32 font_id, const wchar *ptxt, u32 text_length)
{
	if(text_length > 0)
	{
		u32 i, width;
		struct TERFont *pfont;
		u16 srcSizeData[SRC_DATA_SIZE];
		SDK_NULL_ASSERT(cd.res_fnt); // Something wrong with MapEditor generated data. Possible wrong path to res data in editor.ini
		pfont = cd.res_fnt[font_id];
		SDK_NULL_ASSERT(pfont);
		SDK_NULL_ASSERT(pfont->mAlphabet.mpHTable); // Current font is not assigned by data
		width = 0;
		for(i = 0; i < text_length; ++i)
		{
			if (ptxt[i] == W_T(' '))
			{
				width += pfont->mSpaceCharSize;
			}
			else
			{
				_findTextureOffset(pfont, ptxt[i], srcSizeData);
				width += srcSizeData[SRC_DATA_W] + TEXT_MARGIN;
			}
		}
		return width;
	}
	return 0;
}
//---------------------------------------------------------------------------

void ExD_DrawText(const struct ExD_DrawTextData* data)
{
	SDK_NULL_ASSERT(data);
	SDK_NULL_ASSERT(cd.ainst);
	if(cd.ainst->visible_mask != 0)
	{
		struct TERFont *pfont;
		fx32 pos_x, pos_y, txt_width_fx, txt_heigh_fx;
		SDK_ASSERT(cd.ainst->bgType != BGSELECT_NUM);
		SDK_NULL_ASSERT(cd.res_fnt); // Something wrong with MapEditor generated data. Possible wrong path to res data in editor.ini
		pfont = cd.res_fnt[data->font_id];
		SDK_NULL_ASSERT(pfont);
		SDK_NULL_ASSERT(pfont->mAlphabet.mpHTable); // Current font is not assigned by data
		SDK_NULL_ASSERT(pfont->mpAbcImage);
		pos_x = FX32(data->txt_rect.x);
		pos_y = FX32(data->txt_rect.y);
		txt_width_fx = FX32(data->txt_rect.w);
		txt_heigh_fx = FX32(data->txt_rect.h);
		if(data->ptxt != NULL && txt_width_fx > 0 && txt_heigh_fx > 0)
		{
			struct DrawImageFunctionData fd;
			const struct AlphabetTextureOffsetMap* atd;
			s32 i, xb, xe, xs;
			s32 teClipRect[2];
			fx32 ty, tx0, ty0;
			fd.mType = DIT_Obj;
			fd.text = TRUE;
			if(data->clip_rect != NULL)
			{
				teClipRect[0] = ((u16)(data->clip_rect->x) << 16) | (u16)data->clip_rect->y;
				teClipRect[1] = ((u16)(data->clip_rect->w) << 16) | (u16)data->clip_rect->h;
				fd.mpClipRect = teClipRect;
			}
			else
			{
				fd.mpClipRect = NULL;
			}
			fd.mpSrcData = pfont->mpAbcImage;
			tx0 = fd.mX = pos_x; /* -ei->worldx_fx; */
 			ty0 = ty = pos_y; /* -ei->worldy_fx; */
			if((data->bg_color & GX_RGBA_A_MASK) != 0)
			{
				RENDERFN(SetColor)(data->bg_color);
				RENDERFN(ColorRect)(fd.mX, ty, txt_width_fx, txt_heigh_fx);
			}
			fd.mFillColor = (ALPHA_OPAQ << 16) | data->txt_color;
			fd.mSrcSizeData[SRC_DATA_H] = 0;
#ifdef CUSTOMAFFINETRANSFORM_SUPPORT
			fd.mSin = 0;
			fd.mCos = FX32_ONE;
#endif
			fd.mX = tx0;
			xb = 0;
			xe = (s32)data->text_length;
			xs = 1;
			for(i = xb; i != xe; i += xs)
			{				
				if((atd = _findTextureOffset(pfont, data->ptxt[i], fd.mSrcSizeData)) != NULL)
				{
					const fx32 char_w_fx = FX_Mul(FX32(fd.mSrcSizeData[SRC_DATA_W]), data->scale);
					if((fd.mX - tx0) + char_w_fx < 0 || fd.mX - tx0 > txt_width_fx)
					{
						break;
					}
					if((fd.mX - tx0) + char_w_fx > txt_width_fx)
					{
						const s32 dest_w = fx2int(FX_Div(txt_width_fx + tx0 - fd.mX, data->scale));
						if (dest_w > 0)
						{
							fd.mSrcSizeData[SRC_DATA_W] = (u16)dest_w;
						}
						else
						{
							break;
						}
					}
					if(ty - ty0 + FX_Mul(FX32(atd->mY_off + fd.mSrcSizeData[SRC_DATA_H]), data->scale) > txt_heigh_fx)
					{
						const s32 dest_h = fx2int(FX_Div((txt_heigh_fx), data->scale) + ty0 - ty) - atd->mY_off;
						if (dest_h > 0)
						{
							fd.mSrcSizeData[SRC_DATA_H] = (u16)dest_h;
						}
						else
						{
							continue;
						}
					}
					fd.mY = ty + FX_Mul(FX32(atd->mY_off), data->scale);
#ifdef CUSTOMAFFINETRANSFORM_SUPPORT
					fd.mAffineOriginX = fd.mX - tx0 - FX_Mul(txt_width_fx, data->scale) / 2;
					fd.mAffineOriginY = fd.mY - ty0 - FX_Mul(txt_heigh_fx, data->scale) / 2;
					fd.mDestWidth = FX_Mul(FX32(fd.mSrcSizeData[SRC_DATA_W]), data->scale);
					fd.mDestHeight = FX_Mul(FX32(fd.mSrcSizeData[SRC_DATA_H]), data->scale);
#endif				
					if(data->pixel_accurate == TRUE)
					{
						fd.mX = fxFloor(fd.mX);
						fd.mY = fxFloor(fd.mY);
					}
					RENDERFN(DrawImage)(&fd);
					if (data->ptxt[i] == W_T(' '))
					{
						fd.mX += FX_Mul(FX32(pfont->mSpaceCharSize), data->scale);
					}
					else
					{
						fd.mX += FX_Mul(FX32(fd.mSrcSizeData[SRC_DATA_W] + TEXT_MARGIN), data->scale);
					}
				}
				else
				{
					fd.mX += FX_Mul(FX32(fd.mSrcSizeData[SRC_DATA_W] + TEXT_MARGIN), data->scale);
				}
			}
		}
	}
}
//-------------------------------------------------------------------------------------------

void ExD_DrawImage(const struct ExD_DrawImageData* data)
{
	SDK_NULL_ASSERT(data);
	SDK_NULL_ASSERT(cd.ainst);
	if (cd.ainst->visible_mask != 0)
	{
		s32 i, pc_count;
#ifdef CUSTOMAFFINETRANSFORM_SUPPORT
		fx32 scaleX, scaleY;
#endif
		const s16* frdata;
		struct DrawImageFunctionData fd;
		u16 grfr_id;
		SDK_NULL_ASSERT(cd.ainst->pr_idata);
		SDK_NULL_ASSERT(cd.ppr_fr_data);
		pc_count = fx2int(cd.ppr_fr_data[data->obj_type][data->anim_id][data->frame_num][ANIFRFRCOUNT]);
		fd.mType = DIT_Obj;
		fd.text = FALSE;
		fd.mpClipRect = NULL;
#ifdef CUSTOMAFFINETRANSFORM_SUPPORT
		fd.mAffineOriginX = -FX32(data->rect.w / 2);
		fd.mAffineOriginY = -FX32(data->rect.h / 2);
		scaleX = FX_Div(FX32(data->rect.w), cd.ppr_fr_data[data->obj_type][data->anim_id][data->frame_num][ANIFRW]);
		scaleY = FX_Div(FX32(data->rect.h), cd.ppr_fr_data[data->obj_type][data->anim_id][data->frame_num][ANIFRH]);
#else
		(void)rotationAngle;
		(void)affineOriginX;
		(void)affineOriginY;
		(void)scaleX;
		(void)scaleY;
#endif
		for (i = 0; i < pc_count; i++)
		{
			frdata = _getFramePieceData(data->obj_type, data->anim_id, data->frame_num, i);
			SDK_NULL_ASSERT(cd.fr_data);
			fd.mFillColor = (((((frdata[ANIFRFRALPHA] * (data->alpha * 0xff) / 0x1f)) / 0xff) << 16)) | ((u16)data->blend_color);
#ifdef CUSTOMAFFINETRANSFORM_SUPPORT
			fd.mSin = 0;
			fd.mCos = FX32_ONE;
			fd.mX = FX32(data->rect.x) - fd.mAffineOriginX + FX_Mul(FX32(frdata[ANIFRFROFFX]), scaleX);
			fd.mY = FX32(data->rect.y) - fd.mAffineOriginY + FX_Mul(FX32(frdata[ANIFRFROFFY]), scaleY);
#else
			fd.mX = FX32(data->rect.x + data->rect.w / 2 + frdata[ANIFRFROFFX]);
			fd.mY = FX32(data->rect.y + data->rect.h / 2 + frdata[ANIFRFROFFY]);
#endif
			if (_isStreamFrame(frdata[ANIFRFRDATA]))
			{
				SDK_ASSERT(0); // do not use this function for streamed frames
				return;
			}
			else
			{
				grfr_id = (u16)frdata[ANIFRFRID];
			}
			if (cd.fr_data[grfr_id][RES_IMG_IDX] < NULL_IMG_IDX)
			{
				fd.mpSrcData = cd.res_img[cd.fr_data[grfr_id][RES_IMG_IDX]];
#ifdef CUSTOMAFFINETRANSFORM_SUPPORT
				fd.mDestWidth = FX_Mul(FX32(frdata[ANIFRFRWIDTH]), scaleX);
				fd.mDestHeight = FX_Mul(FX32(frdata[ANIFRFRHEIGHT]), scaleY);
#endif
				MI_CpuCopy16(&cd.fr_data[grfr_id][RES_X], &fd.mSrcSizeData, sizeof(u16) * 4);
				RENDERFN(DrawImage)(&fd);
			}
		}
	}
}
//-------------------------------------------------------------------------------------------

#endif
