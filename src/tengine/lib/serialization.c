/*
tengine - cross-platform game engine
------------------------------------

Copyright (c) 2013 Papa Pascualle
Contact Email: pascualle@gmail.com

Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
*/

#ifndef NITRO_SDK

#include "serialization.h"
#include "network.h"

//---------------------------------------------------------------------------

void SerializeU32(u32 in_, struct broadcastCB_* out_)
{
	u32 i;
	for(i = 0; i < sizeof(u32); i++, out_->length++)
	{
		u32 mask = 0x000000ff;
		out_->data[out_->length] = (u8)(in_ >> (i * 8)) & mask;
	}
}
//---------------------------------------------------------------------------

void UnSerializeU32(struct broadcastCB_* in_, u32* out_)
{
	s32 i;
	u32 mask = 0x000000ff;
	(*out_) = 0x00000000;
	in_->length--;
	for(i =sizeof(u32)-1 ;i>=0 ;i--, in_->length--)
	{
		u32 buf; //0x00000000;
		buf = in_->data[in_->length];
		buf &= mask;
		(*out_) |= buf << (i*8);
	}
	in_->length++;  // TEMPOLARY FUCK
}
//---------------------------------------------------------------------------

void SerializeS32(s32 in_, struct broadcastCB_* out_)
{
	u32 i;
	for(i = 0; i < sizeof(s32); i++, out_->length++)
	{
		u32 mask = 0x000000ff;
		out_->data[out_->length] = (u8)(in_ >> (i * 8)) & mask;
	}
}
//---------------------------------------------------------------------------

void UnSerializeS32(struct broadcastCB_* in_, s32* out_)
{
	s32 i;
	u32 mask = 0x000000ff;
	(*out_) = 0x00000000;
	in_->length--;
	for(i =sizeof(s32)-1 ;i>=0 ;i--, in_->length--)
	{
		u32 buf; //0x00000000;
		buf = in_->data[in_->length];
		buf &= mask;
		(*out_) |= buf << (i*8);
	}
	in_->length++;  // TEMPOLARY FUCK
}
//---------------------------------------------------------------------------

void SerializeFx32(fx32 in_, struct broadcastCB_* out_)
{
	SerializeU32((u32)(in_), out_);
}
//---------------------------------------------------------------------------

void UnSerializeFx32(struct broadcastCB_* in_, fx32* out_)
{
	UnSerializeU32(in_, (u32*)(out_));
}
//---------------------------------------------------------------------------

void SerializeFxVec2(struct fxVec2 in_, struct broadcastCB_* out_)
{
	SerializeFx32(in_.x, out_);
	SerializeFx32(in_.y, out_);
}
//---------------------------------------------------------------------------

void UnSerializeFxVec2(struct broadcastCB_* in_, struct fxVec2* out_)
{
	UnSerializeFx32(in_, &(out_->y));
	UnSerializeFx32(in_, &(out_->x));
}
//---------------------------------------------------------------------------

#endif