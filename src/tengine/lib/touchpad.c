/*
tengine - cross-platform game engine
------------------------------------

Copyright (c) 2013 Papa Pascualle
Contact Email: pascualle@gmail.com

Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
*/

#include "touchpad.h"
#include "memory.h"
#if defined ANDROID_NDK || defined WINDOWS_APP || defined NIX_APP || defined IOS_APP || defined EMSCRIPTEN_APP || defined KOLIBRIOS_APP
#include "a_touchpad.h"
#endif
//----------------------------------------------------------------------------------------------

static TouchPadData mTPdata;
#if defined ANDROID_NDK || defined  WINDOWS_APP || defined  NIX_APP || defined IOS_APP || defined EMSCRIPTEN_APP || defined KOLIBRIOS_APP
static TouchPadData tp_data;
#endif
//----------------------------------------------------------------------------------------------

void InitTouchPad()
{
#ifdef NITRO_SDK
	TPCalibrateParam calibrate;
#endif
	ResetTouchPad();
#ifdef NITRO_SDK
    TP_Init();
    // get CalibrateParameter from FlashMemory
    if (!TP_GetUserInfo(&calibrate))
    {
        OS_Panic("FATAL ERROR: can't read UserOwnerInfo\n");
    }

	TP_SetCalibrateParam(&calibrate);
#endif
}
//----------------------------------------------------------------------------------------------

void ResetTouchPad()
{
	MI_CpuClear8(&mTPdata, sizeof(TouchPadData));
#if defined ANDROID_NDK || defined  WINDOWS_APP || defined  NIX_APP || defined IOS_APP || defined EMSCRIPTEN_APP || defined KOLIBRIOS_APP
	MI_CpuClear8(&tp_data, sizeof(TouchPadData));
#endif
}
//----------------------------------------------------------------------------------------------

void ReadTouchPadData(TouchPadData *opData)
{
#ifdef NITRO_SDK
	TPData  tp_data;
	TPData  tp_raw;
	BOOL    old;
	old = mTPdata.point[0].mTouch;
	while (TP_RequestRawSampling(&tp_raw) != 0)
	{
	};
	TP_GetCalibratedPoint(&tp_data, &tp_raw);
	switch (tp_data.validity)
	{
		case TP_VALIDITY_VALID:
			mTPdata.point[0].mX = tp_data.x;
			mTPdata.point[0].mY = tp_data.y;
		break;
		default:
			tp_data.touch = FALSE;
		/*
		case TP_VALIDITY_INVALID_X:
			mTPdata.point[0].mY = tp_data.y;
			break;
		case TP_VALIDITY_INVALID_Y:
			mTPdata.point[0].mX = tp_data.x;
			break;
		*/
	}
	mTPdata.point[0].mTouch = tp_data.touch;
	mTPdata.point[0].mTrg = !old & mTPdata.point[0].mTouch;
	mTPdata.point[0].mRls = old & (old ^ mTPdata.point[0].mTouch);
#endif
#if defined ANDROID_NDK || defined WINDOWS_APP || defined NIX_APP || defined IOS_APP || defined EMSCRIPTEN_APP || defined KOLIBRIOS_APP
	s32 i;
	for(i = 0; i < TOUCH_POINTS_MAX; i++)
	{
		const BOOL old = mTPdata.point[i].mTouch;
		mTPdata.point[i].mTouch = tp_data.point[i].mTouch;
		mTPdata.point[i].mTrg = !old && mTPdata.point[i].mTouch;
		mTPdata.point[i].mRls = old & (old ^ mTPdata.point[i].mTouch);
		mTPdata.point[i].mX = tp_data.point[i].mX;
		mTPdata.point[i].mY = tp_data.point[i].mY;
	}
#endif
	*opData = mTPdata;
}
//----------------------------------------------------------------------------------------------

#if defined ANDROID_NDK || defined  WINDOWS_APP || defined  NIX_APP || defined IOS_APP || defined EMSCRIPTEN_APP || defined KOLIBRIOS_APP
void onTouchPadDown(u8 id, s32 x, s32 y)
{
	if(TOUCH_POINTS_MAX > id)
	{
		tp_data.point[id].mTouch = TRUE;
		tp_data.point[id].mX = x;
		tp_data.point[id].mY = y;
	}
}
//----------------------------------------------------------------------------------------------

void onTouchPadUp(u8 id, s32 x, s32 y)
{
	if(TOUCH_POINTS_MAX > id)
	{
		tp_data.point[id].mTouch = FALSE;
		tp_data.point[id].mX = x;
		tp_data.point[id].mY = y;
	}
}
//----------------------------------------------------------------------------------------------

void onTouchPadMove(u8 id, s32 x, s32 y)
{
	if(TOUCH_POINTS_MAX > id)
	{
#if defined ANDROID_NDK || defined IOS_APP
		tp_data.point[id].mTouch = TRUE;
#endif
		tp_data.point[id].mX = x;
		tp_data.point[id].mY = y;
	}
}
//----------------------------------------------------------------------------------------------
#endif
