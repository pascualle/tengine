/*
tengine - cross-platform game engine
------------------------------------

Copyright (c) 2013 Papa Pascualle
Contact Email: pascualle@gmail.com

Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
*/

#include "system.h"
#include "fxmath.h"
#ifdef EMSCRIPTEN_APP
 #include "emscripten.h"
#endif
#if defined USE_FX32_AS_FLOAT
#include <float.h>
#endif

//----------------------------------------------------------------------------------------------------------------

#ifdef NITRO_SDK
static MATHRandContext32 gRand;
static const u32 RAND_SEED = 3300;
#endif
#if defined WINDOWS_APP || defined NIX_APP || defined KOLIBRIOS_APP
static const s32 MAX_INT = 2147483647;
#endif

//----------------------------------------------------------------------------------------------------------------
/*
static const s32 gSin_t[10] = {0, FX32(0.174f), FX32(0.342f), FX32(0.500f), FX32(0.643f), FX32(0.766f), FX32(0.866f), FX32(0.940f), FX32(0.985f), FX32(1.0f)};
static fx32 _sinus(s32 t);
*/
//----------------------------------------------------------------------------------------------------------------

#if defined ANDROID_NDK || defined  WINDOWS_APP || defined NIX_APP || defined IOS_APP || defined EMSCRIPTEN_APP || defined KOLIBRIOS_APP
#if !defined USE_FX32_AS_FLOAT
//cordic constants for FX32_ONE == 1 << 16
#define CORDIC_ONE 0x00009B74
#define CORDIC_HALF_PI 0x0001921F
#define CORDIC_NTAB 16
s32 cordic_ctab [] = {0x0000C90F, 0x000076B1, 0x00003EB6, 0x00001FD5, 0x00000FFA, 0x000007FF,
						0x000003FF, 0x000001FF, 0x000000FF, 0x0000007F, 0x0000003F, 0x0000001F,
						0x0000000F, 0x00000007, 0x00000003, 0x00000001};
#endif
#else
#if defined NITRO_SDK
//cordic constants for FX32_ONE == 1 << 12
#define CORDIC_ONE 0x000009B7
#define CORDIC_HALF_PI 0x00001921
#define CORDIC_NTAB 12
s32 cordic_ctab [] = {0x00000C90, 0x0000076B, 0x000003EB, 0x000001FD, 0x000000FF, 0x0000007F,
						0x0000003F, 0x0000001F, 0x0000000F, 0x00000007, 0x00000003, 0x0000000};
#endif
#endif


//----------------------------------------------------------------------------------------------------------------

void mthInitRandom()
{
#ifdef NITRO_SDK
   MATH_InitRand32(&gRand, RAND_SEED);
#else
 #if defined WINDOWS_APP || defined NIX_APP || defined EMSCRIPTEN_APP || defined KOLIBRIOS_APP
	srand((unsigned int)time(NULL));
 #endif
#endif
}
//----------------------------------------------------------------------------------------------------------------

u32 mthGetRandom(u32 iMax)
{
	if(iMax == 0)
	{
        return 0;
    }
#ifdef NITRO_SDK
	return MATH_Rand32(&gRand, iMax);
#else
 #if defined WINDOWS_APP || defined NIX_APP || defined KOLIBRIOS_APP
	{
		const s32 a = rand();
		const s32 b = rand();
		const u32 random_uint = (a << 30 | a << 15 | b);
		return random_uint % iMax;
	}
#else
    #if defined EMSCRIPTEN_APP
	  return (u32)(emscripten_random() * iMax);
    #else
	  return random() % iMax;
    #endif
 #endif
#endif
}
//----------------------------------------------------------------------------------------------------------------

fx32 mthGetRandomFx(fx32 iMax)
{
	SDK_ASSERT(iMax >= 0);
#if defined USE_FX32_AS_FLOAT
  #if defined WINDOWS_APP || defined NIX_APP || defined KOLIBRIOS_APP
	{
		const s32 a = rand();
		const s32 b = rand();
		const s32 random_int = (a << 30 | a << 15 | b) & MAX_INT;
		const fx32 random_fx = (fx32)random_int / (fx32)MAX_INT;
		return random_fx * iMax;
	}
  #else
    #if defined EMSCRIPTEN_APP
		return emscripten_random() * iMax;
    #else
		return ((fx32)random() / (fx32)RAND_MAX) * iMax;
    #endif
  #endif
#else
	return (fx32)mthGetRandom((u32)iMax);
#endif
}

#ifndef NITRO_SDK
s32 MATH_ABS(s32 val)
{
	return abs(val);
}
#endif
//----------------------------------------------------------------------------------------------------------------

/*
fx32 _sinus(s32 t)
{
   s32 k = t / 10;
   if(t % 10 == 0)
   {
      return gSin_t[k];
   }
   else
   {
	  return ((gSin_t[k + 1] - gSin_t[k]) * (t % 10) / 10 + gSin_t[k]);
   }
}
//----------------------------------------------------------------------------------------------------------------

fx32 fxSin(fx32 t)
{
    s32 sign = 1;
    t = (t >> FX32_SHIFT) % 360;
    if (t < 0)
    {
      t = -t; sign = -1;
    }
    if (t <= 90){return sign * _sinus(t);}
    else if (t <= 180){ return sign * _sinus(180 - t);}
    else if (t <= 270){ return -sign * _sinus(t - 180);}
    else {return -sign * _sinus(360 - t);}
}
//----------------------------------------------------------------------------------------------------------------

fx32 fxCos(fx32 t)
{
    t = (t >> FX32_SHIFT) % 360;
    if (t < 0){t = -t;}
    if (t <= 90){return _sinus(90 - t);}
    else if (t <= 180){return -_sinus(t - 90);}
    else if (t <= 270){return -_sinus(270 - t);}
    else {return _sinus(t - 270);}
}
//----------------------------------------------------------------------------------------------------------------
*/
//accurate to ~2.1%
fx32 fxSin(fx32 angle_rad)
{
#if defined USE_FX32_AS_FLOAT
	return sin(angle_rad);
#else
	fx32 tempAngleSq, t;
	fx32 tempAngle = angle_rad % (FX_PI * 2);
	if(tempAngle > FX_PI)
	{
		tempAngle -= FX_PI * 2;
	}
	else if(tempAngle < -FX_PI)
	{
		tempAngle += FX_PI * 2;
	}
	tempAngleSq = FX_Mul(tempAngle, tempAngle);
	t = tempAngle;
	tempAngle = FX_Mul(tempAngle, tempAngleSq);
	t -= tempAngle / 6;
	tempAngle = FX_Mul(tempAngle, tempAngleSq);
	t += tempAngle / 120;
	tempAngle = FX_Mul(tempAngle, tempAngleSq);
	t -= tempAngle / 5040;
	tempAngle = FX_Mul(tempAngle, tempAngleSq);
	t += tempAngle / 362880;
	tempAngle = FX_Mul(tempAngle, tempAngleSq);
	t -= tempAngle / 39916800;
	return t;
#endif
}
//----------------------------------------------------------------------------------------------------------------

fx32 fxCos(fx32 angle_rad)
{
#if defined USE_FX32_AS_FLOAT
	return cos(angle_rad);
#else
	return fxSin(angle_rad + FX_PI / 2);
#endif
}
//----------------------------------------------------------------------------------------------------------------

fx32 fxAsin(fx32 angle_rad)
{
#if defined USE_FX32_AS_FLOAT
	return asin(angle_rad);
#else
	fx32 t;
	if(angle_rad > FX32_ONE || angle_rad < -FX32_ONE)
	{
		return 0;
	}
	if(angle_rad == FX32_ONE || angle_rad == -FX32_ONE)
	{
		t = angle_rad >> 31;
		return ((FX_PI / 2) ^ t) - t;
	}
	t = (FX32_ONE - FX_Mul(angle_rad, angle_rad));
	t = FX_Div(angle_rad, fxSqrt(t));
	return fxAtan2(t, FX32_ONE);
#endif
}
//----------------------------------------------------------------------------------------------------------------

fx32 fxAcos(fx32 angle_rad)
{
#if defined USE_FX32_AS_FLOAT
	return acos(angle_rad);
#else
	return (FX_PI / 2 - fxAsin(angle_rad));
#endif
}
//----------------------------------------------------------------------------------------------------------------

fx32 fxAtan(fx32 angle_rad)
{
#if defined USE_FX32_AS_FLOAT
	return atan(angle_rad);
#else
	return fxAtan2(angle_rad, FX32_ONE);
#endif
}
//----------------------------------------------------------------------------------------------------------------

fx32 fxAtan2(fx32 inY, fx32 inX)
{
#if defined USE_FX32_AS_FLOAT
	return atan2(inY, inX);
#else
	fx32 absY, angle, r, r3;
	absY = fxAbs(inY);
	if (inX >= 0)
	{
		r = FX_Div((inX - absY), (inX + absY));
		r3 = FX_Mul(FX_Mul(r, r), r);
		angle = FX_Mul(FX32(0.1962890625f), r3) - FX_Mul(FX32(0.981689453125f), r) + (FX_PI / 4);
	}
	else
	{
		r = FX_Div((inX + absY), (absY - inX));
		r3 = FX_Mul(FX_Mul(r, r), r);
		angle = FX_Mul(FX32(0.1962890625f), r3) - FX_Mul(FX32(0.981689453125f), r) + ((3 * FX_PI) / 4);
	}
	return inY < 0 ? -angle : angle;
#endif
}
//----------------------------------------------------------------------------------------------------------------

// opimal num_iterations = 8 (0.001 precise) for android, win32, nix 
void fxCordic(fx32 theta, fx32 *osin, fx32 *ocos, s32 num_iterations)
{
#if defined USE_FX32_AS_FLOAT
	(void)num_iterations;
	*ocos = cos(theta);
	*osin = sin(theta);
#else
	s32 k, cs;
	fx32 d, tx, ty, x, y;
	SDK_ASSERT(num_iterations <= CORDIC_NTAB);
	cs = 0;
	if(CORDIC_HALF_PI < fxAbs(theta))
	{
		//Function is valid for arguments in range -pi/2 -- pi/2
		tx = theta >> 31;
		d = ((theta % (CORDIC_HALF_PI * 4)) ^ tx) - tx;
		if (d <= CORDIC_HALF_PI)
		{
			theta = (d ^ tx) - tx;
		}
		else if (d <= (CORDIC_HALF_PI * 2))
		{
			theta = ((CORDIC_HALF_PI - (d - CORDIC_HALF_PI)) ^ tx) - tx;
			cs = -1;
		}
		else if (d <= 3 * CORDIC_HALF_PI)
		{
			theta = ((((3 * CORDIC_HALF_PI) - d) - CORDIC_HALF_PI) ^ tx) - tx;
			cs = -1;
		}
		else
		{
			theta = (((-CORDIC_HALF_PI * 4) + d) ^ tx) - tx;
		}
	}
	y = 0;
	x = CORDIC_ONE;
	for(k = 0; k < num_iterations; ++k)
	{
		d = theta >> 31; //sign
		tx = x - (((y >> k) ^ d) - d);
		ty = y + (((x >> k) ^ d) - d);
		x = tx;
		y = ty;
		theta = theta - ((cordic_ctab[k] ^ d) - d);
	}
	*ocos = (x ^ cs) - cs;
	*osin = y;
#endif
}
//----------------------------------------------------------------------------------------------------------------

u32 mthSqrtI(u32 val)
{
	u32 b = 1 << 30, q = 0, r = val;
	while (b > r)
	{
		b >>= 2;
	}
	while (b > 0)
	{
		u32 t = q + b;
		q >>= 1;
		if (r >= t)
		{
			r -= t;
			q += b;
		}
		b >>= 2;
	}
	return q;
}
//----------------------------------------------------------------------------------------------------------------

fx32 fxSqrt(fx32 val)
{
#ifdef NITRO_SDK
	return FX_Sqrt(val);
#else
#if defined USE_FX32_AS_FLOAT
	return sqrt(val);
#else
	fx32 t, q, b, r;
	SDK_ASSERT(val >= 0);
	r = val;
	q = 0;
	b = 0x40000000;
	while (b >= FX32(0.00390625f))
	{
		t = q + b;
		if (r >= t)
		{
			r = r - t;
			q = t + b;
		}
		r = r << 1;
		b = b >> 1;
	}
	q = q >> 8;
	return q;
#endif
#endif
}
//----------------------------------------------------------------------------------------------------------------

struct fxVec2 fxVec2Add(struct fxVec2 const v1, struct fxVec2 const v2)
{
	struct fxVec2 ret;
	ret.x = v1.x + v2.x;
	ret.y = v1.y + v2.y;
	return ret;
}
//----------------------------------------------------------------------------------------------------------------

struct fxVec2 fxVec2Sub(struct fxVec2 const v1, struct fxVec2 const v2)
{
	struct fxVec2 ret;
	ret.x = v1.x - v2.x;
	ret.y = v1.y - v2.y;
	return ret;
}
//----------------------------------------------------------------------------------------------------------------

struct fxVec2 fxVec2Mul(struct fxVec2 const v1, struct fxVec2 const v2)
{
	struct fxVec2 ret;
	ret.x = fxMul(v1.x, v2.x);
	ret.y = fxMul(v1.y, v2.y);
	return ret;
}
//----------------------------------------------------------------------------------------------------------------

struct fxVec2 fxVec2Div(struct fxVec2 const v1, struct fxVec2 const v2)
{
	struct fxVec2 ret;
	ret.x = fxDiv(v1.x, v2.x);
	ret.y = fxDiv(v1.y, v2.y);
	return ret;
}

//----------------------------------------------------------------------------------------------------------------

struct fxVec2 fxVec2MulFx(struct fxVec2 const v1, fx32 const val)
{
    struct fxVec2 ret;
    ret.x = fxMul(v1.x, val);
    ret.y = fxMul(v1.y, val);
    return ret;
}
//----------------------------------------------------------------------------------------------------------------

struct fxVec2 fxVec2DivFx(struct fxVec2 const v1, fx32 const val)
{
    struct fxVec2 ret;
	SDK_ASSERT(val != 0);
    ret.x = fxDiv(v1.x, val);
    ret.y = fxDiv(v1.y, val);
    return ret;
}
//----------------------------------------------------------------------------------------------------------------

fx32 fxVec2Dot(struct fxVec2 const v1, struct fxVec2 const v2)
{
	struct fxVec2 ret = fxVec2Mul(v1, v2);
	return ret.x + ret.y;
}
//----------------------------------------------------------------------------------------------------------------

fx32 fxVec2Cross(struct fxVec2 const v1, struct fxVec2 const v2)
{	
#if defined USE_FX32_AS_FLOAT
	fx32 a, b;
	a = v1.x * v2.y;
	b = v1.y * v2.x;
#else
	s64 a, b;
	a = ((s64)(v1.x) * v2.y) >> FX32_SHIFT;
	b = ((s64)(v1.y) * v2.x) >> FX32_SHIFT;
#endif
	return (fx32)(a - b);
}
//----------------------------------------------------------------------------------------------------------------

fx32 fxVec2Length(struct fxVec2 const v1)
{
#if defined USE_FX32_AS_FLOAT
	return sqrt(v1.x * v1.x + v1.y * v1.y);
#else
	s64 r, t, q, b;
	t = ((s64)(v1.x) * v1.x) >> FX32_SHIFT;
	SDK_ASSERT(t >= 0); // oveflow!
	q = ((s64)(v1.y) * v1.y) >> FX32_SHIFT;
	SDK_ASSERT(q >= 0); // oveflow!
	r =  t + q;
	SDK_ASSERT(r >= 0); // oveflow!
	q = 0;
	b = 0x40000000;
	while(b >= FX32(0.00390625f))
	{
		t = q + b;
		if(r >= t)
		{
			r = r - t;
			q = t + b;
		}
		r = r << 1;
		b = b >> 1;
	}
	q = q >> 8;
	return (fx32)q;
#endif
}
//----------------------------------------------------------------------------------------------------------------

struct fxVec2 fxVec2Normalize(struct fxVec2 const v1)
{
    fx32 len = fxVec2Length(v1);
	return fxVec2DivFx(v1, len);
}
//----------------------------------------------------------------------------------------------------------------

void mthGetInterval(const struct fxVec2* const a, u8 aSize, struct fxVec2 const xAxis, fx32* Pmin0, fx32* Pmax0)
{
	s32 i;
    fx32 min0;
    fx32 max0 = min0 = fxVec2Dot(a[0], xAxis);
    for (i=0; i < aSize; i++)
    {
        fx32 dot = fxVec2Dot(a[i], xAxis); 
        if (dot < min0)
		{
            min0 = dot;
		}
        else if (dot > max0)
		{
            max0 = dot;
		}
    }
    (*Pmin0) = min0;
    (*Pmax0) = max0;
}
//----------------------------------------------------------------------------------------------------------------

BOOL mthIntervalIntersect(struct fxVec2* a, s32 aSize, struct  fxVec2* b,s32 bSize, struct  fxVec2* xAxis, fx32* depth)
{          
    fx32 min0, max0, min1, max1, d0, d1;
	(*depth) = 0;  
    
	mthGetInterval(a, (u8)aSize, (*xAxis), &min0, &max0);
    mthGetInterval(b, (u8)bSize, (*xAxis), &min1, &max1);

    d0 = min0 - max1; 
    d1 = min1 - max0;
    if (d0 > 0 || d1 > 0)
	{
        return FALSE;
	}
    else
    {
#if defined USE_FX32_AS_FLOAT
        (*depth) = (fabs(d0) < fabs(d1)) ? fabs(d0) : fabs(d1);
#else
        (*depth) = (MATH_ABS(d0) < MATH_ABS(d1)) ? MATH_ABS(d0) : MATH_ABS(d1);
#endif
        return TRUE;
    }
}
//----------------------------------------------------------------------------------------------------------------

BOOL mthIntersect(struct fxVec2* a, s32 aSize, struct fxVec2 * b, s32 bSize, struct fxVec2 const offset, struct fxVec2 * shift)
{
	s32 i,j;
	struct fxVec2 pushAxis;
	struct fxVec2 xAxis;
	fx32 mindepth;
    fx32 depth;
    BOOL ret = TRUE;

	struct fxVec2 zeroPoint = a[0];

    for (i = 0; i < aSize; i++)
    {
        zeroPoint.x = fxMin(a[i].x, zeroPoint.x);
        zeroPoint.y = fxMin(a[i].y, zeroPoint.y);
	}
    for (i = 0; i < bSize; i++)
    {
        zeroPoint.x = fxMin(b[i].x, zeroPoint.x);
        zeroPoint.y = fxMin(b[i].y, zeroPoint.y);
    }

    for(i = 0;i<aSize;i++)
    {
        a[i] = fxVec2Sub(a[i], zeroPoint);
    }

    for(i = 0;i<bSize;i++)
    {
        b[i] = fxVec2Sub(b[i], zeroPoint);
    }

    pushAxis = fxVec2Create(0, 0);
#if defined USE_FX32_AS_FLOAT
	mindepth = DBL_MAX;
#else
    mindepth = 0x3fffffff; // Maximum positive value of fx32
#endif
    xAxis = fxVec2Create(0, 0);

    j = aSize - 1;
	i = 0;
    for (; i < aSize;j=i,i++)
    {
        struct fxVec2 E = fxVec2Sub( a[j], a[i]);
        xAxis = fxVec2Create(-E.y, E.x);

        if (mthIntervalIntersect(a, aSize, b, bSize, &xAxis, &depth) == FALSE)
        {
            ret = FALSE;
            goto end;
        }
        else
        {
            if (depth < mindepth)
            {
                pushAxis = xAxis;
                mindepth = depth;
            }
        }
    }

    j = bSize - 1;
	i = 0;
    for (; i < bSize; j = i, i++)
    {
        struct fxVec2 E = fxVec2Sub(b[j], b[i]);
        xAxis = fxVec2Create(-E.y, E.x); //Getting normal vector
        if (mthIntervalIntersect(a, aSize, b, bSize, &xAxis, &depth) == FALSE)
        {
            ret = FALSE;
            goto end;
        }
        else
        {
            if (depth < mindepth)
            {
                pushAxis = xAxis;
                mindepth = depth;
            }
        }
    }

    if(shift != NULL)
    {
        struct fxVec2 pushV = fxVec2Create(0, 0);
        pushV = pushAxis;
        mindepth = fxDiv(mindepth, fxVec2Length(pushV));

        pushV = fxVec2Normalize(pushV);
        pushV= fxVec2MulFx(pushV, fxMul(mindepth, FX32(1.01)));

        if (fxVec2Dot(pushV, offset ) < 0)
        {
            (pushV).x = -(pushV).x;
            (pushV).y = -(pushV).y;
        }
        *shift = pushV;
    }
end:
    for(i = 0;i<aSize;i++)
    {
        a[i] = fxVec2Add(a[i], zeroPoint);
    }

    for(i = 0;i<bSize;i++)
    {
        b[i] = fxVec2Add(b[i], zeroPoint);
    }

    return ret;
}
//----------------------------------------------------------------------------------------------------------------

BOOL mthIntersectLLv2(struct fxVec2 a[2], struct fxVec2 b[2])
{
    fx32 d,da,db,ta,tb;

    static const fx32 PREC = FX32(0.0001);
/*
 *
 *d :=(a1.x-a2.x)*(b2.y-b1.y) - (a1.y-a2.y)*(b2.x-b1.x);
  da:=(a1.x-b1.x)*(b2.y-b1.y) - (a1.y-b1.y)*(b2.x-b1.x);
  db:=(a1.x-a2.x)*(a1.y-b1.y) - (a1.y-a2.y)*(a1.x-b1.x);
 **/

    d = fxMul( (a[0].x - a[1].x), (b[1].y - b[0].y) ) - fxMul( (a[0].y - a[1].y), (b[1].x - b[0].x) );
    da = fxMul( (a[0].x - b[0].x), (b[1].y - b[0].y) ) - fxMul( (a[0].y - b[0].y), (b[1].x - b[0].x) );
    db = fxMul( (a[0].x - a[1].x), (a[0].y - b[0].y) ) - fxMul( (a[0].y - a[1].y), (a[0].x - b[0].x) );

    /*
     *if (abs(d)<eps) then
        checkIntersection := 0
      else
        begin
        ta:=da/d;
        tb:=db/d;
        if    (0<=ta) and (ta<=1)
          and (0<=tb) and (tb<=1)
            then
              begin
              c.setPoint(a1.x+ta*(a2.x-a1.x),a1.y+ta*(a2.y-a1.y));
              checkIntersection := 1
              end
            else checkIntersection := -1;
        end;
      end;
     *
     **/
    if(fxAbs(d) > PREC)
    {
        ta = fxDiv(da, d);
        tb = fxDiv(db, d);
        if((FX32(0.0) <= ta) && (ta <= FX32(1.0)) && (FX32(0.0) <= tb) && (tb <= FX32(1.0)))
        {
			return TRUE;
        }
		else
        {
            return FALSE;
        }
    }
    return FALSE;
}
//----------------------------------------------------------------------------------------------------------------

BOOL mthIntersectPR(struct fxVec2 a[4], s32 aSize, struct fxVec2 b)
{
	s32 i, j, k;
	fx32 dot, pos, min, max;
	struct fxVec2 xAxis;
	struct fxVec2 E;
	struct fxVec2 zeroPoint;

	zeroPoint = b;
	for (i = 0; i < aSize;i++)
    {
		zeroPoint.x = fxMin(zeroPoint.x, a[i].x);
		zeroPoint.y = fxMin(zeroPoint.y, a[i].y);
	}

	zeroPoint.x = zeroPoint.x - FX32(1.0);
	zeroPoint.y = zeroPoint.y - FX32(1.0);
		
    for (i = 0,j = aSize - 1; i < aSize;j=i,i++)
    {
        E = fxVec2Sub( fxVec2Sub(a[j], zeroPoint), fxVec2Sub(a[i], zeroPoint));
        xAxis = fxVec2Create(-E.y, E.x);

		max = fxVec2Dot(fxVec2Sub(a[0], zeroPoint),xAxis); // shape projection
		min = max;
		for (k=0; k < aSize; k++)
		{
			dot = fxVec2Dot(fxVec2Sub(a[k], zeroPoint), xAxis); 
			if (dot < min)
			{
				min = dot;
			}
			else if (dot > max)
			{
				max = dot;
			}
		}			

		pos = fxVec2Dot(fxVec2Sub(b, zeroPoint),xAxis);  //point projection

		if ( (pos < min) || (pos > max) )
		{
			return FALSE;
		}		
    }
	return TRUE;
}
//----------------------------------------------------------------------------------------------------------------

BOOL mthIntersectPRv2(struct fxVec2 a[4], struct fxVec2 b)
{
    s32 i, inters;
    struct fxVec2 l1[2];
    struct fxVec2 l2[2];
    struct fxVec2 zeroPoint;
    zeroPoint = b;
    for (i = 0; i < 4; i++)
    {
        zeroPoint.y = fxMin(zeroPoint.y, a[i].y);
    }
    zeroPoint.y = zeroPoint.y - FX32(1.0);
    inters = 0;
    l1[0] = fxVec2Sub(b, zeroPoint);
    l1[1] = fxVec2Create(FX32(0.0), FX32(0.0));
    for(i = 0 ; i < 3 ; i++)
    {
        l2[0] = fxVec2Sub(a[i], zeroPoint);
        l2[1] = fxVec2Sub(a[i + 1], zeroPoint);
        if(mthIntersectLLv2(l1, l2) == TRUE)
        {
            inters++;
        }
    }
    if(inters == 1)
    {
        return TRUE;
    }
    return FALSE;
}
//----------------------------------------------------------------------------------------------------------------

BOOL mthIntersectPRv3(struct fxVec2 a[4], struct fxVec2 b)
{
    s32 i,inters;
	struct fxVec2 l1[2];
    struct fxVec2 l2[2];

    inters = 0;

    l1[0] = b;
    l1[1] = fxVec2Create( FX32(0.0), FX32(0.0) );

    for( i = 0 ; i < 3 ; i++)
    {
        l2[0] = a[i];
        l2[1] = a[i+1];
        if(mthIntersectLL(l1, l2) == TRUE )
        {
            inters++;
        }
    }
    if( (inters == 2) || (inters == 0) )
    {
        return FALSE;
    }
    return TRUE;
}
//----------------------------------------------------------------------------------------------------------------

BOOL mthIntersectPointAndRectangle(struct fxVec2 point, struct fxVec2 rect[4])
{
	BOOL res;
	s32 v1, v2, xp, yp, xv, yv;
	const s32 rectSize = 4;
	res = FALSE;
#if defined USE_FX32_AS_FLOAT
	xp = (s32)point.x;
	yp = (s32)point.y;
#else
	xp = point.x >> FX32_SHIFT;
	yp = point.y >> FX32_SHIFT;
#endif
	for(v1 = 0; v1 < rectSize; v1++) 
	{
		v2 = (v1 + 1) % rectSize;
		if((point.y > rect[v1].y) ^ (point.y > rect[v2].y))
		{
#if defined USE_FX32_AS_FLOAT
			xv = (s32)rect[v1].x;
			yv = (s32)rect[v1].y;
#else
			xv = rect[v1].x >> FX32_SHIFT;
			yv = rect[v1].y >> FX32_SHIFT;
#endif
#if defined USE_FX32_AS_FLOAT
			if(xp > xv + (((s32)rect[v2].x) - xv) * (yp - yv) / (((s32)rect[v2].y) - yv))
#else
			if(xp > xv + ((rect[v2].x >> FX32_SHIFT) - xv) * (yp - yv) / ((rect[v2].y >> FX32_SHIFT) - yv))
#endif
			{ 
				res = ~res;
			}
		}
	}
	return res;
}
//----------------------------------------------------------------------------------------------------------------

fxMat22 fxMat22FromAngle(fx32 angle)
{
	fx32 c = fxCos(angle);
	fx32 s = fxSin(angle);
	fxMat22 mat;

	mat.c.col1.x = c;
	mat.c.col1.y = s;
	mat.c.col2.x = -s;
	mat.c.col2.y = c;

	return mat;
}

fxMat22 fxMat22FromVecs(struct fxVec2*  col1, struct fxVec2* col2)
{
	fxMat22 mat;
	mat.c.col1 = *(col1);
	mat.c.col2 = *(col2);
	return mat;
}

fxMat22 fxMat22Transpose(fxMat22* mat22)
{
	fxMat22 mat;

	mat.c.col1.x = mat22->c.col1.x;
	mat.c.col1.y = mat22->c.col2.x;

	mat.c.col2.x = mat22->c.col1.y;
	mat.c.col2.y = mat22->c.col2.y;

	return mat;
}

fxMat22 fxMat22Invert(fxMat22* mat22)
{
	fx32 a = mat22->mt._00;
	fx32 b = mat22->mt._10; 
	fx32 c = mat22->mt._01; 
	fx32 d = mat22->mt._11;
	fxMat22 B;
	fx32  det = fxMul(a, d) - fxMul(b, c);
	SDK_ASSERT( det != FX32(0.0) );
	det = fxDiv( FX32(1.0), det);

	B.mt._00 = fxMul(det, d);
	B.mt._01 = -fxMul(det, c);

	B.mt._10 = -fxMul(det, b);
	B.mt._11 = fxMul(det, a);

	return B;
}

struct fxVec2 fxMat22MulVec(fxMat22* mat, struct fxVec2*  vec)
{
	struct fxVec2 ret;
	ret.x = fxMul(mat->c.col1.x, vec->x)
		+	fxMul(mat->c.col2.x, vec->y);
	ret.y = fxMul(mat->c.col1.y, vec->x)
		+	fxMul(mat->c.col2.y, vec->y);
	return ret;
}

fxMat22 fxMat22Add(fxMat22* mat1, fxMat22* mat2)
{
	struct fxVec2 v1 = fxVec2Add(mat1->c.col1, mat2->c.col1);
	struct fxVec2 v2 = fxVec2Add(mat1->c.col2, mat2->c.col2);
	return fxMat22FromVecs(&v1, &v2);
}

fxMat22 fxMat22Mul(fxMat22* mat1, fxMat22* mat2)
{
	struct fxVec2 v1 = fxMat22MulVec(mat1, &mat2->c.col1);
	struct fxVec2 v2 = fxMat22MulVec(mat1, &mat2->c.col2);
	return fxMat22FromVecs(	&v1, &v2);	
}

fxMat22 fxMat22Abs(fxMat22 * mat)
{
	struct fxVec2 r1 = fxVec2Abs(mat->c.col1);
	struct fxVec2 r2 = fxVec2Abs(mat->c.col2);
	return fxMat22FromVecs(&r1, &r2);
}

//from Box2D ))
struct fxVec2 fxVec2CrossFx(struct fxVec2 const v1, fx32 const s)
{
	struct fxVec2 ret;
	ret.x = fxMul(s, v1.y);
	ret.y = fxMul(-s, v1.x);
	return ret;
}

struct fxVec2 fxVec2CrossFxInv(fx32 const s, struct fxVec2 const v1)
{
	struct fxVec2 ret;
	ret.x = fxMul(-s, v1.y);
	ret.y = fxMul(s, v1.x);
	return ret;
}

struct fxVec2 fxVec2Abs(struct fxVec2 vec)
{
#if defined USE_FX32_AS_FLOAT
	return fxVec2Create(fabs(vec.x), fabs(vec.y));
#else
	return fxVec2Create(MATH_ABS(vec.x), MATH_ABS(vec.y));
#endif
}
