/*
tengine - cross-platform game engine
------------------------------------

Copyright (c) 2013 Papa Pascualle
Contact Email: pascualle@gmail.com

Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
*/

#include "texts.h"
#include "texts_low.h"
#include <string.h>
#include <wctype.h>
#include <stdarg.h>

#define _USE_FLOAT

//---------------------------------------------------------------------------

#ifdef _USE_FLOAT
static wchar* STD_WDtoA(wchar *s, double n);
#endif

/*
	u16 fontSize;
	u8 bitField; // bit 0: smooth, bit 1 : unicode, bit 2 : italic, bit 3 : bold, bit 4 : fixedHeigth, bits 5 - 7 : reserved
	u8 charSet;
	u16 stretchH;
	u8 aa;
	u8 paddingUp;
	u8 paddingRight;
	u8 paddingDown;
	u8 paddingLeft;
	u8 spacingHoriz;
	u8 spacingVert;
	u8 outline;
	const char name[1];
*/

//---------------------------------------------------------------------------

const char* Font_GetName(const struct TERFont* font)
{
	SDK_NULL_ASSERT(font);
	SDK_NULL_ASSERT(font->mpInfo);
	return (const char*)(font->mpInfo + TEFONT_INFO_NAME_SHIFT);
}
//---------------------------------------------------------------------------

u16 Font_GetSize(const struct TERFont* font)
{
	SDK_NULL_ASSERT(font);
	SDK_NULL_ASSERT(font->mpInfo);
	return (u16)*font->mpInfo;
}
//---------------------------------------------------------------------------

u16 Font_GetInfoBitField(const struct TERFont* font)
{
	SDK_NULL_ASSERT(font);
	SDK_NULL_ASSERT(font->mpInfo);
	return (u8)*(font->mpInfo + TEFONT_INFO_FLAGS_SHIFT);
}
//---------------------------------------------------------------------------

#ifndef NITRO_SDK

//---------------------------------------------------------------------------

s32 STD_StrLen(const char *str)
{
	return (s32)strlen(str);
}
//---------------------------------------------------------------------------

char* STD_StrCpy(char *dest, const char *src)
{
	return strcpy(dest, src);
}
//---------------------------------------------------------------------------

s32 STD_StrCmp(const char *a, const char *b)
{
	return strcmp(a, b);
}
//---------------------------------------------------------------------------

char *STD_StrCat(char *str1, const char *str2)
{
	return strcat(str1, str2);
}
//---------------------------------------------------------------------------

#endif

void STD_NumToString(u16 num, char *buf, u16 bufSize)
{
	s32 idx;
	s32 max = 1;
	if(num > 0)
	{
		idx = 0;
		while(num >= max)
		{
			max *= 10;
			++idx;
		}
		SDK_ASSERT(bufSize > idx);
		max = idx;
		while(num)
		{
			--idx;
			buf[idx] = (char)('0' + (num % 10));
			num /= 10;
		}
		buf[max] = 0;
	}
	else
	{
		SDK_ASSERT(bufSize >= 2);
		buf[0] = '0';
		buf[1] = 0;
	}
	(void)bufSize;
}
//---------------------------------------------------------------------------

s32 STD_WStrLen(const wchar *str)
{
	s32 ct = 0;
	SDK_ASSERT(str);
	while(str[ct] != W_T('\0'))ct++;
	return ct;
}
//---------------------------------------------------------------------------

static s32 _log10(double n)
{
	s32 neg;
	s32 ret;
	s32 exp = 0;
	neg = (n < 0) ? 1 : 0;
	ret = (neg) ? (s32)-n : (s32)n;
	while((ret / 10) > 0)
	{
		ret /= 10;
		exp++;
	}
	if(neg) exp = -exp;
	return exp;
}
//---------------------------------------------------------------------------

static double _pow(double x, s32 n)
{
	double r = 1;
	s32 plus = n >= 0;
	n = (plus) ? n : -n;
	while(n > 0)
	{
		if((n & 1) == 1)
			r *= x;
		n /= 2;
		x *= x;
	}
	return plus ? r : 1.0 / r;
}
//---------------------------------------------------------------------------

static s32 _ifloord(double x)
{
	x = (double)((s32)x - ((x < 0.0) ? 1 : 0));
	return (s32)x;
}
//---------------------------------------------------------------------------

#define VSPRINTFW_MAX_NUMBER_BUFFER 64
#define VSPRINTFW_DEFAULT (-1)
#define VSPRINTFW_MAX(a,b) ((a) < (b) ? (b) : (a))
#define VSPRINTFW_MIN(a,b) ((a) < (b) ? (a) : (b))

static s32 _vsnprintf_wchar(wchar *buf, s32 buf_size, const wchar *fmt, va_list args)
{
	enum vsnprintf_wchar_arg_type
	{
		VSPRINTFW_ARG_TYPE_CHAR,
		VSPRINTFW_ARG_TYPE_SHORT,
		VSPRINTFW_ARG_TYPE_DEFAULT,
		VSPRINTFW_ARG_TYPE_LONG
	};
	enum vsnprintf_wchar_arg_flags
	{
		VSPRINTFW_ARG_FLAG_LEFT = 0x01,
		VSPRINTFW_ARG_FLAG_PLUS = 0x02,
		VSPRINTFW_ARG_FLAG_SPACE = 0x04,
		VSPRINTFW_ARG_FLAG_NUM = 0x10,
		VSPRINTFW_ARG_FLAG_ZERO = 0x20
	};

	wchar number_buffer[VSPRINTFW_MAX_NUMBER_BUFFER];
	enum vsnprintf_wchar_arg_type arg_type = VSPRINTFW_ARG_TYPE_DEFAULT;
	s32 width;
	s32 precision;
	u32 flag = 0;

	s32 len = 0;
	s32 result = -1;
	const wchar *iter;

	SDK_ASSERT(buf);
	SDK_ASSERT(buf_size);
	if(!buf || !buf_size || !fmt) return 0;
	for(iter = fmt; *iter && len < buf_size; iter++)
	{
		while(*iter && (*iter != W_T('%')) && (len < buf_size))
		{
			buf[len++] = *iter++;
		}
		if(!(*iter) || len >= buf_size) break;
		iter++;

		while(*iter)
		{
			if(*iter == W_T('-')) flag |= VSPRINTFW_ARG_FLAG_LEFT;
			else if(*iter == W_T('+')) flag |= VSPRINTFW_ARG_FLAG_PLUS;
			else if(*iter == W_T(' ')) flag |= VSPRINTFW_ARG_FLAG_SPACE;
			else if(*iter == W_T('#')) flag |= VSPRINTFW_ARG_FLAG_NUM;
			else if(*iter == W_T('0')) flag |= VSPRINTFW_ARG_FLAG_ZERO;
			else break;
			iter++;
		}

		width = VSPRINTFW_DEFAULT;
		if(*iter >= W_T('1') && *iter <= W_T('9'))
		{
			wchar *end;
			width = STD_WStrtoI(iter, &end);
			if(end == iter)
				width = -1;
			else iter = end;
		}
		else if(*iter == W_T('*'))
		{
			width = va_arg(args, s32);
			iter++;
		}

		precision = VSPRINTFW_DEFAULT;
		if(*iter == W_T('.'))
		{
			iter++;
			if(*iter == W_T('*'))
			{
				precision = va_arg(args, s32);
				iter++;
			}
			else
			{
				wchar *end;
				precision = STD_WStrtoI(iter, &end);
				if(end == iter)
					precision = -1;
				else iter = end;
			}
		}

		if(*iter == W_T('h'))
		{
			if(*(iter + 1) == W_T('h'))
			{
				arg_type = VSPRINTFW_ARG_TYPE_CHAR;
				iter++;
			}
			else arg_type = VSPRINTFW_ARG_TYPE_SHORT;
			iter++;
		}
		else if(*iter == W_T('l'))
		{
			arg_type = VSPRINTFW_ARG_TYPE_LONG;
			iter++;
		}
		else arg_type = VSPRINTFW_ARG_TYPE_DEFAULT;

		if(*iter == W_T('%'))
		{
			SDK_ASSERT(arg_type == VSPRINTFW_ARG_TYPE_DEFAULT);
			SDK_ASSERT(precision == VSPRINTFW_DEFAULT);
			SDK_ASSERT(width == VSPRINTFW_DEFAULT);
			if(len < buf_size)
				buf[len++] = W_T('%');
		}
		else if(*iter == W_T('s'))
		{
			const wchar *str = va_arg(args, const wchar*);
			SDK_ASSERT(str != buf && "buffer and argument are not allowed to overlap!");
			SDK_ASSERT(arg_type == VSPRINTFW_ARG_TYPE_DEFAULT);
			SDK_ASSERT(precision == VSPRINTFW_DEFAULT);
			SDK_ASSERT(width == VSPRINTFW_DEFAULT);
			if(str == buf) return -1;
			while(str && *str && len < buf_size)
				buf[len++] = *str++;
		}
		else if(*iter == W_T('n'))
		{
			s32 *n = va_arg(args, s32*);
			SDK_ASSERT(arg_type == VSPRINTFW_ARG_TYPE_DEFAULT);
			SDK_ASSERT(precision == VSPRINTFW_DEFAULT);
			SDK_ASSERT(width == VSPRINTFW_DEFAULT);
			if(n) *n = len;
		}
		else if(*iter == W_T('c') || *iter == W_T('i') || *iter == W_T('d'))
		{
			long value;
			const wchar *num_iter;
			s32 num_len, num_print, padding;
			s32 cur_precision = VSPRINTFW_MAX(precision, 1);
			s32 cur_width = VSPRINTFW_MAX(width, 0);

			if(arg_type == VSPRINTFW_ARG_TYPE_CHAR)
				value = (signed char)va_arg(args, s32);
			else if(arg_type == VSPRINTFW_ARG_TYPE_SHORT)
				value = (s16)va_arg(args, s32);
			else if(arg_type == VSPRINTFW_ARG_TYPE_LONG)
				value = va_arg(args, signed long);
			else if(*iter == W_T('c'))
				value = (unsigned char)va_arg(args, s32);
			else value = va_arg(args, s32);

			STD_WItoA(number_buffer, value);
			num_len = STD_WStrLen(number_buffer);
			padding = VSPRINTFW_MAX(cur_width - VSPRINTFW_MAX(cur_precision, num_len), 0);
			if((flag & VSPRINTFW_ARG_FLAG_PLUS) || (flag & VSPRINTFW_ARG_FLAG_SPACE))
				padding = VSPRINTFW_MAX(padding - 1, 0);

			if(!(flag & VSPRINTFW_ARG_FLAG_LEFT))
			{
				while(padding-- > 0 && (len < buf_size))
				{
					if((flag & VSPRINTFW_ARG_FLAG_ZERO) && (precision == VSPRINTFW_DEFAULT))
						buf[len++] = W_T('0');
					else buf[len++] = W_T(' ');
				}
			}

			if((flag & VSPRINTFW_ARG_FLAG_PLUS) && value >= 0 && len < buf_size)
				buf[len++] = W_T('+');
			else if((flag & VSPRINTFW_ARG_FLAG_SPACE) && value >= 0 && len < buf_size)
				buf[len++] = W_T(' ');

			num_print = VSPRINTFW_MAX(cur_precision, num_len);
			while(precision && (num_print > num_len) && (len < buf_size))
			{
				buf[len++] = W_T('0');
				num_print--;
			}

			num_iter = number_buffer;
			while(precision && *num_iter && len < buf_size)
				buf[len++] = *num_iter++;

			if(flag & VSPRINTFW_ARG_FLAG_LEFT)
			{
				while((padding-- > 0) && (len < buf_size))
					buf[len++] = W_T(' ');
			}
		}
		else if(*iter == W_T('o') || *iter == W_T('x') || *iter == W_T('X') || *iter == W_T('u'))
		{
			unsigned long value;
			s32 num_len = 0, num_print, padding;
			s32 cur_precision = VSPRINTFW_MAX(precision, 1);
			s32 cur_width = VSPRINTFW_MAX(width, 0);
			u32 base = (*iter == W_T('o')) ? 8 : (*iter == W_T('u')) ? 10 : 16;

			const wchar *upper_output_format = W_T("0123456789ABCDEF");
			const wchar *lower_output_format = W_T("0123456789abcdef");
			const wchar *output_format = (*iter == W_T('x')) ?
				lower_output_format : upper_output_format;

			if(arg_type == VSPRINTFW_ARG_TYPE_CHAR)
				value = (unsigned char)va_arg(args, s32);
			else if(arg_type == VSPRINTFW_ARG_TYPE_SHORT)
				value = (u16)va_arg(args, s32);
			else if(arg_type == VSPRINTFW_ARG_TYPE_LONG)
				value = va_arg(args, unsigned long);
			else value = va_arg(args, u32);

			do {
				s32 digit = output_format[value % base];
				if(num_len < VSPRINTFW_MAX_NUMBER_BUFFER)
					number_buffer[num_len++] = (wchar)digit;
				value /= base;
			} while(value > 0);

			num_print = VSPRINTFW_MAX(cur_precision, num_len);
			padding = VSPRINTFW_MAX(cur_width - VSPRINTFW_MAX(cur_precision, num_len), 0);
			if(flag & VSPRINTFW_ARG_FLAG_NUM)
				padding = VSPRINTFW_MAX(padding - 1, 0);

			if(!(flag & VSPRINTFW_ARG_FLAG_LEFT))
			{
				while((padding-- > 0) && (len < buf_size)) {
					if((flag & VSPRINTFW_ARG_FLAG_ZERO) && (precision == VSPRINTFW_DEFAULT))
						buf[len++] = W_T('0');
					else buf[len++] = W_T(' ');
				}
			}

			if(num_print && (flag & VSPRINTFW_ARG_FLAG_NUM))
			{
				if((*iter == W_T('o')) && (len < buf_size))
				{
					buf[len++] = W_T('0');
				}
				else if((*iter == W_T('x')) && ((len + 1) < buf_size))
				{
					buf[len++] = W_T('0');
					buf[len++] = W_T('x');
				}
				else if((*iter == W_T('X')) && ((len + 1) < buf_size))
				{
					buf[len++] = W_T('0');
					buf[len++] = W_T('X');
				}
			}
			while(precision && (num_print > num_len) && (len < buf_size))
			{
				buf[len++] = W_T('0');
				num_print--;
			}

			while(num_len > 0)
			{
				if(precision && (len < buf_size))
					buf[len++] = number_buffer[num_len - 1];
				num_len--;
			}

			if(flag & VSPRINTFW_ARG_FLAG_LEFT)
			{
				while((padding-- > 0) && (len < buf_size))
					buf[len++] = W_T(' ');
			}
		}
#ifdef _USE_FLOAT
		else if(*iter == W_T('f') || *iter == W_T('F'))
		{
			const wchar *num_iter;
			double value;
			s32 padding;
			s32 cur_precision = (precision < 0) ? 6 : precision;
			s32 prefix, cur_width = VSPRINTFW_MAX(width, 0);
			s32 num_len, frac_len = 0, dot = 0;

#if !defined USE_FX32_AS_FLOAT
			if(*iter == W_T('F'))
			{
				fx32 fx32value = (fx32)va_arg(args, fx32);
				value = (double)fx32value / 65535.0;
			}
			else
#endif
			{
				value = va_arg(args, double);
			}

			SDK_ASSERT(arg_type == VSPRINTFW_ARG_TYPE_DEFAULT);
			STD_WDtoA(number_buffer, value);
			num_len = STD_WStrLen(number_buffer);

			num_iter = number_buffer;
			while(*num_iter && *num_iter != W_T('.'))
				num_iter++;

			prefix = (*num_iter == W_T('.')) ? (s32)(num_iter - number_buffer) + 1 : 0;
			padding = VSPRINTFW_MAX(cur_width - (prefix + VSPRINTFW_MIN(cur_precision, num_len - prefix)), 0);
			if((flag & VSPRINTFW_ARG_FLAG_PLUS) || (flag & VSPRINTFW_ARG_FLAG_SPACE))
				padding = VSPRINTFW_MAX(padding - 1, 0);

			if(!(flag & VSPRINTFW_ARG_FLAG_LEFT))
			{
				while(padding-- > 0 && (len < buf_size))
				{
					if(flag & VSPRINTFW_ARG_FLAG_ZERO)
						buf[len++] = W_T('0');
					else buf[len++] = W_T(' ');
				}
			}

			num_iter = number_buffer;
			if((flag & VSPRINTFW_ARG_FLAG_PLUS) && (value >= 0) && (len < buf_size))
				buf[len++] = W_T('+');
			else if((flag & VSPRINTFW_ARG_FLAG_SPACE) && (value >= 0) && (len < buf_size))
				buf[len++] = W_T(' ');
			while(*num_iter)
			{
				if(dot) frac_len++;
				if(len < buf_size)
					buf[len++] = *num_iter;
				if(*num_iter == W_T('.')) dot = 1;
				if(frac_len >= cur_precision) break;
				num_iter++;
			}

			while(frac_len < cur_precision)
			{
				if(!dot && len < buf_size)
				{
					buf[len++] = W_T('.');
					dot = 1;
				}
				if(len < buf_size)
					buf[len++] = W_T('0');
				frac_len++;
			}

			if(flag & VSPRINTFW_ARG_FLAG_LEFT)
			{
				while((padding-- > 0) && (len < buf_size))
					buf[len++] = W_T(' ');
			}
		}
#endif
		else
		{
			/* Specifier not supported: g,G,e,E,p,z */
			SDK_ASSERT(0 && "specifier is not supported!");
			return result;
		}
	}
	buf[(len >= buf_size) ? (buf_size - 1) : len] = W_T('\0');
	result = (len >= buf_size) ? -1 : len;
	return result;
}
//---------------------------------------------------------------------------

s32 STD_WSnprintf(wchar *buf, s32 buf_size, const wchar *fmt, ...)
{
	va_list args;
	s32 i;
	va_start(args, fmt);
	i = _vsnprintf_wchar(buf, buf_size, fmt, args);
	va_end(args);
	return i;
}
//---------------------------------------------------------------------------

s32 STD_WStrCmp(const wchar *cs, const wchar *ct)
{
	while(*cs != W_T('\0') && *ct != W_T('\0') && *cs == *ct)
	{
		++cs;
		++ct;
	}
	return *cs - *ct;
}
//---------------------------------------------------------------------------

s32 STD_WStrNCmp(const wchar *s1, const wchar *s2, s32 n)
{
	wchar c1 = W_T('\0');
	wchar c2 = W_T('\0');
	if(n >= 4)
	{
		size_t n4 = n >> 2;
		do
		{
			c1 = *s1++;
			c2 = *s2++;
			if(c1 == W_T('\0') || c1 != c2)
			{
				return c1 - c2;
			}
			c1 = *s1++;
			c2 = *s2++;
			if(c1 == W_T('\0') || c1 != c2)
			{
				return c1 - c2;
			}
			c1 = *s1++;
			c2 = *s2++;
			if(c1 == W_T('\0') || c1 != c2)
			{
				return c1 - c2;
			}
			c1 = *s1++;
			c2 = *s2++;
			if(c1 == W_T('\0') || c1 != c2)
			{
				return c1 - c2;
			}
		} while(--n4 > 0);
		n &= 3;
	}
	while(n > 0)
	{
		c1 = *s1++;
		c2 = *s2++;
		if(c1 == W_T('\0') || c1 != c2)
		{
			return c1 - c2;
		}
		n--;
	}
	return c1 - c2;
}
//---------------------------------------------------------------------------

wchar* STD_WStrCpy(wchar* str1, const wchar* str2)
{
	wchar* s = str1;
	while((*str2) != W_T('\0'))
	{
		*s = *str2;
		++s;
		++str2;
	}
	*s = W_T('\0');
	return(str1);
}
//---------------------------------------------------------------------------

wchar *STD_WStrStr(wchar *s, const wchar *find)
{
	wchar c, sc;
	s32 len;
	if((c = *find++) != W_T('\0'))
	{
		len = STD_WStrLen(find);
		do
		{
			do
			{
				if((sc = *s++) == W_T('\0'))
				{
					return NULL;
				}
			} while(sc != c);
		} while(STD_WStrNCmp(s, find, len) != 0);
		s--;
	}
	return s;
}

//---------------------------------------------------------------------------
#ifdef _USE_FLOAT
#define WDTOA_FLOAT_PRECISION 0.00000000000001
wchar* STD_WDtoA(wchar *s, double n)
{
	s32 neg;
	s32 useExp;
	s32 digit, m, m1 = 0;
	wchar *c = s;
	SDK_ASSERT(s);
	if(!s) return 0;
	if(n == 0.0)
	{
		s[0] = W_T('0'); s[1] = W_T('\0');
		return s;
	}
	neg = (n < 0);
	if(neg) n = -n;
	m = _log10(n);
	useExp = (m >= 14 || (neg && m >= 9) || m <= -9);
	if(neg) *(c++) = W_T('-');
	if(useExp)
	{
		if(m < 0)
			m -= 1;
		n = n / (double)_pow(10.0, m);
		m1 = m;
		m = 0;
	}
	if(m < 1.0)
	{
		m = 0;
	}
	while(n > WDTOA_FLOAT_PRECISION || m >= 0)
	{
		double weight = _pow(10.0, m);
		if(weight > 0)
		{
			double t = (double)n / weight;
			digit = _ifloord(t);
			n -= ((double)digit * weight);
			*(c++) = (wchar)(W_T('0') + (wchar)digit);
		}
		if(m == 0 && n > 0)
			*(c++) = W_T('.');
		m--;
	}
	if(useExp)
	{
		s32 i, j;
		*(c++) = W_T('e');
		if(m1 > 0)
		{
			*(c++) = W_T('+');
		}
		else
		{
			*(c++) = W_T('-');
			m1 = -m1;
		}
		m = 0;
		while(m1 > 0)
		{
			*(c++) = (wchar)(W_T('0') + (wchar)(m1 % 10));
			m1 /= 10;
			m++;
		}
		c -= m;
		for(i = 0, j = m - 1; i<j; i++, j--)
		{
			c[i] ^= c[j];
			c[j] ^= c[i];
			c[i] ^= c[j];
		}
		c += m;
	}
	*(c) = W_T('\0');
	return s;
}
#endif
//---------------------------------------------------------------------------

wchar* STD_WItoA(wchar *s, s32 n)
{
	s32 i = 0;
	if(n == 0)
	{
		s[i++] = W_T('0');
		s[i] = 0;
		return s;
	}
	if(n < 0)
	{
		s[i++] = W_T('-');
		n = -n;
	}
	while(n > 0)
	{
		s[i++] = (wchar)(W_T('0') + (n % 10));
		n /= 10;
	}
	s[i] = 0;
	if(s[0] == W_T('-'))
		++s;

	{
		const s32 len = STD_WStrLen(s);
		const s32 end = len / 2;
		wchar t;
		i = 0;
		for(; i < end; ++i)
		{
			t = s[i];
			s[i] = s[len - 1 - i];
			s[len - 1 - i] = t;
		}
	}
	return s;
}
//---------------------------------------------------------------------------

s32 STD_WStrtoI(const wchar *str, wchar **endptr)
{
	s32 neg = 1;
	const wchar *p = str;
	s32 value = 0;

	SDK_ASSERT(str);
	if(!str) return 0;

	while(*p == W_T(' ')) p++;
	if(*p == W_T('-'))
	{
		neg = -1;
		p++;
	}
	while(*p && *p >= W_T('0') && *p <= W_T('9'))
	{
		value = value * 10 + (s32)(*p - W_T('0'));
		p++;
	}
	if (endptr)
	{
		*endptr = (wchar*)p;
	}
	return neg * value;
}
//---------------------------------------------------------------------------
