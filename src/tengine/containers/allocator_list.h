/*
tengine - cross-platform game engine
------------------------------------

Copyright (c) 2013 Papa Pascualle
Contact Email: pascualle@gmail.com

Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
*/

#ifndef ALLOCATOR_LIST_H
#define ALLOCATOR_LIST_H

#include "tengine.h"

#ifdef __cplusplus
extern "C" {
#endif

struct ListItem;
struct StaticAllocator;

struct AllocatorList
{
	u32 item_size;
	u32 item_count;
	struct ListItem* head;
	struct ListItem* tail;
	struct StaticAllocator *allocator;
};

u32 AllocatorList_CalculateHeapSize(u32 item_size, u32 max_items);
void AllocatorList_Init(struct AllocatorList* plist, u32 item_size, struct StaticAllocator *allocator);
void AllocatorList_Clear(struct AllocatorList* plist);
const struct ListItem* AllocatorList_PushBack(struct AllocatorList* plist, const void* data);
const struct ListItem* AllocatorList_Insert(struct AllocatorList* plist, struct ListItem* pos, const void* data);
void AllocatorList_Erase(struct AllocatorList* plist, struct ListItem* pos);
void AllocatorList_Move(struct AllocatorList* plist, struct ListItem* from_pos, struct ListItem* to_pos);
void* AllocatorList_Val(const struct AllocatorList* plist, const struct ListItem* pos);

const struct ListItem* AllocatorList_Next_Const(const struct AllocatorList* plist, const struct ListItem* pos);
const struct ListItem* AllocatorList_Prev_Const(const struct AllocatorList* plist, const struct ListItem* pos);
const struct ListItem* AllocatorList_Begin_Const(const struct AllocatorList* plist);
const struct ListItem* AllocatorList_End_Const(const struct AllocatorList* plist);
const struct ListItem* AllocatorList_Tail_Const(const struct AllocatorList* plist);

struct ListItem* AllocatorList_Next(const struct AllocatorList* plist, const struct ListItem* pos);
struct ListItem* AllocatorList_Prev(const struct AllocatorList* plist, const struct ListItem* pos);
struct ListItem* AllocatorList_Begin(const struct AllocatorList* plist);
struct ListItem* AllocatorList_End(const struct AllocatorList* plist);
struct ListItem* AllocatorList_Tail(const struct AllocatorList* plist);

u32 AllocatorList_Size(const struct AllocatorList* plist);
const struct ListItem* AllocatorList_Advance(const struct ListItem* pos, s32 distance);

#define A_LIST_INIT(TYPE, LIST_STRUCT, ALLOCATOR) AllocatorList_Init(&(LIST_STRUCT), sizeof(TYPE), &(ALLOCATOR))
#define A_LIST_PUSH_BACK(TYPE, LIST_STRUCT, ITEM) AllocatorList_PushBack(&(LIST_STRUCT), (void*)(&(ITEM)))
#define A_LIST_INSERT(TYPE, LIST_STRUCT, POS, ITEM) AllocatorList_Insert(&(LIST_STRUCT), POS, (void*)(&(ITEM)))
#define A_LIST_VAL(TYPE, LIST_STRUCT, POS) (*((TYPE*)AllocatorList_Val(&(LIST_STRUCT), POS)))
#define A_LIST_CLEAR(TYPE, LIST_STRUCT) AllocatorList_Clear(&(LIST_STRUCT))
#define A_LIST_ERASE(TYPE, LIST_STRUCT, POS) AllocatorList_Erase(&(LIST_STRUCT), POS)
#define A_LIST_NEXT(TYPE, LIST_STRUCT, POS) AllocatorList_Next(&(LIST_STRUCT), POS)
#define A_LIST_PREV(TYPE, LIST_STRUCT, POS) AllocatorList_Prev(&(LIST_STRUCT), POS)
#define A_LIST_BEGIN(TYPE, LIST_STRUCT) AllocatorList_Begin(&(LIST_STRUCT))
#define A_LIST_END(TYPE, LIST_STRUCT) AllocatorList_End(&(LIST_STRUCT))
#define A_LIST_TAIL(TYPE, LIST_STRUCT) AllocatorList_Begin(&(LIST_STRUCT))
#define A_LIST_SIZE(LIST_STRUCT) AllocatorList_Size(&(LIST_STRUCT))
#define A_LIST_ADVANCE(POS, DISTANCE) AllocatorList_Advance(POS, DISTANCE)

#ifdef __cplusplus
} /* extern "C" */
#endif

#endif
