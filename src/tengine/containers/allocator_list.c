/*
tengine - cross-platform game engine
------------------------------------

Copyright (c) 2013 Papa Pascualle
Contact Email: pascualle@gmail.com

Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
*/

#include "allocator_list.h"
#include "static_allocator.h"
 
//----------------------------------------------------------------------------------------------

struct ListItem
{
	struct ListItem* next;
	struct ListItem* prev;
	void* ptr;
#ifdef SDK_DEBUG
	const struct AllocatorList* plist;
#endif
};

#define A_LIST_BORDER_MARK NULL

//----------------------------------------------------------------------------------------------

u32 AllocatorList_CalculateHeapSize(u32 item_size, u32 max_items)
{
	return StaticAllocator_CalculateHeapSize(sizeof(struct ListItem)) * max_items +
		StaticAllocator_CalculateHeapSize(item_size) * max_items;
}
//----------------------------------------------------------------------------------------------

void AllocatorList_Init(struct AllocatorList* plist, u32 item_size, struct StaticAllocator *allocator)
{
	SDK_NULL_ASSERT(plist);
	SDK_NULL_ASSERT(allocator);
	SDK_ASSERT(item_size > 0);
	plist->head = A_LIST_BORDER_MARK;
	plist->tail = A_LIST_BORDER_MARK;
	plist->item_count = 0;
	plist->item_size = item_size;
	plist->allocator = allocator;
}
//----------------------------------------------------------------------------------------------

void AllocatorList_Clear(struct AllocatorList* plist)
{
	struct ListItem* iter;
	struct ListItem* niter;
	for(iter = plist->head; iter != AllocatorList_End(plist); iter = niter)
	{
		niter = AllocatorList_Next(plist, iter);
		StaticAllocator_Free(plist->allocator, iter->ptr);
		StaticAllocator_Free(plist->allocator, iter);
	}
	AllocatorList_Init(plist, plist->item_size, plist->allocator);
}
//----------------------------------------------------------------------------------------------

static void AllocatorList_push_first(struct AllocatorList* plist, struct ListItem* item)
{
	if(plist->tail != A_LIST_BORDER_MARK || plist->head != A_LIST_BORDER_MARK || plist->item_count != 0)
	{
		SDK_ASSERT(0); //Something wrong
		StaticAllocator_Free(plist->allocator, item->ptr);
		StaticAllocator_Free(plist->allocator, item);
		return;
	}
#ifdef SDK_DEBUG
	SDK_ASSERT(plist == item->plist);
#endif
	item->next = A_LIST_BORDER_MARK;
	item->prev = A_LIST_BORDER_MARK;
	plist->tail = item;
	plist->head = item;
	plist->item_count++;
}
//----------------------------------------------------------------------------------------------

const struct ListItem* AllocatorList_PushBack(struct AllocatorList* plist, const void* val)
{
	struct ListItem* newItem = (struct ListItem*)StaticAllocator_Malloc(plist->allocator, sizeof(struct ListItem));
	if(newItem != NULL)
	{
		newItem->ptr = StaticAllocator_Malloc(plist->allocator, plist->item_size);
		if(newItem->ptr != NULL)
		{
#ifdef SDK_DEBUG
			newItem->plist = plist;
#endif
			if (val != NULL)
			{
				MI_CpuCopy8(val, newItem->ptr, plist->item_size);
			}
			else
			{
				MI_CpuClear8(newItem->ptr, plist->item_size);
			}
			if(plist->tail == A_LIST_BORDER_MARK || plist->head == A_LIST_BORDER_MARK)
			{
				AllocatorList_push_first(plist, newItem);
			}
			else
			{
				newItem->next = A_LIST_BORDER_MARK;
				plist->tail->next = newItem;
				newItem->prev = plist->tail;
				plist->tail = newItem;
				plist->item_count++;
			}
			return newItem;
		}
		StaticAllocator_Free(plist->allocator, newItem);
	}
	SDK_ASSERT(0);
	return NULL;
}
//----------------------------------------------------------------------------------------------

const struct ListItem* AllocatorList_Insert(struct AllocatorList* plist, struct ListItem* pos, const void* val)
{
	struct ListItem* newItem = (struct ListItem*)StaticAllocator_Malloc(plist->allocator, sizeof(struct ListItem));
	if(newItem != NULL)
	{
		newItem->ptr = StaticAllocator_Malloc(plist->allocator, plist->item_size);
		if(newItem->ptr != NULL)
		{
#ifdef SDK_DEBUG
			newItem->plist = plist;
#endif
			if (val != NULL)
			{
				MI_CpuCopy8(val, newItem->ptr, plist->item_size);
			}
			else
			{
				MI_CpuClear8(newItem->ptr, plist->item_size);
			}
			if(plist->tail == A_LIST_BORDER_MARK || plist->head == A_LIST_BORDER_MARK)
			{
				AllocatorList_push_first(plist, newItem);
				return newItem;
			}
			else
			{
				newItem->next = pos;
				newItem->prev = pos->prev;
				if(newItem->prev != A_LIST_BORDER_MARK)
				{
					newItem->prev->next = newItem;
				}
				pos->prev = newItem;
				if(plist->head == pos)
				{
					plist->head = newItem;
				}
				plist->item_count++;
				return newItem;
			}
		}
		StaticAllocator_Free(plist->allocator, newItem);
	}
	SDK_ASSERT(0);
	return NULL;
}
//----------------------------------------------------------------------------------------------

void AllocatorList_Erase(struct AllocatorList* plist, struct ListItem* pos)
{
	if(plist->item_count == 0 || plist->item_count == 1 || (plist->head == pos && plist->tail == pos) || (pos->next == A_LIST_BORDER_MARK && pos->prev == A_LIST_BORDER_MARK))
	{
		if(plist->item_count != 1 || plist->head != pos || plist->tail != pos || pos->next != A_LIST_BORDER_MARK || pos->prev != A_LIST_BORDER_MARK)
		{
			SDK_ASSERT(0);
			return;
		}
#ifdef SDK_DEBUG
		SDK_ASSERT(plist == pos->plist);
#endif
		AllocatorList_Clear(plist);
	}
	else
	{
#ifdef SDK_DEBUG
		SDK_ASSERT(plist == pos->plist);
#endif
		if(pos->prev != A_LIST_BORDER_MARK)
		{
			pos->prev->next = pos->next;
		}
		else
		{
			if(plist->head != pos)
			{
				SDK_ASSERT(0);
			}
			else
			{
				plist->head = pos->next;
			}
		}
		if(pos->next != A_LIST_BORDER_MARK)
		{
			pos->next->prev = pos->prev;
		}
		else
		{
			if(plist->tail != pos)
			{
				SDK_ASSERT(0);
			}
			else
			{
				plist->tail = pos->prev;
			}
		}
		plist->item_count--;
		StaticAllocator_Free(plist->allocator, pos->ptr);
		StaticAllocator_Free(plist->allocator, pos);
	}
}
//----------------------------------------------------------------------------------------------

void AllocatorList_Move(struct AllocatorList* plist, struct ListItem* from, struct ListItem* to)
{
	if (plist->item_count > 0 && from != to)
	{
#ifdef SDK_DEBUG
		SDK_ASSERT(plist == from->plist);
		SDK_ASSERT(plist == to->plist);
#endif
		if (from->prev != A_LIST_BORDER_MARK)
		{
			from->prev->next = from->next;
		}
		else
		{
			if (plist->head != from)
			{
				SDK_ASSERT(0);
			}
			else
			{
				plist->head = from->next;
			}
		}
		if (from->next != A_LIST_BORDER_MARK)
		{
			from->next->prev = from->prev;
		}
		else
		{
			if (plist->tail != from)
			{
				SDK_ASSERT(0);
			}
			else
			{
				plist->tail = from->prev;
			}
		}
		
		{
			BOOL after_pos = FALSE;
			struct ListItem* pos = from;
			while (pos->next != A_LIST_BORDER_MARK)
			{
				if (pos == to)
				{
					after_pos = TRUE;
					break;
				}
				pos = pos->next;
			}

			if (after_pos == TRUE)
			{
				from->next = to->next;
				to->next = from;
				if (from->next != A_LIST_BORDER_MARK)
				{
					from->next->prev = from;
				}
				from->prev = to;
				if (plist->tail == to)
				{
					plist->tail = from;
				}
			}
			else
			{
				from->next = to;
				from->prev = to->prev;
				if (from->prev != A_LIST_BORDER_MARK)
				{
					from->prev->next = from;
				}
				to->prev = from;
				if (plist->head == to)
				{
					plist->head = from;
				}
			}
		}
	}
}
//----------------------------------------------------------------------------------------------

void* AllocatorList_Val(const struct AllocatorList* plist, const struct ListItem* pos)
{
#ifdef SDK_DEBUG
	SDK_ASSERT(plist == pos->plist);
#else
	(void)plist;
#endif
	return pos->ptr;
}
//----------------------------------------------------------------------------------------------

u32 AllocatorList_Size(const struct AllocatorList* plist)
{
	return plist->item_count;
}
//----------------------------------------------------------------------------------------------

struct ListItem* AllocatorList_Begin(const struct AllocatorList* plist)
{
	return plist->head;
}
//----------------------------------------------------------------------------------------------

const struct ListItem* AllocatorList_Begin_Const(const struct AllocatorList* plist)
{
	return plist->head;
}
//----------------------------------------------------------------------------------------------

struct ListItem* AllocatorList_End(const struct AllocatorList* plist)
{
	(void)plist;
	return A_LIST_BORDER_MARK;
}
//----------------------------------------------------------------------------------------------

const struct ListItem* AllocatorList_End_Const(const struct AllocatorList* plist)
{
	(void)plist;
	return A_LIST_BORDER_MARK;
}
//----------------------------------------------------------------------------------------------

struct ListItem* AllocatorList_Next(const struct AllocatorList* plist, const struct ListItem* pos)
{
#ifdef SDK_DEBUG
	SDK_ASSERT(plist == pos->plist);
#else
	(void)plist;
#endif
	return pos->next;
}
//----------------------------------------------------------------------------------------------

const struct ListItem* AllocatorList_Next_Const(const struct AllocatorList* plist, const struct ListItem* pos)
{
#ifdef SDK_DEBUG
	SDK_ASSERT(plist == pos->plist);
#else
	(void)plist;
#endif
	return pos->next;
}
//----------------------------------------------------------------------------------------------

struct ListItem* AllocatorList_Prev(const struct AllocatorList* plist, const struct ListItem* pos)
{
#ifdef SDK_DEBUG
	SDK_ASSERT(plist == pos->plist);
#else
	(void)plist;
#endif
	return pos->prev;
}
//----------------------------------------------------------------------------------------------

const struct ListItem* AllocatorList_Prev_Const(const struct AllocatorList* plist, const struct ListItem* pos)
{
#ifdef SDK_DEBUG
	SDK_ASSERT(plist == pos->plist);
#else
	(void)plist;
#endif
	return pos->prev;
}
//----------------------------------------------------------------------------------------------

struct ListItem* AllocatorList_Tail(const struct AllocatorList* plist)
{
	return plist->tail;
}
//----------------------------------------------------------------------------------------------

const struct ListItem* AllocatorList_Tail_Const(const struct AllocatorList* plist)
{
	return plist->tail;
}
//----------------------------------------------------------------------------------------------

const struct ListItem* AllocatorList_Advance(const struct ListItem* pos, s32 distance)
{
	if(distance < 0) 
	{
		while(distance++ && pos) pos = pos->prev;
	}
	else
	{
		while(distance-- && pos) pos = pos->next;
	}
	return pos;
}
//----------------------------------------------------------------------------------------------
