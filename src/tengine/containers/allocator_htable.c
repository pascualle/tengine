/*
tengine - cross-platform game engine
------------------------------------

Copyright (c) 2013 Papa Pascualle
Contact Email: pascualle@gmail.com

Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
*/

#include "allocator_htable.h"
#include "static_allocator.h"
#include "allocator_list.h"
#include "memory.h"

struct HTableItem
{
	struct HTableItem* next;
	void* value;
	u32 key;
};

#define HTABLE_HASH_PRIME 31
#define HTABLE_BORDER_MARK NULL

//----------------------------------------------------------------------------------------------

u32 AllocatorHTable_CalculateHeapSize(u32 max_items)
{
	return StaticAllocator_CalculateHeapSize(sizeof(struct AllocatorList*) * max_items) +
			StaticAllocator_CalculateHeapSize(StaticAllocator_CalculateHeapSize(sizeof(struct AllocatorList)) * max_items + 
			AllocatorList_CalculateHeapSize(sizeof(struct HTableItem), max_items));
}
//----------------------------------------------------------------------------------------------

void AllocatorHTable_Init(struct AllocatorHTable* phtable, const u32 max_items, struct StaticAllocator* allocator)
{
	u32 size;
	SDK_NULL_ASSERT(phtable);
	SDK_NULL_ASSERT(allocator);
	
	phtable->allocator = allocator;
	phtable->item_count = 0;
	phtable->max_items = max_items;
	phtable->head = HTABLE_BORDER_MARK;
	phtable->tail = HTABLE_BORDER_MARK;

	size = max_items * sizeof(struct AllocatorList*);
	phtable->htable = (struct AllocatorList**)StaticAllocator_Malloc(allocator, size);
	MI_CpuClear8(phtable->htable, size);
}
//----------------------------------------------------------------------------------------------

void AllocatorHTable_Release(struct AllocatorHTable* phtable)
{
	SDK_NULL_ASSERT(phtable);
	AllocatorHTable_Clear(phtable);
	if (phtable->htable != NULL)
	{
		StaticAllocator_Free(phtable->allocator, phtable->htable);
	}
	phtable->htable = NULL;
	phtable->max_items = 0;
}
//----------------------------------------------------------------------------------------------

void AllocatorHTable_Clear(struct AllocatorHTable* phtable)
{
	struct AllocatorList** it;
	SDK_NULL_ASSERT(phtable);
	it = phtable->htable + phtable->max_items - 1;
	while (it != phtable->htable)
	{
		if (*it != NULL)
		{
			AllocatorList_Clear(*it);
			StaticAllocator_Free(phtable->allocator, *it);
			*it = NULL;
		}
		it--;
	}
	phtable->item_count = 0;
	phtable->head = HTABLE_BORDER_MARK;
	phtable->tail = HTABLE_BORDER_MARK;
}
//----------------------------------------------------------------------------------------------

static u32 _AllocatorHTable_Hash(const struct AllocatorHTable* phtable, u32 key)
{
	char ch;
	u32 hashval = 0;
	ch = (key >> 24) & 0xff;
	hashval = ch + HTABLE_HASH_PRIME * hashval;
	ch = (key >> 16) & 0xff;
	hashval = ch + HTABLE_HASH_PRIME * hashval;
	ch = (key >> 8) & 0xff;
	hashval = ch + HTABLE_HASH_PRIME * hashval;
	ch = key & 0xff;
	hashval = ch + HTABLE_HASH_PRIME * hashval;
	return hashval % phtable->max_items;
}
//----------------------------------------------------------------------------------------------

static struct HTableItem* _AllocatorHTable_Find(const struct AllocatorHTable* phtable, struct AllocatorList** outlist, u32* outhashval, const u32 key)
{
	SDK_NULL_ASSERT(phtable);
	*outhashval = _AllocatorHTable_Hash(phtable, key);
	*outlist = phtable->htable[*outhashval /*& (phtable->max_items - 1)*/];
	if (*outlist != NULL)
	{
		const struct ListItem* it = AllocatorList_Begin_Const(*outlist);
		while (it != AllocatorList_End_Const(*outlist))
		{
			struct HTableItem* keyval_it = (struct HTableItem*)AllocatorList_Val(*outlist, it);
			if (keyval_it->key == key)
			{
				return keyval_it;
			}
			it = AllocatorList_Next(*outlist, it);
		}
	}
	return NULL;
}
//----------------------------------------------------------------------------------------------

const struct HTableItem* AllocatorHTable_Push(struct AllocatorHTable* phtable, const u32 key, void* value)
{
	u32 hashval;
	struct AllocatorList* plist;
	struct HTableItem* keyval_it;
	SDK_NULL_ASSERT(phtable);
	keyval_it = _AllocatorHTable_Find(phtable, &plist, &hashval, key);
	if (keyval_it != NULL)
	{
		SDK_NULL_ASSERT(keyval_it->key == key);
		keyval_it->value = value;
	}
	else
	{
		struct HTableItem keyval_new;
		if (plist == NULL)
		{
			SDK_NULL_ASSERT(phtable->allocator);
			plist = (struct AllocatorList*)StaticAllocator_Malloc(phtable->allocator, sizeof(struct AllocatorList));
			AllocatorList_Init(plist, sizeof(struct HTableItem), phtable->allocator);
			phtable->htable[hashval] = plist;
		}
		keyval_new.key = key;
		keyval_new.value = value;
		keyval_new.next = NULL;
		keyval_it = (struct HTableItem*)AllocatorList_Val(plist, AllocatorList_PushBack(plist, &keyval_new));
		
		if (phtable->tail == HTABLE_BORDER_MARK || phtable->head == HTABLE_BORDER_MARK)
		{
			keyval_it->next = HTABLE_BORDER_MARK;
			phtable->tail = keyval_it;
			phtable->head = keyval_it;
		}
		else
		{
			keyval_it->next = HTABLE_BORDER_MARK;
			phtable->tail->next = keyval_it;
			phtable->tail = keyval_it;
		}

		phtable->item_count++;
	}
	return keyval_it;
}
//----------------------------------------------------------------------------------------------

const struct HTableItem* AllocatorHTable_Find(const struct AllocatorHTable* phtable, const u32 key)
{
	u32 hashval;
	struct AllocatorList* plist;
	SDK_NULL_ASSERT(phtable);
	return _AllocatorHTable_Find(phtable, &plist, &hashval, key);
}
//----------------------------------------------------------------------------------------------

u32 AllocatorHTable_Size(const struct AllocatorHTable* phtable)
{
	return phtable->item_count;
}
//----------------------------------------------------------------------------------------------

void* AllocatorHTable_Val(const struct AllocatorHTable* phtable, const struct HTableItem* item)
{
	(void)phtable;
	SDK_NULL_ASSERT(phtable);
	if (item != NULL)
	{
		return item->value;
	}
	return NULL;
}
//----------------------------------------------------------------------------------------------

struct HTableItem* AllocatorHTable_Begin(const struct AllocatorHTable* phtable)
{
	SDK_NULL_ASSERT(phtable);
	return phtable->head;
}
//----------------------------------------------------------------------------------------------

const struct HTableItem* AllocatorHTable_Begin_Const(const struct AllocatorHTable* phtable)
{
	SDK_NULL_ASSERT(phtable);
	return phtable->head;
}
//----------------------------------------------------------------------------------------------

struct HTableItem* AllocatorHTable_End(const struct AllocatorHTable* phtable)
{
	(void)phtable;
	return HTABLE_BORDER_MARK;
}
//----------------------------------------------------------------------------------------------

const struct HTableItem* AllocatorHTable_End_Const(const struct AllocatorHTable* phtable)
{
	(void)phtable;
	return HTABLE_BORDER_MARK;
}
//----------------------------------------------------------------------------------------------

struct HTableItem* AllocatorHTable_Next(const struct AllocatorHTable* phtable, const struct HTableItem* pos)
{
	(void)phtable;
	return pos->next;
}
//----------------------------------------------------------------------------------------------

const struct HTableItem* AllocatorHTable_Next_Const(const struct AllocatorHTable* phtable, const struct HTableItem* pos)
{
	(void)phtable;
	return pos->next;
}
//----------------------------------------------------------------------------------------------
