/*
tengine - cross-platform game engine
------------------------------------

Copyright (c) 2013 Papa Pascualle
Contact Email: pascualle@gmail.com

Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
*/

#include "memory.h"
#include "static_allocator.h"
#include "allocator_array.h"

//----------------------------------------------------------------------------------------------

u32 AllocatorArray_CalculateHeapSize(u32 item_size, u32 max_items)
{
	max_items += ALLOCATOR_ARRAY_RESERVE_COUNT;
	return StaticAllocator_CalculateHeapSize(item_size * max_items) + // middle
		StaticAllocator_CalculateHeapSize(item_size * (max_items - ALLOCATOR_ARRAY_RESERVE_COUNT)) + // left realloc reserve
		StaticAllocator_CalculateHeapSize(item_size * (max_items - ALLOCATOR_ARRAY_RESERVE_COUNT)); // right realloc reserve
}
//----------------------------------------------------------------------------------------------

void AllocatorArray_Init(struct AllocatorArray* parray, u32 item_size, struct StaticAllocator *allocator)
{
	AllocatorArray_InitWithReserve(parray, ALLOCATOR_ARRAY_RESERVE_COUNT, item_size, allocator);
}
//----------------------------------------------------------------------------------------------

void AllocatorArray_InitWithReserve(struct AllocatorArray* parray, u32 reserve_count, u32 item_size, struct StaticAllocator *allocator)
{
	SDK_NULL_ASSERT(parray);
	SDK_NULL_ASSERT(allocator);
	SDK_ASSERT(item_size > 0);
	SDK_ASSERT(reserve_count > 0);
	parray->item_size = item_size;
	parray->allocated_count = reserve_count;
	parray->item_count = 0;
	parray->allocator = allocator;
	parray->ptr = (u8*)StaticAllocator_Malloc(allocator, parray->allocated_count * parray->item_size);
}
//----------------------------------------------------------------------------------------------

void AllocatorArray_Release(struct AllocatorArray* parray)
{
	SDK_NULL_ASSERT(parray);
	SDK_NULL_ASSERT(parray->ptr);
	SDK_NULL_ASSERT(parray->allocator);
	parray->item_size = 0;
	parray->item_count = 0;
	StaticAllocator_Free(parray->allocator, parray->ptr);
	parray->ptr = NULL;
	parray->allocator = NULL;
}
//----------------------------------------------------------------------------------------------

void AllocatorArray_Clear(struct AllocatorArray* parray)
{
	SDK_NULL_ASSERT(parray);
	SDK_NULL_ASSERT(parray->ptr);
	SDK_NULL_ASSERT(parray->allocator);
	StaticAllocator_Free(parray->allocator, parray->ptr);
	AllocatorArray_Init(parray, parray->item_size, parray->allocator);
}
//----------------------------------------------------------------------------------------------

void AllocatorArray_ClearFast(struct AllocatorArray* parray)
{
	parray->item_count = 0;
}
//----------------------------------------------------------------------------------------------

void AllocatorArray_PushBack(struct AllocatorArray* parray, void* data)
{
	SDK_NULL_ASSERT(parray);
	SDK_NULL_ASSERT(parray->ptr);
	SDK_NULL_ASSERT(parray->allocator);
	if(parray->allocated_count == parray->item_count)
	{
		u8* newPtr;
		parray->allocated_count = parray->item_count + ALLOCATOR_ARRAY_RESERVE_COUNT;
		newPtr = (u8*)StaticAllocator_Malloc(parray->allocator, parray->allocated_count * parray->item_size);
		MI_CpuCopy8(parray->ptr, newPtr, parray->item_count * parray->item_size);
		StaticAllocator_Free(parray->allocator, parray->ptr);
		parray->ptr = newPtr;
	}
	MI_CpuCopy8(data, parray->ptr + (parray->item_count * parray->item_size), parray->item_size);
	parray->item_count++;
}
//----------------------------------------------------------------------------------------------

void AllocatorArray_Insert(struct AllocatorArray* parray, u32 pos, void* data)
{
	u8* newPtr;
	u32 realPos = pos * parray->item_size;
	SDK_NULL_ASSERT(parray);
	SDK_NULL_ASSERT(parray->ptr);
	SDK_NULL_ASSERT(parray->allocator);
	if(pos >= parray->item_count)
	{
		AllocatorArray_PushBack(parray, data);
		return;
	}
	if(parray->allocated_count == parray->item_count)
	{
		parray->allocated_count = parray->item_count + ALLOCATOR_ARRAY_RESERVE_COUNT;
	}
	newPtr = (u8*)StaticAllocator_Malloc(parray->allocator, parray->allocated_count * parray->item_size);
	if(realPos > 0)
	{
		MI_CpuCopy8(parray->ptr, newPtr, realPos);
	}
	MI_CpuCopy8(data, newPtr + realPos, parray->item_size);
	MI_CpuCopy8(parray->ptr + realPos, newPtr + realPos + parray->item_size, parray->item_count - pos * parray->item_size);
	parray->item_count++;
	StaticAllocator_Free(parray->allocator, parray->ptr);
	parray->ptr = newPtr;
}
//----------------------------------------------------------------------------------------------

void AllocatorArray_Erase(struct AllocatorArray* parray, u32 pos)
{
	SDK_NULL_ASSERT(parray);
	SDK_NULL_ASSERT(parray->ptr);
	SDK_NULL_ASSERT(parray->allocator);
	if(pos >= parray->item_count)
	{
		SDK_ASSERT(0); //Invalid position
		return;
	}
	if(pos < parray->item_count)
	{
		u32 i;
		for(i = pos; i < parray->item_count; i++)
		{
			u8* to = i * parray->item_size + parray->ptr;
			u8* from = to + parray->item_size;
			MI_CpuCopy8(from, to, parray->item_size);
		}
	}
	parray->item_count--;
}
//----------------------------------------------------------------------------------------------

void* AllocatorArray_At(const struct AllocatorArray* parray, u32 pos)
{
	SDK_NULL_ASSERT(parray);
	SDK_NULL_ASSERT(parray->ptr);
	SDK_NULL_ASSERT(parray->allocator);
	if(pos >= parray->item_count)
	{
		SDK_ASSERT(0); //Invalid position
		return NULL;
	}
	return (void*)(parray->ptr + (pos * parray->item_size));
}
//----------------------------------------------------------------------------------------------

u32 AllocatorArray_Size(const struct AllocatorArray* parray)
{
	SDK_NULL_ASSERT(parray);
	SDK_NULL_ASSERT(parray->ptr);
	SDK_NULL_ASSERT(parray->allocator);
	return parray->item_count;
}
//----------------------------------------------------------------------------------------------



