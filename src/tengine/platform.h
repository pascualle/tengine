/*
tengine - cross-platform game engine
------------------------------------

Copyright (c) 2013 Papa Pascualle
Contact Email: pascualle@gmail.com

Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
*/

#ifndef PLATFORM_H_INCLUDED
#define PLATFORM_H_INCLUDED

#define TENGINE 1

#ifndef NITRO_SDK
#ifdef ANDROID_NDK
 #include "stdio.h"
#ifdef SDK_DEBUG
 #include "assert.h"
#endif
 #include "unistd.h"
 #include "android/log.h"
#endif

#ifdef WINDOWS_APP
 #ifdef _MSC_VER
   #define _ALLOW_KEYWORD_MACROS
   #define inline __inline
   #define _CRT_SECURE_NO_WARNINGS
 #endif
 #ifdef __BORLANDC__
    #pragma warn -8058
    #pragma warn -8019
 #endif
 #include "tchar.h"
 #include "windows.h"
 #include "stdio.h"
#ifdef SDK_DEBUG
 #include "assert.h"
#endif
#endif

#if defined NIX_APP || defined EMSCRIPTEN_APP || defined KOLIBRIOS_APP
 #include "stdio.h"
#ifdef SDK_DEBUG
 #include "assert.h"
#endif
 #include "unistd.h"
#endif

#ifdef IOS_APP
 #include "stdio.h"
#ifdef SDK_DEBUG
 #include "assert.h"
#endif
 #include "unistd.h"
#endif
#else
 #include <nitro.h>
 #include <nnsys.h>
#endif

#include "wchar.h"

#ifdef __cplusplus
extern "C" {
#endif

#if defined NITRO_SDK && defined USE_FX32_AS_FLOAT
#undef USE_FX32_AS_FLOAT
#if !defined USE_FX32_AS_FIXED
#define USE_FX32_AS_FIXED
#endif
#endif

#if defined USE_FX32_AS_FLOAT || defined USE_FX32_AS_FIXED || defined NITRO_SDK
#else
	#define USE_FX32_AS_FLOAT
	/* #define USE_FX32_AS_FIXED */
#endif

typedef wchar_t wchar;
#ifndef NITRO_SDK
/*
	ANDROID_NDK by default char is unsigned
	to avoid compatibility issues s8 type is removed
	typedef char s8;
*/
typedef unsigned char u8;
typedef unsigned short u16;
typedef short s16;
typedef unsigned int u32;
typedef signed long long int s64;
typedef unsigned long long int u64;
typedef int s32;
#if defined USE_FX32_AS_FLOAT
typedef double fx32;
#else
typedef int fx32;
#endif
typedef float f32;
typedef int BOOL;
#define FALSE 0
#define TRUE 1

#ifdef SDK_DEBUG
#define SDK_ASSERT assert
#define SDK_NULL_ASSERT assert
#else
#define SDK_ASSERT(...) ((void)0)
#define SDK_NULL_ASSERT(...) ((void)0)
#endif

#if defined USE_FX32_AS_FLOAT

#define FX32_ONE (1.0)
#define FX_FX32_TO_F32(x) ((f32)(x))
#define FX_F32_TO_FX32(x) ((fx32)(x))

#else

#define FX32_SHIFT 16
#define FX32_ONE (1 << FX32_SHIFT)
#define FX_FX32_TO_F32(x) ((f32)((x) / (f32)(1 << FX32_SHIFT)))
#define FX_F32_TO_FX32(x) ((fx32)(((x) > 0) ? \
							((x) * (1 << FX32_SHIFT) + 0.5f ) : \
							((x) * (1 << FX32_SHIFT) - 0.5f )))

#endif

 #ifdef WINDOWS_APP
  #ifdef SDK_DEBUG
    #define OS_Printf _tprintf
    #define OS_Warning _tprintf
  #else
    #define OS_Printf(...) ((void)0)
    #define OS_Warning(...) ((void)0)
  #endif
  #define OS_SpleepMs(timeInMs) Sleep(timeInMs)   
 #else
  #if defined NIX_APP || defined EMSCRIPTEN_APP || defined KOLIBRIOS_APP
   #ifndef KOLIBRIOS_APP
     #define Sleep(ms) usleep(ms * 1000)
   #else
     #define Sleep(ms) {asm volatile ("int $0x40"::"a"(5), "b"(ms * 10));}
   #endif
   #ifdef SDK_DEBUG
     #define OS_Printf printf
     #define OS_Warning printf
   #else
     #define OS_Printf(...) ((void)0)
     #define OS_Warning(...) ((void)0)
   #endif
   #define OS_SpleepMs(timeInMs) usleep(timeInMs*1000)
   #ifndef NULL
      #define NULL (0)
   #endif
  #else
   #ifdef IOS_APP
    #define Sleep(ms) usleep(ms*1000)
    #ifdef SDK_DEBUG
      #define OS_Printf printf
      #define OS_Warning printf
    #else
      #define OS_Printf(...) ((void)0)
      #define OS_Warning(...) ((void)0)
    #endif
    #define OS_SpleepMs(timeInMs) usleep(timeInMs*1000)
   #else
    #define NDK_NATIVE_API10
    #define Sleep(ms) usleep(ms*1000)
    #ifdef SDK_DEBUG
      #define OS_Printf(...) __android_log_print(ANDROID_LOG_INFO, "TengineAppInfo", __VA_ARGS__)
      #define OS_Warning(...) __android_log_print(ANDROID_LOG_WARN, "TengineAppWarning", __VA_ARGS__)
    #else
      #define OS_Printf(...) ((void)0)
      #define OS_Warning(...) ((void)0)
    #endif
    #define OS_SpleepMs(timeInMs) usleep(timeInMs*1000)  
   #endif
  #endif
 #endif

 typedef u16 GXRgba;
 typedef u16 GXRgb;

#if defined KOLIBRIOS_APP
 #define GX_RGBA_B_SHIFT           (0)
 #define GX_RGBA_B_MASK            (0x001f)
 #define GX_RGBA_G_SHIFT           (5)
 #define GX_RGBA_G_MASK            (0x03e0)
 #define GX_RGBA_R_SHIFT           (10)
 #define GX_RGBA_R_MASK            (0x7c00)
 #define GX_RGBA_A_SHIFT           (15)
 #define GX_RGBA_A_MASK            (0x8000)
#else
 #define GX_RGBA_B_SHIFT           (1)
 #define GX_RGBA_B_MASK            (0x003e)
 #define GX_RGBA_G_SHIFT           (6)
 #define GX_RGBA_G_MASK            (0x07c0)
 #define GX_RGBA_R_SHIFT           (11)
 #define GX_RGBA_R_MASK            (0xf800)
 #define GX_RGBA_A_SHIFT           (0)
 #define GX_RGBA_A_MASK            (0x0001)
#endif
 #define GX_RGBA(r, g, b, a)       (GXRgba)(((r) << GX_RGBA_R_SHIFT) | \
											((g) << GX_RGBA_G_SHIFT) | \
											((b) << GX_RGBA_B_SHIFT) | \
											((a) << GX_RGBA_A_SHIFT))
 typedef u32 GXRgba32;

 /*stubs*/
 #define GX_VRAM_BG_256_AB 0
 #define GX_VRAM_SUB_BG_128_C 0
 #define GX_DISP_SELECT_SUB_MAIN 0
 #define GX_BG_BMPSCRBASE_0x00000 0
 #define GX_PLANEMASK_BG3 0
 #define GX_PLANEMASK_BG1 0
 #define GX_BG_SCRSIZE_DCBMP_256x256 0
 #define GX_BG_AREAOVER_XLU 0

 #define GX_SetBankForBG(v)
 #define GX_SetBankForSubBG(v)
 #define GX_SetDispSelect(v)
 #define GXS_SetVisiblePlane(v)
 #define GX_SetVisiblePlane(v)
 #define G2_SetBG2ControlDCBmp(v)
 #define G2_SetBG2Priority(v)

 #define OS_ResetSystem(v)

 typedef unsigned int GXBGBmpScrBase;
 typedef void NNSSndHandle;
 typedef void NNSSndStrmHandle;

#endif

#define GX_RGB_CHANNEL_VALUE_MAX 0x1f
#define COLOR888TO1555(r,g,b) (GX_RGBA((r * 0x1f) >> 8, (g * 0x1f) >> 8, (b * 0x1f) >> 8, 1))
#define COLOR_TO_1555(c) (GX_RGBA(((((c >> 16) & 0xff) * 0x1f) / 255), ((((c >> 8) & 0xff) * 0x1f) / 255), (((c & 0xff) * 0x1f) / 255), 1))

#if defined USE_FX32_AS_FLOAT
 #define FX32(v) ((fx32)(v))
#else
 #define FX32(v) ((fx32)((v) * FX32_ONE))
#endif

#define W_T(t) L##t

//----------------------------------------------------------------------------------------------

#ifdef __cplusplus
}
#endif

#endif /* PLATFORM_H_INCLUDED*/

