/*
tengine - cross-platform game engine
------------------------------------

Copyright (c) 2013 Papa Pascualle
Contact Email: pascualle@gmail.com

Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
*/

#ifndef TENGINE_CONST_H
#define TENGINE_CONST_H
//---------------------------------------------------------------------------

#ifdef __cplusplus
extern "C" {
#endif

typedef enum LoadMode
{
					LOAD_SIZES      = 0,
					LOAD_ALL        = 3
}LoadMode;

typedef enum TextBoxType
{
					TEXTBOX_TYPE_ERROR = 0,
					TEXTBOX_TYPE_STATIC = 1,
					TEXTBOX_TYPE_DYNAMIC = 2
}TextBoxType;

typedef enum LineParameter
{
					LINE_X0 = 0,
					LINE_Y0 = 1,
					LINE_X1 = 2,
					LINE_Y1 = 3
}LineParameter;

enum
{
					SPT_ONCOLLIDE = 0,
					SPT_ONTILECOLLIDE = 1,
					SPT_ONCHANGENODE = 2,
					SPT_ONCHANGE1 = 3,
					SPT_ONCHANGE2 = 4,
					SPT_ONCHANGE3 = 5,
					SPT_ONENDANIM = 6,
					SPT_COUNT = 7
};

typedef enum StateBlendMode
{
					SBM_NONE = 0,
					SBM_FROM_BEGIN_ANIMATION = 1,
					SBM_FROM_END_ANIMATION = 2,
					SBM_RANDOM = 3
}StateBlendMode;

enum
{
					TYPE            = 0,
                    POSITION        = 1,
                    MAP_TYPE_COUNT  = 2,

                    NONE_MAP_IDX  = 65535,
                    NONE_BGTYPE   = -125,

                    FX               = 0,
                    FY               = 1,

					ALIGN_UP         = 0,
					ALIGN_LEFT       = 1,
					ALIGN_CENTER     = 2,
					ALIGN_RIGHT      = 3,
					ALIGN_DOWN       = 4,

					CHECK_UP        = 1,
					CHECK_DOWN      = -1,
					CHECK_ON        = 0
};

//---------------------------------------------------------------------------

#ifdef __cplusplus
} /* extern "C" */
#endif

#endif /*TENGINE_CONST_H*/
