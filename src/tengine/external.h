/*
tengine - cross-platform game engine
------------------------------------

Copyright (c) 2013 Papa Pascualle
Contact Email: pascualle@gmail.com

Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
*/

#ifndef EXTERNAL_API_H
#define EXTERNAL_API_H

#ifdef USE_EXTERNAL_API

#include "system.h"

#ifdef __cplusplus
extern "C" {
#endif

struct TERFont;

struct ExDRect
{
	float x, y, w, h;
};

/* 
	Add TRGLColorVertexData (pos and color data) to active layer render list 
	Note: in case of openGL render it uses glDrawArrays(GL_TRIANGLES, 0, size) function
*/
void ExD_AddRenderExternalPosColorData(const struct TRGLColorVertexData* data);

/* 
	Set active clip rect (glScissor) for further mesh TRGLColorVertexData data
	if data is NULL - cancel clip rect
*/
void ExD_SetActiveClipRect(const struct ExDRect* data);

/* Get specified font  */
const struct TERFont* ExD_GetFont(u32 font_id);

/* Calculate text width */
u32 ExD_GetTextWidth(u32 font_id, const wchar *ptxt, u32 text_length);

struct ExD_DrawTextData
{
	u32 font_id;
	const wchar* ptxt;
	u32 text_length;
	GXRgba txt_color;
	GXRgba bg_color;
	BOOL pixel_accurate;
	fx32 scale;
	struct ExDRect txt_rect;
	const struct ExDRect* clip_rect;
};

/* Draw text to active layer */
void ExD_DrawText(const struct ExD_DrawTextData *data);

struct ExD_DrawImageData
{
	s32 obj_type;
	s32 anim_id;
	s32 frame_num;
	u8 alpha;
	GXRgba blend_color;
	struct ExDRect rect;
};

/* Draw text to active layer */
void ExD_DrawImage(const struct ExD_DrawImageData* data);

#ifdef __cplusplus
} /* extern "C" */
#endif

#endif
#endif // EXTERNAL_API_H
