/*
tengine - cross-platform game engine
------------------------------------

Copyright (c) 2013 Papa Pascualle
Contact Email: pascualle@gmail.com

Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
*/

#ifndef USERCONFIG_H
#define USERCONFIG_H

#include "platform.h"

#ifdef __cplusplus
extern "C" {
#endif

enum UserConfigResult
{
	UCR_CONFIG_NOT_EXIST,
	UCR_SUCCESS,
	UCR_WRITE_ERROR,
	UCR_READ_ERROR,
	UCR_WRONG_FILE_STRUCTURE,
	UCR_WRONG_DATA_SIZE,
	UCR_INIT_ERROR
};

s32 InitUserConfig(const char* appName);
s32 SaveUserConfig(const void* pdata, u32 size);
s32 LoadUserConfig(void* pdata, u32 size);

//----------------------------------------------------------------------------------------------

#ifdef __cplusplus
} /* extern "C" */
#endif

#endif
