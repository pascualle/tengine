/*
tengine - cross-platform game engine
------------------------------------

Copyright (c) 2013 Papa Pascualle
Contact Email: pascualle@gmail.com

Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
*/

#ifndef TENGINE_H
#define TENGINE_H

#include "system.h"
#include "memory.h"
#include "fxmath.h"
#include "tengine_const.h"

#ifdef __cplusplus
extern "C" {
#endif

//---------------------------------------------------------------------------

struct StaticAllocator;

struct InitEngineParameters
{
	u32 layersCount;
	u32 particlesPoolSize;
};

void initEngineParametersWithDefaultValues(struct InitEngineParameters *const params);

void initEngine(const struct InitEngineParameters *params);

void releaseEngine(void);

void initRenderPlaneParametersWithDefaultValues(struct RenderPlaneInitParams *const params);

void initRenderPlane(const struct RenderPlaneInitParams* p);

BOOL isRenderPlaneInit(u32 layer);

void resizeRenderPlane(u32 layer, const struct RenderPlaneSizeParams* p);

void releaseRenderPlane(enum BGSelect iType);

BOOL assignLayerWithRenderPlane(u32 layer, enum BGSelect iBGType, BOOL primaryLayer);

void setRenderPlaneScale(fx32 val, u32 layer);

fx32 getRenderPlaneScale(u32 layer);

void setBackgroundTileLayersForDS(u32 layer, enum BGSelect iBGType, s32 iStartIdx, s32 iEndIdx);

void setVisible(u32 layer, BOOL val);

BOOL getVisible(u32 layer);

fx32 getCurrentDeltaTimeScale(void);

//---------------------------------------------------------------------------

void drawWithoutInternalLogicUpdateMode(void);

void beginLoadListAsynh(s32 cb_parameter);

void addToLoadListCommonData(void);

void addToLoadListMap(u32 layer, u16 level);

void addToLoadListLanguageData(u32 language_id);

void endLoadListAsynh(BOOL loadGraphicResources);

//---------------------------------------------------------------------------

void releaseTextData(void);

void releaseMapData(u32 layer);

void releaseCommonData(void);

void releaseResources(void);

//---------------------------------------------------------------------------

u16* createCopyTileMap(u32 layer, struct StaticAllocator* const allocator);

void setTileMap(u32 layer, u16 *ipTileMap, struct StaticAllocator* const allocator);

void setDefaultTileMap(u32 layer);

#ifdef USE_TILEMAP_ATTRIBUTES

u32* createCopyTileMapColorAttributes(u32 layer, struct StaticAllocator* const allocator);

void setTileMapColorAttributes(u32 layer, u32* ipTileMapCA, struct StaticAllocator* const allocator);

void setDefaultTileMapColorAttributes(u32 layer);

#endif

//---------------------------------------------------------------------------

s32 getCurrentLanguageId(void);

BOOL prIsTextBox(s32 pr_id);
BOOL prIsTextBoxLayer(s32 pr_id, u32 layer);

s32 getStaticTextLinesCount(s32 text_id);

const wchar* getStaticText(s32 text_id, s32 line);

TextBoxType txbGetType(s32 pr_id);
TextBoxType txbGetTypeLayer(s32 pr_id, u32 layer);

s32 txbGetDynamicTextLinesCount(s32 pr_id);
s32 txbGetDynamicTextLinesCountLayer(s32 pr_id, u32 layer);

s32 txbGetDynamicTextMaximumLineCharsCount(s32 pr_id);
s32 txbGetDynamicTextMaximumLineCharsCountLayer(s32 pr_id, u32 layer);

s32 txbGetWidth(s32 pr_id);
s32 txbGetWidthLayer(s32 pr_id, u32 layer);

s32 txbGetHeight(s32 pr_id);
s32 txbGetHeightLayer(s32 pr_id, u32 layer);

BOOL txbSetDynamicText(s32 pr_id, const wchar *text);
BOOL txbSetDynamicTextLayer(s32 pr_id, const wchar *text, u32 layer);

const wchar* txbGetDynamicText(s32 pr_id, s32 line);
const wchar* txbGetDynamicTextLayer(s32 pr_id, s32 line, u32 layer);

BOOL txbSetDynamicTextLine(s32 pr_id, s32 line_idx, const wchar *text);
BOOL txbSetDynamicTextLineLayer(s32 pr_id, s32 line_idx, const wchar *text, u32 layer);

void txbSetTextPerPixelAccuracy(s32 pr_id, BOOL val, u32 layer);
//---------------------------------------------------------------------------

u32 getActiveLayer(void);

void checkToMove(s32 *mp, struct fxVec2 pos, s32 checkDirection);

BOOL prChangeType(s32 pr_id, s32 group_id);
BOOL prChangeTypeLayer(s32 pr_id, s32 group_id, u32 layer);

void prSetState(s32 pr_id, s32 state, enum StateBlendMode mode);
void prSetStateLayer(s32 pr_id, s32 state, enum StateBlendMode mode, u32 layer);

s32 prGetState(s32 pr_id);
s32 prGetStateLayer(s32 pr_id, u32 layer);

void prSetProperty(s32 pr_id, s32 prp_id, fx32 val);
void prSetPropertyLayer(s32 pr_id, s32 prp_id, fx32 val, u32 layer);

fx32 prGetProperty(s32 pr_id, s32 prp_id);
fx32 prGetPropertyLayer(s32 pr_id, s32 prp_id, u32 layer);

s32 prGetGroupIdx(s32 pr_id);
s32 prGetGroupIdxLayer(s32 pr_id, u32 layer);

s32 prGetJoinNodeCount(void);

BOOL prGetJoinNodePosition(s32 pr_id, u8 idx, struct fxVec2* const pos);
BOOL prGetJoinNodePositionLayer(s32 pr_id, u8 idx, struct fxVec2* const pos, u32 layer);

// if idx < 0 -- break joint
BOOL prSetJoinTo(s32 pr_id, s32 pr_id_to, s32 idx);
BOOL prSetJoinToLayer(s32 pr_id, s32 pr_id_to, s32 idx, u32 layer);

s32 prGetJoinedTo(s32 pr_id);
s32 prGetJoinedToLayer(s32 pr_id, u32 layer);

s32 prGetJoinedChild(s32 pr_id, u8 idx);
s32 prGetJoinedChildLayer(s32 pr_id, u8 idx, u32 layer);

s32 prGetTargetPathNode(s32 pr_id);

void prTeleportToPathNode(s32 pr_id, s32 pathnode_id);
void prTeleportToPathNodeLayer(s32 pr_id, s32 pathnode_id, u32 layer);

void prSetOppositeState(s32 pr_id, enum StateBlendMode mode);

char prGetCurrentDirection(s32 pr_id);

void prGetCollideRect(s32 pr_id, fx32 rect[RECT_SIZE]);
void prGetCollideRectLayer(s32 pr_id, fx32 rect[RECT_SIZE], u32 layer);

void prGetViewRect(s32 pr_id, fx32 rect[RECT_SIZE]);
void prGetViewRectLayer(s32 pr_id, fx32 rect[RECT_SIZE], u32 layer);

#ifdef TENGINE_LEGACY_CODE
void prRunAssignedScript(s32 pr_id, u16 pr_spt_id);
#endif

void prTeleportToTileNo(s32 pr_id, s32 tileNo, char align);

void prSetPosition(s32 pr_id, struct fxVec2 pos);
void prSetPositionLayer(s32 pr_id, struct fxVec2 pos, u32 layer);

void prAddXYToPosition(s32 pr_id, fx32 xv, fx32 yv);

struct fxVec2 prGetPosition(s32 pr_id);
struct fxVec2 prGetPositionLayer(s32 pr_id, u32 layer);

BOOL prHasAlphaCollideMap(s32 pr_id);

BOOL prGetAlphaCollideMapValue(s32 pr_id, struct fxVec2 pos);

u16 prGetAnimationId(s32 pr_id);

void prSetCustomRotation(s32 pr_id, fx32 deg_angle);
void prSetCustomRotationLayer(s32 pr_id, fx32 deg_angle, u32 layer);

fx32 prGetCustomRotation(s32 pr_id);
fx32 prGetCustomRotationLayer(s32 pr_id, u32 layer);

void prResetRotationToDefault(s32 pr_id);
void prResetRotationToDefaultLayer(s32 pr_id, u32 layer);

void prSetCustomScale(s32 pr_id, struct fxVec2 scaleRatio);
void prSetCustomScaleLayer(s32 pr_id, struct fxVec2 scaleRatio, u32 layer);

struct fxVec2 prGetCustomScale(s32 pr_id);
struct fxVec2 prGetCustomScaleLayer(s32 pr_id, u32 layer);

void prResetScaleToDefault(s32 pr_id);
void prResetScaleToDefaultLayer(s32 pr_id, u32 layer);

void prSetCustomAlpha(s32 pr_id, u8 alpha);
void prSetCustomAlphaLayer(s32 pr_id, u8 alpha, u32 layer);

u8 prGetCustomAlpha(s32 pr_id);
u8 prGetCustomAlphaLayer(s32 pr_id, u32 layer);

void prResetAlphaToDefault(s32 pr_id);
void prResetAlphaToDefaultLayer(s32 pr_id, u32 layer);

void prBlendWithColor(s32 pr_id, GXRgba color);
void prBlendWithColorLayer(s32 pr_id, GXRgba color, u32 layer);

struct GameObjectAnimationFrameInfo
{
	s32 index;
	s32 time_remain;
	s32 animation_id;
	fx32 left;
	fx32 top;
	fx32 width;
	fx32 height;
};

BOOL prGetCurrentAnimationFrameInfo(s32 pr_id, struct GameObjectAnimationFrameInfo *const goafi);

//---------------------------------------------------------------------------

s32 cldShapesCount(u32 layer);

s32 cldShapeNodesCount(u32 layer, s32 cldShapeIdx);

const struct fxVec2* cldShapeGetNodesArray(u32 layer, s32 cldShapeIdx);

//---------------------------------------------------------------------------

void setPathNodePosition(s32 pn_id, struct fxVec2 pos);

struct fxVec2 getPathNodePosition(s32 pn_id);

// start and end coordinates must be static during using this function
// (if start or end has changes, pos must be equal to start)
BOOL nextPointAtLine(struct fxVec2 start, struct fxVec2 end, fx32 speed, struct fxVec2 *pos);

//---------------------------------------------------------------------------

s32 getHeightBGTile(void);

s32 getWidthBGTile(void);

s32 getViewWidth(u32 layer);

s32 getViewHeight(u32 layer);

s32 getViewLeft(u32 layer);

s32 getViewTop(u32 layer);

s32 getMapWidth(void);

s32 getMapHeight(void);

fx32 getViewOffsetX(u32 layer);

fx32 getViewOffsetY(u32 layer);

struct fxVec2 getCamera(void);
struct fxVec2 getCameraLayer(u32 layer);

void setCamera(struct fxVec2 pos);
void setCameraLayer(struct fxVec2 pos, u32 layer);

struct fxVec2 getCameraMinPosition(void);
struct fxVec2 getCameraMinPositionLayer(u32 layer);

struct fxVec2 getCameraMaxPosition(void);
struct fxVec2 getCameraMaxPositionLayer(u32 layer);

void refreshAllScreen(void);

s32 getFPS(void);

void setPauseEngine(BOOL pause);

//---------------------------------------------------------------------------
//All primitives are used world coordinates (use getViewOffsetX() and getViewOffsetY() to determine the current coordinates of the camera)

void setColor(GXRgba color);

void fillRect(fx32 x, fx32 y, fx32 w, fx32 h);

//#ifdef USE_CUSTOM_RENDER
//void clearRect(fx32 x, fx32 y, fx32 w, fx32 h);
//#endif

void drawLine(fx32 x0, fx32 y0, fx32 x1, fx32 y1);

// alpha 0...31, NULL -- use internal settings
void drawSingleFrame(s32 group_id, s32 anim_id, s32 anim_frame, fx32 left, fx32 top, u8* alpha, 
						fx32* rotationAngle, fx32* scaleX, fx32* scaleY, fx32 affineOriginX, fx32 affineOriginY);

//---------------------------------------------------------------------------

s32 getZonesCount(u32 layer);

void setCurrentZone(u32 layer, s32 zone);

s32 getCurrentZone(u32 layer);

//---------------------------------------------------------------------------

u16 rlGetCount(u32 layer);

BOOL rlIsAvailable(void);

BOOL rlMove(u32 layer, u32 zone, u16 index_from, u16 index_to);

BOOL rlGetGameObjectIndex(s32 pr_id, u32 layer, u32 zone, u16 *const orenderlist_index, u16 *const oindex);

BOOL rlMoveGameObject(u32 layer, u32 zone, u16 renderlist_index, u16 index_from, u16 index_to);

u16 rlGetGameObjectsCount(u32 layer, u32 zone, u16 renderlist_index);

//---------------------------------------------------------------------------

fx32 getYBGTileAt(fx32 y, char align);

fx32 getXBGTileAt(fx32 x, char align);

//---------------------------------------------------------------------------
#ifdef TENGINE_LEGACY_CODE
void startScript(s32 script_id, BOOL force, s32 owner, s32 second);

void onCollide(s32 pr, s32 opr);
#endif
//---------------------------------------------------------------------------

void resetParticles(void);

s32 getParticlesCount(void);

void createParticles(s32 pr_idx,
					  u32 colCount,
					  u32 rowCount,
					  fx32 minSpeed,
					  fx32 varSpeed,
					  u32 minLifeTime,
					  u32 varLifeTime,
					  u32 exploudTime,
					  fx32 *targetX,
					  fx32 *targetY);

void createExplode(fx32 x,
					fx32 y,
					u32 radius,
					u32 count,
					u32 time,
					s32 pr_idx,
					s32 state);
                    
void createSimpleParticles(u32 size,
							GXRgba color,
							fx32 x,
							fx32 y,
							u32 radius,
							u32 count,
							fx32 minSpeed,
							fx32 varSpeed,
							u32 minLifeTime,
							u32 varLifeTime,
							u32 exploudTime,
							fx32 *targetX,
                            fx32 *targetY);
                            
//---------------------------------------------------------------------------
//callbacks

struct SoundHandle;

struct ScriptCallbackData
{
	s32 scriptId;
	s32 initiatorId;
	s32 gotoLevel;
	s32 textMessageId;
};

enum EventType
{
	EVENT_TYPE_ON_ANIMATION = 0,
    EVENT_TYPE_ON_DIRECTOR = 1,
	EVENT_TYPE_ON_END_ASYNH_LOAD_DATA = 2,
    EVENT_TYPE_ON_END_ASYNH_LOAD_RESOURSES = 3,
    EVENT_TYPE_ON_SOUND = 4
};

enum EvendIdLoadDataType
{
	LOAD_TYPE_COMMONDATA = 1,
	LOAD_TYPE_MAPDATA = 2,
	LOAD_TYPE_TEXTDATA = 3,
	LOAD_TYPE_ENDLOADDATATASK = 4
};

enum EvendIdSoundType
{
	SOUND_TYPE_PLAY = 1,
	SOUND_TYPE_PAUSE = 2,
	SOUND_TYPE_STOP = 3
};

struct EventCallbackData
{
	enum EventType eventType;
	s32 layer;
	s32 eventId;
	s32 ownerId;
	s32 initiatorId;
	union
	{
		const struct SoundHandle *mpSoundHandle;
		const void *mpData;
	}eventData;
};

typedef void (*OnEventCallback)(const struct EventCallbackData *pData);
typedef void (*OnScriptCallback)(const struct ScriptCallbackData *pData);
typedef void (*OnChangeHeroCallback)(s32 iNewId, s32 iInitiatorId);

typedef void(*OnBeginUpdate)(u32 layer);
typedef void(*OnDrawBackgroundTiles)(u32 layer);
typedef void (*OnDrawObject)(u32 layer, s32 iId, BOOL* const oDraw);
typedef void (*OnFinishUpdate)(u32 layer);

typedef void (*CallForEachGameObjectFn)(u32 layer, s32 iIdInitiator, s32 iId);
typedef void (*OnSkipStreamFrameCallback)(u32 layer, s32 iId, BOOL* const skipFrame);

void setOnEvent(OnEventCallback pCallbackFn);

void setOnBeginUpdate(OnBeginUpdate pCallbackFn);
void setOnDrawBackgroundTiles(OnDrawBackgroundTiles pCallbackFn);
void setOnDrawDecorationObject(OnDrawObject pCallbackFn);
void setOnDrawGameObject(OnDrawObject pCallbackFn);
void setOnFinishUpdate(OnFinishUpdate pCallbackFn);

BOOL callForEachGameObject(s32 iId, CallForEachGameObjectFn pCallbackFn);
void setOnSkipStreamFrame(OnSkipStreamFrameCallback pCallbackFn);

void setOnScript(u32 layer, OnScriptCallback pCallbackFn);
void setOnChangeHero(u32 layer, OnChangeHeroCallback pCallbackFn);

//---------------------------------------------------------------------------

#ifdef __cplusplus
} /* extern "C" */
#endif

#endif /*TENGINE_H*/
