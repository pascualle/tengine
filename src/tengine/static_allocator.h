/*
tengine - cross-platform game engine
------------------------------------

Copyright (c) 2013 Papa Pascualle
Contact Email: pascualle@gmail.com

Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
*/

#ifndef STATIC_HEAP_ALLOCATOR_H
#define STATIC_HEAP_ALLOCATOR_H

#include "platform.h"

#ifdef __cplusplus
extern "C" {
#endif

struct StaticAllocator
{ 
	BOOL inited; 
	u32 last_pos;
	u32 head_pos;
	u32 max_size;
	u32 alloc_count;
	u32 alloc_size;
	u8* internal_buf;
};

u32 StaticAllocator_CalculateHeapSize(u32 size);

void StaticAllocator_Init(struct StaticAllocator* allocator, const void* static_array, u32 array_size);

void StaticAllocator_Release(struct StaticAllocator* allocator);

void StaticAllocator_Reset(struct StaticAllocator* allocator);

void* StaticAllocator_Malloc(struct StaticAllocator* allocator, u32 size);

void StaticAllocator_Free(struct StaticAllocator* allocator, void* ptr);

u32 StaticAllocator_Check(struct StaticAllocator* allocator, void* ptr);

#ifdef __cplusplus
} /* extern "C" */
#endif

#endif