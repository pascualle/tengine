/*
tengine - cross-platform game engine
------------------------------------

Copyright (c) 2013 Papa Pascualle
Contact Email: pascualle@gmail.com

Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
*/

#ifndef NETWORK_H
#define NETWORK_H

#include "platform.h"

#ifndef NITRO_SDK

#ifdef __cplusplus
extern "C" {
#endif

#ifndef INADDR_LOOPBACK
#define INADDR_LOOPBACK		0x7f000001
#endif

#if defined ANDROID_NDK
    #include <sys/endian.h>
#endif

//---------------------------------------------------------------------------

#ifndef MAX_NETWORK_CLIENTS
#define MAX_NETWORK_CLIENTS 20
#endif

#ifndef MAX_DATABUF_SIZE
#define MAX_DATABUF_SIZE 100000
#endif

#ifdef WINDOWS_APP
    #include <WinSock.h>
	#include <fcntl.h>
	#ifndef MSG_WAITALL
	 #define MSG_WAITALL 0x08
	#endif
	#define NET_ERR GetLastError()
	#define CERBER_PRINTF(a) OS_Printf("%s : %ld \n", a, GetLastError ())
	typedef int socklen_t;
	#ifndef __BORLANDC__
	 typedef long ssize_t;
	#endif
	#define SOCK_T SOCKET
	#define ADDR_T struct sockaddr
	#define ADDRIN_T struct sockaddr_in
	#define CLOSESOCK(s) closesocket(s)
	#ifndef EWOULDBLOCK
	 #define EWOULDBLOCK WSAEWOULDBLOCK
	#endif
	#define USE_NETWORK
#endif

#if defined NIX_APP || defined ANDROID_NDK || defined IOS_APP || defined EMSCRIPTEN_APP
    #include <sys/socket.h>
    #include <sys/types.h>
    #include <arpa/inet.h>
    #include <netinet/in.h>
    #include <unistd.h>
	#include <errno.h>
	#define NET_ERR errno
    #define CERBER_PRINTF(a) OS_Printf("%s ; error -  %d \n", a, errno )
    #define SOCK_T s32
    #define ADDR_T struct sockaddr
    #define ADDRIN_T struct sockaddr_in
    #define CLOSESOCK(s) close(s)
	#define USE_NETWORK
#endif

struct broadcastCB_
{
    u8* data;
    s32 maxSize;
    s32 length;
};

typedef u32 UDPconn;

#ifdef USE_NETWORK

struct NWsenderUDP;

BOOL NWInitNetwork(void);

BOOL NWstartUDPreciever(u16 port, BOOL (*CB_)(u32 addr, struct broadcastCB_* datain, struct broadcastCB_* dataout));

BOOL NWinitUDPsender(u32 addr, u16 port, UDPconn* conn);

BOOL NWsendUDP(UDPconn conn, struct broadcastCB_* outData);

BOOL NWcloseUDPsender(UDPconn conn);

// New Interf

enum BatConnType
{
    InvalidBatConnType = 0,
    Upd,
    Tcp
};

struct sBatConn
{
    BOOL isValid;
    BOOL isConnected;
    enum BatConnType type;
    ADDRIN_T addr;
    s32 sock;
};

typedef struct sBatConn BatConn;

BOOL BatInit();
BOOL BatInitAcceptSocket(s32 port, BatConn* conn);
BOOL BatConnect(s32 addr, s32 port, BatConn* conn);
BOOL BatAccept(BatConn* conn, BatConn* nconn);
s32 BatReceive(BatConn* conn, void* buf, s32 bufMaxSize);
s32 BatSend(BatConn* conn, void* buf, s32 size);
BOOL BatInitUdpSender(u32 addr, u16 port, BatConn *conn);
BOOL BatInitUdpReciever(u16 port, BatConn* conn);
BOOL BatClose(BatConn* conn);

#endif // USE_NETWORK

#ifdef __cplusplus
} /* extern "C" */
#endif

#endif //NITRO_SDK
#endif //NETWORK_H
