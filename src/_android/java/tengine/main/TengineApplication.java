/*
tengine - cross-platform game engine
------------------------------------

Copyright (c) 2013 Papa Pascualle
Contact Email: pascualle@gmail.com

Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
*/

package tengine.main;

import android.app.Activity;
import android.os.Bundle;
import android.view.Window;
import android.view.WindowManager;
import android.view.View;
import tengine.common.AppGLSurfaceView;

//-----------------------------------------------------------------------
//-----------------------------------------------------------------------
//-----------------------------------------------------------------------

public class TengineApplication extends Activity
{
	protected AppGLSurfaceView mGLView = null;

	//-----------------------------------------------------------------------

	static
	{
		System.loadLibrary("tengine");
	}
	//-----------------------------------------------------------------------

	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
	}
	//-----------------------------------------------------------------------

	@Override
	protected void onPause()
	{
		super.onPause();
		if(mGLView != null)
		{
			mGLView.onPause();
		}
	}
	//-----------------------------------------------------------------------

	@Override
	protected void onResume()
	{
		super.onResume();
		if(mGLView != null)
		{
			mGLView.onResume();
		}
	}
	//-----------------------------------------------------------------------

    @Override
    protected void onStart()
	{
		super.onStart();
		setGLSurfaceView();
    }
	//-----------------------------------------------------------------------

	protected void setGLSurfaceView()
	{
		mGLView = new AppGLSurfaceView(this);
		setContentView(mGLView);
	}
	//-----------------------------------------------------------------------

	@Override
	protected void onStop()
	{		
		super.onStop();
		if(mGLView != null)
		{
			mGLView.onStop();
			setContentView(new View(this));
			mGLView = null;
		}
		System.gc();
	}
	//-----------------------------------------------------------------------

	@Override
	public void onLowMemory()
	{
		super.onLowMemory();
		if(mGLView != null)
		{
			mGLView.onLowMemory();
		}
		System.gc();
	}
	//-----------------------------------------------------------------------

	@Override
	protected void onDestroy()
	{
		super.onDestroy();
		final boolean is_finish = isFinishing();
		if(mGLView != null)
		{
			mGLView.onDestroy(is_finish);
		}
		mGLView = null;
		if(is_finish)
		{
			System.runFinalizersOnExit(true);
			System.exit(0);
		}
	}
}
