/*
tengine - cross-platform game engine
------------------------------------

Copyright (c) 2013 Papa Pascualle
Contact Email: pascualle@gmail.com

Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
*/

package tengine.common;

import javax.microedition.khronos.egl.EGL10;
import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.egl.EGLContext;
import javax.microedition.khronos.egl.EGLDisplay;
import javax.microedition.khronos.opengles.GL10;
import android.content.res.AssetManager;
import android.content.Context;
import android.graphics.PixelFormat;
import android.opengl.GLSurfaceView;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.app.ActivityManager;
import android.content.pm.ConfigurationInfo;
import android.os.Build;
import android.util.AttributeSet;
import android.util.Log;
import java.util.List;
import java.util.ArrayList;
import java.util.Collections;

import static javax.microedition.khronos.egl.EGL10.EGL_ALPHA_SIZE;
import static javax.microedition.khronos.egl.EGL10.EGL_BLUE_SIZE;
import static javax.microedition.khronos.egl.EGL10.EGL_BUFFER_SIZE;
import static javax.microedition.khronos.egl.EGL10.EGL_COLOR_BUFFER_TYPE;
import static javax.microedition.khronos.egl.EGL10.EGL_CONFIG_CAVEAT;
import static javax.microedition.khronos.egl.EGL10.EGL_DEPTH_SIZE;
import static javax.microedition.khronos.egl.EGL10.EGL_GREEN_SIZE;
import static javax.microedition.khronos.egl.EGL10.EGL_NONE;
import static javax.microedition.khronos.egl.EGL10.EGL_RED_SIZE;
import static javax.microedition.khronos.egl.EGL10.EGL_RENDERABLE_TYPE;
import static javax.microedition.khronos.egl.EGL10.EGL_RGB_BUFFER;
import static javax.microedition.khronos.egl.EGL10.EGL_SAMPLES;
import static javax.microedition.khronos.egl.EGL10.EGL_SAMPLE_BUFFERS;
import static javax.microedition.khronos.egl.EGL10.EGL_STENCIL_SIZE;
import static javax.microedition.khronos.egl.EGL10.EGL_SURFACE_TYPE;
import static javax.microedition.khronos.egl.EGL10.EGL_WINDOW_BIT;

public class AppGLSurfaceView extends GLSurfaceView 
{
	private static final String TAGWarning = "TengineAppWarning";
	private static final String TAGInfo = "TengineAppInfo";

	private static native void nativeInit(AssetManager mgr, String internalDataPath);
    private static native void nativeStop();
	private static native void nativeRelease(int is_finish);
	private static native void nativePause();
	private static native void nativeResume();
	private static native void nativeLowMemory();
	
    private static native void nativeKeyDown(int keyCode);
    private static native void nativeKeyUp(int keyCode);
    
	private static native void nativeCreateRenderDevice();
	private static native void nativeResize(int w, int h);
	private static native void nativeTick();
    
	private AppRenderer mRenderer;
	
//-----------------------------------------------------------------------

	public AppGLSurfaceView(Context context)
	{
		super(context);
		init(context);
	}

	public AppGLSurfaceView(Context context, AttributeSet attrs)
	{
		super(context, attrs);
		init(context);
	}

	private void init(Context context)
    {
    	ActivityManager activityManager = (ActivityManager)context.getSystemService(Context.ACTIVITY_SERVICE);
		ConfigurationInfo configurationInfo = activityManager.getDeviceConfigurationInfo();
		
		final boolean probablyEmulator = isProbablyEmulator();
		final boolean supportsEs2 = configurationInfo.reqGlEsVersion >= 0x20000 || probablyEmulator; 
		
	    // By default, GLSurfaceView() creates a RGB_565 opaque surface.
	    // If we want a translucent one, we should change the surface's
	    // format here, using PixelFormat.TRANSLUCENT for GL Surfaces
	    // is interpreted as any 32-bit surface with alpha by SurfaceFlinger.
	    boolean translucent = false;
	    int depth = 0; /*16*/
		
    	if (translucent)
	    {
	        this.getHolder().setFormat(PixelFormat.TRANSLUCENT);
	    }
					
		if (supportsEs2)
		{
			/*if(probablyEmulator)
			{
				depth = 16;
				Log.i(TAGInfo "OpenGL ES2, EGLConfig:8888");
				setEGLConfigChooser(8, 8, 8, 8, depth, 0);
			}
			else
			{
				Log.i(TAGInfo "OpenGL ES2, EGLConfig:5650");
				setEGLConfigChooser(5, 6, 5, 0, depth, 0);
			}*/
			setEGLContextClientVersion(2);
			setEGLContextFactory(new ContextFactory(2));
			setEGLConfigChooser(new ConfigChooserGL2(translucent));
		}
		else
		{
			if (probablyEmulator)
			{
				// Avoids crashes on startup with some emulator images.
				depth = 16;
				translucent = true;
			}
			// Setup the context factory for rendering.
			// See ContextFactory class definition below
			setEGLContextFactory(new ContextFactory(1));
			// We need to choose an EGLConfig that matches the format of
			// our surface exactly. This is going to be done in our
			// custom config chooser. See ConfigChooser class definition
			// below.
			if(translucent)
			{
				Log.i(TAGInfo, "OpenGL ES1, EGLConfig:8888");
			}
			else
			{
				Log.i(TAGInfo, "OpenGL ES1, EGLConfig:5650");
			}
			setEGLConfigChooser(translucent ?
									new ConfigChooserGL1(8, 8, 8, 8, depth) :
									new ConfigChooserGL1(5, 6, 5, 0, depth));
		}
	    
    	AppTouchpad.init();
    	mRenderer = new AppRenderer();
    	nativeInit(context.getResources().getAssets(), context.getFilesDir().toString());

    	setRenderer(mRenderer);
    }
//-----------------------------------------------------------------------

	private boolean checkGL20()
	{
		EGL10 egl = (EGL10) EGLContext.getEGL();
		EGLDisplay display = egl.eglGetDisplay(EGL10.EGL_DEFAULT_DISPLAY);

		int[] version = new int[2];
		egl.eglInitialize(display, version);

		int EGL_OPENGL_ES2_BIT = 4;
		int[] configAttribs = { EGL10.EGL_RED_SIZE, 4, EGL10.EGL_GREEN_SIZE, 4,
				EGL10.EGL_BLUE_SIZE, 4, EGL10.EGL_RENDERABLE_TYPE,
				EGL_OPENGL_ES2_BIT, EGL10.EGL_NONE };

		EGLConfig[] configs = new EGLConfig[10];
		int[] num_config = new int[1];
		egl.eglChooseConfig(display, configAttribs, configs, 10, num_config);
		egl.eglTerminate(display);
		return num_config[0] > 0;
	}
//-----------------------------------------------------------------------

	public static boolean isProbablyEmulator()
	{
		return Build.FINGERPRINT.startsWith("generic")
				|| Build.FINGERPRINT.startsWith("unknown")
				|| Build.MODEL.contains("google_sdk")
				|| Build.MODEL.contains("Emulator")
				|| Build.MODEL.contains("Android SDK built for x86")
				|| (Build.BRAND.startsWith("generic") && Build.DEVICE.startsWith("generic"))
				|| "google_sdk".equals(Build.PRODUCT)
				|| System.getProperty("ro.kernel.qemu") != null;
	}
//-----------------------------------------------------------------------

	@Override
    public void onPause()
    {
        super.onPause();
    	nativePause();
    }
//-----------------------------------------------------------------------

    @Override
    public void onResume()
    {
        super.onResume();
        nativeResume();
    }
//-----------------------------------------------------------------------

    public void onStop()
	{
		nativeStop();
		mRenderer = null;
	}
//-----------------------------------------------------------------------
    
    public void onDestroy(boolean is_finish)
    {
		nativeRelease(is_finish ? 1 : 0);
    	mRenderer = null;
    }
//-----------------------------------------------------------------------
    
    public void onLowMemory()
    {
    	nativeLowMemory();
    }
//-----------------------------------------------------------------------
    
    @Override
    public boolean onTouchEvent(MotionEvent event)
    {
    	return AppTouchpad.onTouchEventV2(event);
    }
//-----------------------------------------------------------------------
    
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent msg)
    {
		nativeKeyDown(keyCode);
    	return super.onKeyDown(keyCode, msg);
    }
//-----------------------------------------------------------------------
    
    @Override
    public boolean onKeyUp(int keyCode, KeyEvent msg)
    {
		nativeKeyUp(keyCode);
    	return super.onKeyUp(keyCode, msg);
    } 
//-----------------------------------------------------------------------
    
	public static void checkEglError(String prompt, EGL10 egl)
	{
	    int error;
	    while ((error = egl.eglGetError()) != EGL10.EGL_SUCCESS)
	    {
	    	Log.w(TAGWarning, String.format("%s: EGL error: 0x%x", prompt, error));
	    }
	}
    
//-----------------------------------------------------------------------
//-----------------------------------------------------------------------
    
	private static class ContextFactory implements GLSurfaceView.EGLContextFactory
	{
		private int glVer;

		ContextFactory(int glVersion)
		{
			glVer = glVersion;
		}

	    public EGLContext createContext(EGL10 egl, EGLDisplay display, EGLConfig eglConfig)
	    {
			int EGL_CONTEXT_CLIENT_VERSION = 0x3098;
			if (display == null || eglConfig == null)
			{
				return EGL10.EGL_NO_CONTEXT;
			}
			checkEglError("before eglCreateContext", egl);
			int[] attrib_list = {EGL_CONTEXT_CLIENT_VERSION, glVer, EGL10.EGL_NONE};
			EGLContext context = egl.eglCreateContext(display, eglConfig, EGL10.EGL_NO_CONTEXT, attrib_list);
			checkEglError("after eglCreateContext", egl);
			return context;

	    }
	    //-----------------------------------------------------------------------
	    
	    public void destroyContext(EGL10 egl, EGLDisplay display, EGLContext context)
	    {
	        egl.eglDestroyContext(display, context);
	    }
	}
    
//-----------------------------------------------------------------------
//-----------------------------------------------------------------------

	private static class ConfigChooserGL2 implements GLSurfaceView.EGLConfigChooser
	{
		/**
		 * Requires API level 17
		 * @see android.opengl.EGL14.EGL_CONFORMANT;
		 */
		@SuppressWarnings("JavadocReference")
		private static final int EGL_CONFORMANT = 0x3042;

		/**
		 * Requires API level 17
		 * @see android.opengl.EGL14.EGL_OPENGL_ES2_BIT;
		 */
		@SuppressWarnings("JavadocReference")
		private static final int EGL_OPENGL_ES2_BIT = 0x0004;

		private final boolean translucentSurface;

		//-----------------------------------------------------------------------

		public ConfigChooserGL2(boolean translucentSurface)
		{
			super();
			this.translucentSurface = translucentSurface;
		}
		//-----------------------------------------------------------------------

		@Override
		public EGLConfig chooseConfig(EGL10 egl, EGLDisplay display)
		{
			int[] configAttribs = getConfigAttributes();
			int[] numConfigs = getNumberOfConfigurations(egl, display, configAttribs);
			if (numConfigs[0] < 1)
			{
				Log.w(AppGLSurfaceView.TAGWarning, "EGLConfigChooser: getNumberOfConfigurations() returned no configs");
			}
			EGLConfig[] possibleConfigurations = getPossibleConfigurations(egl, display, configAttribs, numConfigs);
			EGLConfig config = chooseBestMatchConfig(egl, display, possibleConfigurations);
			if (config == null)
			{
				Log.w(AppGLSurfaceView.TAGWarning, "EGLConfigChooser: no config chosen");
			}
			return config;
		}
		//-----------------------------------------------------------------------

		private int[] getNumberOfConfigurations(EGL10 egl, EGLDisplay display, int[] configAttributes)
		{
			int[] numConfigs = new int[1];
			if (!egl.eglChooseConfig(display, configAttributes, null, 0, numConfigs))
			{
				AppGLSurfaceView.checkEglError("eglChooseConfig(NULL)", egl);
			}
			return numConfigs;
		}
		//-----------------------------------------------------------------------

		private EGLConfig[] getPossibleConfigurations(EGL10 egl, EGLDisplay display, int[] configAttributes, int[] numConfigs)
		{
			EGLConfig[] configs = new EGLConfig[numConfigs[0]];
			if (!egl.eglChooseConfig(display, configAttributes, configs, numConfigs[0], numConfigs))
			{
				AppGLSurfaceView.checkEglError("eglChooseConfig()", egl);
			}
			return configs;
		}

		enum BufferQualityFormat
		{
			QF24Bit(0),
			QF32BitNoAlpha(1),
			QF32BitAlpha(2),
			QF16Bit(3),
			QFUnknown(4);

			int value;

			BufferQualityFormat(int value)
			{
				this.value = value;
			}
		}

		enum DepthStencilFormat
		{
			DS0Depth0Stencil(0),
			DS16Depth8Stencil(1),
			DS24Depth8Stencil(2),
			DSUnknown(3);

			int value;

			DepthStencilFormat(int value)
			{
				this.value = value;
			}
		}

		private EGLConfig chooseBestMatchConfig(EGL10 egl, EGLDisplay display, EGLConfig[] configs)
		{
			class Config implements Comparable<Config>
			{
				private final BufferQualityFormat bufferFormat;
				private final DepthStencilFormat depthStencilFormat;
				private final boolean isNotConformant;
				private final boolean isCaveat;
				private final int index;
				private final EGLConfig config;

				public Config(BufferQualityFormat bufferFormat, DepthStencilFormat depthStencilFormat, boolean isNotConformant, boolean isCaveat, int index, EGLConfig config)
				{
					this.bufferFormat = bufferFormat;
					this.depthStencilFormat = depthStencilFormat;
					this.isNotConformant = isNotConformant;
					this.isCaveat = isCaveat;
					this.index = index;
					this.config = config;
				}

				private int compare(int x, int y)
				{
					return Integer.compare(x, y);
				}

				private int compare(boolean x, boolean y)
				{
					return (x == y) ? 0 : (x ? 1 : -1);
				}

				@Override
				public int compareTo(Config other)
				{
					int i = compare(bufferFormat.value, other.bufferFormat.value);
					if (i != 0)
					{
						return i;
					}
					i = compare(depthStencilFormat.value, other.depthStencilFormat.value);
					if (i != 0)
					{
						return i;
					}
					i = compare(isNotConformant, other.isNotConformant);
					if (i != 0)
					{
						return i;
					}
					i = compare(isCaveat, other.isCaveat);
					if (i != 0)
					{
						return i;
					}
					i = compare(index, other.index);
					return i;
				}
			}

			List<Config> matches = new ArrayList<>();

			int i = 0;
			for (EGLConfig config : configs)
			{
				if (config == null)
				{
					continue;
				}

				i++;

				int caveat = getConfigAttr(egl, display, config, EGL_CONFIG_CAVEAT);
				int conformant = getConfigAttr(egl, display, config, EGL_CONFORMANT);
				int bits = getConfigAttr(egl, display, config, EGL_BUFFER_SIZE);
				int red = getConfigAttr(egl, display, config, EGL_RED_SIZE);
				int green = getConfigAttr(egl, display, config, EGL_GREEN_SIZE);
				int blue = getConfigAttr(egl, display, config, EGL_BLUE_SIZE);
				int alpha = getConfigAttr(egl, display, config, EGL_ALPHA_SIZE);
				int depth = getConfigAttr(egl, display, config, EGL_DEPTH_SIZE);
				int stencil = getConfigAttr(egl, display, config, EGL_STENCIL_SIZE);
				int sampleBuffers = getConfigAttr(egl, display, config, EGL_SAMPLE_BUFFERS);
				int samples = getConfigAttr(egl, display, config, EGL_SAMPLES);

				boolean firstCheck = (depth == 24) || (depth == 16) || (depth == 0);
				firstCheck &= (stencil == 8) || (stencil == 0);
				firstCheck &= sampleBuffers == 0;
				firstCheck &= samples == 0;

				if (firstCheck)
				{
					BufferQualityFormat bufferFormat;
					if ((bits == 16) && (red == 5) && (green == 6) && (blue == 5) && (alpha == 0))
					{
						bufferFormat = BufferQualityFormat.QF16Bit;
					}
					else if ((bits == 32) && (red == 8) && (green == 8) && (blue == 8) && (alpha == 0))
					{
						bufferFormat = BufferQualityFormat.QF32BitNoAlpha;
					}
					else if ((bits == 32) && (red == 8) && (green == 8) && (blue == 8) && (alpha == 8))
					{
						bufferFormat = BufferQualityFormat.QF32BitAlpha;
					}
					else if ((bits == 24) && (red == 8) && (green == 8) && (blue == 8) && (alpha == 0))
					{
						bufferFormat = BufferQualityFormat.QF24Bit;
					}
					else
					{
						bufferFormat = BufferQualityFormat.QFUnknown;
					}

					DepthStencilFormat depthStencilFormat;
					if ((depth == 16) && (stencil == 8))
					{
						depthStencilFormat = DepthStencilFormat.DS16Depth8Stencil;
					}
					else if ((depth == 24) && (stencil == 8))
					{
						depthStencilFormat = DepthStencilFormat.DS24Depth8Stencil;
					}
					else if ((depth == 0) && (stencil == 0))
					{
						depthStencilFormat = DepthStencilFormat.DS0Depth0Stencil;
					}
					else
					{
						depthStencilFormat = DepthStencilFormat.DSUnknown;
					}

					boolean isNotConformant = (conformant & EGL_OPENGL_ES2_BIT) != EGL_OPENGL_ES2_BIT;
					boolean isCaveat = caveat != EGL_NONE;

					if (bufferFormat != BufferQualityFormat.QFUnknown)
					{
						matches.add(new Config(bufferFormat, depthStencilFormat, isNotConformant, isCaveat, i, config));
					}
				}
			}

			Collections.sort(matches);

			if (matches.size() == 0)
			{
				Log.w(AppGLSurfaceView.TAGWarning,"EGLConfig: No matching configurations after filtering");
				return null;
			}
			Config bestMatch = matches.get(0);
			if (bestMatch.isCaveat)
			{
				Log.w(AppGLSurfaceView.TAGWarning,"EGLConfig: Chosen config has a caveat");
			}
			if (bestMatch.isNotConformant)
			{
				Log.w(AppGLSurfaceView.TAGWarning,"EGLConfig: Chosen config is not conformant");
			}

			return bestMatch.config;
		}

		private int getConfigAttr(EGL10 egl, EGLDisplay display, EGLConfig config, int attributeName)
		{
			int[] attributevalue = new int[1];
			if (!egl.eglGetConfigAttrib(display, config, attributeName, attributevalue))
			{
				AppGLSurfaceView.checkEglError("eglGetConfigAttrib(" + attributeName + ")", egl);
			}
			return attributevalue[0];
		}

		private int[] getConfigAttributes() {
			boolean emulator = AppGLSurfaceView.isProbablyEmulator();
			if(emulator)
			{
				Log.i(AppGLSurfaceView.TAGInfo, "Run in emulator");
			}
			return new int[]{
					EGL_CONFIG_CAVEAT, EGL_NONE,
					EGL_SURFACE_TYPE, EGL_WINDOW_BIT,
					EGL_BUFFER_SIZE, 16,
					EGL_RED_SIZE, 5,
					EGL_GREEN_SIZE, 6,
					EGL_BLUE_SIZE, 5,
					EGL_ALPHA_SIZE, translucentSurface ? 8 : 0,
					EGL_DEPTH_SIZE, 0, /*16*/
					EGL_STENCIL_SIZE, 0, /*8*/
					(emulator ? EGL_NONE : EGL_CONFORMANT), EGL_OPENGL_ES2_BIT,
					(emulator ? EGL_NONE : EGL_COLOR_BUFFER_TYPE), EGL_RGB_BUFFER,
					EGL_RENDERABLE_TYPE, EGL_OPENGL_ES2_BIT,
					EGL_NONE
			};
		}
	}

    private static class ConfigChooserGL1 implements GLSurfaceView.EGLConfigChooser
    {
        public ConfigChooserGL1(int r, int g, int b, int a, int depth)
        {
            mRedSize = r;
            mGreenSize = g;
            mBlueSize = b;
            mAlphaSize = a;
            mDepthSize = depth;
        }
        //-----------------------------------------------------------------------
        
        // This EGL config specification is used to specify 1.x rendering.
        // We use a minimum size of 4 bits for red/green/blue, but will
        // perform actual matching in chooseConfig() below.
        private static final int EGL_OPENGL_ES_BIT = 1;
        private static final int[] s_configAttribs2 =
        {
            EGL10.EGL_RED_SIZE, 4,
            EGL10.EGL_GREEN_SIZE, 4,
            EGL10.EGL_BLUE_SIZE, 4,
            EGL10.EGL_RENDERABLE_TYPE, EGL_OPENGL_ES_BIT,
            EGL10.EGL_NONE
        };
        //-----------------------------------------------------------------------
        
        public EGLConfig chooseConfig(EGL10 egl, EGLDisplay display)
        {
            int[] num_config = new int[1];
            egl.eglChooseConfig(display, s_configAttribs2, null, 0, num_config);
            int numConfigs = num_config[0];
            if (numConfigs <= 0)
            {
                throw new IllegalArgumentException("No configs match configSpec");
            }
            EGLConfig[] configs = new EGLConfig[numConfigs];
            egl.eglChooseConfig(display, s_configAttribs2, configs, numConfigs, num_config);
            return chooseConfig(egl, display, configs);
        }
        //-----------------------------------------------------------------------
        
        public EGLConfig chooseConfig(EGL10 egl, EGLDisplay display, EGLConfig[] configs)
        {
            for(EGLConfig config : configs)
            {
                int d = findConfigAttrib(egl, display, config, EGL10.EGL_DEPTH_SIZE);
                if (d < mDepthSize)
                {
                    continue;
                }
				int r = findConfigAttrib(egl, display, config, EGL10.EGL_RED_SIZE);
				int g = findConfigAttrib(egl, display, config, EGL10.EGL_GREEN_SIZE);
				int b = findConfigAttrib(egl, display, config, EGL10.EGL_BLUE_SIZE);
				int a = findConfigAttrib(egl, display, config, EGL10.EGL_ALPHA_SIZE);
                if (r == mRedSize && g == mGreenSize && b == mBlueSize && a == mAlphaSize)
                {
                    return config;
                }
            }
            return null;
        }
        //-----------------------------------------------------------------------
        
        private int findConfigAttrib(EGL10 egl, EGLDisplay display, EGLConfig config, int attribute)
        {
            if (egl.eglGetConfigAttrib(display, config, attribute, mValue))
            {
                return mValue[0];
            }
            return 0;
        }
        //-----------------------------------------------------------------------
        
        protected int mRedSize;
        protected int mGreenSize;
        protected int mBlueSize;
        protected int mAlphaSize;
        protected int mDepthSize;
        private final int[] mValue = new int[1];
    }
    
  //-----------------------------------------------------------------------
  //-----------------------------------------------------------------------

    private static class AppRenderer implements GLSurfaceView.Renderer
    {
		public void onSurfaceCreated(GL10 gl, EGLConfig config)
		{
			logConfig(config);
			nativeCreateRenderDevice();
	  	}
		//-----------------------------------------------------------------------

		public void onSurfaceChanged(GL10 gl, int w, int h)
		{
			nativeResize(w, h);
		}
		//-----------------------------------------------------------------------
	      
		public void onDrawFrame(GL10 gl)
		{
			nativeTick();
		}
		//-----------------------------------------------------------------------

		private void logConfig(EGLConfig config)
		{
			EGL10 egl = (EGL10) EGLContext.getEGL();
			EGLDisplay display = egl.eglGetDisplay(EGL10.EGL_DEFAULT_DISPLAY);
			int r = getAttrib(egl, display, config, EGL10.EGL_RED_SIZE);
			int g = getAttrib(egl, display, config, EGL10.EGL_GREEN_SIZE);
			int b = getAttrib(egl, display, config, EGL10.EGL_BLUE_SIZE);
			int a = getAttrib(egl, display, config, EGL10.EGL_ALPHA_SIZE);
			Log.i(TAGInfo, "framebuffer: (" + r + ", " + g + ", " + b + ", " + a + ")");
		}
		//-----------------------------------------------------------------------

		private int getAttrib(EGL10 egl, EGLDisplay display, EGLConfig config, int attrib)
		{
			int[] value = new int[1];
			if (egl.eglGetConfigAttrib(display, config, attrib, value))
			{
				return value[0];
			}
			return 0;
		}
		//-----------------------------------------------------------------------
	}
}
