#ifndef _A_STAR_HEAP_H_
#define _A_STAR_HEAP_H_

#include "platform.h"

#ifdef __cplusplus
extern "C" {
#endif

struct AStarPathNode;

struct AStarHeap
{
	u32 size;
	u32 top;
	struct AStarPathNode **arr;
	s32 (*cmpfn)(const struct AStarPathNode *p1, const struct AStarPathNode *p2);
};

void A_STAR_HeapInit(struct AStarHeap *heap, u32 maxSize, s32 (*cmpfn)(const struct AStarPathNode *p1, const struct AStarPathNode *p2));
void A_STAR_HeapRelease(struct AStarHeap *heap);

void A_STAR_HeapClear(struct AStarHeap *heap);
void A_STAR_HeapPush(struct AStarHeap *heap, const struct AStarPathNode *ptr);
void A_STAR_HeapRearrange(struct AStarHeap *heap, s32 c);
const struct AStarPathNode* A_STAR_HeapPop(struct AStarHeap *heap);

#ifdef __cplusplus
} /* extern "C" */
#endif

#endif
