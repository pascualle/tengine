/*
tengine - cross-platform game engine
------------------------------------

Copyright (c) 2013 Papa Pascualle
Contact Email: pascualle@gmail.com

Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
*/

#include "lib/platform_low.h"
#include <windowsx.h>

#ifndef _MSC_VER
	#pragma hdrstop
	#pragma argsused
#endif

#include <fcntl.h>
#include <io.h>
#include <iostream>
#include <fstream>
#include <direct.h>
#include "lib/a_touchpad.h"
#include "lib/a_gamepad.h"
#include "lib/render.h"
#include "filesystem.h"
#include "gamefield.h"
#include "lib/jobs_low.h"
#include "filesystem.h"
#include "gamepad.h"
#include "profiler/profile.h"
#ifdef PROFILER_ENABLE
#include "static_allocator.h"
#endif
#ifdef USE_OPENGL_RENDER
#include <GL/gl.h>
#endif

#ifndef _USE_OLD_IOSTREAMS
using namespace std;
#endif

struct CmdDataStruct
{
	_TCHAR strw[MAX_PATH];
	_TCHAR strh[MAX_PATH];
	_TCHAR path[MAX_PATH * 2];
};

#define OUTPUT_CONSOLE 1

s32 gsAppAlive = TRUE;

//do not change values here, use your IDE debug command line instead
// for example -w1024 -h728 
#define DEFAULT_CLIENT_SIZE_W 512
#define DEFAULT_CLIENT_SIZE_H 512
static s32 sgClientWidth = DEFAULT_CLIENT_SIZE_W;
static s32 sgClientHeight = DEFAULT_CLIENT_SIZE_H;
static s32 sgClientWidth0 = -1;
static s32 sgClientHeight0 = -1;
static HWND  shWnd = NULL;
static HGLRC sghRC = NULL;
static BOOL gsWindowActive = FALSE;
static BOOL gsHasFocus = FALSE;
static TEnginePlatformData *gspPlatformData = NULL;
static struct CmdDataStruct gsCmdData;
static const _TCHAR APP_NAME[] = _T("TEmulator");
static const char DEFAULT_RES_FOLDERS[] = "assets\\data\\";
static char gsResPath[MAX_PATH];

//---------------------------------------------------------------------------

static BOOL checkGLErrors(void);
static s32 _init_display(void);
static void _term_display(void);
static void OpenStdConsole(void);
static void CloseStdConsole(void);
#ifdef USE_CUSTOM_RENDER
//dont forget about USE_TILEBUFFER
static HWND sghBmpWnd = NULL;
static HBITMAP sgDirectBitmap = NULL;
static void _transferFrameBuffer(const struct BMPImage* pFrameBuffer, s32 offX, s32 offY, s32 frameWidth, s32 frameHeight);
#endif
//---------------------------------------------------------------------------

#ifdef PROFILER_ENABLE
#define PROFILER_STATIC_ALLOCATOR_HEAP_SIZE (1024 * 1024 * 4)
static struct StaticAllocator sProfilerStaticAllocator;
static u8 sStaticAllocatorHeap[PROFILER_STATIC_ALLOCATOR_HEAP_SIZE];
#endif

//---------------------------------------------------------------------------

static BOOL checkGLErrors()
{
#ifdef USE_OPENGL_RENDER
	GLenum error = glGetError();
	if (error != GL_NO_ERROR)
	{
		_TCHAR errorString[32];
		_stprintf(errorString, _T("0x%04x"), error);
		MessageBox(NULL, errorString, _T("GL Error"), MB_OK);
	}
	return error;
#else
	return 0;
#endif
}
//---------------------------------------------------------------------------

BOOL SetupPixelFormat(HDC hdc)
{
#ifdef USE_OPENGL_RENDER
	s32 pixelformat;
	PIXELFORMATDESCRIPTOR pfd = {
        sizeof(PIXELFORMATDESCRIPTOR),  //  size of this pfd
        1,                     // version number
        PFD_DRAW_TO_WINDOW |   // support window
        PFD_SUPPORT_OPENGL |   // support OpenGL
        PFD_DOUBLEBUFFER,      // double buffered
        PFD_TYPE_RGBA,         // RGBA type
        32,                    // 32-bit color depth
        0, 0, 0, 0, 0, 0,      // color bits ignored
        0,                     // no alpha buffer
        0,                     // shift bit ignored
        0,                     // no accumulation buffer
        0, 0, 0, 0,            // accum bits ignored
        32,                    // 32-bit z-buffer
        0,                     // no stencil buffer
        0,                     // no auxiliary buffer
        PFD_MAIN_PLANE,        // main layer
        0,                     // reserved
        0, 0, 0                // layer masks ignored
	};
	if((pixelformat = ChoosePixelFormat(hdc, &pfd)) == 0 )
	{
		return FALSE;
	}
	if(SetPixelFormat(hdc, pixelformat, &pfd) == FALSE)
	{
		return FALSE;
	}
#else
	(void)hdc;
#endif
    return TRUE;
}
//---------------------------------------------------------------------------

static void _setClientSize(HWND hwnd, int clientWidth, int clientHeight)
{
	if(IsWindow(hwnd))
	{
		RECT winRC, clientRC;
		GetWindowRect(hwnd, &winRC);
		GetClientRect(hwnd, &clientRC);
		int dx = (clientRC.right - clientRC.left) - clientWidth;
		int dy = (clientRC.bottom - clientRC.top) - clientHeight;
		SetWindowPos(hwnd, NULL, 0, 0, winRC.right - winRC.left - dx, winRC.bottom - winRC.top - dy, SWP_NOZORDER | SWP_NOMOVE | SWP_NOACTIVATE);
		//RECT newClientRC;
		//GetClientRect(hwnd, &newClientRC);
		//SDK_ASSERT((newClientRC.right - newClientRC.left) == clientWidth);
		//SDK_ASSERT((newClientRC.bottom - newClientRC.top) == clientHeight);
	}  
}
//-----------------------------------------------------------------------

static LRESULT CALLBACK wndProc(HWND wnd, UINT message,
								WPARAM wParam, LPARAM lParam)
{
    RECT rc;
    switch (message)
    {
#ifdef USE_CUSTOM_RENDER
		case WM_CREATE:
			sghBmpWnd = CreateWindow(_T("STATIC"), NULL, WS_VISIBLE | WS_CHILD | SS_BITMAP, 0, 0, 2, 2, wnd, NULL, NULL, NULL);
			break;
#endif
		case WM_KEYUP:
			switch(wParam)
			{
				case VK_F11:
					if(gspPlatformData->renderDevice == TRUE)
					{
						OS_Printf("lost render device signal\n");
						_term_display();
					}
					else
					{
						_init_display();
					}
				break;
				case VK_F9:
					if(shWnd)
					{
						SDK_ASSERT(sgClientWidth0 > 0);
						const s32 t = sgClientWidth0;
						sgClientWidth0 = sgClientHeight0;
						sgClientHeight0 = t; 
						_setClientSize(shWnd, sgClientWidth0, sgClientHeight0);
					}
				break;
				case VK_F7:
					tfgLowMemory();
				break;
				default:
				break;
			}

		case WM_KEYDOWN:			
			if (wParam == VK_ESCAPE)
			{
				DestroyWindow(wnd);
				gsAppAlive = FALSE;
			}
			else
			{
				s32 key = -1;
				switch(wParam)
				{
					case VK_CONTROL:
						key = PAD_BUTTON_A;
						// test resize GameField::GetInstance().resize(sWindowWidth, sWindowHeight);
					break;
					case VK_SHIFT:
						key = PAD_BUTTON_B;
					break;
					case VK_RETURN:
						key = PAD_BUTTON_START;
					break;
					case VK_RIGHT: 
						key = PAD_KEY_RIGHT;
					break;
					case VK_LEFT:
						key = PAD_KEY_LEFT;
					break;
					case VK_UP:
						key = PAD_KEY_UP;
					break;
					case VK_DOWN:
						key = PAD_KEY_DOWN;
					break;
					default:
					break;
				}
				if(key >= 0)
				{
					switch (message)
					{
						case WM_KEYDOWN:
							onKeyDown(key);
						break;
						case WM_KEYUP:
							onKeyUp(key);
					}
				}
			}
			break;
			
		case WM_ACTIVATE:
			gsHasFocus = wParam != WA_INACTIVE;
		    if(gspPlatformData->renderDevice == FALSE && gsWindowActive == FALSE)
			{
				if(_init_display() != 0)
				{
		    		gsAppAlive = FALSE;
				}
				else
				{
					if(gsWindowActive == FALSE)
					{
						SDK_ASSERT(sgClientWidth0 > 0);
						tfgInit();
						tfgResize(sgClientWidth0, sgClientHeight0);
#ifdef USE_OPENGL_RENDER
						glRender_Resize(sgClientWidth0, sgClientHeight0);
#endif
						OS_Printf("init tengine display with size: %d %d\n", sgClientWidth0, sgClientHeight0);
					}
					gsWindowActive = TRUE;
				}
			}
			break;

		case WM_CLOSE:
			DestroyWindow(wnd);
#ifdef USE_CUSTOM_RENDER
			if(sghBmpWnd)
			{
				DestroyWindow(sghBmpWnd);
				sghBmpWnd = NULL;
			}
#endif
			gsAppAlive = FALSE;
			break;

		case WM_DESTROY:
			PostQuitMessage(0);
			gsAppAlive = FALSE;
			break;

		case WM_SIZE:
			{
				GetClientRect(shWnd, &rc);
				if(sgClientWidth0 < 0)
				{
					sgClientWidth0 = rc.right - rc.left;
					sgClientHeight0 = rc.bottom - rc.top;
				}
				if(sgClientWidth != rc.right - rc.left || sgClientHeight != rc.bottom - rc.top)
				{
					sgClientWidth = rc.right - rc.left;
					sgClientHeight = rc.bottom - rc.top;
					tfgResize(sgClientWidth, sgClientHeight);
#ifdef USE_OPENGL_RENDER
					glRender_Resize(sgClientWidth, sgClientHeight);
#endif
				}
			}
			return 0;

		default:
		break;
	}
	return DefWindowProc(wnd, message, wParam, lParam);
}
//---------------------------------------------------------------------------

s32 _init_display()
{
#ifdef USE_OPENGL_RENDER
	if(sghRC == NULL)
	{
		SDK_NULL_ASSERT(gspPlatformData);
		gspPlatformData->hDC = GetDC(shWnd);
		if(!SetupPixelFormat(gspPlatformData->hDC))
		{
			_term_display();
			return -1;
		}
		sghRC = wglCreateContext(gspPlatformData->hDC);
		wglMakeCurrent(gspPlatformData->hDC, sghRC);
		checkGLErrors();
	}
#endif
	Platform_restoreRenderDevice();
    return 0;
}
//---------------------------------------------------------------------------

void _term_display()
{
	Platform_lostRenderDevice();
#ifdef USE_OPENGL_RENDER
	SDK_NULL_ASSERT(gspPlatformData);
	wglMakeCurrent(NULL, NULL);
	if(sghRC)
	{
		wglDeleteContext(sghRC);
	}
	if(gspPlatformData->hDC)
	{
		ReleaseDC(shWnd, gspPlatformData->hDC);
	}
	sghRC = NULL;
	gspPlatformData->hDC = NULL;
#endif
}
//---------------------------------------------------------------------------
 
void _parseCmdLine(const _TCHAR *str)
{
	gsCmdData.path[0] = 0;
	gsCmdData.strh[0] = 0;
	gsCmdData.strw[0] = 0;
	if(_tcslen(str) > 0)
	{
		size_t len, pos_t;
		const _TCHAR *pos_pt;
		const _TCHAR *chw = _T("-w");
		const _TCHAR *chh = _T("-h");
		const _TCHAR *chp = _T("-p");
		const _TCHAR *ch_ = _T(" ");
		const _TCHAR *che = _T("\0");
		len = _tcslen(str); 
		pos_pt	= _tcsstr(str, chw);
		if(pos_pt != NULL)
		{
			pos_t = (pos_pt - str) / sizeof(_TCHAR) + 2;
			while(len > pos_t && str[pos_t] != ch_[0] && str[pos_t] != che[0])
			{
				_tcsncat(gsCmdData.strw, &str[pos_t], 1);
				pos_t++;
			}
		}
		pos_pt = _tcsstr(str, chh);
		if(pos_pt != NULL)
		{
			pos_t = (pos_pt - str) / sizeof(_TCHAR) + 2;
			while(len > pos_t && str[pos_t] != ch_[0] && str[pos_t] != che[0])
			{
				_tcsncat(gsCmdData.strh, &str[pos_t], 1);
				pos_t++;
			}
		}
		pos_pt = _tcsstr(str, chp);
		if(pos_pt != NULL)
		{
			pos_t = (pos_pt - str) / sizeof(_TCHAR) + 2;
			while(len > pos_t && str[pos_t] != ch_[0] && str[pos_t] != che[0])
			{
				_tcsncat(gsCmdData.path, &str[pos_t], 1);
				pos_t++;
			}
		}
	}
}
//-----------------------------------------------------------------------

//#define SIMULATE_SLOW_FRAMERATE
#ifdef SIMULATE_SLOW_FRAMERATE
static u32 _Platform_GetTime()
{
	LARGE_INTEGER s_frequency;
	const BOOL use_qpf = QueryPerformanceFrequency(&s_frequency);
	if(use_qpf)
	{
		LARGE_INTEGER now;
		QueryPerformanceCounter(&now);
		return (u32)((1000LL * now.QuadPart) / s_frequency.QuadPart);
	}
	else
	{
		return (u32)GetTickCount();
	}
}
#endif
//-----------------------------------------------------------------------

int WINAPI _tWinMain(HINSTANCE instance, HINSTANCE prevInstance, LPTSTR cmdLine, int cmdShow)
{
    MSG msg;
	WNDCLASSEX wc;
    DWORD windowStyle;
	struct InitFileSystemData fileSystemData;
	s32 windowX, windowY;
	char *resPathPtr;

	gspPlatformData = Platform_GetPlatformData();
	gspPlatformData->renderDevice = FALSE;

#ifdef PROFILER_ENABLE
	StaticAllocator_Init(&sProfilerStaticAllocator, sStaticAllocatorHeap, PROFILER_STATIC_ALLOCATOR_HEAP_SIZE);
	PROFILE_INIT(sProfilerStaticAllocator);
#endif

	strncpy(gsResPath, GetCommandLine(), MAX_PATH);
	resPathPtr = strrchr(gsResPath, '\\');
	*++resPathPtr = 0;
	strcat(resPathPtr, DEFAULT_RES_FOLDERS);
	resPathPtr = gsResPath; 
	if(*resPathPtr == '\"')
	{
		++resPathPtr;
	}

	_parseCmdLine(cmdLine);

	tfgInitMemory();

#ifdef USE_CUSTOM_RENDER
	egRender_Init(_transferFrameBuffer);
#elif defined USE_OPENGL_RENDER
	glRender_Init();
#endif

	tfgSetWindowModeSize(&sgClientWidth, &sgClientHeight);

	if(_tcslen(gsCmdData.path) > 0)
	{
		fileSystemData.mpPath = gsCmdData.path;
		InitFileSystem(&fileSystemData);
	}
	else
	{
		fileSystemData.mpPath = resPathPtr;
		InitFileSystem(&fileSystemData);
	}

	if(_tcslen(gsCmdData.strw) > 0)
	{
		sgClientWidth = _ttoi(gsCmdData.strw);
	}
	if(_tcslen(gsCmdData.strh) > 0)
	{
		sgClientHeight = _ttoi(gsCmdData.strh);
	}

	wc.cbSize = sizeof(WNDCLASSEX);
	wc.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;
	wc.lpfnWndProc = (WNDPROC)wndProc;
	wc.cbClsExtra = 0;
	wc.cbWndExtra = 0;
	wc.hInstance = instance;
	wc.hIcon = LoadIcon(NULL, IDI_WINLOGO);
	wc.hCursor = LoadCursor(prevInstance, IDC_ARROW);
	wc.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	wc.lpszMenuName = NULL;
	wc.lpszClassName = APP_NAME;
	wc.hIconSm = NULL;
	if (!RegisterClassEx(&wc))
	{
		return FALSE;
	}

	windowStyle = WS_OVERLAPPED | WS_CAPTION | WS_SYSMENU | WS_CLIPSIBLINGS | WS_CLIPCHILDREN | WS_VISIBLE;
	windowX = CW_USEDEFAULT;
	windowY = 0;

	shWnd = CreateWindow(APP_NAME, APP_NAME, windowStyle,
                        windowX, windowY,
						sgClientWidth, sgClientHeight,
                        NULL, NULL, instance, NULL);

	if (!shWnd)
	{
		return FALSE;
	}

	_setClientSize(shWnd, sgClientWidth, sgClientHeight);

	OpenStdConsole();

	if(_tcslen(gsCmdData.strw) > 0 || _tcslen(gsCmdData.strh) > 0 || _tcslen(gsCmdData.path) > 0)
	{
		_tprintf("parameters: ");
		if(_tcslen(gsCmdData.strw) > 0)
		{
			_tprintf("w:%s ", gsCmdData.strw);
		}
		if(_tcslen(gsCmdData.strh) > 0)
		{
			_tprintf("h:%s ", gsCmdData.strh);
		}
		if(_tcslen(gsCmdData.path) > 0)
		{
			_tprintf("assets: %s ", gsCmdData.path);
		}
		_tprintf("\n");
    }

	ShowWindow(shWnd, cmdShow);
	UpdateWindow(shWnd);

#ifdef SIMULATE_SLOW_FRAMERATE
	u32 prev_t = _Platform_GetTime(); 
#endif
	Platform_InitTick();

	BOOL lbuttonPressed = FALSE;
	while (gsAppAlive)
	{
		PROFILE_START;
		while (PeekMessage(&msg, shWnd, 0, 0, PM_NOREMOVE))
        {
			if (GetMessage(&msg, shWnd, 0, 0))
            {
                TranslateMessage(&msg);
                DispatchMessage(&msg);
            }
			else
			{
				gsAppAlive = FALSE;
				continue;
        	}
		}
#ifdef SIMULATE_SLOW_FRAMERATE
		u32 curr_t = _Platform_GetTime();
		const u32 ms = curr_t - prev_t; 
		if(ms < 200)
		{
			continue;
		}
		prev_t = curr_t;
#endif
		POINT curPos;
		if(GetCursorPos(&curPos) == TRUE)
		{
			ScreenToClient(shWnd, &curPos);
			onTouchPadMove(0, curPos.x, curPos.y);
			if(gsHasFocus == TRUE)
			{
				if(GetAsyncKeyState(VK_LBUTTON) & 0x8000)
				{
					lbuttonPressed = TRUE;
					onTouchPadDown(0, curPos.x, curPos.y);
				}
				else if(lbuttonPressed == TRUE)
				{
					lbuttonPressed = FALSE;
					onTouchPadUp(0, curPos.x, curPos.y);
				}
			}
			else if(lbuttonPressed == TRUE)
			{
				lbuttonPressed = FALSE;
				onTouchPadUp(0, curPos.x, curPos.y);
			}
		}
		Platform_Tick();
#ifdef SDK_DEBUG
		if(gspPlatformData->renderDevice == TRUE && checkGLErrors() != 0)
		{
			gsAppAlive = FALSE;
		}
#endif
		PROFILE_FINISH;
    }
	tfgRelease();
	ReleaseFileSystem();
#ifdef USE_CUSTOM_RENDER
	egRender_Release();
#elif defined USE_OPENGL_RENDER
	glRender_Release();
#endif
	_term_display();
	CloseStdConsole();
#ifdef USE_CUSTOM_RENDER
	if(sgDirectBitmap)
	{
		DeleteObject(sgDirectBitmap);
		sgDirectBitmap = NULL;
	}
#endif
    return 0;
}
//---------------------------------------------------------------------------

void OpenStdConsole()
{
#if defined OUTPUT_CONSOLE && defined SDK_DEBUG
	AllocConsole();
	SetConsoleTitle("TEngine output window");
	SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), FOREGROUND_GREEN | FOREGROUND_BLUE | FOREGROUND_RED);

	CONSOLE_SCREEN_BUFFER_INFO coninfo;
	GetConsoleScreenBufferInfo(GetStdHandle(STD_OUTPUT_HANDLE), &coninfo);
	coninfo.dwSize.Y = 500;
	SetConsoleScreenBufferSize(GetStdHandle(STD_OUTPUT_HANDLE), coninfo.dwSize);

#if (defined(_MSC_VER) && (_MSC_VER < 1900)) //since vc2015
	LONG hStdHandle;
	int hConHandle;
	FILE *file;
	// redirect unbuffered STDOUT to the console
	hStdHandle = (LONG)GetStdHandle(STD_OUTPUT_HANDLE);
	hConHandle = _open_osfhandle(hStdHandle, _O_TEXT);
	file = _fdopen(hConHandle, "w");
	setvbuf(file, NULL, _IONBF, 0);
	*stdout = *file;

	// redirect unbuffered STDIN to the console
	hStdHandle = (LONG)GetStdHandle(STD_INPUT_HANDLE);
	hConHandle = _open_osfhandle(hStdHandle, _O_TEXT);
	file = _fdopen(hConHandle, "r");
	setvbuf(stdin, NULL, _IONBF, 0);
	*stdin = *file;

	// redirect unbuffered STDERR to the console
	hStdHandle = (LONG)GetStdHandle(STD_ERROR_HANDLE);
	hConHandle = _open_osfhandle(hStdHandle, _O_TEXT);
	file = _fdopen(hConHandle, "w");
	setvbuf(stderr, NULL, _IONBF, 0);
	*stderr = *file;
#else
	// Redirect the CRT standard input, output, and error handles to the console
	freopen("CONIN$", "r", stdin);
	freopen("CONOUT$", "w", stdout);
	freopen("CONOUT$", "w", stderr);
#endif
	//Clear the error state for each of the C++ standard stream objects. We need to do this, as
	//attempts to access the standard streams before they refer to a valid target will cause the
	//iostream objects to enter an error state. In versions of Visual Studio after 2005, this seems
	//to always occur during startup regardless of whether anything has been read from or written to
	//the console or not.
	std::wcout.clear();
	std::cout.clear();
	std::wcerr.clear();
	std::cerr.clear();
	std::wcin.clear();
	std::cin.clear();

	//Small note about the ios::sync_with_stdio() function, don't bother with it. Calling this function
	//with an arg of true does nothing other than to set a state which already defaults to enabled anyway.
	//Anything else a call to that function happens to trigger is a pure accidental side-effect.
	//ios::sync_with_stdio();
#endif
}
//---------------------------------------------------------------------------

void CloseStdConsole()
{
#if defined OUTPUT_CONSOLE && defined SDK_DEBUG
#if (defined(_MSC_VER) && (_MSC_VER < 1900)) //since vc2015
#else
	fclose(stdin);
	fclose(stdout);
	fclose(stderr);
#endif
	FreeConsole();
#endif
}
//---------------------------------------------------------------------------

#ifdef USE_CUSTOM_RENDER
void _transferFrameBuffer(const struct BMPImage* pFrameBuffer, s32 offX, s32 offY, s32 frameWidth, s32 frameHeight)
{
	(void)offX;
	(void)offY;

	SDK_ASSERT(pFrameBuffer->mType == BMP_TYPE_DC16);

	if(sgDirectBitmap)
	{
		DeleteObject(sgDirectBitmap);
		sgDirectBitmap = NULL;
	}

	if(sghBmpWnd)
	{
		//http://www.codeproject.com/Articles/2841/How-to-replace-a-color-in-a-HBITMAP
		HDC BufferDC=CreateCompatibleDC(NULL);
		if (BufferDC)
		{
			HDC DirectDC = CreateCompatibleDC(NULL);
			if (DirectDC)
			{
				BITMAPINFO RGB32BitsBITMAPINFO; 
				ZeroMemory(&RGB32BitsBITMAPINFO, sizeof(BITMAPINFO));
				RGB32BitsBITMAPINFO.bmiHeader.biSize = sizeof(BITMAPINFOHEADER);
				RGB32BitsBITMAPINFO.bmiHeader.biWidth = frameWidth;
				RGB32BitsBITMAPINFO.bmiHeader.biHeight = frameHeight;
				RGB32BitsBITMAPINFO.bmiHeader.biPlanes = 1;
				RGB32BitsBITMAPINFO.bmiHeader.biBitCount = 32;
				UINT *ptPixels;    
				sgDirectBitmap = CreateDIBSection(DirectDC, 
										(BITMAPINFO *)&RGB32BitsBITMAPINFO, 
										DIB_RGB_COLORS,
										(void **)&ptPixels, 
										NULL, 0);
				if (sgDirectBitmap)
				{
					HGDIOBJ PreviousObject = SelectObject(DirectDC, sgDirectBitmap);
					BitBlt(DirectDC,0,0,
									frameWidth, frameHeight,
									BufferDC,0,0,SRCCOPY);

					{
						s32 i, j, t_h;
						u16 d1555;
						u8 *buf;
						u8 *buf8888 = (u8*)ptPixels;
						t_h = frameHeight - 1;
						for(i = 0; i < frameHeight; i++)
						{
							buf = buf8888 + frameWidth * t_h * 4;
							for(j = 0; j < frameWidth; j++)
							{
								d1555 = *(pFrameBuffer->data.mpDataDC16 + i * pFrameBuffer->mWidth2n + j);
								*(buf++) = (u8)(((((d1555 & GX_RGBA_B_MASK) >> GX_RGBA_B_SHIFT)) * 255) / 0x1f);
								*(buf++) = (u8)(((((d1555 & GX_RGBA_G_MASK) >> GX_RGBA_G_SHIFT)) * 255) / 0x1f);
								*(buf++) = (u8)(((((d1555 & GX_RGBA_R_MASK) >> GX_RGBA_R_SHIFT)) * 255) / 0x1f);
								*(buf++);
							}
							--t_h;
						}
					}
					SelectObject(DirectDC, PreviousObject);
				}
				DeleteDC(DirectDC);
			}            
			DeleteDC(BufferDC);
			if(sgDirectBitmap)
			{
				SendMessage(sghBmpWnd, STM_SETIMAGE, IMAGE_BITMAP, (LPARAM)sgDirectBitmap);
			}
		}
	}
}
#endif
//---------------------------------------------------------------------------
