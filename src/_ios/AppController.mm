/*
tengine - cross-platform game engine
------------------------------------

Copyright (c) 2013 Papa Pascualle
Contact Email: pascualle@gmail.com

Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
*/

#include "_ios/app_ios.h"
#import "AppController.h"
#import <OpenGLES/ES1/gl.h>
#import <QuartzCore/QuartzCore.h>

@implementation AppController

- (void) OnDisplay:(CADisplayLink*)displayLink
{
    IOSApp_nativeTick();
    if([mGlView BeginRender])
    {
        IOSApp_nativeRender();
        [mGlView EndRender];
	}
}

- (void) OpenURL : (NSString*)url
{
    [[UIApplication sharedApplication] openURL:[[NSURL alloc] initWithString:url]];
}

- (void) applicationDidFinishLaunching:(UIApplication*)application
{
	UIScreen* screen = [UIScreen mainScreen];
	CGRect rect = [screen applicationFrame];
	
	NSString* readPath = [NSString stringWithFormat:@"%@%@", [[NSBundle mainBundle] bundlePath], @"/"];
    IOSApp_nativeInit([readPath UTF8String]);
    
    mViewController = [[AppViewController alloc] initWithNibName:nil bundle:nil];
    mViewController.wantsFullScreenLayout = YES;
    
	mWindow = [[UIWindow alloc] initWithFrame:rect];
    mWindow.rootViewController = mViewController;
    
    mGlView = [[EAGLView alloc] InitWithFrame:rect];
    [mViewController SetMainView:mGlView];
    
	[mWindow makeKeyAndVisible];
    
	[UIApplication sharedApplication].idleTimerDisabled = YES;
	
	[self OnDisplay : 0];
    mDisplayLink = [CADisplayLink displayLinkWithTarget:self selector:@selector(OnDisplay:)];
    [mDisplayLink addToRunLoop:[NSRunLoop currentRunLoop] forMode:NSDefaultRunLoopMode];
}

- (void) dealloc
{
    IOSApp_nativeRelease();
	[mGlView release];
	[mWindow release];
	[super dealloc];
}

- (void)applicationDidReceiveMemoryWarning:(UIApplication *)application
{
    IOSApp_nativeLowMemory();
}

- (void)applicationWillTerminate:(UIApplication *)application
{
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    IOSApp_nativeStop();
    [mGlView DestroySurface];
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    [mGlView CreateSurface];
    IOSApp_nativeResume();
}

@end

@implementation AppViewController

- (void) SetMainView:(UIView *) view
{
    mView = view;
}

- (void)dealloc
{
    [super dealloc];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return UIInterfaceOrientationMaskAll;
}

-(BOOL)shouldAutorotate
{
    return YES;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.view = mView;
}

-(void)accelerometer:(UIAccelerometer *)accelerometer didAccelerate:(UIAcceleration *)acceleration
{
}

@end