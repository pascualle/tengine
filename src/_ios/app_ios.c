/*
tengine - cross-platform game engine
------------------------------------

Copyright (c) 2013 Papa Pascualle
Contact Email: pascualle@gmail.com

Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
*/

#include <stdint.h>
#include <errno.h>
#include "lib/platform_low.h"
#include "app_ios.h"
#include "texts.h"
#include "gamefield.h"
#include "filesystem.h"
#include "lib/filesystem_low.h"
#include "lib/render.h"
#include "lib/a_touchpad.h"
#include "profiler/profile.h"
#ifdef PROFILER_ENABLE
#include "static_allocator.h"
#endif

#ifdef PROFILER_ENABLE
#define PROFILER_STATIC_ALLOCATOR_HEAP_SIZE (1024 * 1024 * 4)
static struct StaticAllocator sProfilerStaticAllocator;
static u8 sStaticAllocatorHeap[PROFILER_STATIC_ALLOCATOR_HEAP_SIZE];
#endif
static struct TEnginePlatformData *gspPlatformData = NULL;
static char sgSysBundkePath[MAX_FILEPATH] = {0};

#define RES_PATH "data/"

//-----------------------------------------------------------------------

void IOSApp_nativeInit(const char* bundle_path)
{
#ifdef PROFILER_ENABLE
	StaticAllocator_Init(&sProfilerStaticAllocator, sStaticAllocatorHeap, PROFILER_STATIC_ALLOCATOR_HEAP_SIZE);
	PROFILE_INIT(sProfilerStaticAllocator);
#endif
    SDK_ASSERT(gspPlatformData == NULL);
    gspPlatformData = Platform_GetPlatformData();
    gspPlatformData->renderDevice = FALSE;
    gspPlatformData->tengineInit = FALSE;
    SDK_NULL_ASSERT(bundle_path);
    SDK_ASSERT(STD_StrLen(bundle_path) < MAX_FILEPATH);
    STD_StrCpy(sgSysBundkePath, bundle_path);
    STD_StrCat(sgSysBundkePath, RES_PATH);
    Platform_InitTick();
}
//-----------------------------------------------------------------------

void IOSApp_nativeCreateRenderDevice()
{
    OS_Printf("app restore res data\n");
    Platform_restoreRenderDevice();
}
//-----------------------------------------------------------------------

void IOSApp_nativeStop()
{
	OS_Printf("app lost render device\n");
    Platform_lostRenderDevice();
}
//-----------------------------------------------------------------------

void IOSApp_nativeRelease()
{
    SDK_NULL_ASSERT(gspPlatformData);
    if(gspPlatformData->tengineInit == TRUE)
    {
        tfgRelease();
        ReleaseFileSystem();
        glRender_Release();
    }
    gspPlatformData->tengineInit = FALSE;
}
//-----------------------------------------------------------------------

void IOSApp_nativePause()
{
	IOSApp_nativeStop();
}
//-----------------------------------------------------------------------

void IOSApp_nativeResume()
{
    IOSApp_nativeCreateRenderDevice();
}
//-----------------------------------------------------------------------

void IOSApp_nativeLowMemory()
{
    OS_Printf("app low memory signal");
    tfgLowMemory();
}
//-----------------------------------------------------------------------

void IOSApp_nativeResize(int w, int h)
{
    if(gspPlatformData->tengineInit)
    {
        tfgResize(w, h);
        glRender_Resize(w, h);
    }
    else if(gspPlatformData->tengineInit == FALSE)
    {
        struct InitFileSystemData fileSystemData;
            
        tfgInitMemory();
        glRender_Init();
            
        SDK_ASSERT(sgSysBundkePath[0] != '\0');
        fileSystemData.mpPath = sgSysBundkePath;
        InitFileSystem(&fileSystemData);
            
        tfgInit();
        tfgResize(w, h);
        OS_Printf("init tengine display with size: %d %d\n", w, h);
        glRender_Resize(w, h);
            
        gspPlatformData->tengineInit = TRUE;
	}
}
//-----------------------------------------------------------------------

void IOSApp_nativeTouchPadDown(int ptid, float x, float y)
{
	onTouchPadDown((u8)ptid, (u16)x, (u16)y);
}
//-----------------------------------------------------------------------

void IOSApp_nativeTouchPadUp(int ptid, float x, float y)
{
	onTouchPadUp((u8)ptid, (u16)x, (u16)y);
}
//-----------------------------------------------------------------------

void IOSApp_nativeTouchPadMove(int ptid, float x, float y)
{
	onTouchPadMove((u8)ptid, (u16)x, (u16)y);
}
//-----------------------------------------------------------------------

void IOSApp_nativeKeyDown(int keyCode)
{
	(void)keyCode;
}
//-----------------------------------------------------------------------

void IOSApp_nativeKeyUp(int keyCode)
{
	(void)keyCode;
}
//-----------------------------------------------------------------------

void IOSApp_nativeTick()
{
    PROFILE_START;
    Platform_Tick();
    PROFILE_FINISH;
}
//-----------------------------------------------------------------------

void IOSApp_nativeRender()
{
	PROFILE_START;
    SDK_NULL_ASSERT(gspPlatformData);
	if(gspPlatformData->renderDevice)
	{
        glRender_DrawFrame();
	}
	PROFILE_FINISH;
}
//-----------------------------------------------------------------------
