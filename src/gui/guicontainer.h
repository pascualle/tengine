#ifndef GUI_CONTAINER_H
#define GUI_CONTAINER_H

#ifdef __cplusplus
extern "C" {
#endif

#include "guitypes.h"
#include "touchpad.h"

typedef void (*OnGUIEvent)(const struct GUIEvent* event);

struct GUIContainer
{
	struct GUIObject2D** mppObjects;
	struct GUIObject2D* mpFocused;
	struct GUIObject2D* mpTrigged[TOUCH_POINTS_MAX];
	struct GUIObject2D* mpParent;
	OnGUIEvent mpEventHandler;
	s32 mMaxCapasity;
	s32 mSize;    
	u32 mLayerNo;
	BOOL mDPADIsActive;
};

//---------------------------------------------------------------------------

void GUIContainer_Init(struct GUIContainer *container, u32 layer, s32 maxCapasity);

void GUIContainer_Release(struct GUIContainer *container);

void GUIContainer_FreeAll(struct GUIContainer *container);


void GUIContainer_SetGUIEventHandler(struct GUIContainer *container, OnGUIEvent handler);

u32 GUIContainer_GetLayer(struct GUIContainer *container);


struct GUIObject2D* GUIContainer_GetObject(struct GUIContainer *container, s32 index);

s32 GUIContainer_GetObjectsCount(struct GUIContainer *container);


void GUIContainer_ProcessInput(struct GUIContainer *container, const struct TouchPadData *tpData);

void GUIContainer_Update(struct GUIContainer *container, s32 ms);


void GUIContainer_SetFocus(struct GUIContainer *container, struct GUIObject2D* obj); 

const struct GUIObject2D* GUIContainer_GetFocus(struct GUIContainer *container);


BOOL GUIContainer_FindFirstByTabOrder(struct GUIContainer *container);

BOOL GUIContainer_FindLastByTabOrder(struct GUIContainer *container);
 
BOOL GUIContainer_FindNextByTabOrder(struct GUIContainer *container);

BOOL GUIContainer_FindPrevByTabOrder(struct GUIContainer *container);

//---------------------------------------------------------------------------

#ifdef __cplusplus
} /* extern "C" */
#endif

#endif
