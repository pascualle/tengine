#ifndef GUI_HEADER_H
#define GUI_HEADER_H

#ifdef __cplusplus
extern "C" {
#endif

#include "guicontainer.h"
#include "gamepad.h"

//---------------------------------------------------------------------------

typedef s32 (*go2dGetTabOrder)(struct GUIObject2D *obj);
typedef void (*go2dProcessInput)(struct GUIObject2D *obj, struct TouchPadData const *tpData);
typedef void (*go2dSetEnable)(struct GUIObject2D *obj, BOOL state);
typedef BOOL (*go2dIsEnable)(struct GUIObject2D *obj);
typedef void (*go2dSetFocusFlag)(struct GUIObject2D *obj, BOOL state);
typedef void (*go2dUpdate)(struct GUIObject2D *obj, s32 ms);

//---------------------------------------------------------------------------

struct GUIObject2D
{
	go2dGetTabOrder mpGetTabOrder;
	go2dProcessInput mpProcessInput;
	go2dSetEnable mpSetEnable;
	go2dIsEnable mpIsEnable;
	go2dSetFocusFlag mpSetFocusFlag;
	go2dUpdate mpUpdate;
	struct GUIContainer *mpParent;
	enum GUIControlType mType;
	s32 mMapObjID;
	void* mpObject;
};

void GUIObject2D_protected_Init(struct GUIObject2D *self);
void GUIObject2D_protected_Release(struct GUIObject2D *self);
void GUIObject2D_protected_SetFocusFlag(struct GUIObject2D *self, BOOL state);
void GUIObject2D_protected_ProcessInput(struct GUIObject2D *self, struct TouchPadData const *tpData);

//---------------------------------------------------------------------------

typedef void (*gbbPress)(struct GUIBaseButton *obj);
typedef void (*gbbRelease)(struct GUIBaseButton *obj);
typedef void (*gbbSetEnableInput)(struct GUIBaseButton *obj, BOOL val);

struct GUIBaseButton 
{
	struct GUIObject2D guiObject;
	gbbPress mpButtonPress;
	gbbRelease mpButtonRelease;
	gbbSetEnableInput mpSetEnableInput;
	const struct GUIBaseButtonInitParameters *mpBaseParams;
    enum GUIStateType mState;
	enum GUIPointerStateType mPointerState;
	struct GUIAction mAction;
	BOOL mEnableInput;
	s32 mTouchPointerId;
};

void GUIBaseButton_protected_Init(struct GUIBaseButton *self);
void GUIBaseButton_protected_Release(struct GUIBaseButton *self);
void GUIBaseButton_protected_ButtonPress(struct GUIBaseButton *self, BOOL sendEvent);
void GUIBaseButton_protected_ButtonRelease(struct GUIBaseButton *self, BOOL sendEvent);

//---------------------------------------------------------------------------

struct GUISpeedButton 
{
	struct GUIBaseButton baseButton;
	const struct GUISpeedButtonInitParameters* mpParams;
	s32 mTxtMapPbjId;
    enum KeyType mHotKey;
	BOOL mHotkeyDown;
};

void GUISpeedButton_protected_Init(struct GUISpeedButton *self, const struct GUISpeedButtonInitParameters* params, s32 mapObjID, struct GUIContainer* parent);
void GUISpeedButton_protected_Release(struct GUISpeedButton *self);
void GUISpeedButton_protected_SetTextMapObjId(struct GUISpeedButton *self, s32 id);

//---------------------------------------------------------------------------

struct GUIButton
{
	struct GUIBaseButton baseButton;
	const struct GUIButtonInitParameters* mpParams;
	s32 mTxtMapPbjId;
	s32 mFocusMapObjId;
};

void GUIButton_protected_Init(struct GUIButton *self, const struct GUIButtonInitParameters* params, s32 mapObjID, struct GUIContainer* parent);
void GUIButton_protected_Release(struct GUIButton *self);
void GUIButton_protected_SetTextMapObjId(struct GUIButton *self, s32 id);
void GUIButton_protected_SetFocusMapObjId(struct GUIButton *self, s32 id);

//---------------------------------------------------------------------------

struct GUISlider;
struct StaticAllocator;
struct AllocatorArray;

typedef void (*gosUpdateAllItemsPositions)(struct GUISlider* self);

struct GUISlider
{
	struct GUIObject2D guiObject;
	const struct GUISliderInitParameters* mpParams;
	struct GUISpeedButtonInitParameters **mppItemParams;
	struct GUIAction mAction;
	struct GUIContainer mContainer;
	struct StaticAllocator *mpPoolAllocator;
	struct AllocatorArray *mpArray;
	u8* mpPool;
	gosUpdateAllItemsPositions mpUpdatePosFn;
	s32 mMapObjId[SLIDER_MAPOBJ_ID_COUNT];
	s32 mItemCursorIndex;
	s32 mTimer;
	s32 mSpeed;
	fx32 mSlideProcess;
	s32 mSlideDirection;
	BOOL mLongDelay;
	fx32 mItem_width_koef;
	fx32 mItemWidth;
	fx32 mItem_height_koef;
	fx32 mItemHeight;
	BOOL mEnableInput;
	BOOL mReset;
};

void GUISlider_protected_Init(struct GUISlider *self, const struct GUISliderInitParameters* params, s32 mapObjID, struct GUIContainer* parent);
void GUISlider_protected_Release(struct GUISlider *self);
void GUISlider_protected_SetContainerId(struct GUISlider *self, s32 id);

//---------------------------------------------------------------------------

void GUIContainer_protected_Add(struct GUIContainer *container, struct GUIObject2D* obj);
const struct GUIObject2D* GUIContainer_protected_GetTrigged(struct GUIContainer *container, s32 touchPointerId);
void GUIContainer_protected_SetTrigged(struct GUIContainer *container, struct GUIObject2D* obj, s32 touchPointerId);
void GUIContainer_protected_SwapTriggedPointers(struct GUIContainer *container, s32 oldTouchPointerId, s32 newTouchPointerId);

//---------------------------------------------------------------------------

#ifdef __cplusplus
} /* extern "C" */
#endif

#endif
