/*
tengine - cross-platform game engine
------------------------------------

Copyright (c) 2013 Papa Pascualle
Contact Email: pascualle@gmail.com

Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
*/

#include "touchpad.h"
#include "gamepad.h"
#include "lib/tengine_low.h"
#include "guicontainer.h"
#include "guiobject2D.h"
#include "guibutton.h"
#include "gui_low.h"

//---------------------------------------------------------------------------

void GUIContainer_Release(struct GUIContainer *container)
{
	GUIContainer_FreeAll(container);  
}
//---------------------------------------------------------------------------

void GUIContainer_Init(struct GUIContainer *container, u32 layer, s32 maxCapasity)
{
    GUIContainer_FreeAll(container);
    container->mMaxCapasity = maxCapasity;
	container->mppObjects = (struct GUIObject2D**)MALLOC(container->mMaxCapasity * sizeof(struct GUIObject2D*), "guicontainetinit::mppObjects");
	container->mLayerNo = layer;
}
//---------------------------------------------------------------------------

u32 GUIContainer_GetLayer(struct GUIContainer *container)
{
	return container->mLayerNo;
}
//---------------------------------------------------------------------------

void GUIContainer_ProcessInput(struct GUIContainer *container, const struct TouchPadData *tpData)
{                       
    if(container->mppObjects == NULL)
    {
        return;
    }

    container->mDPADIsActive = FALSE; 

	if(getVisible(GUIContainer_GetLayer(container)) == FALSE)
	{
		return;
	}

    if(IsButtonDown(TYPE_A) || IsButtonPress(TYPE_A))
    {
        if(container->mpFocused != NULL)
        {
            if(GUIObject2D_GetType(container->mpFocused) == GUI_TYPE_BUTTON)
            {
                container->mDPADIsActive = TRUE;
				GUIBaseButton_protected_ButtonPress(&((struct GUIButton*)container->mpFocused)->baseButton, FALSE);
            }
        }
    }
    else
    if(IsButtonUp(TYPE_A))
    {
        if(container->mpFocused != NULL)
        {
            if(GUIObject2D_GetType(container->mpFocused) == GUI_TYPE_BUTTON)
            {
                container->mDPADIsActive = TRUE; 
                GUIButton_Click((struct GUIButton*)container->mpFocused);
                return;
            }
        }
    }
                
    if(IsButtonDown(TYPE_UP))
    {
        GUIContainer_FindPrevByTabOrder(container);
    }
    else
    if(IsButtonDown(TYPE_DOWN))
    {
        GUIContainer_FindNextByTabOrder(container);
    }
    
    if(!container->mDPADIsActive)
    {
        s32 i = container->mSize;
		while(--i >= 0)
        {	
			GUIObject2D_protected_ProcessInput(container->mppObjects[i], tpData);
        }
    }    
}
//---------------------------------------------------------------------------
  
void GUIContainer_FreeAll(struct GUIContainer *container)
{
    s32 i;
	container->mpFocused = NULL;
	for(i = 0; i < TOUCH_POINTS_MAX; i++)
	{
		container->mpTrigged[i] = NULL;
	}
    container->mDPADIsActive = FALSE;
    if(container->mppObjects != NULL)
    {
		while(container->mSize > 0)
        {
            container->mSize--;
			switch(GUIObject2D_GetType(container->mppObjects[container->mSize]))
			{
				case GUI_TYPE_NONE:
					SDK_ASSERT(0);
				break;
				case GUI_TYPE_BUTTON:
					GUIButton_protected_Release((struct GUIButton*)container->mppObjects[container->mSize]);				
				break;
				case GUI_TYPE_SPEEDBUTTON:
					GUISpeedButton_protected_Release((struct GUISpeedButton*)container->mppObjects[container->mSize]);
				break;
				case GUI_TYPE_SLIDER:
					GUISlider_protected_Release((struct GUISlider*)container->mppObjects[container->mSize]);
			}
			FREE(container->mppObjects[container->mSize]);
        }
        FREE(container->mppObjects);
    }
    container->mppObjects = NULL;
	container->mSize = 0;
	container->mMaxCapasity = 0;
}
//---------------------------------------------------------------------------

BOOL GUIContainer_FindFirstByTabOrder(struct GUIContainer *container)
{
    if(container->mppObjects != NULL)
    {
        struct GUIObject2D* obj, *tobj;
		s32 i, min = 1000000;
		obj = NULL;
        for(i = 0; i < container->mSize; i++)
        {
            tobj = container->mppObjects[i];
			if(min > GUIObject2D_GetTabOrder(tobj) && GUIObject2D_GetTabOrder(tobj) > 0)
            {
                min = GUIObject2D_GetTabOrder(tobj);
                obj = tobj;
            }
        }
        if(obj != NULL)
        {
            GUIContainer_SetFocus(container, obj);
            return TRUE;
        }
    }
    return FALSE;
}
//---------------------------------------------------------------------------

void GUIContainer_Update(struct GUIContainer *container, s32 ms)
{
    if(container->mppObjects != NULL)
    {
        s32 i;
        for(i = 0; i < container->mSize; i++)
        {
            GUIObject2D_Update(container->mppObjects[i], ms);
        }
    }
}
//---------------------------------------------------------------------------

BOOL GUIContainer_FindLastByTabOrder(struct GUIContainer *container)
{
    if(container->mppObjects != NULL)
    {
        struct GUIObject2D* obj, *tobj;
		s32 i, max = 0;
        obj = NULL;
        for(i = 0; i < container->mSize; i++)
        {
            tobj = container->mppObjects[i];
			if(max < GUIObject2D_GetTabOrder(tobj) && GUIObject2D_GetTabOrder(tobj) > 0)
            {
                max = GUIObject2D_GetTabOrder(tobj);
                obj = tobj;
            }
        }
        if(obj != NULL)
        {
            GUIContainer_SetFocus(container, obj);
            return TRUE;
        }
    }
    return FALSE;
}
//---------------------------------------------------------------------------

BOOL GUIContainer_FindNextByTabOrder(struct GUIContainer *container)
{
    if(container->mppObjects == NULL)
    {
        return FALSE;
    }

    if(container->mpFocused != NULL)
    {
        struct GUIObject2D* obj, *tobj;
		s32 i, min = 1000000;
		s32 current = GUIObject2D_GetTabOrder(container->mpFocused);
		obj = NULL;
        for(i = 0; i < container->mSize; i++)
        {
            tobj = container->mppObjects[i];
			if(current < GUIObject2D_GetTabOrder(tobj) && 
                min > GUIObject2D_GetTabOrder(tobj) &&      
                GUIObject2D_GetTabOrder(tobj) > 0)
            {
                min = GUIObject2D_GetTabOrder(tobj);
                obj = tobj;
            }
        }
        if(obj != NULL)
        {
            GUIContainer_SetFocus(container, obj);
            return TRUE;
        }
        else
        {
            return GUIContainer_FindFirstByTabOrder(container); 
        }
    }
    else
    {
        return GUIContainer_FindFirstByTabOrder(container); 
    }
}
//---------------------------------------------------------------------------

BOOL GUIContainer_FindPrevByTabOrder(struct GUIContainer *container)
{
    if(container->mppObjects == NULL)
    {
        return FALSE;
    }
    
    if(container->mpFocused != NULL)
    {
        s32 i, min = 0;
		struct GUIObject2D* obj = NULL;
		s32 current = GUIObject2D_GetTabOrder(container->mpFocused);
        for(i = 0; i < container->mSize; i++)
        {
            if(current > GUIObject2D_GetTabOrder(container->mppObjects[i]) &&
                min < GUIObject2D_GetTabOrder(container->mppObjects[i]) &&      
                GUIObject2D_GetTabOrder(container->mppObjects[i]) > 0)
            {
                min = GUIObject2D_GetTabOrder(container->mppObjects[i]);
                obj = container->mppObjects[i];
            }
        }
        if(obj != NULL)
        {
            GUIContainer_SetFocus(container, obj);
            return TRUE;
        }
        else
        {
            return GUIContainer_FindLastByTabOrder(container); 
        }
    }
    else
    {
        return GUIContainer_FindLastByTabOrder(container); 
    }
}
//---------------------------------------------------------------------------

s32 GUIContainer_GetObjectsCount(struct GUIContainer *container)
{
    return container->mSize;
}
//---------------------------------------------------------------------------

struct GUIObject2D* GUIContainer_GetObject(struct GUIContainer *container, s32 index)
{
    SDK_NULL_ASSERT(container->mppObjects); //please init GUIManager
    SDK_ASSERT(index < container->mSize && index >= 0);
    return container->mppObjects[index];
}
//---------------------------------------------------------------------------

void GUIContainer_protected_Add(struct GUIContainer *container, struct GUIObject2D* obj)
{
    SDK_ASSERT(container->mSize < container->mMaxCapasity);
    SDK_NULL_ASSERT(container->mppObjects); //please init GUIManager
    container->mppObjects[container->mSize] = obj;
    container->mSize++;
}
//---------------------------------------------------------------------------

void GUIContainer_SetFocus(struct GUIContainer *container, struct GUIObject2D* obj)
{
    SDK_NULL_ASSERT(container->mppObjects); //please init GUIManager
    if(container->mpFocused != NULL)
    {
        GUIObject2D_protected_SetFocusFlag(container->mpFocused, FALSE);
        if(container->mDPADIsActive)
        {
			TouchPadData d;
			MI_CpuClear8(&d, sizeof(TouchPadData));
			GUIObject2D_protected_ProcessInput(container->mpFocused, &d);
        }
    }
    container->mpFocused = obj;
    if(container->mpFocused != NULL)
    {
        s32 i;
		for(i = 0; i < container->mSize; ++i)
        {
            if(container->mppObjects[i] == container->mpFocused)
            {
				GUIObject2D_protected_SetFocusFlag(container->mppObjects[i], TRUE);
                return;
            }
        }
    }
    container->mpFocused = NULL;
} 
//---------------------------------------------------------------------------

const struct GUIObject2D* GUIContainer_GetFocus(struct GUIContainer *container)
{
    return container->mpFocused;
}
//---------------------------------------------------------------------------

void GUIContainer_protected_SwapTriggedPointers(struct GUIContainer *container, s32 oldTouchPointerId, s32 newTouchPointerId)
{
	if(oldTouchPointerId >= 0)
	{
		SDK_ASSERT(newTouchPointerId >= 0);
		container->mpTrigged[newTouchPointerId] = container->mpTrigged[oldTouchPointerId];
	}
}
//---------------------------------------------------------------------------

const struct GUIObject2D* GUIContainer_protected_GetTrigged(struct GUIContainer *container, s32 touchPointerId)
{
	if(touchPointerId >= 0)
	{
		return container->mpTrigged[touchPointerId];
	}
	return NULL;
}
//---------------------------------------------------------------------------

void GUIContainer_protected_SetTrigged(struct GUIContainer *container, struct GUIObject2D* obj, s32 touchPointerId)
{
	if(touchPointerId >= 0)
	{
		container->mpTrigged[touchPointerId] = obj;
	}
} 
//---------------------------------------------------------------------------

void GUIContainer_SetGUIEventHandler(struct GUIContainer *container, OnGUIEvent handler)
{
    container->mpEventHandler = handler;
}
//---------------------------------------------------------------------------
