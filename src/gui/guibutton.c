/*
tengine - cross-platform game engine
------------------------------------

Copyright (c) 2013 Papa Pascualle
Contact Email: pascualle@gmail.com

Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
*/

#include "lib/tengine_low.h"
#include "touchpad.h"
#include "guibutton.h"
#include "guicontainer.h"
#include "guifactory.h"
#include "gui_low.h"
#include "fxmath.h"

//---------------------------------------------------------------------------

void _GUIButton_GUIObject2D_SetFocusFlag(struct GUIObject2D *obj, BOOL state);
void _GUIButton_GUIObject2D_ProcessInput(struct GUIObject2D *obj, const TouchPadData *tpData);
s32 _GUIButton_GUIObject2D_GetTabOrder(struct GUIObject2D *obj);
void _GUIButton_GUIObject2D_SetEnable(struct GUIObject2D *obj, BOOL state);
void _GUIButton_GUIBaseButton_ButtonPress(struct GUIBaseButton *obj);
void _GUIButton_GUIBaseButton_ButtonRelease(struct GUIBaseButton *obj);
void _GUIButton_GUIBaseButton_SetEnableInput(struct GUIBaseButton *obj, BOOL val);

//---------------------------------------------------------------------------

void GUIButton_protected_Init(struct GUIButton *self, const struct GUIButtonInitParameters* params, s32 mapObjID, struct GUIContainer* parent)
{
	SDK_ASSERT(mapObjID >= 0);

	GUIBaseButton_protected_Init(&self->baseButton);
	
	self->baseButton.guiObject.mType = GUI_TYPE_BUTTON;
	self->baseButton.guiObject.mpObject = self;

    self->mpParams = params;
    SDK_NULL_ASSERT(self->mpParams);
	SDK_ASSERT(self->mpParams->baseInitParameters.mapObjTypeID[BUTTON_MAPOBJ_ID_IDLE] != -1);
    self->baseButton.mpBaseParams = &self->mpParams->baseInitParameters;

	self->baseButton.mAction.mType = AT_DO_NOTHING;
	self->baseButton.guiObject.mMapObjID = mapObjID;
	self->baseButton.guiObject.mpParent = parent;
	self->mTxtMapPbjId = self->mFocusMapObjId = -1;

	self->baseButton.guiObject.mpProcessInput = _GUIButton_GUIObject2D_ProcessInput;
	self->baseButton.guiObject.mpGetTabOrder = _GUIButton_GUIObject2D_GetTabOrder;
	self->baseButton.guiObject.mpSetEnable = _GUIButton_GUIObject2D_SetEnable;
	self->baseButton.guiObject.mpSetFocusFlag = _GUIButton_GUIObject2D_SetFocusFlag;
	self->baseButton.guiObject.mpUpdate = NULL;
	self->baseButton.mTouchPointerId = -1;

	self->baseButton.mpButtonPress = _GUIButton_GUIBaseButton_ButtonPress;
	self->baseButton.mpButtonRelease = _GUIButton_GUIBaseButton_ButtonRelease;
	self->baseButton.mpSetEnableInput = _GUIButton_GUIBaseButton_SetEnableInput;

	prChangeTypeLayer(self->baseButton.guiObject.mMapObjID, self->mpParams->baseInitParameters.mapObjTypeID[BUTTON_MAPOBJ_ID_IDLE], GUIContainer_GetLayer(parent));
}
//---------------------------------------------------------------------------

void GUIButton_protected_Release(struct GUIButton *self)
{
	GUIBaseButton_protected_Release(&self->baseButton);
	MI_CpuClear8(self, sizeof(struct GUIButton));
}
//---------------------------------------------------------------------------

s32 GUIButton_GetMapObjId(const struct GUIButton *self)
{
	return GUIObject2D_GetMapObjId(&self->baseButton.guiObject);
}
//---------------------------------------------------------------------------

s32 GUIButton_GetTabOrder(const struct GUIButton *self)
{
	return GUIObject2D_GetTabOrder(&self->baseButton.guiObject);
}
//---------------------------------------------------------------------------

void GUIButton_SetEnable(struct GUIButton *self, BOOL value)
{
	GUIObject2D_SetEnable(&self->baseButton.guiObject, value);
}
//---------------------------------------------------------------------------

void GUIButton_Click(struct GUIButton *self)
{
	GUIBaseButton_Click(&self->baseButton);
}
//---------------------------------------------------------------------------

BOOL GUIButton_IsEnable(const struct GUIButton *self)
{
	return GUIObject2D_IsEnable(&self->baseButton.guiObject);
}
//---------------------------------------------------------------------------

void GUIButton_SetAction(struct GUIButton *self, const struct GUIAction *action)
{
	GUIBaseButton_SetAction(&self->baseButton, action);
}
//---------------------------------------------------------------------------

struct GUIAction GUIButton_GetAction(const struct GUIButton *self)
{
	return GUIBaseButton_GetAction(&self->baseButton);
}
//---------------------------------------------------------------------------

void GUIButton_SetPosition(struct GUIButton *self, fx32 x, fx32 y)
{
	GUIObject2D_SetPosition(&self->baseButton.guiObject, x, y);
}
//---------------------------------------------------------------------------

void GUIButton_GetPosition(const struct GUIButton *self, fx32 *x, fx32 *y)
{
	GUIObject2D_GetPosition(&self->baseButton.guiObject, x, y);
}
//---------------------------------------------------------------------------

void GUIButton_GetCollideRect(const struct GUIButton *self, fx32 rect[RECT_SIZE])
{
	GUIBaseButton_GetCollideRect(&self->baseButton, rect);
}
//---------------------------------------------------------------------------

void GUIButton_protected_SetFocusMapObjId(struct GUIButton *self, s32 id)
{
	self->mFocusMapObjId = id;
}
//---------------------------------------------------------------------------

void GUIButton_SetTextMapObjId(struct GUIButton *self, s32 id)
{
	self->mTxtMapPbjId = id;
}
//---------------------------------------------------------------------------

void GUIButton_protected_SetTextMapObjId(struct GUIButton *self, s32 id)
{
	self->mTxtMapPbjId = id;
}
//---------------------------------------------------------------------------

s32 _GUIButton_GUIObject2D_GetTabOrder(struct GUIObject2D *obj)
{
	struct GUIButton *self = (struct GUIButton *)obj;
	SDK_NULL_ASSERT(self->mpParams);
	if(self->mpParams->tabOrderPrpId >= 0)
	{
		return fx2int(prGetPropertyLayer(GUIObject2D_GetMapObjId(&self->baseButton.guiObject), self->mpParams->tabOrderPrpId, GUIContainer_GetLayer(self->baseButton.guiObject.mpParent)));
	}
	return -1;
}  
//---------------------------------------------------------------------------

void _GUIButton_GUIObject2D_SetFocusFlag(struct GUIObject2D *obj, BOOL state)
{
	struct GUIButton *self = (struct GUIButton *)obj;
	if(self->mFocusMapObjId >= 0)
    {
		SDK_NULL_ASSERT(self->mpParams);
		SDK_ASSERT(self->mpParams->baseInitParameters.enabledPrpId != -1);
		prSetPropertyLayer(self->mFocusMapObjId, self->mpParams->baseInitParameters.enabledPrpId, FX32((char)state), GUIContainer_GetLayer(self->baseButton.guiObject.mpParent));
    }  
}
//---------------------------------------------------------------------------

void _GUIButton_GUIBaseButton_ButtonPress(struct GUIBaseButton *obj)
{
	struct GUIButton *self = (struct GUIButton *)obj;
	if(GUIButton_IsEnable(self))
	{
		SDK_NULL_ASSERT(self->mpParams);
		self->baseButton.mState = ST_PRESSED;
		if(self->mpParams->baseInitParameters.mapObjTypeID[BUTTON_MAPOBJ_ID_PRESSED] != -1)
		{
			prChangeTypeLayer(GUIObject2D_GetMapObjId(&self->baseButton.guiObject),
					self->mpParams->baseInitParameters.mapObjTypeID[BUTTON_MAPOBJ_ID_PRESSED],
					GUIContainer_GetLayer(self->baseButton.guiObject.mpParent));
		}
	}
}
//---------------------------------------------------------------------------

void _GUIButton_GUIBaseButton_ButtonRelease(struct GUIBaseButton *obj)
{
	struct GUIButton *self = (struct GUIButton *)obj;
	if(self->baseButton.mState == ST_PRESSED)
	{
		SDK_NULL_ASSERT(self->mpParams);
		self->baseButton.mState = ST_NONE;
		SDK_ASSERT(self->mpParams->baseInitParameters.mapObjTypeID[BUTTON_MAPOBJ_ID_IDLE] != -1);
		prChangeTypeLayer(GUIObject2D_GetMapObjId(&self->baseButton.guiObject), 
			self->mpParams->baseInitParameters.mapObjTypeID[BUTTON_MAPOBJ_ID_IDLE],
			GUIContainer_GetLayer(self->baseButton.guiObject.mpParent));
	}
}
//---------------------------------------------------------------------------

void _GUIButton_GUIObject2D_SetEnable(struct GUIObject2D *obj, BOOL state)
{
	u8 i;
	struct GUIButton *self = (struct GUIButton *)obj;
	SDK_NULL_ASSERT(self->mpParams);
	SDK_ASSERT(self->mpParams->baseInitParameters.enabledPrpId != -1);
	prSetPropertyLayer(GUIObject2D_GetMapObjId(&self->baseButton.guiObject),
			self->mpParams->baseInitParameters.enabledPrpId,
			FX32((char)state),
			GUIContainer_GetLayer(self->baseButton.guiObject.mpParent));
	for(i = 0; i < 3; i++)
	{
		s32 ch_idx = prGetJoinedChildLayer(GUIObject2D_GetMapObjId(&self->baseButton.guiObject), i, 
										GUIContainer_GetLayer(self->baseButton.guiObject.mpParent));
		if(NONE_MAP_IDX != ch_idx)
		{
			prSetPropertyLayer(ch_idx,
					self->mpParams->baseInitParameters.enabledPrpId,
					FX32((char)state),
					GUIContainer_GetLayer(self->baseButton.guiObject.mpParent));
		}
	}
	self->baseButton.mPointerState = PST_UP;
	if(self->baseButton.mState != ST_NONE)
    {
		self->baseButton.mState  = ST_NONE;
		SDK_ASSERT(self->mpParams->baseInitParameters.mapObjTypeID[BUTTON_MAPOBJ_ID_IDLE] != -1);
		prChangeTypeLayer(GUIObject2D_GetMapObjId(&self->baseButton.guiObject),
			self->mpParams->baseInitParameters.mapObjTypeID[BUTTON_MAPOBJ_ID_IDLE],
			GUIContainer_GetLayer(self->baseButton.guiObject.mpParent));
    }
	if(GUIContainer_protected_GetTrigged(self->baseButton.guiObject.mpParent, self->baseButton.mTouchPointerId) == &self->baseButton.guiObject)
    {
		GUIContainer_protected_SetTrigged(self->baseButton.guiObject.mpParent, NULL, self->baseButton.mTouchPointerId);
    }
	if(GUIContainer_GetFocus(self->baseButton.guiObject.mpParent) == &self->baseButton.guiObject)
    {
		GUIContainer_FindNextByTabOrder(self->baseButton.guiObject.mpParent);
    }
}
//---------------------------------------------------------------------------
   
void _GUIButton_GUIObject2D_ProcessInput(struct GUIObject2D *obj, const TouchPadData *tpData)
{
	s32 stp, tp;
	struct fxVec2 rect[4];
	fx32 r[RECT_SIZE];
	BOOL tpfound;
	struct GUIButton *bb = (struct GUIButton*)obj;
	
	if(!GUIObject2D_IsEnable(&bb->baseButton.guiObject) || !bb->baseButton.mEnableInput)
    {
        return;
    }

	GUIButton_GetCollideRect(bb, r);
	rect[0].x = r[RECT_LEFT_TOP_X];
	rect[0].y = r[RECT_LEFT_TOP_Y];
	rect[1].x = r[RECT_RIGHT_TOP_X];
	rect[1].y = r[RECT_RIGHT_TOP_Y];
	rect[2].x = r[RECT_RIGHT_BOTTOM_X];
	rect[2].y = r[RECT_RIGHT_BOTTOM_Y];
	rect[3].x = r[RECT_LEFT_BOTTOM_X];
	rect[3].y = r[RECT_LEFT_BOTTOM_Y];
	stp = tp = bb->baseButton.mTouchPointerId;
	tpfound = FALSE;
	if(tp < 0)
	{
		stp = tp = 0;
	}
	do
	{
		if(tpData->point[tp].mTouch == TRUE)
		{
			s32 vt, vl, vw, vh;
			s32 layer = (s32)GUIContainer_GetLayer(bb->baseButton.guiObject.mpParent);
			vt = getViewTop(layer);
			vl = getViewLeft(layer);
			vw = getViewWidth(layer);
			vh = getViewHeight(layer);
			if(tpData->point[tp].mX > vl && tpData->point[tp].mY > vt &&
					tpData->point[tp].mX < vl + vw && tpData->point[tp].mY < vt + vh)
			{
				fx32 scale;
				struct fxVec2 point;
				scale = getRenderPlaneScale(layer);
				point.x = FX_Div(FX32(tpData->point[tp].mX - vl), scale) + getViewOffsetX(layer);
				point.y = FX_Div(FX32(tpData->point[tp].mY - vt), scale) + getViewOffsetY(layer);
				if(mthIntersectPointAndRectangle(point, rect))
				{
					tpfound = TRUE;
					bb->baseButton.mPointerState = tpData->point[tp].mTrg ? PST_ENTER : PST_DOWN;
					GUIContainer_protected_SwapTriggedPointers(bb->baseButton.guiObject.mpParent, bb->baseButton.mTouchPointerId, tp);
					bb->baseButton.mTouchPointerId = tp;
					break;
				}
			}
		}
		if(++tp == TOUCH_POINTS_MAX)
		{
			tp = 0;
		}
	}
	while(stp != tp);

	if(bb->baseButton.mTouchPointerId >= 0)
	{
		if(tpData->point[bb->baseButton.mTouchPointerId].mTouch == TRUE)
		{
				if(tpfound)
				{
					const struct GUIObject2D* pobj = GUIContainer_protected_GetTrigged(bb->baseButton.guiObject.mpParent, bb->baseButton.mTouchPointerId);
					if((pobj == NULL && tpData->point[bb->baseButton.mTouchPointerId].mTrg == TRUE) || pobj == &bb->baseButton.guiObject)
					{
						GUIBaseButton_protected_ButtonPress(&bb->baseButton, FALSE);
						GUIContainer_SetFocus(bb->baseButton.guiObject.mpParent, &bb->baseButton.guiObject);
						GUIContainer_protected_SetTrigged(bb->baseButton.guiObject.mpParent, &bb->baseButton.guiObject, bb->baseButton.mTouchPointerId);
					}
				}
				else
				{
					if(bb->baseButton.mState == ST_PRESSED)
					{
						SDK_NULL_ASSERT(bb->mpParams);
						bb->baseButton.mState = ST_NONE;
						SDK_ASSERT(bb->mpParams->baseInitParameters.mapObjTypeID[BUTTON_MAPOBJ_ID_IDLE] != -1);
						prChangeTypeLayer(GUIObject2D_GetMapObjId(&bb->baseButton.guiObject),
										bb->mpParams->baseInitParameters.mapObjTypeID[BUTTON_MAPOBJ_ID_IDLE],
										GUIContainer_GetLayer(bb->baseButton.guiObject.mpParent));
					}
				}
		}
		else
		{
			BOOL isPressed = bb->baseButton.mState == ST_PRESSED;
			GUIBaseButton_protected_ButtonRelease(&bb->baseButton, FALSE);
			if(GUIContainer_protected_GetTrigged(bb->baseButton.guiObject.mpParent, bb->baseButton.mTouchPointerId) == &bb->baseButton.guiObject)
			{
				GUIContainer_protected_SetTrigged(bb->baseButton.guiObject.mpParent, NULL, bb->baseButton.mTouchPointerId);
				if(isPressed)
				{
					GUIButton_Click(bb);
				}
			}
			bb->baseButton.mTouchPointerId = -1;
		}
	}
}
//---------------------------------------------------------------------------

void GUIButton_SetText(struct GUIButton *self, const wchar *text)
{
	if(self->mTxtMapPbjId >= 0)
	{
		txbSetDynamicTextLayer(self->mTxtMapPbjId, text, GUIContainer_GetLayer(self->baseButton.guiObject.mpParent));
	}
}
//---------------------------------------------------------------------------

void GUIButton_SetEnableInput(struct GUIButton *self, BOOL val)
{
	GUIBaseButton_EnableInput(&self->baseButton, val);
}
//---------------------------------------------------------------------------

void _GUIButton_GUIBaseButton_SetEnableInput(struct GUIBaseButton *obj, BOOL val)
{
	(void)obj;
	(void)val;
}
//---------------------------------------------------------------------------

BOOL GUIButton_IsEnableInput(const struct GUIButton *self)
{
	return GUIBaseButton_IsEnableInput(&self->baseButton);
}
//---------------------------------------------------------------------------
