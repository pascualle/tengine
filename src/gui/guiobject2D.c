/*
tengine - cross-platform game engine
------------------------------------

Copyright (c) 2013 Papa Pascualle
Contact Email: pascualle@gmail.com

Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
*/

#include "lib/tengine_low.h"
#include "guiobject2D.h"
#include "gui_low.h"

//---------------------------------------------------------------------------

void GUIObject2D_protected_Init(struct GUIObject2D *self)
{
	MI_CpuClear8(self, sizeof(struct GUIObject2D));
	self->mMapObjID = -1;
}
//---------------------------------------------------------------------------

void GUIObject2D_protected_Release(struct GUIObject2D *self)
{
	MI_CpuClear8(self, sizeof(struct GUIObject2D));
}
//---------------------------------------------------------------------------

s32 GUIObject2D_GetTabOrder(const struct GUIObject2D *self)
{
	SDK_NULL_ASSERT(self->mpGetTabOrder);
	SDK_NULL_ASSERT(self->mpObject);
	return self->mpGetTabOrder((struct GUIObject2D *)self->mpObject);
}
//---------------------------------------------------------------------------

void GUIObject2D_protected_ProcessInput(struct GUIObject2D *self, struct TouchPadData const *tpData)
{
	SDK_NULL_ASSERT(self->mpProcessInput);
	SDK_NULL_ASSERT(self->mpObject);
	self->mpProcessInput((struct GUIObject2D *)self->mpObject, tpData);
}
//---------------------------------------------------------------------------

void GUIObject2D_SetEnable(struct GUIObject2D *self, BOOL state)
{
	SDK_NULL_ASSERT(self->mpSetEnable);
	SDK_NULL_ASSERT(self->mpObject);
	self->mpSetEnable((struct GUIObject2D *)self->mpObject, state);
}
//---------------------------------------------------------------------------

BOOL GUIObject2D_IsEnable(const struct GUIObject2D *self)
{
	SDK_NULL_ASSERT(self->mpIsEnable);
	SDK_NULL_ASSERT(self->mpObject);
	return self->mpIsEnable((struct GUIObject2D *)self->mpObject);
}
//---------------------------------------------------------------------------

void GUIObject2D_protected_SetFocusFlag(struct GUIObject2D *self, BOOL state)
{
	SDK_NULL_ASSERT(self->mpSetFocusFlag);
	SDK_NULL_ASSERT(self->mpObject);
	self->mpSetFocusFlag((struct GUIObject2D *)self->mpObject, state);
}
//---------------------------------------------------------------------------

enum GUIControlType GUIObject2D_GetType(const struct GUIObject2D *self)
{
    return self->mType;
}
//---------------------------------------------------------------------------

s32 GUIObject2D_GetMapObjId(const struct GUIObject2D *self)
{
    return self->mMapObjID;
}
//---------------------------------------------------------------------------

void GUIObject2D_SetPosition(struct GUIObject2D *self, fx32 x, fx32 y)
{
	struct fxVec2 pos;
	SDK_ASSERT(self->mMapObjID >= 0);
	pos.x = x;
	pos.y = y;
	prSetPositionLayer(self->mMapObjID, pos, GUIContainer_GetLayer(self->mpParent));
}
//---------------------------------------------------------------------------

void GUIObject2D_GetPosition(const struct GUIObject2D *self, fx32 *x, fx32 *y)
{
	struct fxVec2 pos;
	SDK_ASSERT(self->mMapObjID >= 0);
	pos = prGetPositionLayer(self->mMapObjID, GUIContainer_GetLayer(self->mpParent));
	*x = pos.x;
    *y = pos.y;
}
//---------------------------------------------------------------------------

void GUIObject2D_Update(const struct GUIObject2D *self, s32 ms)
{
	SDK_NULL_ASSERT(self->mpObject);
	if(self->mpUpdate != NULL)
	{
		self->mpUpdate((struct GUIObject2D *)self->mpObject, ms);
	}
}
//---------------------------------------------------------------------------
